create table DATABASECHANGELOG
(
    ID            varchar(255) not null,
    AUTHOR        varchar(255) not null,
    FILENAME      varchar(255) not null,
    DATEEXECUTED  datetime     not null,
    ORDEREXECUTED int          not null,
    EXECTYPE      varchar(10)  not null,
    MD5SUM        varchar(35)  null,
    DESCRIPTION   varchar(255) null,
    COMMENTS      varchar(255) null,
    TAG           varchar(255) null,
    LIQUIBASE     varchar(20)  null,
    CONTEXTS      varchar(255) null,
    LABELS        varchar(255) null,
    DEPLOYMENT_ID varchar(10)  null
);

create table DATABASECHANGELOGLOCK
(
    ID          int          not null
        primary key,
    LOCKED      bit          not null,
    LOCKGRANTED datetime     null,
    LOCKEDBY    varchar(255) null
);

create table category
(
    id            bigint auto_increment
        primary key,
    code          varchar(255) not null,
    name          varchar(255) not null,
    category_type varchar(255) null,
    constraint ux_category_code
        unique (code)
);

create table jhi_authority
(
    name varchar(50) not null
        primary key
);

create table jhi_persistent_audit_event
(
    event_id   bigint auto_increment
        primary key,
    principal  varchar(50)  not null,
    event_date timestamp    null,
    event_type varchar(255) null
);

create index idx_persistent_audit_event
    on jhi_persistent_audit_event (principal, event_date);

create table jhi_persistent_audit_evt_data
(
    event_id bigint       not null,
    name     varchar(150) not null,
    value    varchar(255) null,
    primary key (event_id, name),
    constraint fk_evt_pers_audit_evt_data
        foreign key (event_id) references jhi_persistent_audit_event (event_id)
);

create index idx_persistent_audit_evt_data
    on jhi_persistent_audit_evt_data (event_id);

create table jhi_user
(
    id                 bigint auto_increment
        primary key,
    login              varchar(50)  not null,
    password_hash      varchar(60)  not null,
    first_name         varchar(50)  null,
    last_name          varchar(50)  null,
    email              varchar(191) null,
    image_url          varchar(256) null,
    activated          bit          not null,
    lang_key           varchar(6)   null,
    activation_key     varchar(20)  null,
    reset_key          varchar(20)  null,
    created_by         varchar(50)  not null,
    created_date       timestamp    null,
    reset_date         timestamp    null,
    last_modified_by   varchar(50)  null,
    last_modified_date timestamp    null,
    constraint ux_user_email
        unique (email),
    constraint ux_user_login
        unique (login)
);

create table jhi_user_authority
(
    user_id        bigint      not null,
    authority_name varchar(50) not null,
    primary key (user_id, authority_name),
    constraint fk_authority_name
        foreign key (authority_name) references jhi_authority (name),
    constraint fk_user_id
        foreign key (user_id) references jhi_user (id)
);

create table order_transaction
(
    id                bigint auto_increment
        primary key,
    program_id        bigint        null,
    program_name      varchar(255)  null,
    client_name       varchar(255)  null,
    participant_name  varchar(255)  null,
    created_date      datetime      null,
    message           varchar(1020) null,
    json_content      json          null,
    email             varchar(255)  null,
    external_order_id varchar(255)  null
);

create table program
(
    id                         bigint auto_increment
        primary key,
    name                       varchar(255)     not null,
    start_date                 date             null,
    reset_date                 date             null,
    last_sent                  date             null,
    is_retrigger_email         bit              null,
    is_eligible                bit              null,
    is_sent_registration_email bit              null,
    is_Registered_for_platform bit              null,
    is_scheduled_screening     bit              null,
    is_functionally            bit              null,
    logo_url                   varchar(255)     null,
    description                varchar(255)     null,
    user_point                 decimal(21, 2)   null,
    last_modified_date         datetime         null,
    created_date               datetime         null,
    last_modified_by           varchar(255)     null,
    created_by                 varchar(255)     null,
    status                     varchar(255)     null,
    is_use_point               bit              null,
    is_screen                  bit              null,
    is_hp                      bit              null,
    is_well_matric             bit              null,
    is_coaching                bit              null,
    is_use_level               bit default b'0' null,
    constraint ux_program_name
        unique (name)
);

create table client_program
(
    id          bigint auto_increment
        primary key,
    client_id   varchar(255) not null,
    program_id  bigint       not null,
    client_name varchar(255) null,
    constraint ux_clientId_programId
        unique (client_id, program_id),
    constraint fk_client_program_program_id
        foreign key (program_id) references program (id)
);

create table client_program_reward_setting
(
    id           bigint auto_increment
        primary key,
    client_id    varchar(255) not null,
    client_name  varchar(255) not null,
    access_token varchar(255) null,
    team_name    varchar(255) null,
    updated_date datetime     not null,
    updated_by   varchar(255) not null,
    program_id   bigint       not null,
    constraint fk_client_program_reward_setting_program_id
        foreign key (program_id) references program (id)
);

create table program_activity
(
    id            bigint auto_increment
        primary key,
    activity_id   varchar(255)     not null,
    activity_code varchar(255)     not null,
    program_id    bigint           not null,
    is_customized bit default b'0' null,
    constraint ux_program_activityid
        unique (program_id, activity_id),
    constraint fk_program_activity_program_id
        foreign key (program_id) references program (id)
);

create table program_category_point
(
    id            bigint auto_increment
        primary key,
    category_code varchar(255)     not null,
    category_name varchar(255)     null,
    percent_point decimal(19, 4)   null,
    value_point   decimal(21, 2)   null,
    program_id    bigint           not null,
    locked        bit default b'0' null,
    constraint ux_category_code_program
        unique (category_code, program_id),
    constraint fk_program_category_point_program_id
        foreign key (program_id) references program (id)
);

create table program_level
(
    id          bigint auto_increment
        primary key,
    description varchar(255) null,
    start_point int          null,
    end_point   int          null,
    level_order int          null,
    icon_path   varchar(255) null,
    name        varchar(255) null,
    end_date    date         null,
    program_id  bigint       not null,
    constraint fk_program_level_program_id
        foreign key (program_id) references program (id)
);

create table program_level_activity
(
    id               bigint auto_increment
        primary key,
    activity_code    varchar(255) null,
    activity_id      varchar(255) not null,
    program_level_id bigint       not null,
    constraint fk_program_level_activity_program_level_id
        foreign key (program_level_id) references program_level (id)
);

create table program_level_reward
(
    id               bigint auto_increment
        primary key,
    description      varchar(255)   null,
    quantity         int            null,
    code             varchar(255)   not null,
    program_level_id bigint         not null,
    reward_amount    decimal(21, 2) null,
    constraint fk_program_level_reward_program_level_id
        foreign key (program_level_id) references program_level (id)
);

create table program_sub_category_point
(
    id                        bigint auto_increment
        primary key,
    code                      varchar(255)     not null,
    percent_point             decimal(19, 4)   null,
    value_point               decimal(21, 2)   null,
    completions_cap           int              null,
    program_category_point_id bigint           not null,
    name                      varchar(255)     null,
    locked                    bit default b'0' null,
    constraint ux_sub__category_code_sub
        unique (code, program_category_point_id),
    constraint fk_program_sub_category_point_program_category_point_id
        foreign key (program_category_point_id) references program_category_point (id)
);

create table program_user
(
    id               bigint auto_increment
        primary key,
    participant_id   varchar(255)   not null,
    client_id        varchar(255)   not null,
    total_user_point decimal(21, 2) null,
    program_id       bigint         not null,
    current_level    int            null,
    email            varchar(255)   null,
    first_name       varchar(255)   null,
    last_name        varchar(255)   null,
    phone            varchar(255)   null,
    client_name      varchar(255)   null,
    constraint ux_participantId_clientId_programId
        unique (participant_id, client_id, program_id)
);

create table reward
(
    id          bigint auto_increment
        primary key,
    description varchar(255) null,
    code        varchar(255) not null,
    constraint ux_reward_code
        unique (code)
);

create table setting_configuration
(
    id           bigint auto_increment
        primary key,
    name         varchar(255) not null,
    value_string varchar(255) null,
    constraint ux_setting_configuration
        unique (name)
);

create table sub_category
(
    id          bigint auto_increment
        primary key,
    code        varchar(255) not null,
    name        varchar(255) null,
    category_id bigint       not null,
    constraint ux_sub_category_code
        unique (code),
    constraint fk_sub_category_category_id
        foreign key (category_id) references category (id)
);

create table user_event
(
    id              bigint auto_increment
        primary key,
    event_code      varchar(255)   not null,
    event_id        varchar(255)   not null,
    event_date      datetime       not null,
    event_point     decimal(21, 2) null,
    program_user_id bigint         not null,
    event_category  varchar(255)   null,
    constraint fk_user_event_program_user_id
        foreign key (program_user_id) references program_user (id)
);

create table user_reward
(
    id                   bigint auto_increment
        primary key,
    program_level        int            null,
    status               varchar(255)   null,
    order_id             varchar(255)   null,
    order_date           datetime       null,
    gift_amount          decimal(21, 2) null,
    gift_id              varchar(255)   null,
    catalog              varchar(255)   null,
    level_completed_date datetime       null,
    participant_id       varchar(255)   not null,
    email                varchar(255)   not null,
    client_id            varchar(255)   not null,
    program_user_id      bigint         not null,
    program_id           bigint         not null,
    constraint fk_user_reward_program_id
        foreign key (program_id) references program (id),
    constraint fk_user_reward_program_user_id
        foreign key (program_user_id) references program_user (id)
);

