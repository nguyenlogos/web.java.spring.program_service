
alter table program_activity add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_activity add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_biometric_data add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_biometric_data add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_category_point add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_category_point add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_healthy_range add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_healthy_range add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_level_activity add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_level_activity add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_level_path add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_level_path add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_level_practice add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_level_practice add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_level_reward add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_level_reward add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_sc_point_configuration add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_sc_point_configuration add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_sub_category_point add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_sub_category_point add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_subgroup add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_subgroup add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;


-- set default date for existing data in program_activity
update program_activity pa
inner join  program  on pa.program_id=program.id
set pa.created_date= program.created_date, pa.modified_date=program.last_modified_date
where 1=1;


-- set default date for existing data in program_category_point
update program_category_point pa
inner join  program  on pa.program_id=program.id
set pa.created_date= program.created_date, pa.modified_date=program.last_modified_date
where 1=1;


-- set default date for existing data in program_healthy_range
update program_healthy_range pa
inner join  program  on pa.program_id=program.id
set pa.created_date= program.created_date, pa.modified_date=program.last_modified_date
where 1=1;

-- set default date for existing data in program_level_activity
update program_level_activity pla
inner join program_level pl on pl.id=pla.program_level_id
set pla.created_date= pl.created_date, pla.modified_date=pl.modified_date
where 1=1;

-- set default date for existing data in program_level_path
update program_level_path pla
inner join program_level pl on pl.id=pla.program_level_id
set pla.created_date= pl.created_date, pla.modified_date=pl.modified_date
where 1=1;

-- set default date for existing data in program_level_reward
update program_level_reward pla
inner join program_level pl on pl.id=pla.program_level_id
set pla.created_date= pl.created_date, pla.modified_date=pl.modified_date
where 1=1;

-- set default date for existing data in program_sc_point_configuration
update program_sc_point_configuration pla
inner join program  on pla.program_id=program.id
set pla.created_date= program.created_date, pla.modified_date=program.last_modified_date
where 1=1;

-- set default date for existing data in program_sc_point_configuration
update program_sub_category_point psc
inner join program_category_point pc on pc.id=psc.program_category_point_id
set psc.created_date= pc.created_date, psc.modified_date=pc.modified_date
where 1=1;

-- set default date for existing data in program_subgroup
update program_subgroup ps
inner join program  on ps.program_id=program.id
set ps.created_date= program.created_date, ps.modified_date=program.last_modified_date
where 1=1;



