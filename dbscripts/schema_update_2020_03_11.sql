ALTER TABLE  program  ADD    is_outcomes_lite  boolean  default false;;
ALTER TABLE  program  ADD    is_tobacco_free  boolean  default false;;
ALTER TABLE  program  ADD    is_outcomes  boolean  default false;
create table program_healthy_range
(
    id             bigint auto_increment
        primary key,
    male_min  float  not null,
    male_max      float  not null,
    female_min  float  not null,
    female_max      float  not null,
    unidentified_min float  not null,
    unidentified_max   float  not null,
    is_apply_point boolean null default false,
    program_id bigint not null ,
    biometric_code varchar(255) not null ,
    constraint fk_program_healthy_range_program
        foreign key (program_id) references program (id)
);

create table program_biometric_data
(
    id bigint auto_increment primary key ,
    biometric_code varchar(50) not null ,
    biometric_name varchar(255) not null,
    male_min  float  not null,
    male_max      float  not null,
    female_min  float  not null,
    female_max      float  not null,
    unidentified_min float  not null,
    unidentified_max   float  not null
);

INSERT INTO program_biometric_data (id, biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
 VALUES (1, 'BP_Systolic__c', 'Systolic', 1, 120, 1, 120, 1, 120);

INSERT INTO program_biometric_data (id, biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES (2, 'BP_Diastolic__c', 'Diastolic', 1, 80, 1, 80, 1, 80);

INSERT INTO program_biometric_data (id, biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES (3, 'Waist__c', 'Waist', 1, 40, 1, 35, 1, 40);

INSERT INTO program_biometric_data (id, biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES (7, 'RFpg__c', 'Glucose', 70, 99, 70, 99, 70, 99);

INSERT INTO program_biometric_data (id, biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES (8, 'Non_Fasting_Y_N__c', 'Non-Fasting Glucose', 70, 125,  70, 125,  70, 125);

INSERT INTO program_biometric_data (id, biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES (9, 'BMI__c', 'BMI', 1, 25, 1, 25, 1, 25);

INSERT INTO program_biometric_data (id, biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES (10, 'TC_HDL_Ratio__c', 'Total Cholesterol / HDL Ratio', 1, 4.5, 1, 4, 1, 4.5);

INSERT INTO program_biometric_data (biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES ('RCho__c', 'Total Cholesterol ', 0, 0, 0, 0, 0, 0);
INSERT INTO program_biometric_data (biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES ('RHdl__c', 'HDL', 0, 0, 0, 0, 0, 0);
INSERT INTO program_biometric_data (biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
VALUES ('RTrig__c', 'Triglycerides', 0, 0, 0, 0, 0, 0);


INSERT INTO sub_category ( code, name, category_id) VALUES ( 'BP_Systolic__c', 'Systolic', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'BP_Diastolic__c', 'Diastolic', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'Waist__c', 'Waist', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'RFpg__c', 'Glucose', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'Non_Fasting_Y_N__c', 'Non-Fasting Glucose', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'BMI__c', 'BMI', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'TC_HDL_Ratio__c', 'Total Cholesterol / HDL Ratio', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'RTrig__c', 'Triglycerides', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'RCho__c', 'Total Cholesterol', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'RHdl__c', 'HDL', 7);

UPDATE sub_category
set code = 'HUMAN_PERFORMANCE_ASSESSMENT',
name = 'Human Performance Assessment'
where code = 'WELLBEING_ASSESSMENT';

UPDATE sub_category
set code = 'OTHER_ASSESSMENT',
name = 'Other Assessments'
where code = 'FLOURING_INDEX';

UPDATE sub_category
set code = 'FLOURING_INDEX',
name = 'Flourishing Index (Quarterly)'
where code = 'ADURO_INDEX';

DELETE FROM sub_category
WHERE code = 'WBA_HEARTBEAT';

UPDATE program_sub_category_point
set code = 'OTHER_ASSESSMENT'
where code = 'FLOURING_INDEX';

UPDATE program_sub_category_point
set name = 'Other Assessments'
where code = 'OTHER_ASSESSMENT';


UPDATE program_sub_category_point
set code = 'FLOURING_INDEX'
where code = 'ADURO_INDEX';

UPDATE program_sub_category_point
set name = 'Flourishing Index (Quarterly)'
where code = 'FLOURING_INDEX';


UPDATE program_sub_category_point
set code = 'HUMAN_PERFORMANCE_ASSESSMENT'
where code = 'WELLBEING_ASSESSMENT';

UPDATE program_sub_category_point
set name = 'Human Performance Assessment'
where code = 'HUMAN_PERFORMANCE_ASSESSMENT';
