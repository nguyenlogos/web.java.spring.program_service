create table activity_event_queue
(
    id             bigint auto_increment
        primary key,
    participant_id varchar(255) not null,
    client_id      varchar(255) not null,
    activity_code  varchar(255) not null,
    email          varchar(255) not null,
    created_date   datetime     not null,
    first_name     varchar(255) not null,
    last_name      varchar(255) not null,
    transaction_id varchar(255) not null,
    error_message  varchar(255) null,
    execute_status varchar(255) null,
    subgroup_id varchar(255) null
);

