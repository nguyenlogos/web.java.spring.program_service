INSERT INTO category(id, code, name, category_type) VALUES (1, 'COACH', '1:1 Coaching', 'Change');
INSERT INTO category(id, code, name, category_type) VALUES (2, 'ACTIVITIES', 'Activities', 'Change');
INSERT INTO category(id, code, name, category_type) VALUES (3, 'ASSESSMENTS', 'Assessments', 'Data Collection');
INSERT INTO category(id, code, name, category_type) VALUES (4, 'HPCONTENT', 'HPCONTENT', 'HPCONTENT');
INSERT INTO category(id, code, name, category_type) VALUES (5, 'HPINTERACTIONS', 'HPINTERACTIONS', 'HPINTERACTIONS');
INSERT INTO category(id, code, name, category_type) VALUES (6, 'BONUS', 'BONUS', 'BONUS');
INSERT INTO category(id, code, name, category_type) VALUES (7, 'WELLMETRICS', 'Wellmetrics', 'Change');
