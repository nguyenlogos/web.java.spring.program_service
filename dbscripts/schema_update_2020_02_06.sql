create table term_and_condition
(
    id             bigint auto_increment
        primary key,
    client_id      varchar(255) not null,
    subgroup_id  varchar(255)  null,
    title          varchar(255)  null,
    content     text  null,
    is_target_subgroup      boolean null,
    subgroup_name varchar(255)  null
);



