
create table user_reward
(
    id                   bigint auto_increment
        primary key,
    program_level        int            null,
    status               varchar(255)   null,
    order_id             varchar(255)   null,
    order_date           datetime       null,
    gift_amount          decimal(21, 2) null,
    gift_id              varchar(255)   null,
    catalog              varchar(255)   null,
    level_completed_date datetime       null,
    participant_id       varchar(255)   not null,
    email                varchar(255)   not null,
    client_id            varchar(255)   not null,
    program_user_id      bigint         not null,
    program_id           bigint         not null,
    constraint fk_user_reward_program_id
        foreign key (program_id) references program (id),
    constraint fk_user_reward_program_user_id
        foreign key (program_user_id) references program_user (id)
);
