
ALTER TABLE  program_activity ADD  is_customized  bit default b'0' null;

ALTER TABLE  program ADD  is_use_level  bit default b'0' null;

-- 22/july
create table setting_configuration
(
    id              bigint auto_increment primary key,

    name           varchar(255) not null,
    value_string    varchar(255)  null,
    constraint ux_setting_configuration
        unique (name)
);

ALTER TABLE  program_user ADD  email  varchar(255) null;
ALTER TABLE  program_user ADD  first_name  varchar(255) null;
ALTER TABLE  program_user ADD  last_name  varchar(255) null;
ALTER TABLE  program_user ADD  phone  varchar(255) null;


create table order_transaction
(
    id              bigint auto_increment primary key,
    program_id      bigint  ,
    program_name    varchar(255)  null,
    client_name     varchar(255)  null,
    participant_name    varchar(255)  null,
    created_date  datetime       null,
    message varchar(1020)  null,
    json_content json  null,
    email varchar(255)  null,
    external_order_id varchar(255)  null
);

ALTER TABLE  program_user ADD  client_name  varchar(255) null;





