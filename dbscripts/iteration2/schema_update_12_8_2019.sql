-- auto-generated definition

-- drop table program_level_reward;
create table program_level_reward
(
    id               bigint auto_increment
        primary key,
    description      varchar(255)   null,
    quantity         int            null,
    code             varchar(255)   not null,
    reward_amount    decimal(21, 2) not null,
    program_level_id bigint         not null,
    constraint fk_program_level_reward_program_level_id
        foreign key (program_level_id) references program_level (id)
);

-- auto-generated definition
create table client_program_reward_setting
(
    id           bigint auto_increment
        primary key,
    client_id    varchar(255) not null,
    client_name  varchar(255) not null,
    access_token varchar(255) null,
    team_name    varchar(255) null,
    updated_date datetime     not null,
    updated_by   varchar(255) not null,
    program_id   bigint       not null,
    constraint fk_client_program_reward_setting_program_id
        foreign key (program_id) references program (id)
);

