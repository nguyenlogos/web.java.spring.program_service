create index client_program__client_name_index
	on client_program (client_name);

create index client_brand_setting__client_name_index
	on client_brand_setting (client_name);

create index activity_event_queue__participant_id_index
	on activity_event_queue (participant_id);

create index activity_event_queue__client_id_index
	on activity_event_queue (client_id);

create index activity_event_queue__activity_code_index
	on activity_event_queue (activity_code);

create index activity_event_queue__email_index
	on activity_event_queue (email);

create index activity_event_queue__subgroup_id_index
	on activity_event_queue (subgroup_id);

create index user_event_event_code_index
	on user_event (event_code);

create index user_event_event_id_index
	on user_event (event_id);

create index user_event_event_category_index
	on user_event (event_category);


create index program_name_index
	on program (name);

create index program_status_index
	on program (status);


create index program_activity__activity_code_index
	on program_activity (activity_code);


create index program_activity__program_phase_id_index
	on program_activity (program_phase_id);

create index program_category_point__category_code_index
	on program_category_point (category_code);

create index program_category_point__category_name_index
	on program_category_point (category_name);

create index program_category_point__program_id_index
	on program_category_point (program_id);


create index program_level_name_index
	on program_level (name);


create index program_level_activity__activity_code_index
	on program_level_activity (activity_code);

create index program_level_activity__activity_id_index
	on program_level_activity (activity_id);

create index program_level_activity__subgroup_id_index
	on program_level_activity (subgroup_id);

create index program_level_activity__subgroup_name_index
	on program_level_activity (subgroup_name);




create index program_level_path__path_id_index
	on program_level_path (path_id);

create index program_level_path__name_index
	on program_level_path (name);

create index program_level_path__subgroup_id_index
	on program_level_path (subgroup_id);

create index program_level_path__subgroup_name_index
	on program_level_path (subgroup_name);

create index program_level_path__path_type_index
	on program_level_path (path_type);

create index program_level_path__path_category_index
	on program_level_path (path_category);




create index program_level_practice__practice_id_index
	on program_level_practice (practice_id);
create index program_level_practice__name_index
	on program_level_practice (name);

create index program_level_practice__subgroup_id_index
	on program_level_practice (subgroup_id);




create index program_level_reward__code_index
	on program_level_reward (code);

create index program_level_reward__reward_type_index
	on program_level_reward (reward_type);

create index program_level_reward__subgroup_id_index
	on program_level_reward (subgroup_id);

create index program_level_reward__campaign_id_index
	on program_level_reward (campaign_id);




create index program_sub_category_point__code_index
	on program_sub_category_point (code);

create index program_sub_category_point__name_index
	on program_sub_category_point (name);




create index program_subgroup__subgroup_id_index
	on program_subgroup (subgroup_id);

create index program_subgroup__subgroup_name_index
	on program_subgroup (subgroup_name);





create index program_user__subgroup_id_index
	on program_user (subgroup_id);

create index program_user__subgroup_name_index
	on program_user (subgroup_name);

create index program_user__client_name_index
	on program_user (client_name);




create index term_and_condition__client_id_index
	on term_and_condition (client_id);

create index term_and_condition__subgroup_id_index
	on term_and_condition (subgroup_id);

create index term_and_condition__title_index
	on term_and_condition (title);

create index term_and_condition__subgroup_name_index
	on term_and_condition (subgroup_name);


create index user_reward__status_index
	on user_reward (status);

create index user_reward__order_id_index
	on user_reward (order_id);

create index user_reward__gift_id_index
	on user_reward (gift_id);

create index user_reward__participant_id_index
	on user_reward (participant_id);

create index user_reward__client_id_index
	on user_reward (client_id);

create index user_reward__reward_type_index
	on user_reward (reward_type);

create index user_reward__reward_code_index
	on user_reward (reward_code);


create index program_biometric_data__biometric_code_index
	on program_biometric_data (biometric_code);

create index program_biometric_data__biometric_name_index
	on program_biometric_data (biometric_name);
