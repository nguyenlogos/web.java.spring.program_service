ALTER TABLE  program_level_reward  ADD    subgroup_id  varchar(255)  null;
ALTER TABLE  program_level_reward  ADD    subgroup_name  varchar(255)  null;





ALTER TABLE  program_level_activity  ADD    subgroup_id  varchar(255)  null;
ALTER TABLE  program_level_activity  ADD    subgroup_name  varchar(255)  null;

ALTER TABLE  program_user ADD    subgroup_id  varchar(255)  null;
ALTER TABLE  program_user ADD    subgroup_name  varchar(255)  null;




drop index ux_program_name on program;

create table program_subgroup
(
    id          bigint auto_increment
        primary key,
    program_id  bigint       not null,
    subgroup_id varchar(255)  not null,
    subgroup_name  varchar(255)  not null,
    constraint fk_program_subgroup_program_id
        foreign key (program_id) references program (id)
);

-- ALTER TABLE program_user ADD INDEX index_program_user_participant (client_id,  program_id, participant_id);
ALTER TABLE user_event ADD INDEX index_user_event_code (program_user_id,  event_code);

ALTER TABLE  program  ADD    apply_reward_all_subgroup   bit default b'0'  null;
-- alter table program_level_reward drop column apply_all_subgroup;




