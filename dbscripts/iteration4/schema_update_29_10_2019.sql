ALTER TABLE  program  ADD    is_hpsf   bit default b'0'  null;
ALTER TABLE  program  ADD    is_htk   bit default b'0'  null;
ALTER TABLE  program  ADD    is_labcorp   bit default b'0'  null;
ALTER TABLE  program  ADD    labcorp_account_number   varchar(255)  null;
ALTER TABLE  program  ADD    labcorp_file_url   varchar(255)  null;
