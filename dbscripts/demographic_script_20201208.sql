use adp;

-- clean up table em_demographics before inserting
truncate table  em_demographics

-- get JobCode
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'postalCode', p.postal_code, 'postal code'
from em_employees e
inner join em_employees_profile p on e.id=p.employee_id
where p.postal_code is not null
order by e.employer_id;

-- get JobCode
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'jobCode', e.job_code, 'job code'
from em_employees e
where e.job_code is not null
order by e.employer_id;

-- get Division
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'division', e.division, 'division'
from em_employees e
where e.division is not null
order by e.employer_id;

-- get Group
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'groupCode', e.group_code, 'group code'
from em_employees e
where e.group_code is not null
order by e.employer_id;

-- get Country
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'country',  p.country, 'country'
from em_employees e
inner join adp.em_employees_profile p on e.id =p.employee_id
where p.country  is not null
order by e.employer_id;

-- get health_plan
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'healthPlan', e.health_plan, 'health plan'
from em_employees e
where e.health_plan is not null
order by e.employer_id;

-- get location
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'location', e.location, 'location'
from em_employees e
where e.location is not null
order by e.employer_id;

-- get Region
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'region', e.region, 'region'
from em_employees e
where e.region is not null
order by e.employer_id;

-- get Store
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'store', e.store, 'store'
from em_employees e
where e.store is not null
order by e.employer_id;

-- get AgeRange
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'ageRange', e.age_range, 'age range'
from em_employees e
where e.age_range is not null
order by e.employer_id;

-- get BargainingUnit
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'bargainingUnit', e.bargaining_unit, 'bargaining unit'
from em_employees e
where e.bargaining_unit is not null
order by e.employer_id;

-- get Class
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'class', e.class, 'class'
from em_employees e
where e.class is not null
order by e.employer_id;

-- get Department
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'department', e.department, 'department'
from em_employees e
where e.department is not null
order by e.employer_id;

-- get District
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'district', e.district, 'district'
from em_employees e
where e.district is not null
order by e.employer_id;

-- get Facility
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'facility', e.facility, 'facility'
from em_employees e
where e.facility is not null
order by e.employer_id;

-- get Gender
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'gender', p.gender, 'gender'
from em_employees e
inner join em_employees_profile p on e.id= p.employee_id
where p.gender  is not null
order by e.employer_id;

-- get Status
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'status', e.status, 'status'
from em_employees e
where e.status is not null
order by e.employer_id;

-- get RelationshipCode
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'relationship', e.relationship, 'relationship'
from em_employees e
where e.relationship is not null
order by e.employer_id;

-- get City
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'city',  p.city, 'city'
from em_employees e
inner join em_employees_profile p on e.id=p.employee_id
where  p.city  is not null
order by e.employer_id;

-- get State
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'state',  p.state, 'state'
from em_employees e
inner join em_employees_profile p on e.id=p.employee_id
where  p.state  is not null
order by e.employer_id;

-- get NewHire
insert into em_demographics (employer_id, demographic_type, demographic_value, description)
select distinct e.employer_id, 'newHire', e.new_hire, 'new hire'
from em_employees e
where e.new_hire is not null
order by e.employer_id;


