create table client_brand_setting
(
    id             bigint auto_increment
        primary key,
    client_id      varchar(255) not null,
    client_name      varchar(255)  null,
    sub_path_url  varchar(255) not null,
    web_logo_url          varchar(255)  null,
    primary_color_value   varchar(255)  null,
    constraint ux_client_brand_setting_client_id
        unique (client_id),
    constraint ux_client_brand_setting_sub_path_url
        unique (sub_path_url)
);



create table program_level_path
(
    id             bigint auto_increment
        primary key,
    path_id varchar(255) not null,
    name      varchar(255) not null,
    subgroup_id  varchar(255)  null,
    subgroup_name   varchar(255)  null,
    path_type   varchar(255)  null,
    path_category   varchar(255)  null,
    program_level_id bigint       not null,
    constraint fk_program_level_path_program_level_id
        foreign key (program_level_id) references program_level (id)
);



create table program_level_practice
(
    id             bigint auto_increment
        primary key,
    practice_id varchar(255) not null,
    name      varchar(255) not null,
    subgroup_id  varchar(255)  null,
    subgroup_name   varchar(255)  null,
    program_level_id bigint       not null,
    constraint fk_program_level_practice_program_level_id
        foreign key (program_level_id) references program_level (id)
);

ALTER TABLE  program  ADD    landing_background_image_url   varchar(400)  null;

