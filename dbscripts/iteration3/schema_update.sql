ALTER TABLE  client_program_reward_setting ADD  team_id  varchar(255) null;

ALTER TABLE  program_level_reward ADD  reward_type  varchar(255) not null;

ALTER TABLE  program ADD  is_template  bit default b'0' not null;

ALTER TABLE  user_reward ADD  reward_type  varchar(255)  null;

ALTER TABLE  user_reward ADD  reward_code  varchar(255)  null;

ALTER TABLE  user_reward ADD  reward_amount decimal(21,2)  null;

ALTER TABLE  program ADD  end_time_zone varchar(255)  null;

ALTER TABLE  program ADD  start_time_zone varchar(255)  null;

ALTER TABLE  program ADD  preview_time_zone varchar(255)  null;

ALTER TABLE  program ADD  preview_date DATETIME  null;

ALTER TABLE  program ADD  is_preview  bit default b'0'  null;

ALTER TABLE  program ADD  level_structure  varchar(255)  null;

ALTER TABLE  order_transaction ADD  user_reward_id  bigint(20)  null;


ALTER TABLE  program_level_reward ADD  campaign_id  varchar(255)  null;

ALTER TABLE  user_reward ADD  campaign_id  varchar(255)  null;

alter table order_transaction modify json_content json null;

alter table order_transaction modify message longtext null;

-- auto-generated definition
create table program_phase
(
    id            bigint auto_increment
        primary key,
    program_id    bigint       not null,
    start_date    datetime     not null,
    end_date    datetime     not  null,
    phase_order    integer      not  null,
    constraint fk_program_phase_program_id
        foreign key (program_id) references program (id)
);

ALTER TABLE  program_activity ADD  program_phase_id  bigint(20)  null;

CREATE TABLE shedlock(
     name VARCHAR(64),
     lock_until TIMESTAMP(3) NULL,
     locked_at TIMESTAMP(3) NULL,
     locked_by  VARCHAR(255),
     PRIMARY KEY (name)
)


ALTER TABLE  client_program_reward_setting ADD  customerIdentifier  varchar(255)  null;

ALTER TABLE  client_program_reward_setting ADD  accountIdentifier  varchar(255)  null;

alter table client_program_reward_setting drop column team_id;
alter table client_program_reward_setting drop column team_name;
alter table client_program_reward_setting drop column access_token;


ALTER TABLE  program ADD  program_length  INTEGER  null;

ALTER TABLE  program_activity ADD    master_id  varchar(255)  null;

create table client_setting
(
    id                        bigint auto_increment
        primary key,
    client_id                 varchar(255)   not null,
    client_name               varchar(255)   not null,
    client_email              varchar(255)   null,
    tango_account_identifier  varchar(255)   null,
    tango_customer_identifier varchar(255)   null,
    tango_current_balance     decimal(21, 2) null,
    tango_threshold_balance   decimal(21, 2) null,
    tango_credit_token        varchar(255)   null,
    updated_date              datetime       not null,
    updated_by                varchar(255)   not null,
    constraint ux_client_setting_client_id
        unique (client_id),
    constraint ux_client_setting_client_name
        unique (client_name)
);







