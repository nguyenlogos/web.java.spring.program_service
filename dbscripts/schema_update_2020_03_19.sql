
create table wellmetric_event_queue
(
    id bigint auto_increment primary key ,
    email char(36) not null,
    client_id varchar(20) null,
    participant_id char(36) not null ,
    execute_status varchar(36) not null,
    gender varchar(20) not null,
    created_at datetime not null ,
    attempt_count int not null,
    last_attempt_time datetime not null ,
    error_message text not null,
    subgroup_id varchar(36) null,
    result varchar(36) null ,
    event_code varchar(150) not null,
    event_id varchar(150) not null,
    healthy_value float not null,
    has_incentive boolean null
);


create index wellmetric_event_queue_execute_status_index
    on wellmetric_event_queue (execute_status ASC);
create index wellmetric_event_queue_id_index
    on wellmetric_event_queue (id ASC);
create index wellmetric_event_queue_eventCode_index
    on wellmetric_event_queue (event_code ASC);


create table notification_center
(
    id bigint auto_increment primary key ,
    title text not null,
    participant_id char(36) not null ,
    execute_status varchar(36) not null,
    created_at datetime not null ,
    attempt_count int not null,
    last_attempt_time datetime not null ,
    error_message text null,
    event_id varchar(150) not null,
    content text not null,
    notification_type varchar(36) not null,
    receiver_name varchar(36) not null,
    is_ready boolean null
);

create index notification_center_is_ready_index
    on notification_center (is_ready);

create index notification_center_id_index
    on notification_center (id);

create index notification_center_execute_status_index
    on notification_center (execute_status);
