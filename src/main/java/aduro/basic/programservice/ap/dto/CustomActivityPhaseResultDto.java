package aduro.basic.programservice.ap.dto;

import java.util.List;

public class CustomActivityPhaseResultDto {
    List<CustomActivityResultDto> customActivities;

    public List<CustomActivityResultDto> getCustomActivities() {
        return customActivities;
    }

    public void setCustomActivities(List<CustomActivityResultDto> customActivities) {
        this.customActivities = customActivities;
    }
}
