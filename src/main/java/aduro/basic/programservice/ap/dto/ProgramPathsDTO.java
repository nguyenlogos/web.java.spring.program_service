package aduro.basic.programservice.ap.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProgramPathsDTO {
    private Long id;
    private String name;
    private String background;
    private PathCategoryDto category;
    private List<String> metaTags;
    private List<String> tags;
    private String type;
    private Long categoryId;
    private Boolean discoverable;
    private Instant createdAt;
    private Instant updatedAt;
    private Boolean isRas;
    private String title;
    private Boolean isPath;
    private String headerImageUrl;
    private Integer assignToLevel;
    private BigDecimal point;
    private Boolean required;
}
