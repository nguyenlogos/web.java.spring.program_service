package aduro.basic.programservice.ap.dto;

public class ActivityRecurrenceDto {
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
