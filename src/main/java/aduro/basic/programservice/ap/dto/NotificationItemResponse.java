package aduro.basic.programservice.ap.dto;

public class NotificationItemResponse {

    public NotificationItemDTO getNotificationItemDTO() {
        return notificationItemDTO;
    }

    public void setNotificationItemDTO(NotificationItemDTO notificationItemDTO) {
        this.notificationItemDTO = notificationItemDTO;
    }

    private NotificationItemDTO notificationItemDTO;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String message;
    private String error;
}
