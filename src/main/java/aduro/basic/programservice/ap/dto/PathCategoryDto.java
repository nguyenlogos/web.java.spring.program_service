package aduro.basic.programservice.ap.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PathCategoryDto {
    private Long id;
    private String name;
    private String description;
    private String color;
    private String thumbnailUrl;
    private String cardTypeTextColor;
    private String completedTextColor;
    private String incompleteTextColor;
    private String progressBarTextColor;
    private String enrollButtonColor;
    private String overlayBackgroundColor;
    private String progressBarIncompleteTextColor;
}
