package aduro.basic.programservice.ap.dto;

import java.util.List;

public class SearchUserRequest {
    List<String> aduroIds;

    public List<String> getAduroIds() {
        return aduroIds;
    }

    public void setAduroIds(List<String> aduroIds) {
        this.aduroIds = aduroIds;
    }
}
