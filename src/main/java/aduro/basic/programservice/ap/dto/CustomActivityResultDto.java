package aduro.basic.programservice.ap.dto;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomActivityResultDto {
    private long id;
    private String title;
    private long activityId;
    private String headerImageUrl;
    private String type;
    private String assignToLevel;
    private BigDecimal point;
    private String progressStatus;
    private CategoryDto category;
    private long programId;
    private long completions;
    private boolean activeCompletion;
    private String secondaryType;
    private boolean isGroup;
    private long quantityCompletions;
    private String quantityUnit;
    private boolean employerVerified;
    private boolean isFeatured;
    private boolean isRas;
    private String screeningType;
    private String preventiveFileUrl;
    private String activityCode;
    private boolean bonusPointsEnabled;
    private long bonusPoints;
    private String sizeOfActivity;
    private long maxTeamSize;
    private long programPhaseId;
    private boolean isCustomized;
    private String programName;
    private long templateActivityId;
    private String startDate;
    private String endDate;
    private boolean isRequired;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public String getHeaderImageUrl() {
        return headerImageUrl;
    }

    public void setHeaderImageUrl(String headerImageUrl) {
        this.headerImageUrl = headerImageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAssignToLevel() {
        return assignToLevel;
    }

    public void setAssignToLevel(String assignToLevel) {
        this.assignToLevel = assignToLevel;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public String getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(String progressStatus) {
        this.progressStatus = progressStatus;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public long getProgramId() {
        return programId;
    }

    public void setProgramId(long programId) {
        this.programId = programId;
    }

    public long getCompletions() {
        return completions;
    }

    public void setCompletions(long completions) {
        this.completions = completions;
    }

    public boolean isActiveCompletion() {
        return activeCompletion;
    }

    public void setActiveCompletion(boolean activeCompletion) {
        this.activeCompletion = activeCompletion;
    }

    public String getSecondaryType() {
        return secondaryType;
    }

    public void setSecondaryType(String secondaryType) {
        this.secondaryType = secondaryType;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public long getQuantityCompletions() {
        return quantityCompletions;
    }

    public void setQuantityCompletions(long quantityCompletions) {
        this.quantityCompletions = quantityCompletions;
    }

    public String getQuantityUnit() {
        return quantityUnit;
    }

    public void setQuantityUnit(String quantityUnit) {
        this.quantityUnit = quantityUnit;
    }

    public boolean isEmployerVerified() {
        return employerVerified;
    }

    public void setEmployerVerified(boolean employerVerified) {
        this.employerVerified = employerVerified;
    }

    public boolean isFeatured() {
        return isFeatured;
    }

    public void setFeatured(boolean featured) {
        isFeatured = featured;
    }

    public boolean isRas() {
        return isRas;
    }

    public void setRas(boolean ras) {
        isRas = ras;
    }

    public String getScreeningType() {
        return screeningType;
    }

    public void setScreeningType(String screeningType) {
        this.screeningType = screeningType;
    }

    public String getPreventiveFileUrl() {
        return preventiveFileUrl;
    }

    public void setPreventiveFileUrl(String preventiveFileUrl) {
        this.preventiveFileUrl = preventiveFileUrl;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public boolean isBonusPointsEnabled() {
        return bonusPointsEnabled;
    }

    public void setBonusPointsEnabled(boolean bonusPointsEnabled) {
        this.bonusPointsEnabled = bonusPointsEnabled;
    }

    public long getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(long bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    public String getSizeOfActivity() {
        return sizeOfActivity;
    }

    public void setSizeOfActivity(String sizeOfActivity) {
        this.sizeOfActivity = sizeOfActivity;
    }

    public long getMaxTeamSize() {
        return maxTeamSize;
    }

    public void setMaxTeamSize(long maxTeamSize) {
        this.maxTeamSize = maxTeamSize;
    }

    public long getProgramPhaseId() {
        return programPhaseId;
    }

    public void setProgramPhaseId(long programPhaseId) {
        this.programPhaseId = programPhaseId;
    }

    public boolean isCustomized() {
        return isCustomized;
    }

    public void setCustomized(boolean customized) {
        isCustomized = customized;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public long getTemplateActivityId() {
        return templateActivityId;
    }

    public void setTemplateActivityId(long templateActivityId) {
        this.templateActivityId = templateActivityId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }
}
