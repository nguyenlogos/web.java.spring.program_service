package aduro.basic.programservice.ap.dto;

import java.util.List;

public class SearchUserResponse {
    List<UserDto> users;

    public List<UserDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserDto> users) {
        this.users = users;
    }
}
