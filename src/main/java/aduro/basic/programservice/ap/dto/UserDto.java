package aduro.basic.programservice.ap.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private long userId;
    private String aduroId;
    private String subgroupId;
    private String subgroupName;
    private String email;
    private String firstName;
    private String lastName;

    private boolean asProgramTester;

    @Override
    public String toString() {
        return "UserDto{" +
            "userId=" + userId +
            ", subgroupId='" + subgroupId + '\'' +
            ", subgroupName='" + subgroupName + '\'' +
            ", email='" + email + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            '}';
    }
}
