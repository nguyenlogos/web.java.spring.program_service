package aduro.basic.programservice.ap.dto;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonProperty;

public class CustomActivityResponseDto {

    private String message;

    private String error;

    @JsonProperty("customActivty")
    private CustomActivityResultDto customActivity;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CustomActivityResultDto getCustomActivity() {
        return customActivity;
    }

    public void setCustomActivity(CustomActivityResultDto customActivity) {
        this.customActivity = customActivity;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
