package aduro.basic.programservice.ap.dto;

public class CountCustomActivityResultDto {
    Integer total;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
