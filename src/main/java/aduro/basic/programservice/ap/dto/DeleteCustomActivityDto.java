package aduro.basic.programservice.ap.dto;

import java.util.List;

public class DeleteCustomActivityDto {
    private List<String> customActivityIds;
    private List<Long> programActivityIds;
    private Long programId;

    public List<String> getCustomActivityIds() {
        return customActivityIds;
    }

    public void setCustomActivityIds(List<String> customActivityIds) {
        this.customActivityIds = customActivityIds;
    }

    public List<Long> getProgramActivityIds() {
        return programActivityIds;
    }

    public void setProgramActivityIds(List<Long> programActivityIds) {
        this.programActivityIds = programActivityIds;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }
}
