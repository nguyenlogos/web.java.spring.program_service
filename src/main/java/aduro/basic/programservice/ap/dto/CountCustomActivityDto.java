package aduro.basic.programservice.ap.dto;

import java.util.List;

public class CountCustomActivityDto {
    List<String> customActivityIds;

    public List<String> getCustomActivityIds() {
        return customActivityIds;
    }

    public void setCustomActivityIds(List<String> customActivityIds) {
        this.customActivityIds = customActivityIds;
    }
}
