package aduro.basic.programservice.ap.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckDeletePathDto {
    private boolean isIsRemovable;
}
