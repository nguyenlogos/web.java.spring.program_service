package aduro.basic.programservice.ap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

@Configuration
public class AduroPlatformAdapter {

    @Autowired
    private Environment env;

    protected String getBaseUrl() throws Exception {
        String aduroPlatformApiUrl = env.getProperty("aduro-platform.aduro-platform-api-url");
        if(isNullOrEmpty(aduroPlatformApiUrl))
            throw new Exception("Missing AP  URL...");
        return aduroPlatformApiUrl;
    }
}
