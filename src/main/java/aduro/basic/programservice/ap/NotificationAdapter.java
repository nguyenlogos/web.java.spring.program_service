package aduro.basic.programservice.ap;

import aduro.basic.programservice.ap.dto.NotificationItemRequest;
import aduro.basic.programservice.ap.dto.NotificationItemResponse;
import aduro.basic.programservice.helpers.RequestHeaderBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class NotificationAdapter extends AduroPlatformAdapter {
    private final Logger log = LoggerFactory.getLogger(NotificationAdapter.class);
    private String resourceAddNotification = "/common/integration/notifications/add";
    private String resourceAddHPANotification = "/common/hpa/notifications/add";
    private String resourceAddIncentiveNotification = "/common/incentive/notifications/add";

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<NotificationItemResponse> AddNotificationItem(NotificationItemRequest notificationItemRequest) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<NotificationItemRequest> entity = new HttpEntity<NotificationItemRequest>(notificationItemRequest, headers);
            ResponseEntity<NotificationItemResponse> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceAddNotification,entity, NotificationItemResponse.class);
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }

    @Async
    public CompletableFuture<NotificationItemResponse> AddHPANotificationItem(NotificationItemRequest notificationItemRequest) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<NotificationItemRequest> entity = new HttpEntity<NotificationItemRequest>(notificationItemRequest, headers);
            ResponseEntity<NotificationItemResponse> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceAddHPANotification,entity, NotificationItemResponse.class);
            log.debug("Request to save ProgramActivity ASSESSMENT AddHPANotificationItem responseEntity: {}", responseEntity);
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }

    @Async
    public CompletableFuture<NotificationItemResponse> sendIncentiveNotificationItem(NotificationItemRequest notificationItemRequest) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<NotificationItemRequest> entity = new HttpEntity<NotificationItemRequest>(notificationItemRequest, headers);
            ResponseEntity<NotificationItemResponse> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceAddIncentiveNotification,entity, NotificationItemResponse.class);
            log.debug("Request to save ProgramActivity Incentive : {}", responseEntity);
            return CompletableFuture.completedFuture(responseEntity.getBody());

        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }

}
