package aduro.basic.programservice.ap;

import aduro.basic.programservice.ap.dto.CheckDeletePathDto;
import aduro.basic.programservice.ap.dto.PathDTO;
import aduro.basic.programservice.helpers.RequestHeaderBuilder;

import aduro.basic.programservice.service.dto.APIResponse;
import aduro.basic.programservice.service.dto.CheckPathInProgressRequest;
import aduro.basic.programservice.service.dto.ListProgramPathsResponseDto;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class PathAdapter extends AduroPlatformAdapter {

    private String resourceCustomPath = "/common/cc-activities/custom-activities/ras";
    private String resourceAttachPath = "/common/programs/company/attach-topic";
    private String resourceVerifyPath = "/common/programs/topic/verify";
    private String resourceAllPathUser = "/common/cc-paths/paths-by-user";

    private final RestTemplate restTemplate;

    public PathAdapter(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    public List<PathDTO> getRasPathByUserId(String userId, Long programId) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = String.format("%s%s?programId=%s&participantId=%s",this.getBaseUrl(), resourceCustomPath, programId, userId);
            ResponseEntity<List<PathDTO>> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<PathDTO>>(){});
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }

    public String attachPathToCompany(PathDTO pathDTO) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<PathDTO> entity = new HttpEntity<PathDTO>(pathDTO, headers);
            String requestUrl = String.format("%s%s", this.getBaseUrl(), resourceAttachPath, entity);
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(requestUrl ,entity, String.class);
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public CheckDeletePathDto verifyPathById(CheckPathInProgressRequest dto) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<CheckPathInProgressRequest> entity = new HttpEntity<>(dto, headers);
            String requestUrl = String.format("%s%s", this.getBaseUrl(), resourceVerifyPath);
            ResponseEntity<CheckDeletePathDto> responseEntity = restTemplate.postForEntity(requestUrl ,entity, CheckDeletePathDto.class);
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public APIResponse<ListProgramPathsResponseDto> getPathsByClientIdAndParticipantId(String clientId,
                                                                                       String participantId
    ) throws Exception {
        APIResponse<ListProgramPathsResponseDto> res = new APIResponse<ListProgramPathsResponseDto>();
        List<String> errors = new ArrayList<>();
        try {
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = String.format("%s%s?clientId=%s&participantId=%s",this.getBaseUrl(), resourceAllPathUser, clientId, participantId);
            StringBuilder builder = new StringBuilder(requestUrl);
            ResponseEntity<ListProgramPathsResponseDto> responseEntity = restTemplate.exchange(builder.toString(), HttpMethod.GET, entity, new ParameterizedTypeReference<ListProgramPathsResponseDto>(){});
            ListProgramPathsResponseDto responseDto =  CompletableFuture.completedFuture(responseEntity.getBody()).get();
            res.setHttpStatus(HttpStatus.OK);
            res.setResult(responseDto);
            return res;
        } catch(HttpStatusCodeException e) {
            errors.add(e.getResponseBodyAsString());
            res.setErrors(errors);
            res.setHttpStatus(e.getStatusCode());
            return res;
        } catch (Exception e) {
            errors.add(e.getMessage() != null ? e.getMessage() : "ERR_INTERNAL_SERVER");
            res.setErrors(errors);
            res.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            return res;
        }
    }
}
