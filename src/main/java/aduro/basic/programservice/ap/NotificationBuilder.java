package aduro.basic.programservice.ap;

import aduro.basic.programservice.domain.NotificationCenter;
import aduro.basic.programservice.domain.enumeration.NotificationReceiverName;
import aduro.basic.programservice.domain.enumeration.NotificationType;
import aduro.basic.programservice.service.dto.IncentiveStatusEnum;

import java.time.Instant;

public class NotificationBuilder  {

    public static  NotificationCenter notificationWellmetricBuilder(String participantId, String eventId) {
        NotificationCenter notificationCenter = new NotificationCenter();
        notificationCenter.setParticipantId(participantId);
        notificationCenter.setCreatedAt(Instant.now());
        notificationCenter.setIsReady(false);
        notificationCenter.setContent("Your latest screening results are available");
        notificationCenter.setTitle("Screening results!");
        notificationCenter.setNotificationType(NotificationType.WELLMETRIC_SCREENING_RESULT);
        notificationCenter.setExecuteStatus(IncentiveStatusEnum.New.name());
        notificationCenter.setEventId(eventId);
        notificationCenter.setReceiverName(NotificationReceiverName.MONARCH.name());
        notificationCenter.setAttemptCount(0);
        notificationCenter.setLastAttemptTime(Instant.now());
        return notificationCenter;
    }


    public static  NotificationCenter notificationLevelUp(String participantId, Instant date, Integer levelUp, String eventId) {
        NotificationCenter notificationCenter = new NotificationCenter();
        notificationCenter.setParticipantId(participantId);
        notificationCenter.setCreatedAt(Instant.now());
        notificationCenter.setIsReady(false);
        notificationCenter.setContent("You have reached level " + levelUp);
        notificationCenter.setTitle("Screening results With Level!");
        notificationCenter.setNotificationType(NotificationType.INCENTIVE_RESULT);
        notificationCenter.setExecuteStatus(IncentiveStatusEnum.New.name());
        notificationCenter.setEventId(eventId);
        notificationCenter.setReceiverName(NotificationReceiverName.MONARCH.name());
        notificationCenter.setAttemptCount(0);
        notificationCenter.setLastAttemptTime(Instant.now());
        return notificationCenter;
    }

    public static  NotificationCenter notificationRequiredTobaccoUser(String participantId) {
        NotificationCenter notificationCenter = new NotificationCenter();
        notificationCenter.setParticipantId(participantId);
        notificationCenter.setCreatedAt(Instant.now());
        notificationCenter.setIsReady(false);
        notificationCenter.setContent("You need to complete tobacco requirements to level up.");
        notificationCenter.setTitle("Tobacco Requirements");
        notificationCenter.setNotificationType(NotificationType.TOBACCO_USER);
        notificationCenter.setExecuteStatus(IncentiveStatusEnum.New.name());
        notificationCenter.setReceiverName(NotificationReceiverName.MONARCH.name());
        notificationCenter.setAttemptCount(0);
        notificationCenter.setEventId(participantId);
        notificationCenter.setLastAttemptTime(Instant.now());
        return notificationCenter;
    }

}
