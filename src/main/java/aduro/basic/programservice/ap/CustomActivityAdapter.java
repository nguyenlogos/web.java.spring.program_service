package aduro.basic.programservice.ap;

import aduro.basic.programservice.ap.dto.*;
import aduro.basic.programservice.helpers.RequestHeaderBuilder;
import aduro.basic.programservice.service.dto.APIResponse;
import aduro.basic.programservice.service.dto.CloneCustomActivityRequest;
import aduro.basic.programservice.service.dto.CustomActivityMapperResponseDto;
import aduro.basic.programservice.service.dto.ListCustomActivityResponseDto;
import aduro.basic.programservice.service.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.DeserializationFeature;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Service
public class CustomActivityAdapter extends AduroPlatformAdapter {
    private final Logger log = LoggerFactory.getLogger(CustomActivityAdapter.class);

    private String resourceCustomActivity = "/common/cc-activities/custom-activities";
    private String resourceUser = "/common/integration/user/get-user-id";
    private String resourceSearchUser = "/common/integration/user/list";
    private String resourceCustomActivityWithStatus = "/common/cc-activities/custom-activities-status";
    private String resourceUpdateDateCustomActivities = "/common/cc-activities/custom-activities/update-date-activities";
    private String resourceDeleteCustomActivities = "/common/cc-activities/custom-activities/bulk-delete";
    private String resourceCountCustomActivities = "/common/cc-activities/custom-activities/joined-users/count";
    private String resourceCloneCustomActivities = "/common/cc-activities/custom-activities/clone-by-program-id";
    private static final int MAX_LIMIT_ITEMS = 1000;

    private static final String PARAM_USER_EMAIL = "email";
    private static final String PARAM_FIRST_NAME = "firstName";
    private static final String PARAM_LAST_NAME = "lastName";
    private static final String PARAM_SUBGROUP_NAME = "subgroupName";
    private static final String PARAM_SUBGROUP_ID = "subgroupId";
    private static final String PARAM_AS_PROGRAM_TESTER = "asProgramTester";

    @Autowired
    private RestTemplate restTemplate;

    public List<CustomActivityDTO> getCustomActivityByTypeAndProgramId(String type, Long programId) throws Exception {
        List<CustomActivityDTO> customActivityDTOS = new ArrayList<>();
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
          //  String requestUrl = this.getBaseUrl() + resourceActivity + "/custom-activities" + "?type=" + type;
            String requestUrl = String.format("%s%s?type=%s&program_id=%s",this.getBaseUrl(), resourceCustomActivity,type,programId) ;
            ResponseEntity<List<CustomActivityDTO>> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<CustomActivityDTO>>(){});
            customActivityDTOS =  CompletableFuture.completedFuture(responseEntity.getBody()).get();
            return customActivityDTOS;
        } catch(HttpStatusCodeException e){
            return customActivityDTOS;
        }catch (Exception e){
            return customActivityDTOS;
        }

    }

    public List<CustomActivityResultDto> getCustomActivitiesByIds(List<String> ids, MultiValueMap<String, String> queryParams) throws Exception {
        List<CustomActivityResultDto> customActivityDTOS = new ArrayList<>();
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = String.format("%s%s",this.getBaseUrl(), resourceCustomActivity) ;
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl);

            if (queryParams.get("program_id") != null) {
                builder.queryParam("program_id", queryParams.get("program_id").get(0));
            }

            builder.queryParam("limit", queryParams.get("limit") != null ? queryParams.get("limit").get(0) : MAX_LIMIT_ITEMS);

            // build search query param
            if (queryParams.get("search") != null && !queryParams.get("search").get(0).isEmpty()) {
                String[] searchList = queryParams.get("search").get(0).split(";");
                for(String searchItem : searchList) {
                    String[] queryItems = searchItem.split(":");
                    if (queryItems.length > 0) {
                        builder.queryParam(queryItems[0], queryItems[1]);
                    }
                }
            } else if (!CollectionUtils.isEmpty(ids)) {
                builder.queryParam("ids", String.join(",", ids));
            }

            URI uri = builder.build().encode().toUri();
            ResponseEntity<List<CustomActivityResultDto>> responseEntity = restTemplate.exchange(uri, HttpMethod.GET, entity, new ParameterizedTypeReference<List<CustomActivityResultDto>>(){});
            customActivityDTOS =  CompletableFuture.completedFuture(responseEntity.getBody()).get();
            return customActivityDTOS;
        } catch(HttpStatusCodeException e){
            return customActivityDTOS;
        }catch (Exception e){
            return customActivityDTOS;
        }

    }


    public CustomActivityDTO AddCustomActivity(CustomActivityDTO customActivityDto) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<CustomActivityDTO> entity = new HttpEntity<CustomActivityDTO>(customActivityDto, headers);
            ResponseEntity<CustomActivityDTO> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceCustomActivity,entity, CustomActivityDTO.class);
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }

    }

    public CustomActivityResponseDto AddCustomActivityToProgram(CustomActivityDTO customActivityDto) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<CustomActivityDTO> entity = new HttpEntity<>(customActivityDto, headers);
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceCustomActivity,entity, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            CustomActivityResponseDto resultDto =  objectMapper.readValue(responseEntity.getBody(), CustomActivityResponseDto.class);
            return CompletableFuture.completedFuture(resultDto).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        } catch(Exception e){
            throw new Exception(e.getMessage());
        }

    }

    public UserDto parseUserParams(String participantId, Map<String, String> queryParams){
        try {
            if(queryParams == null || !queryParams.containsKey(PARAM_AS_PROGRAM_TESTER)){
                return this.getUser(participantId);
            }else{

                UserDto userDto = UserDto.builder().build();

                boolean asProgramTester = (queryParams.containsKey(PARAM_AS_PROGRAM_TESTER)) && Boolean.parseBoolean(queryParams.get(PARAM_AS_PROGRAM_TESTER));
                userDto.setAsProgramTester(asProgramTester);

                userDto.setEmail(queryParams.get(PARAM_USER_EMAIL));
                userDto.setFirstName(queryParams.get(PARAM_FIRST_NAME));
                userDto.setLastName(queryParams.get(PARAM_LAST_NAME));
                userDto.setSubgroupName(queryParams.get(PARAM_SUBGROUP_NAME));
                userDto.setSubgroupId(queryParams.get(PARAM_SUBGROUP_ID));

                return userDto;
            }
        } catch (Exception e) {
            log.error("parseUserParams", e.getMessage());
        }
        return null;
    }

    public UserDto getUser(String participantId) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = this.getBaseUrl() + resourceUser + "" + "?participantId=" + participantId;
            ResponseEntity<UserDto> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<UserDto>(){});
            if(responseEntity.getStatusCode() != HttpStatus.OK){
                log.error("getUser NotOk", responseEntity.getStatusCode());
                throw new Exception("Participant not found");
            }
            UserDto user =  CompletableFuture.completedFuture(responseEntity.getBody()).get();

            if(user.getUserId() == 0){
                log.error("getUser userid0");
                throw new Exception("Participant not found");
            }
            return user;
        } catch(HttpStatusCodeException e){
            log.error("getUser HttpEx", e.getMessage());
            throw  new Exception(e.getResponseBodyAsString());
        }catch (Exception e){
            log.error("getUser", e.getMessage());
            throw  new Exception(e.getMessage());
        }
    }

    public CustomActivityDTO getCustomActivityDetail(Long customActivityId) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = this.getBaseUrl() + resourceCustomActivity + "/" + customActivityId;
            ResponseEntity<CustomActivityDTO> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<CustomActivityDTO>(){});
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    public APIResponse<ListCustomActivityResponseDto> getCustomActivityByActiveProgram(String clientId,
                                                                                       String participantId,
                                                                                       String subgroupId,
                                                                                       long limit,
                                                                                       long offset,
                                                                                       String searchTitle) throws Exception {

        APIResponse<ListCustomActivityResponseDto> res = new APIResponse<ListCustomActivityResponseDto>();
        List<String> errors = new ArrayList<>();
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = String.format("%s%s?clientId=%s&participantId=%s&limit=%d&offset=%d",this.getBaseUrl(), resourceCustomActivityWithStatus, clientId, participantId, limit, offset);
            StringBuilder builder = new StringBuilder(requestUrl);
            if (!RandomUtil.isNullOrEmpty(searchTitle)) {
                builder.append(String.format("&searchTitle=%s", searchTitle));
            }
            if (!RandomUtil.isNullOrEmpty(subgroupId)) {
                builder.append(String.format("&subgroupId=%s", subgroupId));
            }
            ResponseEntity<ListCustomActivityResponseDto> responseEntity = restTemplate.exchange(builder.toString(), HttpMethod.GET, entity, new ParameterizedTypeReference<ListCustomActivityResponseDto>(){});
            ListCustomActivityResponseDto responseDto =  CompletableFuture.completedFuture(responseEntity.getBody()).get();
            res.setHttpStatus(HttpStatus.OK);
            res.setResult(responseDto);
            return res;
        } catch(HttpStatusCodeException e){
            errors.add(e.getResponseBodyAsString());
            res.setErrors(errors);
            res.setHttpStatus(e.getStatusCode());
            return res;
        }catch (Exception e){
            errors.add(e.getMessage() != null ? e.getMessage() : "ERR_INTERNAL_SERVER");
            res.setErrors(errors);
            res.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            return res;
        }
    }

    public CustomActivityPhaseResultDto updateDateCustomActivities(List<CustomActivityPhaseDto> customActivityPhaseDtoList) throws Exception {
        try{
            ResponseEntity<CustomActivityPhaseResultDto> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceUpdateDateCustomActivities, customActivityPhaseDtoList, CustomActivityPhaseResultDto.class);
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }  catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public Object deleteCustomActivitiesInProgram(DeleteCustomActivityDto customActivityDto) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<DeleteCustomActivityDto> entity = new HttpEntity<>(customActivityDto, headers);
            String requestUrl = String.format("%s%s", this.getBaseUrl(), resourceDeleteCustomActivities, entity);
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(requestUrl ,entity, String.class);
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public CountCustomActivityResultDto countUsersJoinedCustomActivity(CountCustomActivityDto customActivityDto) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<CountCustomActivityDto> entity = new HttpEntity<>(customActivityDto, headers);
            ResponseEntity<CountCustomActivityResultDto> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceCountCustomActivities,entity, CountCustomActivityResultDto.class);
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }

    }

    public boolean invalidDeleteUserJoinedCustomActivity(List<String> customActivitiesIds) {
        boolean invalid = false;
        try {
            // check custom activities do not have any joined users
            CountCustomActivityDto countCustomActivityDto = new CountCustomActivityDto();
            countCustomActivityDto.setCustomActivityIds(customActivitiesIds);
            CountCustomActivityResultDto countCustomActivityResultDto = countUsersJoinedCustomActivity(countCustomActivityDto);
            invalid = countCustomActivityResultDto != null && countCustomActivityResultDto.getTotal() > 0;
        } catch (Exception e) {
            log.error("Request to isValidDeleteUserJoinedCustomActivity : {}", e.getMessage());
        }
        return invalid;
    }

    public CustomActivityMapperResponseDto cloneProgramPhases(CloneCustomActivityRequest request) throws Exception {
        try{
            ResponseEntity<CustomActivityMapperResponseDto> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceCloneCustomActivities, request, CustomActivityMapperResponseDto.class);
            if (!responseEntity.getStatusCode().equals(HttpStatus.OK) ){
                throw new Exception("error request clone activities");
            }
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }  catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public SearchUserResponse searchUsers(SearchUserRequest searchUserRequest) throws Exception {
        try{
            ResponseEntity<SearchUserResponse> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceSearchUser,searchUserRequest, SearchUserResponse.class);
            return CompletableFuture.completedFuture(responseEntity.getBody()).get();
        } catch(HttpStatusCodeException e){
            log.error("searchUsers HttpEx", e.getMessage());
            throw new Exception(e.getResponseBodyAsString());
        } catch (Exception e){
            log.error("searchUsers", e.getMessage());
            throw  new Exception(e.getMessage());
         }
    }
}
