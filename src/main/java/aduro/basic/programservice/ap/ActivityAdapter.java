package aduro.basic.programservice.ap;

import aduro.basic.programservice.ap.dto.ActivityDTO;
import aduro.basic.programservice.ap.dto.NotificationItemRequest;
import aduro.basic.programservice.ap.dto.NotificationItemResponse;
import aduro.basic.programservice.helpers.RequestHeaderBuilder;
import aduro.basic.programservice.tangocard.dto.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class ActivityAdapter extends AduroPlatformAdapter {

    private String resourceActivity = "/common/cc-activities";

    @Autowired
    private RestTemplate restTemplate;

    public ActivityDTO getActivityByTypeAndCode(String type, String activityCode) throws Exception {
        try{
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = this.getBaseUrl() + resourceActivity + "" + "?type=" + type;
            ResponseEntity<List<ActivityDTO>> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<ActivityDTO>>(){});
            List<ActivityDTO> activities =  CompletableFuture.completedFuture(responseEntity.getBody()).get();
            for (ActivityDTO activity:activities) {
                if(activity.getActivityCode().equals(activityCode))
                    return activity;
            }
            return null;
        } catch(HttpStatusCodeException e){
            return null;
        }catch (Exception e){
            return null;
        }

    }



}
