package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramMemberConsentDetail;
import aduro.basic.programservice.service.dto.ProgramMemberConsentDetailDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ProgramMemberConsentDetail entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramMemberConsentDetailRepository extends JpaRepository<ProgramMemberConsentDetail, Long>, JpaSpecificationExecutor<ProgramMemberConsentDetail> {
    List<ProgramMemberConsentDetail> findByConsentIdAndProgramId(Long consentId, Long programId);
    List<ProgramMemberConsentDetail> findByParticipantIdAndClientIdAndConsentIdAndProgramIdOrderByCreatedDateDesc(String participantId, String clientId, Long consentId, Long programId);
    List<ProgramMemberConsentDetail> findByParticipantIdAndClientIdAndConsentIdOrderByCreatedDateDesc(String participantId, String clientId, Long consentId);
}
