package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ClientSetting;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the ClientSetting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientSettingRepository extends JpaRepository<ClientSetting, Long>, JpaSpecificationExecutor<ClientSetting> {
    ClientSetting findClientSettingByClientId(String clientId);
}
