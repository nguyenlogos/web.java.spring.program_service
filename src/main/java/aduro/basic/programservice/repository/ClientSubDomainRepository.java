package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ClientSubDomain;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ClientSubDomain entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientSubDomainRepository extends JpaRepository<ClientSubDomain, Long>, JpaSpecificationExecutor<ClientSubDomain> {

}
