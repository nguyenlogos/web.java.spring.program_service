package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramRequirementItems;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramRequirementItems entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramRequirementItemsRepository extends JpaRepository<ProgramRequirementItems, Long>, JpaSpecificationExecutor<ProgramRequirementItems> {

    List<ProgramRequirementItems> findByProgramLevelId(Long programLevelId);
}
