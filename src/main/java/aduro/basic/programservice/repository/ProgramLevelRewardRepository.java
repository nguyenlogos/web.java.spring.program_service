package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.domain.ProgramLevelReward;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramLevelReward entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramLevelRewardRepository extends JpaRepository<ProgramLevelReward, Long>, JpaSpecificationExecutor<ProgramLevelReward> {

    List<ProgramLevelReward> findProgramLevelRewardsByProgramLevelId(long programLevelId);

    List<ProgramLevelReward> findProgramLevelRewardsByProgramLevelProgramId(Long programId);

    void deleteProgramLevelRewardsByProgramLevelProgramIdAndSubgroupIdNotNullAndSubgroupIdIsNotLike(Long programId, String subgroupFilter);
}
