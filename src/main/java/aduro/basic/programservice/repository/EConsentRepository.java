package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.EConsent;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the EConsent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EConsentRepository extends JpaRepository<EConsent, Long>, JpaSpecificationExecutor<EConsent> {
    List<EConsent> findByProgramIdAndParticipantId(long programId, String participantId);
    long countByProgramIdAndParticipantId(long programId, String participantId);
}
