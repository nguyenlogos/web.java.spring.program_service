package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramActivity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramActivityRepository extends JpaRepository<ProgramActivity, Long>, JpaSpecificationExecutor<ProgramActivity> {

    List<ProgramActivity> findProgramActivitiesByProgramId(long programId);

    List<ProgramActivity> findProgramActivitiesByProgramIdAndProgramPhaseIdIsNotNull(long programId);

    Page<ProgramActivity> findProgramActivitiesByProgramIdAndActivityIdIsIn(long programId, List<String> ids, Pageable pageable);

    Page<ProgramActivity> findProgramActivitiesByProgramIdAndProgramPhaseIdAndActivityIdIsIn(long programId, long phaseId, List<String> ids, Pageable pageable);

    List<ProgramActivity> findProgramActivitiesByProgramIdAndBonusPointsEnabledIsTrue(long programId);

    List<ProgramActivity> findProgramActivitiesByProgramPhaseId(long programPhaseId);

    ProgramActivity findByProgramIdAndActivityId(long programId, String activityId);

    ProgramActivity findByProgramIdAndActivityIdAndBonusPointsEnabledIsTrue(long programId, String activityId);

    void deleteProgramActivityByActivityIdAndProgramId(String activityId, Long programId);

    @Query("select p.activityId from ProgramActivity p where p.programId = :programId")
    List<String> findProgramActivityIdsByProgramId(@Param("programId") Long programId);

    List<ProgramActivity> findByIdIn(List<Long> ids);

    Page<ProgramActivity> findProgramActivitiesByProgramIdAndProgramPhaseId(long programId, long programPhaseId, Pageable pageable);

    Page<ProgramActivity> findProgramActivitiesByProgramId(long programId, Pageable pageable);
}
