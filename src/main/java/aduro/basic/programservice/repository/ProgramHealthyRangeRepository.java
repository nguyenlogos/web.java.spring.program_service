package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramHealthyRange;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProgramHealthyRange entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramHealthyRangeRepository extends JpaRepository<ProgramHealthyRange, Long>, JpaSpecificationExecutor<ProgramHealthyRange> {

}
