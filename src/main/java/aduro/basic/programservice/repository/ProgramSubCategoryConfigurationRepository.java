package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ProgramSubCategoryConfiguration entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramSubCategoryConfigurationRepository extends JpaRepository<ProgramSubCategoryConfiguration, Long>, JpaSpecificationExecutor<ProgramSubCategoryConfiguration> {

    List<ProgramSubCategoryConfiguration> findAllByProgramId(Long programId);

}
