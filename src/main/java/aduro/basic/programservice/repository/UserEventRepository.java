package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.UserEvent;
import io.vavr.collection.Seq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the UserEvent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserEventRepository extends JpaRepository<UserEvent, Long>, JpaSpecificationExecutor<UserEvent> {

    long countAllByEventCodeAndProgramUserId(String eventCode, Long programUserId);

   // long countAllByEventIdIn(ArrayList<String> eventIds);

    long countAllByEventIdInAndProgramUserId(ArrayList<String> eventIds, Long programUserId);

    Integer countAllByEventCodeAndProgramUserProgramIdAndProgramUserParticipantId(String subCategoryCode, Long programId, String participantId);

    List<UserEvent> findUserEventsByProgramUserIdAndEventIdIn(Long programUserId, List<String> eventIds);

    List<UserEvent> findUserEventsByProgramUserIdAndEventCategoryInAndEventIdIn(Long programUserId, List<String> eventCategory, List<String> eventIds);
    List<UserEvent> findUserEventsByProgramUserIdAndEventCategoryAndEventIdIn(Long programUserId, String eventCategory, List<String> eventIds);

    Optional<UserEvent> getUserEventByProgramUserAndEventIdAndEventCode(ProgramUser programUserId, String eventId, String eventCode);

    Integer countAllByProgramUserAndEventIdAndEventCode(ProgramUser programUserId, String eventId, String eventCode);

    Seq<UserEvent> findUserEventsByProgramUserIdAndEventCategory(Long programUserId, String eventCategory);

    Seq<UserEvent> findUserEventsByProgramUserIdAndEventId(Long programUserId, String eventId);

    List<UserEvent> findByProgramUserIdOrderByEventDateDesc(Long programUserId);

    List<UserEvent> findUserEventsByProgramUserIdAndEventIdAndEventCategory(Long programUserId, String eventId, String eventCategory);

    Seq<UserEvent> findByProgramUserIdAndEventIdOrderByEventDateAsc(Long programUserId, String eventId);

    List<UserEvent> findByProgramUserIdAndEventIdAndEventDateBetween(Long programUserId, String eventId, Instant startDate, Instant endDate);
    List<UserEvent> findByProgramUserIdAndEventIdAndEventCodeAndEventDateBetween(Long programUserId, String eventId, String eventCode, Instant startDate, Instant endDate);

    @Query("select ue from UserEvent ue where ue.programUser.id = :programUserId")
    List<UserEvent> findAllByProgramUserId(@Param("programUserId") Long programUserId);
}
