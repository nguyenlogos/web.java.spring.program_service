package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.OrderTransaction;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the OrderTransaction entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderTransactionRepository extends JpaRepository<OrderTransaction, Long>, JpaSpecificationExecutor<OrderTransaction> {

}
