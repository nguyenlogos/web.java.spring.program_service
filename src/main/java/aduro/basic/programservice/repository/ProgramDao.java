package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.*;

import java.util.List;

public interface ProgramDao {
    ProgramUser getActiveProgramUserByClientIdAndParticipantIdAndStatus(String clientId, String participantId, String status);
    List<ProgramLevelActivity> getRequiredActivitiesByProgramIdAndSubgroupId(Long programId, Integer levelOrder, String subgroupId);
    List<ProgramLevelPath> getRequiredPathsByProgramIdAndSubgroupId(Long programId, Integer levelOrder, String subgroupId);
    List<ProgramRequirementItems> getProgramRequirementItemsRasByProgramIdAndSubgroupId(Long programId, Integer levelOrder, String subgroupId);
    List<ProgramActivity> getProgramActivitiesByProgramId(Long programId);
    List<String> getParticipantsNeedCalculateWmPoint(int page, int pageSize);
}
