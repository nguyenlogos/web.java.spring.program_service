package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ClientProgram;
import io.vavr.collection.Seq;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ClientProgram entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientProgramRepository extends JpaRepository<ClientProgram, Long>, JpaSpecificationExecutor<ClientProgram> {

    List<ClientProgram> findByClientId(String clientId);

    List<ClientProgram> findByClientIdAndProgramStatusIn(String clientId, ArrayList<String> status);
    List<ClientProgram> findByClientIdInAndProgramStatusAndProgramQaVerify(List<String> clientId, String status, boolean qaVerify);
    Optional<ClientProgram> findByClientIdAndProgramQaVerifyAndProgramStatusIn(String clientId, boolean isQAVerify, ArrayList<String> status);
    List<ClientProgram> findByClientIdAndProgramStatus(String clientId, String status);

    Optional<ClientProgram> findByClientIdAndProgramStatusAndProgramQaVerify(String clientId, String status, boolean qaVerify);

    List<ClientProgram> findClientProgramsByProgramId(Long programId);

    List<ClientProgram> findClientProgramsByClientIdAndProgramStatusIn(String clientId, ArrayList<String> status);



    @Query("select clientProgram from ClientProgram clientProgram left join fetch clientProgram.program p " +
           " where clientProgram.clientId = :clientId AND p.isTemplate <> 1 AND (p.status = 'Draft' Or p.status = 'Active') " +
             " AND ((:startDate >= p.startDate AND :startDate < p.resetDate) Or (:resetDate > p.startDate And :resetDate <= p.resetDate) Or (:startDate < p.startDate And :resetDate > p.resetDate))")
    List<ClientProgram> findClientProgramByDateRange(@Param("clientId") String clientId, @Param("startDate") Instant startDate, @Param("resetDate") Instant resetDate);

    Seq<ClientProgram> findDistinctByClientIdInAndProgramStatus(List<String> clientIds, String status);

    @Query("select clientProgram from ClientProgram clientProgram left join fetch clientProgram.program p " +
              " where clientProgram.clientId = :clientId AND p.isTemplate <> 1 AND (p.status = 'Draft' Or (p.status = 'Active' AND p.qaVerify = 1)) " +
              " AND ((:programQAStartDate >= p.programQAStartDate AND :programQAStartDate <= p.programQAEndDate) Or (:programQAEndDate > p.programQAStartDate AND :programQAEndDate <= p.programQAEndDate) Or (:programQAStartDate < p.programQAStartDate AND :programQAEndDate > p.programQAEndDate))")
    List<ClientProgram> findClientProgramByQADateRange(@Param("clientId") String clientId, @Param("programQAStartDate") Instant programQAStartDate, @Param("programQAEndDate") Instant programQAEndDate);

    List<ClientProgram> findClientProgramsByClientIdAndProgramStatusAndProgramQaVerify(String clientId, String status, boolean qaVerify);

    @Query("select clientProgram from ClientProgram clientProgram inner join fetch clientProgram.program p " +
        " where clientProgram.clientId = :clientId " +
        " order by p.resetDate desc"
    )
    List<ClientProgram> findLastResetDateByClientId(@Param("clientId") String clientId, Pageable pageable);
}
