package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.JobExecutionTracking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobExecutionTrackingRepository extends JpaRepository<JobExecutionTracking, Long> {

}
