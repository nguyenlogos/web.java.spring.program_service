package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ClientProgramRewardSetting;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the ClientProgramRewardSetting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientProgramRewardSettingRepository extends JpaRepository<ClientProgramRewardSetting, Long>, JpaSpecificationExecutor<ClientProgramRewardSetting> {
    Optional<ClientProgramRewardSetting> findClientProgramRewardSettingByClientIdAndProgramId(String clientId, Long programId);

    Optional<ClientProgramRewardSetting> findClientProgramRewardSettingByClientId(String clientId);

}
