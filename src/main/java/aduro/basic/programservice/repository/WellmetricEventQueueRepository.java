package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.WellmetricEventQueue;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the WellmetricEventQueue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WellmetricEventQueueRepository extends JpaRepository<WellmetricEventQueue, Long>, JpaSpecificationExecutor<WellmetricEventQueue> {

}
