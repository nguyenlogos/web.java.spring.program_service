package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.Category;
import aduro.basic.programservice.domain.ProgramCategoryPoint;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.*;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Transactional(rollbackFor = Throwable.class)
public interface CategoryRepository extends JpaRepository<Category, Long>, JpaSpecificationExecutor<Category> {
    Category findByCode(String code);

    /*@Query(value = "select category from Category category left join fetch category.subCategories" + " ORDER BY ?#{#pageable}",
        countQuery = "select count(*) from Category category left join fetch category.subCategories sc" +
            " ORDER BY ?#{#pageable}",
        nativeQuery = true
        )
    Page<Category> findAll(@Nullable Specification<Category> var1, Pageable var2);*/

}
