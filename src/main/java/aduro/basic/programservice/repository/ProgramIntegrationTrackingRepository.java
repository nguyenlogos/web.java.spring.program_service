package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramIntegrationTracking;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProgramIntegrationTracking entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramIntegrationTrackingRepository extends JpaRepository<ProgramIntegrationTracking, Long>, JpaSpecificationExecutor<ProgramIntegrationTracking> {

}
