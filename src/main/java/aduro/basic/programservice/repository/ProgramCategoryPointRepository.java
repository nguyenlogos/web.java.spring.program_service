package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramCategoryPoint;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ProgramCategoryPoint entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramCategoryPointRepository extends JpaRepository<ProgramCategoryPoint, Long>, JpaSpecificationExecutor<ProgramCategoryPoint> {
    ProgramCategoryPoint findByCategoryCodeAndProgram_Id(String categoryCode, long program_Id);

    List<ProgramCategoryPoint> findAllByProgram_Id(long program_Id);

    Optional<ProgramCategoryPoint> findByProgramIdAndCategoryCode(long programId, String categoryCode);
}
