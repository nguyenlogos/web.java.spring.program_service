package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramUser;
import io.vavr.collection.Seq;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ProgramUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramUserRepository extends JpaRepository<ProgramUser, Long>, JpaSpecificationExecutor<ProgramUser> {

    Optional<ProgramUser> findProgramUserByParticipantIdAndProgramId(String participantId, Long programId);


   @Query("select programUser from ProgramUser programUser " +
         " where programUser.participantId =:participantId and programUser.clientId =:clientId and programUser.programId =:programId")
    Optional<ProgramUser> getCurrentUserProgram(@Param("participantId") String participantId, @Param("clientId") String clientId,
                                            @Param("programId") Long programId);


    @Query("select distinct programUser from ProgramUser programUser left join  programUser.userEvents userEvent " +
        " where userEvent.eventId =:eventId and programUser.clientId =:clientId and programUser.programId =:programId")
    List<ProgramUser> getProgramUserWithEventId(@Param("eventId") String eventId, @Param("clientId") String clientId,
                                                 @Param("programId") Long programId);

    @Query("select distinct programUser from ProgramUser programUser" +
        " where programUser.programId =:programId")
    List<ProgramUser> getProgramUsersByProgramId(@Param("programId") Long programId);

    Seq<ProgramUser> findProgramUserByClientIdAndProgramId(String clientId, Long programId);

    Seq<ProgramUser> findProgramUserByParticipantIdAndClientId(String participantId, String clientId);

    long countAllByClientIdAndSubgroupId(String clientId, String subgroupId);

    Seq<ProgramUser> findProgramUserByParticipantId(String participantId);

    List<ProgramUser> findByIdIn(List<Long> programUserIds);


    Optional<ProgramUser> findProgramUserByParticipantIdAndClientIdAndProgramId(String participantId, String clientId, Long programId);

    List<ProgramUser> findProgramUserByClientIdAndProgramId(String clientId, Long programId, Pageable pageable);

    List<ProgramUser> findProgramUserByClientIdAndProgramIdAndParticipantIdIsIn(String clientId, Long programId, List<String> participantIds);

}
