package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.SupportAdditionalUserPointQueue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupportAdditionalUserPointQueueRepository extends JpaRepository<SupportAdditionalUserPointQueue, Long>, JpaSpecificationExecutor<SupportAdditionalUserPointQueue> {
    List<SupportAdditionalUserPointQueue> findSupportAdditionalUserPointQueueByExecuteStatus(String executeStatus);
}
