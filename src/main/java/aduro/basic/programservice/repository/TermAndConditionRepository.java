package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.TermAndCondition;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the TermAndCondition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TermAndConditionRepository extends JpaRepository<TermAndCondition, Long>, JpaSpecificationExecutor<TermAndCondition> {

    List<TermAndCondition> findTermAndConditionsByClientId(String clientId);
    List<TermAndCondition> findTermAndConditionsByClientIdAndSubgroupIdContaining(String clientId, String subgroupId);
    List<TermAndCondition> findTermAndConditionsByClientIdAndSubgroupIdContainingAndIsTerminated(String clientId, String subgroupId, Boolean isTerminated);
    List<TermAndCondition> findTermAndConditionsByClientIdAndSubgroupIdIsNullAndIsTerminated(String clientId, Boolean isTerminated);
    List<TermAndCondition> findTermAndConditionsByClientIdAndIsTerminatedOrderByConsentOrderAsc(String clientId, Boolean isTerminated);
}
