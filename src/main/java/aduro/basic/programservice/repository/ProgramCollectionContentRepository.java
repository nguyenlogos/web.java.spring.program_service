package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramCollection;
import aduro.basic.programservice.domain.ProgramCollectionContent;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramCollectionContent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramCollectionContentRepository extends JpaRepository<ProgramCollectionContent, Long>, JpaSpecificationExecutor<ProgramCollectionContent> {

    List<ProgramCollectionContent> findByProgramCollection(ProgramCollection programCollection);

    @Query("select c from ProgramCollectionContent c " +
        "where c.programCollection.program.id = :programId " +
        "and c.itemId in :itemIds " +
        "and c.contentType = :contentType")
    List<ProgramCollectionContent> findAllByContentTypeAndItemIdIn(@Param("programId") Long programId, @Param("contentType") String contentType, @Param("itemIds") List<String> itemIds);
}
