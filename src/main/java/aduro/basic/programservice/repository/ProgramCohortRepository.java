package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramCohort;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramCohort entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramCohortRepository extends JpaRepository<ProgramCohort, Long>, JpaSpecificationExecutor<ProgramCohort> {

    List<ProgramCohort> findAllByProgramId(Long id);

    List<ProgramCohort> findAllByProgramIdAndIsDefault(Long programId, Boolean isDefault);

    boolean existsByCohortNameAndProgram(String name, Program program);
}
