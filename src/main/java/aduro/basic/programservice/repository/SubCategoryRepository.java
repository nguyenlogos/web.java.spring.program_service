package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.SubCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the SubCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubCategoryRepository extends JpaRepository<SubCategory, Long>, JpaSpecificationExecutor<SubCategory> {
    SubCategory findByCode(String code);

    List<SubCategory> findSubCategoriesByCategoryCode(String categoryCode);
}
