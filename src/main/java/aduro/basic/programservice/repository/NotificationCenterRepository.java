package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.NotificationCenter;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NotificationCenter entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificationCenterRepository extends JpaRepository<NotificationCenter, Long>, JpaSpecificationExecutor<NotificationCenter> {

}
