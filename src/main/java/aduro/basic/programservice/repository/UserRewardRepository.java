package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.domain.UserReward;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the UserReward entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserRewardRepository extends JpaRepository<UserReward, Long>, JpaSpecificationExecutor<UserReward> {
    List<UserReward> findUserRewardsByStatusAndRewardTypeOrderByLevelCompletedDateAsc(String status, String rewardType);

    List<UserReward> findUserRewardsByStatusOrderByLevelCompletedDateAsc(String status);

    List<UserReward> findUserRewardByParticipantIdAndClientIdAndEventId(String participantId, String clientId, String eventId);

    List<UserReward> findByProgramUserIdAndProgramLevel(Long programUserId, Integer programLevel);

    List<UserReward> findByProgramUserIdAndProgramLevelAndRewardType(Long programUserId, Integer programLevel, String rewardType);


}
