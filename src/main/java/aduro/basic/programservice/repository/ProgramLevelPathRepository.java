package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramLevelPath;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ProgramLevelPath entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramLevelPathRepository extends JpaRepository<ProgramLevelPath, Long>, JpaSpecificationExecutor<ProgramLevelPath> {
    List<ProgramLevelPath> findProgramLevelPathsByProgramLevelId(Long programLevelId);

    List<ProgramLevelPath> findProgramLevelPathsByProgramLevelProgramId(Long programId);
}
