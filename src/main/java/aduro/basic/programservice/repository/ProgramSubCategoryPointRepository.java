package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramCategoryPoint;
import aduro.basic.programservice.domain.ProgramSubCategoryPoint;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ProgramSubCategoryPoint entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramSubCategoryPointRepository extends JpaRepository<ProgramSubCategoryPoint, Long>, JpaSpecificationExecutor<ProgramSubCategoryPoint> {
    ProgramSubCategoryPoint findByCodeAndProgramCategoryPointId(String code, Long programCategoryPointId);
    List<ProgramSubCategoryPoint> findProgramSubCategoryPointsByProgramCategoryPointId(Long programCategoryPointId);

    ProgramSubCategoryPoint findProgramSubCategoryPointsByCodeAndProgramCategoryPointProgramId(String code, Long programId);

    void deleteByProgramCategoryPointCategoryCodeAndProgramCategoryPointProgramId(String code, long programId);

    @Query("select sc from ProgramSubCategoryPoint sc left join fetch sc.programCategoryPoint c where c.programId = :programId and c.categoryCode = :categoryCode and sc.code in :subProgramCodes")
    List<ProgramSubCategoryPoint> findProgramSubCategoryPointWithCodeIn(@Param("categoryCode") String categoryCode, @Param("programId") Long programId, @Param("subProgramCodes") List<String> subProgramCodes);

    List<ProgramSubCategoryPoint> findByProgramCategoryPointCategoryCodeAndProgramCategoryPointProgramId(String code, Long programId);

}


