package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramUserLevelProgress;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProgramUserLevelProgress entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramUserLevelProgressRepository extends JpaRepository<ProgramUserLevelProgress, Long>, JpaSpecificationExecutor<ProgramUserLevelProgress> {

}
