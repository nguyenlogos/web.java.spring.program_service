package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramDTO;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ProgramDaoImpl implements ProgramDao {

    @PersistenceContext
    public EntityManager entityManager;

    @Override
    public ProgramUser getActiveProgramUserByClientIdAndParticipantIdAndStatus(String clientId, String participantId, String status) {
        Query qPrograms = entityManager
            .createNativeQuery("select programUser.* from program_user programUser inner join program p " +
                "on programUser.program_id = p.id " +
                "where programUser.client_id =:clientId AND programUser.participant_id=:participantId AND p.status=:status"
                , ProgramUser.class)
            .setParameter("clientId", clientId)
            .setParameter("participantId", participantId)
            .setParameter("status", status)
            .setMaxResults(1);
        try {
            return (ProgramUser) qPrograms.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public List<ProgramLevelActivity> getRequiredActivitiesByProgramIdAndSubgroupId(Long programId, Integer levelOrder, String subgroupId) {
        Query qPrograms = entityManager
            .createNativeQuery("select pl.*, pla.* from program_level pl inner join program_level_activity pla " +
                    "on pl.id = pla.program_level_id " +
                    "where pl.program_id=:programId AND pla.subgroup_id=:subgroupId AND pl.level_order=:levelOrder"
                , ProgramLevelActivity.class)
            .setParameter("programId", programId)
            .setParameter("levelOrder", levelOrder)
            .setParameter("subgroupId", subgroupId);
        return qPrograms.getResultList();
    }

    @Override
    public List<ProgramLevelPath> getRequiredPathsByProgramIdAndSubgroupId(Long programId, Integer levelOrder, String subgroupId) {
        Query qPrograms = entityManager
            .createNativeQuery("select pl.*, pla.* from program_level pl inner join program_level_path pla " +
                    "on pl.id = pla.program_level_id " +
                    "where pl.program_id=:programId AND pla.subgroup_id=:subgroupId AND pl.level_order=:levelOrder"
                , ProgramLevelPath.class)
            .setParameter("programId", programId)
            .setParameter("levelOrder", levelOrder)
            .setParameter("subgroupId", subgroupId);
        return qPrograms.getResultList();
    }

    @Override
    public List<ProgramRequirementItems> getProgramRequirementItemsRasByProgramIdAndSubgroupId(Long programId, Integer levelOrder, String subgroupId) {
        Query qPrograms = entityManager
            .createNativeQuery("select pl.*, pla.* from program_level pl inner join program_requirement_items pla " +
                    "on pl.id = pla.program_level_id " +
                    "where pl.program_id=:programId AND pla.subgroup_id=:subgroupId AND pl.level_order=:levelOrder"
                , ProgramRequirementItems.class)
            .setParameter("programId", programId)
            .setParameter("levelOrder", levelOrder)
            .setParameter("subgroupId", subgroupId);
        return qPrograms.getResultList();
    }

    @Override
    public List<ProgramActivity> getProgramActivitiesByProgramId(Long programId) {

        Query qPrograms = entityManager
            .createNativeQuery("select pa.* from program_activity pa where pa.program_id=:programId"
                , ProgramActivity.class)
            .setParameter("programId", programId);
        return qPrograms.getResultList();
    }

    @Override
    public List<String> getParticipantsNeedCalculateWmPoint(int page, int pageSize) {
        Query qWellmetricEventQueue = entityManager
            .createNativeQuery("select distinct weq.participant_id from wellmetric_event_queue weq where weq.execute_status='New' limit :limit offset :offset")
            .setParameter("limit", pageSize)
            .setParameter("offset", pageSize*page);
        List<Object> lst = qWellmetricEventQueue.getResultList();
        return lst.stream().map(p -> (String)p).collect(Collectors.toList());
    }
}
