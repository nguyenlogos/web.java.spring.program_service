package aduro.basic.programservice.repository.extensions;

import aduro.basic.programservice.domain.WellmetricEventQueue;
import aduro.basic.programservice.domain.enumeration.ResultStatus;
import aduro.basic.programservice.repository.WellmetricEventQueueRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public interface WellmetricEventQueueRepositoryExtension extends WellmetricEventQueueRepository {

    List<WellmetricEventQueue> findAllByResultAndExecuteStatus(ResultStatus resultStatus, String status);
    List<WellmetricEventQueue> findAllByExecuteStatusAndParticipantIdIn(String status, List<String> participantIds);
    List<WellmetricEventQueue> findAllByParticipantIdAndEventIdAndIsWaitingPush(String participant, String eventId, Boolean isWaitingPush);
    List<WellmetricEventQueue> findAllByParticipantIdAndClientIdAndAttemptedAtAfterAndAttemptedAtBefore(String participant, String clientId, Instant from, Instant to);
    Optional<WellmetricEventQueue> findByParticipantIdAndClientIdAndEventIdAndResultAndEventCodeAndProgramId(String participantId, String clientId, String eventId, ResultStatus status, String eventCode, Long programId);
    List<WellmetricEventQueue> findAllByParticipantIdAndSubCategoryCodeAndResult(String participant, String subCategoryCode, ResultStatus resultStatus);
    List<WellmetricEventQueue> findAllByParticipantIdAndEventId(String participant, String eventId);

    @Modifying
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Query(value = "UPDATE WellmetricEventQueue st SET st.executeStatus=:status WHERE st.id in :ids")
    int updateStatusRunning(@Param("status")String status, @Param("ids")List<Long> ids);


}
