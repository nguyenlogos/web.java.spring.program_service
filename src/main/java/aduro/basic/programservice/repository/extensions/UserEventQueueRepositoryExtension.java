package aduro.basic.programservice.repository.extensions;

import aduro.basic.programservice.domain.UserEventQueue;
import aduro.basic.programservice.repository.UserEventQueueRepository;
import io.vavr.collection.Seq;
import org.springframework.stereotype.Repository;

@Repository
public interface UserEventQueueRepositoryExtension extends UserEventQueueRepository {

    Seq<UserEventQueue> findByProgramUserIdAndProgramIdAndEventCategory(Long programUserId, Long programId, String categoryEvent);
}
