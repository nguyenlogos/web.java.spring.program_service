package aduro.basic.programservice.repository.extensions;

import aduro.basic.programservice.domain.ClientSubDomain;
import aduro.basic.programservice.repository.ClientSubDomainRepository;
import io.vavr.control.Option;
import org.springframework.stereotype.Repository;


@Repository
public interface ClientSubDomainRepositoryExtension extends ClientSubDomainRepository {
    Option<ClientSubDomain> findClientSubDomainBySubDomainUrl(String subDomainUrl);
    Option<ClientSubDomain> findClientSubDomainBySubDomainUrlAndClientId(String subDomainUrl, String clientId);
}
