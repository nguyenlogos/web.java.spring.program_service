package aduro.basic.programservice.repository.extensions;

import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.repository.ProgramHealthyRangeRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
@Primary
public interface ProgramHealthyRangeRepositoryExtension extends ProgramHealthyRangeRepository {
    Optional<ProgramHealthyRange> findByProgramIdAndAndBiometricCode(Long id, String biometricCode);
    Set<ProgramHealthyRange> findByProgramId(Long id);
    void deleteAllByIdIn(List<Long> ranges);

    List<ProgramHealthyRange> findByProgramIdAndBiometricCodeIn(Long id, List<String> biometricCodes);
    List<ProgramHealthyRange> findByProgramIdAndBiometricCodeNotIn(Long id, List<String> biometricCodes);
}
