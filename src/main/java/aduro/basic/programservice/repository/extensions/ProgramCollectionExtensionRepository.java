package aduro.basic.programservice.repository.extensions;

import aduro.basic.programservice.domain.ProgramCollection;
import aduro.basic.programservice.repository.ProgramCollectionRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramCollection entity.
 */
@SuppressWarnings("unused")
@Repository
@Primary
public interface ProgramCollectionExtensionRepository extends ProgramCollectionRepository {

    List<ProgramCollection> findAllByProgramId(Long programId);
}
