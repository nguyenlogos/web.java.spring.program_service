package aduro.basic.programservice.repository.extensions;

import aduro.basic.programservice.domain.ProgramUserLevelProgress;
import aduro.basic.programservice.domain.UserEventQueue;
import aduro.basic.programservice.repository.ProgramUserLevelProgressRepository;
import aduro.basic.programservice.repository.UserEventQueueRepository;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProgramUserLevelProgressRepositoryExtension extends ProgramUserLevelProgressRepository {

    List<ProgramUserLevelProgress> findByProgramUserIdOrderByCreatedAtAsc(Long programUserId);

    List<ProgramUserLevelProgress> findByProgramUserIdAndLevelFrom(Long programUserId, Integer levelUp);
}
