package aduro.basic.programservice.repository.extensions;

import aduro.basic.programservice.domain.NotificationCenter;
import aduro.basic.programservice.repository.NotificationCenterRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationCenterRepositoryExtension extends NotificationCenterRepository {

    List<NotificationCenter> findAllByExecuteStatus(String status);
}
