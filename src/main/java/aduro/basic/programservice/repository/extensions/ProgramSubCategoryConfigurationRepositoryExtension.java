package aduro.basic.programservice.repository.extensions;

import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import aduro.basic.programservice.repository.ProgramSubCategoryConfigurationRepository;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Primary
public interface ProgramSubCategoryConfigurationRepositoryExtension extends ProgramSubCategoryConfigurationRepository {
    Optional<ProgramSubCategoryConfiguration> findByProgramIdAndSubCategoryCode(long programId, String subcategoryCode);
    List<ProgramSubCategoryConfiguration> findByProgramId(long programId);
}
