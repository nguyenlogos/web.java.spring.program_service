package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.domain.ProgramCohortRule;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramCohortRule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramCohortRuleRepository extends JpaRepository<ProgramCohortRule, Long>, JpaSpecificationExecutor<ProgramCohortRule> {

    List<ProgramCohortRule> findAllByProgramCohort(ProgramCohort programCohort);

    void deleteAllByProgramCohort(ProgramCohort programCohort);
}
