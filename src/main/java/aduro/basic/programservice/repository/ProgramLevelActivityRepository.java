package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramLevelActivity;
import aduro.basic.programservice.domain.ProgramLevelReward;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramLevelActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramLevelActivityRepository extends JpaRepository<ProgramLevelActivity, Long>, JpaSpecificationExecutor<ProgramLevelActivity> {
    List<ProgramLevelActivity> findProgramLevelActivitiesByProgramLevelId(long programLevelId);
    void deleteProgramLevelActivitiesByActivityIdAndProgramLevelIdIn(String activityId, List<Long> levelIds);
    List<ProgramLevelActivity> findProgramLevelActivitiesByProgramLevelProgramId(long programId);
    void deleteProgramLevelActivitiesByProgramLevelProgramIdAndSubgroupIdIsNotNullAndSubgroupIdNotLike(Long programId, String subgroupFilter);
}
