package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramUserLogs;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProgramUserLogs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramUserLogsRepository extends JpaRepository<ProgramUserLogs, Long> {

}
