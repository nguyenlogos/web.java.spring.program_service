package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.Program;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Program entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramRepository extends JpaRepository<Program, Long>, JpaSpecificationExecutor<Program> {

    /*
    @Query("select program from Program program left join fetch program.programCategoryPoints cp " +
        " left join fetch cp.programSubCategoryPoints " +
        " left join fetch program.programLevels l " +
        " left join fetch l.programLevelActivities " +
        " left join fetch l.programLevelRewards " +
        " left join fetch program.programActivities "+
        " left join fetch program.programSubgroups " +
        " where program.id =:id ")
    Optional<Program> findOneWithEagerRelationships(@Param("id") Long id);
     */
    @Query("select program from Program program where program.id =:id ")
    Optional<Program> findOneWithEagerRelationships(@Param("id") Long id);


    @Query("update Program p set p.lastModifiedDate =:lastModifiedDate where p.id =:programId")
    void updateModified(@Param("lastModifiedDate") Integer lastModifiedDate,@Param("programId") long programId );

    Boolean existsProgramByName(String name);

    List<Program> findProgramsByStartDateBeforeAndStatusInAndIsTemplate(Instant currentDate, List<String> status, Boolean isTemplate);

    List<Program> findProgramsByProgramQAStartDateBeforeAndStatusInAndIsTemplate(Instant currentDate, List<String> status, Boolean isTemplate);
    List<Program> findProgramsByProgramQAEndDateBeforeAndStatusAndQaVerifyAndIsTemplate(Instant currentDate, String status, boolean qaVerify, Boolean isTemplate);

    List<Program> findProgramsByResetDateBeforeAndStatusAndIsTemplate(Instant currentDate, String status, Boolean isTemplate);

    List<Program> findByIdIn(List<Long> programIds);

    // use for template -> be carefully when use this.
    @Query("select program from Program program " +
        "left join fetch program.programCategoryPoints cp " +
        " left join fetch cp.programSubCategoryPoints " +
        " left join fetch program.programLevels l " +
        " left join fetch l.programLevelActivities " +
        " left join fetch l.programLevelRewards " +
        " left join fetch program.programActivities "+
        " left join fetch program.programSubgroups " +
        " left join fetch program.programCollections " +
        " left join fetch program.programCohorts " +
        " where program.id =:id ")
    Optional<Program> findOneWithEagerRelationshipsForProgram(@Param("id") Long id);

    @Query("select program from Program program left join fetch program.programCategoryPoints cp " +
        " left join fetch cp.programSubCategoryPoints " +
        " left join fetch program.programLevels l " +
        " where program.id =:id ")
    Optional<Program> findProgramWithConfigurationPoints(@Param("id") Long id);
}
