package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramLevelPractice;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProgramLevelPractice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramLevelPracticeRepository extends JpaRepository<ProgramLevelPractice, Long>, JpaSpecificationExecutor<ProgramLevelPractice> {

}
