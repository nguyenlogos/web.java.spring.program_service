package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramCollection;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramCollection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramCollectionRepository extends JpaRepository<ProgramCollection, Long>, JpaSpecificationExecutor<ProgramCollection> {

    List<ProgramCollection> getAllByProgramId(Long id);
}
