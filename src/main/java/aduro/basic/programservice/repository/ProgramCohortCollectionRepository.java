package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.domain.ProgramCohortCollection;
import aduro.basic.programservice.domain.ProgramCollection;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramCohortCollection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramCohortCollectionRepository extends JpaRepository<ProgramCohortCollection, Long>, JpaSpecificationExecutor<ProgramCohortCollection> {

    List<ProgramCohortCollection> findAllByProgramCohort(ProgramCohort programCohort);
    List<ProgramCohortCollection> findAllByProgramCollection(ProgramCollection programCollection);

    List<ProgramCohortCollection> findAllByProgramCollectionId(Long programCollectionId);

    void deleteByProgramCollectionId(Long programCollectionId);
}
