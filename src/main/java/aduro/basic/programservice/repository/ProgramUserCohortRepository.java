package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.ProgramUserCohort;
import aduro.basic.programservice.domain.enumeration.CohortStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ProgramUserCohort entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramUserCohortRepository extends JpaRepository<ProgramUserCohort, Long>, JpaSpecificationExecutor<ProgramUserCohort> {

    List<ProgramUserCohort> getByProgramUserId(Long userId);

    Optional<ProgramUserCohort> findByProgramCohortAndProgramUser(ProgramCohort programCohort, ProgramUser programUser);

   long countByProgramCohortIdIn(List<Long> ids);

   @Query("select puc from ProgramUserCohort puc left join fetch puc.programCohort pc where puc.programUser.id = :programUser and puc.status not in :listStatus")
    List<ProgramUserCohort> getProgramUserInProgramCohort(@Param("programUser") Long programUser, @Param("listStatus") List<CohortStatus> listStatus);

    List<ProgramUserCohort> findProgramUserCohortByProgramUserIdAndStatusIsNotIn(Long userId, List<CohortStatus> status);
    List<ProgramUserCohort> findProgramUserCohortByProgramUserIdAndStatusIsIn(Long userId, List<CohortStatus> status);
}
