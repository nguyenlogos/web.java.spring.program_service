package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramLevel;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


/**
 * Spring Data  repository for the ProgramLevel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramLevelRepository extends JpaRepository<ProgramLevel, Long>, JpaSpecificationExecutor<ProgramLevel> {
    List<ProgramLevel> findProgramLevelsByProgram_Id(long program);

    @Query("select pl from ProgramLevel pl where pl.programId = :programId and  pl.levelOrder = :currentLevel")
    Optional<ProgramLevel> findAllByProgramIdAndCurrentLevel(@Param("programId") Long programId, @Param("currentLevel") Integer currentLevel);
}
