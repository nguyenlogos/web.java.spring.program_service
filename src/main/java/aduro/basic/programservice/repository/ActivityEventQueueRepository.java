package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ActivityEventQueue;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ActivityEventQueue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ActivityEventQueueRepository extends JpaRepository<ActivityEventQueue, Long>, JpaSpecificationExecutor<ActivityEventQueue> {

    ActivityEventQueue findFirstByExecuteStatusOrderByCreatedDateAsc(String status);

    @Query("select distinct queue.transactionId  from ActivityEventQueue queue  where queue.executeStatus = 'New'")
    List<String> findListTransactionIds();

    List<ActivityEventQueue> findByExecuteStatus(String status, Pageable pageable);

    List<ActivityEventQueue> findActivityEventQueuesByTransactionIdAndExecuteStatus(String transactionId, String status);
}
