package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramPhase;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramPhase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramPhaseRepository extends JpaRepository<ProgramPhase, Long>, JpaSpecificationExecutor<ProgramPhase> {
    List<ProgramPhase> findProgramPhasesByProgramId(Long programId);
}
