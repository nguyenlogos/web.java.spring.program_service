package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramUserCollectionProgress;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the ProgramUserCollectionProgress entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramUserCollectionProgressRepository extends JpaRepository<ProgramUserCollectionProgress, Long>, JpaSpecificationExecutor<ProgramUserCollectionProgress> {

    List<ProgramUserCollectionProgress> getAllByProgramUserCohortId(Long programUserCohortId);

    Optional<ProgramUserCollectionProgress> findByProgramUserCohortIdAndCollectionId(Long programUserCohortId, Long collectionId);

    List<ProgramUserCollectionProgress> findByProgramUserCohortIdAndCollectionIdIn(Long programUserCohortId, List<Long> collectionIds);
}
