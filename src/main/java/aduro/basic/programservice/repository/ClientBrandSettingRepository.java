package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ClientBrandSetting;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the ClientBrandSetting entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ClientBrandSettingRepository extends JpaRepository<ClientBrandSetting, Long>, JpaSpecificationExecutor<ClientBrandSetting> {

    Optional<ClientBrandSetting> findClientBrandSettingByClientId(String clientId);

    Optional<ClientBrandSetting> findClientBrandSettingBySubPathUrl(String subPathUrl);
}
