package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.CronJobLoader;
import aduro.basic.programservice.domain.enumeration.ProcessLevelType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CronJobLoaderRepository extends JpaRepository<CronJobLoader, Long> {

    List<CronJobLoader> findByTypeAndIsForce(ProcessLevelType type, Boolean isForce);

    List<CronJobLoader> findByIsCompletedAndIsForce(Boolean isCompleted, Boolean isForce);

}
