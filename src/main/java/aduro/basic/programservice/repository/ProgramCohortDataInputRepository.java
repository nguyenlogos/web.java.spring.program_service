package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramCohortDataInput;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProgramCohortDataInput entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramCohortDataInputRepository extends JpaRepository<ProgramCohortDataInput, Long>, JpaSpecificationExecutor<ProgramCohortDataInput> {

}
