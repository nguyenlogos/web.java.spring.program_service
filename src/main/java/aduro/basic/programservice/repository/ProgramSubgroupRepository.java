package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramSubgroup;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProgramSubgroup entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramSubgroupRepository extends JpaRepository<ProgramSubgroup, Long>, JpaSpecificationExecutor<ProgramSubgroup> {

    void deleteProgramSubgroupsByProgramId(Long programId);
    void deleteProgramSubgroupsByProgramIdAndSubgroupId(Long programId, String subgroupId);
}
