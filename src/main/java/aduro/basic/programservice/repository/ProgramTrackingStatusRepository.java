package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramTrackingStatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the ProgramTrackingStatus entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramTrackingStatusRepository extends JpaRepository<ProgramTrackingStatus, Long>, JpaSpecificationExecutor<ProgramTrackingStatus> {

    List<ProgramTrackingStatus> findByProgramIdAndProgramStatus(Long id, String status);
}
