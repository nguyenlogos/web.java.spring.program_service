package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProgramBiometricData;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ProgramBiometricData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProgramBiometricDataRepository extends JpaRepository<ProgramBiometricData, Long>, JpaSpecificationExecutor<ProgramBiometricData> {

}
