package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.User;
import aduro.basic.programservice.domain.UserEventQueue;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the UserEventQueue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserEventQueueRepository extends JpaRepository<UserEventQueue, Long>, JpaSpecificationExecutor<UserEventQueue> {
    List<UserEventQueue> findByEventPointIsNotNullAndExecuteStatusAndAttemptCountLessThan(String executeStatus, int attemptCount);
    List<UserEventQueue> findByProgramUserIdAndEventIdAndProgramId(long programUserId, String eventId, long programId);
    List<UserEventQueue> findByProgramUserIdAndEventIdAndProgramIdAndEventCode(long programUserId, String eventId, long programId, String eventCode);
}
