package aduro.basic.programservice.repository;

import aduro.basic.programservice.domain.ProcessLevelParticipantQueue;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProcessLevelParticipantQueueRepository extends JpaRepository<ProcessLevelParticipantQueue, Long> {


    List<ProcessLevelParticipantQueue> findByStatusAndAttemptCountLessThanEqualAndIsTerminatedOrderByCreatedAt( String status, int attemptCount, Boolean isTerminated, Pageable pageable);

    List<ProcessLevelParticipantQueue> findByProgramUserIdAndCurrentLevelAndArchiveLevel(long programUserId, int currentLevel, int archivedLevel);
}
