/*
package aduro.basic.programservice.datasource;


import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories(
    entityManagerFactoryRef = "ampEntityManagerFactory",
    transactionManagerRef = "ampTransactionManager",
    basePackages = "aduro.basic.programservice.amp.repository"
)
@EnableTransactionManagement
public class AmpDbConfig {
    @Bean
    @ConfigurationProperties("amp.datasource")
    public DataSourceProperties ampDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("amp.datasource.hikari")
    public HikariDataSource ampDataSource() {
        return ampDataSourceProperties().initializeDataSourceBuilder()
            .type(HikariDataSource.class)
            .build();
    }

    @Bean(name = "ampEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean ampEntityManagerFactory(
        EntityManagerFactoryBuilder builder) {
        return builder
            .dataSource(ampDataSource())
            .packages("aduro.basic.programservice.amp.domain")
            .persistenceUnit("amp")
            .build();
    }

    @Bean(name = "ampTransactionManager")
    public JpaTransactionManager db2TransactionManager(@Qualifier("ampEntityManagerFactory") final EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }
}
*/
