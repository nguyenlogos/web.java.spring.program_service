package aduro.basic.programservice.datasource;


import aduro.basic.programservice.ProgramserviceApp;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;

@Configuration
@EnableJpaRepositories(
    entityManagerFactoryRef = "adpEntityManagerFactory",
    transactionManagerRef = "adpTransactionManager",
    basePackages = "aduro.basic.programservice.adp.repository"
)
@EnableTransactionManagement
public class AdpDbConfig {
    @Bean
    @ConfigurationProperties("adp.datasource")
    public DataSourceProperties adpDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @ConfigurationProperties("adp.datasource.hikari")
    public HikariDataSource adpDataSource() {
        return adpDataSourceProperties().initializeDataSourceBuilder()
            .type(HikariDataSource.class)
            .build();
    }

    @Bean(name = "adpEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean adpEntityManagerFactory(
        EntityManagerFactoryBuilder builder) {
        return builder
            .dataSource(adpDataSource())
            .packages("aduro.basic.programservice.adp.domain")
            .persistenceUnit("adp")
            .build();
    }

    @Bean(name = "adpTransactionManager")
    public JpaTransactionManager db2TransactionManager(@Qualifier("adpEntityManagerFactory") final EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }
}
