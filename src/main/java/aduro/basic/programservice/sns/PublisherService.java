package aduro.basic.programservice.sns;


import aduro.basic.programservice.domain.enumeration.ProgramSNSEvent;

import java.io.Serializable;

public interface PublisherService {
    void publish(String message, String topic);
     <MESSAGE extends Serializable> void publishWithEvent(ProgramSNSEvent event, MESSAGE message);
    void publishProgramUpdatedEvent(String clientId, Long programId);
}
