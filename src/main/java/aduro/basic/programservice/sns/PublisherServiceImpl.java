package aduro.basic.programservice.sns;

import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.enumeration.ProgramSNSEvent;
import aduro.basic.programservice.domain.enumeration.PublishMessageDto;
import aduro.basic.programservice.repository.ClientProgramRepository;
import aduro.basic.programservice.service.dto.ProgramStatusChangeMessage;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Service
public class PublisherServiceImpl implements PublisherService{

    private final Logger log = LoggerFactory.getLogger(PublisherServiceImpl.class);

    @Autowired
    @Qualifier("snsClientDefault")
    private AmazonSNS amazonSNS;

    @Value("${aws.sns.topics.program-updated-events-topic}")
    private String topicProgramStatusChange;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    @Override
    public void publish(String message, String topic) {
        PublishRequest publishRequest = new PublishRequest(topic, message);
        PublishResult publishResult = amazonSNS.publish(publishRequest);
        log.info("Print MessageId of message published to SNS topic {}", publishResult.getMessageId());
    }

    @Override
    @Async
    public <MESSAGE extends Serializable> void publishWithEvent(ProgramSNSEvent event, MESSAGE message) {
        try {

            PublishMessageDto<MESSAGE> dto = PublishMessageDto.<MESSAGE>builder()
                .eventName(event.toString())
                .data(message)
                .build();

            String mg = objectMapper.writeValueAsString(dto);
            publish(mg, topicProgramStatusChange);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void publishProgramUpdatedEvent(String clientId, Long programId) {
        if (StringUtils.isEmpty(clientId)) {
            List<ClientProgram> clientPrograms = clientProgramRepository.findClientProgramsByProgramId(programId);
            if (CollectionUtils.isEmpty(clientPrograms)) {
                return;
            }
            ClientProgram clientProgram = clientPrograms.get(0);
            clientId = clientProgram.getClientId();
        }

        publishWithEvent(ProgramSNSEvent.PROGRAM_UPDATED,
            ProgramStatusChangeMessage.builder()
                .programId(programId)
                .clientId(clientId)
                .timeStamp(Instant.now())
                .build()
        );
    }
}
