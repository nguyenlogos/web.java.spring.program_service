package aduro.basic.programservice.sns;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SNSConfig {

    @Bean(name = "snsClientDefault")
    AmazonSNS createAmazonSNSClient() {
        return AmazonSNSClientBuilder.standard().build();
    }


}
