package aduro.basic.programservice.cc.dto;

public class ESResponseDto {
    private ESProfileDto data;

    public ESProfileDto getData() {
        return data;
    }

    public void setData(ESProfileDto data) {
        this.data = data;
    }
}
