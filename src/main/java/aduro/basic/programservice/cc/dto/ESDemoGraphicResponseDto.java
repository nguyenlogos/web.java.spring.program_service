package aduro.basic.programservice.cc.dto;

import java.util.List;

public class ESDemoGraphicResponseDto {
    private List<ESDemographicDto> data;

    public List<ESDemographicDto> getData() {
        return data;
    }

    public void setData(List<ESDemographicDto> data) {
        this.data = data;
    }
}
