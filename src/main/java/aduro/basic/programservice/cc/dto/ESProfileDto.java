package aduro.basic.programservice.cc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ESProfileDto {
    private String id;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;
    private String phone;

    @JsonProperty("employee_id")
    private String employeeId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmployeeId() { return this.employeeId; };

    public void setEmployeeId(String employeeId) { this.employeeId = employeeId; }
}
