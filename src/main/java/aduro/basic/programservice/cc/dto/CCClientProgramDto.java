package aduro.basic.programservice.cc.dto;

import aduro.basic.programservice.domain.ClientProgram;

import java.util.List;

public class CCClientProgramDto {

    private String id;

    private String status;

    private List<ClientProgram> clientPrograms;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ClientProgram> getClientPrograms() {
        return clientPrograms;
    }

    public void setClientPrograms(List<ClientProgram> clientPrograms) {
        this.clientPrograms = clientPrograms;
    }

    @Override
    public String toString() {
        return "CCClientProgramDto{" +
            "id='" + id + '\'' +
            ", status='" + status + '\'' +
            ", clientPrograms=" + clientPrograms +
            '}';
    }
}
