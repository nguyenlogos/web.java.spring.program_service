package aduro.basic.programservice.cc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ESDemographicDto {
    private long id;

    @JsonProperty("employer_id")
    private String employerId;

    @JsonProperty("demographic_type")
    private String demographicType;

    @JsonProperty("demographic_value")
    private String demographicValue;

    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmployerId() {
        return employerId;
    }

    public void setEmployerId(String employerId) {
        this.employerId = employerId;
    }

    public String getDemographicType() {
        return demographicType;
    }

    public void setDemographicType(String demographicType) {
        this.demographicType = demographicType;
    }

    public String getDemographicValue() {
        return demographicValue;
    }

    public void setDemographicValue(String demographicValue) {
        this.demographicValue = demographicValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
