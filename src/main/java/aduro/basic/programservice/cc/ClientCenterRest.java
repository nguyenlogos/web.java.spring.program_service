package aduro.basic.programservice.cc;

import aduro.basic.programservice.cc.dto.CCClientProgramDto;
import aduro.basic.programservice.cc.dto.EmailTriggerResponse;
import aduro.basic.programservice.domain.ProgramIntegrationTracking;
import aduro.basic.programservice.helpers.APITracker;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.helpers.GsonParserUtils;
import aduro.basic.programservice.helpers.RequestHeaderBuilder;
import aduro.basic.programservice.repository.ProgramIntegrationTrackingRepository;
import io.swagger.models.auth.In;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

import static aduro.basic.programservice.config.Constants.CLIENT_CENTER;

@Service
@Transactional
public class ClientCenterRest {


    private final String emailTriggerEndPoint = "/email-triggers";

    private final Environment environment;

    private final RestTemplate restTemplate;

    private final ProgramIntegrationTrackingRepository programIntegrationTrackingRepository;

    private final APITracker apiTracker;

    public ClientCenterRest(Environment environment, RestTemplate restTemplate, ProgramIntegrationTrackingRepository programIntegrationTrackingRepository, APITracker apiTracker) {
        this.environment = environment;
        this.restTemplate = restTemplate;
        this.programIntegrationTrackingRepository = programIntegrationTrackingRepository;
        this.apiTracker = apiTracker;
    }

    @Transactional(rollbackForClassName = {"NullpointerException"})
    public CompletableFuture<Integer> triggerProgramActiveEmail(CCClientProgramDto ccClientProgramDto) throws Exception {
        String requestUrl = getBaseUrl();
        try {
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<CCClientProgramDto> entity = new HttpEntity<CCClientProgramDto>(ccClientProgramDto, headers);
            ResponseEntity<EmailTriggerResponse> response = restTemplate.postForEntity(requestUrl, entity, EmailTriggerResponse.class);
            apiTracker.trackIntegrationService("", response.getStatusCode().toString(), ccClientProgramDto.toString(), CLIENT_CENTER, requestUrl);
            return CompletableFuture.completedFuture(response.getStatusCode().value());
        } catch (HttpStatusCodeException e) {
            apiTracker.trackIntegrationService(e.getMessage(), e.getStatusCode().toString(), ccClientProgramDto.toString(), CLIENT_CENTER, requestUrl);
            return null;
        } catch (Exception e) {
            apiTracker.trackIntegrationService(e.getMessage(), "500", ccClientProgramDto.toString(), CLIENT_CENTER, requestUrl);
            return null;
        }

    }

    public String getBaseUrl() {
        return environment.getProperty("client-center.server") + emailTriggerEndPoint;
    }



}
