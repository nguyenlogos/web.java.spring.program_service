package aduro.basic.programservice.cc;

import aduro.basic.programservice.cc.dto.ESDemoGraphicResponseDto;
import aduro.basic.programservice.cc.dto.ESResponseDto;
import aduro.basic.programservice.helpers.APITracker;
import aduro.basic.programservice.helpers.RequestHeaderBuilder;
import aduro.basic.programservice.repository.ProgramIntegrationTrackingRepository;
import aduro.basic.programservice.service.impl.ProgramServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

import static aduro.basic.programservice.config.Constants.EMPLOYEE_SERVICE;

@Service
@Transactional
public class EmployeeServiceRest {
    private final Logger log = LoggerFactory.getLogger(EmployeeServiceRest.class);
    private final String employeeServiceProfile = "/api/v1/employees/${aduroId}/profile";
    private final String employersDemographicService = "/api/v1/employers/${employerId}/demographics";

    private final Environment environment;

    private final RestTemplate restTemplate;

    private final ProgramIntegrationTrackingRepository programIntegrationTrackingRepository;

    private final APITracker apiTracker;

    public EmployeeServiceRest(Environment environment, RestTemplate restTemplate, ProgramIntegrationTrackingRepository programIntegrationTrackingRepository, APITracker apiTracker) {
        this.environment = environment;
        this.restTemplate = restTemplate;
        this.programIntegrationTrackingRepository = programIntegrationTrackingRepository;
        this.apiTracker = apiTracker;
    }

    @Transactional(rollbackForClassName = {"NullpointerException"})
    public ESResponseDto getEmployeeProfile(String employeeId) throws Exception {
        String requestUrl = getBaseUrl() + employeeServiceProfile.replace("${aduroId}", employeeId);
        try {
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            ResponseEntity<ESResponseDto> response = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, ESResponseDto.class);
            apiTracker.trackIntegrationService("", response.getStatusCode().toString(), employeeId, EMPLOYEE_SERVICE, requestUrl);
            return CompletableFuture.completedFuture(response.getBody()).get();
        } catch (HttpStatusCodeException e) {
            apiTracker.trackIntegrationService(e.getMessage(), e.getStatusCode().toString(), employeeId, EMPLOYEE_SERVICE, requestUrl);
            return null;
        } catch (Exception e) {
            apiTracker.trackIntegrationService(e.getMessage(), "500", employeeId, EMPLOYEE_SERVICE, requestUrl);
            return null;
        }

    }

    @Transactional(rollbackForClassName = {"NullpointerException"})
    public ESDemoGraphicResponseDto getEmployerDemographic(String employerId) throws Exception {
        String requestUrl = getBaseUrl() + employersDemographicService.replace("${employerId}", employerId);
        log.debug("getDemoGraphicList url:{}", requestUrl);
        try {
            HttpHeaders headers = RequestHeaderBuilder.initHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            ResponseEntity<ESDemoGraphicResponseDto> response = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, ESDemoGraphicResponseDto.class);
            return CompletableFuture.completedFuture(response.getBody()).get();
        } catch (HttpStatusCodeException e) {
            throw new Exception(e.getResponseBodyAsString());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public String getBaseUrl() {
        return environment.getProperty("client-center.es-url");
    }
}
