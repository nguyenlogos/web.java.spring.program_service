package aduro.basic.programservice.helpers;

import org.springframework.http.HttpHeaders;


public class RequestHeaderBuilder {
	
	public static HttpHeaders initHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("User-Agent", "ProgramService");
		return headers;
	}
}
