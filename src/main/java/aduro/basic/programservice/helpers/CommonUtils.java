package aduro.basic.programservice.helpers;

import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramIntegrationTracking;
import aduro.basic.programservice.domain.enumeration.ActivityType;
import aduro.basic.programservice.domain.enumeration.BioCategory;
import aduro.basic.programservice.domain.enumeration.BiometricCode;
import com.google.common.base.Enums;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class CommonUtils {

    /*get ActivityType*/
    public static ActivityType getIfPresent(String name) {
        return Enums.getIfPresent(ActivityType.class, name).orNull();
    }

    public static Map<String, List<String >> getBioConfiguration() {
        List<String> bloods = new ArrayList<>();
        bloods.add(BiometricCode.BP_Diastolic__c.getValue());
        bloods.add(BiometricCode.BP_Systolic__c.getValue());

        List<String> cholesterol = new ArrayList<>();
        cholesterol.add(BiometricCode.TC_HDL_Ratio__c.getValue());
        cholesterol.add(BiometricCode.RTrig__c.getValue());
        cholesterol.add(BiometricCode.RCho__c.getValue());
        cholesterol.add(BiometricCode.RHdl__c.getValue());

        List<String> glucose = new ArrayList<>();
        glucose.add(BiometricCode.RFpg__c.getValue());
        glucose.add(BiometricCode.Non_Fasting_Y_N__c.getValue());

        List<String> bodyCompositions = new ArrayList<>();
        bodyCompositions.add(BiometricCode.BMI__c.getValue());
        bodyCompositions.add(BiometricCode.Waist__c.getValue());

        Map<String, List<String >> configs = Stream.of(
            new AbstractMap.SimpleEntry<>(BioCategory.BLOOD_PRESSURE.getValue(), bloods),
            new AbstractMap.SimpleEntry<>(BioCategory.CHOLESTEROL.getValue(), cholesterol),
            new AbstractMap.SimpleEntry<>(BioCategory.GLUCOSE.getValue(), glucose),
            new AbstractMap.SimpleEntry<>(BioCategory.BODY_COMPOSITION.getValue(), bodyCompositions)
        ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return configs;
    }

    public static String findSubCategoryByCode(String code) {
        String subCategory = getBioConfiguration().entrySet().stream()
            .filter(stringListEntry -> stringListEntry.getValue().stream().filter(v -> code.equals(code)).collect(Collectors.toList()).isEmpty())
            .map(e -> e.getKey()).collect(Collectors.joining());
        return subCategory;
    }


    public static ProgramIntegrationTracking setTrackingAPI(String errorMessage, String statusCode, String payload, String sideIntegration, String endPoint) {
        ProgramIntegrationTracking programIntegrationTracking = new ProgramIntegrationTracking();
        programIntegrationTracking.setServiceType(sideIntegration);
        programIntegrationTracking.setErrorMessage(errorMessage);
        programIntegrationTracking.setCreatedAt(Instant.now());
        programIntegrationTracking.setAttemptedAt(Instant.now());
        programIntegrationTracking.setStatus(statusCode);
        programIntegrationTracking.setPayload(payload);
        programIntegrationTracking.setEndPoint(endPoint);
        return programIntegrationTracking;
    }

    public static Predicate<String> nullOrEqualSubgroupId(String subId) {
        return (subgroupId) -> subgroupId == null || subgroupId.equals("")
            || (subgroupId.equalsIgnoreCase(subId));
    }

    public static BigDecimal roundPoint(BigDecimal point) {
        if (point == null) return BigDecimal.ZERO;
        return point.setScale(0, RoundingMode.HALF_UP);
    }

    public static BigDecimal roundNearestFive(Double value) {
        if (value == null) return BigDecimal.ZERO;
        return BigDecimal.valueOf(Math.round(value.doubleValue() / 5) * 5);
    }

    public static BigDecimal getEconomyPointByProgram(Program program, ApplicationProperties properties) {
        if ( program == null || program.getUserPoint() == null) {
            return BigDecimal.ZERO;
        }
        BigDecimal defaultPoint = properties.getTotalPercentEconomy().multiply(program.getUserPoint());
        return  program.getEconomyPoint().compareTo(BigDecimal.ZERO) == 0 ? defaultPoint :  program.getEconomyPoint();
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }
}
