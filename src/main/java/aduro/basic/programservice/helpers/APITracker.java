package aduro.basic.programservice.helpers;


import aduro.basic.programservice.domain.ProgramIntegrationTracking;
import aduro.basic.programservice.repository.ProgramIntegrationTrackingRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
@Transactional
public class APITracker {

    private final ProgramIntegrationTrackingRepository programIntegrationTrackingRepository;

    public APITracker(ProgramIntegrationTrackingRepository programIntegrationTrackingRepository) {
        this.programIntegrationTrackingRepository = programIntegrationTrackingRepository;
    }
    @Transactional(rollbackFor = Exception.class)
    public void trackIntegrationService(String errorMessage, String statusCode, String payload, String sideIntegration, String endPoint) {
        ProgramIntegrationTracking programIntegrationTracking = new ProgramIntegrationTracking();
        programIntegrationTracking.setServiceType(sideIntegration);
        programIntegrationTracking.setErrorMessage(errorMessage);
        programIntegrationTracking.setCreatedAt(Instant.now());
        programIntegrationTracking.setAttemptedAt(Instant.now());
        programIntegrationTracking.setStatus(statusCode);
        programIntegrationTracking.setPayload(payload);
        programIntegrationTracking.setEndPoint(endPoint);
        programIntegrationTrackingRepository.save(programIntegrationTracking);
    }
}
