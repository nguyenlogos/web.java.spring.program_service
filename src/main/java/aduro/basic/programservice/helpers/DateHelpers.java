package aduro.basic.programservice.helpers;

import com.google.common.base.Strings;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Interval;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.YearMonth;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

/**
 * Created by HaiVu on 7/2/18.
 */
public class DateHelpers {

    private static final long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;
    public static final String DATE_ONLY = "MM/dd/yyyy";
    static Map<String, String> timeZoneDict;

//    public static LocalTime getLocalTimeFromTimeString(String timeString) {
//
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(Constants.TIME_FORMAT);
//        LocalTime startTime;
//        try {
//            startTime = LocalTime.parse(timeString, dateTimeFormatter);
//        } catch (Exception ex) {
//            startTime = LocalTime.parse(timeString, DateTimeFormatter.ofPattern(Constants.TIME_FORMAT_HH_MM));
//        }
//
//        return startTime;
//    }


    public static boolean isValidYear(Date date){

        if(date == null){
            return false;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        return year > 1900;
    }


    public static Date getDateOnly(Date date) {
        DateFormat formatter = new SimpleDateFormat(DATE_ONLY);

        try {
            return formatter.parse(formatter.format(date));
        } catch (ParseException e) {
            return date;
        }
    }

//    public static Date getDateWithFormat(String dateTime, String format) {
//
//        if (StringUtils.isEmpty(dateTime)) return null;
//        SimpleDateFormat formatter = new SimpleDateFormat(format);
//
//        Date formattedDate = null;
//        try {
//            formattedDate = formatter.parse(dateTime);
//        } catch (ParseException e) {
//            return null;
//        }
//
//        return formattedDate;
//    }
    private static Date convertToDateViaInstant(LocalDateTime dateToConvert) {
        return Date
                .from(dateToConvert.atZone(ZoneId.systemDefault())
                        .toInstant());
    }

    public static Date getDateIso8601(String dateString){
        return convertToDateViaInstant(LocalDateTime.parse(dateString));
    }

    public static String concatDateTimeString(String prefDate, String startTime12HourString) {
        // MM/DD/YYYY hh:mm A z
        return String.format("%s %s", prefDate, startTime12HourString);
    }

    public static Date getCurrentDateTimeUTC() {
        org.joda.time.DateTime now = new org.joda.time.DateTime(); // Default time zone.
        org.joda.time.DateTime zulu = now.toDateTime(DateTimeZone.UTC);
        return zulu.toDate();
    }

    public static Date getCurrentDateTimeWithZone(String zoneId) {
        org.joda.time.DateTime now = new org.joda.time.DateTime(); // Default time zone.
        if(!Strings.isNullOrEmpty(zoneId)) {
            try {
                DateTimeZone zone = DateTimeZone.forID(zoneId);
                org.joda.time.DateTime zulu = now.toDateTime(zone);
                return zulu.toDate();
            } catch (Exception ex) {
                return now.toDateTime(DateTimeZone.UTC).toDate();
            }
        }

        return now.toDateTime(DateTimeZone.UTC).toDate();
    }

    public static String toStringWithFormatAndZone(Date date, String format, String zoneId) {
        if (date == null) {
            return null;
        }
        try {
            DateFormat df = new SimpleDateFormat(format);
            DateTimeZone zone = DateTimeZone.getDefault();
            if (zoneId != null) {
                zone = DateTimeZone.forID(zoneId);
            }
            df.setTimeZone(zone.toTimeZone());
            // Get the date today using Calendar object.

            // Using DateFormat format method we can create a string
            // representation of a date with the defined format.
            return df.format(date);
        }catch (Exception ex){
            return null;
        }
    }

//    public static Date getDateFromUTC(Date utcDate, String timeZoneId){
//
//        DateTimeZone zone = DateTimeZone.getDefault();
//        if (timeZoneId != null) {
//            zone = DateTimeZone.forID(timeZoneId);
//        }
//
//        DateTime dateTimeAtEventPlace = getDateTimeWithFormat(toStringWithFormat(utcDate, Constants.DATE_FULL_FORMAT), Constants.DATE_FULL_FORMAT, DateTimeZone.UTC.getID());
//        if(dateTimeAtEventPlace != null) {
//            DateTime utcTime = dateTimeAtEventPlace.toDateTime(zone);
//            return utcTime.toDate();
//        }
//
//        return null;
//
//    }

//    public static Date getDateFromUTCDateString(String utcDate, String format){
//        DateTime utc = getDateTimeWithFormat(utcDate, format, DateTimeZone.UTC.getID());
//
//        if(utc == null){
//            return null;
//        }
//        DateTimeZone zone = DateTimeZone.getDefault();
//
//        DateTime localTime = utc.toDateTime(zone);
//        return localTime.toDate();
//    }

    public static Date getUTCDateFromSystemDate(Date date){

        org.joda.time.DateTime now = new org.joda.time.DateTime(date); // Default time zone.
        org.joda.time.DateTime zulu = now.toDateTime(DateTimeZone.UTC);

        return zulu.toDate();
    }

//    public static Date getDate(String dateTime) {
//
//        return getDateWithFormat(dateTime, DATE_ONLY);
//    }
//
//    public static LocalTime getLocalDate(String dateTime) {
//
//        if (StringUtils.isEmpty(dateTime)) return null;
//
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_ONLY);
//
//        //convert String to LocalDate
//
//        return LocalTime.parse(dateTime, formatter);
//    }

    public static String toStringWithFormat(Date date, String format) {
        if (date == null) {
            return null;
        }
        try {
            DateFormat df = new SimpleDateFormat(format);

            // Get the date today using Calendar object.

            // Using DateFormat format method we can create a string
            // representation of a date with the defined format.
            String dateStr = df.format(date);
            return dateStr;
        }catch (Exception ex){
            return null;
        }
    }

    public static String toEncodedStringWithFormat(Date date, String format) {
        if (date == null) {
            return null;
        }
        try {
            DateFormat df = new SimpleDateFormat(format);

            // Get the date today using Calendar object.

            // Using DateFormat format method we can create a string
            // representation of a date with the defined format.
            String dateStr = df.format(date);
            dateStr = URLEncoder.encode(dateStr, "UTF-8");
            return dateStr;
        }catch (Exception ex){
            return null;
        }
    }

    public static  Date getStarOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static  Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);

        return calendar.getTime();
    }

    public static Calendar getCalendarForNow() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        return calendar;
    }

    public static void setTimeToBeginningOfDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    public static void setTimeToEndofDay(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
    }

    public static Date getDateBefore(Date dateInput, int dateNum) {
        Calendar c = Calendar.getInstance();
        c.setTime(dateInput);
        c.add(Calendar.DATE, -dateNum);
        return c.getTime();
    }

    public static Date getDateBeforeHours(Date dateInput, int hourNum) {
        Calendar c = Calendar.getInstance();
        c.setTime(dateInput);
        c.add(Calendar.HOUR, -hourNum);
        return c.getTime();
    }

    public static Date getDateAfterMinutes(Date dateInput, int minutesNum) {
        Calendar c = Calendar.getInstance();
        c.setTime(dateInput);
        c.add(Calendar.MINUTE, minutesNum);
        return c.getTime();
    }

    public static Date getDateAfter(Date dateInput, int dateNum) {
        Calendar c = Calendar.getInstance();
        c.setTime(dateInput);
        c.add(Calendar.DATE, dateNum);
        return c.getTime();
    }


    public static boolean compareHrsAndMintsOnly(Date startSH, Date now, Date stopSH) {
        DateTime start = toDateTime(startSH);
        DateTime end = toDateTime(stopSH);

        Interval interval = new Interval(start, end);

        DateTime instant = toDateTime(now);

        // now is between startSH and stopSH
        return interval.contains(instant);
    }

    private static DateTime toDateTime(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return new DateTime(
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH),
                c.get(Calendar.HOUR_OF_DAY),
                c.get(Calendar.MINUTE));
    }


    public static Date lastDayOfMonth(Date date) {
        DateTime dateTime = toDateTime(date);
        YearMonth yearMonth = YearMonth.from(date.toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate());
        return Date.from(yearMonth.atEndOfMonth().atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date startDayOfMonth(Date date) {
        DateTime dateTime = toDateTime(date);
        YearMonth yearMonth = YearMonth.from(date.toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate());
        return Date.from(yearMonth.atDay(1).atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date getMonthStart(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    public static Date getMonthEnd(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return calendar.getTime();
    }



    public static Date dateUTC(Date fromDate) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(fromDate);
        return calendar.getTime();
    }


    public static Date getFirstDayOfWeekInUS(Date date) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    public static Date lastDayOfWeek( Date date) {
        int offset = 6 * 24 * 60 * 60 * 1000;
        Date d = new Date();
        d.setTime(date.getTime() + offset);
        return d;
    }

    public static Date getLastDayOfWeek(Date date) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek() + 6);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }

    public static int getDayOfWeek(Date date) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == 1) {
            return 7;
        } else {
            return dayOfWeek - 1;
        }
    }

    public static int getNumberDaysOfMonth(Date date) {
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar calendar = Calendar.getInstance(timeZone);
        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(date);
        int numberOfDays = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        return  numberOfDays;
    }

    public static boolean isBetween(Date target, Date begin, Date end) {
        return begin.before(target) && target.before(end);
    }


    public static List<Date> getListOfDaysBetweenTwoDates(Date startDate, Date endDate) {
        List<Date> result = new ArrayList<Date>();
        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        Calendar start = Calendar.getInstance(timeZone);
        start.setTime(startDate);
        Calendar end = Calendar.getInstance(timeZone);
        end.setTime(endDate);
        end.add(Calendar.DAY_OF_YEAR, 1); //Add 1 day to endDate to make sure endDate is included into the final list
        while (start.before(end)) {
            result.add(start.getTime());
            start.add(Calendar.DAY_OF_YEAR, 1);
        }
        return result;
    }

    public static Date findPrevDay(Date date)
    {
        return new Date(date.getTime() - MILLIS_IN_A_DAY);
    }


}
