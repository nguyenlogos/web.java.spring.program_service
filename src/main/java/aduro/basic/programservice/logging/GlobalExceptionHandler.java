package aduro.basic.programservice.logging;

import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.springframework.context.i18n.LocaleContextHolder.getLocale;

@ControllerAdvice
public class GlobalExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<GenericApiResultDto> handleMessageNotReadableException(Exception ex, WebRequest request) {
        // some handling
        logUnhandledException(ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    @ExceptionHandler(value = BadRequestAlertException.class)
    public ResponseEntity<GenericApiResultDto> handleBadRequestApiException (Exception e) {
        logUnhandledException(e);

        String url = getRequestUrl();
        if (StringUtils.startsWith(url, "/api/")) {

            // log error
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            logger.error("Error code: {} - Message {}", timeStamp, e.getMessage());
            String stackTraceMessage = "";
            if(e.getStackTrace() != null && e.getStackTrace().length > 0){
                StackTraceElement stackTraceElement =  e.getStackTrace()[0];
                stackTraceMessage = String.format("%s FileName %s, method %s, lineNumber: %d ", e.getMessage(),
                    stackTraceElement.getFileName(), stackTraceElement.getMethodName(), stackTraceElement.getLineNumber() );
            }
            // return for user
            GenericApiResultDto result = GenericApiResultDto.genericResultBuilder()
                .statusCode(HttpStatus.BAD_REQUEST)
                .message(e.getMessage())
                .exception(stackTraceMessage)
                .build();
            return ResponseEntity.status(result.getStatusCode()).body(result);
        }

        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<GenericApiResultDto> handleUnhandledApiException (Exception e) {
        logUnhandledException(e);

        String url = getRequestUrl();
        if (StringUtils.startsWith(url, "/api/")) {

            // log error
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            logger.error("Error code: {} - Message {}", timeStamp, e.getMessage());

            String stackTraceMessage = "";
            if(e.getStackTrace() != null && e.getStackTrace().length > 0){
                StackTraceElement stackTraceElement =  e.getStackTrace()[0];
                stackTraceMessage = String.format("%s FileName %s, method %s, lineNumber: %d ", e.getMessage(),
                    stackTraceElement.getFileName(), stackTraceElement.getMethodName(), stackTraceElement.getLineNumber() );
            }
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            if(e instanceof InsufficientAuthenticationException){
                status = HttpStatus.UNAUTHORIZED;
            }
            // return for user
            GenericApiResultDto result = GenericApiResultDto.genericResultBuilder()
                .statusCode(status)
                .message(e.getMessage())
                .exception(stackTraceMessage)
                .build();
            return ResponseEntity.status(result.getStatusCode()).body(result);
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    private void logUnhandledException (Exception e) {
        String url = getRequestUrl();
        if (url != null) {
            logger.error("Error happen on request {}", url);
        }

        if (e instanceof HttpRequestMethodNotSupportedException
            || e instanceof HttpMediaTypeNotSupportedException) {
            logger.error(e.getMessage());
        } else {
            if(e.getStackTrace() != null && e.getStackTrace().length > 0 ) {
                StackTraceElement stackTraceElement =  e.getStackTrace()[0];
                String stackTraceMessage = String.format("%s FileName %s, method %s, lineNumber: %d ", e.getMessage(),
                    stackTraceElement.getFileName(), stackTraceElement.getMethodName(), stackTraceElement.getLineNumber() );
                logger.error(e.getMessage(), stackTraceMessage);
            }else{
                logger.error(e.getMessage());
            }
        }
    }

    private String getRequestUrl () {
        try {
            ServletRequestAttributes requestAtt =
                (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            return requestAtt.getRequest().getRequestURI();
        } catch (Exception e1) {
            // this call can be in the background thread
        }
        return null;
    }


}
