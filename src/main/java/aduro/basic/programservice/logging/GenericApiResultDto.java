package aduro.basic.programservice.logging;

import com.fasterxml.jackson.annotation.JsonGetter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.List;

@Getter
@Setter
@ToString
@Builder(builderMethodName = "genericResultBuilder")
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "All APIs will return either this model or a subclass of this model.")
public class GenericApiResultDto {

    @ApiModelProperty(
            value = "200 if the API call succeeded, otherwise <> 200",
            allowableValues = "{200, <>200}",
            required = true)
    private HttpStatus statusCode;

    @JsonGetter
    public int getStatusCodeNumber(){
        if(statusCode == null){
            return HttpStatus.OK.value();
        }
        return statusCode.value();
    }

    @ApiModelProperty(value = "For global message regardless the fields which were sent in the request")
    private String message;

    @ApiModelProperty(value = "For detail exception")
    private String exception;

    @ApiModelProperty(value = "For a message that is specific for a field in the request. Example use case is a validation error")
    private List<FieldError> errors;

    public static GenericApiResultDto ok() {
        return GenericApiResultDto.genericResultBuilder()
                .statusCode(HttpStatus.OK)
                .build();
    }
}
