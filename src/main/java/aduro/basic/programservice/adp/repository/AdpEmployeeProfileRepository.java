package aduro.basic.programservice.adp.repository;

import aduro.basic.programservice.adp.domain.AdpEmployeeProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AdpEmployeeProfileRepository extends JpaRepository<AdpEmployeeProfile, Long>, JpaSpecificationExecutor<AdpEmployeeProfile> {
    @Query("select distinct em.gender from AdpEmployeeProfile em where em.gender is not null group by em.gender")
    List<String> findAllGenders();

    @Query("select p from AdpEmployeeProfile p where p.employeeId=:participantId")
    Optional<AdpEmployeeProfile> findParticipantById(@Param("participantId") String participantId);

    Optional<AdpEmployeeProfile> getByEmployeeId(String employeeId);
}
