package aduro.basic.programservice.adp.repository;

import aduro.basic.programservice.adp.domain.AdpProcessingError;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface AdpProcessingErrorRepository extends JpaRepository<AdpProcessingError, Integer>, JpaSpecificationExecutor<AdpProcessingError> {

    List<AdpProcessingError> findAllByDataType(String dataType);
}
