package aduro.basic.programservice.adp.repository;

import aduro.basic.programservice.adp.domain.AdpEmployees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdpEmployeeRepository extends JpaRepository<AdpEmployees, String>, JpaSpecificationExecutor<AdpEmployees> {

    @Query(value = "select distinct em.job_code  from em_employees em where em.job_code is not null group by em.job_code", nativeQuery = true)
    List<String> findAllByJobCode();

    @Query(value = "select distinct em.division  from em_employees em where em.division is not null group by em.division", nativeQuery = true)
    List<String> findAllByDivision();

    @Query(value = "select distinct em.group_code  from em_employees em where em.group_code is not null group by em.group_code", nativeQuery = true)
    List<String> findAllByGroupCode();

    @Query(value = "select distinct em.health_plan  from em_employees em where em.health_plan is not null group by em.health_plan", nativeQuery = true)
    List<String> findAllByHealthPlan();

    @Query("select distinct em.location  from AdpEmployees em where em.location is not null group by em.location")
    List<String> findAllByLocations();

    @Query("select distinct em.region  from AdpEmployees em where em.region is not null group by em.region")
    List<String> findAllByRegions();

    @Query(value = "select distinct em.store  from em_employees em where em.store is not null group by em.store", nativeQuery = true)
    List<String> findAllByStore();

    @Query(value = "select distinct em.age_range  from em_employees em where em.age_range is not null group by em.age_range", nativeQuery = true)
    List<String> findAllByAgeRange();

    @Query(value = "select distinct em.bargaining_unit  from em_employees em where em.bargaining_unit is not null group by em.bargaining_unit", nativeQuery = true)
    List<String> findAllByBargainingUnit();

    @Query(value = "select distinct em.class  from em_employees em where em.class is not null group by em.class", nativeQuery = true)
    List<String> findAllByClass();

    @Query(value = "select distinct em.department  from em_employees em where em.department is not null group by em.department", nativeQuery = true)
    List<String> findAllByDepartment();

    @Query(value = "select distinct em.district  from em_employees em where em.district is not null group by em.district", nativeQuery = true)
    List<String> findAllByDistrict();

    @Query(value = "select distinct em.facility  from em_employees em where em.facility is not null group by em.facility", nativeQuery = true)
    List<String> findAllByFacility();

    @Query(value = "select distinct em.status  from em_employees em where em.status is not null group by em.status", nativeQuery = true)
    List<String> findAllByStatus();

    @Query("select distinct em.relationship  from AdpEmployees em where em.relationship is not null group by em.relationship")
    List<String> findAllByRelationships();

    @Query(value = "select distinct em.new_hire  from em_employees em where em.new_hire is not null group by em.new_hire", nativeQuery = true)
    List<String> findAllByNewHire();
}
