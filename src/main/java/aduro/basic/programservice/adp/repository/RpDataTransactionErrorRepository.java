package aduro.basic.programservice.adp.repository;

import aduro.basic.programservice.adp.domain.RpDataTransactionError;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface RpDataTransactionErrorRepository extends JpaRepository<RpDataTransactionError, Integer>, JpaSpecificationExecutor<RpDataTransactionError> {

}
