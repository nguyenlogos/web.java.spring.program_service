package aduro.basic.programservice.adp.repository;

import aduro.basic.programservice.adp.domain.AmpUserActivityProgressHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
/*@Transactional("adpTransactionManager")
@PersistenceContext(name = "adpEntityManagerFactory")*/
public interface AmpUserActivityProgressHistoryRepository extends JpaRepository<AmpUserActivityProgressHistory, Long>, JpaSpecificationExecutor<AmpUserActivityProgressHistory> {

    @Query("SELECT amp from AmpUserActivityProgressHistory amp WHERE amp.userId = :userId AND amp.customActivityId = :customActivityId AND amp.trackingDate = :trackingDate AND amp.trackingDate >= :p1 And amp.trackingDate <= :p2")
    List<AmpUserActivityProgressHistory> findByUserIdAndCustomActivityIdAndTrackingDateBetween(@Param("userId") long userId,
                                                                                               @Param("customActivityId") long customActivityId,
                                                                                               @Param("trackingDate") Instant trackingDate,
                                                                                               @Param("p1") Instant p1,
                                                                                               @Param("p2") Instant p2);

    List<AmpUserActivityProgressHistory> findAllByUserIdAndCustomActivityId(long  userId, long customId);

    List<AmpUserActivityProgressHistory>findAllByUserIdAndCustomActivityIdAndTrackingDateBetween(long userId, long customId, Instant p1, Instant p2);
}
