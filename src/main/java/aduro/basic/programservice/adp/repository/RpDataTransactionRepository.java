package aduro.basic.programservice.adp.repository;

import aduro.basic.programservice.adp.domain.RpDataTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface RpDataTransactionRepository extends JpaRepository<RpDataTransaction, Integer>, JpaSpecificationExecutor<RpDataTransaction> {

}
