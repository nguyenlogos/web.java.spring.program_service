package aduro.basic.programservice.adp.repository;

import aduro.basic.programservice.adp.domain.AmpUserActivityProgress;
import io.vavr.collection.Seq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Category entity.
 */
@SuppressWarnings("unused")
@Repository
/*@Transactional("adpTransactionManager")
@PersistenceContext(name = "adpEntityManagerFactory")*/
public interface AmpUserActivityProgressRepository extends JpaRepository<AmpUserActivityProgress, Long>, JpaSpecificationExecutor<AmpUserActivityProgress> {
    List<AmpUserActivityProgress> findByUserIdAndTypeAndCompletedAtIsNull(Long userId, String activityType);
    List<AmpUserActivityProgress> findByUserIdAndCustomActivityId(Long userId, Long customActivity);
    Seq<AmpUserActivityProgress> findByUserIdAndType(Long userId, String activityType);
    Optional<AmpUserActivityProgress> findByUserIdAndCustomActivityIdAndCompletedAtNull(Long userId, Long customActivityId);
    Optional<AmpUserActivityProgress> findByUserIdAndCustomActivityIdAndCompletedAtNotNull(Long userId, Long customActivityId);
    long countByCustomActivityId(Long customActivity);

    List<AmpUserActivityProgress> findByUserIdAndTypeAndCustomActivityIdInAndCompletedAtIsNull(Long userId, String activityType, List<Long> customActivityIds);
}
