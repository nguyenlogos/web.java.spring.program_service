package aduro.basic.programservice.adp.repository;

import aduro.basic.programservice.adp.domain.AmpUserActivityIncentive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AmpUserActivityIncentiveRepository extends JpaRepository<AmpUserActivityIncentive, String>, JpaSpecificationExecutor<AmpUserActivityIncentive> {

    List<AmpUserActivityIncentive> findAllByTargetIdAndCustomActivityIdAndStatus(long userId, long customId, String status);
}
