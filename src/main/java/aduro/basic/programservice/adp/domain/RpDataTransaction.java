package aduro.basic.programservice.adp.domain;

import aduro.basic.programservice.service.dto.SupportableActivityType;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.Instant;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "rp_data_transaction")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RpDataTransaction {

    public enum DataType {
        eligibility_data, incentive_data,
        unknown_export_to_egnyte, query_result,
        incentive_points_data,
        incentive_events_data, event_challenge, custom;
    }

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "client_admin_id")
    private String clientAdminId;

    @Column(name = "client_ftp_id")
    private Integer clientFtpId;

    @Column(name = "data_vendor_id")
    private Integer dataVendorId;

    @Column(name = "query_result_id")
    private Integer queryResultId;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "stored_file_path")
    private String storeFilePath;

    @Column(name = "type")
    private String type;

    @Column( name = "data_type", columnDefinition = "ENUM('eligibility_data', 'incentive_data', 'unknown_export_to_egnyte', 'query_result', 'incentive_points_data', 'incentive_events_data', 'event_challenge', 'custom')")
    @Enumerated(EnumType.STRING)
    private DataType dataType;

    @Column(name = "method")
    private String method;

    @Column(name = "status")
    private String status;

    @Column(name = "row_count")
    private Integer rowCount;

    @Column(name = "processed_row_count")
    private Integer processRowCount;

    @Column(name = "success_row_count")
    private Integer successRowCount;

    @Column(name = "error_row_count")
    private Integer errorRowCount;

    @Column(name = "row_changed_count")
    private Integer rowChangedCount;

    @Column(name = "row_inserted_count")
    private Integer rowInsertedCount;

    @Column(name = "errors_count")
    private Integer errorsCount;

    @Column(name = "tbo_removed_count")
    private Integer tboRemoveCount;

    @Column(name = "tbo_count")
    private Integer tboCount;

    @Column(name = "rehired_count")
    private Integer rehiredCount;

    @Column(name = "terminated_count")
    private Integer terminatedCount;

    @Column(name = "exported_changes_to_limeade")
    private Boolean exportedChangesToLimeade;

    @Column(name = "is_completed")
    private Boolean isCompleted;

    @Column(name = "file_id")
    private String fileId;

    @Column(name = "s3_file_info")
    private String s3FileInfo;

    @Column(name = "s3_decrypted_file_info")
    private String s3DecryptedFil;

    @Column(name = "encrypted_file_id")
    private Integer encryptedFileId;

    @Column(name = "is_encrypted")
    private Boolean isEncrypted;

    @Column(name = "is_test")
    private Boolean isTest;

    @Column(name = "initiated_by")
    private String initiatedBy;

    @Column(name = "exported_to_limeade")
    private Boolean exportedToLimeade;

    @Column(name = "exported_to_egnyte")
    private Boolean exportedToEgnyte;

    @Column(name = "transaction_config")
    private String transactionConfig;

    @Column(name = "transaction_metadata")
    private String transactionMetadata;

    @Column(name = "migration_id")
    private Integer migrationId;

    @Column(name = "start_date")
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @Column(name = "upload_date")
    private Instant uploadDate;

    @Column(name = "test_date")
    private Instant testDate;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "deleted_at")
    private Instant deletedAt;

    @Column(name = "retry_count")
    private Integer retryCount;

    @Column(name = "source_file")
    private String sourceFile;

    @Column(name = "source_file_path")
    private String sourceFilePath;

    @Column(name = "updated_by")
    private String updatedBy;
}
