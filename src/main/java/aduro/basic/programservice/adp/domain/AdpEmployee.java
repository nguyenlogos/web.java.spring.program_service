package aduro.basic.programservice.adp.domain;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "em_employees")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdpEmployee {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "employee_id")
    private String employeeId;

    @Column(name = "employer_id")
    private String employerId;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "external_id")
    private String externalId;

    @Column(name = "is_terminated")
    private Boolean isTerminated;

    @Column(name = "is_program_test")
    private Boolean isProgramTest;
}
