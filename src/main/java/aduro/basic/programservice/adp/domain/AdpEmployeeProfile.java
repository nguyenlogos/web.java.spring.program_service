package aduro.basic.programservice.adp.domain;

import aduro.basic.programservice.domain.enumeration.Gender;
import lombok.*;

import javax.persistence.*;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "em_employees_profile")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdpEmployeeProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "employee_id")
    private String employeeId;

    @Column(name = "account_created")
    private Integer accountCreated;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    private Gender gender;
}
