
package aduro.basic.programservice.adp.domain;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "amp_user_activity_progress_histories")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AmpUserActivityProgressHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "custom_activity_id", nullable = false)
    private Long customActivityId;

    @Column(name = "tracking_date", nullable = false)
    private Instant trackingDate;

    @Column(name = "tracking_value", nullable = false)
    private Integer trackingValue;


    @Column(name = "tracking_unit", nullable = false)
    private String trackingUnit;

    @Column(name = "activity_progress_id")
    private Long activityProgressId;

    @Column(name = "tracking_completed")
    private Boolean trackingCompleted;

    @Column(name = "tracking_type")
    private Integer trackingType;

    @Column(name = "tracking_method")
    private String trackingMethod;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

}

