package aduro.basic.programservice.adp.domain;


import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "amp_user_activity_incentives")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AmpUserActivityIncentive {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "target_id")
    private String targetId;

    @Column(name = "target_type")
    private String targetType;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "tracking_date")
    private Instant trackingDate;

    @Column(name = "custom_activity_id")
    private Long customActivityId;

    @Column(name = "payload")
    private String payload;

    @Column(name = "response")
    private String response;

    @Column(name = "status")
    private String status;
}
