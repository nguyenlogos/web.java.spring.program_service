
package aduro.basic.programservice.adp.domain;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "amp_user_activity_progresses")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AmpUserActivityProgress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "custom_activity_id", nullable = false)
    private Long customActivityId;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "created_at", nullable = false)
    private Instant createdAt;

    @Column(name = "updated_at", nullable = false)
    private Instant updatedAt;

    @Column(name = "completed_at", nullable = false)
    private Instant completedAt;

    @Column(name = "is_uploaded", nullable = false)
    private Boolean isUploaded;


}

