package aduro.basic.programservice.adp.domain;


import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "rp_data_transaction_error")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RpDataTransactionError {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "error_code")
    private Integer errorCode;

    @Column(name = "message")
    private String message ;

    @Column(name = "data_type")
    private String dataType;

    @Column(name = "row_count")
    private Integer rowCount;

    @Column(name = "field")
    private String field;

    @Column(name = "value")
    private String value;

    @Column(name = "is_test")
    private Boolean isTest;

    @Column(name = "is_warning")
    private Boolean isWarning;

    @Column(name = "is_internal")
    private Boolean isInternal;

    @Column(name = "is_critical")
    private Boolean isCritical;

    @Column(name = "is_fixed")
    private Boolean isFixed;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "fixed_at")
    private Instant fixedAt;

    @Column(name = "data_transaction")
    private String dataTransaction;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "client_admin_id")
    private String clientAdminId;
}
