package aduro.basic.programservice.adp.domain;
import javax.persistence.*;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "em_employees")
public class AdpEmployees {
    @Id
    private String id;

    @Column(name = "employee_id")
    private String employeeId;

    @Column(name = "employer_id")
    private String employerId;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "relationship")
    private String relationship;

    @Column(name = "region")
    private String region;

    @Column(name = "location")
    private String location;

    @Column(name = "subscriber_id")
    private String subscriberId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployerId() {
        return employerId;
    }

    public void setEmployerId(String employerId) {
        this.employerId = employerId;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }
}
