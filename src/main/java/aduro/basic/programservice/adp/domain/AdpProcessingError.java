package aduro.basic.programservice.adp.domain;

import lombok.*;

import javax.persistence.*;

@PersistenceContext(name = "adpEntityManagerFactory")
@Entity
@Table(name = "processing_error")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AdpProcessingError {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "data_type")
    private String dataType;

    @Column(name = "error_type")
    private String errorType;

    @Column(name = "code")
    private Integer code;

    @Column(name = "name")
    private String name;
}
