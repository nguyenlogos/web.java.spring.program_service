package aduro.basic.programservice.adp.service.impl;

import aduro.basic.programservice.adp.domain.AmpUserActivityProgress;
import aduro.basic.programservice.adp.repository.AmpUserActivityProgressRepository;
import aduro.basic.programservice.adp.service.AmpUserActivityProgressService;
import aduro.basic.programservice.adp.service.mapper.AmpUserActivityProgressMapper;
import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.domain.ActivityEventQueue;
import aduro.basic.programservice.domain.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

/**
 * Service Implementation for managing {@link Category}.
 */
@Service
@Transactional
public class AmpUserActivityProgressServiceImpl implements AmpUserActivityProgressService {

    private final Logger log = LoggerFactory.getLogger(AmpUserActivityProgressServiceImpl.class);

    private final AmpUserActivityProgressRepository ampUserActivityProgressRepository;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;

    private final AmpUserActivityProgressMapper ampUserActivityProgressMapper;

    public AmpUserActivityProgressServiceImpl(AmpUserActivityProgressRepository ampUserActivityProgressRepository, AmpUserActivityProgressMapper ampUserActivityProgressMapper) {
        this.ampUserActivityProgressRepository = ampUserActivityProgressRepository;
        this.ampUserActivityProgressMapper = ampUserActivityProgressMapper;
    }

    @Override
    public Long countByCriteria() {
        return ampUserActivityProgressRepository.count();
    }

    @Override
    public void AddEmployerVerifiedActivity(ActivityEventQueue activityQueue, long customActivityId, long userId) {

        //progress
        AmpUserActivityProgress ampUserActivityProgress = new AmpUserActivityProgress();
        ampUserActivityProgress.setCreatedAt(Instant.now());
        ampUserActivityProgress.setType("EMPLOYER_VERIFIED");
        ampUserActivityProgress.setUpdatedAt(Instant.now());
        ampUserActivityProgress.setCompletedAt(activityQueue.getCreatedDate());
        ampUserActivityProgress.setUserId(userId);
        ampUserActivityProgress.setCustomActivityId(customActivityId);
        ampUserActivityProgressRepository.save(ampUserActivityProgress);
    }

    @Override
    public void updateEmployerVerifiedActivity(AmpUserActivityProgress ampUserActivityProgress, long customActivityId, long userId) {
        ampUserActivityProgress.setCreatedAt(Instant.now());
        ampUserActivityProgress.setType("EMPLOYER_VERIFIED");
        ampUserActivityProgress.setUpdatedAt(Instant.now());
        ampUserActivityProgress.setCompletedAt(Instant.now());
        ampUserActivityProgress.setUserId(userId);
        ampUserActivityProgress.setCustomActivityId(customActivityId);
        ampUserActivityProgressRepository.save(ampUserActivityProgress);
    }
}


