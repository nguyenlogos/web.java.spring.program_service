package aduro.basic.programservice.adp.service;

import aduro.basic.programservice.adp.domain.AmpUserActivityProgress;
import aduro.basic.programservice.domain.ActivityEventQueue;

public interface AmpUserActivityProgressService {
    Long countByCriteria();
    void AddEmployerVerifiedActivity(ActivityEventQueue activityQueue, long customActivityId, long userId);

    void updateEmployerVerifiedActivity(AmpUserActivityProgress progress, long customActivityId, long userId);

}
