package aduro.basic.programservice.sqs;

import aduro.basic.programservice.domain.enumeration.PublishMessageDto;
import aduro.basic.programservice.service.ProgramUserService;
import aduro.basic.programservice.service.dto.EmployeeDto;
import aduro.basic.programservice.service.dto.SNSPayloadDto;
import com.fasterxml.jackson.core.type.TypeReference;
import aduro.basic.programservice.service.dto.ProgramUserCreateRequest;
import aduro.basic.programservice.service.dto.ProgramUserDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;


@Component
public class ProgramConsumer {

    private final Logger log = LoggerFactory.getLogger(ProgramConsumer.class);

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    private ProgramUserService programUserService;

    @JmsListener(destination ="${aws.queues.switching-subgroup}")
    public void processChangeSubgroup(final String message) throws IOException {
        log.info("Message received from Subgroup Change: {}", message);
        if (Objects.nonNull(message) ) {
            PublishMessageDto<EmployeeDto> dto = objectMapper.readValue(message,  new TypeReference<PublishMessageDto<EmployeeDto>>() {});
            programUserService.switchSubgroupClient(dto.getData());
        }
    }

    @JmsListener(destination ="${aws.queues.new-program-user-creation-queue}")
    public void processCreateProgramUsers(String message) throws IOException {
        log.info("Message received from monarch account: {}", message);

        ProgramUserCreateRequest request = objectMapper.readValue(message, ProgramUserCreateRequest.class);
        programUserService.processCreateProgramUsers(request);
    }

}
