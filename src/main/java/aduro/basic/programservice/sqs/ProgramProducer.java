package aduro.basic.programservice.sqs;

import aduro.basic.programservice.service.dto.ActivityEventQueueDTO;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazon.sqs.javamessaging.SQSMessagingClientConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.jms.Message;
import java.io.Serializable;

@Component
public class ProgramProducer {

    private final Logger log = LoggerFactory.getLogger(ProgramProducer.class);

    @Resource
    ObjectMapper objectMapper;

    @Resource
    protected JmsTemplate jmsTemplate;

    public <MESSAGE extends Serializable> void send(String queue, MESSAGE payload) {

        jmsTemplate.send(queue, session -> {
            try {
                Message createMessage = session.createTextMessage(objectMapper.writeValueAsString(payload));
                createMessage.setStringProperty(SQSMessagingClientConstants.JMSX_GROUP_ID, "messageProgramGroup");
                createMessage.setStringProperty(SQSMessagingClientConstants.JMS_SQS_DEDUPLICATION_ID, "1" + System.currentTimeMillis());
                createMessage.setStringProperty("documentType", payload.getClass().getName());
                return createMessage;
            } catch (Exception | Error e) {
                log.error("Fail to send message {}", payload);
                throw new RuntimeException(e);
            }
        });
    }

}
