package aduro.basic.programservice.tangocard.dto;

import java.util.List;

public class Reward {

    private List< RewardCredential > credentialList;

    public List<RewardCredential> getCredentialList() {
        return credentialList;
    }

    public void setCredentialList(List<RewardCredential> credentialList) {
        this.credentialList = credentialList;
    }

   /* public RewardCredential getCredentials() {
        return credentials;
    }

    public void setCredentials(RewardCredential credentials) {
        this.credentials = credentials;
    }*/

    public String getRedemptionInstructions() {
        return redemptionInstructions;
    }

    public void setRedemptionInstructions(String redemptionInstructions) {
        this.redemptionInstructions = redemptionInstructions;
    }

    /*private RewardCredential credentials;*/

    private String redemptionInstructions;

}
