package aduro.basic.programservice.tangocard.dto;

public class CreditCardDepositsResponse {

    private String accountNumber;
    private float amount;
    private float amountCharged;
    private String createdDate;
    private float feePercent;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public float getAmountCharged() {
        return amountCharged;
    }

    public void setAmountCharged(float amountCharged) {
        this.amountCharged = amountCharged;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public float getFeePercent() {
        return feePercent;
    }

    public void setFeePercent(float feePercent) {
        this.feePercent = feePercent;
    }

    public String getReferenceDepositID() {
        return referenceDepositID;
    }

    public void setReferenceDepositID(String referenceDepositID) {
        this.referenceDepositID = referenceDepositID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String referenceDepositID;
    private String status;
}
