package aduro.basic.programservice.tangocard.dto;

import software.amazon.ion.Decimal;

public class OrderRequest {
    private String accountIdentifier;
    private float amount;
    private String campaign;
    private String customerIdentifier;

    public String getAccountIdentifier() {
        return accountIdentifier;
    }

    public void setAccountIdentifier(String accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getCustomerIdentifier() {
        return customerIdentifier;
    }

    public void setCustomerIdentifier(String customerIdentifier) {
        this.customerIdentifier = customerIdentifier;
    }

    public String getEmailSubject() {
        return emailSubject;
    }

    public void setEmailSubject(String emailSubject) {
        this.emailSubject = emailSubject;
    }

    public String getEtid() {
        return etid;
    }

    public void setEtid(String etid) {
        this.etid = etid;
    }

    public String getExternalRefID() {
        return externalRefID;
    }

    public void setExternalRefID(String externalRefID) {
        this.externalRefID = externalRefID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Contact getSender() {
        return sender;
    }

    public void setSender(Contact sender) {
        this.sender = sender;
    }

    public boolean isSendEmail() {
        return sendEmail;
    }

    public void setSendEmail(boolean sendEmail) {
        this.sendEmail = sendEmail;
    }

    public Contact getRecipient() {
        return recipient;
    }

    public void setRecipient(Contact recipient) {
        this.recipient = recipient;
    }

    public String getUtid() {
        return utid;
    }

    public void setUtid(String utid) {
        this.utid = utid;
    }

    private String emailSubject;
    private String etid;
    private String externalRefID;
    private String message;
    private String notes;
    Contact sender;
    private boolean sendEmail;
    Contact recipient;
    private String utid;

    public OrderRequest(String accountIdentifier, float amount, String customerIdentifier, String externalRefID, String recipientEmail,
                        String recipientFirstName, String recipientLastName, boolean sendEmail, String senderEmail, String senderFirstName, String senderLastName, String utid, String notes) {

        this.accountIdentifier = accountIdentifier;
        this.amount = amount;
        this.customerIdentifier = customerIdentifier;
        this.externalRefID = externalRefID;
        this.recipient =new Contact();
        this.recipient.setEmail(recipientEmail);
        this.recipient.setFirstName(recipientFirstName);
        this.recipient.setLastName(recipientLastName);
        this.sendEmail = sendEmail;
        this.sender = new Contact();
        this.sender.setEmail(senderEmail);
        this.sender.setFirstName(senderFirstName);
        this.sender.setLastName(senderLastName);
        this.utid =utid;
        this.notes = notes;

    }
}
