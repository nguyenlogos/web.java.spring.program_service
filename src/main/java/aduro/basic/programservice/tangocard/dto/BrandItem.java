package aduro.basic.programservice.tangocard.dto;

import software.amazon.ion.Decimal;

import java.io.Serializable;

public class BrandItem implements Serializable {
    private String utid;
    private String rewardName;
    private String currencyCode;
    private String status;

    public String getFaceValue() {
        return faceValue;
    }

    public void setFaceValue(String faceValue) {
        this.faceValue = faceValue;
    }

    private String faceValue;

    public String getUtid() {
        return utid;
    }

    public void setUtid(String utid) {
        this.utid = utid;
    }

    public String getRewardName() {
        return rewardName;
    }

    public void setRewardName(String rewardName) {
        this.rewardName = rewardName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
        this.valueType = valueType;
    }

    public Decimal getMinValue() {
        return minValue;
    }

    public void setMinValue(Decimal minValue) {
        this.minValue = minValue;
    }

    public Decimal getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Decimal maxValue) {
        this.maxValue = maxValue;
    }

    private String rewardType;
    private String valueType;
    private Decimal minValue;
    private Decimal maxValue;


}
