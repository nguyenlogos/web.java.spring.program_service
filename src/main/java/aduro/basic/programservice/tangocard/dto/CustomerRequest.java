package aduro.basic.programservice.tangocard.dto;

import java.io.Serializable;

public class CustomerRequest implements Serializable {
    private String customerIdentifier;

    public String getCustomerIdentifier() {
        return customerIdentifier;
    }

    public void setCustomerIdentifier(String customerIdentifier) {
        this.customerIdentifier = customerIdentifier;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    private String displayName;
}
