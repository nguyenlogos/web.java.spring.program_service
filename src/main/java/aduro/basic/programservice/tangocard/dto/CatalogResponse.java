package aduro.basic.programservice.tangocard.dto;

import java.io.Serializable;
import java.util.List;

public class CatalogResponse implements Serializable {

    private String catalogName;

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }


    public List<CatalogBrand> getBrands() {
        return brands;
    }

    public void setBrands(List<CatalogBrand> brands) {
        this.brands = brands;
    }

    private List<CatalogBrand> brands;


}
