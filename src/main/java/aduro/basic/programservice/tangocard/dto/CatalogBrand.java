package aduro.basic.programservice.tangocard.dto;

import java.io.Serializable;
import java.util.List;

public class CatalogBrand implements Serializable {

    private String brandKey ;
    private String brandName;
    private String disclaimer;

    private String description;
    private String shortDescription;
    private String terms;

    public List<BrandItem> getItems() {
        return items;
    }

    public void setItems(List<BrandItem> items) {
        this.items = items;
    }

    private List<BrandItem> items;

    public String getBrandKey() {
        return brandKey;
    }

    public void setBrandKey(String brandKey) {
        this.brandKey = brandKey;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }




}
