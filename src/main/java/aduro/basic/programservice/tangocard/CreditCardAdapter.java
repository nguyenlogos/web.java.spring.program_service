package aduro.basic.programservice.tangocard;

import aduro.basic.programservice.tangocard.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CreditCardAdapter extends TangoCardAdapter  {

    private String resourceCreditcard = "/creditCards";

    private String resourceCreditcardDeposits = "/creditCardDeposits";

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<CreditCardResponse> registerCreditCardAysnc(CreditCardRequest creditCardRequest) throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<CreditCardRequest> entity = new HttpEntity<CreditCardRequest>(creditCardRequest, headers);
            ResponseEntity<CreditCardResponse> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceCreditcard,entity, CreditCardResponse.class);
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }


    @Async
    public CompletableFuture<CreditCardDepositsResponse> createCreditCardDepositsAsync(CreditCardDepositsRequest creditCardDepositsRequest) throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<CreditCardDepositsRequest> entity = new HttpEntity<CreditCardDepositsRequest>(creditCardDepositsRequest, headers);
            ResponseEntity<CreditCardDepositsResponse> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceCreditcardDeposits,entity, CreditCardDepositsResponse.class);
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }


    @Async
    public CompletableFuture<List<CreditCardResponse>> getCreditCards()throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);

            String requestUrl = String.format("%s%s",this.getBaseUrl(), resourceCreditcard) ;
            ResponseEntity<List<CreditCardResponse>> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity,new ParameterizedTypeReference<List<CreditCardResponse>>(){});

            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }




}
