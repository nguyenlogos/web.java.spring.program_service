package aduro.basic.programservice.tangocard;

import aduro.basic.programservice.tangocard.dto.CatalogResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class CatalogAdapter extends TangoCardAdapter {

    private String resourceCatalogs = "/catalogs";

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<CatalogResponse> getCatalogAsync()throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = this.getBaseUrl() + resourceCatalogs;
            ResponseEntity<CatalogResponse> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, CatalogResponse.class);
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }

}
