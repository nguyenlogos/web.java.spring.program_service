package aduro.basic.programservice.tangocard;

import aduro.basic.programservice.tangocard.dto.CustomerRequest;
import aduro.basic.programservice.tangocard.dto.CustomerResponse;

import aduro.basic.programservice.tangocard.dto.OrderRequest;
import aduro.basic.programservice.tangocard.dto.OrderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class OrderAdapter extends TangoCardAdapter  {

    private String resourceOrders = "/orders";
    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<OrderResponse> CreateOrderAsync(OrderRequest orderRequest) throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<OrderRequest> entity = new HttpEntity<OrderRequest>(orderRequest, headers);
            ResponseEntity<OrderResponse> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceOrders,entity, OrderResponse.class);
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }
}
