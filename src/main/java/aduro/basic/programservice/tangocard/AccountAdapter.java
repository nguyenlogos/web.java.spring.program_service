package aduro.basic.programservice.tangocard;

import aduro.basic.programservice.tangocard.dto.AccountRequest;
import aduro.basic.programservice.tangocard.dto.AccountResponse;
import aduro.basic.programservice.tangocard.dto.CustomerRequest;
import aduro.basic.programservice.tangocard.dto.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class AccountAdapter extends TangoCardAdapter {

    private String resourceAccounts = "/accounts";

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<AccountResponse>  GetAccountDetailAsync(String accountIdentifier) throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = String.format("%s/%s/%s",this.getBaseUrl(),resourceAccounts, accountIdentifier);
            ResponseEntity<AccountResponse> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<AccountResponse>(){});
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }



}
