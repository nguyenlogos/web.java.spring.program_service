package aduro.basic.programservice.tangocard;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.core.env.Environment;

import java.nio.charset.Charset;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

@Configuration
public class TangoCardAdapter {

    @Autowired
    private Environment env;

    protected String getBaseUrl() throws Exception {
        String tangoCardApiUrl = env.getProperty("tango-card.tango-card-api-url");
        if(isNullOrEmpty(tangoCardApiUrl))
            throw new Exception("Missing Tango API URL...");
        return tangoCardApiUrl;
    }


    protected  HttpHeaders createHeaders() throws Exception {
        String tangoCardUsername = env.getProperty("tango-card.tango-card-username");
        String tangoCardPassword = env.getProperty("tango-card.tango-card-password");
        if(isNullOrEmpty(tangoCardUsername) || isNullOrEmpty(tangoCardPassword))
            throw new Exception("Missing tango card credential ...");
        return new HttpHeaders() {{
            String auth = tangoCardUsername + ":" + tangoCardPassword;
            byte[] encodedAuth = Base64.encodeBase64(
                auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }
}
