package aduro.basic.programservice.tangocard;

import aduro.basic.programservice.tangocard.dto.AccountRequest;
import aduro.basic.programservice.tangocard.dto.AccountResponse;
import aduro.basic.programservice.tangocard.dto.CustomerRequest;
import aduro.basic.programservice.tangocard.dto.CustomerResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CustomerAdapter extends TangoCardAdapter  {

    private String resourceCustomers = "/customers";

    @Autowired
    private RestTemplate restTemplate;

    @Async
    public CompletableFuture<CustomerResponse>  GetCustomerDetailAsync(String customerIdentifier) throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = String.format("%s/%s/%s",this.getBaseUrl(),resourceCustomers, customerIdentifier);
            ResponseEntity<CustomerResponse> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<CustomerResponse>(){});
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }



    @Async
    public CompletableFuture<List<CustomerResponse>>  GetCustomersAsync() throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            String requestUrl = this.getBaseUrl() + resourceCustomers;
            ResponseEntity<List<CustomerResponse>> responseEntity = restTemplate.exchange(requestUrl, HttpMethod.GET, entity, new ParameterizedTypeReference<List<CustomerResponse>>(){});
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }


    @Async
    public CompletableFuture<CustomerResponse>  CreateCustomerAysnc(CustomerRequest customerRequest) throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            HttpEntity<CustomerRequest> entity = new HttpEntity<CustomerRequest>(customerRequest, headers);
            ResponseEntity<CustomerResponse> responseEntity = restTemplate.postForEntity(this.getBaseUrl() + resourceCustomers,entity, CustomerResponse.class);
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }

    @Async
    public CompletableFuture<AccountResponse> CreateAccountAysnc(String customerIdentifier, AccountRequest accountRequest) throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            String url = String.format("%s/%s/%s/%s",this.getBaseUrl(),resourceCustomers, customerIdentifier, "accounts");
            HttpEntity<AccountRequest> entity = new HttpEntity<AccountRequest>(accountRequest, headers);
            ResponseEntity<AccountResponse> responseEntity = restTemplate.postForEntity( url ,entity, AccountResponse.class);
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }


    public CompletableFuture<List<AccountResponse>> getAccountsByCustomer(String customerIdentifier) throws Exception {
        try{
            HttpHeaders headers = this.createHeaders();
            String url = String.format("%s/%s/%s/%s",this.getBaseUrl(),resourceCustomers, customerIdentifier, "accounts");
            HttpEntity<String> entity = new HttpEntity<String>("", headers);
            ResponseEntity<List<AccountResponse>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, new ParameterizedTypeReference<List<AccountResponse>>(){});
            return CompletableFuture.completedFuture(responseEntity.getBody());
        } catch(HttpStatusCodeException e){
            throw new Exception(e.getResponseBodyAsString());
        }
    }
}
