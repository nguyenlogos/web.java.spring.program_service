package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.OrderTransaction;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.OrderTransactionRepository;
import aduro.basic.programservice.service.dto.OrderTransactionCriteria;
import aduro.basic.programservice.service.dto.OrderTransactionDTO;
import aduro.basic.programservice.service.mapper.OrderTransactionMapper;

/**
 * Service for executing complex queries for {@link OrderTransaction} entities in the database.
 * The main input is a {@link OrderTransactionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrderTransactionDTO} or a {@link Page} of {@link OrderTransactionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrderTransactionQueryService extends QueryService<OrderTransaction> {

    private final Logger log = LoggerFactory.getLogger(OrderTransactionQueryService.class);

    private final OrderTransactionRepository orderTransactionRepository;

    private final OrderTransactionMapper orderTransactionMapper;

    public OrderTransactionQueryService(OrderTransactionRepository orderTransactionRepository, OrderTransactionMapper orderTransactionMapper) {
        this.orderTransactionRepository = orderTransactionRepository;
        this.orderTransactionMapper = orderTransactionMapper;
    }

    /**
     * Return a {@link List} of {@link OrderTransactionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrderTransactionDTO> findByCriteria(OrderTransactionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OrderTransaction> specification = createSpecification(criteria);
        return orderTransactionMapper.toDto(orderTransactionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OrderTransactionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrderTransactionDTO> findByCriteria(OrderTransactionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OrderTransaction> specification = createSpecification(criteria);
        return orderTransactionRepository.findAll(specification, page)
            .map(orderTransactionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrderTransactionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OrderTransaction> specification = createSpecification(criteria);
        return orderTransactionRepository.count(specification);
    }

    /**
     * Function to convert OrderTransactionCriteria to a {@link Specification}.
     */
    private Specification<OrderTransaction> createSpecification(OrderTransactionCriteria criteria) {
        Specification<OrderTransaction> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), OrderTransaction_.id));
            }
            if (criteria.getProgramName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProgramName(), OrderTransaction_.programName));
            }
            if (criteria.getClientName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientName(), OrderTransaction_.clientName));
            }
            if (criteria.getParticipantName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParticipantName(), OrderTransaction_.participantName));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), OrderTransaction_.createdDate));
            }
            if (criteria.getMessage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMessage(), OrderTransaction_.message));
            }
            if (criteria.getJsonContent() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJsonContent(), OrderTransaction_.jsonContent));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), OrderTransaction_.email));
            }
            if (criteria.getExternalOrderId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExternalOrderId(), OrderTransaction_.externalOrderId));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramId(), OrderTransaction_.programId));
            }
            if (criteria.getUserRewardId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserRewardId(), OrderTransaction_.userRewardId));
            }
        }
        return specification;
    }
}
