package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ClientProgramRewardSettingDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ClientProgramRewardSetting}.
 */
public interface ClientProgramRewardSettingService {

    /**
     * Save a clientProgramRewardSetting.
     *
     * @param clientProgramRewardSettingDTO the entity to save.
     * @return the persisted entity.
     */
    ClientProgramRewardSettingDTO save(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO);

    /**
     * Get all the clientProgramRewardSettings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClientProgramRewardSettingDTO> findAll(Pageable pageable);


    /**
     * Get the "id" clientProgramRewardSetting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClientProgramRewardSettingDTO> findOne(Long id) throws Exception;

    /**
     * Delete the "id" clientProgramRewardSetting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    ClientProgramRewardSettingDTO generateAccessToken(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception;

    ClientProgramRewardSettingDTO createTremendousTeam(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception;

    ClientProgramRewardSettingDTO createTangoAccount(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception;

    /* ClientProgramRewardSettingDTO getTremendousTeam(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception;*/

}
