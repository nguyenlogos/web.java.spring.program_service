package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ClientSubDomain;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ClientSubDomainRepository;
import aduro.basic.programservice.service.dto.ClientSubDomainCriteria;
import aduro.basic.programservice.service.dto.ClientSubDomainDTO;
import aduro.basic.programservice.service.mapper.ClientSubDomainMapper;

/**
 * Service for executing complex queries for {@link ClientSubDomain} entities in the database.
 * The main input is a {@link ClientSubDomainCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientSubDomainDTO} or a {@link Page} of {@link ClientSubDomainDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientSubDomainQueryService extends QueryService<ClientSubDomain> {

    private final Logger log = LoggerFactory.getLogger(ClientSubDomainQueryService.class);

    private final ClientSubDomainRepository clientSubDomainRepository;

    private final ClientSubDomainMapper clientSubDomainMapper;

    public ClientSubDomainQueryService(ClientSubDomainRepository clientSubDomainRepository, ClientSubDomainMapper clientSubDomainMapper) {
        this.clientSubDomainRepository = clientSubDomainRepository;
        this.clientSubDomainMapper = clientSubDomainMapper;
    }

    /**
     * Return a {@link List} of {@link ClientSubDomainDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientSubDomainDTO> findByCriteria(ClientSubDomainCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ClientSubDomain> specification = createSpecification(criteria);
        return clientSubDomainMapper.toDto(clientSubDomainRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientSubDomainDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientSubDomainDTO> findByCriteria(ClientSubDomainCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ClientSubDomain> specification = createSpecification(criteria);
        return clientSubDomainRepository.findAll(specification, page)
            .map(clientSubDomainMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ClientSubDomainCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ClientSubDomain> specification = createSpecification(criteria);
        return clientSubDomainRepository.count(specification);
    }

    /**
     * Function to convert ClientSubDomainCriteria to a {@link Specification}.
     */
    private Specification<ClientSubDomain> createSpecification(ClientSubDomainCriteria criteria) {
        Specification<ClientSubDomain> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientSubDomain_.id));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), ClientSubDomain_.clientId));
            }
            if (criteria.getSubDomainUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubDomainUrl(), ClientSubDomain_.subDomainUrl));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), ClientSubDomain_.createdAt));
            }
            if (criteria.getModifiedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedAt(), ClientSubDomain_.modifiedAt));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramId(), ClientSubDomain_.programId));
            }
            if (criteria.getProgramName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProgramName(), ClientSubDomain_.programName));
            }
        }
        return specification;
    }
}
