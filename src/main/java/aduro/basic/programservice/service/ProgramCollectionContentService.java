package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCollectionContentDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramCollectionContent}.
 */
public interface ProgramCollectionContentService {

    /**
     * Save a programCollectionContent.
     *
     * @param programCollectionContentDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramCollectionContentDTO save(ProgramCollectionContentDTO programCollectionContentDTO);

    /**
     * Get all the programCollectionContents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramCollectionContentDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programCollectionContent.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCollectionContentDTO> findOne(Long id);

    /**
     * Delete the "id" programCollectionContent.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
