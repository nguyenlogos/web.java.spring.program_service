package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.WellmetricEventQueueDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.WellmetricEventQueue}.
 */
public interface WellmetricEventQueueService {

    /**
     * Save a wellmetricEventQueue.
     *
     * @param wellmetricEventQueueDTO the entity to save.
     * @return the persisted entity.
     */
    WellmetricEventQueueDTO save(WellmetricEventQueueDTO wellmetricEventQueueDTO);

    /**
     * Get all the wellmetricEventQueues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WellmetricEventQueueDTO> findAll(Pageable pageable);


    /**
     * Get the "id" wellmetricEventQueue.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WellmetricEventQueueDTO> findOne(Long id);

    /**
     * Delete the "id" wellmetricEventQueue.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
