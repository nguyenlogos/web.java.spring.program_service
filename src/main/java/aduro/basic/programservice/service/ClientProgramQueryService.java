package aduro.basic.programservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.criteria.JoinType;

import aduro.basic.programservice.repository.ClientBrandSettingRepository;
import aduro.basic.programservice.service.dto.*;
import io.jsonwebtoken.lang.Collections;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ClientProgramRepository;
import aduro.basic.programservice.service.mapper.ClientProgramMapper;
import org.springframework.util.MultiValueMap;

/**
 * Service for executing complex queries for {@link ClientProgram} entities in the database.
 * The main input is a {@link ClientProgramCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientProgramDTO} or a {@link Page} of {@link ClientProgramDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientProgramQueryService extends QueryService<ClientProgram> {

    private final Logger log = LoggerFactory.getLogger(ClientProgramQueryService.class);

    private static final String PARAM_PROGRAM_STATUS = "programStatus";

    private final ClientProgramRepository clientProgramRepository;

    private final ClientProgramMapper clientProgramMapper;

    private final ClientBrandSettingRepository clientBrandSettingRepository;

    public ClientProgramQueryService(ClientProgramRepository clientProgramRepository, ClientProgramMapper clientProgramMapper, ClientBrandSettingRepository clientBrandSettingRepository) {
        this.clientProgramRepository = clientProgramRepository;
        this.clientProgramMapper = clientProgramMapper;
        this.clientBrandSettingRepository = clientBrandSettingRepository;
    }

    /**
     * Return a {@link List} of {@link ClientProgramDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientProgramDTO> findByCriteria(ClientProgramCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ClientProgram> specification = createSpecification(criteria);
        return clientProgramMapper.toDto(clientProgramRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientProgramDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientProgramDTO> findByCriteria(ClientProgramCriteria criteria, Pageable page, MultiValueMap<String, String> queryParams) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ClientProgram> specification = createSpecification(criteria);
        if (queryParams != null && queryParams.containsKey(PARAM_PROGRAM_STATUS)) {
            List<ClientProgramDTO> clientProgramDTOList = new ArrayList<>();
            int pageSize = page.getPageSize();
            if (page != null && page.getPageSize() == 0) {
                pageSize = Integer.parseInt(String.valueOf(clientProgramRepository.count()));
            }

            // TODO need to check the paging
            Pageable allPages = PageRequest.of(0, pageSize);
            List<ClientProgram> clientProgramList = clientProgramRepository.findAll().stream()
                .filter(clientProgram -> clientProgram.getProgram() != null &&
                    clientProgram.getProgram().getStatus() != null &&
                    clientProgram.getProgram().getStatus().equalsIgnoreCase(queryParams.getFirst(PARAM_PROGRAM_STATUS)))
                .collect(Collectors.toList());


            if (!Collections.isEmpty(clientProgramList)) {
                clientProgramList.forEach(clientProgram -> {
                    Program program = clientProgram.getProgram();
                    ClientProgramDTO clientProgramDTO = clientProgramMapper.toDto(clientProgram);
                    clientProgramDTO.setLogoUrl(program.getLogoUrl());
                    clientProgramDTO.setStartDate(program.getStartDate());
                    clientProgramDTO.setStartTimeZone(program.getStartTimeZone());
                    clientProgramDTO.setEndDate(program.getResetDate());
                    clientProgramDTO.setEndTimeZone(program.getEndTimeZone());
                    clientProgramDTOList.add(clientProgramDTO);
                });
            }
            return new PageImpl<>(clientProgramDTOList, allPages, clientProgramDTOList.size());
        }
        return clientProgramRepository.findAll(specification, page)
            .map(clientProgramMapper::toDto);
    }

    @Transactional(readOnly = true)
    public Page<ClientProgramDTO> findByCriteria(ClientProgramCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ClientProgram> specification = createSpecification(criteria);
        return clientProgramRepository.findAll(specification, page)
            .map(clientProgramMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ClientProgramCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ClientProgram> specification = createSpecification(criteria);
        return clientProgramRepository.count(specification);
    }

    /**
     * Function to convert ClientProgramCriteria to a {@link Specification}.
     */
    private Specification<ClientProgram> createSpecification(ClientProgramCriteria criteria) {
        Specification<ClientProgram> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientProgram_.id));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), ClientProgram_.clientId));
            }
            if (criteria.getClient_name() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClient_name(), ClientProgram_.clientName));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ClientProgram_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }

    @Transactional(readOnly = true)
    public List<CustomClientBrandDTO> filterProgramByClient(List<String> clientIds, String programStatus) {
        log.debug("find Program Brand : {}, page: {}", clientIds, programStatus);

        Seq<ClientProgram> clientPrograms = clientProgramRepository.findDistinctByClientIdInAndProgramStatus(clientIds, programStatus);

        List<CustomClientBrandDTO> res = clientPrograms.toList().map(this::getProgramBrandByClientId)
            .collect(Collectors.toList());

        return res;
    }

    private CustomClientBrandDTO getProgramBrandByClientId (ClientProgram clientProgram) {

        Option<ClientBrandSetting> clientBrandSettingOption = Option.ofOptional(
            clientBrandSettingRepository.findClientBrandSettingByClientId(clientProgram.getClientId())
        );
        ClientBrandSetting clientBrandSetting = clientBrandSettingOption.getOrElse(new ClientBrandSetting());

        Program program = clientProgram.getProgram();
        CustomClientBrandDTO customClientBrandDTO = new CustomClientBrandDTO(
            program.getId(),
            program.getName(),
            program.getLogoUrl(),
            program.getStartDate(),
            program.getResetDate(),
            program.getStartTimeZone(),
            program.getEndTimeZone(),
            clientProgram.getClientId(),
            clientProgram.getClientName(),
            clientBrandSetting.getClientUrl(),
            program.getStatus()
        );

        return customClientBrandDTO;
    }

    @Transactional(readOnly = true)
    public List<ProgramDTO> getProgramForSendEmailToQC(List<String> clientIds) {
        log.debug("find Program for QC Email: {}", clientIds);
        List<ProgramDTO> programDTOs = new ArrayList<>();

        ArrayList<String> statuses = new ArrayList<>();

        statuses.add(ProgramStatus.Active.toString());

        // get qc program of eeach client
        List<ClientProgram> clientPrograms = clientProgramRepository.findByClientIdInAndProgramStatusAndProgramQaVerify(clientIds, ProgramStatus.Active.toString(), false);
        List<String> clientIdsHasActiveProgram = clientPrograms.stream().map(cp -> cp.getClientId()).collect(Collectors.toList());

        // if client has no qc program, get active program
        List<String> clientIdsHasNoActiveProgram = new ArrayList<>(clientIds);
        clientIdsHasNoActiveProgram.removeAll(clientIdsHasActiveProgram);
        List<ClientProgram> clientProgramsActive = clientProgramRepository.findByClientIdInAndProgramStatusAndProgramQaVerify(clientIdsHasNoActiveProgram, ProgramStatus.Active.toString(), true);

        // get active program
        clientPrograms.addAll(clientProgramsActive);
        List<String> clientIdHaveQcProgram = new ArrayList<>();
        for(ClientProgram clientProgram: clientPrograms) {
            Program program = clientProgram.getProgram();
            ProgramDTO programDTO = new ProgramDTO();
            programDTO.setName(program.getName());
            programDTO.setId(program.getId());
            programDTO.setClientId(clientProgram.getClientId());
            programDTO.setLogoUrl(program.getLogoUrl());
            programDTO.setStatus(program.getStatus());
            programDTO.setQaVerify(program.getQaVerify());
            programDTO.setStatusBeforeSendingIfProgramIsQAVerify();
            programDTOs.add(programDTO);
        }

        return programDTOs;

    }
}
