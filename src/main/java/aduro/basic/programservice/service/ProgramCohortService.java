package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCohortDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramCohort}.
 */
public interface ProgramCohortService {

    /**
     * Save a programCohort.
     *
     * @param programCohortDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramCohortDTO save(ProgramCohortDTO programCohortDTO);

    /**
     * Get all the programCohorts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramCohortDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programCohort.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCohortDTO> findOne(Long id);

    /**
     * Delete the "id" programCohort.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
