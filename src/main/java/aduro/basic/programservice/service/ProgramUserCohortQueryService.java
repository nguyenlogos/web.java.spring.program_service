package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramUserCohort;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramUserCohortRepository;
import aduro.basic.programservice.service.dto.ProgramUserCohortCriteria;
import aduro.basic.programservice.service.dto.ProgramUserCohortDTO;
import aduro.basic.programservice.service.mapper.ProgramUserCohortMapper;

/**
 * Service for executing complex queries for {@link ProgramUserCohort} entities in the database.
 * The main input is a {@link ProgramUserCohortCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramUserCohortDTO} or a {@link Page} of {@link ProgramUserCohortDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramUserCohortQueryService extends QueryService<ProgramUserCohort> {

    private final Logger log = LoggerFactory.getLogger(ProgramUserCohortQueryService.class);

    private final ProgramUserCohortRepository programUserCohortRepository;

    private final ProgramUserCohortMapper programUserCohortMapper;

    public ProgramUserCohortQueryService(ProgramUserCohortRepository programUserCohortRepository, ProgramUserCohortMapper programUserCohortMapper) {
        this.programUserCohortRepository = programUserCohortRepository;
        this.programUserCohortMapper = programUserCohortMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramUserCohortDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramUserCohortDTO> findByCriteria(ProgramUserCohortCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramUserCohort> specification = createSpecification(criteria);
        return programUserCohortMapper.toDto(programUserCohortRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramUserCohortDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramUserCohortDTO> findByCriteria(ProgramUserCohortCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramUserCohort> specification = createSpecification(criteria);
        return programUserCohortRepository.findAll(specification, page)
            .map(programUserCohortMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramUserCohortCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramUserCohort> specification = createSpecification(criteria);
        return programUserCohortRepository.count(specification);
    }

    /**
     * Function to convert ProgramUserCohortCriteria to a {@link Specification}.
     */
    private Specification<ProgramUserCohort> createSpecification(ProgramUserCohortCriteria criteria) {
        Specification<ProgramUserCohort> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramUserCohort_.id));
            }
            if (criteria.getProgress() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgress(), ProgramUserCohort_.progress));
            }
            if (criteria.getNumberOfPass() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfPass(), ProgramUserCohort_.numberOfPass));
            }
            if (criteria.getNumberOfFailure() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfFailure(), ProgramUserCohort_.numberOfFailure));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), ProgramUserCohort_.status));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramUserCohort_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramUserCohort_.modifiedDate));
            }
            if (criteria.getProgramUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramUserId(),
                    root -> root.join(ProgramUserCohort_.programUser, JoinType.LEFT).get(ProgramUser_.id)));
            }
            if (criteria.getProgramCohortId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCohortId(),
                    root -> root.join(ProgramUserCohort_.programCohort, JoinType.LEFT).get(ProgramCohort_.id)));
            }
        }
        return specification;
    }
}
