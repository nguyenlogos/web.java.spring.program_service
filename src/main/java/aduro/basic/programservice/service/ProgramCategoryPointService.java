package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCategoryPointDTO;

import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramCategoryPoint}.
 */
public interface ProgramCategoryPointService {

    /**
     * Save a programCategoryPoint.
     *
     * @param programCategoryPointDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramCategoryPointDTO save(ProgramCategoryPointDTO programCategoryPointDTO);

    /**
     * Get all the programCategoryPoints.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramCategoryPointDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programCategoryPoint.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCategoryPointDTO> findOne(Long id);

    /**
     * Delete the "id" programCategoryPoint.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void deletes(List<Long> ids);

    List<ProgramCategoryPointDTO> updateProgramCategoryPoints(Long programId, List<ProgramCategoryPointDTO> programCategoryPointDTOs);

    List<ProgramCategoryPointDTO> addAndRemoveProgramCategoryPoints(Long programId, List<ProgramCategoryPointDTO> programCategoryPointDTOs);

    ProgramCategoryPointDTO getProgramCategoryPointByClientIdAnCategory(String clientId, String categoryCode);

    ProgramCategoryPointDTO getProgramCategoryPointByUser(String clientId, String participantId, String categoryCode, Map<String, String> queryParams);

    ProgramCategoryPointDTO getProgramCategoryPointByProgramId(Long programId, String categoryCode);
}
