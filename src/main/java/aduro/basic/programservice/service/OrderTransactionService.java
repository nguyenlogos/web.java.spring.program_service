package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.OrderTransactionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.OrderTransaction}.
 */
public interface OrderTransactionService {

    /**
     * Save a orderTransaction.
     *
     * @param orderTransactionDTO the entity to save.
     * @return the persisted entity.
     */
    OrderTransactionDTO save(OrderTransactionDTO orderTransactionDTO);

    /**
     * Get all the orderTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrderTransactionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" orderTransaction.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrderTransactionDTO> findOne(Long id);

    /**
     * Delete the "id" orderTransaction.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
