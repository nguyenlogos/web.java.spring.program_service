package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.EConsentDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.EConsent}.
 */
public interface EConsentService {

    /**
     * Save a eConsent.
     *
     * @param eConsentDTO the entity to save.
     * @return the persisted entity.
     */
    EConsentDTO save(EConsentDTO eConsentDTO);

    /**
     * Get all the eConsents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EConsentDTO> findAll(Pageable pageable);


    /**
     * Get the "id" eConsent.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EConsentDTO> findOne(Long id);

    /**
     * Delete the "id" eConsent.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
