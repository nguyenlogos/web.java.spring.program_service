package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramHealthyRange}.
 */
public interface ProgramHealthyRangeService {

    /**
     * Save a programHealthyRange.
     *
     * @param programHealthyRangeDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramHealthyRangeDTO save(ProgramHealthyRangeDTO programHealthyRangeDTO);

    /**
     * Get all the programHealthyRanges.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramHealthyRangeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programHealthyRange.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramHealthyRangeDTO> findOne(Long id);

    /**
     * Delete the "id" programHealthyRange.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
