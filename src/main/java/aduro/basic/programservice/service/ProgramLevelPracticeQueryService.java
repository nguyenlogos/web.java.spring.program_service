package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramLevelPractice;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramLevelPracticeRepository;
import aduro.basic.programservice.service.dto.ProgramLevelPracticeCriteria;
import aduro.basic.programservice.service.dto.ProgramLevelPracticeDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelPracticeMapper;

/**
 * Service for executing complex queries for {@link ProgramLevelPractice} entities in the database.
 * The main input is a {@link ProgramLevelPracticeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramLevelPracticeDTO} or a {@link Page} of {@link ProgramLevelPracticeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramLevelPracticeQueryService extends QueryService<ProgramLevelPractice> {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelPracticeQueryService.class);

    private final ProgramLevelPracticeRepository programLevelPracticeRepository;

    private final ProgramLevelPracticeMapper programLevelPracticeMapper;

    public ProgramLevelPracticeQueryService(ProgramLevelPracticeRepository programLevelPracticeRepository, ProgramLevelPracticeMapper programLevelPracticeMapper) {
        this.programLevelPracticeRepository = programLevelPracticeRepository;
        this.programLevelPracticeMapper = programLevelPracticeMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramLevelPracticeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramLevelPracticeDTO> findByCriteria(ProgramLevelPracticeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramLevelPractice> specification = createSpecification(criteria);
        return programLevelPracticeMapper.toDto(programLevelPracticeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramLevelPracticeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramLevelPracticeDTO> findByCriteria(ProgramLevelPracticeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramLevelPractice> specification = createSpecification(criteria);
        return programLevelPracticeRepository.findAll(specification, page)
            .map(programLevelPracticeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramLevelPracticeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramLevelPractice> specification = createSpecification(criteria);
        return programLevelPracticeRepository.count(specification);
    }

    /**
     * Function to convert ProgramLevelPracticeCriteria to a {@link Specification}.
     */
    private Specification<ProgramLevelPractice> createSpecification(ProgramLevelPracticeCriteria criteria) {
        Specification<ProgramLevelPractice> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramLevelPractice_.id));
            }
            if (criteria.getPracticeId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPracticeId(), ProgramLevelPractice_.practiceId));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ProgramLevelPractice_.name));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), ProgramLevelPractice_.subgroupId));
            }
            if (criteria.getSubgroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupName(), ProgramLevelPractice_.subgroupName));
            }
            if (criteria.getProgramLevelId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramLevelId(),
                    root -> root.join(ProgramLevelPractice_.programLevel, JoinType.LEFT).get(ProgramLevel_.id)));
            }
        }
        return specification;
    }
}
