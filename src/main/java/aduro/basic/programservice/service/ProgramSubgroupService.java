package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramSubgroupDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramSubgroup}.
 */
public interface ProgramSubgroupService {

    /**
     * Save a programSubgroup.
     *
     * @param programSubgroupDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramSubgroupDTO save(ProgramSubgroupDTO programSubgroupDTO);

    /**
     * Get all the programSubgroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramSubgroupDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programSubgroup.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramSubgroupDTO> findOne(Long id);

    /**
     * Delete the "id" programSubgroup.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
