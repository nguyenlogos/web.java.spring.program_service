package aduro.basic.programservice.service.supportability;

import aduro.basic.programservice.adp.repository.AmpUserActivityIncentiveRepository;
import aduro.basic.programservice.adp.repository.AmpUserActivityProgressHistoryRepository;
import aduro.basic.programservice.adp.repository.AmpUserActivityProgressRepository;
import aduro.basic.programservice.adp.service.AmpUserActivityProgressService;
import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.helpers.APITracker;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.ProgramUserLevelProgressRepositoryExtension;
import aduro.basic.programservice.service.businessrule.ActivityIncentiveRuleImpl;
import aduro.basic.programservice.service.businessrule.ActivityProcessingRule;
import aduro.basic.programservice.service.businessrule.UserIncentiveBusinessRule;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.ProgramUserMapper;
import aduro.basic.programservice.service.mapper.UserEventMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.ap.PathAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

@Service
@Transactional
public class SProgramServiceImpl implements SProgramService {

    private final Logger log = LoggerFactory.getLogger(SProgramServiceImpl.class);

    public static final String ERR_MISS_PARTICIPANT_ID = "ERR_MISS_PARTICIPANT_ID";
    public static final String ERR_MISS_CLIENT_ID = "ERR_MISS_CLIENT_ID";
    public static final String NOT_FOUND_ACTIVITY = "NOT_FOUND_ACTIVITY";
    public static final String SUPPORTABILITY_NOT_SUPPORT_YET = "SUPPORTABILITY_NOT_SUPPORT_YET";
    private static final String ACTIVITY_NOT_SUPPORT_YET = "ACTIVITY_NOT_SUPPORT_YET";

    public static final String SERVER_RESPONSE_ERROR = "SERVER_RESPONSE_ERROR";
    public static final String CLIENT_NOT_NULL = "CLIENT_NOT_NULL";
    public static final String PARTICIPANT_NOT_NULL = "PARTICIPANT_NOT_NULL";
    public static final String PROGRAM_NOT_NULL = "PROGRAM_NOT_NULL";
    public static final String USER_NOT_JOINED_PROGRAM = "USER_NOT_JOINED_PROGRAM";
    public static final String ACTIVITY = "ACTIVITY";
    public static final String PATH = "PATH";
    public static final String RAS = "RAS";

    @Autowired
    ProgramDao programDao;

    @Autowired
    ProgramUserMapper programUserMapper;

    @Autowired
    ClientProgramRepository clientProgramRepository;

    @Autowired
    ProgramRepository programRepository;

    @Autowired
    CustomActivityAdapter customActivityAdapter;

    @Autowired
    APITracker apiTracker;

    @Autowired
    UserEventRepository userEventRepository;

    @Autowired
    UserEventMapper userEventMapper;

    @Autowired
    ProgramUserRepository programUserRepository;

    @Autowired
    ActivityIncentiveRuleImpl activityIncentiveRule;

    @Autowired
    AmpUserActivityProgressRepository ampUserActivityProgressRepository;

    @Autowired
    AmpUserActivityProgressService ampUserActivityProgressService;

    @Autowired
    AmpUserActivityProgressHistoryRepository ampUserActivityProgressHistoryRepository;

    @Autowired
    AmpUserActivityIncentiveRepository ampUserActivityIncentiveRepository;

    @Autowired
    ActivityProcessingRule activityProcessingRule;

    @Autowired
    UserIncentiveBusinessRule userIncentiveBusinessRule;

    @Autowired
    ProgramUserLevelProgressRepositoryExtension programUserLevelProgressRepositoryExtension;

    @Autowired
    UserRewardRepository userRewardRepository;

    @Autowired
    PathAdapter pathAdapter;

    @Override
    public ProgramUserInfoDto getActiveProgramUserByClientIdAndParticipantId(String clientId, String participantId) {
        if (isNullOrEmpty(participantId)) {
            throw new BadRequestAlertException("Missing participantId or aduroId", "Program", ERR_MISS_PARTICIPANT_ID);
        }

        if (isNullOrEmpty(clientId)) {
            throw new BadRequestAlertException("Missing clientId or employerId", "Program", ERR_MISS_CLIENT_ID);
        }

        UserDto user = null;
        try {
            user = customActivityAdapter.getUser(participantId);
        } catch (Exception e) {
            throw new BadRequestAlertException("Missing participantId or aduroId 1", "Program", "getActiveProgramUserByClientIdAndParticipantId");
        }

        Optional<ClientProgram> clientProgramOptional = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.name(), user.isAsProgramTester());

        if (clientProgramOptional.isPresent()) {
            ProgramUser programUser = programDao.getActiveProgramUserByClientIdAndParticipantIdAndStatus(clientId, participantId, "Active");
            Program activeProgram = clientProgramOptional.get().getProgram();
            if (programUser != null) {
                ProgramUserInfoDto dto = new ProgramUserInfoDto();
                dto.setStartDate(activeProgram.getStartDate());
                dto.setId(programUser.getId());
                dto.setClientId(programUser.getClientId());
                dto.setClientName(programUser.getClientName());
                dto.setCurrentLevel(programUser.getCurrentLevel());
                dto.setEmail(programUser.getEmail());
                dto.setFirstName(programUser.getFirstName());
                dto.setLastName(programUser.getLastName());
                dto.setIsTobaccoUser(programUser.isIsTobaccoUser());
                dto.setParticipantId(programUser.getParticipantId());
                dto.setPhone(programUser.getPhone());
                dto.setProgramId(programUser.getProgramId());
                dto.setSubgroupId(programUser.getSubgroupId());
                dto.setSubgroupName(programUser.getSubgroupName());
                dto.setTotalUserPoint(programUser.getTotalUserPoint());

                dto.setTobaccoAttestationDeadline(activeProgram.getTobaccoAttestationDeadline());
                dto.setTobaccoProgramDeadline(activeProgram.getTobaccoProgramDeadline());
                dto.setBiometricLookbackDate(activeProgram.getBiometricLookbackDate());
                dto.setBiometricDeadline(activeProgram.getBiometricDeadlineDate());
                dto.setProgramName(activeProgram.getName());
                dto.setProgramEndDate(activeProgram.getResetDate());
                dto.setStartTimeZone(activeProgram.getStartTimeZone());
                return dto;
            }
        }
        return null;
    }

    @Override
    public APIResponse<ListCustomActivityResponseDto> getCustomActivityByActiveProgramId(String clientId, String participantId, long limit, long offset, String searchTitle) throws Exception {
        UserDto user = null;
        try {
            user = customActivityAdapter.getUser(participantId);
        } catch (Exception e) {
            throw new BadRequestAlertException("Participant is Not Found", "ActivityEventDTO", "getCustomActivityByActiveProgramId");
        }
        Optional<ClientProgram> clientProgramOptional = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.name(), user.isAsProgramTester());
        ProgramUser programUser = null;

        if (clientProgramOptional.isPresent()) {
            programUser = programDao.getActiveProgramUserByClientIdAndParticipantIdAndStatus(clientId, participantId, ProgramStatus.Active.name());
        }

        if (programUser == null) {
            APIResponse<ListCustomActivityResponseDto> responseDto = new APIResponse<ListCustomActivityResponseDto>();
            responseDto.setHttpStatus(HttpStatus.OK);
            responseDto.setMessage(Constants.ERR_USER_NOT_FOUND);
            ListCustomActivityResponseDto dto = new ListCustomActivityResponseDto();
            dto.setModels(new ArrayList<>());
            dto.setTotalCount(0);
            responseDto.setResult(dto);
            return responseDto;
        }

        APIResponse<ListCustomActivityResponseDto> responseDto = customActivityAdapter.getCustomActivityByActiveProgram(clientId, participantId, programUser.getSubgroupId(), limit, offset, searchTitle);
        String requestUrl = String.format("clientId=%s&participantId=%s&subgroupId=%s&limit=%d&offset=%d&searchTitle=%s", clientId, participantId, programUser.getSubgroupId(), limit, offset, searchTitle);

        if (responseDto.getResult() != null) {
            apiTracker.trackIntegrationService(null, responseDto.getHttpStatus().toString(), requestUrl, Constants.MONARCH, requestUrl);
        } else {
            apiTracker.trackIntegrationService(responseDto.getErrors().get(0), responseDto.getHttpStatus().toString(), requestUrl, Constants.MONARCH, requestUrl);
        }
        return responseDto;
    }

    @Override
    public APIResponse<ActivityUpdatePointResponseDto> getUserEventByEventId(String participantId, String clientId, Integer activityId, long programId) {

        log.debug("Service: fetching events by Admin");

        APIResponse<ActivityUpdatePointResponseDto> result = new APIResponse<>();

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(participantId, programId);
        if (!programUserOptional.isPresent()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Not Found Participant");
            result.getErrors().add(Constants.ERR_USER_NOT_FOUND);
            return result;
        }

        UserDto userAMP = activityProcessingRule.getUserAMP(participantId);

        if (userAMP == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Not Found Participant");
            result.getErrors().add(Constants.ERR_USER_NOT_FOUND);
            return result;
        }

        CustomActivityDTO customActivity = activityProcessingRule.getCustomActivity(activityId);
        if (customActivity == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Not Found Activity");
            result.getErrors().add(NOT_FOUND_ACTIVITY);
            return result;
        }

        SupportableActivityType type = SupportableActivityType.fromString(customActivity.getType());

        if (type == null) {
            type = SupportableActivityType.fromString(customActivity.getSecondaryType());
            if (type == null) {
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                result.getErrors().add(SUPPORTABILITY_NOT_SUPPORT_YET);
                result.setMessage("The type of activity has not support yet.");
                return result;
            }
        }

        switch (type) {
            case INFORMATION:
            case EMPLOYER_VERIFIED:
            case OTHER_ASSESSMENT:
                return activityProcessingRule.fetchingEventProgressForOnceTimeTracking(programUserOptional.get(), customActivity, userAMP.getUserId(), type);
            case PASSIVE_TRACKING:
                // secondary type and quality unit should be one field.
                //adjust unit
                customActivity.setQuantityUnit(customActivity.getSecondaryType());
                return activityProcessingRule.fetchingEventMultipleTracking(participantId, programId, type, customActivity, userAMP);
            case SELF_TRACKING:
                return activityProcessingRule.fetchingEventMultipleTracking(participantId, programId, type, customActivity, userAMP);
        }

        result.setHttpStatus(HttpStatus.BAD_REQUEST);
        result.getErrors().add(SUPPORTABILITY_NOT_SUPPORT_YET);
        result.setMessage("The type of activity has not support yet");
        return result;
    }

    @Override
    @Transactional
    public APIResponse<ProcessIncentiveActivityResponse> processIncentiveActivity(ProcessIncentiveActivityDto dto) throws Exception {

        log.debug("Service: Admin Processing Activity.");

        // processing by activity Type
        APIResponse<ProcessIncentiveActivityResponse> result = new APIResponse<>();

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(dto.getParticipantId(), dto.getProgramId());
        if (!programUserOptional.isPresent()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(Constants.ERR_USER_NOT_FOUND);
            result.setMessage("Participant Not Found");
            return result;
        }

        ProgramUser programUser = programUserOptional.get();

        List<UserEvent> userEvents = userEventRepository.findByProgramUserIdAndEventIdOrderByEventDateAsc(programUser.getId(), dto.getActivityId()).collect(Collectors.toList());

        CustomActivityDTO customActivity = activityProcessingRule.getCustomActivity(Long.parseLong(dto.getActivityId()));
        if (customActivity == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Not Found Activity");
            result.getErrors().add(NOT_FOUND_ACTIVITY);
            return result;
        }

        SupportableActivityType type = SupportableActivityType.fromString(customActivity.getType());

        if (type == null) {
            type = SupportableActivityType.fromString(customActivity.getSecondaryType());
            if (type == null) {
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                result.getErrors().add(SUPPORTABILITY_NOT_SUPPORT_YET);
                result.setMessage("The type of activity has not support yet.");
                return result;
            }
        }

        UserDto userAMP = activityProcessingRule.getUserAMP(dto.getParticipantId());

        if (userAMP == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Not Found Participant");
            result.getErrors().add(Constants.ERR_USER_NOT_FOUND);
            return result;
        }

        switch (type) {
            case INFORMATION:
            case OTHER_ASSESSMENT:
                return activityProcessingRule.processIncentiveOnceTimeTracking(dto, programUser, userEvents, customActivity, type);
            case EMPLOYER_VERIFIED:
                return activityProcessingRule.processIncentiveEmployerVerified(dto, programUser, userEvents, userAMP, customActivity, type);
            case PASSIVE_TRACKING:
                //adjust unit
                customActivity.setQuantityUnit(customActivity.getSecondaryType());
                return activityProcessingRule.processIncentiveMultipleTracing(dto, programUser, type, customActivity, userAMP, result);
            case SELF_TRACKING:
                return activityProcessingRule.processIncentiveMultipleTracing(dto, programUser, type, customActivity, userAMP, result);

        }
        result.setHttpStatus(HttpStatus.BAD_REQUEST);
        result.setMessage("The Activity has not supported yet.");
        result.getErrors().add(ACTIVITY_NOT_SUPPORT_YET);
        return result;
    }

    @Override
    public APIResponse<List<ScProgramLevelDataDto>> getLevelInfoByParticipantIdAndClientIdAndProgramId(String clientId, String participantId, Long programId) {

        APIResponse<List<ScProgramLevelDataDto>> response = new APIResponse();
        try {
            //validate
            if (isNullOrEmpty(clientId)) {
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
                response.setMessage("Client can not be null.");
                response.getErrors().add(CLIENT_NOT_NULL);
                return response;
            }

            if (isNullOrEmpty(participantId)) {
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
                response.setMessage("ParticipantId can not be null.");
                response.getErrors().add(PARTICIPANT_NOT_NULL);
                return response;
            }

            if (programId == 0) {
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
                response.setMessage("ProgramID can not be null.");
                response.getErrors().add(PROGRAM_NOT_NULL);
                return response;
            }

            List<ScProgramLevelDataDto> result = new ArrayList<>();

            Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndClientIdAndProgramId(participantId, clientId, programId);
            if (!programUserOptional.isPresent()) {
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
                response.setMessage("User not joined program yet.");
                response.getErrors().add(USER_NOT_JOINED_PROGRAM);
                return response;
            }

            ProgramUser programUser = programUserOptional.get();

            Optional<Program> programOptional = programRepository.findById(programId);
            if (!programOptional.isPresent()) {
                response.setHttpStatus(HttpStatus.BAD_REQUEST);
                response.setMessage("Program can not be null.");
                response.getErrors().add(PROGRAM_NOT_NULL);
                return response;
            }

            Program program = programOptional.get();

            for (ProgramLevel programLevel : program.getProgramLevels()) {

                ScProgramLevelDataDto programLevelDataDto = ScProgramLevelDataDto.builder()
                    .levelOrder(programLevel.getLevelOrder())
                    .levelName(programLevel.getName())
                    .description(programLevel.getDescription())
                    .pointRequired(programLevel.getEndPoint())
                    .build();

                // get level completed date
                Instant completedDate = null;
                String levelStatus = "Not Started";

                if (programUser.getCurrentLevel().equals(programLevel.getLevelOrder())) {
                    levelStatus = "In Progress";
                }

                if (programLevel.getLevelOrder().compareTo(programUser.getCurrentLevel()) < 0) {
                    levelStatus = "Completed";
                }

                /// get progress complete and reward level
                List<ProgramUserLevelProgress> progressUsers = programUserLevelProgressRepositoryExtension.findByProgramUserIdAndLevelFrom(programUser.getId(), programLevel.getLevelOrder());
                if (!progressUsers.isEmpty()) {
                    completedDate = progressUsers.get(0).getLevelUpAt();

                }

                List<ScProgramLevelRewardDto> rewards = new ArrayList<>();

                if (!programLevel.getProgramLevelRewards().isEmpty()) {

                    List<ProgramLevelReward> expectedRewards = userIncentiveBusinessRule.getProgramLevelRewardsBySubgroup(programLevel, programUser.getSubgroupId());

                    for (ProgramLevelReward expectedReward : expectedRewards) {
                        List<UserReward> archiveLevel = userRewardRepository.findByProgramUserIdAndProgramLevelAndRewardType(programUser.getId(), programLevel.getLevelOrder(), expectedReward.getRewardType());

                        ScProgramLevelRewardDto rewardDto = ScProgramLevelRewardDto.builder()
                            .programLevelRewardId(expectedReward.getId())
                            .rewardCode(expectedReward.getCode())
                            .rewardStatus("Not Archived")
                            .build();

                        if (!archiveLevel.isEmpty()) {
                            UserReward userReward = archiveLevel.get(0);
                            rewardDto.setRewardStatus(userReward.getStatus().equalsIgnoreCase(IncentiveStatusEnum.Completed.name()) ? "Archived" : "Not Archived");
                            rewardDto.setRewardCompletedDate(userReward.getLevelCompletedDate());
                        }

                        rewards.add(rewardDto);

                    }

                }

                programLevelDataDto.setRewardDtoList(rewards);

                // get required items;
                List<ProgramLevelActivity> programLevelActivities = userIncentiveBusinessRule.getRequiredActivityBySubgroup(programLevel, programUser.getSubgroupId());

                List<ProgramLevelPath> requiredPathBySubgroup = userIncentiveBusinessRule.getRequiredPathBySubgroup(programLevel, programUser.getSubgroupId());

                List<ProgramRequirementItems> requiredRasItems = userIncentiveBusinessRule.getRequiredItemsBySubgroup(programLevel, programUser.getSubgroupId());

                List<ScProgramLevelRequiredItemDto> totalItems = new ArrayList<>();

                List<ScProgramLevelRequiredItemDto> items = programLevelActivities.stream()
                    .map(p -> ScProgramLevelRequiredItemDto.builder()
                        .title(p.getActivityCode())
                        .itemId(p.getActivityId())
                        .type(ACTIVITY)
                        .build()).collect(Collectors.toList());

                List<ScProgramLevelRequiredItemDto> requiredPaths = requiredPathBySubgroup.stream()
                    .map(p -> ScProgramLevelRequiredItemDto.builder()
                        .title(p.getName())
                        .itemId(p.getPathId())
                        .type(PATH)
                        .build()).collect(Collectors.toList());

                List<ScProgramLevelRequiredItemDto> requiredItems = requiredRasItems.stream()
                    .map(p -> ScProgramLevelRequiredItemDto.builder()
                        .title(p.getNameOfItem())
                        .itemId(p.getItemId())
                        .type(RAS)
                        .build()).collect(Collectors.toList());

                totalItems.addAll(items);
                totalItems.addAll(requiredPaths);
                totalItems.addAll(requiredItems);

                programLevelDataDto.setLevelCompletedDate(completedDate);
                programLevelDataDto.setLevelStatus(levelStatus);
                programLevelDataDto.setRequiredItemList(totalItems);
                result.add(programLevelDataDto);

            }

            response.setHttpStatus(HttpStatus.OK);
            response.setMessage("Success");
            response.setResult(result);
            return response;
        } catch (Exception e) {
            log.info("Request get level info: {}", e.getMessage());
            response.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            response.setMessage("Something went wrong.");
            response.getErrors().add(SERVER_RESPONSE_ERROR);
            return response;
        }

    }

    @Override
    public APIResponse<ListProgramPathsResponseDto> getPathsByClientIdAndParticipantId(String clientId, String participantId) throws Exception {
        if (isNullOrEmpty(participantId)) {
            throw new BadRequestAlertException("Missing participantId or aduroId", "Program", ERR_MISS_PARTICIPANT_ID);
        }

        if (isNullOrEmpty(clientId)) {
            throw new BadRequestAlertException("Missing clientId or employerId", "Program", ERR_MISS_CLIENT_ID);
        }

        UserDto user = null;
        try {
            user = customActivityAdapter.getUser(participantId);
        } catch (Exception e) {
            throw new BadRequestAlertException("Participant is Not Found", "ActivityEventDTO", "getCustomPathsByActiveProgramId");
        }

        APIResponse<ListProgramPathsResponseDto> responseDto = pathAdapter.getPathsByClientIdAndParticipantId(clientId, participantId);

        return responseDto;
    }
}
