package aduro.basic.programservice.service.supportability;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.dto.APIResponse;
import aduro.basic.programservice.service.dto.ListCustomActivityResponseDto;
import aduro.basic.programservice.service.dto.ProgramUserInfoDto;

import java.util.List;


public interface SProgramService {
    ProgramUserInfoDto getActiveProgramUserByClientIdAndParticipantId(String clientId, String participantId);
    APIResponse<ListCustomActivityResponseDto> getCustomActivityByActiveProgramId(String clientId, String participantId, long limit, long offset, String searchTitle) throws Exception;
    APIResponse<ActivityUpdatePointResponseDto> getUserEventByEventId(String participantId, String clientId, Integer activityId, long programId);
    APIResponse<ProcessIncentiveActivityResponse> processIncentiveActivity(ProcessIncentiveActivityDto dto) throws Exception;
    APIResponse<List<ScProgramLevelDataDto>> getLevelInfoByParticipantIdAndClientIdAndProgramId(String clientId, String participantId, Long programId) throws Exception;
    APIResponse<ListProgramPathsResponseDto> getPathsByClientIdAndParticipantId(String clientId, String participantId) throws Exception;
}
