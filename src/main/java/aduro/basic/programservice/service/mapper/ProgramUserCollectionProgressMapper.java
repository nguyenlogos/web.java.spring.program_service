package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramUserCollectionProgress} and its DTO {@link ProgramUserCollectionProgressDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramUserCohortMapper.class})
public interface ProgramUserCollectionProgressMapper extends EntityMapper<ProgramUserCollectionProgressDTO, ProgramUserCollectionProgress> {

    @Mapping(source = "programUserCohort.id", target = "programUserCohortId")
    ProgramUserCollectionProgressDTO toDto(ProgramUserCollectionProgress programUserCollectionProgress);

    @Mapping(source = "programUserCohortId", target = "programUserCohort")
    ProgramUserCollectionProgress toEntity(ProgramUserCollectionProgressDTO programUserCollectionProgressDTO);

    default ProgramUserCollectionProgress fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramUserCollectionProgress programUserCollectionProgress = new ProgramUserCollectionProgress();
        programUserCollectionProgress.setId(id);
        return programUserCollectionProgress;
    }
}
