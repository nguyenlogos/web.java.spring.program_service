package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramTrackingStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramTrackingStatus} and its DTO {@link ProgramTrackingStatusDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramTrackingStatusMapper extends EntityMapper<ProgramTrackingStatusDTO, ProgramTrackingStatus> {



    default ProgramTrackingStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramTrackingStatus programTrackingStatus = new ProgramTrackingStatus();
        programTrackingStatus.setId(id);
        return programTrackingStatus;
    }
}
