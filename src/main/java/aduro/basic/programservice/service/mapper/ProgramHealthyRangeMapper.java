package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramHealthyRange} and its DTO {@link ProgramHealthyRangeDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ProgramHealthyRangeMapper extends EntityMapper<ProgramHealthyRangeDTO, ProgramHealthyRange> {

    @Mapping(source = "program.id", target = "programId")
    ProgramHealthyRangeDTO toDto(ProgramHealthyRange programHealthyRange);

    @Mapping(source = "programId", target = "program")
    ProgramHealthyRange toEntity(ProgramHealthyRangeDTO programHealthyRangeDTO);

    default ProgramHealthyRange fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramHealthyRange programHealthyRange = new ProgramHealthyRange();
        programHealthyRange.setId(id);
        return programHealthyRange;
    }
}
