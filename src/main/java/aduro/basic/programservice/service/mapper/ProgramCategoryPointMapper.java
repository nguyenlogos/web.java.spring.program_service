package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramCategoryPointDTO;

import org.mapstruct.*;

import java.util.Set;

/**
 * Mapper for the entity {@link ProgramCategoryPoint} and its DTO {@link ProgramCategoryPointDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ProgramCategoryPointMapper extends EntityMapper<ProgramCategoryPointDTO, ProgramCategoryPoint> {

   // @Mapping(source = "program.id", target = "programId")
    @Mapping(source = "program.name", target = "programName")
    ProgramCategoryPointDTO toDto(ProgramCategoryPoint programCategoryPoint);



    @Mapping(source = "programId", target = "program")
    @Mapping(target = "programSubCategoryPoints", ignore = true)
    ProgramCategoryPoint toEntity(ProgramCategoryPointDTO programCategoryPointDTO);

    default ProgramCategoryPoint fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramCategoryPoint programCategoryPoint = new ProgramCategoryPoint();
        programCategoryPoint.setId(id);
        return programCategoryPoint;
    }
}
