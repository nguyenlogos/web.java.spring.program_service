package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ClientProgramRewardSettingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ClientProgramRewardSetting} and its DTO {@link ClientProgramRewardSettingDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ClientProgramRewardSettingMapper extends EntityMapper<ClientProgramRewardSettingDTO, ClientProgramRewardSetting> {

    @Mapping(source = "program.id", target = "programId")
    @Mapping(source = "program.name", target = "programName")
    ClientProgramRewardSettingDTO toDto(ClientProgramRewardSetting clientProgramRewardSetting);

    @Mapping(source = "programId", target = "program")
    ClientProgramRewardSetting toEntity(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO);

    default ClientProgramRewardSetting fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientProgramRewardSetting clientProgramRewardSetting = new ClientProgramRewardSetting();
        clientProgramRewardSetting.setId(id);
        return clientProgramRewardSetting;
    }
}
