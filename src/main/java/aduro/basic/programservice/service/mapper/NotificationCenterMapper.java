package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.NotificationCenterDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link NotificationCenter} and its DTO {@link NotificationCenterDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NotificationCenterMapper extends EntityMapper<NotificationCenterDTO, NotificationCenter> {



    default NotificationCenter fromId(Long id) {
        if (id == null) {
            return null;
        }
        NotificationCenter notificationCenter = new NotificationCenter();
        notificationCenter.setId(id);
        return notificationCenter;
    }
}
