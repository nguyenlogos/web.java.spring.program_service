package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramUserCohortDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramUserCohort} and its DTO {@link ProgramUserCohortDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramUserMapper.class, ProgramCohortMapper.class})
public interface ProgramUserCohortMapper extends EntityMapper<ProgramUserCohortDTO, ProgramUserCohort> {

    @Mapping(source = "programUser.id", target = "programUserId")
    @Mapping(source = "programCohort.id", target = "programCohortId")
    ProgramUserCohortDTO toDto(ProgramUserCohort programUserCohort);

    @Mapping(source = "programUserId", target = "programUser")
    @Mapping(source = "programCohortId", target = "programCohort")
    ProgramUserCohort toEntity(ProgramUserCohortDTO programUserCohortDTO);

    default ProgramUserCohort fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramUserCohort programUserCohort = new ProgramUserCohort();
        programUserCohort.setId(id);
        return programUserCohort;
    }
}
