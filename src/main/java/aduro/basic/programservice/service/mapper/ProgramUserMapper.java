package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.service.dto.ProgramUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramUser} and its DTO {@link ProgramUserDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramUserMapper extends EntityMapper<ProgramUserDTO, ProgramUser> {


    @Mapping(target = "userEvents", ignore = true)
    ProgramUserDTO toDto(ProgramUser programUser);

    default ProgramUser fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramUser programUser = new ProgramUser();
        programUser.setId(id);
        return programUser;
    }
}
