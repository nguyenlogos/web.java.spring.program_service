package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramCohortRuleDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramCohortRule} and its DTO {@link ProgramCohortRuleDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramCohortMapper.class})
public interface ProgramCohortRuleMapper extends EntityMapper<ProgramCohortRuleDTO, ProgramCohortRule> {

    @Mapping(source = "programCohort.id", target = "programCohortId")
    ProgramCohortRuleDTO toDto(ProgramCohortRule programCohortRule);

    @Mapping(source = "programCohortId", target = "programCohort")
    ProgramCohortRule toEntity(ProgramCohortRuleDTO programCohortRuleDTO);

    default ProgramCohortRule fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramCohortRule programCohortRule = new ProgramCohortRule();
        programCohortRule.setId(id);
        return programCohortRule;
    }
}
