package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramActivityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramActivity} and its DTO {@link ProgramActivityDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class, ProgramPhaseMapper.class})
public interface ProgramActivityMapper extends EntityMapper<ProgramActivityDTO, ProgramActivity> {

    @Mapping(source = "program.id", target = "programId")
    @Mapping(source = "program.name", target = "programName")
    @Mapping(source = "programPhase.id", target = "programPhaseId")
    @Mapping(source = "programPhase.phaseOrder", target = "programPhasePhaseOrder")
    ProgramActivityDTO toDto(ProgramActivity programActivity);

    @Mapping(source = "programId", target = "program")
    @Mapping(source = "programPhaseId", target = "programPhase")
    ProgramActivity toEntity(ProgramActivityDTO programActivityDTO);

    default ProgramActivity fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramActivity programActivity = new ProgramActivity();
        programActivity.setId(id);
        return programActivity;
    }
}
