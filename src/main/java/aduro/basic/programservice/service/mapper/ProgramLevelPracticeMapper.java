package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramLevelPracticeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramLevelPractice} and its DTO {@link ProgramLevelPracticeDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramLevelMapper.class})
public interface ProgramLevelPracticeMapper extends EntityMapper<ProgramLevelPracticeDTO, ProgramLevelPractice> {

    @Mapping(source = "programLevel.id", target = "programLevelId")
    @Mapping(source = "programLevel.name", target = "programLevelName")
    ProgramLevelPracticeDTO toDto(ProgramLevelPractice programLevelPractice);

    @Mapping(source = "programLevelId", target = "programLevel")
    ProgramLevelPractice toEntity(ProgramLevelPracticeDTO programLevelPracticeDTO);

    default ProgramLevelPractice fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramLevelPractice programLevelPractice = new ProgramLevelPractice();
        programLevelPractice.setId(id);
        return programLevelPractice;
    }
}
