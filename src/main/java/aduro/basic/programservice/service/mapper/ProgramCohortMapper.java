package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramCohortDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramCohort} and its DTO {@link ProgramCohortDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ProgramCohortMapper extends EntityMapper<ProgramCohortDTO, ProgramCohort> {

    @Mapping(source = "program.id", target = "programId")
    ProgramCohortDTO toDto(ProgramCohort programCohort);

    @Mapping(source = "programId", target = "program")
    @Mapping(target = "programCohortRules", ignore = true)
    @Mapping(target = "programCohortCollections", ignore = true)
    ProgramCohort toEntity(ProgramCohortDTO programCohortDTO);

    default ProgramCohort fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramCohort programCohort = new ProgramCohort();
        programCohort.setId(id);
        return programCohort;
    }
}
