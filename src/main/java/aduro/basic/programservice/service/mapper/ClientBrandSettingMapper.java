package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ClientBrandSettingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ClientBrandSetting} and its DTO {@link ClientBrandSettingDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClientBrandSettingMapper extends EntityMapper<ClientBrandSettingDTO, ClientBrandSetting> {


    @Mapping(source = "colorValue", target = "primaryColorValue")
    ClientBrandSetting toEntity(ClientBrandSettingDTO dto);

    @Mapping(source = "primaryColorValue", target = "colorValue")
    ClientBrandSettingDTO toDto(ClientBrandSetting entity);

    default ClientBrandSetting fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientBrandSetting clientBrandSetting = new ClientBrandSetting();
        clientBrandSetting.setId(id);
        return clientBrandSetting;
    }
}
