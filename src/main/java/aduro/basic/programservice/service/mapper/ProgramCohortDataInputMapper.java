package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramCohortDataInputDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramCohortDataInput} and its DTO {@link ProgramCohortDataInputDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramCohortDataInputMapper extends EntityMapper<ProgramCohortDataInputDTO, ProgramCohortDataInput> {



    default ProgramCohortDataInput fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramCohortDataInput programCohortDataInput = new ProgramCohortDataInput();
        programCohortDataInput.setId(id);
        return programCohortDataInput;
    }
}
