package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ClientSettingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ClientSetting} and its DTO {@link ClientSettingDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClientSettingMapper extends EntityMapper<ClientSettingDTO, ClientSetting> {



    default ClientSetting fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientSetting clientSetting = new ClientSetting();
        clientSetting.setId(id);
        return clientSetting;
    }
}
