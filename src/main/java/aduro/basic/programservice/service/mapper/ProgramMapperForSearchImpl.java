package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.service.dto.ClientProgramDTO;
import aduro.basic.programservice.service.dto.ProgramDTO;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Generated;

import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import org.springframework.stereotype.Component;


@Component
public class ProgramMapperForSearchImpl {

    public List<Program> toEntity(List<ProgramDTO> arg0) {
        if ( arg0 == null ) {
            return null;
        }

        List<Program> list = new ArrayList<Program>( arg0.size() );
        for ( ProgramDTO programDTO : arg0 ) {
            list.add( toEntity( programDTO ) );
        }

        return list;
    }

    public List<ProgramDTO> toDto(List<Program> arg0) {
        if ( arg0 == null ) {
            return null;
        }

        List<ProgramDTO> list = new ArrayList<ProgramDTO>( arg0.size() );
        for ( Program program : arg0 ) {
            list.add( toDto( program ) );
        }

        return list;
    }

    public Program toEntity(ProgramDTO programDTO) {
        if ( programDTO == null ) {
            return null;
        }

        Program program = new Program();

        program.isTemplate( programDTO.getIsTemplate() );
        program.isWellMatric( programDTO.isIsWellMatric() );
        program.setPreviewDate( programDTO.getPreviewDate() );
        program.setPreviewTimeZone( programDTO.getPreviewTimeZone() );
        program.setStartTimeZone( programDTO.getStartTimeZone() );
        program.setEndTimeZone( programDTO.getEndTimeZone() );
        program.isCoaching( programDTO.isIsCoaching() );
        program.setLevelStructure( programDTO.getLevelStructure() );
        program.setIsHp( programDTO.isIsHp() );
        program.setIsUseLevel( programDTO.isIsUseLevel() );
        program.setProgramLength( programDTO.getProgramLength() );
        program.setId( programDTO.getId() );
        program.setName( programDTO.getName() );
        program.setStartDate( programDTO.getStartDate() );
        program.setResetDate( programDTO.getResetDate() );
        program.setLastSent( programDTO.getLastSent() );
        program.setIsRetriggerEmail( programDTO.isIsRetriggerEmail() );
        program.setIsEligible( programDTO.isIsEligible() );
        program.setIsSentRegistrationEmail( programDTO.isIsSentRegistrationEmail() );
        program.setIsRegisteredForPlatform( programDTO.isIsRegisteredForPlatform() );
        program.setIsScheduledScreening( programDTO.isIsScheduledScreening() );
        program.setIsFunctionally( programDTO.isIsFunctionally() );
        program.setLogoUrl( programDTO.getLogoUrl() );
        program.setDescription( programDTO.getDescription() );
        program.setUserPoint( programDTO.getUserPoint() );
        program.setLastModifiedDate( programDTO.getLastModifiedDate() );
        program.setCreatedDate( programDTO.getCreatedDate() );
        program.setLastModifiedBy( programDTO.getLastModifiedBy() );
        program.setCreatedBy( programDTO.getCreatedBy() );
        program.setStatus( programDTO.getStatus() );
        program.setIsUsePoint( programDTO.isIsUsePoint() );
        program.setIsScreen( programDTO.isIsScreen() );
        program.setClientPrograms( clientProgramDTOListToClientProgramSet( programDTO.getClientPrograms() ) );
        program.setIsPreview( programDTO.isIsPreview() );
        programDTO.setProgramLevels( programLevelSetToProgramLevelDTOList( program.getProgramLevels() ) );
        program.setExtractable(programDTO.getExtractable());
        program.setTobaccoSurchageManagement(programDTO.getTobaccoSurchargeManagement());
        program.setTobaccoProgramDeadline(programDTO.getTobaccoProgramDeadline());
        program.setTobaccoAttestationDeadline(programDTO.getTobaccoAttestationDeadline());
        program.setProgramType(programDTO.getProgramType());
        return program;
    }

    protected List<ProgramLevelDTO> programLevelSetToProgramLevelDTOList(Set<ProgramLevel> set) {
        if ( set == null ) {
            return null;
        }

        List<ProgramLevelDTO> list = new ArrayList<ProgramLevelDTO>( set.size() );
        for ( ProgramLevel programLevel : set ) {
            list.add( programLevelToProgramLevelDTO( programLevel ) );
        }

        return list;
    }

    protected ProgramLevelDTO programLevelToProgramLevelDTO(ProgramLevel programLevel) {
        if ( programLevel == null ) {
            return null;
        }

        ProgramLevelDTO programLevelDTO = new ProgramLevelDTO();

        programLevelDTO.setId( programLevel.getId() );
        programLevelDTO.setDescription( programLevel.getDescription() );
        programLevelDTO.setStartPoint( programLevel.getStartPoint() );
        programLevelDTO.setEndPoint( programLevel.getEndPoint() );
        programLevelDTO.setLevelOrder( programLevel.getLevelOrder() );
        programLevelDTO.setIconPath( programLevel.getIconPath() );
        programLevelDTO.setName( programLevel.getName() );
        programLevelDTO.setEndDate( programLevel.getEndDate() );
        programLevelDTO.setProgramId( programLevel.getProgramId() );

        return programLevelDTO;
    }

    public ProgramDTO toDto(Program program) {
        if ( program == null ) {
            return null;
        }

        ProgramDTO programDTO = new ProgramDTO();

        programDTO.setLevelStructure( program.getLevelStructure() );
        programDTO.setPreviewDate( program.getPreviewDate() );
        programDTO.setPreviewTimeZone( program.getPreviewTimeZone() );
        programDTO.setStartTimeZone( program.getStartTimeZone() );
        programDTO.setEndTimeZone( program.getEndTimeZone() );
        programDTO.setIsPreview( program.isIsPreview() );
        programDTO.setIsUseLevel( program.isIsUseLevel() );
        programDTO.setId( program.getId() );
        programDTO.setName( program.getName() );
        programDTO.setStartDate( program.getStartDate() );
        programDTO.setResetDate( program.getResetDate() );
        programDTO.setLastSent( program.getLastSent() );
        programDTO.setIsRetriggerEmail( program.isIsRetriggerEmail() );
        programDTO.setIsEligible( program.isIsEligible() );
        programDTO.setIsSentRegistrationEmail( program.isIsSentRegistrationEmail() );
        programDTO.setIsRegisteredForPlatform( program.isIsRegisteredForPlatform() );
        programDTO.setIsScheduledScreening( program.isIsScheduledScreening() );
        programDTO.setIsFunctionally( program.isIsFunctionally() );
        programDTO.setLogoUrl( program.getLogoUrl() );
        programDTO.setDescription( program.getDescription() );
        programDTO.setUserPoint( program.getUserPoint() );
        programDTO.setLastModifiedDate( program.getLastModifiedDate() );
        programDTO.setCreatedDate( program.getCreatedDate() );
        programDTO.setLastModifiedBy( program.getLastModifiedBy() );
        programDTO.setCreatedBy( program.getCreatedBy() );
        programDTO.setStatus( program.getStatus() );
        programDTO.setIsUsePoint( program.isIsUsePoint() );
        programDTO.setIsScreen( program.isIsScreen() );
        programDTO.setIsTemplate( program.getIsTemplate() );
        programDTO.setIsHp( program.isIsHp() );
        programDTO.setIsWellMatric( program.isIsWellMatric() );
        programDTO.setIsCoaching( program.isIsCoaching() );
        programDTO.setClientPrograms( clientProgramSetToClientProgramDTOList( program.getClientPrograms() ) );
        programDTO.setProgramLength( program.getProgramLength() );
        programDTO.setProgramLevels( programLevelSetToProgramLevelDTOList( program.getProgramLevels() ) );
        programDTO.setQaVerify(program.getQaVerify());
        programDTO.setProgramQAStartDate(program.getProgramQAStartDate());
        programDTO.setProgramQAEndDate(program.getProgramQAEndDate());
        programDTO.setExtractable(program.getExtractable());
        programDTO.setTobaccoSurchargeManagement(program.isIsTobaccoSurchargeManagement());
        programDTO.setTobaccoAttestationDeadline(program.getTobaccoAttestationDeadline());
        programDTO.setTobaccoProgramDeadline(program.getTobaccoProgramDeadline());
        programDTO.setProgramType(program.getProgramType());
        return programDTO;
    }

    protected ClientProgram clientProgramDTOToClientProgram(ClientProgramDTO clientProgramDTO) {
        if ( clientProgramDTO == null ) {
            return null;
        }

        ClientProgram clientProgram = new ClientProgram();

        clientProgram.setProgramId( clientProgramDTO.getProgramId() );
        clientProgram.clientName( clientProgramDTO.getClientName() );
        clientProgram.setId( clientProgramDTO.getId() );
        clientProgram.setClientId( clientProgramDTO.getClientId() );

        return clientProgram;
    }

    protected Set<ClientProgram> clientProgramDTOListToClientProgramSet(List<ClientProgramDTO> list) {
        if ( list == null ) {
            return null;
        }

        Set<ClientProgram> set = new HashSet<ClientProgram>( Math.max( (int) ( list.size() / .75f ) + 1, 16 ) );
        for ( ClientProgramDTO clientProgramDTO : list ) {
            set.add( clientProgramDTOToClientProgram( clientProgramDTO ) );
        }

        return set;
    }

    protected ClientProgramDTO clientProgramToClientProgramDTO(ClientProgram clientProgram) {
        if ( clientProgram == null ) {
            return null;
        }

        ClientProgramDTO clientProgramDTO = new ClientProgramDTO();

        clientProgramDTO.setClientName( clientProgram.getClientName() );
        clientProgramDTO.setId( clientProgram.getId() );
        clientProgramDTO.setClientId( clientProgram.getClientId() );
        clientProgramDTO.setProgramId( clientProgram.getProgramId() );

        return clientProgramDTO;
    }

    protected List<ClientProgramDTO> clientProgramSetToClientProgramDTOList(Set<ClientProgram> set) {
        if ( set == null ) {
            return null;
        }

        List<ClientProgramDTO> list = new ArrayList<ClientProgramDTO>( set.size() );
        for ( ClientProgram clientProgram : set ) {
            list.add( clientProgramToClientProgramDTO( clientProgram ) );
        }

        return list;
    }
}
