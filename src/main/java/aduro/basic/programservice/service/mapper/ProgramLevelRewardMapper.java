package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramLevelRewardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramLevelReward} and its DTO {@link ProgramLevelRewardDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramLevelMapper.class})
public interface ProgramLevelRewardMapper extends EntityMapper<ProgramLevelRewardDTO, ProgramLevelReward> {

    @Mapping(source = "programLevel.id", target = "programLevelId")
    @Mapping(source = "programLevel.name", target = "programLevelName")
    ProgramLevelRewardDTO toDto(ProgramLevelReward programLevelReward);

    @Mapping(source = "programLevelId", target = "programLevel")
    ProgramLevelReward toEntity(ProgramLevelRewardDTO programLevelRewardDTO);

    default ProgramLevelReward fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramLevelReward programLevelReward = new ProgramLevelReward();
        programLevelReward.setId(id);
        return programLevelReward;
    }
}
