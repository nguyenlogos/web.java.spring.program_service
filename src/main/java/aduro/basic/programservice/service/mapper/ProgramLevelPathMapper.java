package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramLevelPathDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramLevelPath} and its DTO {@link ProgramLevelPathDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramLevelMapper.class})
public interface ProgramLevelPathMapper extends EntityMapper<ProgramLevelPathDTO, ProgramLevelPath> {

    @Mapping(source = "programLevel.id", target = "programLevelId")
    @Mapping(source = "programLevel.name", target = "programLevelName")
    @Mapping(source = "programLevel.levelOrder", target = "programLevelLevelOrder")
    ProgramLevelPathDTO toDto(ProgramLevelPath programLevelPath);

    @Mapping(source = "programLevelId", target = "programLevel")
    ProgramLevelPath toEntity(ProgramLevelPathDTO programLevelPathDTO);

    default ProgramLevelPath fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramLevelPath programLevelPath = new ProgramLevelPath();
        programLevelPath.setId(id);
        return programLevelPath;
    }
}
