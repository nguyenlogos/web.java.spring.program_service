package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramCollectionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramCollection} and its DTO {@link ProgramCollectionDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ProgramCollectionMapper extends EntityMapper<ProgramCollectionDTO, ProgramCollection> {

    @Mapping(source = "program.id", target = "programId")
    ProgramCollectionDTO toDto(ProgramCollection programCollection);

    @Mapping(source = "programId", target = "program")
    @Mapping(target = "programCollectionContents", ignore = true)
    ProgramCollection toEntity(ProgramCollectionDTO programCollectionDTO);

    default ProgramCollection fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramCollection programCollection = new ProgramCollection();
        programCollection.setId(id);
        return programCollection;
    }
}
