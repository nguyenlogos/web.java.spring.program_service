package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.UserEventDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserEvent} and its DTO {@link UserEventDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramUserMapper.class})
public interface UserEventMapper extends EntityMapper<UserEventDTO, UserEvent> {

    @Mapping(source = "programUser.id", target = "programUserId")
    @Mapping(source = "programUser.participantId", target = "programUserParticipantId")
    UserEventDTO toDto(UserEvent userEvent);

    @Mapping(source = "programUserId", target = "programUser")
    UserEvent toEntity(UserEventDTO userEventDTO);

    default UserEvent fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserEvent userEvent = new UserEvent();
        userEvent.setId(id);
        return userEvent;
    }
}
