package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramLevelActivityDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramLevelActivity} and its DTO {@link ProgramLevelActivityDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramLevelMapper.class})
public interface ProgramLevelActivityMapper extends EntityMapper<ProgramLevelActivityDTO, ProgramLevelActivity> {

    @Mapping(source = "programLevel.id", target = "programLevelId")
    @Mapping(source = "programLevel.description", target = "programLevelDescription")
    ProgramLevelActivityDTO toDto(ProgramLevelActivity programLevelActivity);

    @Mapping(source = "programLevelId", target = "programLevel")
    ProgramLevelActivity toEntity(ProgramLevelActivityDTO programLevelActivityDTO);

    default ProgramLevelActivity fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramLevelActivity programLevelActivity = new ProgramLevelActivity();
        programLevelActivity.setId(id);
        return programLevelActivity;
    }
}
