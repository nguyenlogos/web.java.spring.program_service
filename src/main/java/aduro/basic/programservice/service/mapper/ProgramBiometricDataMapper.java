package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramBiometricDataDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramBiometricData} and its DTO {@link ProgramBiometricDataDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramBiometricDataMapper extends EntityMapper<ProgramBiometricDataDTO, ProgramBiometricData> {



    default ProgramBiometricData fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramBiometricData programBiometricData = new ProgramBiometricData();
        programBiometricData.setId(id);
        return programBiometricData;
    }
}
