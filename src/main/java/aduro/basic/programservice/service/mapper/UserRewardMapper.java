package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.UserRewardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserReward} and its DTO {@link UserRewardDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramUserMapper.class, ProgramMapper.class})
public interface UserRewardMapper extends EntityMapper<UserRewardDTO, UserReward> {

    @Mapping(source = "programUser.id", target = "programUserId")
    @Mapping(source = "programUser.participantId", target = "programUserParticipantId")
    @Mapping(source = "program.id", target = "programId")
    @Mapping(source = "program.name", target = "programName")
    UserRewardDTO toDto(UserReward userReward);

    @Mapping(source = "programUserId", target = "programUser")
    @Mapping(source = "programId", target = "program")
    UserReward toEntity(UserRewardDTO userRewardDTO);

    default UserReward fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserReward userReward = new UserReward();
        userReward.setId(id);
        return userReward;
    }
}
