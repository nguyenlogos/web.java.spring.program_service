package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramLevelDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramLevel} and its DTO {@link ProgramLevelDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ProgramLevelMapper extends EntityMapper<ProgramLevelDTO, ProgramLevel> {

   // @Mapping(source = "program.id", target = "programId")
    ProgramLevelDTO toDto(ProgramLevel programLevel);

    @Mapping(source = "programId", target = "program")
    ProgramLevel toEntity(ProgramLevelDTO programLevelDTO);




    default ProgramLevel fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramLevel programLevel = new ProgramLevel();
        programLevel.setId(id);
        return programLevel;
    }
}
