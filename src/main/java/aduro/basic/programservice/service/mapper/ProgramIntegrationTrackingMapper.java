package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramIntegrationTracking} and its DTO {@link ProgramIntegrationTrackingDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramIntegrationTrackingMapper extends EntityMapper<ProgramIntegrationTrackingDTO, ProgramIntegrationTracking> {



    default ProgramIntegrationTracking fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramIntegrationTracking programIntegrationTracking = new ProgramIntegrationTracking();
        programIntegrationTracking.setId(id);
        return programIntegrationTracking;
    }
}
