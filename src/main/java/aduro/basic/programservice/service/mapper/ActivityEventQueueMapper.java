package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ActivityEventQueueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ActivityEventQueue} and its DTO {@link ActivityEventQueueDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ActivityEventQueueMapper extends EntityMapper<ActivityEventQueueDTO, ActivityEventQueue> {



    default ActivityEventQueue fromId(Long id) {
        if (id == null) {
            return null;
        }
        ActivityEventQueue activityEventQueue = new ActivityEventQueue();
        activityEventQueue.setId(id);
        return activityEventQueue;
    }
}
