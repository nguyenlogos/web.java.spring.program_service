package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramRequirementItemsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramRequirementItems} and its DTO {@link ProgramRequirementItemsDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramLevelMapper.class})
public interface ProgramRequirementItemsMapper extends EntityMapper<ProgramRequirementItemsDTO, ProgramRequirementItems> {

    @Mapping(source = "programLevel.id", target = "programLevelId")
    @Mapping(source = "programLevel.levelOrder", target = "programLevelLevelOrder")
    ProgramRequirementItemsDTO toDto(ProgramRequirementItems programRequirementItems);

    @Mapping(source = "programLevelId", target = "programLevel")
    ProgramRequirementItems toEntity(ProgramRequirementItemsDTO programRequirementItemsDTO);

    default ProgramRequirementItems fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramRequirementItems programRequirementItems = new ProgramRequirementItems();
        programRequirementItems.setId(id);
        return programRequirementItems;
    }
}
