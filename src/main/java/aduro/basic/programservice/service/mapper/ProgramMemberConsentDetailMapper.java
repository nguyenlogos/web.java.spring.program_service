package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.service.dto.ProgramMemberConsentDetailDTO;
import aduro.basic.programservice.domain.ProgramMemberConsentDetail;
import org.mapstruct.Mapper;

/**
 * Mapper for the entity {@link ProgramMemberConsentDetail} and its DTO {@link ProgramMemberConsentDetailDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramMemberConsentDetailMapper extends EntityMapper<ProgramMemberConsentDetailDTO, ProgramMemberConsentDetail> {



    default ProgramMemberConsentDetail fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramMemberConsentDetail programMemberConsentDetail = new ProgramMemberConsentDetail();
        programMemberConsentDetail.setId(id);
        return programMemberConsentDetail;
    }
}
