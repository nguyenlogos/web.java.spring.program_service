package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.TermAndConditionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link TermAndCondition} and its DTO {@link TermAndConditionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface TermAndConditionMapper extends EntityMapper<TermAndConditionDTO, TermAndCondition> {



    default TermAndCondition fromId(Long id) {
        if (id == null) {
            return null;
        }
        TermAndCondition termAndCondition = new TermAndCondition();
        termAndCondition.setId(id);
        return termAndCondition;
    }
}
