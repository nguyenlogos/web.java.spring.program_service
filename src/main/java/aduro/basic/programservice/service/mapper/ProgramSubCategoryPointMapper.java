package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramSubCategoryPoint} and its DTO {@link ProgramSubCategoryPointDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramCategoryPointMapper.class})
public interface ProgramSubCategoryPointMapper extends EntityMapper<ProgramSubCategoryPointDTO, ProgramSubCategoryPoint> {

    @Mapping(source = "programCategoryPoint.id", target = "programCategoryPointId")
    @Mapping(source = "programCategoryPoint.categoryCode", target = "programCategoryPointCategoryCode")
    ProgramSubCategoryPointDTO toDto(ProgramSubCategoryPoint programSubCategoryPoint);

    @Mapping(source = "programCategoryPointId", target = "programCategoryPoint")
    ProgramSubCategoryPoint toEntity(ProgramSubCategoryPointDTO programSubCategoryPointDTO);

    default ProgramSubCategoryPoint fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramSubCategoryPoint programSubCategoryPoint = new ProgramSubCategoryPoint();
        programSubCategoryPoint.setId(id);
        return programSubCategoryPoint;
    }
}
