package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.OrderTransactionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrderTransaction} and its DTO {@link OrderTransactionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface OrderTransactionMapper extends EntityMapper<OrderTransactionDTO, OrderTransaction> {



    default OrderTransaction fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrderTransaction orderTransaction = new OrderTransaction();
        orderTransaction.setId(id);
        return orderTransaction;
    }
}
