package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramCollectionContentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramCollectionContent} and its DTO {@link ProgramCollectionContentDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramCollectionMapper.class})
public interface ProgramCollectionContentMapper extends EntityMapper<ProgramCollectionContentDTO, ProgramCollectionContent> {

    @Mapping(source = "programCollection.id", target = "programCollectionId")
    ProgramCollectionContentDTO toDto(ProgramCollectionContent programCollectionContent);

    @Mapping(source = "programCollectionId", target = "programCollection")
    ProgramCollectionContent toEntity(ProgramCollectionContentDTO programCollectionContentDTO);

    default ProgramCollectionContent fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramCollectionContent programCollectionContent = new ProgramCollectionContent();
        programCollectionContent.setId(id);
        return programCollectionContent;
    }
}
