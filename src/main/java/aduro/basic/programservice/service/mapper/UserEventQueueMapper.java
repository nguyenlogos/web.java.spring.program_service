package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.UserEventQueueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserEventQueue} and its DTO {@link UserEventQueueDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface UserEventQueueMapper extends EntityMapper<UserEventQueueDTO, UserEventQueue> {



    default UserEventQueue fromId(Long id) {
        if (id == null) {
            return null;
        }
        UserEventQueue userEventQueue = new UserEventQueue();
        userEventQueue.setId(id);
        return userEventQueue;
    }
}
