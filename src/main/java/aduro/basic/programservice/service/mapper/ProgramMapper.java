package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Program} and its DTO {@link ProgramDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramMapper extends EntityMapper<ProgramDTO, Program> {


    @Mapping(target = "programCategoryPoints", ignore = true)
    @Mapping(target = "programLevels", ignore = true)
    @Mapping(source = "tobaccoSurchargeManagement", target = "isTobaccoSurchargeManagement")
    Program toEntity(ProgramDTO programDTO);

    @Mapping(target = "programCategoryPoints", ignore = true)
    @Mapping(target = "programLevels", ignore = true)
    @Mapping(source = "isTobaccoSurchargeManagement", target = "tobaccoSurchargeManagement")
    @Mapping(target = "programHealthyRanges", ignore = true)
    ProgramDTO toDto(Program program);


    default Program fromId(Long id) {
        if (id == null) {
            return null;
        }
        Program program = new Program();
        program.setId(id);
        return program;
    }
}
