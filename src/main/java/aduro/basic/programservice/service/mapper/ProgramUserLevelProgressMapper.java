package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramUserLevelProgressDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramUserLevelProgress} and its DTO {@link ProgramUserLevelProgressDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramUserLevelProgressMapper extends EntityMapper<ProgramUserLevelProgressDTO, ProgramUserLevelProgress> {



    default ProgramUserLevelProgress fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramUserLevelProgress programUserLevelProgress = new ProgramUserLevelProgress();
        programUserLevelProgress.setId(id);
        return programUserLevelProgress;
    }
}
