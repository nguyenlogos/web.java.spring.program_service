package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.RewardDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Reward} and its DTO {@link RewardDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RewardMapper extends EntityMapper<RewardDTO, Reward> {



    default Reward fromId(Long id) {
        if (id == null) {
            return null;
        }
        Reward reward = new Reward();
        reward.setId(id);
        return reward;
    }
}
