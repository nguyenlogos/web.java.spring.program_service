package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramCohortCollection} and its DTO {@link ProgramCohortCollectionDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramCollectionMapper.class, ProgramCohortMapper.class})
public interface ProgramCohortCollectionMapper extends EntityMapper<ProgramCohortCollectionDTO, ProgramCohortCollection> {

    @Mapping(source = "programCollection.id", target = "programCollectionId")
    @Mapping(source = "programCohort.id", target = "programCohortId")
    ProgramCohortCollectionDTO toDto(ProgramCohortCollection programCohortCollection);

    @Mapping(source = "programCollectionId", target = "programCollection")
    @Mapping(source = "programCohortId", target = "programCohort")
    ProgramCohortCollection toEntity(ProgramCohortCollectionDTO programCohortCollectionDTO);

    default ProgramCohortCollection fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramCohortCollection programCohortCollection = new ProgramCohortCollection();
        programCohortCollection.setId(id);
        return programCohortCollection;
    }
}
