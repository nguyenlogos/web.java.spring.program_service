package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramSubgroupDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramSubgroup} and its DTO {@link ProgramSubgroupDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ProgramSubgroupMapper extends EntityMapper<ProgramSubgroupDTO, ProgramSubgroup> {

    @Mapping(source = "program.id", target = "programId")
    @Mapping(source = "program.name", target = "programName")
    ProgramSubgroupDTO toDto(ProgramSubgroup programSubgroup);

    @Mapping(source = "programId", target = "program")
    ProgramSubgroup toEntity(ProgramSubgroupDTO programSubgroupDTO);

    default ProgramSubgroup fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramSubgroup programSubgroup = new ProgramSubgroup();
        programSubgroup.setId(id);
        return programSubgroup;
    }
}
