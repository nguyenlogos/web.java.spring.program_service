package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.WellmetricEventQueueDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link WellmetricEventQueue} and its DTO {@link WellmetricEventQueueDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface WellmetricEventQueueMapper extends EntityMapper<WellmetricEventQueueDTO, WellmetricEventQueue> {



    default WellmetricEventQueue fromId(Long id) {
        if (id == null) {
            return null;
        }
        WellmetricEventQueue wellmetricEventQueue = new WellmetricEventQueue();
        wellmetricEventQueue.setId(id);
        return wellmetricEventQueue;
    }
}
