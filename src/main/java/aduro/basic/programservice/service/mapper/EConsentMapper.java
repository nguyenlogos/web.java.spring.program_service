package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.EConsentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link EConsent} and its DTO {@link EConsentDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EConsentMapper extends EntityMapper<EConsentDTO, EConsent> {



    default EConsent fromId(Long id) {
        if (id == null) {
            return null;
        }
        EConsent eConsent = new EConsent();
        eConsent.setId(id);
        return eConsent;
    }
}
