package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramSubCategoryConfiguration} and its DTO {@link ProgramSubCategoryConfigurationDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProgramSubCategoryConfigurationMapper extends EntityMapper<ProgramSubCategoryConfigurationDTO, ProgramSubCategoryConfiguration> {



    default ProgramSubCategoryConfiguration fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramSubCategoryConfiguration programSubCategoryConfiguration = new ProgramSubCategoryConfiguration();
        programSubCategoryConfiguration.setId(id);
        return programSubCategoryConfiguration;
    }
}
