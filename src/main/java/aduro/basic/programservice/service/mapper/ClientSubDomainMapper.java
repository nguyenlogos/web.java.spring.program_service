package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ClientSubDomainDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ClientSubDomain} and its DTO {@link ClientSubDomainDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ClientSubDomainMapper extends EntityMapper<ClientSubDomainDTO, ClientSubDomain> {



    default ClientSubDomain fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientSubDomain clientSubDomain = new ClientSubDomain();
        clientSubDomain.setId(id);
        return clientSubDomain;
    }
}
