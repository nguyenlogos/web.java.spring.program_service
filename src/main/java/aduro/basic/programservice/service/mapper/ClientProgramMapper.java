package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ClientProgramDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ClientProgram} and its DTO {@link ClientProgramDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ClientProgramMapper extends EntityMapper<ClientProgramDTO, ClientProgram> {

    //@Mapping(source = "program.id", target = "programId")
    @Mapping(source = "program.name", target = "programName")
    ClientProgramDTO toDto(ClientProgram clientProgram);

    @Mapping(source = "programId", target = "program")
    ClientProgram toEntity(ClientProgramDTO clientProgramDTO);

    default ClientProgram fromId(Long id) {
        if (id == null) {
            return null;
        }
        ClientProgram clientProgram = new ClientProgram();
        clientProgram.setId(id);
        return clientProgram;
    }
}
