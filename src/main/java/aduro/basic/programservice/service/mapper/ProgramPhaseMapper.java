package aduro.basic.programservice.service.mapper;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.service.dto.ProgramPhaseDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProgramPhase} and its DTO {@link ProgramPhaseDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProgramMapper.class})
public interface ProgramPhaseMapper extends EntityMapper<ProgramPhaseDTO, ProgramPhase> {

    @Mapping(source = "program.id", target = "programId")
    @Mapping(source = "program.name", target = "programName")
    ProgramPhaseDTO toDto(ProgramPhase programPhase);

    @Mapping(source = "programId", target = "program")
    ProgramPhase toEntity(ProgramPhaseDTO programPhaseDTO);

    default ProgramPhase fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProgramPhase programPhase = new ProgramPhase();
        programPhase.setId(id);
        return programPhase;
    }
}
