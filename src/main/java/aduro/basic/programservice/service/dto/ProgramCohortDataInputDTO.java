package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramCohortDataInput} entity.
 */
public class ProgramCohortDataInputDTO implements Serializable {

    private Long id;

    @Size(max = 150)
    private String nameDataInput;

    @Size(max = 100)
    private String inputType;

    @NotNull
    private Instant createdDate;

    @NotNull
    private Instant modifiedDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameDataInput() {
        return nameDataInput;
    }

    public void setNameDataInput(String nameDataInput) {
        this.nameDataInput = nameDataInput;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramCohortDataInputDTO programCohortDataInputDTO = (ProgramCohortDataInputDTO) o;
        if (programCohortDataInputDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programCohortDataInputDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramCohortDataInputDTO{" +
            "id=" + getId() +
            ", nameDataInput='" + getNameDataInput() + "'" +
            ", inputType='" + getInputType() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
