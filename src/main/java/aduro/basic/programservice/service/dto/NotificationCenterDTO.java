package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import aduro.basic.programservice.domain.enumeration.NotificationType;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.NotificationCenter} entity.
 */
public class NotificationCenterDTO implements Serializable {

    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String participantId;

    @NotNull
    private String executeStatus;

    @NotNull
    private Instant createdAt;

    private Integer attemptCount;

    @NotNull
    private Instant lastAttemptTime;

    private String errorMessage;

    @NotNull
    private String content;

    @NotNull
    private NotificationType notificationType;

    @NotNull
    private String eventId;

    private Boolean isReady;

    @NotNull
    private String receiverName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
    }

    public Instant getLastAttemptTime() {
        return lastAttemptTime;
    }

    public void setLastAttemptTime(Instant lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Boolean isIsReady() {
        return isReady;
    }

    public void setIsReady(Boolean isReady) {
        this.isReady = isReady;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NotificationCenterDTO notificationCenterDTO = (NotificationCenterDTO) o;
        if (notificationCenterDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), notificationCenterDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NotificationCenterDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", participantId='" + getParticipantId() + "'" +
            ", executeStatus='" + getExecuteStatus() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", attemptCount=" + getAttemptCount() +
            ", lastAttemptTime='" + getLastAttemptTime() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", content='" + getContent() + "'" +
            ", notificationType='" + getNotificationType() + "'" +
            ", eventId='" + getEventId() + "'" +
            ", isReady='" + isIsReady() + "'" +
            ", receiverName='" + getReceiverName() + "'" +
            "}";
    }
}
