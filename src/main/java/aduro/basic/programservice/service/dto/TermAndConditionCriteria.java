package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.TermAndCondition} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.TermAndConditionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /term-and-conditions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TermAndConditionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter clientId;

    private StringFilter subgroupId;

    private StringFilter title;

    private StringFilter content;

    private BooleanFilter isTargetSubgroup;

    private StringFilter subgroupName;

    public TermAndConditionCriteria(){
    }

    public TermAndConditionCriteria(TermAndConditionCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.content = other.content == null ? null : other.content.copy();
        this.isTargetSubgroup = other.isTargetSubgroup == null ? null : other.isTargetSubgroup.copy();
        this.subgroupName = other.subgroupName == null ? null : other.subgroupName.copy();
    }

    @Override
    public TermAndConditionCriteria copy() {
        return new TermAndConditionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getContent() {
        return content;
    }

    public void setContent(StringFilter content) {
        this.content = content;
    }

    public BooleanFilter getIsTargetSubgroup() {
        return isTargetSubgroup;
    }

    public void setIsTargetSubgroup(BooleanFilter isTargetSubgroup) {
        this.isTargetSubgroup = isTargetSubgroup;
    }

    public StringFilter getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(StringFilter subgroupName) {
        this.subgroupName = subgroupName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TermAndConditionCriteria that = (TermAndConditionCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(title, that.title) &&
            Objects.equals(content, that.content) &&
            Objects.equals(isTargetSubgroup, that.isTargetSubgroup) &&
            Objects.equals(subgroupName, that.subgroupName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        clientId,
        subgroupId,
        title,
        content,
        isTargetSubgroup,
        subgroupName
        );
    }

    @Override
    public String toString() {
        return "TermAndConditionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (content != null ? "content=" + content + ", " : "") +
                (isTargetSubgroup != null ? "isTargetSubgroup=" + isTargetSubgroup + ", " : "") +
                (subgroupName != null ? "subgroupName=" + subgroupName + ", " : "") +
            "}";
    }

}
