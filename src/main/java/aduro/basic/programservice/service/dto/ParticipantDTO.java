package aduro.basic.programservice.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParticipantDTO {

    private String participantId;

    private String email;

    private String firstName;
    private String lastName;

    private String subgroupId;

    private String subgroupName;

    private Boolean asProgramTester;

    public boolean isAsProgramTester(){
        return asProgramTester != null ? asProgramTester: false;
    }
}
