package aduro.basic.programservice.service.dto;

import java.util.List;

public class DemoGraphicDto {
    private String columnHeader;
    private List<DemoGraphicValueDto> columnValues;

    public String getColumnHeader() {
        return columnHeader;
    }

    public void setColumnHeader(String columnHeader) {
        this.columnHeader = columnHeader;
    }

    public List<DemoGraphicValueDto> getColumnValues() {
        return columnValues;
    }

    public void setColumnValues(List<DemoGraphicValueDto> columnValues) {
        this.columnValues = columnValues;
    }
}
