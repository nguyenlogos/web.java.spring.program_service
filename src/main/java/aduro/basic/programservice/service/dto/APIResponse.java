package aduro.basic.programservice.service.dto;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

public class APIResponse <T> {
    private T result;
    private String message;
    private List<String> errors = new ArrayList<>();
    private HttpStatus httpStatus;

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
