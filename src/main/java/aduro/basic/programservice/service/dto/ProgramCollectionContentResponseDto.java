package aduro.basic.programservice.service.dto;

import lombok.*;

import java.util.List;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProgramCollectionContentResponseDto {

    private Integer totalPages;
    private Integer currentPage;
    private List<ProgramCollectionContentDTO> items;
}
