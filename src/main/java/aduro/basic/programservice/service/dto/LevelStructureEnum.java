package aduro.basic.programservice.service.dto;

public enum LevelStructureEnum {

        Linear {
            @Override
            public String toString() {
                return "Linear";
            }
        },
        Exponential  {
            @Override
            public String toString() {
                return "Exponential";
            }
        },
        Custom {
            @Override
            public String toString() {
                return "Custom";
            }
        },

}
