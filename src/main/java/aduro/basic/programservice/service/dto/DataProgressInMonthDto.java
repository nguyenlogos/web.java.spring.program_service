package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.adp.domain.AmpUserActivityProgressHistory;
import lombok.*;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataProgressInMonthDto {
    private Date startOfMonth;
    private Date endOfMonth;
    private Boolean isCompletedRequired;
    private UserTrackingDataDto trackingDataDto;
    private List<AmpUserActivityProgressHistory> activityProgressHistories;
}
