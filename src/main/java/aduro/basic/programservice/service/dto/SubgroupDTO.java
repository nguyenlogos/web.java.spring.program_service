package aduro.basic.programservice.service.dto;

public class SubgroupDTO {

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    private String subgroupId;
    private String subgroupName;

}
