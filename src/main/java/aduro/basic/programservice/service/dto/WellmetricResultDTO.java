package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.UserEvent;
import aduro.basic.programservice.domain.enumeration.Gender;
import aduro.basic.programservice.domain.enumeration.ResultStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.math.BigDecimal;

public class WellmetricResultDTO implements Serializable {

    private String participantId;

    private String clientId;

    private ResultStatus result;

    private String eventCode;

    private Gender gender;

    private Float healthyValue;

    private String eventDate;

    private Long programId;


    @JsonIgnore
    private BigDecimal point;

    private String email;
    private boolean appealApplied;

    public WellmetricResultDTO(WellmetricEventQueueDTO wellmetricEventQueueDTO, UserEvent userEvent) {
        this.participantId = wellmetricEventQueueDTO.getParticipantId();
        this.clientId = wellmetricEventQueueDTO.getClientId();
        this.eventCode = wellmetricEventQueueDTO.getEventCode();
        this.gender = wellmetricEventQueueDTO.getGender();
        this.healthyValue = wellmetricEventQueueDTO.getHealthyValue();
        this.eventDate = wellmetricEventQueueDTO.getEventDate();
        this.point = userEvent.getEventPoint();
        this.result = wellmetricEventQueueDTO.getResult();
        this.email = wellmetricEventQueueDTO.getEmail();
        this.programId = wellmetricEventQueueDTO.getProgramId();
        this.appealApplied = wellmetricEventQueueDTO.isAppealApplied();
    }

    public WellmetricResultDTO(WellmetricEventQueueDTO wellmetricEventQueueDTO) {
        this.participantId = wellmetricEventQueueDTO.getParticipantId();
        this.clientId = wellmetricEventQueueDTO.getClientId();
        this.eventCode = wellmetricEventQueueDTO.getEventCode();
        this.gender = wellmetricEventQueueDTO.getGender();
        this.healthyValue = wellmetricEventQueueDTO.getHealthyValue();
        this.eventDate = wellmetricEventQueueDTO.getEventDate();
        this.result = wellmetricEventQueueDTO.getResult();
        this.email = wellmetricEventQueueDTO.getEmail();
        this.programId = wellmetricEventQueueDTO.getProgramId();
        this.appealApplied = wellmetricEventQueueDTO.isAppealApplied();
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public ResultStatus getResult() {
        return result;
    }

    public void setResult(ResultStatus result) {
        this.result = result;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Float getHealthyValue() {
        return healthyValue;
    }

    public void setHealthyValue(Float healthyValue) {
        this.healthyValue = healthyValue;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public boolean isAppealApplied() {
        return appealApplied;
    }

    public void setAppealApplied(boolean appealApplied) {
        this.appealApplied = appealApplied;
    }
}
