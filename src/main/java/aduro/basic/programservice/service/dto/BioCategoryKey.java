package aduro.basic.programservice.service.dto;

import java.util.Objects;

public class BioCategoryKey {
    private final long programId;
    private final String participantId;
    private final String eventDate;


    public BioCategoryKey(long programId, String participantId, String eventDate) {
        this.programId = programId;
        this.participantId = participantId;
        this.eventDate = eventDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BioCategoryKey that = (BioCategoryKey) o;
        return programId == that.programId &&
            Objects.equals(participantId, that.participantId) &&
            Objects.equals(eventDate, that.eventDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(programId, participantId, eventDate);
    }
}
