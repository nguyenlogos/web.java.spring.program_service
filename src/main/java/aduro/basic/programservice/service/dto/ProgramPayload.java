package aduro.basic.programservice.service.dto;

/* this class will response for short program, include used field, remove unused field*/
//TODO: Need Configuration Lombok

import java.time.Instant;

public class ProgramPayload {
    private Long programId;
    private String programName;
    private String status;
    private Instant startDate;
    private Instant endDate;

    public ProgramPayload(Long programId, String programName, String status, Instant startDate, Instant endDate) {
        this.programId = programId;
        this.programName = programName;
        this.status = status;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public ProgramPayload() {
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "ProgramPayload{" +
            "programId=" + programId +
            ", programName='" + programName + '\'' +
            ", status='" + status + '\'' +
            ", startDate=" + startDate +
            ", endDate=" + endDate +
            '}';
    }
}
