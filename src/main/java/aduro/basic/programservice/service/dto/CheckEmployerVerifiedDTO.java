package aduro.basic.programservice.service.dto;

import lombok.*;

import java.time.Instant;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckEmployerVerifiedDTO {
    private String clientId;
    private String activityCode;
    private boolean isValid;
    private Instant programStartDate;
    private Instant programEndDate;
    private Long customActivityId;

}
