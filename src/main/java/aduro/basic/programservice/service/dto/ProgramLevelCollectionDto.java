package aduro.basic.programservice.service.dto;

import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProgramLevelCollectionDto {

    private Long id;

    private String description;

    private Integer startPoint;

    private Integer endPoint;

    private Integer levelOrder;

    private String iconPath;

    private String name;

    private LocalDate endDate;

    private Integer numberOfRequired;

    private List<ProgramCollectionDTO> requiredCollections;
}
