package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.enumeration.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppealDto implements Serializable {

    private String gender;

    // 'a1c_appeal', 'fasting_glucose_appeal', 'heart_rate_appeal', 'blood_pressure_appeal',
    // 'wthr_appeal',
    // 'waist_appeal', 'bmi_appeal',
    //   'ldl_appeal', 'hdl_appeal', 'tc_appeal', 'tg_appeal', 'under_direct_suppervision_appeal'
    // tobacco_free, fregnancy
    @JsonProperty("a1c_appeal")
    private boolean a1C;

    @JsonProperty("fasting_glucose_appeal")
    private boolean fastingGlucose;

    @JsonProperty("heart_rate_appeal")
    private boolean heartRate;

    @JsonProperty("blood_pressure_appeal")
    private boolean bloodPressure;

    @JsonProperty("wthr_appeal")
    private boolean wthr;

    @JsonProperty("waist_appeal")
    private boolean waist;

    @JsonProperty("bmi_appeal")
    private boolean bmi;

    @JsonProperty("ldl_appeal")
    private boolean ldl;

    @JsonProperty("hdl_appeal")
    private boolean hdl;

    @JsonProperty("tc_appeal")
    private boolean tc;

    @JsonProperty("tg_appeal")
    private boolean tg;

    @JsonProperty("under_direct_supervision_appeal")
    private boolean underDirectSupervision;

    @JsonProperty("tobacco_free")
    private boolean tobacco;

    @JsonProperty("pregnancy_appeal")
    private boolean pregnancy;

    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("attemptedAt")
    private Date dateOfTest;

    private String wmEventId;

    public Gender getGender(){
        Gender genderType = Gender.UNIDENTIFIED;
        try {
            genderType = Gender.valueOf(this.gender);
        }
        catch (Exception ex){
            if(gender.toLowerCase().startsWith("m")){
                genderType = Gender.MALE;
            }else if(gender.toLowerCase().startsWith("f")){
                genderType = Gender.FEMALE;
            }
        }

        return genderType;
    }
}


