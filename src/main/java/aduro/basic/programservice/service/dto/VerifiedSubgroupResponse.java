package aduro.basic.programservice.service.dto;

public class VerifiedSubgroupResponse {
    private Boolean allowChanged;

    public Boolean getAllowChanged() {
        return allowChanged;
    }

    public void setAllowChanged(Boolean allowChanged) {
        this.allowChanged = allowChanged;
    }

    public VerifiedSubgroupResponse(Boolean allowChanged) {
        this.allowChanged = allowChanged;
    }
}
