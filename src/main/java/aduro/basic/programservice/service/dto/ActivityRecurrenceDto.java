package aduro.basic.programservice.service.dto;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityRecurrenceDto {
    private String type;
}

