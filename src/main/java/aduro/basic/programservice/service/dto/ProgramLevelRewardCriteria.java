package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramLevelReward} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramLevelRewardResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-level-rewards?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramLevelRewardCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter description;

    private IntegerFilter quantity;

    private StringFilter code;

    private BigDecimalFilter rewardAmount;

    private StringFilter rewardType;

    private StringFilter campaignId;

    private StringFilter subgroupId;

    private StringFilter subgroupName;

    private LongFilter programLevelId;

    public ProgramLevelRewardCriteria(){
    }

    public ProgramLevelRewardCriteria(ProgramLevelRewardCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.quantity = other.quantity == null ? null : other.quantity.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.rewardAmount = other.rewardAmount == null ? null : other.rewardAmount.copy();
        this.rewardType = other.rewardType == null ? null : other.rewardType.copy();
        this.campaignId = other.campaignId == null ? null : other.campaignId.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.subgroupName = other.subgroupName == null ? null : other.subgroupName.copy();
        this.programLevelId = other.programLevelId == null ? null : other.programLevelId.copy();
    }

    @Override
    public ProgramLevelRewardCriteria copy() {
        return new ProgramLevelRewardCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public IntegerFilter getQuantity() {
        return quantity;
    }

    public void setQuantity(IntegerFilter quantity) {
        this.quantity = quantity;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public BigDecimalFilter getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimalFilter rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public StringFilter getRewardType() {
        return rewardType;
    }

    public void setRewardType(StringFilter rewardType) {
        this.rewardType = rewardType;
    }

    public StringFilter getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(StringFilter campaignId) {
        this.campaignId = campaignId;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public StringFilter getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(StringFilter subgroupName) {
        this.subgroupName = subgroupName;
    }

    public LongFilter getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(LongFilter programLevelId) {
        this.programLevelId = programLevelId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramLevelRewardCriteria that = (ProgramLevelRewardCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(description, that.description) &&
            Objects.equals(quantity, that.quantity) &&
            Objects.equals(code, that.code) &&
            Objects.equals(rewardAmount, that.rewardAmount) &&
            Objects.equals(rewardType, that.rewardType) &&
            Objects.equals(campaignId, that.campaignId) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(subgroupName, that.subgroupName) &&
            Objects.equals(programLevelId, that.programLevelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        description,
        quantity,
        code,
        rewardAmount,
        rewardType,
        campaignId,
        subgroupId,
        subgroupName,
        programLevelId
        );
    }

    @Override
    public String toString() {
        return "ProgramLevelRewardCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (quantity != null ? "quantity=" + quantity + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (rewardAmount != null ? "rewardAmount=" + rewardAmount + ", " : "") +
                (rewardType != null ? "rewardType=" + rewardType + ", " : "") +
                (campaignId != null ? "campaignId=" + campaignId + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (subgroupName != null ? "subgroupName=" + subgroupName + ", " : "") +
                (programLevelId != null ? "programLevelId=" + programLevelId + ", " : "") +
            "}";
    }

}
