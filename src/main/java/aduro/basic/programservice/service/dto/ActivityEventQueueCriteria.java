package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ActivityEventQueue} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ActivityEventQueueResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /activity-event-queues?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ActivityEventQueueCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter participantId;

    private StringFilter clientId;

    private StringFilter activityCode;

    private StringFilter firstName;

    private StringFilter lastName;

    private StringFilter email;

    private InstantFilter createdDate;

    private StringFilter transactionId;

    private StringFilter executeStatus;

    private StringFilter errorMessage;

    private StringFilter subgroupId;

    public ActivityEventQueueCriteria(){
    }

    public ActivityEventQueueCriteria(ActivityEventQueueCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.participantId = other.participantId == null ? null : other.participantId.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.activityCode = other.activityCode == null ? null : other.activityCode.copy();
        this.firstName = other.firstName == null ? null : other.firstName.copy();
        this.lastName = other.lastName == null ? null : other.lastName.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.transactionId = other.transactionId == null ? null : other.transactionId.copy();
        this.executeStatus = other.executeStatus == null ? null : other.executeStatus.copy();
        this.errorMessage = other.errorMessage == null ? null : other.errorMessage.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
    }

    @Override
    public ActivityEventQueueCriteria copy() {
        return new ActivityEventQueueCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getParticipantId() {
        return participantId;
    }

    public void setParticipantId(StringFilter participantId) {
        this.participantId = participantId;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(StringFilter activityCode) {
        this.activityCode = activityCode;
    }

    public StringFilter getFirstName() {
        return firstName;
    }

    public void setFirstName(StringFilter firstName) {
        this.firstName = firstName;
    }

    public StringFilter getLastName() {
        return lastName;
    }

    public void setLastName(StringFilter lastName) {
        this.lastName = lastName;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(StringFilter transactionId) {
        this.transactionId = transactionId;
    }

    public StringFilter getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(StringFilter executeStatus) {
        this.executeStatus = executeStatus;
    }

    public StringFilter getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(StringFilter errorMessage) {
        this.errorMessage = errorMessage;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ActivityEventQueueCriteria that = (ActivityEventQueueCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(participantId, that.participantId) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(activityCode, that.activityCode) &&
            Objects.equals(firstName, that.firstName) &&
            Objects.equals(lastName, that.lastName) &&
            Objects.equals(email, that.email) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(transactionId, that.transactionId) &&
            Objects.equals(executeStatus, that.executeStatus) &&
            Objects.equals(errorMessage, that.errorMessage) &&
            Objects.equals(subgroupId, that.subgroupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        participantId,
        clientId,
        activityCode,
        firstName,
        lastName,
        email,
        createdDate,
        transactionId,
        executeStatus,
        errorMessage,
        subgroupId
        );
    }

    @Override
    public String toString() {
        return "ActivityEventQueueCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (participantId != null ? "participantId=" + participantId + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (activityCode != null ? "activityCode=" + activityCode + ", " : "") +
                (firstName != null ? "firstName=" + firstName + ", " : "") +
                (lastName != null ? "lastName=" + lastName + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (transactionId != null ? "transactionId=" + transactionId + ", " : "") +
                (executeStatus != null ? "executeStatus=" + executeStatus + ", " : "") +
                (errorMessage != null ? "errorMessage=" + errorMessage + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
            "}";
    }

}
