package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramCohort} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramCohortResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-cohorts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramCohortCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter cohortName;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    private BooleanFilter isDefault;

    private LongFilter programId;

    private LongFilter programCohortRuleId;

    private LongFilter programCohortCollectionId;

    public ProgramCohortCriteria(){
    }

    public ProgramCohortCriteria(ProgramCohortCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.cohortName = other.cohortName == null ? null : other.cohortName.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
        this.isDefault = other.isDefault == null ? null : other.isDefault.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.programCohortRuleId = other.programCohortRuleId == null ? null : other.programCohortRuleId.copy();
        this.programCohortCollectionId = other.programCohortCollectionId == null ? null : other.programCohortCollectionId.copy();
    }

    @Override
    public ProgramCohortCriteria copy() {
        return new ProgramCohortCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCohortName() {
        return cohortName;
    }

    public void setCohortName(StringFilter cohortName) {
        this.cohortName = cohortName;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public BooleanFilter getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(BooleanFilter isDefault) {
        this.isDefault = isDefault;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public LongFilter getProgramCohortRuleId() {
        return programCohortRuleId;
    }

    public void setProgramCohortRuleId(LongFilter programCohortRuleId) {
        this.programCohortRuleId = programCohortRuleId;
    }

    public LongFilter getProgramCohortCollectionId() {
        return programCohortCollectionId;
    }

    public void setProgramCohortCollectionId(LongFilter programCohortCollectionId) {
        this.programCohortCollectionId = programCohortCollectionId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramCohortCriteria that = (ProgramCohortCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(cohortName, that.cohortName) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate) &&
            Objects.equals(isDefault, that.isDefault) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(programCohortRuleId, that.programCohortRuleId) &&
            Objects.equals(programCohortCollectionId, that.programCohortCollectionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        cohortName,
        createdDate,
        modifiedDate,
        isDefault,
        programId,
        programCohortRuleId,
        programCohortCollectionId
        );
    }

    @Override
    public String toString() {
        return "ProgramCohortCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (cohortName != null ? "cohortName=" + cohortName + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
                (isDefault != null ? "isDefault=" + isDefault + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (programCohortRuleId != null ? "programCohortRuleId=" + programCohortRuleId + ", " : "") +
                (programCohortCollectionId != null ? "programCohortCollectionId=" + programCohortCollectionId + ", " : "") +
            "}";
    }

}
