package aduro.basic.programservice.service.dto;
import aduro.basic.programservice.ap.dto.CategoryDto;
import aduro.basic.programservice.domain.ProgramActivity;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramActivity} entity.
 */
public class ProgramActivityDTO implements Serializable {

    private Long id;

    @NotNull
    private String activityId;

    @NotNull
    private String activityCode;

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    private String activityType;

    private Long programPhaseId;

    private String programPhasePhaseOrder;

    private Long programId;

    private String programName;

    private String masterId;

    private Boolean bonusPointsEnabled;

    private BigDecimal bonusPoints;

    private String title;

    private String sizeOfActivity;

    private CategoryDto category;

    private long templateActivityId;

    private Instant startDate;
    private Instant endDate;
    private Boolean required;
    private Boolean featured;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }


    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public Boolean isIsCustomized() {
        return isCustomized;
    }

    public ProgramActivityDTO isCustomized(Boolean isCustomized) {
        this.isCustomized = isCustomized;
        return this;
    }

    public void setIsCustomized(Boolean isCustomized) {
        this.isCustomized = isCustomized;
    }

    private boolean isCustomized;

    public String getAssignToLevel() {
        return assignToLevel;
    }

    public void setAssignToLevel(String assignToLevel) {
        this.assignToLevel = assignToLevel;
    }

    private String assignToLevel;

    public Long getProgramPhaseId() {
        return programPhaseId;
    }

    public void setProgramPhaseId(Long programPhaseId) {
        this.programPhaseId = programPhaseId;
    }

    public String getProgramPhasePhaseOrder() {
        return programPhasePhaseOrder;
    }

    public void setProgramPhasePhaseOrder(String programPhasePhaseOrder) {
        this.programPhasePhaseOrder = programPhasePhaseOrder;
    }


    public String getMasterId() {
        return masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public Boolean isBonusPointsEnabled() {
        return bonusPointsEnabled;
    }

    public void setBonusPointsEnabled(Boolean bonusPointsEnabled) {
        this.bonusPointsEnabled = bonusPointsEnabled;
    }

    public BigDecimal getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(BigDecimal bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramActivityDTO programActivityDTO = (ProgramActivityDTO) o;
        if (programActivityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programActivityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramActivityDTO{" +
            "id=" + getId() +
            ", activityId='" + getActivityId() + "'" +
            ", isCustomized='" + isIsCustomized() + "'" +
            ", program=" + getProgramId() +
            ", program='" + getProgramName() + "'" +
            ", programPhase=" + getProgramPhaseId() +
            ", programPhase='" + getProgramPhasePhaseOrder() + "'" +
            "}";
    }

    public Boolean getBonusPointsEnabled() {
        return bonusPointsEnabled;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSizeOfActivity() {
        return sizeOfActivity;
    }

    public void setSizeOfActivity(String sizeOfActivity) {
        this.sizeOfActivity = sizeOfActivity;
    }

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public boolean isCustomized() {
        return isCustomized;
    }

    public void setCustomized(boolean customized) {
        isCustomized = customized;
    }

    public long getTemplateActivityId() {
        return templateActivityId;
    }

    public void setTemplateActivityId(long templateActivityId) {
        this.templateActivityId = templateActivityId;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Boolean getFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }
}
