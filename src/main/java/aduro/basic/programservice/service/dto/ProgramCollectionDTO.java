package aduro.basic.programservice.service.dto;
import aduro.basic.programservice.domain.enumeration.CollectionStatus;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramCollection} entity.
 */
public class ProgramCollectionDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 150)
    private String displayName;

    @Size(max = 255)
    private String longDescription;

    private Instant createdDate;

    private Instant modifiedDate;

    private Long programId;

    private Long programCohortId;

    private Integer numberOfCompletion;

    private CollectionStatus collectionStatus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Long getProgramCohortId() {
        return programCohortId;
    }

    public void setProgramCohortId(Long programCohortId) {
        this.programCohortId = programCohortId;
    }

    public Integer getNumberOfCompletion() {
        return numberOfCompletion;
    }

    public void setNumberOfCompletion(Integer numberOfCompletion) {
        this.numberOfCompletion = numberOfCompletion;
    }

    public CollectionStatus getCollectionStatus() {
        return collectionStatus;
    }

    public void setCollectionStatus(CollectionStatus collectionStatus) {
        this.collectionStatus = collectionStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramCollectionDTO programCollectionDTO = (ProgramCollectionDTO) o;
        if (programCollectionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programCollectionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramCollectionDTO{" +
            "id=" + getId() +
            ", displayName='" + getDisplayName() + "'" +
            ", longDescription='" + getLongDescription() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", program=" + getProgramId() +
            "}";
    }
}
