package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramCollectionContent} entity.
 */
public class ProgramCollectionContentDTO implements Serializable {

    private Long id;

    @Size(max = 255)
    private String itemName;

    @Size(max = 100)
    private String itemId;

    @NotNull
    @Size(max = 100)
    private String itemType;

    @NotNull
    @Size(max = 100)
    private String contentType;

    @Size(max = 255)
    private String itemIcon;

    private Boolean hasRequired;

    private Instant createdDate;

    private Instant modifiedDate;


    private Long programCollectionId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getItemIcon() {
        return itemIcon;
    }

    public void setItemIcon(String itemIcon) {
        this.itemIcon = itemIcon;
    }

    public Boolean isHasRequired() {
        return hasRequired;
    }

    public void setHasRequired(Boolean hasRequired) {
        this.hasRequired = hasRequired;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getProgramCollectionId() {
        return programCollectionId;
    }

    public void setProgramCollectionId(Long programCollectionId) {
        this.programCollectionId = programCollectionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramCollectionContentDTO programCollectionContentDTO = (ProgramCollectionContentDTO) o;
        if (programCollectionContentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programCollectionContentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramCollectionContentDTO{" +
            "id=" + getId() +
            ", itemName='" + getItemName() + "'" +
            ", itemId='" + getItemId() + "'" +
            ", itemType='" + getItemType() + "'" +
            ", contentType='" + getContentType() + "'" +
            ", itemIcon='" + getItemIcon() + "'" +
            ", hasRequired='" + isHasRequired() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", programCollection=" + getProgramCollectionId() +
            "}";
    }
}
