package aduro.basic.programservice.service.dto;

public enum RewardTypeEnum {
    TangoCard {
        @Override
        public String toString() {
            return "TangoCard";
        }
    },
    Tremendous {
        @Override
        public String toString() {
            return "Tremendous";
        }
    },
    PayrollCredits {
        @Override
        public String toString() {
            return "PayrollCredits";
        }
    },
    HealthSavings {
        @Override
        public String toString() {
            return "HealthSavings";
        }
    },
    PaidTimeOff {
        @Override
        public String toString() {
            return "PaidTimeOff";
        }
    },
    Custom {
        @Override
        public String toString() {
            return "Custom";
        }
    },
    TobaccoSurcharge {
        @Override
        public String toString() {
            return "TobaccoSurcharge";
        }
    },
    TobaccoPayrollCredit {
        @Override
        public String toString() {
            return "TobaccoPayrollCredit";
        }
    },
    TobaccoCustom {
        @Override
        public String toString() {
            return "TobaccoCustom";
        }
    }

}
