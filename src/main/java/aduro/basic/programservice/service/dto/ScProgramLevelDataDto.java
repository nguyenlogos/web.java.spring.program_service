package aduro.basic.programservice.service.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScProgramLevelDataDto {
    private Integer levelOrder;
    private String levelName;
    private String description;
    private List<ScProgramLevelRequiredItemDto> requiredItemList;
    private Instant levelCompletedDate;
    private String levelStatus;
    private Integer pointRequired;
    private List<ScProgramLevelRewardDto> rewardDtoList;
}
