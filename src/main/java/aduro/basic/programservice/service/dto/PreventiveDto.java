package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.enumeration.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PreventiveDto implements Serializable {

    private String gender;


    // Dental Visit : date_of_dental_visit
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("date_of_dental_visit")
    private Date dentalVisitDate;

    // Vision screening date: date_of_vision_screening_visit
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("date_of_vision_screening_visit")
    private Date dateOfVision;

//    Hemogobin A1C: preventive_2nd_hemoglobin_a1c
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("preventive_2nd_hemoglobin_a1c")
    private Date dateHemoglobinA1c;
//    dilated eye exam: dialated_eye_exam
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("dialated_eye_exam")
    private Date dateDialedEyeExam;

//    Createinine & Urine test: preventive_urine_protein_test
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("preventive_urine_protein_test")
    private Date dateUrineProtein;

//    pre-natal -> pregnancy_program_tri_1 ???????
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("pregnancy_program_tri_1")
    private Date datePreNatal;


    //    annual pcp exam : check_in_check_up
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("check_in_check_up")
    private Date dateAnnualPCP;

    //    Followup PCP exam: pcp_follow_up
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("pcp_follow_up")
    private Date dateFollowupPCP;

    //    mammography: mammogram
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("mammogram")
    private Date dateMamogram;

    //    colonoscopy: colonoscopy
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("colonoscopy")
    private Date dateColonoscopy;

    //    Colorectal Cancer: colorectal_cancer_screening
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("colorectal_cancer_screening")
    private Date dateColorectalCancer;

    //    Prostate cancer :prostate_cancer_screening
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("prostate_cancer_screening")
    private Date dateProstateCancer;

    //    Cervical: pap_smear
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("pap_smear")
    private Date dateCervicalCancer;

    // flu vaccine
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("flu_vaccine_date")
    private Date fluVaccineDate;

    // covid vaccine
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("covid_vaccine_date_first")
    private Date covidVaccineFirstDate;

    // covid vaccine booster
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("covid_vaccine_date_second")
    private Date covidVaccineSecondDate;

    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("covid_vaccine_date_single_dose")
    private Date covidVaccineSingleDoseDate;

    // covid vaccine booster
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("covid_vaccine_booster_date")
    private Date covidVaccineBoosterDate;


    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    @JsonProperty("attemptedAt")
    private Date dateOfTest;

    @JsonProperty("activityId")
    private Long activityId;

    @JsonProperty("screening_type")
    private String screeningType;

    private List<PreventiveActivityDto> eligibleActivities;

    public Gender getGender(){
        Gender genderType = Gender.UNIDENTIFIED;
        try {
            genderType = Gender.valueOf(this.gender);
        }
        catch (Exception ex){
            if(gender.toLowerCase().startsWith("m")){
                genderType = Gender.MALE;
            }else if(gender.toLowerCase().startsWith("f")){
                genderType = Gender.FEMALE;
            }
        }

        return genderType;
    }
}


