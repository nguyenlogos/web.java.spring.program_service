package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramSubCategoryConfiguration} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramSubCategoryConfigurationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-sub-category-configurations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramSubCategoryConfigurationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter subCategoryCode;

    private BooleanFilter isBloodPressureSingleTest;

    private BooleanFilter isBloodPressureIndividualTest;

    private BooleanFilter isGlucoseAwardedInRange;

    private BooleanFilter isBmiAwardedInRange;

    private BooleanFilter isBmiAwardedFasting;

    private BooleanFilter isBmiAwardedNonFasting;

    private LongFilter programId;

    public ProgramSubCategoryConfigurationCriteria(){
    }

    public ProgramSubCategoryConfigurationCriteria(ProgramSubCategoryConfigurationCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.subCategoryCode = other.subCategoryCode == null ? null : other.subCategoryCode.copy();
        this.isBloodPressureSingleTest = other.isBloodPressureSingleTest == null ? null : other.isBloodPressureSingleTest.copy();
        this.isBloodPressureIndividualTest = other.isBloodPressureIndividualTest == null ? null : other.isBloodPressureIndividualTest.copy();
        this.isGlucoseAwardedInRange = other.isGlucoseAwardedInRange == null ? null : other.isGlucoseAwardedInRange.copy();
        this.isBmiAwardedInRange = other.isBmiAwardedInRange == null ? null : other.isBmiAwardedInRange.copy();
        this.isBmiAwardedFasting = other.isBmiAwardedFasting == null ? null : other.isBmiAwardedFasting.copy();
        this.isBmiAwardedNonFasting = other.isBmiAwardedNonFasting == null ? null : other.isBmiAwardedNonFasting.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
    }

    @Override
    public ProgramSubCategoryConfigurationCriteria copy() {
        return new ProgramSubCategoryConfigurationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSubCategoryCode() {
        return subCategoryCode;
    }

    public void setSubCategoryCode(StringFilter subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public BooleanFilter getIsBloodPressureSingleTest() {
        return isBloodPressureSingleTest;
    }

    public void setIsBloodPressureSingleTest(BooleanFilter isBloodPressureSingleTest) {
        this.isBloodPressureSingleTest = isBloodPressureSingleTest;
    }

    public BooleanFilter getIsBloodPressureIndividualTest() {
        return isBloodPressureIndividualTest;
    }

    public void setIsBloodPressureIndividualTest(BooleanFilter isBloodPressureIndividualTest) {
        this.isBloodPressureIndividualTest = isBloodPressureIndividualTest;
    }

    public BooleanFilter getIsGlucoseAwardedInRange() {
        return isGlucoseAwardedInRange;
    }

    public void setIsGlucoseAwardedInRange(BooleanFilter isGlucoseAwardedInRange) {
        this.isGlucoseAwardedInRange = isGlucoseAwardedInRange;
    }

    public BooleanFilter getIsBmiAwardedInRange() {
        return isBmiAwardedInRange;
    }

    public void setIsBmiAwardedInRange(BooleanFilter isBmiAwardedInRange) {
        this.isBmiAwardedInRange = isBmiAwardedInRange;
    }

    public BooleanFilter getIsBmiAwardedFasting() {
        return isBmiAwardedFasting;
    }

    public void setIsBmiAwardedFasting(BooleanFilter isBmiAwardedFasting) {
        this.isBmiAwardedFasting = isBmiAwardedFasting;
    }

    public BooleanFilter getIsBmiAwardedNonFasting() {
        return isBmiAwardedNonFasting;
    }

    public void setIsBmiAwardedNonFasting(BooleanFilter isBmiAwardedNonFasting) {
        this.isBmiAwardedNonFasting = isBmiAwardedNonFasting;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramSubCategoryConfigurationCriteria that = (ProgramSubCategoryConfigurationCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(subCategoryCode, that.subCategoryCode) &&
            Objects.equals(isBloodPressureSingleTest, that.isBloodPressureSingleTest) &&
            Objects.equals(isBloodPressureIndividualTest, that.isBloodPressureIndividualTest) &&
            Objects.equals(isGlucoseAwardedInRange, that.isGlucoseAwardedInRange) &&
            Objects.equals(isBmiAwardedInRange, that.isBmiAwardedInRange) &&
            Objects.equals(isBmiAwardedFasting, that.isBmiAwardedFasting) &&
            Objects.equals(isBmiAwardedNonFasting, that.isBmiAwardedNonFasting) &&
            Objects.equals(programId, that.programId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        subCategoryCode,
        isBloodPressureSingleTest,
        isBloodPressureIndividualTest,
        isGlucoseAwardedInRange,
        isBmiAwardedInRange,
        isBmiAwardedFasting,
        isBmiAwardedNonFasting,
        programId
        );
    }

    @Override
    public String toString() {
        return "ProgramSubCategoryConfigurationCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (subCategoryCode != null ? "subCategoryCode=" + subCategoryCode + ", " : "") +
                (isBloodPressureSingleTest != null ? "isBloodPressureSingleTest=" + isBloodPressureSingleTest + ", " : "") +
                (isBloodPressureIndividualTest != null ? "isBloodPressureIndividualTest=" + isBloodPressureIndividualTest + ", " : "") +
                (isGlucoseAwardedInRange != null ? "isGlucoseAwardedInRange=" + isGlucoseAwardedInRange + ", " : "") +
                (isBmiAwardedInRange != null ? "isBmiAwardedInRange=" + isBmiAwardedInRange + ", " : "") +
                (isBmiAwardedFasting != null ? "isBmiAwardedFasting=" + isBmiAwardedFasting + ", " : "") +
                (isBmiAwardedNonFasting != null ? "isBmiAwardedNonFasting=" + isBmiAwardedNonFasting + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
            "}";
    }

}
