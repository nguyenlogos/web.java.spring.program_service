package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import aduro.basic.programservice.domain.enumeration.CollectionStatus;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramUserCollectionProgress} entity.
 */
public class ProgramUserCollectionProgressDTO implements Serializable {

    private Long id;

    @NotNull
    private Long collectionId;

    @NotNull
    private Integer totalRequiredItems;

    private Long currentProgress;

    private CollectionStatus status;

    @NotNull
    private Instant createdDate;

    @NotNull
    private Instant modifiedDate;


    private Long programUserCohortId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(Long collectionId) {
        this.collectionId = collectionId;
    }

    public Integer getTotalRequiredItems() {
        return totalRequiredItems;
    }

    public void setTotalRequiredItems(Integer totalRequiredItems) {
        this.totalRequiredItems = totalRequiredItems;
    }

    public Long getCurrentProgress() {
        return currentProgress;
    }

    public void setCurrentProgress(Long currentProgress) {
        this.currentProgress = currentProgress;
    }

    public CollectionStatus getStatus() {
        return status;
    }

    public void setStatus(CollectionStatus status) {
        this.status = status;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getProgramUserCohortId() {
        return programUserCohortId;
    }

    public void setProgramUserCohortId(Long programUserCohortId) {
        this.programUserCohortId = programUserCohortId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = (ProgramUserCollectionProgressDTO) o;
        if (programUserCollectionProgressDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programUserCollectionProgressDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramUserCollectionProgressDTO{" +
            "id=" + getId() +
            ", collectionId=" + getCollectionId() +
            ", totalRequiredItems=" + getTotalRequiredItems() +
            ", currentProgress=" + getCurrentProgress() +
            ", status='" + getStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", programUserCohort=" + getProgramUserCohortId() +
            "}";
    }
}
