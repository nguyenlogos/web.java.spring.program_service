package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import aduro.basic.programservice.domain.enumeration.CohortStatus;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramUserCohort} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramUserCohortResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-user-cohorts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramUserCohortCriteria implements Serializable, Criteria {
    /**
     * Class for filtering CohortStatus
     */
    public static class CohortStatusFilter extends Filter<CohortStatus> {

        public CohortStatusFilter() {
        }

        public CohortStatusFilter(CohortStatusFilter filter) {
            super(filter);
        }

        @Override
        public CohortStatusFilter copy() {
            return new CohortStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter progress;

    private IntegerFilter numberOfPass;

    private IntegerFilter numberOfFailure;

    private CohortStatusFilter status;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    private LongFilter programUserId;

    private LongFilter programCohortId;

    public ProgramUserCohortCriteria(){
    }

    public ProgramUserCohortCriteria(ProgramUserCohortCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.progress = other.progress == null ? null : other.progress.copy();
        this.numberOfPass = other.numberOfPass == null ? null : other.numberOfPass.copy();
        this.numberOfFailure = other.numberOfFailure == null ? null : other.numberOfFailure.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
        this.programUserId = other.programUserId == null ? null : other.programUserId.copy();
        this.programCohortId = other.programCohortId == null ? null : other.programCohortId.copy();
    }

    @Override
    public ProgramUserCohortCriteria copy() {
        return new ProgramUserCohortCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getProgress() {
        return progress;
    }

    public void setProgress(LongFilter progress) {
        this.progress = progress;
    }

    public IntegerFilter getNumberOfPass() {
        return numberOfPass;
    }

    public void setNumberOfPass(IntegerFilter numberOfPass) {
        this.numberOfPass = numberOfPass;
    }

    public IntegerFilter getNumberOfFailure() {
        return numberOfFailure;
    }

    public void setNumberOfFailure(IntegerFilter numberOfFailure) {
        this.numberOfFailure = numberOfFailure;
    }

    public CohortStatusFilter getStatus() {
        return status;
    }

    public void setStatus(CohortStatusFilter status) {
        this.status = status;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public LongFilter getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(LongFilter programUserId) {
        this.programUserId = programUserId;
    }

    public LongFilter getProgramCohortId() {
        return programCohortId;
    }

    public void setProgramCohortId(LongFilter programCohortId) {
        this.programCohortId = programCohortId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramUserCohortCriteria that = (ProgramUserCohortCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(progress, that.progress) &&
            Objects.equals(numberOfPass, that.numberOfPass) &&
            Objects.equals(numberOfFailure, that.numberOfFailure) &&
            Objects.equals(status, that.status) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate) &&
            Objects.equals(programUserId, that.programUserId) &&
            Objects.equals(programCohortId, that.programCohortId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        progress,
        numberOfPass,
        numberOfFailure,
        status,
        createdDate,
        modifiedDate,
        programUserId,
        programCohortId
        );
    }

    @Override
    public String toString() {
        return "ProgramUserCohortCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (progress != null ? "progress=" + progress + ", " : "") +
                (numberOfPass != null ? "numberOfPass=" + numberOfPass + ", " : "") +
                (numberOfFailure != null ? "numberOfFailure=" + numberOfFailure + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
                (programUserId != null ? "programUserId=" + programUserId + ", " : "") +
                (programCohortId != null ? "programCohortId=" + programCohortId + ", " : "") +
            "}";
    }

}
