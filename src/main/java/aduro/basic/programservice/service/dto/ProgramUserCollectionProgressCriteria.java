package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import aduro.basic.programservice.domain.enumeration.CollectionStatus;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramUserCollectionProgress} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramUserCollectionProgressResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-user-collection-progresses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramUserCollectionProgressCriteria implements Serializable, Criteria {
    /**
     * Class for filtering CollectionStatus
     */
    public static class CollectionStatusFilter extends Filter<CollectionStatus> {

        public CollectionStatusFilter() {
        }

        public CollectionStatusFilter(CollectionStatusFilter filter) {
            super(filter);
        }

        @Override
        public CollectionStatusFilter copy() {
            return new CollectionStatusFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter collectionId;

    private IntegerFilter totalRequiredItems;

    private LongFilter currentProgress;

    private CollectionStatusFilter status;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    private LongFilter programUserCohortId;

    public ProgramUserCollectionProgressCriteria(){
    }

    public ProgramUserCollectionProgressCriteria(ProgramUserCollectionProgressCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.collectionId = other.collectionId == null ? null : other.collectionId.copy();
        this.totalRequiredItems = other.totalRequiredItems == null ? null : other.totalRequiredItems.copy();
        this.currentProgress = other.currentProgress == null ? null : other.currentProgress.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
        this.programUserCohortId = other.programUserCohortId == null ? null : other.programUserCohortId.copy();
    }

    @Override
    public ProgramUserCollectionProgressCriteria copy() {
        return new ProgramUserCollectionProgressCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(LongFilter collectionId) {
        this.collectionId = collectionId;
    }

    public IntegerFilter getTotalRequiredItems() {
        return totalRequiredItems;
    }

    public void setTotalRequiredItems(IntegerFilter totalRequiredItems) {
        this.totalRequiredItems = totalRequiredItems;
    }

    public LongFilter getCurrentProgress() {
        return currentProgress;
    }

    public void setCurrentProgress(LongFilter currentProgress) {
        this.currentProgress = currentProgress;
    }

    public CollectionStatusFilter getStatus() {
        return status;
    }

    public void setStatus(CollectionStatusFilter status) {
        this.status = status;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public LongFilter getProgramUserCohortId() {
        return programUserCohortId;
    }

    public void setProgramUserCohortId(LongFilter programUserCohortId) {
        this.programUserCohortId = programUserCohortId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramUserCollectionProgressCriteria that = (ProgramUserCollectionProgressCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(collectionId, that.collectionId) &&
            Objects.equals(totalRequiredItems, that.totalRequiredItems) &&
            Objects.equals(currentProgress, that.currentProgress) &&
            Objects.equals(status, that.status) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate) &&
            Objects.equals(programUserCohortId, that.programUserCohortId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        collectionId,
        totalRequiredItems,
        currentProgress,
        status,
        createdDate,
        modifiedDate,
        programUserCohortId
        );
    }

    @Override
    public String toString() {
        return "ProgramUserCollectionProgressCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (collectionId != null ? "collectionId=" + collectionId + ", " : "") +
                (totalRequiredItems != null ? "totalRequiredItems=" + totalRequiredItems + ", " : "") +
                (currentProgress != null ? "currentProgress=" + currentProgress + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
                (programUserCohortId != null ? "programUserCohortId=" + programUserCohortId + ", " : "") +
            "}";
    }

}
