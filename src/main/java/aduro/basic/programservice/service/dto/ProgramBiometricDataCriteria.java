package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramBiometricData} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramBiometricDataResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-biometric-data?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramBiometricDataCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter biometricCode;

    private StringFilter biometricName;

    private FloatFilter maleMin;

    private FloatFilter maleMax;

    private FloatFilter femaleMin;

    private FloatFilter femaleMax;

    private FloatFilter unidentifiedMin;

    private FloatFilter unidentifiedMax;

    public ProgramBiometricDataCriteria(){
    }

    public ProgramBiometricDataCriteria(ProgramBiometricDataCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.biometricCode = other.biometricCode == null ? null : other.biometricCode.copy();
        this.biometricName = other.biometricName == null ? null : other.biometricName.copy();
        this.maleMin = other.maleMin == null ? null : other.maleMin.copy();
        this.maleMax = other.maleMax == null ? null : other.maleMax.copy();
        this.femaleMin = other.femaleMin == null ? null : other.femaleMin.copy();
        this.femaleMax = other.femaleMax == null ? null : other.femaleMax.copy();
        this.unidentifiedMin = other.unidentifiedMin == null ? null : other.unidentifiedMin.copy();
        this.unidentifiedMax = other.unidentifiedMax == null ? null : other.unidentifiedMax.copy();
    }

    @Override
    public ProgramBiometricDataCriteria copy() {
        return new ProgramBiometricDataCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getBiometricCode() {
        return biometricCode;
    }

    public void setBiometricCode(StringFilter biometricCode) {
        this.biometricCode = biometricCode;
    }

    public StringFilter getBiometricName() {
        return biometricName;
    }

    public void setBiometricName(StringFilter biometricName) {
        this.biometricName = biometricName;
    }

    public FloatFilter getMaleMin() {
        return maleMin;
    }

    public void setMaleMin(FloatFilter maleMin) {
        this.maleMin = maleMin;
    }

    public FloatFilter getMaleMax() {
        return maleMax;
    }

    public void setMaleMax(FloatFilter maleMax) {
        this.maleMax = maleMax;
    }

    public FloatFilter getFemaleMin() {
        return femaleMin;
    }

    public void setFemaleMin(FloatFilter femaleMin) {
        this.femaleMin = femaleMin;
    }

    public FloatFilter getFemaleMax() {
        return femaleMax;
    }

    public void setFemaleMax(FloatFilter femaleMax) {
        this.femaleMax = femaleMax;
    }

    public FloatFilter getUnidentifiedMin() {
        return unidentifiedMin;
    }

    public void setUnidentifiedMin(FloatFilter unidentifiedMin) {
        this.unidentifiedMin = unidentifiedMin;
    }

    public FloatFilter getUnidentifiedMax() {
        return unidentifiedMax;
    }

    public void setUnidentifiedMax(FloatFilter unidentifiedMax) {
        this.unidentifiedMax = unidentifiedMax;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramBiometricDataCriteria that = (ProgramBiometricDataCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(biometricCode, that.biometricCode) &&
            Objects.equals(biometricName, that.biometricName) &&
            Objects.equals(maleMin, that.maleMin) &&
            Objects.equals(maleMax, that.maleMax) &&
            Objects.equals(femaleMin, that.femaleMin) &&
            Objects.equals(femaleMax, that.femaleMax) &&
            Objects.equals(unidentifiedMin, that.unidentifiedMin) &&
            Objects.equals(unidentifiedMax, that.unidentifiedMax);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        biometricCode,
        biometricName,
        maleMin,
        maleMax,
        femaleMin,
        femaleMax,
        unidentifiedMin,
        unidentifiedMax
        );
    }

    @Override
    public String toString() {
        return "ProgramBiometricDataCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (biometricCode != null ? "biometricCode=" + biometricCode + ", " : "") +
                (biometricName != null ? "biometricName=" + biometricName + ", " : "") +
                (maleMin != null ? "maleMin=" + maleMin + ", " : "") +
                (maleMax != null ? "maleMax=" + maleMax + ", " : "") +
                (femaleMin != null ? "femaleMin=" + femaleMin + ", " : "") +
                (femaleMax != null ? "femaleMax=" + femaleMax + ", " : "") +
                (unidentifiedMin != null ? "unidentifiedMin=" + unidentifiedMin + ", " : "") +
                (unidentifiedMax != null ? "unidentifiedMax=" + unidentifiedMax + ", " : "") +
            "}";
    }

}
