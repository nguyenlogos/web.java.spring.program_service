package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ClientProgramRewardSetting} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ClientProgramRewardSettingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /client-program-reward-settings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClientProgramRewardSettingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter clientId;

    private StringFilter clientName;

    private InstantFilter updatedDate;

    private StringFilter updatedBy;

    private StringFilter customerIdentifier;

    private StringFilter accountIdentifier;

    private BigDecimalFilter accountThreshold;

    private BigDecimalFilter currentBalance;

    private StringFilter clientEmail;

    private LongFilter programId;

    public ClientProgramRewardSettingCriteria(){
    }

    public ClientProgramRewardSettingCriteria(ClientProgramRewardSettingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.clientName = other.clientName == null ? null : other.clientName.copy();
        this.updatedDate = other.updatedDate == null ? null : other.updatedDate.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
        this.customerIdentifier = other.customerIdentifier == null ? null : other.customerIdentifier.copy();
        this.accountIdentifier = other.accountIdentifier == null ? null : other.accountIdentifier.copy();
        this.accountThreshold = other.accountThreshold == null ? null : other.accountThreshold.copy();
        this.currentBalance = other.currentBalance == null ? null : other.currentBalance.copy();
        this.clientEmail = other.clientEmail == null ? null : other.clientEmail.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
    }

    @Override
    public ClientProgramRewardSettingCriteria copy() {
        return new ClientProgramRewardSettingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getClientName() {
        return clientName;
    }

    public void setClientName(StringFilter clientName) {
        this.clientName = clientName;
    }

    public InstantFilter getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(InstantFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public StringFilter getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(StringFilter updatedBy) {
        this.updatedBy = updatedBy;
    }

    public StringFilter getCustomerIdentifier() {
        return customerIdentifier;
    }

    public void setCustomerIdentifier(StringFilter customerIdentifier) {
        this.customerIdentifier = customerIdentifier;
    }

    public StringFilter getAccountIdentifier() {
        return accountIdentifier;
    }

    public void setAccountIdentifier(StringFilter accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }

    public BigDecimalFilter getAccountThreshold() {
        return accountThreshold;
    }

    public void setAccountThreshold(BigDecimalFilter accountThreshold) {
        this.accountThreshold = accountThreshold;
    }

    public BigDecimalFilter getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimalFilter currentBalance) {
        this.currentBalance = currentBalance;
    }

    public StringFilter getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(StringFilter clientEmail) {
        this.clientEmail = clientEmail;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClientProgramRewardSettingCriteria that = (ClientProgramRewardSettingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(clientName, that.clientName) &&
            Objects.equals(updatedDate, that.updatedDate) &&
            Objects.equals(updatedBy, that.updatedBy) &&
            Objects.equals(customerIdentifier, that.customerIdentifier) &&
            Objects.equals(accountIdentifier, that.accountIdentifier) &&
            Objects.equals(accountThreshold, that.accountThreshold) &&
            Objects.equals(currentBalance, that.currentBalance) &&
            Objects.equals(clientEmail, that.clientEmail) &&
            Objects.equals(programId, that.programId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        clientId,
        clientName,
        updatedDate,
        updatedBy,
        customerIdentifier,
        accountIdentifier,
        accountThreshold,
        currentBalance,
        clientEmail,
        programId
        );
    }

    @Override
    public String toString() {
        return "ClientProgramRewardSettingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (clientName != null ? "clientName=" + clientName + ", " : "") +
                (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
                (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
                (customerIdentifier != null ? "customerIdentifier=" + customerIdentifier + ", " : "") +
                (accountIdentifier != null ? "accountIdentifier=" + accountIdentifier + ", " : "") +
                (accountThreshold != null ? "accountThreshold=" + accountThreshold + ", " : "") +
                (currentBalance != null ? "currentBalance=" + currentBalance + ", " : "") +
                (clientEmail != null ? "clientEmail=" + clientEmail + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
            "}";
    }

}
