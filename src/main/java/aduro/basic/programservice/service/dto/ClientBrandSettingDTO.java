package aduro.basic.programservice.service.dto;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ClientBrandSetting} entity.
 */
public class ClientBrandSettingDTO implements Serializable {

    private Long id;

    @NotNull
    private String clientId;

    @NotNull
    private String subPathUrl;

    private String webLogoUrl;

    public String getLogoImageUrl() {
        return logoImageUrl;
    }

    public void setLogoImageUrl(String logoImageUrl) {
        this.logoImageUrl = logoImageUrl;
    }

    private String logoImageUrl;

    private String colorValue;

    private String clientName;

    private String clientUrl;

    private String programName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSubPathUrl() {
        return subPathUrl;
    }

    public void setSubPathUrl(String subPathUrl) {
        this.subPathUrl = subPathUrl;
    }

    public String getWebLogoUrl() {
        return webLogoUrl;
    }

    public void setWebLogoUrl(String webLogoUrl) {
        this.webLogoUrl = webLogoUrl;
    }


    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    private String programRegisterMessage;

    public String getProgramRegisterMessage() {
        return programRegisterMessage;
    }

    public void setProgramRegisterMessage(String programRegisterMessage) {
        this.programRegisterMessage = programRegisterMessage;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientBrandSettingDTO clientBrandSettingDTO = (ClientBrandSettingDTO) o;
        if (clientBrandSettingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientBrandSettingDTO.getId());
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientBrandSettingDTO{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", subPathUrl='" + getSubPathUrl() + "'" +
            ", webLogoUrl='" + getWebLogoUrl() + "'" +
            ", colorValue='" + getColorValue() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", clientUrl='" + getClientUrl() + "'" +
            "}";
    }
}
