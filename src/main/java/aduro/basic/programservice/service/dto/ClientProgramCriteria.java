package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ClientProgram} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ClientProgramResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /client-programs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClientProgramCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter clientId;

    private StringFilter client_name;

    private LongFilter programId;

    public ClientProgramCriteria(){
    }

    public ClientProgramCriteria(ClientProgramCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.client_name = other.client_name == null ? null : other.client_name.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
    }

    @Override
    public ClientProgramCriteria copy() {
        return new ClientProgramCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getClient_name() {
        return client_name;
    }

    public void setClient_name(StringFilter client_name) {
        this.client_name = client_name;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClientProgramCriteria that = (ClientProgramCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(client_name, that.client_name) &&
            Objects.equals(programId, that.programId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        clientId,
        client_name,
        programId
        );
    }

    @Override
    public String toString() {
        return "ClientProgramCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (client_name != null ? "client_name=" + client_name + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
            "}";
    }

}
