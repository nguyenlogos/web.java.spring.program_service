package aduro.basic.programservice.service.dto;
import java.time.Instant;

public class ProgramUserInfoDto extends ProgramUserDTO {

    private Instant startDate;

    private String programName;

    private Instant programEndDate;

    private Instant biometricDeadline;

    private Instant biometricLookbackDate;

    private Instant tobaccoAttestationDeadline;

    private Instant tobaccoProgramDeadline;

    private String startTimeZone;

    public String getStartTimeZone() {
        return startTimeZone;
    }

    public void setStartTimeZone(String startTimeZone) {
        this.startTimeZone = startTimeZone;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public Instant getProgramEndDate() {
        return programEndDate;
    }

    public void setProgramEndDate(Instant programEndDate) {
        this.programEndDate = programEndDate;
    }

    public Instant getBiometricDeadline() {
        return biometricDeadline;
    }

    public void setBiometricDeadline(Instant biometricDeadline) {
        this.biometricDeadline = biometricDeadline;
    }

    public Instant getBiometricLookbackDate() {
        return biometricLookbackDate;
    }

    public void setBiometricLookbackDate(Instant biometricLookbackDate) {
        this.biometricLookbackDate = biometricLookbackDate;
    }

    public Instant getTobaccoAttestationDeadline() {
        return tobaccoAttestationDeadline;
    }

    public void setTobaccoAttestationDeadline(Instant tobaccoAttestationDeadline) {
        this.tobaccoAttestationDeadline = tobaccoAttestationDeadline;
    }

    public Instant getTobaccoProgramDeadline() {
        return tobaccoProgramDeadline;
    }

    public void setTobaccoProgramDeadline(Instant tobaccoProgramDeadline) {
        this.tobaccoProgramDeadline = tobaccoProgramDeadline;
    }
}
