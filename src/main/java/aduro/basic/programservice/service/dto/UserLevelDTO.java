package aduro.basic.programservice.service.dto;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UserLevelDTO {




    public Integer getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Integer endPoint) {
        this.endPoint = endPoint;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private LocalDate deadline;

    private Integer endPoint;

    private Integer level;

    public Instant getLevelCompletedDate() {
        return levelCompletedDate;
    }

    public void setLevelCompletedDate(Instant levelCompletedDate) {
        this.levelCompletedDate = levelCompletedDate;
    }

    private Instant levelCompletedDate;

    private String name;

    private List<UserLevelActivityDTO> userLevelActivities = new ArrayList<>();

    public List<ProgramLevelPathDTO> getUserLevelPaths() {
        return userLevelPaths;
    }

    public void setUserLevelPaths(List<ProgramLevelPathDTO> userLevelPaths) {
        this.userLevelPaths = userLevelPaths;
    }

    private List<ProgramLevelPathDTO> userLevelPaths = new ArrayList<>();

    private List<UserLevelRewardDTO> userLevelRewards = new ArrayList<>();

    public List<UserLevelActivityDTO> getUserLevelActivities() {
        return userLevelActivities;
    }

    public void setUserLevelActivities(List<UserLevelActivityDTO> userLevelActivities) {
        this.userLevelActivities = userLevelActivities;
    }

    public List<UserLevelRewardDTO> getUserLevelRewards() {
        return userLevelRewards;
    }

    public void setUserLevelRewards(List<UserLevelRewardDTO> userLevelRewards) {
        this.userLevelRewards = userLevelRewards;
    }

    private List<UserLevelRewardDTO> userLevelTobaccoRewards = new ArrayList<>();
    private List<ProgramRequirementItemsDTO> tobaccoRequirementItems = new ArrayList<>();

    public List<ProgramRequirementItemsDTO> getTobaccoRequirementItems() {
        return tobaccoRequirementItems;
    }

    public void setTobaccoRequirementItems(List<ProgramRequirementItemsDTO> tobaccoRequirementItems) {
        this.tobaccoRequirementItems = tobaccoRequirementItems;
    }

    public List<UserLevelRewardDTO> getUserLevelTobaccoRewards() {
        return userLevelTobaccoRewards;
    }

    public void setUserLevelTobaccoRewards(List<UserLevelRewardDTO> userLevelTobaccoRewards) {
        this.userLevelTobaccoRewards = userLevelTobaccoRewards;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }
}
