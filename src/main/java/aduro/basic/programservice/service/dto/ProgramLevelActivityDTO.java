package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramLevelActivity} entity.
 */
public class ProgramLevelActivityDTO implements Serializable {

    private Long id;

    private String activityCode;

    @NotNull
    private String activityId;

    private String subgroupId;

    private String subgroupName;

    private Long programLevelId;

    private String programLevelDescription;

    private Boolean isDelete;

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }


    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public Long getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(Long programLevelId) {
        this.programLevelId = programLevelId;
    }

    public String getProgramLevelDescription() {
        return programLevelDescription;
    }

    public void setProgramLevelDescription(String programLevelDescription) {
        this.programLevelDescription = programLevelDescription;
    }

    public Boolean getDelete() {
        return isDelete;
    }

    public void setDelete(Boolean delete) {
        isDelete = delete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramLevelActivityDTO programLevelActivityDTO = (ProgramLevelActivityDTO) o;
        if (programLevelActivityDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programLevelActivityDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramLevelActivityDTO{" +
            "id=" + getId() +
            ", activityCode='" + getActivityCode() + "'" +
            ", activityId='" + getActivityId() + "'" +
            ", programLevel=" + getProgramLevelId() +
            ", programLevel='" + getProgramLevelDescription() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", isDelete='" + getDelete() + "'" +
            "}";
    }
}
