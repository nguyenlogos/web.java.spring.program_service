package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.OrderTransaction} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.OrderTransactionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /order-transactions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class OrderTransactionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter programName;

    private StringFilter clientName;

    private StringFilter participantName;

    private InstantFilter createdDate;

    private StringFilter message;

    private StringFilter jsonContent;

    private StringFilter email;

    private StringFilter externalOrderId;

    private LongFilter programId;

    private LongFilter userRewardId;

    public OrderTransactionCriteria(){
    }

    public OrderTransactionCriteria(OrderTransactionCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.programName = other.programName == null ? null : other.programName.copy();
        this.clientName = other.clientName == null ? null : other.clientName.copy();
        this.participantName = other.participantName == null ? null : other.participantName.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.message = other.message == null ? null : other.message.copy();
        this.jsonContent = other.jsonContent == null ? null : other.jsonContent.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.externalOrderId = other.externalOrderId == null ? null : other.externalOrderId.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.userRewardId = other.userRewardId == null ? null : other.userRewardId.copy();
    }

    @Override
    public OrderTransactionCriteria copy() {
        return new OrderTransactionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getProgramName() {
        return programName;
    }

    public void setProgramName(StringFilter programName) {
        this.programName = programName;
    }

    public StringFilter getClientName() {
        return clientName;
    }

    public void setClientName(StringFilter clientName) {
        this.clientName = clientName;
    }

    public StringFilter getParticipantName() {
        return participantName;
    }

    public void setParticipantName(StringFilter participantName) {
        this.participantName = participantName;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getMessage() {
        return message;
    }

    public void setMessage(StringFilter message) {
        this.message = message;
    }

    public StringFilter getJsonContent() {
        return jsonContent;
    }

    public void setJsonContent(StringFilter jsonContent) {
        this.jsonContent = jsonContent;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getExternalOrderId() {
        return externalOrderId;
    }

    public void setExternalOrderId(StringFilter externalOrderId) {
        this.externalOrderId = externalOrderId;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public LongFilter getUserRewardId() {
        return userRewardId;
    }

    public void setUserRewardId(LongFilter userRewardId) {
        this.userRewardId = userRewardId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OrderTransactionCriteria that = (OrderTransactionCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(programName, that.programName) &&
            Objects.equals(clientName, that.clientName) &&
            Objects.equals(participantName, that.participantName) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(message, that.message) &&
            Objects.equals(jsonContent, that.jsonContent) &&
            Objects.equals(email, that.email) &&
            Objects.equals(externalOrderId, that.externalOrderId) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(userRewardId, that.userRewardId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        programName,
        clientName,
        participantName,
        createdDate,
        message,
        jsonContent,
        email,
        externalOrderId,
        programId,
        userRewardId
        );
    }

    @Override
    public String toString() {
        return "OrderTransactionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (programName != null ? "programName=" + programName + ", " : "") +
                (clientName != null ? "clientName=" + clientName + ", " : "") +
                (participantName != null ? "participantName=" + participantName + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (message != null ? "message=" + message + ", " : "") +
                (jsonContent != null ? "jsonContent=" + jsonContent + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (externalOrderId != null ? "externalOrderId=" + externalOrderId + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (userRewardId != null ? "userRewardId=" + userRewardId + ", " : "") +
            "}";
    }

}
