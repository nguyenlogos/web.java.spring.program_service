package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.EConsent} entity.
 */
public class EConsentDTO implements Serializable {

    private Long id;

    @Size(max = 36)
    private String participantId;

    private boolean asProgramTester;

    private Long programId;

    private Boolean hasConfirmed;

    private Instant createdAt;

    private Long termAndConditionId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Boolean isHasConfirmed() {
        return hasConfirmed;
    }

    public void setHasConfirmed(Boolean hasConfirmed) {
        this.hasConfirmed = hasConfirmed;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Long getTermAndConditionId() {
        return termAndConditionId;
    }

    public void setTermAndConditionId(Long termAndConditionId) {
        this.termAndConditionId = termAndConditionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EConsentDTO eConsentDTO = (EConsentDTO) o;
        if (eConsentDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eConsentDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EConsentDTO{" +
            "id=" + getId() +
            ", participantId='" + getParticipantId() + "'" +
            ", programId=" + getProgramId() +
            ", hasConfirmed='" + isHasConfirmed() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", termAndConditionId=" + getTermAndConditionId() +
            "}";
    }

    public boolean isAsProgramTester() {
        return asProgramTester;
    }

    public void setAsProgramTester(boolean asProgramTester) {
        this.asProgramTester = asProgramTester;
    }
}
