package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.UserEventQueue} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.UserEventQueueResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-event-queues?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserEventQueueCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter eventCode;

    private StringFilter eventId;

    private InstantFilter eventDate;

    private BigDecimalFilter eventPoint;

    private StringFilter eventCategory;

    private LongFilter programUserId;

    private StringFilter executeStatus;

    private LongFilter programId;

    private InstantFilter readAt;

    private StringFilter errorMessage;

    private StringFilter currentStep;

    public UserEventQueueCriteria(){
    }

    public UserEventQueueCriteria(UserEventQueueCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.eventCode = other.eventCode == null ? null : other.eventCode.copy();
        this.eventId = other.eventId == null ? null : other.eventId.copy();
        this.eventDate = other.eventDate == null ? null : other.eventDate.copy();
        this.eventPoint = other.eventPoint == null ? null : other.eventPoint.copy();
        this.eventCategory = other.eventCategory == null ? null : other.eventCategory.copy();
        this.programUserId = other.programUserId == null ? null : other.programUserId.copy();
        this.executeStatus = other.executeStatus == null ? null : other.executeStatus.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.readAt = other.readAt == null ? null : other.readAt.copy();
        this.errorMessage = other.errorMessage == null ? null : other.errorMessage.copy();
        this.currentStep = other.currentStep == null ? null : other.currentStep.copy();
    }

    @Override
    public UserEventQueueCriteria copy() {
        return new UserEventQueueCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEventCode() {
        return eventCode;
    }

    public void setEventCode(StringFilter eventCode) {
        this.eventCode = eventCode;
    }

    public StringFilter getEventId() {
        return eventId;
    }

    public void setEventId(StringFilter eventId) {
        this.eventId = eventId;
    }

    public InstantFilter getEventDate() {
        return eventDate;
    }

    public void setEventDate(InstantFilter eventDate) {
        this.eventDate = eventDate;
    }

    public BigDecimalFilter getEventPoint() {
        return eventPoint;
    }

    public void setEventPoint(BigDecimalFilter eventPoint) {
        this.eventPoint = eventPoint;
    }

    public StringFilter getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(StringFilter eventCategory) {
        this.eventCategory = eventCategory;
    }

    public LongFilter getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(LongFilter programUserId) {
        this.programUserId = programUserId;
    }

    public StringFilter getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(StringFilter executeStatus) {
        this.executeStatus = executeStatus;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public InstantFilter getReadAt() {
        return readAt;
    }

    public void setReadAt(InstantFilter readAt) {
        this.readAt = readAt;
    }

    public StringFilter getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(StringFilter errorMessage) {
        this.errorMessage = errorMessage;
    }

    public StringFilter getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(StringFilter currentStep) {
        this.currentStep = currentStep;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserEventQueueCriteria that = (UserEventQueueCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(eventCode, that.eventCode) &&
            Objects.equals(eventId, that.eventId) &&
            Objects.equals(eventDate, that.eventDate) &&
            Objects.equals(eventPoint, that.eventPoint) &&
            Objects.equals(eventCategory, that.eventCategory) &&
            Objects.equals(programUserId, that.programUserId) &&
            Objects.equals(executeStatus, that.executeStatus) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(readAt, that.readAt) &&
            Objects.equals(errorMessage, that.errorMessage) &&
            Objects.equals(currentStep, that.currentStep);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        eventCode,
        eventId,
        eventDate,
        eventPoint,
        eventCategory,
        programUserId,
        executeStatus,
        programId,
        readAt,
        errorMessage,
        currentStep
        );
    }

    @Override
    public String toString() {
        return "UserEventQueueCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (eventCode != null ? "eventCode=" + eventCode + ", " : "") +
                (eventId != null ? "eventId=" + eventId + ", " : "") +
                (eventDate != null ? "eventDate=" + eventDate + ", " : "") +
                (eventPoint != null ? "eventPoint=" + eventPoint + ", " : "") +
                (eventCategory != null ? "eventCategory=" + eventCategory + ", " : "") +
                (programUserId != null ? "programUserId=" + programUserId + ", " : "") +
                (executeStatus != null ? "executeStatus=" + executeStatus + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (readAt != null ? "readAt=" + readAt + ", " : "") +
                (errorMessage != null ? "errorMessage=" + errorMessage + ", " : "") +
                (currentStep != null ? "currentStep=" + currentStep + ", " : "") +
            "}";
    }

}
