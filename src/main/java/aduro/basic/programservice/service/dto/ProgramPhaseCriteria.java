package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramPhase} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramPhaseResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-phases?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramPhaseCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter phaseOrder;

    private InstantFilter startDate;

    private InstantFilter endDate;

    private LongFilter programId;

    public ProgramPhaseCriteria(){
    }

    public ProgramPhaseCriteria(ProgramPhaseCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.phaseOrder = other.phaseOrder == null ? null : other.phaseOrder.copy();
        this.startDate = other.startDate == null ? null : other.startDate.copy();
        this.endDate = other.endDate == null ? null : other.endDate.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
    }

    @Override
    public ProgramPhaseCriteria copy() {
        return new ProgramPhaseCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getPhaseOrder() {
        return phaseOrder;
    }

    public void setPhaseOrder(IntegerFilter phaseOrder) {
        this.phaseOrder = phaseOrder;
    }

    public InstantFilter getStartDate() {
        return startDate;
    }

    public void setStartDate(InstantFilter startDate) {
        this.startDate = startDate;
    }

    public InstantFilter getEndDate() {
        return endDate;
    }

    public void setEndDate(InstantFilter endDate) {
        this.endDate = endDate;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramPhaseCriteria that = (ProgramPhaseCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(phaseOrder, that.phaseOrder) &&
            Objects.equals(startDate, that.startDate) &&
            Objects.equals(endDate, that.endDate) &&
            Objects.equals(programId, that.programId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        phaseOrder,
        startDate,
        endDate,
        programId
        );
    }

    @Override
    public String toString() {
        return "ProgramPhaseCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (phaseOrder != null ? "phaseOrder=" + phaseOrder + ", " : "") +
                (startDate != null ? "startDate=" + startDate + ", " : "") +
                (endDate != null ? "endDate=" + endDate + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
            "}";
    }

}
