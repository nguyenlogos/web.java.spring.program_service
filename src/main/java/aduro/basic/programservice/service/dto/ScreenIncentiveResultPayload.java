package aduro.basic.programservice.service.dto;

import java.util.List;

public class ScreenIncentiveResultPayload {
    private List<WellmetricResultDTO> screenResult;
    private List<UserEventDTO> userEvents;

    public List<UserEventDTO> getUserEvents() {
        return userEvents;
    }

    public void setUserEvents(List<UserEventDTO> userEvents) {
        this.userEvents = userEvents;
    }

    public List<WellmetricResultDTO> getScreenResult() {
        return screenResult;
    }

    public void setScreenResult(List<WellmetricResultDTO> screenResult) {
        this.screenResult = screenResult;
    }

    public ScreenIncentiveResultPayload() {
    }
}
