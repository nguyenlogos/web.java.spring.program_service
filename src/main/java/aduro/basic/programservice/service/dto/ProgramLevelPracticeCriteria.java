package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramLevelPractice} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramLevelPracticeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-level-practices?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramLevelPracticeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter practiceId;

    private StringFilter name;

    private StringFilter subgroupId;

    private StringFilter subgroupName;

    private LongFilter programLevelId;

    public ProgramLevelPracticeCriteria(){
    }

    public ProgramLevelPracticeCriteria(ProgramLevelPracticeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.practiceId = other.practiceId == null ? null : other.practiceId.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.subgroupName = other.subgroupName == null ? null : other.subgroupName.copy();
        this.programLevelId = other.programLevelId == null ? null : other.programLevelId.copy();
    }

    @Override
    public ProgramLevelPracticeCriteria copy() {
        return new ProgramLevelPracticeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(StringFilter practiceId) {
        this.practiceId = practiceId;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public StringFilter getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(StringFilter subgroupName) {
        this.subgroupName = subgroupName;
    }

    public LongFilter getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(LongFilter programLevelId) {
        this.programLevelId = programLevelId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramLevelPracticeCriteria that = (ProgramLevelPracticeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(practiceId, that.practiceId) &&
            Objects.equals(name, that.name) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(subgroupName, that.subgroupName) &&
            Objects.equals(programLevelId, that.programLevelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        practiceId,
        name,
        subgroupId,
        subgroupName,
        programLevelId
        );
    }

    @Override
    public String toString() {
        return "ProgramLevelPracticeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (practiceId != null ? "practiceId=" + practiceId + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (subgroupName != null ? "subgroupName=" + subgroupName + ", " : "") +
                (programLevelId != null ? "programLevelId=" + programLevelId + ", " : "") +
            "}";
    }

}
