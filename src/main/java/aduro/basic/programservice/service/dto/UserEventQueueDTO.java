package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.UserEventQueue} entity.
 */
public class UserEventQueueDTO implements Serializable {

    private Long id;

    @NotNull
    private String eventCode;

    @NotNull
    private String eventId;

    @NotNull
    private Instant eventDate;

    private BigDecimal eventPoint;

    private String eventCategory;

    @NotNull
    private Long programUserId;

    @NotNull
    private String executeStatus;

    @NotNull
    private Long programId;

    private Instant readAt;

    private String errorMessage;

    private String currentStep;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Instant getEventDate() {
        return eventDate;
    }

    public void setEventDate(Instant eventDate) {
        this.eventDate = eventDate;
    }

    public BigDecimal getEventPoint() {
        return eventPoint;
    }

    public void setEventPoint(BigDecimal eventPoint) {
        this.eventPoint = eventPoint;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public Long getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(Long programUserId) {
        this.programUserId = programUserId;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Instant getReadAt() {
        return readAt;
    }

    public void setReadAt(Instant readAt) {
        this.readAt = readAt;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(String currentStep) {
        this.currentStep = currentStep;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserEventQueueDTO userEventQueueDTO = (UserEventQueueDTO) o;
        if (userEventQueueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userEventQueueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserEventQueueDTO{" +
            "id=" + getId() +
            ", eventCode='" + getEventCode() + "'" +
            ", eventId='" + getEventId() + "'" +
            ", eventDate='" + getEventDate() + "'" +
            ", eventPoint=" + getEventPoint() +
            ", eventCategory='" + getEventCategory() + "'" +
            ", programUserId=" + getProgramUserId() +
            ", executeStatus='" + getExecuteStatus() + "'" +
            ", programId=" + getProgramId() +
            ", readAt='" + getReadAt() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", currentStep='" + getCurrentStep() + "'" +
            "}";
    }
}
