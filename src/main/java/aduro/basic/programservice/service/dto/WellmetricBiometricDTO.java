package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.enumeration.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.type.descriptor.java.DataHelper;

import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class WellmetricBiometricDTO implements Serializable {

    public boolean isFasting(){
        try {
            Float f = Float.parseFloat(isFastingString);
            if(Float.isNaN(f)){
                return false;
            }

            return f > 0;
        }catch (Exception e) {
            return false;
        }
    }
    private float fastingYN; //Glucose
    @JsonProperty("isFasting")
    private String isFastingString;
    private String gender;
    private float systolic;
    private float diastolic;
    private float bmi;
    private float tcHdl;
    private float waist;
    private float tc;
    private float hdl;
    private float triglycerides;

    @JsonProperty("HR_Pulse__c")
    private float heartRate;
    @JsonProperty("a1c")
    private float a1C;
    @JsonProperty("tobacco")
    private float tobaccoUse;
    @JsonProperty("waist_to_height_ratio")
    private float wthr;
    @JsonProperty("RLdl__c")
    private float ldl;

    @JsonProperty("attemptedAt")
    @JsonFormat(pattern = Constants.DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT)
    private Date dateOfTest;

    public Gender getGender(){
        Gender genderType = Gender.UNIDENTIFIED;
        try {
            genderType = Gender.valueOf(this.gender);
        }
        catch (Exception ex){
            if(gender.toLowerCase().startsWith("m")){
                genderType = Gender.MALE;
            }else if(gender.toLowerCase().startsWith("f")){
                genderType = Gender.FEMALE;
            }
        }

        return genderType;
    }
}


