package aduro.basic.programservice.service.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ItemCollectionResponseDto {
    private boolean isScreening;
    private List<Integer> activities;
    private List<Integer> paths;
}
