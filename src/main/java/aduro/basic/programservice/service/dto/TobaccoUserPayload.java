package aduro.basic.programservice.service.dto;

import javax.validation.constraints.NotNull;

public class TobaccoUserPayload {

    @NotNull
    private String participantId;

    private Boolean isTobaccoUser;

    @NotNull
    private Long programId;

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public Boolean getIsTobaccoUser() {
        return isTobaccoUser;
    }

    public Boolean isTobaccoUser() {
        return isTobaccoUser;
    }

    public void setIsTobaccoUser(Boolean tobaccoUser) {
        isTobaccoUser = tobaccoUser;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    @Override
    public String toString() {
        return "TobaccoUserPayload{" +
            ", participantId='" + participantId + '\'' +
            ", isTobaccoUser=" + isTobaccoUser +
            ", programId=" + programId +
            '}';
    }
}
