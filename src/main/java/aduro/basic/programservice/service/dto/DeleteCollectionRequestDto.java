package aduro.basic.programservice.service.dto;

import lombok.*;

import java.util.List;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DeleteCollectionRequestDto {

    private Long id;

    private Boolean isDeleteConfirmation;

    private boolean isSuccess;

    private List<String> cohorts;

}
