package aduro.basic.programservice.service.dto;

import lombok.*;

import java.util.List;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CustomActivityMapperResponseDto {


    private List<CloneActivityDTO> oldToNewMapping;


}

