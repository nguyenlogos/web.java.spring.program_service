package aduro.basic.programservice.service.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class UpdatedSubgroupParticipantPayload {

    @NotNull
    @NotBlank
    public String participantId;

    public ProgramSubgroupDTO subgroup;

    public Boolean isAddNew;

    public String message;

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public ProgramSubgroupDTO getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(ProgramSubgroupDTO subgroup) {
        this.subgroup = subgroup;
    }

    public Boolean getIsAddNew() {
        return isAddNew;
    }

    public void setIsAddNew(Boolean addNew) {
        isAddNew = addNew;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "UpdatedSubgroupParticipantPayload{" +
            "participantId='" + participantId + '\'' +
            ", subgroup=" + subgroup +
            ", isAddNew=" + isAddNew +
            '}';
    }
}
