package aduro.basic.programservice.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramMemberConsentDetail} entity.
 */


public class ProgramMemberConsentDetailDTO  implements Serializable{

  private Long id;
    @NotNull
  private String clientId;
    @NotNull
  private Long programId;
    @NotNull
  private Long consentId;
  private String consentName;
    @NotNull
  private String participantId;
  private Date consentDate;
  private Long programUserId;
  private double consentVersion;
  private Boolean statusConsent;
  private String createdBy;
  private Instant createdDate;
  private String modifiedBy;
  private Instant modifiedDate;
  private String deletedBy;
  private Instant deletedDate;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public Long getProgramUserId() { return programUserId; }

    public void setProgramUserId(Long programUserId) { this.programUserId = programUserId; }

    public Boolean getStatusConsent() { return statusConsent; }
    public void setStatusConsent(Boolean statusConsent) { this.statusConsent = statusConsent; }

  public Long getProgramId() {
    return programId;
  }

  public void setProgramId(Long programId) {
    this.programId = programId;
  }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }
    public Long getConsentId() {
        return consentId;
    }

    public void setConsentId(Long consentId) {
        this.consentId = consentId;
    }

  public String getConsentName() {
    return consentName;
  }

  public void setConsentName(String consentName) {
    this.consentName = consentName;
  }

  public Date getConsentDate() {
    return consentDate;
  }

  public void setConsentDate(Date consentDate) {
    this.consentDate = consentDate;
  }


  public double getConsentVersion() {
    return consentVersion;
  }

  public void setConsentVersion(double consentVersion) {
    this.consentVersion = consentVersion;
  }


  public String getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }


  public Instant getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Instant createdDate) {
    this.createdDate = createdDate;
  }


  public String getModifiedBy() {
    return modifiedBy;
  }

  public void setModifiedBy(String modifiedBy) {
    this.modifiedBy = modifiedBy;
  }


  public Instant getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(Instant modifiedDate) {
    this.modifiedDate = modifiedDate;
  }


  public String getDeletedBy() {
    return deletedBy;
  }

  public void setDeletedBy(String deletedBy) {
    this.deletedBy = deletedBy;
  }


  public Instant getDeletedDate() {
    return deletedDate;
  }

  public void setDeletedDate(Instant deletedDate) {
    this.deletedDate = deletedDate;
  }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ActivityEventQueueDTO activityEventQueueDTO = (ActivityEventQueueDTO) o;
        if (activityEventQueueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), activityEventQueueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramMemberConsentDetailDTO{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", programId='" + getProgramId() + "'" +
            ", programUserId='" + getProgramUserId() + "'" +
            ", participantId='" + getParticipantId() + "'" +
            ", consentName='" + getConsentName() + "'" +
            ", consentDate='" + getConsentDate() + "'" +
            ", consentVersion='" + getConsentVersion() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", deletedBy='" + getDeletedBy() + "'" +
            ", deletedDate='" + getDeletedDate() + "'" +
            "}";
    }
}

