package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramUser} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramUserResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-users?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramUserCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter participantId;

    private StringFilter clientId;

    private BigDecimalFilter totalUserPoint;

    private LongFilter programId;

    private IntegerFilter currentLevel;

    private StringFilter email;

    private StringFilter firstName;

    private StringFilter lastName;

    private StringFilter phone;

    private StringFilter clientName;

    private StringFilter subgroupId;

    private StringFilter subgroupName;

    private BooleanFilter isTobaccoUser;

    public ProgramUserCriteria(){
    }

    public ProgramUserCriteria(ProgramUserCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.participantId = other.participantId == null ? null : other.participantId.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.totalUserPoint = other.totalUserPoint == null ? null : other.totalUserPoint.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.currentLevel = other.currentLevel == null ? null : other.currentLevel.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.firstName = other.firstName == null ? null : other.firstName.copy();
        this.lastName = other.lastName == null ? null : other.lastName.copy();
        this.phone = other.phone == null ? null : other.phone.copy();
        this.clientName = other.clientName == null ? null : other.clientName.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.subgroupName = other.subgroupName == null ? null : other.subgroupName.copy();
        this.isTobaccoUser = other.isTobaccoUser == null ? null : other.isTobaccoUser.copy();
    }

    public BooleanFilter getIsTobaccoUser() {
        return isTobaccoUser;
    }

    public void setIsTobaccoUser(BooleanFilter isTobaccoUser) {
        this.isTobaccoUser = isTobaccoUser;
    }

    @Override
    public ProgramUserCriteria copy() {
        return new ProgramUserCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getParticipantId() {
        return participantId;
    }

    public void setParticipantId(StringFilter participantId) {
        this.participantId = participantId;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public BigDecimalFilter getTotalUserPoint() {
        return totalUserPoint;
    }

    public void setTotalUserPoint(BigDecimalFilter totalUserPoint) {
        this.totalUserPoint = totalUserPoint;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public IntegerFilter getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(IntegerFilter currentLevel) {
        this.currentLevel = currentLevel;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getFirstName() {
        return firstName;
    }

    public void setFirstName(StringFilter firstName) {
        this.firstName = firstName;
    }

    public StringFilter getLastName() {
        return lastName;
    }

    public void setLastName(StringFilter lastName) {
        this.lastName = lastName;
    }

    public StringFilter getPhone() {
        return phone;
    }

    public void setPhone(StringFilter phone) {
        this.phone = phone;
    }

    public StringFilter getClientName() {
        return clientName;
    }

    public void setClientName(StringFilter clientName) {
        this.clientName = clientName;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public StringFilter getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(StringFilter subgroupName) {
        this.subgroupName = subgroupName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramUserCriteria that = (ProgramUserCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(participantId, that.participantId) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(totalUserPoint, that.totalUserPoint) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(currentLevel, that.currentLevel) &&
            Objects.equals(email, that.email) &&
            Objects.equals(firstName, that.firstName) &&
            Objects.equals(lastName, that.lastName) &&
            Objects.equals(phone, that.phone) &&
            Objects.equals(clientName, that.clientName) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(subgroupName, that.subgroupName) &&
                Objects.equals(isTobaccoUser, that.isTobaccoUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        participantId,
        clientId,
        totalUserPoint,
        programId,
        currentLevel,
        email,
        firstName,
        lastName,
        phone,
        clientName,
        subgroupId,
        subgroupName,
            isTobaccoUser
        );
    }

    @Override
    public String toString() {
        return "ProgramUserCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (participantId != null ? "participantId=" + participantId + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (totalUserPoint != null ? "totalUserPoint=" + totalUserPoint + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (currentLevel != null ? "currentLevel=" + currentLevel + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (firstName != null ? "firstName=" + firstName + ", " : "") +
                (lastName != null ? "lastName=" + lastName + ", " : "") +
                (phone != null ? "phone=" + phone + ", " : "") +
                (clientName != null ? "clientName=" + clientName + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (subgroupName != null ? "subgroupName=" + subgroupName + ", " : "") +
            (isTobaccoUser != null ? "isTobaccoUser=" + isTobaccoUser + ", " : "") +
            "}";
    }

}
