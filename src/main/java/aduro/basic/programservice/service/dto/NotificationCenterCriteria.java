package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import aduro.basic.programservice.domain.enumeration.NotificationType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.NotificationCenter} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.NotificationCenterResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /notification-centers?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NotificationCenterCriteria implements Serializable, Criteria {
    /**
     * Class for filtering NotificationType
     */
    public static class NotificationTypeFilter extends Filter<NotificationType> {

        public NotificationTypeFilter() {
        }

        public NotificationTypeFilter(NotificationTypeFilter filter) {
            super(filter);
        }

        @Override
        public NotificationTypeFilter copy() {
            return new NotificationTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter title;

    private StringFilter participantId;

    private StringFilter executeStatus;

    private InstantFilter createdAt;

    private IntegerFilter attemptCount;

    private InstantFilter lastAttemptTime;

    private StringFilter errorMessage;

    private StringFilter content;

    private NotificationTypeFilter notificationType;

    private StringFilter eventId;

    private BooleanFilter isReady;

    private StringFilter receiverName;

    public NotificationCenterCriteria(){
    }

    public NotificationCenterCriteria(NotificationCenterCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.title = other.title == null ? null : other.title.copy();
        this.participantId = other.participantId == null ? null : other.participantId.copy();
        this.executeStatus = other.executeStatus == null ? null : other.executeStatus.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.attemptCount = other.attemptCount == null ? null : other.attemptCount.copy();
        this.lastAttemptTime = other.lastAttemptTime == null ? null : other.lastAttemptTime.copy();
        this.errorMessage = other.errorMessage == null ? null : other.errorMessage.copy();
        this.content = other.content == null ? null : other.content.copy();
        this.notificationType = other.notificationType == null ? null : other.notificationType.copy();
        this.eventId = other.eventId == null ? null : other.eventId.copy();
        this.isReady = other.isReady == null ? null : other.isReady.copy();
        this.receiverName = other.receiverName == null ? null : other.receiverName.copy();
    }

    @Override
    public NotificationCenterCriteria copy() {
        return new NotificationCenterCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTitle() {
        return title;
    }

    public void setTitle(StringFilter title) {
        this.title = title;
    }

    public StringFilter getParticipantId() {
        return participantId;
    }

    public void setParticipantId(StringFilter participantId) {
        this.participantId = participantId;
    }

    public StringFilter getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(StringFilter executeStatus) {
        this.executeStatus = executeStatus;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public IntegerFilter getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(IntegerFilter attemptCount) {
        this.attemptCount = attemptCount;
    }

    public InstantFilter getLastAttemptTime() {
        return lastAttemptTime;
    }

    public void setLastAttemptTime(InstantFilter lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public StringFilter getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(StringFilter errorMessage) {
        this.errorMessage = errorMessage;
    }

    public StringFilter getContent() {
        return content;
    }

    public void setContent(StringFilter content) {
        this.content = content;
    }

    public NotificationTypeFilter getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(NotificationTypeFilter notificationType) {
        this.notificationType = notificationType;
    }

    public StringFilter getEventId() {
        return eventId;
    }

    public void setEventId(StringFilter eventId) {
        this.eventId = eventId;
    }

    public BooleanFilter getIsReady() {
        return isReady;
    }

    public void setIsReady(BooleanFilter isReady) {
        this.isReady = isReady;
    }

    public StringFilter getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(StringFilter receiverName) {
        this.receiverName = receiverName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NotificationCenterCriteria that = (NotificationCenterCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(title, that.title) &&
            Objects.equals(participantId, that.participantId) &&
            Objects.equals(executeStatus, that.executeStatus) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(attemptCount, that.attemptCount) &&
            Objects.equals(lastAttemptTime, that.lastAttemptTime) &&
            Objects.equals(errorMessage, that.errorMessage) &&
            Objects.equals(content, that.content) &&
            Objects.equals(notificationType, that.notificationType) &&
            Objects.equals(eventId, that.eventId) &&
            Objects.equals(isReady, that.isReady) &&
            Objects.equals(receiverName, that.receiverName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        title,
        participantId,
        executeStatus,
        createdAt,
        attemptCount,
        lastAttemptTime,
        errorMessage,
        content,
        notificationType,
        eventId,
        isReady,
        receiverName
        );
    }

    @Override
    public String toString() {
        return "NotificationCenterCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (title != null ? "title=" + title + ", " : "") +
                (participantId != null ? "participantId=" + participantId + ", " : "") +
                (executeStatus != null ? "executeStatus=" + executeStatus + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (attemptCount != null ? "attemptCount=" + attemptCount + ", " : "") +
                (lastAttemptTime != null ? "lastAttemptTime=" + lastAttemptTime + ", " : "") +
                (errorMessage != null ? "errorMessage=" + errorMessage + ", " : "") +
                (content != null ? "content=" + content + ", " : "") +
                (notificationType != null ? "notificationType=" + notificationType + ", " : "") +
                (eventId != null ? "eventId=" + eventId + ", " : "") +
                (isReady != null ? "isReady=" + isReady + ", " : "") +
                (receiverName != null ? "receiverName=" + receiverName + ", " : "") +
            "}";
    }

}
