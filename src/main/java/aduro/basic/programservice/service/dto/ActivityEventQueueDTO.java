package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.persistence.Column;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ActivityEventQueue} entity.
 */
public class ActivityEventQueueDTO implements Serializable {

    private Long id;

    @NotNull
    private String participantId;

    @NotNull
    private String clientId;

    @NotNull
    private String activityCode;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private String email;

    @NotNull
    private Instant createdDate;

    private String transactionId;

    private String executeStatus;

    private String errorMessage;

    private String subgroupId;

    @NotNull
    private Long customActivityId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public Long getCustomActivityId() {
        return customActivityId;
    }

    public void setCustomActivityId(Long customActivityId) {
        this.customActivityId = customActivityId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ActivityEventQueueDTO activityEventQueueDTO = (ActivityEventQueueDTO) o;
        if (activityEventQueueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), activityEventQueueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ActivityEventQueueDTO{" +
            "id=" + getId() +
            ", participantId='" + getParticipantId() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", activityCode='" + getActivityCode() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", transactionId='" + getTransactionId() + "'" +
            ", executeStatus='" + getExecuteStatus() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            "}";
    }
}
