package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramLevelReward} entity.
 */
public class ProgramLevelRewardDTO implements Serializable {

    private Long id;

    private String description;

    @Min(value = 1)
    private Integer quantity;

    @NotNull
    private String code;

    @NotNull
    @DecimalMin(value = "1")
    private BigDecimal rewardAmount;

    @NotNull
    private String rewardType;

    private String campaignId;

    private String subgroupId;


    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    private String subgroupName;

    private Long programLevelId;

    private String programLevelName;

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public Long getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(Long programLevelId) {
        this.programLevelId = programLevelId;
    }

    public String getProgramLevelName() {
        return programLevelName;
    }

    public void setProgramLevelName(String programLevelName) {
        this.programLevelName = programLevelName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramLevelRewardDTO programLevelRewardDTO = (ProgramLevelRewardDTO) o;
        if (programLevelRewardDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programLevelRewardDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramLevelRewardDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", quantity=" + getQuantity() +
            ", code='" + getCode() + "'" +
            ", rewardAmount=" + getRewardAmount() +
            ", rewardType='" + getRewardType() + "'" +
            ", campaignId='" + getCampaignId() + "'" +
            ", programLevel=" + getProgramLevelId() +
            ", programLevel='" + getProgramLevelName() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            "}";
    }
}
