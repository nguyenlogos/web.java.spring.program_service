package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ClientSubDomain} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ClientSubDomainResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /client-sub-domains?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClientSubDomainCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter clientId;

    private StringFilter subDomainUrl;

    private InstantFilter createdAt;

    private InstantFilter modifiedAt;

    private LongFilter programId;

    private StringFilter programName;

    public ClientSubDomainCriteria(){
    }

    public ClientSubDomainCriteria(ClientSubDomainCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.subDomainUrl = other.subDomainUrl == null ? null : other.subDomainUrl.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.modifiedAt = other.modifiedAt == null ? null : other.modifiedAt.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.programName = other.programName == null ? null : other.programName.copy();
    }

    @Override
    public ClientSubDomainCriteria copy() {
        return new ClientSubDomainCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getSubDomainUrl() {
        return subDomainUrl;
    }

    public void setSubDomainUrl(StringFilter subDomainUrl) {
        this.subDomainUrl = subDomainUrl;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public InstantFilter getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(InstantFilter modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public StringFilter getProgramName() {
        return programName;
    }

    public void setProgramName(StringFilter programName) {
        this.programName = programName;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClientSubDomainCriteria that = (ClientSubDomainCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(subDomainUrl, that.subDomainUrl) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(modifiedAt, that.modifiedAt) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(programName, that.programName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        clientId,
        subDomainUrl,
        createdAt,
        modifiedAt,
        programId,
        programName
        );
    }

    @Override
    public String toString() {
        return "ClientSubDomainCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (subDomainUrl != null ? "subDomainUrl=" + subDomainUrl + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (modifiedAt != null ? "modifiedAt=" + modifiedAt + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (programName != null ? "programName=" + programName + ", " : "") +
            "}";
    }

}
