package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ClientSubDomain} entity.
 */
public class ClientSubDomainDTO implements Serializable {

    private Long id;

    @NotNull
    private String clientId;

    @NotNull
    private String subDomainUrl;

    private Instant createdAt;

    private Instant modifiedAt;

    private Long programId;

    private String programName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSubDomainUrl() {
        return subDomainUrl;
    }

    public void setSubDomainUrl(String subDomainUrl) {
        this.subDomainUrl = subDomainUrl;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Instant modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientSubDomainDTO clientSubDomainDTO = (ClientSubDomainDTO) o;
        if (clientSubDomainDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientSubDomainDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientSubDomainDTO{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", subDomainUrl='" + getSubDomainUrl() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", modifiedAt='" + getModifiedAt() + "'" +
            ", programId=" + getProgramId() +
            ", programName='" + getProgramName() + "'" +
            "}";
    }
}
