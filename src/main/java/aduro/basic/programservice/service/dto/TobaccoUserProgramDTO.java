package aduro.basic.programservice.service.dto;

import java.time.Instant;

public class TobaccoUserProgramDTO extends ProgramUserDTO  implements Cloneable{

    private Long userApId;

    private String tobaccoCompleted;

    private Instant completedAt;

    private String goalId;

    private String activityId;

    private String pathId;

    private String status;

    private String progress;

    private String enrollType;

    private String surveyActivityTitle;

    private String pathRasTitle;

    private String activityRasTitle;

    private String pathRasStatus;

    private String activityRasStatus;

    private String coachingStatus;

    private String coachingProgress;

    public String getCoachingProgress() {
        return coachingProgress;
    }

    public void setCoachingProgress(String coachingProgress) {
        this.coachingProgress = coachingProgress;
    }

    public Instant getCompletedAt() {
        return completedAt;
    }

    public void setCompletedAt(Instant completedAt) {
        this.completedAt = completedAt;
    }

    public String getTobaccoCompleted() {
        return tobaccoCompleted;
    }

    public String getGoalId() {
        return goalId;
    }

    public void setGoalId(String goalId) {
        this.goalId = goalId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getEnrollType() {
        return enrollType;
    }

    public void setEnrollType(String enrollType) {
        this.enrollType = enrollType;
    }

    public void setTobaccoCompleted(String tobaccoCompleted) {
        this.tobaccoCompleted = tobaccoCompleted;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public Long getUserApId() {
        return userApId;
    }

    public void setUserApId(Long userApId) {
        this.userApId = userApId;
    }

    public String getPathId() {
        return pathId;
    }

    public void setPathId(String pathId) {
        this.pathId = pathId;
    }

    public String getSurveyActivityTitle() {
        return surveyActivityTitle;
    }

    public void setSurveyActivityTitle(String surveyActivityTitle) {
        this.surveyActivityTitle = surveyActivityTitle;
    }

    public String getPathRasTitle() {
        return pathRasTitle;
    }

    public void setPathRasTitle(String pathRasTitle) {
        this.pathRasTitle = pathRasTitle;
    }

    public String getActivityRasTitle() {
        return activityRasTitle;
    }

    public void setActivityRasTitle(String activityRasTitle) {
        this.activityRasTitle = activityRasTitle;
    }

    public String getPathRasStatus() {
        return pathRasStatus;
    }

    public void setPathRasStatus(String pathRasStatus) {
        this.pathRasStatus = pathRasStatus;
    }

    public String getActivityRasStatus() {
        return activityRasStatus;
    }

    public void setActivityRasStatus(String activityRasStatus) {
        this.activityRasStatus = activityRasStatus;
    }

    public String getCoachingStatus() {
        return coachingStatus;
    }

    public void setCoachingStatus(String coachingStatus) {
        this.coachingStatus = coachingStatus;
    }

    public Object clone() throws
        CloneNotSupportedException
    {
        return super.clone();
    }
}
