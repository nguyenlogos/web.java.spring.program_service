package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.domain.enumeration.ProcessLevelType;
import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UpdateLevelRequest {
    private Long id;
    private String clientId;
    private String participantId;
    private Long programId;
    private ProcessLevelType type;
    private Boolean isCompleted;
    private Boolean isForce;
}
