package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;

import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.enumeration.Gender;
import aduro.basic.programservice.domain.enumeration.ProgramType;
import aduro.basic.programservice.web.rest.ProgramResource;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link Program} entity. This class is used
 * in {@link ProgramResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /programs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private LocalDateFilter lastSent;

    private BooleanFilter isRetriggerEmail;

    private BooleanFilter isEligible;

    private BooleanFilter isSentRegistrationEmail;

    private BooleanFilter isRegisteredForPlatform;

    private BooleanFilter isScheduledScreening;

    private BooleanFilter isFunctionally;

    private StringFilter logoUrl;

    private StringFilter description;

    private BigDecimalFilter userPoint;

    private InstantFilter lastModifiedDate;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private StringFilter createdBy;

    private StringFilter status;

    private BooleanFilter isUsePoint;

    private BooleanFilter isScreen;

    private BooleanFilter isCoaching;

    private BooleanFilter isWellMatric;

    private BooleanFilter isHP;

    private BooleanFilter isUseLevel;

    private InstantFilter startDate;

    private InstantFilter resetDate;

    private BooleanFilter isTemplate;

    private BooleanFilter isPreview;

    private InstantFilter previewDate;

    private StringFilter previewTimeZone;

    private StringFilter startTimeZone;

    private StringFilter endTimeZone;

    private StringFilter levelStructure;

    private IntegerFilter programLength;

    private BooleanFilter isHPSF;

    private BooleanFilter isHTK;

    private BooleanFilter isLabcorp;

    private StringFilter labcorpAccountNumber;

    private StringFilter labcorpFileUrl;

    private BooleanFilter applyRewardAllSubgroup;

    private InstantFilter biometricDeadlineDate;

    private InstantFilter biometricLookbackDate;

    private StringFilter landingBackgroundImageUrl;

    private ProgramTypeFilter programType;

    private BooleanFilter isTobaccoSurchargeManagement;

    private InstantFilter tobaccoAttestationDeadline;

    private InstantFilter tobaccoProgramDeadline;

    private LongFilter programCategoryPointId;

    private LongFilter programHealthyRangeId;

    private StringFilter clientId;

    private BooleanFilter isAwardedForTobaccoRas;

    private BooleanFilter isAwardedForTobaccoAttestation;

    private BooleanFilter qaVerify;

    private BigDecimalFilter economyPoint;

    private BooleanFilter isExtractable;

    public ProgramCriteria(){
    }

    public ProgramCriteria(ProgramCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.lastSent = other.lastSent == null ? null : other.lastSent.copy();
        this.isRetriggerEmail = other.isRetriggerEmail == null ? null : other.isRetriggerEmail.copy();
        this.isEligible = other.isEligible == null ? null : other.isEligible.copy();
        this.isSentRegistrationEmail = other.isSentRegistrationEmail == null ? null : other.isSentRegistrationEmail.copy();
        this.isRegisteredForPlatform = other.isRegisteredForPlatform == null ? null : other.isRegisteredForPlatform.copy();
        this.isScheduledScreening = other.isScheduledScreening == null ? null : other.isScheduledScreening.copy();
        this.isFunctionally = other.isFunctionally == null ? null : other.isFunctionally.copy();
        this.logoUrl = other.logoUrl == null ? null : other.logoUrl.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.userPoint = other.userPoint == null ? null : other.userPoint.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.isUsePoint = other.isUsePoint == null ? null : other.isUsePoint.copy();
        this.isScreen = other.isScreen == null ? null : other.isScreen.copy();
        this.isCoaching = other.isCoaching == null ? null : other.isCoaching.copy();
        this.isWellMatric = other.isWellMatric == null ? null : other.isWellMatric.copy();
        this.isHP = other.isHP == null ? null : other.isHP.copy();
        this.isUseLevel = other.isUseLevel == null ? null : other.isUseLevel.copy();
        this.startDate = other.startDate == null ? null : other.startDate.copy();
        this.resetDate = other.resetDate == null ? null : other.resetDate.copy();
        this.isTemplate = other.isTemplate == null ? null : other.isTemplate.copy();
        this.isPreview = other.isPreview == null ? null : other.isPreview.copy();
        this.previewDate = other.previewDate == null ? null : other.previewDate.copy();
        this.previewTimeZone = other.previewTimeZone == null ? null : other.previewTimeZone.copy();
        this.startTimeZone = other.startTimeZone == null ? null : other.startTimeZone.copy();
        this.endTimeZone = other.endTimeZone == null ? null : other.endTimeZone.copy();
        this.levelStructure = other.levelStructure == null ? null : other.levelStructure.copy();
        this.programLength = other.programLength == null ? null : other.programLength.copy();
        this.isHPSF = other.isHPSF == null ? null : other.isHPSF.copy();
        this.isHTK = other.isHTK == null ? null : other.isHTK.copy();
        this.isLabcorp = other.isLabcorp == null ? null : other.isLabcorp.copy();
        this.labcorpAccountNumber = other.labcorpAccountNumber == null ? null : other.labcorpAccountNumber.copy();
        this.labcorpFileUrl = other.labcorpFileUrl == null ? null : other.labcorpFileUrl.copy();
        this.applyRewardAllSubgroup = other.applyRewardAllSubgroup == null ? null : other.applyRewardAllSubgroup.copy();
        this.biometricDeadlineDate = other.biometricDeadlineDate == null ? null : other.biometricDeadlineDate.copy();
        this.biometricLookbackDate = other.biometricLookbackDate == null ? null : other.biometricLookbackDate.copy();
        this.landingBackgroundImageUrl = other.landingBackgroundImageUrl == null ? null : other.landingBackgroundImageUrl.copy();
        this.programType = other.programType == null ? null : other.programType.copy();
        this.isTobaccoSurchargeManagement = other.isTobaccoSurchargeManagement == null ? null : other.isTobaccoSurchargeManagement.copy();
        this.tobaccoAttestationDeadline = other.tobaccoAttestationDeadline == null ? null : other.tobaccoAttestationDeadline.copy();
        this.tobaccoProgramDeadline = other.tobaccoProgramDeadline == null ? null : other.tobaccoProgramDeadline.copy();
        this.programCategoryPointId = other.programCategoryPointId == null ? null : other.programCategoryPointId.copy();
        this.programHealthyRangeId = other.programHealthyRangeId == null ? null : other.programHealthyRangeId.copy();
        this.economyPoint = other.economyPoint == null ? null : other.economyPoint.copy();
        this.isExtractable = other.isExtractable == null ? null : other.isExtractable.copy();
    }

    @Override
    public ProgramCriteria copy() {
        return new ProgramCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LocalDateFilter getLastSent() {
        return lastSent;
    }

    public void setLastSent(LocalDateFilter lastSent) {
        this.lastSent = lastSent;
    }

    public BooleanFilter getIsRetriggerEmail() {
        return isRetriggerEmail;
    }

    public void setIsRetriggerEmail(BooleanFilter isRetriggerEmail) {
        this.isRetriggerEmail = isRetriggerEmail;
    }

    public BooleanFilter getIsEligible() {
        return isEligible;
    }

    public void setIsEligible(BooleanFilter isEligible) {
        this.isEligible = isEligible;
    }

    public BooleanFilter getIsSentRegistrationEmail() {
        return isSentRegistrationEmail;
    }

    public void setIsSentRegistrationEmail(BooleanFilter isSentRegistrationEmail) {
        this.isSentRegistrationEmail = isSentRegistrationEmail;
    }

    public BooleanFilter getIsRegisteredForPlatform() {
        return isRegisteredForPlatform;
    }

    public void setIsRegisteredForPlatform(BooleanFilter isRegisteredForPlatform) {
        this.isRegisteredForPlatform = isRegisteredForPlatform;
    }

    public BooleanFilter getIsScheduledScreening() {
        return isScheduledScreening;
    }

    public void setIsScheduledScreening(BooleanFilter isScheduledScreening) {
        this.isScheduledScreening = isScheduledScreening;
    }

    public BooleanFilter getIsFunctionally() {
        return isFunctionally;
    }

    public void setIsFunctionally(BooleanFilter isFunctionally) {
        this.isFunctionally = isFunctionally;
    }

    public StringFilter getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(StringFilter logoUrl) {
        this.logoUrl = logoUrl;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public BigDecimalFilter getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(BigDecimalFilter userPoint) {
        this.userPoint = userPoint;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public BooleanFilter getIsUsePoint() {
        return isUsePoint;
    }

    public void setIsUsePoint(BooleanFilter isUsePoint) {
        this.isUsePoint = isUsePoint;
    }

    public BooleanFilter getIsScreen() {
        return isScreen;
    }

    public void setIsScreen(BooleanFilter isScreen) {
        this.isScreen = isScreen;
    }

    public BooleanFilter getIsCoaching() {
        return isCoaching;
    }

    public void setIsCoaching(BooleanFilter isCoaching) {
        this.isCoaching = isCoaching;
    }

    public BooleanFilter getIsWellMatric() {
        return isWellMatric;
    }

    public void setIsWellMatric(BooleanFilter isWellMatric) {
        this.isWellMatric = isWellMatric;
    }

    public BooleanFilter getIsHP() {
        return isHP;
    }

    public void setIsHP(BooleanFilter isHP) {
        this.isHP = isHP;
    }

    public BooleanFilter getIsUseLevel() {
        return isUseLevel;
    }

    public void setIsUseLevel(BooleanFilter isUseLevel) {
        this.isUseLevel = isUseLevel;
    }

    public InstantFilter getStartDate() {
        return startDate;
    }

    public void setStartDate(InstantFilter startDate) {
        this.startDate = startDate;
    }

    public InstantFilter getResetDate() {
        return resetDate;
    }

    public void setResetDate(InstantFilter resetDate) {
        this.resetDate = resetDate;
    }

    public BooleanFilter getIsTemplate() {
        return isTemplate;
    }

    public void setIsTemplate(BooleanFilter isTemplate) {
        this.isTemplate = isTemplate;
    }

    public BooleanFilter getIsPreview() {
        return isPreview;
    }

    public void setIsPreview(BooleanFilter isPreview) {
        this.isPreview = isPreview;
    }

    public InstantFilter getPreviewDate() {
        return previewDate;
    }

    public void setPreviewDate(InstantFilter previewDate) {
        this.previewDate = previewDate;
    }

    public StringFilter getPreviewTimeZone() {
        return previewTimeZone;
    }

    public void setPreviewTimeZone(StringFilter previewTimeZone) {
        this.previewTimeZone = previewTimeZone;
    }

    public StringFilter getStartTimeZone() {
        return startTimeZone;
    }

    public void setStartTimeZone(StringFilter startTimeZone) {
        this.startTimeZone = startTimeZone;
    }

    public StringFilter getEndTimeZone() {
        return endTimeZone;
    }

    public void setEndTimeZone(StringFilter endTimeZone) {
        this.endTimeZone = endTimeZone;
    }

    public StringFilter getLevelStructure() {
        return levelStructure;
    }

    public void setLevelStructure(StringFilter levelStructure) {
        this.levelStructure = levelStructure;
    }

    public IntegerFilter getProgramLength() {
        return programLength;
    }

    public void setProgramLength(IntegerFilter programLength) {
        this.programLength = programLength;
    }

    public BooleanFilter getIsHPSF() {
        return isHPSF;
    }

    public void setIsHPSF(BooleanFilter isHPSF) {
        this.isHPSF = isHPSF;
    }

    public BooleanFilter getIsHTK() {
        return isHTK;
    }

    public void setIsHTK(BooleanFilter isHTK) {
        this.isHTK = isHTK;
    }

    public BooleanFilter getIsLabcorp() {
        return isLabcorp;
    }

    public void setIsLabcorp(BooleanFilter isLabcorp) {
        this.isLabcorp = isLabcorp;
    }

    public StringFilter getLabcorpAccountNumber() {
        return labcorpAccountNumber;
    }

    public void setLabcorpAccountNumber(StringFilter labcorpAccountNumber) {
        this.labcorpAccountNumber = labcorpAccountNumber;
    }

    public StringFilter getLabcorpFileUrl() {
        return labcorpFileUrl;
    }

    public void setLabcorpFileUrl(StringFilter labcorpFileUrl) {
        this.labcorpFileUrl = labcorpFileUrl;
    }

    public BooleanFilter getApplyRewardAllSubgroup() {
        return applyRewardAllSubgroup;
    }

    public void setApplyRewardAllSubgroup(BooleanFilter applyRewardAllSubgroup) {
        this.applyRewardAllSubgroup = applyRewardAllSubgroup;
    }

    public InstantFilter getBiometricDeadlineDate() {
        return biometricDeadlineDate;
    }

    public void setBiometricDeadlineDate(InstantFilter biometricDeadlineDate) {
        this.biometricDeadlineDate = biometricDeadlineDate;
    }

    public InstantFilter getBiometricLookbackDate() {
        return biometricLookbackDate;
    }

    public void setBiometricLookbackDate(InstantFilter biometricLookbackDate) {
        this.biometricLookbackDate = biometricLookbackDate;
    }

    public StringFilter getLandingBackgroundImageUrl() {
        return landingBackgroundImageUrl;
    }

    public void setLandingBackgroundImageUrl(StringFilter landingBackgroundImageUrl) {
        this.landingBackgroundImageUrl = landingBackgroundImageUrl;
    }

    public BooleanFilter getIsTobaccoSurchargeManagement() {
        return isTobaccoSurchargeManagement;
    }

    public void setIsTobaccoSurchageManagement(BooleanFilter isTobaccoSurchargeManagement) {
        this.isTobaccoSurchargeManagement = isTobaccoSurchargeManagement;
    }

    public InstantFilter getTobaccoAttestationDeadline() {
        return tobaccoAttestationDeadline;
    }

    public void setTobaccoAttestationDeadline(InstantFilter tobaccoAttestationDeadline) {
        this.tobaccoAttestationDeadline = tobaccoAttestationDeadline;
    }

    public InstantFilter getTobaccoProgramDeadline() {
        return tobaccoProgramDeadline;
    }

    public void setTobaccoProgramDeadline(InstantFilter tobaccoProgramDeadline) {
        this.tobaccoProgramDeadline = tobaccoProgramDeadline;
    }

    public LongFilter getProgramCategoryPointId() {
        return programCategoryPointId;
    }

    public void setProgramCategoryPointId(LongFilter programCategoryPointId) {
        this.programCategoryPointId = programCategoryPointId;
    }

    public LongFilter getProgramHealthyRangeId() {
        return programHealthyRangeId;
    }

    public void setProgramHealthyRangeId(LongFilter programHealthyRangeId) {
        this.programHealthyRangeId = programHealthyRangeId;
    }


    public BooleanFilter getIsAwardedForTobaccoAttestation() {
        return isAwardedForTobaccoAttestation;
    }

    public void setIsAwardedForTobaccoAttestation(BooleanFilter isAwardedForTobaccoAttestation) {
        this.isAwardedForTobaccoAttestation = isAwardedForTobaccoAttestation;
    }

    public BooleanFilter getIsAwardedForTobaccoRas() {
        return isAwardedForTobaccoRas;
    }

    public void setIsAwardedForTobaccoRas(BooleanFilter isAwardedForTobaccoRas) {
        this.isAwardedForTobaccoRas = isAwardedForTobaccoRas;
    }

    public BooleanFilter getQaVerify() {
        return qaVerify;
    }

    public void setQaVerify(BooleanFilter qaVerify) {
        this.qaVerify = qaVerify;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramCriteria that = (ProgramCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(lastSent, that.lastSent) &&
            Objects.equals(isRetriggerEmail, that.isRetriggerEmail) &&
            Objects.equals(isEligible, that.isEligible) &&
            Objects.equals(isSentRegistrationEmail, that.isSentRegistrationEmail) &&
            Objects.equals(isRegisteredForPlatform, that.isRegisteredForPlatform) &&
            Objects.equals(isScheduledScreening, that.isScheduledScreening) &&
            Objects.equals(isFunctionally, that.isFunctionally) &&
            Objects.equals(logoUrl, that.logoUrl) &&
            Objects.equals(description, that.description) &&
            Objects.equals(userPoint, that.userPoint) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(status, that.status) &&
            Objects.equals(isUsePoint, that.isUsePoint) &&
            Objects.equals(isScreen, that.isScreen) &&
            Objects.equals(isCoaching, that.isCoaching) &&
            Objects.equals(isWellMatric, that.isWellMatric) &&
            Objects.equals(isHP, that.isHP) &&
            Objects.equals(isUseLevel, that.isUseLevel) &&
            Objects.equals(startDate, that.startDate) &&
            Objects.equals(resetDate, that.resetDate) &&
            Objects.equals(isTemplate, that.isTemplate) &&
            Objects.equals(isPreview, that.isPreview) &&
            Objects.equals(previewDate, that.previewDate) &&
            Objects.equals(previewTimeZone, that.previewTimeZone) &&
            Objects.equals(startTimeZone, that.startTimeZone) &&
            Objects.equals(endTimeZone, that.endTimeZone) &&
            Objects.equals(levelStructure, that.levelStructure) &&
            Objects.equals(programLength, that.programLength) &&
            Objects.equals(isHPSF, that.isHPSF) &&
            Objects.equals(isHTK, that.isHTK) &&
            Objects.equals(isLabcorp, that.isLabcorp) &&
            Objects.equals(labcorpAccountNumber, that.labcorpAccountNumber) &&
            Objects.equals(labcorpFileUrl, that.labcorpFileUrl) &&
            Objects.equals(applyRewardAllSubgroup, that.applyRewardAllSubgroup) &&
            Objects.equals(biometricDeadlineDate, that.biometricDeadlineDate) &&
            Objects.equals(biometricLookbackDate, that.biometricLookbackDate) &&
            Objects.equals(landingBackgroundImageUrl, that.landingBackgroundImageUrl) &&
            Objects.equals(isTobaccoSurchargeManagement, that.isTobaccoSurchargeManagement) &&
            Objects.equals(tobaccoAttestationDeadline, that.tobaccoAttestationDeadline) &&
            Objects.equals(tobaccoProgramDeadline, that.tobaccoProgramDeadline) &&
            Objects.equals(programCategoryPointId, that.programCategoryPointId) &&
            Objects.equals(qaVerify, that.qaVerify) &&
                Objects.equals(economyPoint, that.economyPoint) &&
                Objects.equals(isExtractable, that.isExtractable) &&
            Objects.equals(programHealthyRangeId, that.programHealthyRangeId) &&
                Objects.equals(programType, that.programType) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        lastSent,
        isRetriggerEmail,
        isEligible,
        isSentRegistrationEmail,
        isRegisteredForPlatform,
        isScheduledScreening,
        isFunctionally,
        logoUrl,
        description,
        userPoint,
        lastModifiedDate,
        createdDate,
        lastModifiedBy,
        createdBy,
        status,
        isUsePoint,
        isScreen,
        isCoaching,
        isWellMatric,
        isHP,
        isUseLevel,
        startDate,
        resetDate,
        isTemplate,
        isPreview,
        previewDate,
        previewTimeZone,
        startTimeZone,
        endTimeZone,
        levelStructure,
        programLength,
        isHPSF,
        isHTK,
        isLabcorp,
        labcorpAccountNumber,
        labcorpFileUrl,
        applyRewardAllSubgroup,
        biometricDeadlineDate,
        biometricLookbackDate,
        landingBackgroundImageUrl,
        isTobaccoSurchargeManagement,
        tobaccoAttestationDeadline,
        tobaccoProgramDeadline,
        programCategoryPointId,
        qaVerify,
        economyPoint,
        programHealthyRangeId,
            isExtractable,
            programType
        );
    }

    @Override
    public String toString() {
        return "ProgramCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (lastSent != null ? "lastSent=" + lastSent + ", " : "") +
                (isRetriggerEmail != null ? "isRetriggerEmail=" + isRetriggerEmail + ", " : "") +
                (isEligible != null ? "isEligible=" + isEligible + ", " : "") +
                (isSentRegistrationEmail != null ? "isSentRegistrationEmail=" + isSentRegistrationEmail + ", " : "") +
                (isRegisteredForPlatform != null ? "isRegisteredForPlatform=" + isRegisteredForPlatform + ", " : "") +
                (isScheduledScreening != null ? "isScheduledScreening=" + isScheduledScreening + ", " : "") +
                (isFunctionally != null ? "isFunctionally=" + isFunctionally + ", " : "") +
                (logoUrl != null ? "logoUrl=" + logoUrl + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (userPoint != null ? "userPoint=" + userPoint + ", " : "") +
                (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
                (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (isUsePoint != null ? "isUsePoint=" + isUsePoint + ", " : "") +
                (isScreen != null ? "isScreen=" + isScreen + ", " : "") +
                (isCoaching != null ? "isCoaching=" + isCoaching + ", " : "") +
                (isWellMatric != null ? "isWellMatric=" + isWellMatric + ", " : "") +
                (isHP != null ? "isHP=" + isHP + ", " : "") +
                (isUseLevel != null ? "isUseLevel=" + isUseLevel + ", " : "") +
                (startDate != null ? "startDate=" + startDate + ", " : "") +
                (resetDate != null ? "resetDate=" + resetDate + ", " : "") +
                (isTemplate != null ? "isTemplate=" + isTemplate + ", " : "") +
                (isPreview != null ? "isPreview=" + isPreview + ", " : "") +
                (previewDate != null ? "previewDate=" + previewDate + ", " : "") +
                (previewTimeZone != null ? "previewTimeZone=" + previewTimeZone + ", " : "") +
                (startTimeZone != null ? "startTimeZone=" + startTimeZone + ", " : "") +
                (endTimeZone != null ? "endTimeZone=" + endTimeZone + ", " : "") +
                (levelStructure != null ? "levelStructure=" + levelStructure + ", " : "") +
                (programLength != null ? "programLength=" + programLength + ", " : "") +
                (isHPSF != null ? "isHPSF=" + isHPSF + ", " : "") +
                (isHTK != null ? "isHTK=" + isHTK + ", " : "") +
                (isLabcorp != null ? "isLabcorp=" + isLabcorp + ", " : "") +
                (labcorpAccountNumber != null ? "labcorpAccountNumber=" + labcorpAccountNumber + ", " : "") +
                (labcorpFileUrl != null ? "labcorpFileUrl=" + labcorpFileUrl + ", " : "") +
                (applyRewardAllSubgroup != null ? "applyRewardAllSubgroup=" + applyRewardAllSubgroup + ", " : "") +
                (biometricDeadlineDate != null ? "biometricDeadlineDate=" + biometricDeadlineDate + ", " : "") +
                (biometricLookbackDate != null ? "biometricLookbackDate=" + biometricLookbackDate + ", " : "") +
                (landingBackgroundImageUrl != null ? "landingBackgroundImageUrl=" + landingBackgroundImageUrl + ", " : "") +
                (isTobaccoSurchargeManagement != null ? "isTobaccoSurchageManagement=" + isTobaccoSurchargeManagement + ", " : "") +
                (tobaccoAttestationDeadline != null ? "tobaccoAttestationDeadline=" + tobaccoAttestationDeadline + ", " : "") +
                (tobaccoProgramDeadline != null ? "tobaccoProgramDeadline=" + tobaccoProgramDeadline + ", " : "") +
                (programCategoryPointId != null ? "programCategoryPointId=" + programCategoryPointId + ", " : "") +
                (qaVerify != null ? "qaVerify=" + qaVerify + ", " : "") +
                (programHealthyRangeId != null ? "programHealthyRangeId=" + programHealthyRangeId + ", " : "") +
                (economyPoint != null ? "economyPoint=" + economyPoint + ", " : "") +
                (programType != null ? "programType=" + programType + ", " : "") +
            "}";
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public BigDecimalFilter getEconomyPoint() {
        return economyPoint;
    }

    public void setEconomyPoint(BigDecimalFilter economyPoint) {
        this.economyPoint = economyPoint;
    }

    public BooleanFilter getIsExtractable() {
        return isExtractable;
    }

    public void setIsExtractable(BooleanFilter isExtractable) {
        this.isExtractable = isExtractable;
    }

    public ProgramTypeFilter getProgramType() {
        return programType;
    }

    public void setProgramType(ProgramTypeFilter programType) {
        this.programType = programType;
    }

    public static class ProgramTypeFilter extends Filter<ProgramType> {

        public ProgramTypeFilter() {
        }

        public ProgramTypeFilter(ProgramTypeFilter filter) {
            super(filter);
        }

        @Override
        public ProgramTypeFilter copy() {
            return new ProgramTypeFilter(this);
        }

    }
}
