package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.UserReward} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.UserRewardResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-rewards?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserRewardCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter programLevel;

    private StringFilter status;

    private StringFilter orderId;

    private InstantFilter orderDate;

    private StringFilter giftId;

    private InstantFilter levelCompletedDate;

    private StringFilter participantId;

    private StringFilter email;

    private StringFilter clientId;

    private StringFilter rewardType;

    private StringFilter rewardCode;

    private BigDecimalFilter rewardAmount;

    private StringFilter campaignId;

    private LongFilter programUserId;

    private LongFilter programId;

    private StringFilter eventId;

    public UserRewardCriteria(){
    }

    public UserRewardCriteria(UserRewardCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.programLevel = other.programLevel == null ? null : other.programLevel.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.orderId = other.orderId == null ? null : other.orderId.copy();
        this.orderDate = other.orderDate == null ? null : other.orderDate.copy();
        this.giftId = other.giftId == null ? null : other.giftId.copy();
        this.levelCompletedDate = other.levelCompletedDate == null ? null : other.levelCompletedDate.copy();
        this.participantId = other.participantId == null ? null : other.participantId.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.rewardType = other.rewardType == null ? null : other.rewardType.copy();
        this.rewardCode = other.rewardCode == null ? null : other.rewardCode.copy();
        this.rewardAmount = other.rewardAmount == null ? null : other.rewardAmount.copy();
        this.campaignId = other.campaignId == null ? null : other.campaignId.copy();
        this.programUserId = other.programUserId == null ? null : other.programUserId.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.eventId = other.eventId == null ? null : other.eventId.copy();
    }

    @Override
    public UserRewardCriteria copy() {
        return new UserRewardCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getProgramLevel() {
        return programLevel;
    }

    public void setProgramLevel(IntegerFilter programLevel) {
        this.programLevel = programLevel;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getOrderId() {
        return orderId;
    }

    public void setOrderId(StringFilter orderId) {
        this.orderId = orderId;
    }

    public InstantFilter getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(InstantFilter orderDate) {
        this.orderDate = orderDate;
    }

    public StringFilter getGiftId() {
        return giftId;
    }

    public void setGiftId(StringFilter giftId) {
        this.giftId = giftId;
    }

    public InstantFilter getLevelCompletedDate() {
        return levelCompletedDate;
    }

    public void setLevelCompletedDate(InstantFilter levelCompletedDate) {
        this.levelCompletedDate = levelCompletedDate;
    }

    public StringFilter getParticipantId() {
        return participantId;
    }

    public void setParticipantId(StringFilter participantId) {
        this.participantId = participantId;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getRewardType() {
        return rewardType;
    }

    public void setRewardType(StringFilter rewardType) {
        this.rewardType = rewardType;
    }

    public StringFilter getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(StringFilter rewardCode) {
        this.rewardCode = rewardCode;
    }

    public BigDecimalFilter getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimalFilter rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public StringFilter getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(StringFilter campaignId) {
        this.campaignId = campaignId;
    }

    public LongFilter getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(LongFilter programUserId) {
        this.programUserId = programUserId;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public StringFilter getEventId() {
        return eventId;
    }

    public void setEventId(StringFilter eventId) {
        this.eventId = eventId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserRewardCriteria that = (UserRewardCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(programLevel, that.programLevel) &&
            Objects.equals(status, that.status) &&
            Objects.equals(orderId, that.orderId) &&
            Objects.equals(orderDate, that.orderDate) &&
            Objects.equals(giftId, that.giftId) &&
            Objects.equals(levelCompletedDate, that.levelCompletedDate) &&
            Objects.equals(participantId, that.participantId) &&
            Objects.equals(email, that.email) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(rewardType, that.rewardType) &&
            Objects.equals(rewardCode, that.rewardCode) &&
            Objects.equals(rewardAmount, that.rewardAmount) &&
            Objects.equals(campaignId, that.campaignId) &&
            Objects.equals(programUserId, that.programUserId) &&
            Objects.equals(programId, that.programId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        programLevel,
        status,
        orderId,
        orderDate,
        giftId,
        levelCompletedDate,
        participantId,
        email,
        clientId,
        rewardType,
        rewardCode,
        rewardAmount,
        campaignId,
        programUserId,
        programId, eventId
        );
    }

    @Override
    public String toString() {
        return "UserRewardCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (programLevel != null ? "programLevel=" + programLevel + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (orderId != null ? "orderId=" + orderId + ", " : "") +
                (orderDate != null ? "orderDate=" + orderDate + ", " : "") +
                (giftId != null ? "giftId=" + giftId + ", " : "") +
                (levelCompletedDate != null ? "levelCompletedDate=" + levelCompletedDate + ", " : "") +
                (participantId != null ? "participantId=" + participantId + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (rewardType != null ? "rewardType=" + rewardType + ", " : "") +
                (rewardCode != null ? "rewardCode=" + rewardCode + ", " : "") +
                (rewardAmount != null ? "rewardAmount=" + rewardAmount + ", " : "") +
                (campaignId != null ? "campaignId=" + campaignId + ", " : "") +
                (programUserId != null ? "programUserId=" + programUserId + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (eventId != null ? "wellmetricEventQueueId=" + eventId + ", " : "") +
            "}";
    }

}
