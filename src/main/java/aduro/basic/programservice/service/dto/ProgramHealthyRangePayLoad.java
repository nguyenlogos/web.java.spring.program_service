package aduro.basic.programservice.service.dto;

public class ProgramHealthyRangePayLoad extends ProgramHealthyRangeDTO {

    public ProgramHealthyRangePayLoad(){}
    public ProgramHealthyRangePayLoad(ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO) {
        this.programSubCategoryConfigurationDTO = programSubCategoryConfigurationDTO;
    }

    private ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO;

    public ProgramSubCategoryConfigurationDTO getProgramSubCategoryConfigurationDTO() {
        return programSubCategoryConfigurationDTO;
    }

    public void setProgramSubCategoryConfigurationDTO(ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO) {
        this.programSubCategoryConfigurationDTO = programSubCategoryConfigurationDTO;
    }
}
