package aduro.basic.programservice.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProgramCollectionExtensionDto implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 150)
    private String displayName;

    @Size(max = 255)
    private String longDescription;

    private Instant createdDate;

    private Instant modifiedDate;

    private Long programId;

    private List<ProgramCollectionContentDTO> programCollectionContents;

}
