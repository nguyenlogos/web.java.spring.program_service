package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.LocalDateFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramLevel} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramLevelResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-levels?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramLevelCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter description;

    private IntegerFilter startPoint;

    private IntegerFilter endPoint;

    private IntegerFilter levelOrder;

    private StringFilter iconPath;

    private StringFilter name;

    private LocalDateFilter endDate;

    private LongFilter programId;

    private LongFilter programRequirementItemsId;

    public ProgramLevelCriteria(){
    }

    public ProgramLevelCriteria(ProgramLevelCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.startPoint = other.startPoint == null ? null : other.startPoint.copy();
        this.endPoint = other.endPoint == null ? null : other.endPoint.copy();
        this.levelOrder = other.levelOrder == null ? null : other.levelOrder.copy();
        this.iconPath = other.iconPath == null ? null : other.iconPath.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.endDate = other.endDate == null ? null : other.endDate.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.programRequirementItemsId = other.programRequirementItemsId == null ? null : other.programRequirementItemsId.copy();
    }

    @Override
    public ProgramLevelCriteria copy() {
        return new ProgramLevelCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDescription() {
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public IntegerFilter getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(IntegerFilter startPoint) {
        this.startPoint = startPoint;
    }

    public IntegerFilter getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(IntegerFilter endPoint) {
        this.endPoint = endPoint;
    }

    public IntegerFilter getLevelOrder() {
        return levelOrder;
    }

    public void setLevelOrder(IntegerFilter levelOrder) {
        this.levelOrder = levelOrder;
    }

    public StringFilter getIconPath() {
        return iconPath;
    }

    public void setIconPath(StringFilter iconPath) {
        this.iconPath = iconPath;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public LocalDateFilter getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateFilter endDate) {
        this.endDate = endDate;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public LongFilter getProgramRequirementItemsId() {
        return programRequirementItemsId;
    }

    public void setProgramRequirementItemsId(LongFilter programRequirementItemsId) {
        this.programRequirementItemsId = programRequirementItemsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramLevelCriteria that = (ProgramLevelCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(description, that.description) &&
            Objects.equals(startPoint, that.startPoint) &&
            Objects.equals(endPoint, that.endPoint) &&
            Objects.equals(levelOrder, that.levelOrder) &&
            Objects.equals(iconPath, that.iconPath) &&
            Objects.equals(name, that.name) &&
            Objects.equals(endDate, that.endDate) &&
            Objects.equals(programId, that.programId) &&
             Objects.equals(programId, that.programId) &&
            Objects.equals(programRequirementItemsId, that.programRequirementItemsId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        description,
        startPoint,
        endPoint,
        levelOrder,
        iconPath,
        name,
        endDate,
        programId
        );
    }

    @Override
    public String toString() {
        return "ProgramLevelCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (description != null ? "description=" + description + ", " : "") +
                (startPoint != null ? "startPoint=" + startPoint + ", " : "") +
                (endPoint != null ? "endPoint=" + endPoint + ", " : "") +
                (levelOrder != null ? "levelOrder=" + levelOrder + ", " : "") +
                (iconPath != null ? "iconPath=" + iconPath + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (endDate != null ? "endDate=" + endDate + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (programRequirementItemsId != null ? "programRequirementItemsId=" + programRequirementItemsId + ", " : "") +
            "}";
    }

}
