package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.EConsent} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.EConsentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /e-consents?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class EConsentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter participantId;

    private LongFilter programId;

    private BooleanFilter hasConfirmed;

    private InstantFilter createdAt;

    private LongFilter termAndConditionId;

    public EConsentCriteria(){
    }

    public EConsentCriteria(EConsentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.participantId = other.participantId == null ? null : other.participantId.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.hasConfirmed = other.hasConfirmed == null ? null : other.hasConfirmed.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.termAndConditionId = other.termAndConditionId == null ? null : other.termAndConditionId.copy();
    }

    @Override
    public EConsentCriteria copy() {
        return new EConsentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getParticipantId() {
        return participantId;
    }

    public void setParticipantId(StringFilter participantId) {
        this.participantId = participantId;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public BooleanFilter getHasConfirmed() {
        return hasConfirmed;
    }

    public void setHasConfirmed(BooleanFilter hasConfirmed) {
        this.hasConfirmed = hasConfirmed;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public LongFilter getTermAndConditionId() {
        return termAndConditionId;
    }

    public void setTermAndConditionId(LongFilter termAndConditionId) {
        this.termAndConditionId = termAndConditionId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EConsentCriteria that = (EConsentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(participantId, that.participantId) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(hasConfirmed, that.hasConfirmed) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(termAndConditionId, that.termAndConditionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        participantId,
        programId,
        hasConfirmed,
        createdAt,
        termAndConditionId
        );
    }

    @Override
    public String toString() {
        return "EConsentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (participantId != null ? "participantId=" + participantId + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (hasConfirmed != null ? "hasConfirmed=" + hasConfirmed + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (termAndConditionId != null ? "termAndConditionId=" + termAndConditionId + ", " : "") +
            "}";
    }

}
