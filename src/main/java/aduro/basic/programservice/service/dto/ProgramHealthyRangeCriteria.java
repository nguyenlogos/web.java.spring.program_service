package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramHealthyRange} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramHealthyRangeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-healthy-ranges?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramHealthyRangeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private FloatFilter maleMin;

    private FloatFilter maleMax;

    private FloatFilter femaleMin;

    private FloatFilter femaleMax;

    private FloatFilter unidentifiedMin;

    private FloatFilter unidentifiedMax;

    private BooleanFilter isApplyPoint;

    private StringFilter biometricCode;

    private StringFilter subCategoryCode;

    private LongFilter subCategoryId;

    private LongFilter programId;

    public ProgramHealthyRangeCriteria(){
    }

    public ProgramHealthyRangeCriteria(ProgramHealthyRangeCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.maleMin = other.maleMin == null ? null : other.maleMin.copy();
        this.maleMax = other.maleMax == null ? null : other.maleMax.copy();
        this.femaleMin = other.femaleMin == null ? null : other.femaleMin.copy();
        this.femaleMax = other.femaleMax == null ? null : other.femaleMax.copy();
        this.unidentifiedMin = other.unidentifiedMin == null ? null : other.unidentifiedMin.copy();
        this.unidentifiedMax = other.unidentifiedMax == null ? null : other.unidentifiedMax.copy();
        this.isApplyPoint = other.isApplyPoint == null ? null : other.isApplyPoint.copy();
        this.biometricCode = other.biometricCode == null ? null : other.biometricCode.copy();
        this.subCategoryCode = other.subCategoryCode == null ? null : other.subCategoryCode.copy();
        this.subCategoryId = other.subCategoryId == null ? null : other.subCategoryId.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
    }

    @Override
    public ProgramHealthyRangeCriteria copy() {
        return new ProgramHealthyRangeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public FloatFilter getMaleMin() {
        return maleMin;
    }

    public void setMaleMin(FloatFilter maleMin) {
        this.maleMin = maleMin;
    }

    public FloatFilter getMaleMax() {
        return maleMax;
    }

    public void setMaleMax(FloatFilter maleMax) {
        this.maleMax = maleMax;
    }

    public FloatFilter getFemaleMin() {
        return femaleMin;
    }

    public void setFemaleMin(FloatFilter femaleMin) {
        this.femaleMin = femaleMin;
    }

    public FloatFilter getFemaleMax() {
        return femaleMax;
    }

    public void setFemaleMax(FloatFilter femaleMax) {
        this.femaleMax = femaleMax;
    }

    public FloatFilter getUnidentifiedMin() {
        return unidentifiedMin;
    }

    public void setUnidentifiedMin(FloatFilter unidentifiedMin) {
        this.unidentifiedMin = unidentifiedMin;
    }

    public FloatFilter getUnidentifiedMax() {
        return unidentifiedMax;
    }

    public void setUnidentifiedMax(FloatFilter unidentifiedMax) {
        this.unidentifiedMax = unidentifiedMax;
    }

    public BooleanFilter getIsApplyPoint() {
        return isApplyPoint;
    }

    public void setIsApplyPoint(BooleanFilter isApplyPoint) {
        this.isApplyPoint = isApplyPoint;
    }

    public StringFilter getBiometricCode() {
        return biometricCode;
    }

    public void setBiometricCode(StringFilter biometricCode) {
        this.biometricCode = biometricCode;
    }

    public StringFilter getSubCategoryCode() {
        return subCategoryCode;
    }

    public void setSubCategoryCode(StringFilter subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public LongFilter getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(LongFilter subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramHealthyRangeCriteria that = (ProgramHealthyRangeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(maleMin, that.maleMin) &&
            Objects.equals(maleMax, that.maleMax) &&
            Objects.equals(femaleMin, that.femaleMin) &&
            Objects.equals(femaleMax, that.femaleMax) &&
            Objects.equals(unidentifiedMin, that.unidentifiedMin) &&
            Objects.equals(unidentifiedMax, that.unidentifiedMax) &&
            Objects.equals(isApplyPoint, that.isApplyPoint) &&
            Objects.equals(biometricCode, that.biometricCode) &&
            Objects.equals(subCategoryCode, that.subCategoryCode) &&
            Objects.equals(subCategoryId, that.subCategoryId) &&
            Objects.equals(programId, that.programId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        maleMin,
        maleMax,
        femaleMin,
        femaleMax,
        unidentifiedMin,
        unidentifiedMax,
        isApplyPoint,
        biometricCode,
        subCategoryCode,
        subCategoryId,
        programId
        );
    }

    @Override
    public String toString() {
        return "ProgramHealthyRangeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (maleMin != null ? "maleMin=" + maleMin + ", " : "") +
                (maleMax != null ? "maleMax=" + maleMax + ", " : "") +
                (femaleMin != null ? "femaleMin=" + femaleMin + ", " : "") +
                (femaleMax != null ? "femaleMax=" + femaleMax + ", " : "") +
                (unidentifiedMin != null ? "unidentifiedMin=" + unidentifiedMin + ", " : "") +
                (unidentifiedMax != null ? "unidentifiedMax=" + unidentifiedMax + ", " : "") +
                (isApplyPoint != null ? "isApplyPoint=" + isApplyPoint + ", " : "") +
                (biometricCode != null ? "biometricCode=" + biometricCode + ", " : "") +
                (subCategoryCode != null ? "subCategoryCode=" + subCategoryCode + ", " : "") +
                (subCategoryId != null ? "subCategoryId=" + subCategoryId + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
            "}";
    }

}
