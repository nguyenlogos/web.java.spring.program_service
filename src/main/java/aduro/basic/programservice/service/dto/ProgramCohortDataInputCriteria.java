package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramCohortDataInput} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramCohortDataInputResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-cohort-data-inputs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramCohortDataInputCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter nameDataInput;

    private StringFilter inputType;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    public ProgramCohortDataInputCriteria(){
    }

    public ProgramCohortDataInputCriteria(ProgramCohortDataInputCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.nameDataInput = other.nameDataInput == null ? null : other.nameDataInput.copy();
        this.inputType = other.inputType == null ? null : other.inputType.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
    }

    @Override
    public ProgramCohortDataInputCriteria copy() {
        return new ProgramCohortDataInputCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getNameDataInput() {
        return nameDataInput;
    }

    public void setNameDataInput(StringFilter nameDataInput) {
        this.nameDataInput = nameDataInput;
    }

    public StringFilter getInputType() {
        return inputType;
    }

    public void setInputType(StringFilter inputType) {
        this.inputType = inputType;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramCohortDataInputCriteria that = (ProgramCohortDataInputCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(nameDataInput, that.nameDataInput) &&
            Objects.equals(inputType, that.inputType) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        nameDataInput,
        inputType,
        createdDate,
        modifiedDate
        );
    }

    @Override
    public String toString() {
        return "ProgramCohortDataInputCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (nameDataInput != null ? "nameDataInput=" + nameDataInput + ", " : "") +
                (inputType != null ? "inputType=" + inputType + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
            "}";
    }

}
