package aduro.basic.programservice.service.dto;

import lombok.*;

import java.math.BigDecimal;

@Setter
@Getter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class JoinRequestResponseDto {
    private String clientId;
    private String participantId;
    private Long programId;
    private Integer currentLevel;
    private BigDecimal totalPoint;
}
