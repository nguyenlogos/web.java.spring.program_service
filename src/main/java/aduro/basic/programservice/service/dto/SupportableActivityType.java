package aduro.basic.programservice.service.dto;

public enum SupportableActivityType {
    INFORMATION("INFORMATIONAL"),
    SELF_TRACKING("SELF_TRACKING"),
    PASSIVE_TRACKING("PASSIVE_TRACKING"),
    EMPLOYER_VERIFIED("EMPLOYER_VERIFIED"),
    OTHER_ASSESSMENT("OTHER_ASSESSMENT");

    private final String value;

    SupportableActivityType(final String text) {
        this.value = text;
    }

    @Override
    public String toString() {
        return value;
    }

    public static SupportableActivityType fromString(String text) {
        for (SupportableActivityType b : SupportableActivityType.values()) {
            if (b.toString().equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
