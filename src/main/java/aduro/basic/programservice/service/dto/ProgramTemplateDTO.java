package aduro.basic.programservice.service.dto;

import lombok.*;

import java.time.Instant;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProgramTemplateDTO extends  ProgramDTO{


    private String clientId;

    private String clientName;

    private List<ProgramSubgroupDTO> programSubgroups;


}
