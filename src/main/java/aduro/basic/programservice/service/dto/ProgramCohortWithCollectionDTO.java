package aduro.basic.programservice.service.dto;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class ProgramCohortWithCollectionDTO extends ProgramCohortDTO {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortWithCollectionDTO.class);

    private List<ProgramCohortCollectionDTO> cohortCollections;

    public List<ProgramCohortCollectionDTO> getCohortCollections() {
        return cohortCollections;
    }

    public void setCohortCollections(List<ProgramCohortCollectionDTO> cohortCollections) {
        this.cohortCollections = cohortCollections;
    }

    public ProgramCohortWithCollectionDTO(ProgramCohortDTO programCohortDTO) {
        try {
            BeanUtils.copyProperties(this, programCohortDTO);
        } catch (IllegalAccessException e) {
            log.error("Error Copy properties ProgramCohortWithCollectionDTO", e);
        } catch (InvocationTargetException e) {
            log.error("Error Copy properties ProgramCohortWithCollectionDTO", e);
        }
    }
}
