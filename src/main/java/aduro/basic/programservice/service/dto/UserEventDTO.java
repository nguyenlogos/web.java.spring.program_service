package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class UserEventDTO implements Serializable {

    private Long id;

    @NotNull
    private String eventCode;

    @NotNull
    private String eventId;

    @NotNull
    private Instant eventDate;

    private BigDecimal eventPoint;

    private Integer programLevel;

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    private String eventCategory;

    private Long programId;

    private Long programUserId;

    private String programUserParticipantId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Instant getEventDate() {
        return eventDate;
    }

    public void setEventDate(Instant eventDate) {
        this.eventDate = eventDate;
    }

    public BigDecimal getEventPoint() {
        return eventPoint;
    }

    public void setEventPoint(BigDecimal eventPoint) {
        this.eventPoint = eventPoint;
    }



    public Long getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(Long programUserId) {
        this.programUserId = programUserId;
    }

    public String getProgramUserParticipantId() {
        return programUserParticipantId;
    }

    public void setProgramUserParticipantId(String programUserParticipantId) {
        this.programUserParticipantId = programUserParticipantId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserEventDTO userEventDTO = (UserEventDTO) o;
        if (userEventDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userEventDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserEventDTO{" +
            "id=" + getId() +
            ", eventCode='" + getEventCode() + "'" +
            ", eventId='" + getEventId() + "'" +
            ", eventDate='" + getEventDate() + "'" +
            ", eventPoint=" + getEventPoint() +
            ", programUser=" + getProgramUserId() +
            ", programUser='" + getProgramUserParticipantId() + "'" +
            "}";
    }
}
