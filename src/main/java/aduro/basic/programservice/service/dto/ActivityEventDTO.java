package aduro.basic.programservice.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityEventDTO implements Serializable {

    @NotNull
    private String eventCode;

    @NotNull
    private String eventId;

    @NotNull
    private Instant eventDate;

    private String eventCategory;

    private String clientId;

    private Boolean isHpContent;

    private String clientName;

    private Long programId;

    private String createdByAdmin;

    private List<ParticipantDTO> participants = new ArrayList<>();

    public ActivityEventDTO(@NotNull String eventCode,
                            @NotNull String eventId,
                            @NotNull Instant eventDate,
                            String clientId, String clientName, Long programId, List<ParticipantDTO> participants) {
        this.eventCode = eventCode;
        this.eventId = eventId;
        this.eventDate = eventDate;
        this.clientId = clientId;
        this.clientName = clientName;
        this.programId = programId;
        this.participants = participants;
    }

    public ActivityEventDTO(@NotNull String eventCode,
                            @NotNull String eventId,
                            String clientId,
                            @NotNull Instant eventDate,
                            List<ParticipantDTO> participants
                            ) {
        this.eventCode = eventCode;
        this.eventId = eventId;
        this.eventDate = eventDate;
        this.clientId = clientId;
        this.participants = participants;
    }
}
