package aduro.basic.programservice.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.TermAndCondition} entity.
 */
public class TermAndConditionRequest implements Serializable {

    private Long id;

    @NotNull
    private String clientId;

    private int[] subgroupId;

    private String title;

    private String content;

    private Boolean isTargetSubgroup;

    private String subgroupName;

    private Boolean isRequiredAnnually;

    private Integer consentOrder;

    private Boolean isTerminated;

    public Boolean getIsRequiredAnnually() { return isRequiredAnnually; }

    public void setIsRequiredAnnually(Boolean requiredAnnually) { this.isRequiredAnnually = requiredAnnually; }

    public Integer getConsentOrder() { return consentOrder; }

    public void setConsentOrder(Integer consentOrder) { this.consentOrder = consentOrder; }

    public Boolean getIsTerminated() { return isTerminated; }

    public void setIsTerminated(Boolean isTerminated) { this.isTerminated = isTerminated; }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public int[] getSubgroupId() { return subgroupId; }

    public void setSubgroupId(int[] subgroupId) { this.subgroupId = subgroupId; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getIsTargetSubgroup() {
        return isTargetSubgroup;
    }

    public void setIsTargetSubgroup(Boolean isTargetSubgroup) {
        this.isTargetSubgroup = isTargetSubgroup;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TermAndConditionRequest termAndConditionDTO = (TermAndConditionRequest) o;
        if (termAndConditionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), termAndConditionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TermAndConditionDTO{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", title='" + getTitle() + "'" +
            ", content='" + getContent() + "'" +
            ", isTargetSubgroup='" + getIsTargetSubgroup() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", isRequiredAnnually='" + getIsRequiredAnnually() + "'" +
            ", consentOrder='" + getConsentOrder() + "'" +
            ", isTerminated='" + getIsTerminated() + "'" +
            "}";
    }
}
