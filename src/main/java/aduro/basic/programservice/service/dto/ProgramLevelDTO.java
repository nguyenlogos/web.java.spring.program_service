package aduro.basic.programservice.service.dto;
import aduro.basic.programservice.domain.ProgramLevelActivity;
import aduro.basic.programservice.domain.ProgramLevelReward;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramLevel} entity.
 */
public class ProgramLevelDTO implements Serializable {

    private Long id;

    private String description;

    private Integer startPoint;

    private Integer endPoint;

    private Integer levelOrder;

    private String iconPath;

    private String name;

    private LocalDate endDate;


    private Long programId;

    private List<ProgramRequirementItemsDTO> programRequirementItems= new ArrayList<>();
    private List<ProgramLevelRewardDTO> programLevelTobaccoRewards= new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Integer startPoint) {
        this.startPoint = startPoint;
    }

    public Integer getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Integer endPoint) {
        this.endPoint = endPoint;
    }

    public Integer getLevelOrder() {
        return levelOrder;
    }

    public void setLevelOrder(Integer levelOrder) {
        this.levelOrder = levelOrder;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    private List<ProgramLevelRewardDTO> programLevelRewards = new ArrayList<>();
    private List<ProgramLevelActivityDTO> programLevelActivities = new ArrayList<>();

    public List<ProgramLevelPathDTO> getProgramLevelPaths() {
        return programLevelPaths;
    }

    public void setProgramLevelPaths(List<ProgramLevelPathDTO> programLevelPaths) {
        this.programLevelPaths = programLevelPaths;
    }

    private List<ProgramLevelPathDTO> programLevelPaths = new ArrayList<>();

    public List<ProgramLevelRewardDTO> getProgramLevelRewards() {
        return programLevelRewards;
    }

    public void setProgramLevelRewards(List<ProgramLevelRewardDTO> programLevelRewards) {
        this.programLevelRewards = programLevelRewards;
    }

    public List<ProgramLevelActivityDTO> getProgramLevelActivities() {
        return programLevelActivities;
    }

    public void setProgramLevelActivities(List<ProgramLevelActivityDTO> programLevelActivities) {
        this.programLevelActivities = programLevelActivities;
    }

    public List<ProgramRequirementItemsDTO> getProgramRequirementItems() {
        return programRequirementItems;
    }

    public void setProgramRequirementItems(List<ProgramRequirementItemsDTO> programRequirementItems) {
        this.programRequirementItems = programRequirementItems;
    }

    public List<ProgramLevelRewardDTO> getProgramLevelTobaccoRewards() {
        return programLevelTobaccoRewards;
    }

    public void setProgramLevelTobaccoRewards(List<ProgramLevelRewardDTO> programLevelTobaccoRewards) {
        this.programLevelTobaccoRewards = programLevelTobaccoRewards;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramLevelDTO programLevelDTO = (ProgramLevelDTO) o;
        if (programLevelDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programLevelDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramLevelDTO{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", startPoint=" + getStartPoint() +
            ", endPoint=" + getEndPoint() +
            ", levelOrder=" + getLevelOrder() +
            ", iconPath='" + getIconPath() + "'" +
            ", name='" + getName() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", program=" + getProgramId() +
            "}";
    }
}
