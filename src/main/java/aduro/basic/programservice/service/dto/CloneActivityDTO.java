package aduro.basic.programservice.service.dto;

public class CloneActivityDTO {

    public String getOldCustomActivityId() {
        return oldCustomActivityId;
    }

    public void setOldCustomActivityId(String oldCustomActivityId) {
        this.oldCustomActivityId = oldCustomActivityId;
    }

    public String getNewCustomActivityId() {
        return newCustomActivityId;
    }

    public void setNewCustomActivityId(String newCustomActivityId) {
        this.newCustomActivityId = newCustomActivityId;
    }

    private String oldCustomActivityId;
    private String newCustomActivityId;
}
