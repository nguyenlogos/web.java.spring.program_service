package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.enumeration.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PreventiveActivityDto implements Serializable {


    @JsonProperty("id")
    private int id;

    @JsonProperty("screening_type")
    private String screeningType;

    @JsonProperty("type")
    private String activityType;

    @JsonFormat(pattern = Constants.DATE_MONARCH_RECEIVE_FORMAT)
    @JsonProperty("start_date")
    private Date startDate;

    @JsonFormat(pattern = Constants.DATE_MONARCH_RECEIVE_FORMAT)
    @JsonProperty("end_date")
    private Date endDate;

    @JsonProperty("activity_id")
    private int activityId;

    @JsonProperty("is_required")
    private Boolean isRequired;
}


