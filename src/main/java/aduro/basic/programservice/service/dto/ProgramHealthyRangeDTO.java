package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramHealthyRange} entity.
 */
public class ProgramHealthyRangeDTO implements Serializable {

    private Long id;

    @NotNull
    private Float maleMin;

    @NotNull
    private Float maleMax;

    @NotNull
    private Float femaleMin;

    @NotNull
    private Float femaleMax;

    @NotNull
    private Float unidentifiedMin;

    @NotNull
    private Float unidentifiedMax;

    private Boolean isApplyPoint;

    @NotNull
    private String biometricCode;

    private String subCategoryCode;

    @NotNull
    private Long subCategoryId;


    private Long programId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getMaleMin() {
        return maleMin;
    }

    public void setMaleMin(Float maleMin) {
        this.maleMin = maleMin;
    }

    public Float getMaleMax() {
        return maleMax;
    }

    public void setMaleMax(Float maleMax) {
        this.maleMax = maleMax;
    }

    public Float getFemaleMin() {
        return femaleMin;
    }

    public void setFemaleMin(Float femaleMin) {
        this.femaleMin = femaleMin;
    }

    public Float getFemaleMax() {
        return femaleMax;
    }

    public void setFemaleMax(Float femaleMax) {
        this.femaleMax = femaleMax;
    }

    public Float getUnidentifiedMin() {
        return unidentifiedMin;
    }

    public void setUnidentifiedMin(Float unidentifiedMin) {
        this.unidentifiedMin = unidentifiedMin;
    }

    public Float getUnidentifiedMax() {
        return unidentifiedMax;
    }

    public void setUnidentifiedMax(Float unidentifiedMax) {
        this.unidentifiedMax = unidentifiedMax;
    }

    public Boolean isIsApplyPoint() {
        return isApplyPoint;
    }

    public void setIsApplyPoint(Boolean isApplyPoint) {
        this.isApplyPoint = isApplyPoint;
    }

    public String getBiometricCode() {
        return biometricCode;
    }

    public void setBiometricCode(String biometricCode) {
        this.biometricCode = biometricCode;
    }

    public String getSubCategoryCode() {
        return subCategoryCode;
    }

    public void setSubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramHealthyRangeDTO programHealthyRangeDTO = (ProgramHealthyRangeDTO) o;
        if (programHealthyRangeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programHealthyRangeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramHealthyRangeDTO{" +
            "id=" + getId() +
            ", maleMin=" + getMaleMin() +
            ", maleMax=" + getMaleMax() +
            ", femaleMin=" + getFemaleMin() +
            ", femaleMax=" + getFemaleMax() +
            ", unidentifiedMin=" + getUnidentifiedMin() +
            ", unidentifiedMax=" + getUnidentifiedMax() +
            ", isApplyPoint='" + isIsApplyPoint() + "'" +
            ", biometricCode='" + getBiometricCode() + "'" +
            ", subCategoryCode='" + getSubCategoryCode() + "'" +
            ", subCategoryId=" + getSubCategoryId() +
            ", program=" + getProgramId() +
            "}";
    }
}
