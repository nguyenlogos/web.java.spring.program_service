package aduro.basic.programservice.service.dto;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.UserReward} entity.
 */
public class UserRewardDTO implements Serializable {

    private Long id;

    private Integer programLevel;

    private String status;

    private String orderId;

    private Instant orderDate;

    private String giftId;

    private Instant levelCompletedDate;

    @NotNull
    private String participantId;

    @NotNull
    private String email;

    @NotNull
    private String clientId;

    private String rewardType;

    private String rewardCode;

    private BigDecimal rewardAmount;

    private String campaignId;


    private Long programUserId;

    private String programUserParticipantId;

    private Long programId;

    private String programName;

    private String rewardMessage;

    private String eventId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getProgramLevel() {
        return programLevel;
    }

    public void setProgramLevel(Integer programLevel) {
        this.programLevel = programLevel;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public Instant getLevelCompletedDate() {
        return levelCompletedDate;
    }

    public void setLevelCompletedDate(Instant levelCompletedDate) {
        this.levelCompletedDate = levelCompletedDate;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public Long getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(Long programUserId) {
        this.programUserId = programUserId;
    }

    public String getProgramUserParticipantId() {
        return programUserParticipantId;
    }

    public void setProgramUserParticipantId(String programUserParticipantId) {
        this.programUserParticipantId = programUserParticipantId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getRewardMessage() {
        return rewardMessage;
    }

    public void setRewardMessage(String rewardMessage) {
        this.rewardMessage = rewardMessage;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserRewardDTO userRewardDTO = (UserRewardDTO) o;
        if (userRewardDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userRewardDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserRewardDTO{" +
            "id=" + getId() +
            ", programLevel=" + getProgramLevel() +
            ", status='" + getStatus() + "'" +
            ", orderId='" + getOrderId() + "'" +
            ", orderDate='" + getOrderDate() + "'" +
            ", giftId='" + getGiftId() + "'" +
            ", levelCompletedDate='" + getLevelCompletedDate() + "'" +
            ", participantId='" + getParticipantId() + "'" +
            ", email='" + getEmail() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", rewardType='" + getRewardType() + "'" +
            ", rewardCode='" + getRewardCode() + "'" +
            ", rewardAmount=" + getRewardAmount() +
            ", campaignId='" + getCampaignId() + "'" +
            ", rewardMessage='" + getRewardMessage() + "'" +
            ", programUser=" + getProgramUserId() +
            ", programUser='" + getProgramUserParticipantId() + "'" +
            ", program=" + getProgramId() +
            ", program='" + getProgramName() + "'" +
            "}";
    }
}
