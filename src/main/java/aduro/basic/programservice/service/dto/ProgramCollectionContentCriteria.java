package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramCollectionContent} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramCollectionContentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-collection-contents?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramCollectionContentCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter itemName;

    private StringFilter itemId;

    private StringFilter itemType;

    private StringFilter contentType;

    private StringFilter itemIcon;

    private BooleanFilter hasRequired;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    private LongFilter programCollectionId;

    public ProgramCollectionContentCriteria(){
    }

    public ProgramCollectionContentCriteria(ProgramCollectionContentCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.itemName = other.itemName == null ? null : other.itemName.copy();
        this.itemId = other.itemId == null ? null : other.itemId.copy();
        this.itemType = other.itemType == null ? null : other.itemType.copy();
        this.contentType = other.contentType == null ? null : other.contentType.copy();
        this.itemIcon = other.itemIcon == null ? null : other.itemIcon.copy();
        this.hasRequired = other.hasRequired == null ? null : other.hasRequired.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
        this.programCollectionId = other.programCollectionId == null ? null : other.programCollectionId.copy();
    }

    @Override
    public ProgramCollectionContentCriteria copy() {
        return new ProgramCollectionContentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getItemName() {
        return itemName;
    }

    public void setItemName(StringFilter itemName) {
        this.itemName = itemName;
    }

    public StringFilter getItemId() {
        return itemId;
    }

    public void setItemId(StringFilter itemId) {
        this.itemId = itemId;
    }

    public StringFilter getItemType() {
        return itemType;
    }

    public void setItemType(StringFilter itemType) {
        this.itemType = itemType;
    }

    public StringFilter getContentType() {
        return contentType;
    }

    public void setContentType(StringFilter contentType) {
        this.contentType = contentType;
    }

    public StringFilter getItemIcon() {
        return itemIcon;
    }

    public void setItemIcon(StringFilter itemIcon) {
        this.itemIcon = itemIcon;
    }

    public BooleanFilter getHasRequired() {
        return hasRequired;
    }

    public void setHasRequired(BooleanFilter hasRequired) {
        this.hasRequired = hasRequired;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public LongFilter getProgramCollectionId() {
        return programCollectionId;
    }

    public void setProgramCollectionId(LongFilter programCollectionId) {
        this.programCollectionId = programCollectionId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramCollectionContentCriteria that = (ProgramCollectionContentCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(itemName, that.itemName) &&
            Objects.equals(itemId, that.itemId) &&
            Objects.equals(itemType, that.itemType) &&
            Objects.equals(contentType, that.contentType) &&
            Objects.equals(itemIcon, that.itemIcon) &&
            Objects.equals(hasRequired, that.hasRequired) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate) &&
            Objects.equals(programCollectionId, that.programCollectionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        itemName,
        itemId,
        itemType,
        contentType,
        itemIcon,
        hasRequired,
        createdDate,
        modifiedDate,
        programCollectionId
        );
    }

    @Override
    public String toString() {
        return "ProgramCollectionContentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (itemName != null ? "itemName=" + itemName + ", " : "") +
                (itemId != null ? "itemId=" + itemId + ", " : "") +
                (itemType != null ? "itemType=" + itemType + ", " : "") +
                (contentType != null ? "contentType=" + contentType + ", " : "") +
                (itemIcon != null ? "itemIcon=" + itemIcon + ", " : "") +
                (hasRequired != null ? "hasRequired=" + hasRequired + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
                (programCollectionId != null ? "programCollectionId=" + programCollectionId + ", " : "") +
            "}";
    }

}
