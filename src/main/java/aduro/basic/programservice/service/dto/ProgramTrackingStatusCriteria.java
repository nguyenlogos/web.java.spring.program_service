package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramTrackingStatus} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramTrackingStatusResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-tracking-statuses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramTrackingStatusCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter programId;

    private StringFilter programStatus;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    public ProgramTrackingStatusCriteria(){
    }

    public ProgramTrackingStatusCriteria(ProgramTrackingStatusCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.programStatus = other.programStatus == null ? null : other.programStatus.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
    }

    @Override
    public ProgramTrackingStatusCriteria copy() {
        return new ProgramTrackingStatusCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public StringFilter getProgramStatus() {
        return programStatus;
    }

    public void setProgramStatus(StringFilter programStatus) {
        this.programStatus = programStatus;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramTrackingStatusCriteria that = (ProgramTrackingStatusCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(programStatus, that.programStatus) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        programId,
        programStatus,
        createdDate,
        modifiedDate
        );
    }

    @Override
    public String toString() {
        return "ProgramTrackingStatusCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (programStatus != null ? "programStatus=" + programStatus + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
            "}";
    }

}
