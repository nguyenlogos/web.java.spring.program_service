package aduro.basic.programservice.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProgramStatusChangeMessage implements Serializable {

    @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("program_id")
    private Long programId;

    @JsonProperty("timestamp")
    private Instant timeStamp;
}
