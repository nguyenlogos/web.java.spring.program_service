package aduro.basic.programservice.service.dto;

import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CloneCustomActivityRequest {

    private Long fromProgramId;

    private Long newProgramId;

    private String message;

}
