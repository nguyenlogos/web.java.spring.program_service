package aduro.basic.programservice.service.dto;
import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.OrderTransaction} entity.
 */
public class OrderTransactionDTO implements Serializable {

    private Long id;

    private String programName;

    private String clientName;

    private String participantName;

    private Instant createdDate;

    private String message;

    private String jsonContent;

    private String email;

    private String externalOrderId;

    private Long programId;

    private Long userRewardId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getJsonContent() {
        return jsonContent;
    }

    public void setJsonContent(String jsonContent) {
        this.jsonContent = jsonContent;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExternalOrderId() {
        return externalOrderId;
    }

    public void setExternalOrderId(String externalOrderId) {
        this.externalOrderId = externalOrderId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Long getUserRewardId() {
        return userRewardId;
    }

    public void setUserRewardId(Long userRewardId) {
        this.userRewardId = userRewardId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderTransactionDTO orderTransactionDTO = (OrderTransactionDTO) o;
        if (orderTransactionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), orderTransactionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OrderTransactionDTO{" +
            "id=" + getId() +
            ", programName='" + getProgramName() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", participantName='" + getParticipantName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", message='" + getMessage() + "'" +
            ", jsonContent='" + getJsonContent() + "'" +
            ", email='" + getEmail() + "'" +
            ", externalOrderId='" + getExternalOrderId() + "'" +
            ", programId=" + getProgramId() +
            ", userRewardId=" + getUserRewardId() +
            "}";
    }
}
