package aduro.basic.programservice.service.dto;

public class DemoGraphicValueDto {
    private String columnValue;

    public String getColumnValue() {
        return columnValue;
    }

    public void setColumnValue(String columnValue) {
        this.columnValue = columnValue;
    }
}
