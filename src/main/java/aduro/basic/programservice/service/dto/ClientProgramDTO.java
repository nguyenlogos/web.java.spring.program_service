package aduro.basic.programservice.service.dto;
import aduro.basic.programservice.domain.ProgramSubgroup;

import java.time.Instant;
import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ClientProgram} entity.
 */
public class ClientProgramDTO implements Serializable {

    private Long id;

    @NotNull
    private String clientId;

    private Long programId;

    private String programName;

    private String logoUrl;

    private Instant startDate;

    private String startTimeZone;

    private Instant endDate;

    private String endTimeZone;

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    private String clientName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public List<ProgramSubgroupDTO> getProgramSubgroups() {
        return programSubgroups;
    }

    public void setProgramSubgroups(List<ProgramSubgroupDTO> programSubgroups) {
        this.programSubgroups = programSubgroups;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public String getStartTimeZone() {
        return startTimeZone;
    }

    public void setStartTimeZone(String startTimeZone) {
        this.startTimeZone = startTimeZone;
    }

    public String getEndTimeZone() {
        return endTimeZone;
    }

    public void setEndTimeZone(String endTimeZone) {
        this.endTimeZone = endTimeZone;
    }

    private List<ProgramSubgroupDTO> programSubgroups = new ArrayList<>();



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientProgramDTO clientProgramDTO = (ClientProgramDTO) o;
        if (clientProgramDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientProgramDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientProgramDTO{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", program=" + getProgramId() +
            ", program='" + getProgramName() + "'" +
            "}";
    }
}
