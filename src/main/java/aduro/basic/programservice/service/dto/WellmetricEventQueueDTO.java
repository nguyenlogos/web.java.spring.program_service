package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import aduro.basic.programservice.domain.enumeration.ResultStatus;
import aduro.basic.programservice.domain.enumeration.Gender;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.WellmetricEventQueue} entity.
 */
public class WellmetricEventQueueDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 36)
    private String participantId;

    @NotNull
    @Size(max = 36)
    private String clientId;

    @NotNull
    private String email;

    private String errorMessage;

    @NotNull
    private ResultStatus result;

    @NotNull
    private String eventCode;

    @NotNull
    private Gender gender;

    private Integer attemptCount;

    @NotNull
    private Instant lastAttemptTime;

    @NotNull
    private Instant createdAt;

    private String subgroupId;

    @NotNull
    private Float healthyValue;

    @NotNull
    private String executeStatus;

    @NotNull
    private String eventId;

    private Boolean hasIncentive;

    private Boolean isWaitingPush;

    @NotNull
    private Instant attemptedAt;

    @NotNull
    private Long programId;

    @NotNull
    private String eventDate;

    private String subCategoryCode;

    private boolean appealApplied;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ResultStatus getResult() {
        return result;
    }

    public void setResult(ResultStatus result) {
        this.result = result;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Integer getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
    }

    public Instant getLastAttemptTime() {
        return lastAttemptTime;
    }

    public void setLastAttemptTime(Instant lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public Float getHealthyValue() {
        return healthyValue;
    }

    public void setHealthyValue(Float healthyValue) {
        this.healthyValue = healthyValue;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Boolean isHasIncentive() {
        return hasIncentive;
    }

    public void setHasIncentive(Boolean hasIncentive) {
        this.hasIncentive = hasIncentive;
    }

    public Boolean isIsWaitingPush() {
        return isWaitingPush;
    }

    public void setIsWaitingPush(Boolean isWaitingPush) {
        this.isWaitingPush = isWaitingPush;
    }

    public Instant getAttemptedAt() {
        return attemptedAt;
    }

    public void setAttemptedAt(Instant attemptedAt) {
        this.attemptedAt = attemptedAt;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getSubCategoryCode() {
        return subCategoryCode;
    }

    public void setSubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        WellmetricEventQueueDTO wellmetricEventQueueDTO = (WellmetricEventQueueDTO) o;
        if (wellmetricEventQueueDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), wellmetricEventQueueDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "WellmetricEventQueueDTO{" +
            "id=" + getId() +
            ", participantId='" + getParticipantId() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", email='" + getEmail() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", result='" + getResult() + "'" +
            ", eventCode='" + getEventCode() + "'" +
            ", gender='" + getGender() + "'" +
            ", attemptCount=" + getAttemptCount() +
            ", lastAttemptTime='" + getLastAttemptTime() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", healthyValue=" + getHealthyValue() +
            ", executeStatus='" + getExecuteStatus() + "'" +
            ", eventId='" + getEventId() + "'" +
            ", hasIncentive='" + isHasIncentive() + "'" +
            ", isWaitingPush='" + isIsWaitingPush() + "'" +
            ", attemptedAt='" + getAttemptedAt() + "'" +
            ", programId=" + getProgramId() +
            ", eventDate='" + getEventDate() + "'" +
            ", subCategoryCode='" + getSubCategoryCode() + "'" +
            "}";
    }

    public boolean isAppealApplied() {
        return appealApplied;
    }

    public void setAppealApplied(boolean appealApplied) {
        this.appealApplied = appealApplied;
    }
}
