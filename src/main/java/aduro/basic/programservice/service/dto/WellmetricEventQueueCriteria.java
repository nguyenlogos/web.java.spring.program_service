package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import aduro.basic.programservice.domain.enumeration.ResultStatus;
import aduro.basic.programservice.domain.enumeration.Gender;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.WellmetricEventQueue} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.WellmetricEventQueueResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /wellmetric-event-queues?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class WellmetricEventQueueCriteria implements Serializable, Criteria {
    /**
     * Class for filtering ResultStatus
     */
    public static class ResultStatusFilter extends Filter<ResultStatus> {

        public ResultStatusFilter() {
        }

        public ResultStatusFilter(ResultStatusFilter filter) {
            super(filter);
        }

        @Override
        public ResultStatusFilter copy() {
            return new ResultStatusFilter(this);
        }

    }
    /**
     * Class for filtering Gender
     */
    public static class GenderFilter extends Filter<Gender> {

        public GenderFilter() {
        }

        public GenderFilter(GenderFilter filter) {
            super(filter);
        }

        @Override
        public GenderFilter copy() {
            return new GenderFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter participantId;

    private StringFilter clientId;

    private StringFilter email;

    private StringFilter errorMessage;

    private ResultStatusFilter result;

    private StringFilter eventCode;

    private GenderFilter gender;

    private IntegerFilter attemptCount;

    private InstantFilter lastAttemptTime;

    private InstantFilter createdAt;

    private StringFilter subgroupId;

    private FloatFilter healthyValue;

    private StringFilter executeStatus;

    private StringFilter eventId;

    private BooleanFilter hasIncentive;

    private BooleanFilter isWaitingPush;

    private InstantFilter attemptedAt;

    private LongFilter programId;

    private StringFilter eventDate;

    private StringFilter subCategoryCode;

    public WellmetricEventQueueCriteria(){
    }

    public WellmetricEventQueueCriteria(WellmetricEventQueueCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.participantId = other.participantId == null ? null : other.participantId.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.email = other.email == null ? null : other.email.copy();
        this.errorMessage = other.errorMessage == null ? null : other.errorMessage.copy();
        this.result = other.result == null ? null : other.result.copy();
        this.eventCode = other.eventCode == null ? null : other.eventCode.copy();
        this.gender = other.gender == null ? null : other.gender.copy();
        this.attemptCount = other.attemptCount == null ? null : other.attemptCount.copy();
        this.lastAttemptTime = other.lastAttemptTime == null ? null : other.lastAttemptTime.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.healthyValue = other.healthyValue == null ? null : other.healthyValue.copy();
        this.executeStatus = other.executeStatus == null ? null : other.executeStatus.copy();
        this.eventId = other.eventId == null ? null : other.eventId.copy();
        this.hasIncentive = other.hasIncentive == null ? null : other.hasIncentive.copy();
        this.isWaitingPush = other.isWaitingPush == null ? null : other.isWaitingPush.copy();
        this.attemptedAt = other.attemptedAt == null ? null : other.attemptedAt.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.eventDate = other.eventDate == null ? null : other.eventDate.copy();
        this.subCategoryCode = other.subCategoryCode == null ? null : other.subCategoryCode.copy();
    }

    @Override
    public WellmetricEventQueueCriteria copy() {
        return new WellmetricEventQueueCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getParticipantId() {
        return participantId;
    }

    public void setParticipantId(StringFilter participantId) {
        this.participantId = participantId;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getEmail() {
        return email;
    }

    public void setEmail(StringFilter email) {
        this.email = email;
    }

    public StringFilter getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(StringFilter errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ResultStatusFilter getResult() {
        return result;
    }

    public void setResult(ResultStatusFilter result) {
        this.result = result;
    }

    public StringFilter getEventCode() {
        return eventCode;
    }

    public void setEventCode(StringFilter eventCode) {
        this.eventCode = eventCode;
    }

    public GenderFilter getGender() {
        return gender;
    }

    public void setGender(GenderFilter gender) {
        this.gender = gender;
    }

    public IntegerFilter getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(IntegerFilter attemptCount) {
        this.attemptCount = attemptCount;
    }

    public InstantFilter getLastAttemptTime() {
        return lastAttemptTime;
    }

    public void setLastAttemptTime(InstantFilter lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public FloatFilter getHealthyValue() {
        return healthyValue;
    }

    public void setHealthyValue(FloatFilter healthyValue) {
        this.healthyValue = healthyValue;
    }

    public StringFilter getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(StringFilter executeStatus) {
        this.executeStatus = executeStatus;
    }

    public StringFilter getEventId() {
        return eventId;
    }

    public void setEventId(StringFilter eventId) {
        this.eventId = eventId;
    }

    public BooleanFilter getHasIncentive() {
        return hasIncentive;
    }

    public void setHasIncentive(BooleanFilter hasIncentive) {
        this.hasIncentive = hasIncentive;
    }

    public BooleanFilter getIsWaitingPush() {
        return isWaitingPush;
    }

    public void setIsWaitingPush(BooleanFilter isWaitingPush) {
        this.isWaitingPush = isWaitingPush;
    }

    public InstantFilter getAttemptedAt() {
        return attemptedAt;
    }

    public void setAttemptedAt(InstantFilter attemptedAt) {
        this.attemptedAt = attemptedAt;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public StringFilter getEventDate() {
        return eventDate;
    }

    public void setEventDate(StringFilter eventDate) {
        this.eventDate = eventDate;
    }

    public StringFilter getSubCategoryCode() {
        return subCategoryCode;
    }

    public void setSubCategoryCode(StringFilter subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final WellmetricEventQueueCriteria that = (WellmetricEventQueueCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(participantId, that.participantId) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(email, that.email) &&
            Objects.equals(errorMessage, that.errorMessage) &&
            Objects.equals(result, that.result) &&
            Objects.equals(eventCode, that.eventCode) &&
            Objects.equals(gender, that.gender) &&
            Objects.equals(attemptCount, that.attemptCount) &&
            Objects.equals(lastAttemptTime, that.lastAttemptTime) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(healthyValue, that.healthyValue) &&
            Objects.equals(executeStatus, that.executeStatus) &&
            Objects.equals(eventId, that.eventId) &&
            Objects.equals(hasIncentive, that.hasIncentive) &&
            Objects.equals(isWaitingPush, that.isWaitingPush) &&
            Objects.equals(attemptedAt, that.attemptedAt) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(eventDate, that.eventDate) &&
            Objects.equals(subCategoryCode, that.subCategoryCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        participantId,
        clientId,
        email,
        errorMessage,
        result,
        eventCode,
        gender,
        attemptCount,
        lastAttemptTime,
        createdAt,
        subgroupId,
        healthyValue,
        executeStatus,
        eventId,
        hasIncentive,
        isWaitingPush,
        attemptedAt,
        programId,
        eventDate,
        subCategoryCode
        );
    }

    @Override
    public String toString() {
        return "WellmetricEventQueueCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (participantId != null ? "participantId=" + participantId + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (email != null ? "email=" + email + ", " : "") +
                (errorMessage != null ? "errorMessage=" + errorMessage + ", " : "") +
                (result != null ? "result=" + result + ", " : "") +
                (eventCode != null ? "eventCode=" + eventCode + ", " : "") +
                (gender != null ? "gender=" + gender + ", " : "") +
                (attemptCount != null ? "attemptCount=" + attemptCount + ", " : "") +
                (lastAttemptTime != null ? "lastAttemptTime=" + lastAttemptTime + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (healthyValue != null ? "healthyValue=" + healthyValue + ", " : "") +
                (executeStatus != null ? "executeStatus=" + executeStatus + ", " : "") +
                (eventId != null ? "eventId=" + eventId + ", " : "") +
                (hasIncentive != null ? "hasIncentive=" + hasIncentive + ", " : "") +
                (isWaitingPush != null ? "isWaitingPush=" + isWaitingPush + ", " : "") +
                (attemptedAt != null ? "attemptedAt=" + attemptedAt + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (eventDate != null ? "eventDate=" + eventDate + ", " : "") +
                (subCategoryCode != null ? "subCategoryCode=" + subCategoryCode + ", " : "") +
            "}";
    }

}
