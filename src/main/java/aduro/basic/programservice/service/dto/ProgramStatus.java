package aduro.basic.programservice.service.dto;

public enum ProgramStatus {
    Draft {
        @Override
        public String toString() {
            return "Draft";
        }
    },
    Active {
        @Override
        public String toString() {
            return "Active";
        }
    },
    Completed {
        @Override
        public String toString() {
            return "Completed";
        }
    },
    Template {
        @Override
        public String toString() {
            return "Template";
        }
    },
    Cancelled {
        @Override
        public String toString() {
            return "Cancelled";
        }
    },
    QA_Verify {
        @Override
        public String toString() {
            return "QA Verify";
        }
    }

}




