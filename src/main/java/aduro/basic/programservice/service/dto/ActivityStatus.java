package aduro.basic.programservice.service.dto;

public enum ActivityStatus {
    COMPLETED("Completed"), JOINED("Joined"), NOT_JOINED_YET("Not Joined Yet"), EXPIRED("Expired");

    private String name;

    public String getValue()
    {
        return this.name;
    }

    ActivityStatus(String name)
    {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public static ActivityStatus fromString(String text) {
        for (ActivityStatus b : ActivityStatus.values()) {
            if (b.toString().equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
