package aduro.basic.programservice.service.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.Instant;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserTrackingDataDto {

    private Instant trackingDate;

    private Long trackingValue;

    private String trackingUnit;

    private BigDecimal expectedPoint;

    private Boolean hasEarnedPoint;

}
