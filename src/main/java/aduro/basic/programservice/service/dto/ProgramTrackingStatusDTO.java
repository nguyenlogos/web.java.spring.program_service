package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramTrackingStatus} entity.
 */
public class ProgramTrackingStatusDTO implements Serializable {

    private Long id;

    @NotNull
    private Long programId;

    private String programStatus;

    private Instant createdDate;

    private Instant modifiedDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramStatus() {
        return programStatus;
    }

    public void setProgramStatus(String programStatus) {
        this.programStatus = programStatus;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramTrackingStatusDTO programTrackingStatusDTO = (ProgramTrackingStatusDTO) o;
        if (programTrackingStatusDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programTrackingStatusDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramTrackingStatusDTO{" +
            "id=" + getId() +
            ", programId=" + getProgramId() +
            ", programStatus='" + getProgramStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
