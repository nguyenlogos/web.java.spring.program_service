package aduro.basic.programservice.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Builder
@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class EmployeeDto implements Serializable {

    @JsonProperty("member_id")
    private String memberId;

    @JsonProperty("subgroup_id")
    private String subgroupId;

    @JsonProperty("subgroup_name")
    private String subgroupName;

    @JsonProperty("id")
    private String participantId;

    @JsonProperty("employer_id")
    private String clientId;
}
