package aduro.basic.programservice.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SNSPayloadDto {
    @JsonProperty("Message")
    private String message;
}
