package aduro.basic.programservice.service.dto;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RemovableCollectionContentDto {

    private boolean isRemovable;

    private String itemId;

    private String contentType;
}
