package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import aduro.basic.programservice.domain.enumeration.CohortStatus;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramUserCohort} entity.
 */
public class ProgramUserCohortDTO implements Serializable {

    private Long id;

    private Long progress;

    private Integer numberOfPass;

    private Integer numberOfFailure;

    private CohortStatus status;

    @NotNull
    private Instant createdDate;

    @NotNull
    private Instant modifiedDate;


    private Long programUserId;

    private Long programCohortId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProgress() {
        return progress;
    }

    public void setProgress(Long progress) {
        this.progress = progress;
    }

    public Integer getNumberOfPass() {
        return numberOfPass;
    }

    public void setNumberOfPass(Integer numberOfPass) {
        this.numberOfPass = numberOfPass;
    }

    public Integer getNumberOfFailure() {
        return numberOfFailure;
    }

    public void setNumberOfFailure(Integer numberOfFailure) {
        this.numberOfFailure = numberOfFailure;
    }

    public CohortStatus getStatus() {
        return status;
    }

    public void setStatus(CohortStatus status) {
        this.status = status;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(Long programUserId) {
        this.programUserId = programUserId;
    }

    public Long getProgramCohortId() {
        return programCohortId;
    }

    public void setProgramCohortId(Long programCohortId) {
        this.programCohortId = programCohortId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramUserCohortDTO programUserCohortDTO = (ProgramUserCohortDTO) o;
        if (programUserCohortDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programUserCohortDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramUserCohortDTO{" +
            "id=" + getId() +
            ", progress=" + getProgress() +
            ", numberOfPass=" + getNumberOfPass() +
            ", numberOfFailure=" + getNumberOfFailure() +
            ", status='" + getStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", programUser=" + getProgramUserId() +
            ", programCohort=" + getProgramCohortId() +
            "}";
    }
}
