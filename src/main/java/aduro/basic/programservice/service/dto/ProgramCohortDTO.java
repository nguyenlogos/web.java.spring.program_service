package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramCohort} entity.
 */
public class ProgramCohortDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 150)
    private String cohortName;

    @NotNull
    private Instant createdDate;

    @NotNull
    private Instant modifiedDate;

    private Boolean isDefault;

    @Size(max = 250)
    private String cohortDescription;

    private Long programId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCohortName() {
        return cohortName;
    }

    public void setCohortName(String cohortName) {
        this.cohortName = cohortName;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Boolean isIsDefault() {
        return isDefault;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getCohortDescription() {
        return cohortDescription;
    }

    public void setCohortDescription(String cohortDescription) {
        this.cohortDescription = cohortDescription;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramCohortDTO programCohortDTO = (ProgramCohortDTO) o;
        if (programCohortDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programCohortDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramCohortDTO{" +
            "id=" + getId() +
            ", cohortName='" + getCohortName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", isDefault='" + isIsDefault() + "'" +
            ", cohortDescription='" + getCohortDescription() +
            ", program=" + getProgramId() +
            "}";
    }
}
