package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramUserLevelProgress} entity.
 */
public class ProgramUserLevelProgressDTO implements Serializable {

    private Long id;

    @NotNull
    private Long userEventId;

    @NotNull
    private Long programUserId;

    private Instant createdAt;

    @NotNull
    private Instant levelUpAt;

    private Integer levelFrom;

    private Integer levelTo;

    private Boolean hasReward;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserEventId() {
        return userEventId;
    }

    public void setUserEventId(Long userEventId) {
        this.userEventId = userEventId;
    }

    public Long getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(Long programUserId) {
        this.programUserId = programUserId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getLevelUpAt() {
        return levelUpAt;
    }

    public void setLevelUpAt(Instant levelUpAt) {
        this.levelUpAt = levelUpAt;
    }

    public Integer getLevelFrom() {
        return levelFrom;
    }

    public void setLevelFrom(Integer levelFrom) {
        this.levelFrom = levelFrom;
    }

    public Integer getLevelTo() {
        return levelTo;
    }

    public void setLevelTo(Integer levelTo) {
        this.levelTo = levelTo;
    }

    public Boolean isHasReward() {
        return hasReward;
    }

    public void setHasReward(Boolean hasReward) {
        this.hasReward = hasReward;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramUserLevelProgressDTO programUserLevelProgressDTO = (ProgramUserLevelProgressDTO) o;
        if (programUserLevelProgressDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programUserLevelProgressDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramUserLevelProgressDTO{" +
            "id=" + getId() +
            ", userEventId=" + getUserEventId() +
            ", programUserId=" + getProgramUserId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", levelUpAt='" + getLevelUpAt() + "'" +
            ", levelFrom=" + getLevelFrom() +
            ", levelTo=" + getLevelTo() +
            ", hasReward='" + isHasReward() + "'" +
            "}";
    }
}
