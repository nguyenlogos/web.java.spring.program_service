package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.adp.domain.AmpUserActivityProgressHistory;
import lombok.*;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataProgressInDurationDto {
    private Date startDay;
    private Date endDay;
    private Boolean isCompletedRequired = false;
    private UserTrackingDataDto trackingDataDto;
    private List<AmpUserActivityProgressHistory> activityProgressHistories;
}
