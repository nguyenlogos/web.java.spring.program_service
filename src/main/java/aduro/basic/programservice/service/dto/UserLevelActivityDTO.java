package aduro.basic.programservice.service.dto;

import org.joda.time.DateTime;
import software.amazon.ion.Decimal;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

public class UserLevelActivityDTO {

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    private BigDecimal point;

    private String activityCode;

    private String activityId;

    public String getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    private int level;

    private boolean completed;

    public Instant getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Instant completedDate) {
        this.completedDate = completedDate;
    }

    private Instant completedDate;

}
