package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramUserLevelProgress} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramUserLevelProgressResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-user-level-progresses?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramUserLevelProgressCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter userEventId;

    private LongFilter programUserId;

    private InstantFilter createdAt;

    private InstantFilter levelUpAt;

    private IntegerFilter levelFrom;

    private IntegerFilter levelTo;

    private BooleanFilter hasReward;

    public ProgramUserLevelProgressCriteria(){
    }

    public ProgramUserLevelProgressCriteria(ProgramUserLevelProgressCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.userEventId = other.userEventId == null ? null : other.userEventId.copy();
        this.programUserId = other.programUserId == null ? null : other.programUserId.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.levelUpAt = other.levelUpAt == null ? null : other.levelUpAt.copy();
        this.levelFrom = other.levelFrom == null ? null : other.levelFrom.copy();
        this.levelTo = other.levelTo == null ? null : other.levelTo.copy();
        this.hasReward = other.hasReward == null ? null : other.hasReward.copy();
    }

    @Override
    public ProgramUserLevelProgressCriteria copy() {
        return new ProgramUserLevelProgressCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getUserEventId() {
        return userEventId;
    }

    public void setUserEventId(LongFilter userEventId) {
        this.userEventId = userEventId;
    }

    public LongFilter getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(LongFilter programUserId) {
        this.programUserId = programUserId;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public InstantFilter getLevelUpAt() {
        return levelUpAt;
    }

    public void setLevelUpAt(InstantFilter levelUpAt) {
        this.levelUpAt = levelUpAt;
    }

    public IntegerFilter getLevelFrom() {
        return levelFrom;
    }

    public void setLevelFrom(IntegerFilter levelFrom) {
        this.levelFrom = levelFrom;
    }

    public IntegerFilter getLevelTo() {
        return levelTo;
    }

    public void setLevelTo(IntegerFilter levelTo) {
        this.levelTo = levelTo;
    }

    public BooleanFilter getHasReward() {
        return hasReward;
    }

    public void setHasReward(BooleanFilter hasReward) {
        this.hasReward = hasReward;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramUserLevelProgressCriteria that = (ProgramUserLevelProgressCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(userEventId, that.userEventId) &&
            Objects.equals(programUserId, that.programUserId) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(levelUpAt, that.levelUpAt) &&
            Objects.equals(levelFrom, that.levelFrom) &&
            Objects.equals(levelTo, that.levelTo) &&
            Objects.equals(hasReward, that.hasReward);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        userEventId,
        programUserId,
        createdAt,
        levelUpAt,
        levelFrom,
        levelTo,
        hasReward
        );
    }

    @Override
    public String toString() {
        return "ProgramUserLevelProgressCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (userEventId != null ? "userEventId=" + userEventId + ", " : "") +
                (programUserId != null ? "programUserId=" + programUserId + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (levelUpAt != null ? "levelUpAt=" + levelUpAt + ", " : "") +
                (levelFrom != null ? "levelFrom=" + levelFrom + ", " : "") +
                (levelTo != null ? "levelTo=" + levelTo + ", " : "") +
                (hasReward != null ? "hasReward=" + hasReward + ", " : "") +
            "}";
    }

}
