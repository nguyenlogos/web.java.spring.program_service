package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramSubCategoryPoint} entity.
 */
public class ProgramSubCategoryPointDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    private BigDecimal percentPoint;

    private BigDecimal valuePoint;

    private BigDecimal actualPoint;

    private Integer completionsCap;


    private Long programCategoryPointId;

    private String programCategoryPointCategoryCode;

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    private Boolean locked;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getPercentPoint() {
        return percentPoint;
    }

    public void setPercentPoint(BigDecimal percentPoint) {
        this.percentPoint = percentPoint;
    }

    public BigDecimal getValuePoint() {
        return valuePoint;
    }

    public void setValuePoint(BigDecimal valuePoint) {
        this.valuePoint = valuePoint;
    }

    public BigDecimal getActualPoint() {
        return actualPoint;
    }

    public void setActualPoint(BigDecimal actualPoint) {
        this.actualPoint = actualPoint;
    }

    public Integer getCompletionsCap() {
        return completionsCap;
    }

    public void setCompletionsCap(Integer completionsCap) {
        this.completionsCap = completionsCap;
    }

    public Long getProgramCategoryPointId() {
        return programCategoryPointId;
    }

    public void setProgramCategoryPointId(Long programCategoryPointId) {
        this.programCategoryPointId = programCategoryPointId;
    }

    public String getProgramCategoryPointCategoryCode() {
        return programCategoryPointCategoryCode;
    }

    public void setProgramCategoryPointCategoryCode(String programCategoryPointCategoryCode) {
        this.programCategoryPointCategoryCode = programCategoryPointCategoryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramSubCategoryPointDTO programSubCategoryPointDTO = (ProgramSubCategoryPointDTO) o;
        if (programSubCategoryPointDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programSubCategoryPointDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramSubCategoryPointDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", percentPoint=" + getPercentPoint() +
            ", valuePoint=" + getValuePoint() +
            ", actualPoint=" + getActualPoint() +
            ", completionsCap=" + getCompletionsCap() +
            ", programCategoryPoint=" + getProgramCategoryPointId() +
            ", programCategoryPoint='" + getProgramCategoryPointCategoryCode() + "'" +
            "}";
    }
}
