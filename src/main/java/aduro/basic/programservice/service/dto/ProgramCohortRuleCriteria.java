package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramCohortRule} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramCohortRuleResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-cohort-rules?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramCohortRuleCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter dataInputType;

    private StringFilter rules;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    private LongFilter programCohortId;

    public ProgramCohortRuleCriteria(){
    }

    public ProgramCohortRuleCriteria(ProgramCohortRuleCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.dataInputType = other.dataInputType == null ? null : other.dataInputType.copy();
        this.rules = other.rules == null ? null : other.rules.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
        this.programCohortId = other.programCohortId == null ? null : other.programCohortId.copy();
    }

    @Override
    public ProgramCohortRuleCriteria copy() {
        return new ProgramCohortRuleCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDataInputType() {
        return dataInputType;
    }

    public void setDataInputType(StringFilter dataInputType) {
        this.dataInputType = dataInputType;
    }

    public StringFilter getRules() {
        return rules;
    }

    public void setRules(StringFilter rules) {
        this.rules = rules;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public LongFilter getProgramCohortId() {
        return programCohortId;
    }

    public void setProgramCohortId(LongFilter programCohortId) {
        this.programCohortId = programCohortId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramCohortRuleCriteria that = (ProgramCohortRuleCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(dataInputType, that.dataInputType) &&
            Objects.equals(rules, that.rules) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate) &&
            Objects.equals(programCohortId, that.programCohortId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        dataInputType,
        rules,
        createdDate,
        modifiedDate,
        programCohortId
        );
    }

    @Override
    public String toString() {
        return "ProgramCohortRuleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (dataInputType != null ? "dataInputType=" + dataInputType + ", " : "") +
                (rules != null ? "rules=" + rules + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
                (programCohortId != null ? "programCohortId=" + programCohortId + ", " : "") +
            "}";
    }

}
