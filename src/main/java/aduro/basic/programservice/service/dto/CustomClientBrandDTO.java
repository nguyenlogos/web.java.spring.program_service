package aduro.basic.programservice.service.dto;

import java.time.Instant;

public class CustomClientBrandDTO {

    private Long id;
    private String name;
    private String logoUrl;
    private Instant startDate;
    private Instant endDate;
    private String startTimeZone;
    private String endTimeZone;
    private String assignedClientId;
    private String assignedClientName;
    private String clientUrl;
    private String status;

    public CustomClientBrandDTO(Long id, String name, String logoUrl, Instant startDate, Instant endDate, String startTimeZone, String endTimeZone, String assignedClientId, String assignedClientName, String clientUrl, String status) {
        this.id = id;
        this.name = name;
        this.logoUrl = logoUrl;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTimeZone = startTimeZone;
        this.endTimeZone = endTimeZone;
        this.assignedClientId = assignedClientId;
        this.assignedClientName = assignedClientName;
        this.clientUrl = clientUrl;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getAssignedClientId() {
        return assignedClientId;
    }

    public void setAssignedClientId(String assignedClientId) {
        this.assignedClientId = assignedClientId;
    }

    public String getAssignedClientName() {
        return assignedClientName;
    }

    public void setAssignedClientName(String assignedClientName) {
        this.assignedClientName = assignedClientName;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public String getStartTimeZone() {
        return startTimeZone;
    }

    public void setStartTimeZone(String startTimeZone) {
        this.startTimeZone = startTimeZone;
    }

    public String getEndTimeZone() {
        return endTimeZone;
    }

    public void setEndTimeZone(String endTimeZone) {
        this.endTimeZone = endTimeZone;
    }
}
