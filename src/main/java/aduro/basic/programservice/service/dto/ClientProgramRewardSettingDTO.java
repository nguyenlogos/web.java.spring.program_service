package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ClientProgramRewardSetting} entity.
 */
public class ClientProgramRewardSettingDTO implements Serializable {

    private Long id;

    @NotNull
    private String clientId;

    @NotNull
    private String clientName;

    @NotNull
    private Instant updatedDate;

    @NotNull
    private String updatedBy;

    private String customerIdentifier;

    private String accountIdentifier;

    private BigDecimal accountThreshold;

    private BigDecimal currentBalance;

    private String clientEmail;


    private Long programId;

    private String programName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCustomerIdentifier() {
        return customerIdentifier;
    }

    public void setCustomerIdentifier(String customerIdentifier) {
        this.customerIdentifier = customerIdentifier;
    }

    public String getAccountIdentifier() {
        return accountIdentifier;
    }

    public void setAccountIdentifier(String accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }

    public BigDecimal getAccountThreshold() {
        return accountThreshold;
    }

    public void setAccountThreshold(BigDecimal accountThreshold) {
        this.accountThreshold = accountThreshold;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = (ClientProgramRewardSettingDTO) o;
        if (clientProgramRewardSettingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientProgramRewardSettingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientProgramRewardSettingDTO{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", customerIdentifier='" + getCustomerIdentifier() + "'" +
            ", accountIdentifier='" + getAccountIdentifier() + "'" +
            ", accountThreshold=" + getAccountThreshold() +
            ", currentBalance=" + getCurrentBalance() +
            ", clientEmail='" + getClientEmail() + "'" +
            ", program=" + getProgramId() +
            ", program='" + getProgramName() + "'" +
            "}";
    }
}
