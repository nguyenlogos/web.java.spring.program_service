package aduro.basic.programservice.service.dto;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProcessIncentiveActivityResponse {
    private BigDecimal earnedPoint;
    private BigDecimal totalPoint;
    private List<String> rewards;
    private Integer earnLevel;
    private HttpStatus httpStatus;
    private String message;
    private List<String> errors;
}
