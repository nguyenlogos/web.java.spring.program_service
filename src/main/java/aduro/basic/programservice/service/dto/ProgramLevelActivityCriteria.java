package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramLevelActivity} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramLevelActivityResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-level-activities?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramLevelActivityCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter activityCode;

    private StringFilter activityId;

    private StringFilter subgroupId;

    private StringFilter subgroupName;

    private LongFilter programLevelId;

    public ProgramLevelActivityCriteria(){
    }

    public ProgramLevelActivityCriteria(ProgramLevelActivityCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.activityCode = other.activityCode == null ? null : other.activityCode.copy();
        this.activityId = other.activityId == null ? null : other.activityId.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.subgroupName = other.subgroupName == null ? null : other.subgroupName.copy();
        this.programLevelId = other.programLevelId == null ? null : other.programLevelId.copy();
    }

    @Override
    public ProgramLevelActivityCriteria copy() {
        return new ProgramLevelActivityCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(StringFilter activityCode) {
        this.activityCode = activityCode;
    }

    public StringFilter getActivityId() {
        return activityId;
    }

    public void setActivityId(StringFilter activityId) {
        this.activityId = activityId;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public StringFilter getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(StringFilter subgroupName) {
        this.subgroupName = subgroupName;
    }

    public LongFilter getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(LongFilter programLevelId) {
        this.programLevelId = programLevelId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramLevelActivityCriteria that = (ProgramLevelActivityCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(activityCode, that.activityCode) &&
            Objects.equals(activityId, that.activityId) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(subgroupName, that.subgroupName) &&
            Objects.equals(programLevelId, that.programLevelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        activityCode,
        activityId,
        subgroupId,
        subgroupName,
        programLevelId
        );
    }

    @Override
    public String toString() {
        return "ProgramLevelActivityCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (activityCode != null ? "activityCode=" + activityCode + ", " : "") +
                (activityId != null ? "activityId=" + activityId + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (subgroupName != null ? "subgroupName=" + subgroupName + ", " : "") +
                (programLevelId != null ? "programLevelId=" + programLevelId + ", " : "") +
            "}";
    }

}
