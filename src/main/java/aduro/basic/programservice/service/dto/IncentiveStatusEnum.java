package aduro.basic.programservice.service.dto;


public enum IncentiveStatusEnum {
    New {
        @Override
        public String toString() {
            return "New";
        }
    },
    Sending {
        @Override
        public String toString() {
            return "Sending";
        }
    },
    Completed {
        @Override
        public String toString() {
            return "Completed";
        }
    },
    Running {
        @Override
        public String toString() {
            return "Running";
        }
    },
    Retry {
        @Override
        public String toString() {
            return "Retry";
        }
    },
    Error {
        @Override
        public String toString() {
            return "Error";
        }
    };

}
