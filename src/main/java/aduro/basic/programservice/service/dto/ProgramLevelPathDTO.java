package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramLevelPath} entity.
 */
public class ProgramLevelPathDTO implements Serializable {

    private Long id;

    @NotNull
    private String pathId;

    @NotNull
    private String name;

    private String subgroupId;

    private String subgroupName;

    private String pathType;

    private String pathCategory;

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    private BigDecimal point;

    private Long programLevelId;

    private String programLevelName;

    public Instant getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Instant completedDate) {
        this.completedDate = completedDate;
    }

    private Instant completedDate;

    public Integer getProgramLevelLevelOrder() {
        return programLevelLevelOrder;
    }

    public void setProgramLevelLevelOrder(Integer programLevelLevelOrder) {
        this.programLevelLevelOrder = programLevelLevelOrder;
    }

    private Integer programLevelLevelOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPathId() {
        return pathId;
    }

    public void setPathId(String pathId) {
        this.pathId = pathId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public String getPathType() {
        return pathType;
    }

    public void setPathType(String pathType) {
        this.pathType = pathType;
    }

    public String getPathCategory() {
        return pathCategory;
    }

    public void setPathCategory(String pathCategory) {
        this.pathCategory = pathCategory;
    }

    public Long getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(Long programLevelId) {
        this.programLevelId = programLevelId;
    }

    public String getProgramLevelName() {
        return programLevelName;
    }

    public void setProgramLevelName(String programLevelName) {
        this.programLevelName = programLevelName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramLevelPathDTO programLevelPathDTO = (ProgramLevelPathDTO) o;
        if (programLevelPathDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programLevelPathDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramLevelPathDTO{" +
            "id=" + getId() +
            ", pathId='" + getPathId() + "'" +
            ", name='" + getName() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", pathType='" + getPathType() + "'" +
            ", pathCategory='" + getPathCategory() + "'" +
            ", programLevel=" + getProgramLevelId() +
            ", programLevel='" + getProgramLevelName() + "'" +
            "}";
    }
}
