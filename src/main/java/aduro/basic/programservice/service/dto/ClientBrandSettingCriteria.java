package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ClientBrandSetting} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ClientBrandSettingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /client-brand-settings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClientBrandSettingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter clientId;

    private StringFilter subPathUrl;

    private StringFilter webLogoUrl;

    private StringFilter primaryColorValue;

    private StringFilter clientName;

    private StringFilter clientUrl;

    public ClientBrandSettingCriteria(){
    }

    public ClientBrandSettingCriteria(ClientBrandSettingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.subPathUrl = other.subPathUrl == null ? null : other.subPathUrl.copy();
        this.webLogoUrl = other.webLogoUrl == null ? null : other.webLogoUrl.copy();
        this.primaryColorValue = other.primaryColorValue == null ? null : other.primaryColorValue.copy();
        this.clientName = other.clientName == null ? null : other.clientName.copy();
        this.clientUrl = other.clientUrl == null ? null : other.clientUrl.copy();
    }

    @Override
    public ClientBrandSettingCriteria copy() {
        return new ClientBrandSettingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getSubPathUrl() {
        return subPathUrl;
    }

    public void setSubPathUrl(StringFilter subPathUrl) {
        this.subPathUrl = subPathUrl;
    }

    public StringFilter getWebLogoUrl() {
        return webLogoUrl;
    }

    public void setWebLogoUrl(StringFilter webLogoUrl) {
        this.webLogoUrl = webLogoUrl;
    }

    public StringFilter getPrimaryColorValue() {
        return primaryColorValue;
    }

    public void setPrimaryColorValue(StringFilter primaryColorValue) {
        this.primaryColorValue = primaryColorValue;
    }

    public StringFilter getClientName() {
        return clientName;
    }

    public void setClientName(StringFilter clientName) {
        this.clientName = clientName;
    }

    public StringFilter getClientUrl() {
        return clientUrl;
    }

    public void setClientUrl(StringFilter clientUrl) {
        this.clientUrl = clientUrl;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClientBrandSettingCriteria that = (ClientBrandSettingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(subPathUrl, that.subPathUrl) &&
            Objects.equals(webLogoUrl, that.webLogoUrl) &&
            Objects.equals(primaryColorValue, that.primaryColorValue) &&
            Objects.equals(clientName, that.clientName) &&
            Objects.equals(clientUrl, that.clientUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        clientId,
        subPathUrl,
        webLogoUrl,
        primaryColorValue,
        clientName,
        clientUrl
        );
    }

    @Override
    public String toString() {
        return "ClientBrandSettingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (subPathUrl != null ? "subPathUrl=" + subPathUrl + ", " : "") +
                (webLogoUrl != null ? "webLogoUrl=" + webLogoUrl + ", " : "") +
                (primaryColorValue != null ? "primaryColorValue=" + primaryColorValue + ", " : "") +
                (clientName != null ? "clientName=" + clientName + ", " : "") +
                (clientUrl != null ? "clientUrl=" + clientUrl + ", " : "") +
            "}";
    }

}
