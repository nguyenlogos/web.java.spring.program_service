package aduro.basic.programservice.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IncentiveEventDTO implements Serializable {

    @NotNull
    private String eventCode;

    @NotNull
    private String eventId;

    private Instant eventDate;

    private String participantId;

    private String clientId;

    private String subgroupName;

    private WellmetricBiometricDTO wellmetricBiometricDTO;

    private PreventiveDto preventiveDto;

    private AppealDto appealDto;

    private String eventCategory;

    private String clientName;

    private Long programId;


    private String email;

    private String firstName;

    private String lastName;

    private String subgroupId;

    @JsonProperty("source")
    private String sourceVendor;

    private Boolean asProgramTester;

    public boolean isAsProgramTester(){
        return asProgramTester != null ? asProgramTester: false;
    }

    public IncentiveEventDTO(String eventCode, String eventId, String clientId, Instant eventDate, String participantId){
        this.eventCode = eventCode;
        this.clientId = clientId;
        this.eventId = eventId;
        this.eventDate = eventDate;
        this.participantId = participantId;
    }
}
