package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramSubgroup} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramSubgroupResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-subgroups?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramSubgroupCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter subgroupId;

    private StringFilter subgroupName;

    private LongFilter programId;

    public ProgramSubgroupCriteria(){
    }

    public ProgramSubgroupCriteria(ProgramSubgroupCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.subgroupName = other.subgroupName == null ? null : other.subgroupName.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
    }

    @Override
    public ProgramSubgroupCriteria copy() {
        return new ProgramSubgroupCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public StringFilter getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(StringFilter subgroupName) {
        this.subgroupName = subgroupName;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramSubgroupCriteria that = (ProgramSubgroupCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(subgroupName, that.subgroupName) &&
            Objects.equals(programId, that.programId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        subgroupId,
        subgroupName,
        programId
        );
    }

    @Override
    public String toString() {
        return "ProgramSubgroupCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (subgroupName != null ? "subgroupName=" + subgroupName + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
            "}";
    }

}
