package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.UserEvent} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.UserEventResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /user-events?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class UserEventCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter eventCode;

    private StringFilter eventId;

    private InstantFilter eventDate;

    private BigDecimalFilter eventPoint;

    private StringFilter eventCategory;

    private LongFilter programUserId;

    public UserEventCriteria(){
    }

    public UserEventCriteria(UserEventCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.eventCode = other.eventCode == null ? null : other.eventCode.copy();
        this.eventId = other.eventId == null ? null : other.eventId.copy();
        this.eventDate = other.eventDate == null ? null : other.eventDate.copy();
        this.eventPoint = other.eventPoint == null ? null : other.eventPoint.copy();
        this.eventCategory = other.eventCategory == null ? null : other.eventCategory.copy();
        this.programUserId = other.programUserId == null ? null : other.programUserId.copy();
    }

    @Override
    public UserEventCriteria copy() {
        return new UserEventCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEventCode() {
        return eventCode;
    }

    public void setEventCode(StringFilter eventCode) {
        this.eventCode = eventCode;
    }

    public StringFilter getEventId() {
        return eventId;
    }

    public void setEventId(StringFilter eventId) {
        this.eventId = eventId;
    }

    public InstantFilter getEventDate() {
        return eventDate;
    }

    public void setEventDate(InstantFilter eventDate) {
        this.eventDate = eventDate;
    }

    public BigDecimalFilter getEventPoint() {
        return eventPoint;
    }

    public void setEventPoint(BigDecimalFilter eventPoint) {
        this.eventPoint = eventPoint;
    }

    public StringFilter getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(StringFilter eventCategory) {
        this.eventCategory = eventCategory;
    }

    public LongFilter getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(LongFilter programUserId) {
        this.programUserId = programUserId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final UserEventCriteria that = (UserEventCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(eventCode, that.eventCode) &&
            Objects.equals(eventId, that.eventId) &&
            Objects.equals(eventDate, that.eventDate) &&
            Objects.equals(eventPoint, that.eventPoint) &&
            Objects.equals(eventCategory, that.eventCategory) &&
            Objects.equals(programUserId, that.programUserId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        eventCode,
        eventId,
        eventDate,
        eventPoint,
        eventCategory,
        programUserId
        );
    }

    @Override
    public String toString() {
        return "UserEventCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (eventCode != null ? "eventCode=" + eventCode + ", " : "") +
                (eventId != null ? "eventId=" + eventId + ", " : "") +
                (eventDate != null ? "eventDate=" + eventDate + ", " : "") +
                (eventPoint != null ? "eventPoint=" + eventPoint + ", " : "") +
                (eventCategory != null ? "eventCategory=" + eventCategory + ", " : "") +
                (programUserId != null ? "programUserId=" + programUserId + ", " : "") +
            "}";
    }

}
