package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramCohortRule} entity.
 */
public class ProgramCohortRuleDTO implements Serializable {

    private Long id;

    @NotNull
    private String dataInputType;

    @NotNull
    private String rules;

    @NotNull
    private Instant createdDate;

    @NotNull
    private Instant modifiedDate;


    private Long programCohortId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataInputType() {
        return dataInputType;
    }

    public void setDataInputType(String dataInputType) {
        this.dataInputType = dataInputType;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getProgramCohortId() {
        return programCohortId;
    }

    public void setProgramCohortId(Long programCohortId) {
        this.programCohortId = programCohortId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramCohortRuleDTO programCohortRuleDTO = (ProgramCohortRuleDTO) o;
        if (programCohortRuleDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programCohortRuleDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramCohortRuleDTO{" +
            "id=" + getId() +
            ", dataInputType='" + getDataInputType() + "'" +
            ", rules='" + getRules() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", programCohort=" + getProgramCohortId() +
            "}";
    }
}
