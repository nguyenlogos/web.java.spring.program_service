package aduro.basic.programservice.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProgramUserCreateRequest implements Serializable {

    @NotNull
    private String aduroId;

    @NotNull
    private String clientId;

    private String email;

    private String firstName;

    private String lastName;

    private String clientName;

    private String subgroupId;

    private String subgroupName;

    boolean isProgramTester;
}
