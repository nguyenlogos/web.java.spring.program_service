package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramCategoryPoint} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramCategoryPointResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-category-points?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramCategoryPointCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter categoryCode;

    private StringFilter categoryName;

    private BigDecimalFilter percentPoint;

    private BigDecimalFilter valuePoint;

    private LongFilter programId;

    public ProgramCategoryPointCriteria(){
    }

    public ProgramCategoryPointCriteria(ProgramCategoryPointCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.categoryCode = other.categoryCode == null ? null : other.categoryCode.copy();
        this.categoryName = other.categoryName == null ? null : other.categoryName.copy();
        this.percentPoint = other.percentPoint == null ? null : other.percentPoint.copy();
        this.valuePoint = other.valuePoint == null ? null : other.valuePoint.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
    }

    @Override
    public ProgramCategoryPointCriteria copy() {
        return new ProgramCategoryPointCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(StringFilter categoryCode) {
        this.categoryCode = categoryCode;
    }

    public StringFilter getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(StringFilter categoryName) {
        this.categoryName = categoryName;
    }

    public BigDecimalFilter getPercentPoint() {
        return percentPoint;
    }

    public void setPercentPoint(BigDecimalFilter percentPoint) {
        this.percentPoint = percentPoint;
    }

    public BigDecimalFilter getValuePoint() {
        return valuePoint;
    }

    public void setValuePoint(BigDecimalFilter valuePoint) {
        this.valuePoint = valuePoint;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramCategoryPointCriteria that = (ProgramCategoryPointCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(categoryCode, that.categoryCode) &&
            Objects.equals(categoryName, that.categoryName) &&
            Objects.equals(percentPoint, that.percentPoint) &&
            Objects.equals(valuePoint, that.valuePoint) &&
            Objects.equals(programId, that.programId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        categoryCode,
        categoryName,
        percentPoint,
        valuePoint,
        programId
        );
    }

    @Override
    public String toString() {
        return "ProgramCategoryPointCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (categoryCode != null ? "categoryCode=" + categoryCode + ", " : "") +
                (categoryName != null ? "categoryName=" + categoryName + ", " : "") +
                (percentPoint != null ? "percentPoint=" + percentPoint + ", " : "") +
                (valuePoint != null ? "valuePoint=" + valuePoint + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
            "}";
    }

}
