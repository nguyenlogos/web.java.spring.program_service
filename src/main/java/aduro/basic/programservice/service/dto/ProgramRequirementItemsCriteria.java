package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramRequirementItems} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramRequirementItemsResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-requirement-items?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramRequirementItemsCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter itemId;

    private StringFilter nameOfItem;

    private StringFilter typeOfItem;

    private StringFilter itemCode;

    private StringFilter subgroupId;

    private StringFilter subgroupName;

    private InstantFilter createdAt;

    private LongFilter programLevelId;

    public ProgramRequirementItemsCriteria(){
    }

    public ProgramRequirementItemsCriteria(ProgramRequirementItemsCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.itemId = other.itemId == null ? null : other.itemId.copy();
        this.nameOfItem = other.nameOfItem == null ? null : other.nameOfItem.copy();
        this.typeOfItem = other.typeOfItem == null ? null : other.typeOfItem.copy();
        this.itemCode = other.itemCode == null ? null : other.itemCode.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.subgroupName = other.subgroupName == null ? null : other.subgroupName.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.programLevelId = other.programLevelId == null ? null : other.programLevelId.copy();
    }

    @Override
    public ProgramRequirementItemsCriteria copy() {
        return new ProgramRequirementItemsCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getItemId() {
        return itemId;
    }

    public void setItemId(StringFilter itemId) {
        this.itemId = itemId;
    }

    public StringFilter getNameOfItem() {
        return nameOfItem;
    }

    public void setNameOfItem(StringFilter nameOfItem) {
        this.nameOfItem = nameOfItem;
    }

    public StringFilter getTypeOfItem() {
        return typeOfItem;
    }

    public void setTypeOfItem(StringFilter typeOfItem) {
        this.typeOfItem = typeOfItem;
    }

    public StringFilter getItemCode() {
        return itemCode;
    }

    public void setItemCode(StringFilter itemCode) {
        this.itemCode = itemCode;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public StringFilter getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(StringFilter subgroupName) {
        this.subgroupName = subgroupName;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public LongFilter getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(LongFilter programLevelId) {
        this.programLevelId = programLevelId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramRequirementItemsCriteria that = (ProgramRequirementItemsCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(itemId, that.itemId) &&
            Objects.equals(nameOfItem, that.nameOfItem) &&
            Objects.equals(typeOfItem, that.typeOfItem) &&
            Objects.equals(itemCode, that.itemCode) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(subgroupName, that.subgroupName) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(programLevelId, that.programLevelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        itemId,
        nameOfItem,
        typeOfItem,
        itemCode,
        subgroupId,
        subgroupName,
        createdAt,
        programLevelId
        );
    }

    @Override
    public String toString() {
        return "ProgramRequirementItemsCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (itemId != null ? "itemId=" + itemId + ", " : "") +
                (nameOfItem != null ? "nameOfItem=" + nameOfItem + ", " : "") +
                (typeOfItem != null ? "typeOfItem=" + typeOfItem + ", " : "") +
                (itemCode != null ? "itemCode=" + itemCode + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (subgroupName != null ? "subgroupName=" + subgroupName + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (programLevelId != null ? "programLevelId=" + programLevelId + ", " : "") +
            "}";
    }

}
