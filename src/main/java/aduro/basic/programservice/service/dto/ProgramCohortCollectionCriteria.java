package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramCohortCollection} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramCohortCollectionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-cohort-collections?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramCohortCollectionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter requiredCompletion;

    private IntegerFilter requiredLevel;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    private LongFilter programCollectionId;

    private LongFilter programCohortId;

    public ProgramCohortCollectionCriteria(){
    }

    public ProgramCohortCollectionCriteria(ProgramCohortCollectionCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.requiredCompletion = other.requiredCompletion == null ? null : other.requiredCompletion.copy();
        this.requiredLevel = other.requiredLevel == null ? null : other.requiredLevel.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
        this.programCollectionId = other.programCollectionId == null ? null : other.programCollectionId.copy();
        this.programCohortId = other.programCohortId == null ? null : other.programCohortId.copy();
    }

    @Override
    public ProgramCohortCollectionCriteria copy() {
        return new ProgramCohortCollectionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getRequiredCompletion() {
        return requiredCompletion;
    }

    public void setRequiredCompletion(IntegerFilter requiredCompletion) {
        this.requiredCompletion = requiredCompletion;
    }

    public IntegerFilter getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(IntegerFilter requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public LongFilter getProgramCollectionId() {
        return programCollectionId;
    }

    public void setProgramCollectionId(LongFilter programCollectionId) {
        this.programCollectionId = programCollectionId;
    }

    public LongFilter getProgramCohortId() {
        return programCohortId;
    }

    public void setProgramCohortId(LongFilter programCohortId) {
        this.programCohortId = programCohortId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramCohortCollectionCriteria that = (ProgramCohortCollectionCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(requiredCompletion, that.requiredCompletion) &&
            Objects.equals(requiredLevel, that.requiredLevel) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate) &&
            Objects.equals(programCollectionId, that.programCollectionId) &&
            Objects.equals(programCohortId, that.programCohortId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        requiredCompletion,
        requiredLevel,
        createdDate,
        modifiedDate,
        programCollectionId,
        programCohortId
        );
    }

    @Override
    public String toString() {
        return "ProgramCohortCollectionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (requiredCompletion != null ? "requiredCompletion=" + requiredCompletion + ", " : "") +
                (requiredLevel != null ? "requiredLevel=" + requiredLevel + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
                (programCollectionId != null ? "programCollectionId=" + programCollectionId + ", " : "") +
                (programCohortId != null ? "programCohortId=" + programCohortId + ", " : "") +
            "}";
    }

}
