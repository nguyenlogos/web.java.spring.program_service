package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.enumeration.ProgramType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link Program} entity.
 */
public class ProgramDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    private Instant startDate;

    private Instant resetDate;

    private LocalDate lastSent;

    private Boolean isRetriggerEmail;

    private Boolean isEligible;

    private Boolean isSentRegistrationEmail;

    private Boolean isRegisteredForPlatform;

    private Boolean isScheduledScreening;

    private Boolean isFunctionally;

    private String logoUrl;

    private String description;

    private BigDecimal userPoint;

    private Instant lastModifiedDate;

    private Instant createdDate;

    private String lastModifiedBy;

    private String createdBy;

    private String status;

    private Boolean isUsePoint;

    private Boolean isScreen;

    private Boolean isTemplate;

    private Boolean isPreview;

    private Instant tobaccoProgramDeadline;

    private Instant tobaccoAttestationDeadline;

    private Boolean isTobaccoSurchargeManagement;

    private String programRegisterMessage;

    private Boolean isAwardedForTobaccoAttestation;

    private Boolean isAwardedForTobaccoRas;

    private List<DemoGraphicDto> demographics;

    private String appealsFormFileUrl;

    private String appealsFormText;

    private Boolean isAppealsForm;

    private boolean qaVerify;

    private Instant programQAStartDate;

    private Instant programQAEndDate;

    private BigDecimal economyPoint;

    private Boolean isExtractable;
    private String hpsfFormType;

    private ProgramType programType;

    private String wellmetricSchedulerV1Url;

    public String getAppealsFormFileUrl() {
        return appealsFormFileUrl;
    }

    public void setAppealsFormFileUrl(String appealsFormFileUrl) {
        this.appealsFormFileUrl = appealsFormFileUrl;
    }

    public String getAppealsFormText() {
        return appealsFormText;
    }

    public void setAppealsFormText(String appealsFormText) {
        this.appealsFormText = appealsFormText;
    }

    public Boolean getIsAppealsForm() {
        return isAppealsForm;
    }

    public void setIsAppealsForm(Boolean isAppealsForm) {
        this.isAppealsForm = isAppealsForm;
    }

    public Boolean isAwardedForTobaccoAttestation() {
        return isAwardedForTobaccoAttestation;
    }

    public void setIsAwardedForTobaccoAttestation(Boolean isAwardedForTobaccoAttestation) {
        this.isAwardedForTobaccoAttestation = isAwardedForTobaccoAttestation;
    }

    public Boolean isAwardedForTobaccoRas() {
        return isAwardedForTobaccoRas;
    }

    public void setIsAwardedForTobaccoRas(Boolean isAwardedForTobaccoRas) {
        this.isAwardedForTobaccoRas = isAwardedForTobaccoRas;
    }

    public Boolean getAwardedForTobaccoAttestation() {
        return isAwardedForTobaccoAttestation;
    }

    public void setAwardedForTobaccoAttestation(Boolean awardedForTobaccoAttestation) {
        isAwardedForTobaccoAttestation = awardedForTobaccoAttestation;
    }

    public Boolean getAwardedForTobaccoRas() {
        return isAwardedForTobaccoRas;
    }

    public void setAwardedForTobaccoRas(Boolean awardedForTobaccoRas) {
        isAwardedForTobaccoRas = awardedForTobaccoRas;
    }



    public Instant getTobaccoProgramDeadline() {
        return tobaccoProgramDeadline;
    }

    public void setTobaccoProgramDeadline(Instant tobaccoProgramDeadline) {
        this.tobaccoProgramDeadline = tobaccoProgramDeadline;
    }

    public Instant getTobaccoAttestationDeadline() {
        return tobaccoAttestationDeadline;
    }

    public void setTobaccoAttestationDeadline(Instant tobaccoAttestationDeadline) {
        this.tobaccoAttestationDeadline = tobaccoAttestationDeadline;
    }

    public Boolean getTobaccoSurchargeManagement() {
        return isTobaccoSurchargeManagement;
    }

    public void setTobaccoSurchargeManagement(Boolean tobaccoSurchargeManagement) {
        isTobaccoSurchargeManagement = tobaccoSurchargeManagement;
    }

    public String getLandingBackgroundImageUrl() {
        return landingBackgroundImageUrl;
    }

    public void setLandingBackgroundImageUrl(String landingBackgroundImageUrl) {
        this.landingBackgroundImageUrl = landingBackgroundImageUrl;
    }

    private String landingBackgroundImageUrl;

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    private String colorValue;

    public String getLevelStructure() {
        return levelStructure;
    }

    public void setLevelStructure(String levelStructure) {
        this.levelStructure = levelStructure;
    }

    private String levelStructure;

    public Instant getPreviewDate() {
        return previewDate;
    }

    public void setPreviewDate(Instant previewDate) {
        this.previewDate = previewDate;
    }

    public String getPreviewTimeZone() {
        return previewTimeZone;
    }

    public void setPreviewTimeZone(String previewTimeZone) {
        this.previewTimeZone = previewTimeZone;
    }

    public String getStartTimeZone() {
        return startTimeZone;
    }

    public void setStartTimeZone(String startTimeZone) {
        this.startTimeZone = startTimeZone;
    }

    public String getEndTimeZone() {
        return endTimeZone;
    }

    public void setEndTimeZone(String endTimeZone) {
        this.endTimeZone = endTimeZone;
    }

    private Instant previewDate;

    private String previewTimeZone;

    private String startTimeZone;

    private String endTimeZone;

    private Integer programLength;

    private Boolean applyRewardAllSubgroup;

    private Boolean isHPSF;

    private Boolean isHTK;
    private Boolean isLabcorp;
    private String labcorpAccountNumber;
    private String labcorpFileUrl;

    private Instant biometricDeadlineDate;

    public Instant getBiometricDeadlineDate() {
        return biometricDeadlineDate;
    }

    public void setBiometricDeadlineDate(Instant biometricDeadlineDate) {
        this.biometricDeadlineDate = biometricDeadlineDate;
    }

    public Instant getBiometricLookbackDate() {
        return biometricLookbackDate;
    }

    public void setBiometricLookbackDate(Instant biometricLookbackDate) {
        this.biometricLookbackDate = biometricLookbackDate;
    }

    private Instant biometricLookbackDate;

    private String subPathUrl;


    public Boolean isApplyRewardAllSubgroup() {
        return applyRewardAllSubgroup;
    }

    public ProgramDTO applyRewardAllSubgroup(Boolean applyRewardAllSubgroup) {
        this.applyRewardAllSubgroup = applyRewardAllSubgroup;
        return this;
    }

    public void setApplyRewardAllSubgroup(Boolean applyRewardAllSubgroup) {
        this.applyRewardAllSubgroup = applyRewardAllSubgroup;
    }


    public Boolean isIsPreview() {
        return isPreview;
    }

    public ProgramDTO isPreview(Boolean isPreview) {
        this.isPreview = isPreview;
        return this;
    }

    public void setIsPreview(Boolean isPreview) {
        this.isPreview = isPreview;
    }



    public Boolean getIsTemplate() {
        return isTemplate;
    }


    public BigDecimal getTotalPercentEconomy() {

        return  totalPercentEconomy;
    }

    public void setTotalPercentEconomy(BigDecimal totalPercentEconomy) {
        this.totalPercentEconomy = totalPercentEconomy;
    }

    private BigDecimal totalPercentEconomy;

    public Boolean isIsUseLevel() {
        return isUseLevel;
    }

    public void setIsUseLevel(Boolean isUseLevel) {
        this.isUseLevel = isUseLevel;
    }

    private Boolean isUseLevel;
    private String clientId;
    public String getClientId(){
        return clientId;
    }

    public void setClientId(String clientId){
        this.clientId = clientId;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getResetDate() {
        return resetDate;
    }

    public void setResetDate(Instant resetDate) {
        this.resetDate = resetDate;
    }

    public LocalDate getLastSent() {
        return lastSent;
    }

    public void setLastSent(LocalDate lastSent) {
        this.lastSent = lastSent;
    }

    public Boolean isIsRetriggerEmail() {
        return isRetriggerEmail;
    }

    public void setIsRetriggerEmail(Boolean isRetriggerEmail) {
        this.isRetriggerEmail = isRetriggerEmail;
    }

    public Boolean isIsEligible() {
        return isEligible;
    }

    public void setIsEligible(Boolean isEligible) {
        this.isEligible = isEligible;
    }

    public Boolean isIsSentRegistrationEmail() {
        return isSentRegistrationEmail;
    }

    public void setIsSentRegistrationEmail(Boolean isSentRegistrationEmail) {
        this.isSentRegistrationEmail = isSentRegistrationEmail;
    }

    public Boolean isIsRegisteredForPlatform() {
        return isRegisteredForPlatform;
    }

    public void setIsRegisteredForPlatform(Boolean isRegisteredForPlatform) {
        this.isRegisteredForPlatform = isRegisteredForPlatform;
    }

    public Boolean isIsScheduledScreening() {
        return isScheduledScreening;
    }

    public void setIsScheduledScreening(Boolean isScheduledScreening) {
        this.isScheduledScreening = isScheduledScreening;
    }

    public Boolean isIsFunctionally() {
        return isFunctionally;
    }

    public void setIsFunctionally(Boolean isFunctionally) {
        this.isFunctionally = isFunctionally;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(BigDecimal userPoint) {
        this.userPoint = userPoint;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean isIsUsePoint() {
        return isUsePoint;
    }

    public void setIsUsePoint(Boolean isUsePoint) {
        this.isUsePoint = isUsePoint;
    }

    public Boolean isIsScreen() {
        return isScreen;
    }

    public void setIsScreen(Boolean isScreen) {
        this.isScreen = isScreen;
    }

    public Boolean isIsTemplate() {
        return isTemplate;
    }

    public void setIsTemplate(Boolean isTemplate) {
        this.isTemplate = isTemplate;
    }


    public Boolean isIsHPSF() {
        return isHPSF;
    }

    public void setIsHPSF(Boolean isHPSF) {
        this.isHPSF = isHPSF;
    }

    public Boolean isIsHTK() {
        return isHTK;
    }

    public void setIsHTK(Boolean isHTK) {
        this.isHTK = isHTK;
    }

    public Boolean isIsLabcorp() {
        return isLabcorp;
    }

    public void setIsLabcorp(Boolean isLabcorp) {
        this.isLabcorp = isLabcorp;
    }

    public String getLabcorpAccountNumber() {
        return labcorpAccountNumber;
    }

    public void setLabcorpAccountNumber(String labcorpAccountNumber) {
        this.labcorpAccountNumber = labcorpAccountNumber;
    }

    public String getLabcorpFileUrl() {
        return labcorpFileUrl;
    }

    public void setLabcorpFileUrl(String labcorpFileUrl) {
        this.labcorpFileUrl = labcorpFileUrl;
    }

    public String getProgramRegisterMessage() {
        return programRegisterMessage;
    }

    public void setProgramRegisterMessage(String programRegisterMessage) {
        this.programRegisterMessage = programRegisterMessage;
    }

    public String getSubPathUrl() {
        return subPathUrl;
    }

    public void setSubPathUrl(String subPathUrl) {
        this.subPathUrl = subPathUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramDTO programDTO = (ProgramDTO) o;
        if (programDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", resetDate='" + getResetDate() + "'" +
            ", lastSent='" + getLastSent() + "'" +
            ", isRetriggerEmail='" + isIsRetriggerEmail() + "'" +
            ", isEligible='" + isIsEligible() + "'" +
            ", isSentRegistrationEmail='" + isIsSentRegistrationEmail() + "'" +
            ", isRegisteredForPlatform='" + isIsRegisteredForPlatform() + "'" +
            ", isScheduledScreening='" + isIsScheduledScreening() + "'" +
            ", isFunctionally='" + isIsFunctionally() + "'" +
            ", logoUrl='" + getLogoUrl() + "'" +
            ", description='" + getDescription() + "'" +
            ", userPoint=" + getUserPoint() +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", status='" + getStatus() + "'" +
            ", isUsePoint='" + isIsUsePoint() + "'" +
            ", isScreen='" + isIsScreen() + "'" +
            ", isCoaching='" + isIsCoaching() + "'" +
            ", isWellMatric='" + isIsWellMatric() + "'" +
            ", isHP='" + isIsHp() + "'" +
            ", isUseLevel='" + isIsUseLevel() + "'" +
            ", isTemplate='" + isIsTemplate() + "'" +
            ", programType='" + getProgramType() + "'" +
            "}";
    }

    private List<ProgramCategoryPointDTO> programCategoryPoints = new ArrayList<>();
    private List<ProgramLevelDTO> programLevels = new ArrayList<>();
    private List<ProgramActivityDTO> programActivities = new ArrayList<>();



    public List<ProgramCategoryPointDTO> getProgramCategoryPoints() {
        return programCategoryPoints;
    }
    public void setProgramCategoryPoints(List<ProgramCategoryPointDTO> programCategoryPoints) {
        this.programCategoryPoints = programCategoryPoints;
    }

    public List<ProgramLevelDTO> getProgramLevels() {
        return programLevels;
    }
    public void setProgramLevels(List<ProgramLevelDTO> programLevels) {
        this.programLevels = programLevels;
    }

    public List<ProgramActivityDTO> getProgramActivities() {
        return programActivities;
    }
    public void setProgramActivities(List<ProgramActivityDTO> programActivities) {
        this.programActivities = programActivities;
    }


    private Boolean isHp;
    private Boolean isWellMatric;
    private Boolean isCoaching;

    public Boolean isIsHp() {
        return isHp;
    }

    public void setIsHp(Boolean isHp) {
        this.isHp = isHp;
    }


    public Boolean isIsWellMatric() {
        return isWellMatric;
    }

    public void setIsWellMatric(Boolean isWellMatric) {
        this.isWellMatric = isWellMatric;
    }

    public Boolean isIsCoaching() {
        return isCoaching;
    }

    public void setIsCoaching(Boolean isCoaching) {
        this.isCoaching = isCoaching;
    }


    public List<ClientProgramDTO> getClientPrograms() {
        return clientPrograms;
    }

    public void setClientPrograms(List<ClientProgramDTO> clientPrograms) {
        this.clientPrograms = clientPrograms;
    }

    private List<ClientProgramDTO> clientPrograms = new ArrayList<>();

    public Integer getProgramLength() {
        return programLength;
    }

    public void setProgramLength(Integer programLength) {
        this.programLength = programLength;
    }


    public List<ProgramSubgroupDTO> getProgramSubgroups() {
        return programSubgroups;
    }

    public void setProgramSubgroups(List<ProgramSubgroupDTO> programSubgroups) {
        this.programSubgroups = programSubgroups;
    }

    private List<ProgramSubgroupDTO> programSubgroups = new ArrayList<>();

    private List<ProgramHealthyRangePayLoad> programHealthyRanges = new ArrayList<>();

    public List<ProgramHealthyRangePayLoad> getProgramHealthyRanges() {
        return programHealthyRanges;
    }

    public void setProgramHealthyRanges(List<ProgramHealthyRangePayLoad> programHealthyRanges) {
        this.programHealthyRanges = programHealthyRanges;
    }

    public boolean getQaVerify() {
        return qaVerify;
    }

    public void setQaVerify(boolean qaVerify) {
        this.qaVerify = qaVerify;
    }

    public ProgramDTO setStatusBeforeSendingIfProgramIsQAVerify() {
        // set status is QA Verify if is_program_qa_verify is true
        if(getQaVerify()) {
            setStatus(ProgramStatus.QA_Verify.toString());
        }
        return this;
    }

    public Instant getProgramQAStartDate() { return programQAStartDate; }

    public void setProgramQAStartDate(Instant programQAStartDate) {
        this.programQAStartDate = programQAStartDate;
    }

    public Instant getProgramQAEndDate() { return programQAEndDate; }

    public void setProgramQAEndDate(Instant programQAEndDate) { this.programQAEndDate = programQAEndDate; }

    public List<DemoGraphicDto> getDemographics() {
        return demographics;
    }

    public void setDemographics(List<DemoGraphicDto> demographics) {
        this.demographics = demographics;
    }

    public BigDecimal getEconomyPoint() {
        return economyPoint;
    }

    public void setEconomyPoint(BigDecimal economyPoint) {
        this.economyPoint = economyPoint;
    }

    public Boolean getExtractable() {
        return isExtractable;
    }

    public void setExtractable(Boolean extractable) {
        isExtractable = extractable;
    }

    public Boolean isExtractable() {
        return isExtractable;
    }

    public ProgramDTO isExtractable(Boolean isExtractable) {
        this.isExtractable = isExtractable;
        return this;
    }

    public String getHpsfFormType() {
        return hpsfFormType;
    }

    public void setHpsfFormType(String hpsfFormType) {
        this.hpsfFormType = hpsfFormType;
    }
    public void setProgramType(ProgramType programType) {
        this.programType = programType;
    }

    public ProgramType getProgramType() {
        return programType;
    }

    @JsonIgnore
    List<ProgramCollectionDTO> programCollectionList;

    @JsonIgnore
    List<ProgramCohortExtensionDto> programCohorts;

    public List<ProgramCollectionDTO> getProgramCollectionList() {
        return programCollectionList;
    }

    public void setProgramCollectionList(List<ProgramCollectionDTO> programCollectionList) {
        this.programCollectionList = programCollectionList;
    }

    public List<ProgramCohortExtensionDto> getProgramCohorts() {
        return programCohorts;
    }

    public void setProgramCohorts(List<ProgramCohortExtensionDto> programCohorts) {
        this.programCohorts = programCohorts;
    }

    private Instant activeDate;

    public Instant getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(Instant activeDate) {
        this.activeDate = activeDate;
    }

    private boolean isIsOutcomesLite;

    public boolean isIsOutcomesLite() {
        return isIsOutcomesLite;
    }

    public void setIsOutcomesLite(boolean isOutcomesLite) {
        this.isIsOutcomesLite = isOutcomesLite;
    }

    public String getWellmetricSchedulerV1Url() {
        return wellmetricSchedulerV1Url;
    }

    public void setWellmetricSchedulerV1Url(String wellmetricSchedulerV1Url) {
        this.wellmetricSchedulerV1Url = wellmetricSchedulerV1Url;
    }
}
