package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ClientSetting} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ClientSettingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /client-settings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ClientSettingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter clientId;

    private StringFilter clientName;

    private StringFilter clientEmail;

    private StringFilter tangoAccountIdentifier;

    private StringFilter tangoCustomerIdentifier;

    private BigDecimalFilter tangoCurrentBalance;

    private BigDecimalFilter tangoThresholdBalance;

    private StringFilter tangoCreditToken;

    private InstantFilter updatedDate;

    private StringFilter updatedBy;

    public ClientSettingCriteria(){
    }

    public ClientSettingCriteria(ClientSettingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.clientId = other.clientId == null ? null : other.clientId.copy();
        this.clientName = other.clientName == null ? null : other.clientName.copy();
        this.clientEmail = other.clientEmail == null ? null : other.clientEmail.copy();
        this.tangoAccountIdentifier = other.tangoAccountIdentifier == null ? null : other.tangoAccountIdentifier.copy();
        this.tangoCustomerIdentifier = other.tangoCustomerIdentifier == null ? null : other.tangoCustomerIdentifier.copy();
        this.tangoCurrentBalance = other.tangoCurrentBalance == null ? null : other.tangoCurrentBalance.copy();
        this.tangoThresholdBalance = other.tangoThresholdBalance == null ? null : other.tangoThresholdBalance.copy();
        this.tangoCreditToken = other.tangoCreditToken == null ? null : other.tangoCreditToken.copy();
        this.updatedDate = other.updatedDate == null ? null : other.updatedDate.copy();
        this.updatedBy = other.updatedBy == null ? null : other.updatedBy.copy();
    }

    @Override
    public ClientSettingCriteria copy() {
        return new ClientSettingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getClientId() {
        return clientId;
    }

    public void setClientId(StringFilter clientId) {
        this.clientId = clientId;
    }

    public StringFilter getClientName() {
        return clientName;
    }

    public void setClientName(StringFilter clientName) {
        this.clientName = clientName;
    }

    public StringFilter getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(StringFilter clientEmail) {
        this.clientEmail = clientEmail;
    }

    public StringFilter getTangoAccountIdentifier() {
        return tangoAccountIdentifier;
    }

    public void setTangoAccountIdentifier(StringFilter tangoAccountIdentifier) {
        this.tangoAccountIdentifier = tangoAccountIdentifier;
    }

    public StringFilter getTangoCustomerIdentifier() {
        return tangoCustomerIdentifier;
    }

    public void setTangoCustomerIdentifier(StringFilter tangoCustomerIdentifier) {
        this.tangoCustomerIdentifier = tangoCustomerIdentifier;
    }

    public BigDecimalFilter getTangoCurrentBalance() {
        return tangoCurrentBalance;
    }

    public void setTangoCurrentBalance(BigDecimalFilter tangoCurrentBalance) {
        this.tangoCurrentBalance = tangoCurrentBalance;
    }

    public BigDecimalFilter getTangoThresholdBalance() {
        return tangoThresholdBalance;
    }

    public void setTangoThresholdBalance(BigDecimalFilter tangoThresholdBalance) {
        this.tangoThresholdBalance = tangoThresholdBalance;
    }

    public StringFilter getTangoCreditToken() {
        return tangoCreditToken;
    }

    public void setTangoCreditToken(StringFilter tangoCreditToken) {
        this.tangoCreditToken = tangoCreditToken;
    }

    public InstantFilter getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(InstantFilter updatedDate) {
        this.updatedDate = updatedDate;
    }

    public StringFilter getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(StringFilter updatedBy) {
        this.updatedBy = updatedBy;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ClientSettingCriteria that = (ClientSettingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(clientId, that.clientId) &&
            Objects.equals(clientName, that.clientName) &&
            Objects.equals(clientEmail, that.clientEmail) &&
            Objects.equals(tangoAccountIdentifier, that.tangoAccountIdentifier) &&
            Objects.equals(tangoCustomerIdentifier, that.tangoCustomerIdentifier) &&
            Objects.equals(tangoCurrentBalance, that.tangoCurrentBalance) &&
            Objects.equals(tangoThresholdBalance, that.tangoThresholdBalance) &&
            Objects.equals(tangoCreditToken, that.tangoCreditToken) &&
            Objects.equals(updatedDate, that.updatedDate) &&
            Objects.equals(updatedBy, that.updatedBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        clientId,
        clientName,
        clientEmail,
        tangoAccountIdentifier,
        tangoCustomerIdentifier,
        tangoCurrentBalance,
        tangoThresholdBalance,
        tangoCreditToken,
        updatedDate,
        updatedBy
        );
    }

    @Override
    public String toString() {
        return "ClientSettingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (clientId != null ? "clientId=" + clientId + ", " : "") +
                (clientName != null ? "clientName=" + clientName + ", " : "") +
                (clientEmail != null ? "clientEmail=" + clientEmail + ", " : "") +
                (tangoAccountIdentifier != null ? "tangoAccountIdentifier=" + tangoAccountIdentifier + ", " : "") +
                (tangoCustomerIdentifier != null ? "tangoCustomerIdentifier=" + tangoCustomerIdentifier + ", " : "") +
                (tangoCurrentBalance != null ? "tangoCurrentBalance=" + tangoCurrentBalance + ", " : "") +
                (tangoThresholdBalance != null ? "tangoThresholdBalance=" + tangoThresholdBalance + ", " : "") +
                (tangoCreditToken != null ? "tangoCreditToken=" + tangoCreditToken + ", " : "") +
                (updatedDate != null ? "updatedDate=" + updatedDate + ", " : "") +
                (updatedBy != null ? "updatedBy=" + updatedBy + ", " : "") +
            "}";
    }

}
