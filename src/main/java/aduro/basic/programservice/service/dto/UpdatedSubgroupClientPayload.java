package aduro.basic.programservice.service.dto;

import javax.validation.constraints.NotNull;
import java.util.List;


public class UpdatedSubgroupClientPayload{
    @NotNull
    private String clientId;

    private List<ProgramSubgroupDTO> addNewSubgroups;

    private List<ProgramSubgroupDTO> deleteSubgroups;

    private String message;

    public UpdatedSubgroupClientPayload() {

    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProgramSubgroupDTO> getAddNewSubgroups() {
        return addNewSubgroups;
    }

    public void setAddNewSubgroups(List<ProgramSubgroupDTO> addNewSubgroups) {
        this.addNewSubgroups = addNewSubgroups;
    }

    public List<ProgramSubgroupDTO> getDeleteSubgroups() {
        return deleteSubgroups;
    }

    public void setDeleteSubgroups(List<ProgramSubgroupDTO> deleteSubgroups) {
        this.deleteSubgroups = deleteSubgroups;
    }
}
