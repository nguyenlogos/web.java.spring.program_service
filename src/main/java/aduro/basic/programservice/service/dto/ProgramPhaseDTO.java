package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.ap.dto.CustomActivityResultDto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramPhase} entity.
 */
public class ProgramPhaseDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer phaseOrder;

    private Instant startDate;

    private Instant endDate;


    private Long programId;

    private String programName;

    private List<CustomActivityResultDto> customActivities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPhaseOrder() {
        return phaseOrder;
    }

    public void setPhaseOrder(Integer phaseOrder) {
        this.phaseOrder = phaseOrder;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public List<CustomActivityResultDto> getCustomActivities() {
        return customActivities;
    }

    public void setCustomActivities(List<CustomActivityResultDto> customActivities) {
        this.customActivities = customActivities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramPhaseDTO programPhaseDTO = (ProgramPhaseDTO) o;
        if (programPhaseDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programPhaseDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramPhaseDTO{" +
            "id=" + getId() +
            ", phaseOrder=" + getPhaseOrder() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", program=" + getProgramId() +
            ", program='" + getProgramName() + "'" +
            "}";
    }
}
