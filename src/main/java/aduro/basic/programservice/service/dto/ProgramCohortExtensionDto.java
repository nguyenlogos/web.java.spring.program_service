package aduro.basic.programservice.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.List;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProgramCohortExtensionDto {

    private Long id;

    @NotNull
    @Size(max = 150)
    private String cohortName;

    private Instant createdDate;

    private Instant modifiedDate;

    @Size(max = 250)
    private String cohortDescription;

    @NotNull
    private Long programId;

    private List<ProgramCohortRuleDTO> programCohortRules;

    private List<ProgramCohortCollectionDTO> programCohortCollections;

    private Boolean  isDefault;

    private Boolean isNotEditable;

}
