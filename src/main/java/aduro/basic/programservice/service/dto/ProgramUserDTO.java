package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramUser} entity.
 */
public class ProgramUserDTO implements Serializable {

    private Long id;

    @NotNull
    private String participantId;

    @NotNull
    private String clientId;

    private BigDecimal totalUserPoint;

    @NotNull
    private Long programId;

    private Integer currentLevel;

    private String email;

    private String firstName;

    private String lastName;

    private String phone;

    private String clientName;

    private String subgroupId;

    private String subgroupName;

    private Boolean isTobaccoUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getTotalUserPoint() {
        return totalUserPoint;
    }

    public void setTotalUserPoint(BigDecimal totalUserPoint) {
        this.totalUserPoint = totalUserPoint;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Integer getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(Integer currentLevel) {
        this.currentLevel = currentLevel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public Boolean isIsTobaccoUser() {
        return isTobaccoUser;
    }

    public void setIsTobaccoUser(Boolean isTobaccoUser) {
        this.isTobaccoUser = isTobaccoUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramUserDTO programUserDTO = (ProgramUserDTO) o;
        if (programUserDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programUserDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramUserDTO{" +
            "id=" + getId() +
            ", participantId='" + getParticipantId() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", totalUserPoint=" + getTotalUserPoint() +
            ", programId=" + getProgramId() +
            ", currentLevel=" + getCurrentLevel() +
            ", email='" + getEmail() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", phone='" + getPhone() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", isTobaccoUser='" + isIsTobaccoUser() + "'" +
            "}";
    }

    public List<UserLevelDTO> getUserlevels() {
        return userlevels;
    }

    public void setUserlevels(List<UserLevelDTO> userlevels) {
        this.userlevels = userlevels;
    }

    private List<UserLevelDTO> userlevels = new ArrayList<>();

    public List<UserEventDTO> getUserEvents() {
        return userEvents;
    }

    public void setUserEvents(List<UserEventDTO> userEvents) {
        this.userEvents = userEvents;
    }

    private List<UserEventDTO> userEvents = new ArrayList<>();


}
