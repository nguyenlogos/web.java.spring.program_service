package aduro.basic.programservice.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JoinProgramRequest {
    @NotNull
    private String clientId;
    @NotNull
    private String participantId;
    private String subgroupId;
    private String subgroupName;
    private String email;
    private String firstName;
    private String lastName;
    private Boolean asProgramTester;
    private Integer userId;

    public boolean isAsProgramTester(){
        return asProgramTester != null ? asProgramTester: false;
    }
}
