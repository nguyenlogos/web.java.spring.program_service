package aduro.basic.programservice.service.dto;

import lombok.*;

import java.math.BigDecimal;
import java.time.Instant;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScProgramLevelRequiredItemDto {
    private String title;
    private String itemId;
    private String type;
}
