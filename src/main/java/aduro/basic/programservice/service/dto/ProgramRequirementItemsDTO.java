package aduro.basic.programservice.service.dto;
import java.math.BigDecimal;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramRequirementItems} entity.
 */
public class ProgramRequirementItemsDTO implements Serializable {

    private Long id;

    @NotNull
    private String itemId;

    private String nameOfItem;

    @NotNull
    private String typeOfItem;

    @NotNull
    private String itemCode;

    private String subgroupId;

    private String subgroupName;

    private Instant createdAt;

    private Long programLevelId;

    private Integer programLevelLevelOrder;

    private Instant completedDate;

    private BigDecimal point;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getNameOfItem() {
        return nameOfItem;
    }

    public void setNameOfItem(String nameOfItem) {
        this.nameOfItem = nameOfItem;
    }

    public String getTypeOfItem() {
        return typeOfItem;
    }

    public void setTypeOfItem(String typeOfItem) {
        this.typeOfItem = typeOfItem;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Long getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(Long programLevelId) {
        this.programLevelId = programLevelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramRequirementItemsDTO programRequirementItemsDTO = (ProgramRequirementItemsDTO) o;
        if (programRequirementItemsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programRequirementItemsDTO.getId());
    }

    public Integer getProgramLevelLevelOrder() {
        return programLevelLevelOrder;
    }

    public void setProgramLevelLevelOrder(Integer programLevelLevelOrder) {
        this.programLevelLevelOrder = programLevelLevelOrder;
    }

    public Instant getCompletedDate() {
        return completedDate;
    }

    public void setCompletedDate(Instant completedDate) {
        this.completedDate = completedDate;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramRequirementItemsDTO{" +
            "id=" + getId() +
            ", itemId='" + getItemId() + "'" +
            ", nameOfItem='" + getNameOfItem() + "'" +
            ", typeOfItem='" + getTypeOfItem() + "'" +
            ", itemCode='" + getItemCode() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", programLevel=" + getProgramLevelId() +
            "}";
    }
}
