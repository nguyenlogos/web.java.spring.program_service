package aduro.basic.programservice.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ClientSetting} entity.
 */
public class ClientSettingDTO implements Serializable {

    private Long id;

    @NotNull
    private String clientId;

    @NotNull
    private String clientName;

    private String clientEmail;

    private String tangoAccountIdentifier;

    public String getTangoAccountName() {
        return tangoAccountName;
    }

    public void setTangoAccountName(String tangoAccountName) {
        this.tangoAccountName = tangoAccountName;
    }

    private String tangoAccountName;

    private String tangoCustomerIdentifier;

    private BigDecimal tangoCurrentBalance;

    private BigDecimal tangoThresholdBalance;

    private String tangoCreditToken;


    private Instant updatedDate;

    @NotNull
    private String updatedBy;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getTangoAccountIdentifier() {
        return tangoAccountIdentifier;
    }

    public void setTangoAccountIdentifier(String tangoAccountIdentifier) {
        this.tangoAccountIdentifier = tangoAccountIdentifier;
    }

    public String getTangoCustomerIdentifier() {
        return tangoCustomerIdentifier;
    }

    public void setTangoCustomerIdentifier(String tangoCustomerIdentifier) {
        this.tangoCustomerIdentifier = tangoCustomerIdentifier;
    }

    public BigDecimal getTangoCurrentBalance() {
        return tangoCurrentBalance;
    }

    public void setTangoCurrentBalance(BigDecimal tangoCurrentBalance) {
        this.tangoCurrentBalance = tangoCurrentBalance;
    }

    public BigDecimal getTangoThresholdBalance() {
        return tangoThresholdBalance;
    }

    public void setTangoThresholdBalance(BigDecimal tangoThresholdBalance) {
        this.tangoThresholdBalance = tangoThresholdBalance;
    }

    public String getTangoCreditToken() {
        return tangoCreditToken;
    }

    public void setTangoCreditToken(String tangoCreditToken) {
        this.tangoCreditToken = tangoCreditToken;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ClientSettingDTO clientSettingDTO = (ClientSettingDTO) o;
        if (clientSettingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), clientSettingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ClientSettingDTO{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", clientEmail='" + getClientEmail() + "'" +
            ", tangoAccountIdentifier='" + getTangoAccountIdentifier() + "'" +
            ", tangoCustomerIdentifier='" + getTangoCustomerIdentifier() + "'" +
            ", tangoCurrentBalance=" + getTangoCurrentBalance() +
            ", tangoThresholdBalance=" + getTangoThresholdBalance() +
            ", tangoCreditToken='" + getTangoCreditToken() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
