package aduro.basic.programservice.service.dto;
import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramIntegrationTracking} entity.
 */
public class ProgramIntegrationTrackingDTO implements Serializable {

    private Long id;

    private String endPoint;

    private String payload;

    private Instant createdAt;

    private String errorMessage;

    private String status;

    private String serviceType;

    private Instant attemptedAt;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Instant getAttemptedAt() {
        return attemptedAt;
    }

    public void setAttemptedAt(Instant attemptedAt) {
        this.attemptedAt = attemptedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramIntegrationTrackingDTO programIntegrationTrackingDTO = (ProgramIntegrationTrackingDTO) o;
        if (programIntegrationTrackingDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programIntegrationTrackingDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramIntegrationTrackingDTO{" +
            "id=" + getId() +
            ", endPoint='" + getEndPoint() + "'" +
            ", payload='" + getPayload() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", status='" + getStatus() + "'" +
            ", serviceType='" + getServiceType() + "'" +
            ", attemptedAt='" + getAttemptedAt() + "'" +
            "}";
    }
}
