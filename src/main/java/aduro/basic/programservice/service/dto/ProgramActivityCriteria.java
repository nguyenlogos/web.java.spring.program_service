package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramActivity} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramActivityResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-activities?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramActivityCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter activityId;

    private StringFilter activityCode;

    private BooleanFilter isCustomized;

    private StringFilter masterId;

    private LongFilter programId;

    private LongFilter programPhaseId;

    public ProgramActivityCriteria(){
    }

    public ProgramActivityCriteria(ProgramActivityCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.activityId = other.activityId == null ? null : other.activityId.copy();
        this.activityCode = other.activityCode == null ? null : other.activityCode.copy();
        this.isCustomized = other.isCustomized == null ? null : other.isCustomized.copy();
        this.masterId = other.masterId == null ? null : other.masterId.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.programPhaseId = other.programPhaseId == null ? null : other.programPhaseId.copy();
    }

    @Override
    public ProgramActivityCriteria copy() {
        return new ProgramActivityCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getActivityId() {
        return activityId;
    }

    public void setActivityId(StringFilter activityId) {
        this.activityId = activityId;
    }

    public StringFilter getActivityCode() {
        return activityCode;
    }

    public void setActivityCode(StringFilter activityCode) {
        this.activityCode = activityCode;
    }

    public BooleanFilter getIsCustomized() {
        return isCustomized;
    }

    public void setIsCustomized(BooleanFilter isCustomized) {
        this.isCustomized = isCustomized;
    }

    public StringFilter getMasterId() {
        return masterId;
    }

    public void setMasterId(StringFilter masterId) {
        this.masterId = masterId;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public LongFilter getProgramPhaseId() {
        return programPhaseId;
    }

    public void setProgramPhaseId(LongFilter programPhaseId) {
        this.programPhaseId = programPhaseId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramActivityCriteria that = (ProgramActivityCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(activityId, that.activityId) &&
            Objects.equals(activityCode, that.activityCode) &&
            Objects.equals(isCustomized, that.isCustomized) &&
            Objects.equals(masterId, that.masterId) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(programPhaseId, that.programPhaseId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        activityId,
        activityCode,
        isCustomized,
        masterId,
        programId,
        programPhaseId
        );
    }

    @Override
    public String toString() {
        return "ProgramActivityCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (activityId != null ? "activityId=" + activityId + ", " : "") +
                (activityCode != null ? "activityCode=" + activityCode + ", " : "") +
                (isCustomized != null ? "isCustomized=" + isCustomized + ", " : "") +
                (masterId != null ? "masterId=" + masterId + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (programPhaseId != null ? "programPhaseId=" + programPhaseId + ", " : "") +
            "}";
    }

}
