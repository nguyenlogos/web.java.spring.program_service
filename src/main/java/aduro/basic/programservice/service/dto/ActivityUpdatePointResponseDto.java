package aduro.basic.programservice.service.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ActivityUpdatePointResponseDto {

    List<UserTrackingDataDto> userTrackingDataList;

    List<DataProgressInDurationDto> progressInRanges;

    String activityName;

    String trackingType;

    BigDecimal totalPotentialPoint;
}
