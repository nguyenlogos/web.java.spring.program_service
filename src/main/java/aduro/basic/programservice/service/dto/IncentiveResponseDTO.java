package aduro.basic.programservice.service.dto;

import java.util.List;

public class IncentiveResponseDTO {
    private Integer currentLevel;
    private Integer totalLevel;
    private List<UserRewardDTO> userRewards;

    public IncentiveResponseDTO(Integer currentLevel, Integer totalLevel, List<UserRewardDTO> userRewards) {
        this.currentLevel = currentLevel;
        this.totalLevel = totalLevel;
        this.userRewards = userRewards;
    }

    public Integer getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(Integer currentLevel) {
        this.currentLevel = currentLevel;
    }

    public Integer getTotalLevel() {
        return totalLevel;
    }

    public void setTotalLevel(Integer totalLevel) {
        this.totalLevel = totalLevel;
    }

    public List<UserRewardDTO> getUserRewards() {
        return userRewards;
    }

    public void setUserRewards(List<UserRewardDTO> userRewards) {
        this.userRewards = userRewards;
    }
}
