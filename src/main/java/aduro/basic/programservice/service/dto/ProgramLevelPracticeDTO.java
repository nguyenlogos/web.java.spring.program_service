package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramLevelPractice} entity.
 */
public class ProgramLevelPracticeDTO implements Serializable {

    private Long id;

    @NotNull
    private String practiceId;

    @NotNull
    private String name;

    private String subgroupId;

    private String subgroupName;


    private Long programLevelId;

    private String programLevelName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPracticeId() {
        return practiceId;
    }

    public void setPracticeId(String practiceId) {
        this.practiceId = practiceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public Long getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(Long programLevelId) {
        this.programLevelId = programLevelId;
    }

    public String getProgramLevelName() {
        return programLevelName;
    }

    public void setProgramLevelName(String programLevelName) {
        this.programLevelName = programLevelName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramLevelPracticeDTO programLevelPracticeDTO = (ProgramLevelPracticeDTO) o;
        if (programLevelPracticeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programLevelPracticeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramLevelPracticeDTO{" +
            "id=" + getId() +
            ", practiceId='" + getPracticeId() + "'" +
            ", name='" + getName() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", programLevel=" + getProgramLevelId() +
            ", programLevel='" + getProgramLevelName() + "'" +
            "}";
    }
}
