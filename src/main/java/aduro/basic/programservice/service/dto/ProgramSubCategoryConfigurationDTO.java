package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramSubCategoryConfiguration} entity.
 */
public class ProgramSubCategoryConfigurationDTO implements Serializable {

    private Long id;

    @NotNull
    private String subCategoryCode;

    private Boolean isBloodPressureSingleTest;

    private Boolean isBloodPressureIndividualTest;

    private Boolean isGlucoseAwardedInRange;

    private Boolean isBmiAwardedInRange;

    private Boolean isBmiAwardedFasting;

    private Boolean isBmiAwardedNonFasting;

    @NotNull
    private Long programId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubCategoryCode() {
        return subCategoryCode;
    }

    public void setSubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public Boolean isIsBloodPressureSingleTest() {
        return isBloodPressureSingleTest;
    }

    public void setIsBloodPressureSingleTest(Boolean isBloodPressureSingleTest) {
        this.isBloodPressureSingleTest = isBloodPressureSingleTest;
    }

    public Boolean isIsBloodPressureIndividualTest() {
        return isBloodPressureIndividualTest;
    }

    public void setIsBloodPressureIndividualTest(Boolean isBloodPressureIndividualTest) {
        this.isBloodPressureIndividualTest = isBloodPressureIndividualTest;
    }

    public Boolean isIsGlucoseAwardedInRange() {
        return isGlucoseAwardedInRange;
    }

    public void setIsGlucoseAwardedInRange(Boolean isGlucoseAwardedInRange) {
        this.isGlucoseAwardedInRange = isGlucoseAwardedInRange;
    }

    public Boolean isIsBmiAwardedInRange() {
        return isBmiAwardedInRange;
    }

    public void setIsBmiAwardedInRange(Boolean isBmiAwardedInRange) {
        this.isBmiAwardedInRange = isBmiAwardedInRange;
    }

    public Boolean isIsBmiAwardedFasting() {
        return isBmiAwardedFasting;
    }

    public void setIsBmiAwardedFasting(Boolean isBmiAwardedFasting) {
        this.isBmiAwardedFasting = isBmiAwardedFasting;
    }

    public Boolean isIsBmiAwardedNonFasting() {
        return isBmiAwardedNonFasting;
    }

    public void setIsBmiAwardedNonFasting(Boolean isBmiAwardedNonFasting) {
        this.isBmiAwardedNonFasting = isBmiAwardedNonFasting;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO = (ProgramSubCategoryConfigurationDTO) o;
        if (programSubCategoryConfigurationDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programSubCategoryConfigurationDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramSubCategoryConfigurationDTO{" +
            "id=" + getId() +
            ", subCategoryCode='" + getSubCategoryCode() + "'" +
            ", isBloodPressureSingleTest='" + isIsBloodPressureSingleTest() + "'" +
            ", isBloodPressureIndividualTest='" + isIsBloodPressureIndividualTest() + "'" +
            ", isGlucoseAwardedInRange='" + isIsGlucoseAwardedInRange() + "'" +
            ", isBmiAwardedInRange='" + isIsBmiAwardedInRange() + "'" +
            ", isBmiAwardedFasting='" + isIsBmiAwardedFasting() + "'" +
            ", isBmiAwardedNonFasting='" + isIsBmiAwardedNonFasting() + "'" +
            ", programId=" + getProgramId() +
            "}";
    }
}
