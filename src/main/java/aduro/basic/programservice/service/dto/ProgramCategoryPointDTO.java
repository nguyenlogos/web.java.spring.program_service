package aduro.basic.programservice.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramCategoryPoint} entity.
 */
public class ProgramCategoryPointDTO implements Serializable {

    private Long id;

    @NotNull
    private String categoryCode;

    private String categoryName;

    private BigDecimal percentPoint;

    private BigDecimal valuePoint;


    private Long programId;

    private String programName;

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    private Boolean locked;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public BigDecimal getPercentPoint() {
        return percentPoint;
    }

    public void setPercentPoint(BigDecimal percentPoint) {
        this.percentPoint = percentPoint;
    }

    public BigDecimal getValuePoint() {
        return valuePoint;
    }

    public void setValuePoint(BigDecimal valuePoint) {
        this.valuePoint = valuePoint;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramCategoryPointDTO programCategoryPointDTO = (ProgramCategoryPointDTO) o;
        if (programCategoryPointDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programCategoryPointDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramCategoryPointDTO{" +
            "id=" + getId() +
            ", categoryCode='" + getCategoryCode() + "'" +
            ", categoryName='" + getCategoryName() + "'" +
            ", percentPoint=" + getPercentPoint() +
            ", valuePoint=" + getValuePoint() +
            ", program=" + getProgramId() +
            ", program='" + getProgramName() + "'" +
            "}";
    }

    private List<ProgramSubCategoryPointDTO> programSubCategoryPoints = new ArrayList<>();
    public List<ProgramSubCategoryPointDTO> getProgramSubCategoryPoints() {
        return programSubCategoryPoints;
    }
    public void setProgramSubCategoryPoints(List<ProgramSubCategoryPointDTO> programCategoryPoints) {
        this.programSubCategoryPoints = programCategoryPoints;
    }


}
