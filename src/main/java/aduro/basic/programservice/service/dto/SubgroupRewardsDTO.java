package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.List;

public class SubgroupRewardsDTO implements Serializable {
    public Boolean getApplyRewardAllSubgroup() {
        return applyRewardAllSubgroup;
    }

    public void setApplyRewardAllSubgroup(Boolean applyRewardAllSubgroup) {
        this.applyRewardAllSubgroup = applyRewardAllSubgroup;
    }

    public List<ProgramLevelRewardDTO> getProgramLevelRewards() {
        return programLevelRewards;
    }

    public void setProgramLevelRewards(List<ProgramLevelRewardDTO> programLevelRewards) {
        this.programLevelRewards = programLevelRewards;
    }

    private Boolean applyRewardAllSubgroup;
    private List<ProgramLevelRewardDTO> programLevelRewards;
}
