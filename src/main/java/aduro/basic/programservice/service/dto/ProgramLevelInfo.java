package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class ProgramLevelInfo implements Serializable {

    private List<ProgramLevelDTO> programLevels;

    public List<ProgramLevelDTO> getProgramLevels() {
        return programLevels;
    }

    public void setProgramLevelDTOs(List<ProgramLevelDTO> programLevels) {
        this.programLevels = programLevels;
    }


    public Boolean getIsUsePoint() {
        return isUsePoint;
    }

    public void seIstUsePoint(Boolean usePoint) {
        isUsePoint = usePoint;
    }

    public BigDecimal getUserPoint() {
        return userPoint;
    }

    public void setUserPoint(BigDecimal userPoint) {
        this.userPoint = userPoint;
    }

    public String getLevelStructure() {
        return levelStructure;
    }

    public void setLevelStructure(String levelStructure) {
        this.levelStructure = levelStructure;
    }

    private String levelStructure ;

    private Boolean isUsePoint;
    private BigDecimal userPoint;

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    private String lastModifiedBy;





}
