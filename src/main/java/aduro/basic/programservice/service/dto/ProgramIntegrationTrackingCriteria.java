package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramIntegrationTracking} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramIntegrationTrackingResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-integration-trackings?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramIntegrationTrackingCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter endPoint;

    private StringFilter payload;

    private InstantFilter createdAt;

    private StringFilter errorMessage;

    private StringFilter status;

    private StringFilter serviceType;

    private InstantFilter attemptedAt;

    public ProgramIntegrationTrackingCriteria(){
    }

    public ProgramIntegrationTrackingCriteria(ProgramIntegrationTrackingCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.endPoint = other.endPoint == null ? null : other.endPoint.copy();
        this.payload = other.payload == null ? null : other.payload.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.errorMessage = other.errorMessage == null ? null : other.errorMessage.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.serviceType = other.serviceType == null ? null : other.serviceType.copy();
        this.attemptedAt = other.attemptedAt == null ? null : other.attemptedAt.copy();
    }

    @Override
    public ProgramIntegrationTrackingCriteria copy() {
        return new ProgramIntegrationTrackingCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(StringFilter endPoint) {
        this.endPoint = endPoint;
    }

    public StringFilter getPayload() {
        return payload;
    }

    public void setPayload(StringFilter payload) {
        this.payload = payload;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public StringFilter getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(StringFilter errorMessage) {
        this.errorMessage = errorMessage;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getServiceType() {
        return serviceType;
    }

    public void setServiceType(StringFilter serviceType) {
        this.serviceType = serviceType;
    }

    public InstantFilter getAttemptedAt() {
        return attemptedAt;
    }

    public void setAttemptedAt(InstantFilter attemptedAt) {
        this.attemptedAt = attemptedAt;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramIntegrationTrackingCriteria that = (ProgramIntegrationTrackingCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(endPoint, that.endPoint) &&
            Objects.equals(payload, that.payload) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(errorMessage, that.errorMessage) &&
            Objects.equals(status, that.status) &&
            Objects.equals(serviceType, that.serviceType) &&
            Objects.equals(attemptedAt, that.attemptedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        endPoint,
        payload,
        createdAt,
        errorMessage,
        status,
        serviceType,
        attemptedAt
        );
    }

    @Override
    public String toString() {
        return "ProgramIntegrationTrackingCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (endPoint != null ? "endPoint=" + endPoint + ", " : "") +
                (payload != null ? "payload=" + payload + ", " : "") +
                (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
                (errorMessage != null ? "errorMessage=" + errorMessage + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (serviceType != null ? "serviceType=" + serviceType + ", " : "") +
                (attemptedAt != null ? "attemptedAt=" + attemptedAt + ", " : "") +
            "}";
    }

}
