package aduro.basic.programservice.service.dto;


import javax.validation.constraints.NotNull;

public class IncentiveRequestBody {
    @NotNull
    private String participantId;
    @NotNull
    private String clientId;

    private Boolean asProgramTester;

    public boolean isAsProgramTester(){
        return asProgramTester != null ? asProgramTester: false;
    }

    @NotNull
    private String eventId;

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
