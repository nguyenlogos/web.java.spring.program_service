package aduro.basic.programservice.service.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProcessIncentiveActivityDto {
    @NotNull
    String participantId;
    @NotNull
    String clientId;
    @NotNull
    Long programId;

    @NotNull
    String activityId;

    @NotNull
    Instant eventDate;

    @NotNull
    String createdBy;

    DataProgressInDurationDto dataProgress;

}
