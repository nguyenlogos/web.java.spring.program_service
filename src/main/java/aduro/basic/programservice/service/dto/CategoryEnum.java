package aduro.basic.programservice.service.dto;

public enum CategoryEnum {
    Activities {
        @Override
        public String toString() {
            return "ACTIVITIES";
        }
    },
    Assesments  {
        @Override
        public String toString() {
            return "ASSESSMENTS ";
        }
    },
    Bonus {
        @Override
        public String toString() {
            return "BONUS";
        }
    },

    HPInteractions {
        @Override
        public String toString() {
            return "HPINTERACTIONS";
        }
    },

    HPContent {
        @Override
        public String toString() {
            return "HPCONTENT";
        }
    },

    WellMetrics {
        @Override
        public String toString() {
            return "WELLMETRICS";
        }
    },


}
