package aduro.basic.programservice.service.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserCohortUpdatedEventDto implements Serializable {

    private Long programId;
    private String aduroId;

    private Instant timestamp;
}
