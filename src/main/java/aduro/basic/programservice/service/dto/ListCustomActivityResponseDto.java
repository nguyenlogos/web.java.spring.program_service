package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ListCustomActivityResponseDto {

    @JsonProperty("models")
    private List<CustomActivityDTO> models;

    @JsonProperty("rowCount")
    private Integer totalCount;

    @JsonProperty("limit")
    private Integer limit;

    @JsonProperty("offset")
    private Integer offset;

    public ListCustomActivityResponseDto() {
    }

    public List<CustomActivityDTO> getModels() {
        return models;
    }

    public void setModels(List<CustomActivityDTO> models) {
        this.models = models;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
