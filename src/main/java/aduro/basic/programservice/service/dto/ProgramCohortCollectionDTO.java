package aduro.basic.programservice.service.dto;
import java.time.Instant;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramCohortCollection} entity.
 */
public class ProgramCohortCollectionDTO implements Serializable {

    private Long id;

    private Integer requiredCompletion;

    private Integer requiredLevel;

    private Instant createdDate;

    private Instant modifiedDate;

    private String programCollectionName;

    private Long programCollectionId;

    private Long programCohortId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRequiredCompletion() {
        return requiredCompletion;
    }

    public void setRequiredCompletion(Integer requiredCompletion) {
        this.requiredCompletion = requiredCompletion;
    }

    public Integer getRequiredLevel() {
        return requiredLevel;
    }

    public void setRequiredLevel(Integer requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getProgramCollectionId() {
        return programCollectionId;
    }

    public void setProgramCollectionId(Long programCollectionId) {
        this.programCollectionId = programCollectionId;
    }

    public Long getProgramCohortId() {
        return programCohortId;
    }

    public void setProgramCohortId(Long programCohortId) {
        this.programCohortId = programCohortId;
    }

    public String getProgramCollectionName() {
        return programCollectionName;
    }

    public void setProgramCollectionName(String programCollectionName) {
        this.programCollectionName = programCollectionName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramCohortCollectionDTO programCohortCollectionDTO = (ProgramCohortCollectionDTO) o;
        if (programCohortCollectionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programCohortCollectionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramCohortCollectionDTO{" +
            "id=" + getId() +
            ", requiredCompletion=" + getRequiredCompletion() +
            ", requiredLevel=" + getRequiredLevel() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", programCollection=" + getProgramCollectionId() +
            ", programCohort=" + getProgramCohortId() +
            "}";
    }
}
