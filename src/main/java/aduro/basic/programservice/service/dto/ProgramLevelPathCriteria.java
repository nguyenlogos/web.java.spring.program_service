package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramLevelPath} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramLevelPathResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-level-paths?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramLevelPathCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter pathId;

    private StringFilter name;

    private StringFilter subgroupId;

    private StringFilter subgroupName;

    private StringFilter pathType;

    private StringFilter pathCategory;

    private LongFilter programLevelId;

    public ProgramLevelPathCriteria(){
    }

    public ProgramLevelPathCriteria(ProgramLevelPathCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.pathId = other.pathId == null ? null : other.pathId.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.subgroupId = other.subgroupId == null ? null : other.subgroupId.copy();
        this.subgroupName = other.subgroupName == null ? null : other.subgroupName.copy();
        this.pathType = other.pathType == null ? null : other.pathType.copy();
        this.pathCategory = other.pathCategory == null ? null : other.pathCategory.copy();
        this.programLevelId = other.programLevelId == null ? null : other.programLevelId.copy();
    }

    @Override
    public ProgramLevelPathCriteria copy() {
        return new ProgramLevelPathCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getPathId() {
        return pathId;
    }

    public void setPathId(StringFilter pathId) {
        this.pathId = pathId;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getSubgroupId() {
        return subgroupId;
    }

    public void setSubgroupId(StringFilter subgroupId) {
        this.subgroupId = subgroupId;
    }

    public StringFilter getSubgroupName() {
        return subgroupName;
    }

    public void setSubgroupName(StringFilter subgroupName) {
        this.subgroupName = subgroupName;
    }

    public StringFilter getPathType() {
        return pathType;
    }

    public void setPathType(StringFilter pathType) {
        this.pathType = pathType;
    }

    public StringFilter getPathCategory() {
        return pathCategory;
    }

    public void setPathCategory(StringFilter pathCategory) {
        this.pathCategory = pathCategory;
    }

    public LongFilter getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(LongFilter programLevelId) {
        this.programLevelId = programLevelId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramLevelPathCriteria that = (ProgramLevelPathCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(pathId, that.pathId) &&
            Objects.equals(name, that.name) &&
            Objects.equals(subgroupId, that.subgroupId) &&
            Objects.equals(subgroupName, that.subgroupName) &&
            Objects.equals(pathType, that.pathType) &&
            Objects.equals(pathCategory, that.pathCategory) &&
            Objects.equals(programLevelId, that.programLevelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        pathId,
        name,
        subgroupId,
        subgroupName,
        pathType,
        pathCategory,
        programLevelId
        );
    }

    @Override
    public String toString() {
        return "ProgramLevelPathCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (pathId != null ? "pathId=" + pathId + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (subgroupId != null ? "subgroupId=" + subgroupId + ", " : "") +
                (subgroupName != null ? "subgroupName=" + subgroupName + ", " : "") +
                (pathType != null ? "pathType=" + pathType + ", " : "") +
                (pathCategory != null ? "pathCategory=" + pathCategory + ", " : "") +
                (programLevelId != null ? "programLevelId=" + programLevelId + ", " : "") +
            "}";
    }

}
