package aduro.basic.programservice.service.dto;

import lombok.*;

import java.time.Instant;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScProgramLevelRewardDto {

    private String rewardType;
    private String rewardCode;
    private Long programLevelRewardId;
    private String rewardStatus;
    private Instant rewardCompletedDate;
}
