package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramCollection} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramCollectionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-collections?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramCollectionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter displayName;

    private StringFilter longDescription;

    private InstantFilter createdDate;

    private InstantFilter modifiedDate;

    private LongFilter programId;

    private LongFilter programCollectionContentId;

    public ProgramCollectionCriteria(){
    }

    public ProgramCollectionCriteria(ProgramCollectionCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.displayName = other.displayName == null ? null : other.displayName.copy();
        this.longDescription = other.longDescription == null ? null : other.longDescription.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.modifiedDate = other.modifiedDate == null ? null : other.modifiedDate.copy();
        this.programId = other.programId == null ? null : other.programId.copy();
        this.programCollectionContentId = other.programCollectionContentId == null ? null : other.programCollectionContentId.copy();
    }

    @Override
    public ProgramCollectionCriteria copy() {
        return new ProgramCollectionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDisplayName() {
        return displayName;
    }

    public void setDisplayName(StringFilter displayName) {
        this.displayName = displayName;
    }

    public StringFilter getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(StringFilter longDescription) {
        this.longDescription = longDescription;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public InstantFilter getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(InstantFilter modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public LongFilter getProgramId() {
        return programId;
    }

    public void setProgramId(LongFilter programId) {
        this.programId = programId;
    }

    public LongFilter getProgramCollectionContentId() {
        return programCollectionContentId;
    }

    public void setProgramCollectionContentId(LongFilter programCollectionContentId) {
        this.programCollectionContentId = programCollectionContentId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramCollectionCriteria that = (ProgramCollectionCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(displayName, that.displayName) &&
            Objects.equals(longDescription, that.longDescription) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(modifiedDate, that.modifiedDate) &&
            Objects.equals(programId, that.programId) &&
            Objects.equals(programCollectionContentId, that.programCollectionContentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        displayName,
        longDescription,
        createdDate,
        modifiedDate,
        programId,
        programCollectionContentId
        );
    }

    @Override
    public String toString() {
        return "ProgramCollectionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (displayName != null ? "displayName=" + displayName + ", " : "") +
                (longDescription != null ? "longDescription=" + longDescription + ", " : "") +
                (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
                (modifiedDate != null ? "modifiedDate=" + modifiedDate + ", " : "") +
                (programId != null ? "programId=" + programId + ", " : "") +
                (programCollectionContentId != null ? "programCollectionContentId=" + programCollectionContentId + ", " : "") +
            "}";
    }

}
