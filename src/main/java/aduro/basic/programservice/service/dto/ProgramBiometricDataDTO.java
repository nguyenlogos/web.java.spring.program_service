package aduro.basic.programservice.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link aduro.basic.programservice.domain.ProgramBiometricData} entity.
 */
public class ProgramBiometricDataDTO implements Serializable {

    private Long id;

    private String biometricCode;

    private String biometricName;

    private Float maleMin;

    private Float maleMax;

    private Float femaleMin;

    private Float femaleMax;

    private Float unidentifiedMin;

    private Float unidentifiedMax;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBiometricCode() {
        return biometricCode;
    }

    public void setBiometricCode(String biometricCode) {
        this.biometricCode = biometricCode;
    }

    public String getBiometricName() {
        return biometricName;
    }

    public void setBiometricName(String biometricName) {
        this.biometricName = biometricName;
    }

    public Float getMaleMin() {
        return maleMin;
    }

    public void setMaleMin(Float maleMin) {
        this.maleMin = maleMin;
    }

    public Float getMaleMax() {
        return maleMax;
    }

    public void setMaleMax(Float maleMax) {
        this.maleMax = maleMax;
    }

    public Float getFemaleMin() {
        return femaleMin;
    }

    public void setFemaleMin(Float femaleMin) {
        this.femaleMin = femaleMin;
    }

    public Float getFemaleMax() {
        return femaleMax;
    }

    public void setFemaleMax(Float femaleMax) {
        this.femaleMax = femaleMax;
    }

    public Float getUnidentifiedMin() {
        return unidentifiedMin;
    }

    public void setUnidentifiedMin(Float unidentifiedMin) {
        this.unidentifiedMin = unidentifiedMin;
    }

    public Float getUnidentifiedMax() {
        return unidentifiedMax;
    }

    public void setUnidentifiedMax(Float unidentifiedMax) {
        this.unidentifiedMax = unidentifiedMax;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgramBiometricDataDTO programBiometricDataDTO = (ProgramBiometricDataDTO) o;
        if (programBiometricDataDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), programBiometricDataDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProgramBiometricDataDTO{" +
            "id=" + getId() +
            ", biometricCode='" + getBiometricCode() + "'" +
            ", biometricName='" + getBiometricName() + "'" +
            ", maleMin=" + getMaleMin() +
            ", maleMax=" + getMaleMax() +
            ", femaleMin=" + getFemaleMin() +
            ", femaleMax=" + getFemaleMax() +
            ", unidentifiedMin=" + getUnidentifiedMin() +
            ", unidentifiedMax=" + getUnidentifiedMax() +
            "}";
    }
}
