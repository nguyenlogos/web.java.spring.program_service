package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.domain.ProgramUserCohort;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MemberProgramDto {
    private String clientId;
    private String participantId;
    private Long programId;
    private Integer currentLevel;
    private BigDecimal totalPoint;
    private List<ProgramUserCohortDTO> programUserCohorts;
    private List<ProgramLevelCollectionDto> programLevelCollections;
    private List<ProgramCollectionDTO> nonRequiredCollections;
}
