package aduro.basic.programservice.service.dto;

public enum ActivityEventQueueStatus {

    New {
        @Override
        public String toString() {
            return "New";
        }
    },
    Error  {
        @Override
        public String toString() {
            return "Error ";
        }
    },
    Completed  {
        @Override
        public String toString() {
            return "Completed ";
        }
    },

}
