package aduro.basic.programservice.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;

/**
 * Criteria class for the {@link aduro.basic.programservice.domain.ProgramSubCategoryPoint} entity. This class is used
 * in {@link aduro.basic.programservice.web.rest.ProgramSubCategoryPointResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /program-sub-category-points?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ProgramSubCategoryPointCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private BigDecimalFilter percentPoint;

    private BigDecimalFilter valuePoint;

    private IntegerFilter completionsCap;

    private LongFilter programCategoryPointId;

    public ProgramSubCategoryPointCriteria(){
    }

    public ProgramSubCategoryPointCriteria(ProgramSubCategoryPointCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.percentPoint = other.percentPoint == null ? null : other.percentPoint.copy();
        this.valuePoint = other.valuePoint == null ? null : other.valuePoint.copy();
        this.completionsCap = other.completionsCap == null ? null : other.completionsCap.copy();
        this.programCategoryPointId = other.programCategoryPointId == null ? null : other.programCategoryPointId.copy();
    }

    @Override
    public ProgramSubCategoryPointCriteria copy() {
        return new ProgramSubCategoryPointCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public BigDecimalFilter getPercentPoint() {
        return percentPoint;
    }

    public void setPercentPoint(BigDecimalFilter percentPoint) {
        this.percentPoint = percentPoint;
    }

    public BigDecimalFilter getValuePoint() {
        return valuePoint;
    }

    public void setValuePoint(BigDecimalFilter valuePoint) {
        this.valuePoint = valuePoint;
    }

    public IntegerFilter getCompletionsCap() {
        return completionsCap;
    }

    public void setCompletionsCap(IntegerFilter completionsCap) {
        this.completionsCap = completionsCap;
    }

    public LongFilter getProgramCategoryPointId() {
        return programCategoryPointId;
    }

    public void setProgramCategoryPointId(LongFilter programCategoryPointId) {
        this.programCategoryPointId = programCategoryPointId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ProgramSubCategoryPointCriteria that = (ProgramSubCategoryPointCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(percentPoint, that.percentPoint) &&
            Objects.equals(valuePoint, that.valuePoint) &&
            Objects.equals(completionsCap, that.completionsCap) &&
            Objects.equals(programCategoryPointId, that.programCategoryPointId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        code,
        percentPoint,
        valuePoint,
        completionsCap,
        programCategoryPointId
        );
    }

    @Override
    public String toString() {
        return "ProgramSubCategoryPointCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (code != null ? "code=" + code + ", " : "") +
                (percentPoint != null ? "percentPoint=" + percentPoint + ", " : "") +
                (valuePoint != null ? "valuePoint=" + valuePoint + ", " : "") +
                (completionsCap != null ? "completionsCap=" + completionsCap + ", " : "") +
                (programCategoryPointId != null ? "programCategoryPointId=" + programCategoryPointId + ", " : "") +
            "}";
    }

}
