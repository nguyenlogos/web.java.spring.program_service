package aduro.basic.programservice.service.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CheckPathInProgressRequest {

    private Long pathId;

}
