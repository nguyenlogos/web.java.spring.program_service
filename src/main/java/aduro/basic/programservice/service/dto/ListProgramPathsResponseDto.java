package aduro.basic.programservice.service.dto;

import aduro.basic.programservice.ap.dto.ProgramPathsDTO;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListProgramPathsResponseDto {
    private List<ProgramPathsDTO> paths;
}
