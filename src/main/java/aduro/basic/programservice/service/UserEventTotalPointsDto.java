package aduro.basic.programservice.service;

import java.math.BigDecimal;

public class UserEventTotalPointsDto {
    BigDecimal totalUserEventPoints;

    public BigDecimal getTotalUserEventPoints() {
        return totalUserEventPoints;
    }

    public void setTotalUserEventPoints(BigDecimal totalUserEventPoints) {
        this.totalUserEventPoints = totalUserEventPoints;
    }
}
