package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ClientProgramDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ClientProgram}.
 */
public interface ClientProgramService {

    /**
     * Save a clientProgram.
     *
     * @param clientProgramDTO the entity to save.
     * @return the persisted entity.
     */
    ClientProgramDTO save(ClientProgramDTO clientProgramDTO);

    /**
     * Get all the clientPrograms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClientProgramDTO> findAll(Pageable pageable);


    /**
     * Get the "id" clientProgram.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClientProgramDTO> findOne(Long id);

    /**
     * Delete the "id" clientProgram.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    ClientProgramDTO  assignClientsToProgram(Long programId, List<ClientProgramDTO> clientProgramDTOList);

    //List<ClientProgramDTO> findClientProgramByDateRange(String clientId, LocalDate startDate, LocalDate resetDate);
}
