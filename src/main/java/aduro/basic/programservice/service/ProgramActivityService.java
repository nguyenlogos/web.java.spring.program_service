package aduro.basic.programservice.service;

import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.service.dto.ProgramActivityDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramActivity}.
 */
public interface ProgramActivityService {

    /**
     * Save a programActivity.
     *
     * @param programActivityDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramActivityDTO save(ProgramActivityDTO programActivityDTO);

    /**
     * Save a programActivity.
     *
     * @param customActivityDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramActivityDTO saveCustomActivity(CustomActivityDTO customActivityDTO);

    /**
     * Get all the programActivities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramActivityDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programActivity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramActivityDTO> findOne(Long id);

    /**
     * Delete the "id" programActivity.
     *
     * @param id the id of the entity.
     */
    boolean delete(Long id);

    boolean deleteProgramActivityByActivityIdAndProgramId(String activityId, Long programId);

    List<ProgramActivityDTO> addBulkActivity(Long programId, List<ProgramActivityDTO> programActivityList);

    boolean deleteBulkProgramActivities(List<Long> ids, Long programId);

    List<CustomActivityDTO> getCustomActivitiesByProgram(String type, Long programId) throws Exception;

    /**
     * Check for the existence of screeningType in the Phase
     *
     * @param programActivityDTO
     * @param customActivityDTO
     */
    boolean checkExitScreeningTypeInPhase(ProgramActivityDTO programActivityDTO , CustomActivityDTO customActivityDTO);

}
