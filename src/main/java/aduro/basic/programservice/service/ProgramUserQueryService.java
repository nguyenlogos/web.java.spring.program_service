package aduro.basic.programservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import aduro.basic.programservice.adp.repository.AmpUserActivityProgressRepository;
import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.PathAdapter;
import aduro.basic.programservice.ap.dto.PathDTO;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.enumeration.CoachEvent;
import aduro.basic.programservice.repository.UserEventRepository;
import aduro.basic.programservice.service.dto.TobaccoUserProgramDTO;
import aduro.basic.programservice.service.dto.UserDTO;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramUserRepository;
import aduro.basic.programservice.service.dto.ProgramUserCriteria;
import aduro.basic.programservice.service.dto.ProgramUserDTO;
import aduro.basic.programservice.service.mapper.ProgramUserMapper;

/**
 * Service for executing complex queries for {@link ProgramUser} entities in the database.
 * The main input is a {@link ProgramUserCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramUserDTO} or a {@link Page} of {@link ProgramUserDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramUserQueryService extends QueryService<ProgramUser> {

    private final Logger log = LoggerFactory.getLogger(ProgramUserQueryService.class);

    private final ProgramUserRepository programUserRepository;

    private final ProgramUserMapper programUserMapper;

    private final CustomActivityAdapter customActivityAdapter;

    private final AmpUserActivityProgressRepository ampUserActivityProgressRepository;

    private final UserEventRepository userEventRepository;

    private final PathAdapter pathAdapter;

    final String path = "Path";
    final String activity = "Activity";

    public ProgramUserQueryService(ProgramUserRepository programUserRepository, ProgramUserMapper programUserMapper, CustomActivityAdapter customActivityAdapter, AmpUserActivityProgressRepository ampUserActivityProgressRepository, UserEventRepository userEventRepository, PathAdapter pathAdapter) {
        this.programUserRepository = programUserRepository;
        this.programUserMapper = programUserMapper;
        this.customActivityAdapter = customActivityAdapter;
        this.ampUserActivityProgressRepository = ampUserActivityProgressRepository;
        this.userEventRepository = userEventRepository;
        this.pathAdapter = pathAdapter;
    }

    /**
     * Return a {@link List} of {@link ProgramUserDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramUserDTO> findByCriteria(ProgramUserCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramUser> specification = createSpecification(criteria);
        return programUserMapper.toDto(programUserRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramUserDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramUserDTO> findByCriteria(ProgramUserCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramUser> specification = createSpecification(criteria);
        return programUserRepository.findAll(specification, page)
            .map(programUserMapper::toDto);
    }




    @Transactional(readOnly = true)
    public List<TobaccoUserProgramDTO> findProgramUserByClientIdAndProgramId(String clientId, Long programId) {
        List<TobaccoUserProgramDTO> result = new ArrayList<>();
        programUserRepository.findProgramUserByClientIdAndProgramId(clientId, programId)
            .map(programUserMapper::toDto)
            .forEach( programUserDTO -> {

            try {
                UserDto user = customActivityAdapter.getUser(programUserDTO.getParticipantId());
                log.debug("Tobacco participantId user: {}", user != null ? user.getUserId() : 0);
                Option<UserDto> userDtoOption = Option.of(user);

                userDtoOption.peek(userDto -> {

                    // Check User Has completed Survey Tobacco
                    //1 user -> n survey tobaccos
                    List<TobaccoUserProgramDTO> tobaccoUserProgramDTOList = ampUserActivityProgressRepository.findByUserIdAndType(userDto.getUserId(), Constants.ACTIVITY_TOBACCO_ATTESTATION)
                        .map(ampUserActivityProgress -> {
                            TobaccoUserProgramDTO progressUser = getTobaccoUserProgramDTO(programUserDTO);
                            progressUser.setTobaccoCompleted(ampUserActivityProgress.getCompletedAt() != null ? "true" : "false");
                            progressUser.setCompletedAt(ampUserActivityProgress.getCompletedAt());
                            progressUser.setActivityId(ampUserActivityProgress.getCustomActivityId().toString());
                            progressUser.setStatus(ampUserActivityProgress.getCompletedAt() != null ? "Completed" : "InProgress");
                            return progressUser;
                        }).collect(Collectors.toList());

                    if (tobaccoUserProgramDTOList.isEmpty()) {
                        TobaccoUserProgramDTO dto = getTobaccoUserProgramDTO(programUserDTO);
                        dto.setTobaccoCompleted("false");
                        dto.setStatus("Not Enroll Yet");
                        result.add(dto);
                    } else {
                        result.addAll(tobaccoUserProgramDTOList);
                    }
                }).onEmpty(() -> {
                    log.debug("User not exist: ID {}", programUserDTO.getId());
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

                // Check User Enroll One-One-Coach
                // 1 user -> n goal
                Seq<UserEvent> coachingEventByUser = userEventRepository.findUserEventsByProgramUserIdAndEventCategory(programUserDTO.getId(), Constants.COACHING);
                Map<String, List<UserEvent>> groups = coachingEventByUser.collect(Collectors.groupingBy(u -> u.getEventId()));
                if (!groups.isEmpty()) {
                    Option<TobaccoUserProgramDTO> existedUser = Option.ofOptional(result.stream().filter(a -> a.getEmail().equalsIgnoreCase(programUserDTO.getEmail())).findFirst());
                    TobaccoUserProgramDTO tobaccoUserProgramDTO = existedUser.get();


                    if (groups.size() == 1) {
                        groups.forEach((goadId, events) -> {
                            long sections = events.stream().filter( e -> e.getEventCode().equalsIgnoreCase(CoachEvent.COMPLETE_ONE_ONE_WEEKLY_SESSION.name())).count();
                            // user complete coaching
                            Optional<UserEvent> userEventOptional = events.stream().filter(e -> e.getEventCode().equalsIgnoreCase(CoachEvent.COMPLETE_ONE_ONE_COACHING.name())).findFirst();

                            Optional<UserEvent> userEventRegisterOptional = events.stream().filter(e -> e.getEventCode().equalsIgnoreCase(CoachEvent.REGISTRATION_ONE_ONE_COACHING.name())).findFirst();
                            Option<UserEvent> registerOption = Option.ofOptional(userEventRegisterOptional);
                            Option<UserEvent> userEventOption = Option.ofOptional(userEventOptional);

                            userEventOption.peek(event -> {
                                tobaccoUserProgramDTO.setGoalId(event.getEventId());
                                tobaccoUserProgramDTO.setCoachingStatus("Completed");
                                tobaccoUserProgramDTO.setCoachingProgress(String.format("%s %s", sections, "Completed Sections"));
                            }).onEmpty(() -> {
                                if (registerOption.isDefined()) {
                                    tobaccoUserProgramDTO.setGoalId(registerOption.get().getEventId());
                                }
                                tobaccoUserProgramDTO.setCoachingStatus("InProgress");
                                tobaccoUserProgramDTO.setCoachingProgress(String.format("%s %s", sections, "Completed Sections"));
                            });
                        });
                    } else {
                        boolean isFirst = true;
                        for (Map.Entry<String, List<UserEvent>> entry: groups.entrySet()) {
                            try {
                                TobaccoUserProgramDTO dto = null;
                                List<UserEvent> events = entry.getValue();
                                if (isFirst) {
                                    dto = tobaccoUserProgramDTO;

                                } else {
                                    dto = (TobaccoUserProgramDTO) tobaccoUserProgramDTO.clone();

                                }
                                long sections = events.stream().filter( e -> e.getEventCode().equalsIgnoreCase(CoachEvent.COMPLETE_ONE_ONE_WEEKLY_SESSION.name())).count();
                                // user complete coaching
                                Optional<UserEvent> userEventOptional = events.stream().filter(e -> e.getEventCode().equalsIgnoreCase(CoachEvent.COMPLETE_ONE_ONE_COACHING.name())).findFirst();
                                Option<UserEvent> userEventOption = Option.ofOptional(userEventOptional);

                                Optional<UserEvent> userEventRegisterOptional = events.stream().filter(e -> e.getEventCode().equalsIgnoreCase(CoachEvent.REGISTRATION_ONE_ONE_COACHING.name())).findFirst();
                                Option<UserEvent> registerOption = Option.ofOptional(userEventRegisterOptional);
                                if (userEventOption.isDefined()) {
                                    UserEvent event = userEventOption.get();
                                    dto.setGoalId(event.getEventId());
                                    dto.setCoachingStatus("Completed");
                                } else {
                                    if (registerOption.isDefined()) {
                                        dto.setGoalId(registerOption.get().getEventId());
                                    }
                                    dto.setCoachingStatus("InProgress");
                                }
                                dto.setCoachingProgress(String.format("%s %s", sections, "Completed Sections"));
                                if  (!isFirst) result.add(tobaccoUserProgramDTO);
                                isFirst = false;
                            } catch (CloneNotSupportedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


                // check user enroll Ras Path
                //1 user -> n paths

                try {
                    List<PathDTO> enrolledRasPaths = pathAdapter.getRasPathByUserId(programUserDTO.getParticipantId(), programUserDTO.getProgramId());
                    List<TobaccoUserProgramDTO> filterEmails = result.stream().filter(a -> a.getEmail().equalsIgnoreCase(programUserDTO.getEmail())).collect(Collectors.toList());
                    Option<TobaccoUserProgramDTO> existedUser = Option.of(filterEmails.get(filterEmails.size() - 1));
                    if (existedUser.isDefined()) {
                        TobaccoUserProgramDTO tobaccoUserProgramDTO = existedUser.get();
                        if (!enrolledRasPaths.isEmpty()) {
                            List<PathDTO> paths = enrolledRasPaths.stream().filter(p -> p.getType().equalsIgnoreCase(path)).collect(Collectors.toList());
                            List<PathDTO> activities = enrolledRasPaths.stream().filter(p -> p.getType().equalsIgnoreCase(activity)).collect(Collectors.toList());
                            createRowForPath(result, enrolledRasPaths, tobaccoUserProgramDTO, paths);
                            createRowForPath(result, enrolledRasPaths, tobaccoUserProgramDTO, activities);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        });


        return result;
    }

    private void createRowForPath(List<TobaccoUserProgramDTO> result, List<PathDTO> enrolledRasPaths, TobaccoUserProgramDTO tobaccoUserProgramDTO, List<PathDTO> paths) {
        if (paths.isEmpty()) return;
        if (paths.size() == 1) {
            PathDTO pathDTO = enrolledRasPaths.get(0);
            if (pathDTO.getType().equalsIgnoreCase(activity)) {
                tobaccoUserProgramDTO.setActivityRasTitle(pathDTO.getTitle());
                tobaccoUserProgramDTO.setActivityRasStatus(pathDTO.getCompletedAt() != null ? "Completed" : "InProgress");
            } else {
                tobaccoUserProgramDTO.setPathRasTitle(pathDTO.getTitle());
                tobaccoUserProgramDTO.setPathRasStatus(pathDTO.getCompletedAt() != null ? "Completed" : "InProgress");
            }


            tobaccoUserProgramDTO.setEnrollType( pathDTO.getType() != null && pathDTO.getType().equalsIgnoreCase(activity) ? "Ras Activity" : "Ras Path");
        } else {
            paths.forEach(pathDTO -> {
                // clone that user
                TobaccoUserProgramDTO dto = null;
                try {
                    dto = (TobaccoUserProgramDTO) tobaccoUserProgramDTO.clone();
                    if (pathDTO.getType().equalsIgnoreCase(activity)) {
                        dto.setActivityRasTitle(pathDTO.getTitle());
                    } else {
                        dto.setPathRasTitle(pathDTO.getTitle());
                    }
                    dto.setPathRasStatus(pathDTO.getCompletedAt() != null ? "Completed" : "InProgress");
                    dto.setEnrollType( pathDTO.getType() != null && pathDTO.getType().equalsIgnoreCase(activity) ? "Ras Activity" : "Ras Path");
                    result.add(dto);
                } catch (CloneNotSupportedException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private TobaccoUserProgramDTO getTobaccoUserProgramDTO(ProgramUserDTO programUserDTO) {
        TobaccoUserProgramDTO dto = new TobaccoUserProgramDTO();
        dto.setId(programUserDTO.getId());
        dto.setClientId(programUserDTO.getClientId());
        dto.setClientName(programUserDTO.getClientName());
        dto.setCurrentLevel(programUserDTO.getCurrentLevel());
        dto.setEmail(programUserDTO.getEmail());
        dto.setFirstName(programUserDTO.getFirstName());
        dto.setParticipantId(programUserDTO.getParticipantId());
        dto.setProgramId(programUserDTO.getProgramId());
        dto.setSubgroupId(programUserDTO.getSubgroupId());
        dto.setTotalUserPoint(programUserDTO.getTotalUserPoint());
        dto.setPhone(programUserDTO.getPhone());
        return dto;
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramUserCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramUser> specification = createSpecification(criteria);
        return programUserRepository.count(specification);
    }

    /**
     * Function to convert ProgramUserCriteria to a {@link Specification}.
     */
    private Specification<ProgramUser> createSpecification(ProgramUserCriteria criteria) {
        Specification<ProgramUser> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramUser_.id));
            }
            if (criteria.getParticipantId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParticipantId(), ProgramUser_.participantId));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), ProgramUser_.clientId));
            }
            if (criteria.getTotalUserPoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalUserPoint(), ProgramUser_.totalUserPoint));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramId(), ProgramUser_.programId));
            }
            if (criteria.getCurrentLevel() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCurrentLevel(), ProgramUser_.currentLevel));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), ProgramUser_.email));
            }
            if (criteria.getFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstName(), ProgramUser_.firstName));
            }
            if (criteria.getLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastName(), ProgramUser_.lastName));
            }
            if (criteria.getPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPhone(), ProgramUser_.phone));
            }
            if (criteria.getClientName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientName(), ProgramUser_.clientName));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), ProgramUser_.subgroupId));
            }
            if (criteria.getSubgroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupName(), ProgramUser_.subgroupName));
            }
        }
        return specification;
    }
}
