package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ClientBrandSettingDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ClientBrandSetting}.
 */
public interface ClientBrandSettingService {

    /**
     * Save a clientBrandSetting.
     *
     * @param clientBrandSettingDTO the entity to save.
     * @return the persisted entity.
     */
    ClientBrandSettingDTO save(ClientBrandSettingDTO clientBrandSettingDTO);

    /**
     * Get all the clientBrandSettings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClientBrandSettingDTO> findAll(Pageable pageable);


    /**
     * Get the "id" clientBrandSetting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClientBrandSettingDTO> findOne(Long id);

    /**
     * Delete the "id" clientBrandSetting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Optional<ClientBrandSettingDTO> getClientBrandSettingByClientId(String clientId);

    ClientBrandSettingDTO getClientBrandSettingBySubPathUrl(String subPathUrl);

    ClientBrandSettingDTO saveClientBrandSetting(ClientBrandSettingDTO clientBrandSettingDTO);

}
