package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramBiometricDataDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramBiometricData}.
 */
public interface ProgramBiometricDataService {

    /**
     * Save a programBiometricData.
     *
     * @param programBiometricDataDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramBiometricDataDTO save(ProgramBiometricDataDTO programBiometricDataDTO);

    /**
     * Get all the programBiometricData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramBiometricDataDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programBiometricData.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramBiometricDataDTO> findOne(Long id);

    /**
     * Delete the "id" programBiometricData.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
