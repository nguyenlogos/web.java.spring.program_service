package aduro.basic.programservice.service;

import aduro.basic.programservice.domain.ActivityEventQueue;
import aduro.basic.programservice.service.dto.ActivityEventQueueDTO;

import aduro.basic.programservice.service.dto.CheckEmployerVerifiedDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ActivityEventQueue}.
 */
public interface ActivityEventQueueService {

    /**
     * Save a activityEventQueue.
     *
     * @param activityEventQueueDTO the entity to save.
     * @return the persisted entity.
     */
    ActivityEventQueueDTO save(ActivityEventQueueDTO activityEventQueueDTO);

    /**
     * Get all the activityEventQueues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ActivityEventQueueDTO> findAll(Pageable pageable);


    /**
     * Get the "id" activityEventQueue.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ActivityEventQueueDTO> findOne(Long id);

    /**
     * Delete the "id" activityEventQueue.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    CheckEmployerVerifiedDTO validateEmployerVierified(CheckEmployerVerifiedDTO verfifiedDTO) throws Exception;

    List<ActivityEventQueueDTO> saveList(List<ActivityEventQueueDTO> activityEventQueueDTOs);

    void processEmployerVierifiedActivity() throws Exception;


}
