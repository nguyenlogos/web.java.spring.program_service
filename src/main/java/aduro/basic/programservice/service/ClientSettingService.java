package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ClientSettingDTO;

import aduro.basic.programservice.tangocard.dto.CreditCardResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ClientSetting}.
 */
public interface ClientSettingService {

    /**
     * Save a clientSetting.
     *
     * @param clientSettingDTO the entity to save.
     * @return the persisted entity.
     */
    ClientSettingDTO save(ClientSettingDTO clientSettingDTO) throws Exception;

    /**
     * Get all the clientSettings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClientSettingDTO> findAll(Pageable pageable);


    /**
     * Get the "id" clientSetting.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClientSettingDTO> findOne(Long id);

    /**
     * Delete the "id" clientSetting.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    ClientSettingDTO getByClientId(String clientId) throws Exception;

    List<CreditCardResponse> getCreditCardsByAccount(String customerIdentifier, String accountIdentifier) throws Exception;

    ClientSettingDTO updateThresholdBalance(ClientSettingDTO clientSettingDTO);
}
