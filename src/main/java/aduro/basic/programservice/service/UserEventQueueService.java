package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.UserEventQueueDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.UserEventQueue}.
 */
public interface UserEventQueueService {

    /**
     * Save a userEventQueue.
     *
     * @param userEventQueueDTO the entity to save.
     * @return the persisted entity.
     */
    UserEventQueueDTO save(UserEventQueueDTO userEventQueueDTO);

    /**
     * Get all the userEventQueues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserEventQueueDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userEventQueue.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserEventQueueDTO> findOne(Long id);

    /**
     * Delete the "id" userEventQueue.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
