package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ClientSubDomainDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ClientSubDomain}.
 */
public interface ClientSubDomainService {

    /**
     * Save a clientSubDomain.
     *
     * @param clientSubDomainDTO the entity to save.
     * @return the persisted entity.
     */
    ClientSubDomainDTO save(ClientSubDomainDTO clientSubDomainDTO);

    /**
     * Get all the clientSubDomains.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ClientSubDomainDTO> findAll(Pageable pageable);


    /**
     * Get the "id" clientSubDomain.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ClientSubDomainDTO> findOne(Long id);

    /**
     * Delete the "id" clientSubDomain.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
