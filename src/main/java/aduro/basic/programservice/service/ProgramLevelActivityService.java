package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.APIResponse;
import aduro.basic.programservice.service.dto.ProgramLevelActivityDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramLevelActivity}.
 */
public interface ProgramLevelActivityService {
    /**
     * Save a programLevelActivity.
     *
     * @param programLevelActivityDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramLevelActivityDTO save(ProgramLevelActivityDTO programLevelActivityDTO);

    /**
     * Get all the programLevelActivities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramLevelActivityDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programLevelActivity.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramLevelActivityDTO> findOne(Long id);

    /**
     * Delete the "id" programLevelActivity.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<ProgramLevelActivityDTO> updateLevelActivities(Long programLevelId, List<ProgramLevelActivityDTO> programLevelActivityDTOS);

    APIResponse<Boolean> saveWithList(List<ProgramLevelActivityDTO> programLevelActivities);
}
