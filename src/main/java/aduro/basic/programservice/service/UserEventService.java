package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.UserEvent}.
 */
public interface UserEventService {

    /**
     * Save a userEvent.
     *
     * @param userEventDTO the entity to save.
     * @return the persisted entity.
     */
    UserEventDTO save(UserEventDTO userEventDTO);

    /**
     * Get all the userEvents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserEventDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userEvent.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserEventDTO> findOne(Long id);

    /**
     * Delete the "id" userEvent.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    //boolean incentiveEvent(IncentiveEventDTO incentiveEventDTO);

    IncentiveEventPointDTO handleActivityIncentiveEvent(ActivityEventDTO activityEventDTO);


    IncentiveEventPointDTO handleIncentiveEventForWellmetric(IncentiveEventDTO incentiveEventDTO);

    IncentiveResponseDTO findIncentiveResultByParticipantIdAndClientIdAndEventId(String participantId, String clientId, String eventId, boolean isProgramTester);

    IncentiveEventPointDTO processingIncentiveEventByEventCategory(IncentiveEventDTO incentiveEventDTO);

    List<UserEventDTO> getAllUserEventByParticipantIdAndClientIdAndEventId(String participantId, String clientId, String eventId, Map<String, String> queryParams);

    UserEventTotalPointsDto getTotalBonusPointsByCustomActivityId (String participantId, String clientId, String eventId, String eventCategory, Map<String, String> queryParams);
}
