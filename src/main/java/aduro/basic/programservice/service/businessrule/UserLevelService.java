package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.domain.CronJobLoader;

import java.util.List;

public interface UserLevelService {
    void processCalculateLevelInQueue();
    void processParticipantOfClient(List<CronJobLoader> loaders);
    void calculateUserArchivedLevel();
}
