package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.service.businessrule.validate.ActivityIncentiveValidator;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

@Service
@Transactional
public class TobaccoRuleImpl implements TobaccoRule {

    private final ActivityIncentiveValidator validator;

    private final ClientProgramRepository clientProgramRepository;

    private final ProgramRepository programRepository;

    private final ApplicationProperties applicationProperties;

    private final ProgramUserRepository programUserRepository;

    private final UserEventQueueRepository userEventQueueRepository;

    private final UserIncentiveBusinessRule userIncentiveBusinessRule;

    private final UserEventRepository userEventRepository;

    private final UserRewardRepository userRewardRepository;

    private final CustomActivityAdapter customActivityAdapter;

    public TobaccoRuleImpl(ActivityIncentiveValidator validator, ClientProgramRepository clientProgramRepository, ProgramRepository programRepository, ApplicationProperties applicationProperties, ProgramUserRepository programUserRepository, UserEventQueueRepository userEventQueueRepository, UserIncentiveBusinessRule userIncentiveBusinessRule, UserEventRepository userEventRepository, UserRewardRepository userRewardRepository, CustomActivityAdapter customActivityAdapter) {
        this.validator = validator;
        this.clientProgramRepository = clientProgramRepository;
        this.programRepository = programRepository;
        this.applicationProperties = applicationProperties;
        this.programUserRepository = programUserRepository;
        this.userEventQueueRepository = userEventQueueRepository;
        this.userIncentiveBusinessRule = userIncentiveBusinessRule;
        this.userEventRepository = userEventRepository;
        this.userRewardRepository = userRewardRepository;
        this.customActivityAdapter = customActivityAdapter;
    }

    @Override
    public Optional<IncentiveEventPointDTO> processIncentive(ActivityEventDTO activityEventDTO) {
        Validation<Seq<String>, ActivityEventDTO> validation = validator.validEventDTO(activityEventDTO.getEventCode(),
            activityEventDTO.getEventId(),
            activityEventDTO.getClientId(),
            activityEventDTO.getEventDate(),
            activityEventDTO.getParticipants());
        if (!validation.isValid()) {
            throw new BadRequestAlertException(validation.toString(), "ActivityEventDTO", "EventDTO");
        }

        UserDto user = null;
        try {
            user = customActivityAdapter.getUser(activityEventDTO.getParticipants().get(0).getParticipantId());
        } catch (Exception e) {
            throw new BadRequestAlertException("User is Not Found", "customActivityAdapter", "processIncentive");
        }
        //get client program
        Optional<ClientProgram> optionalClientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(activityEventDTO.getClientId(), ProgramStatus.Active.toString(), user.isAsProgramTester());
        Option<ClientProgram> optionProgram = Option.ofOptional(optionalClientProgram);

        return  optionProgram.map(clientProgram -> processIncentiveWithClientProgram(clientProgram, activityEventDTO))
            .onEmpty(this::handleClientProgramEmpty).getOrElse(Optional.of(new IncentiveEventPointDTO()));

    }

    private Optional<IncentiveEventPointDTO> processIncentiveWithClientProgram(ClientProgram clientProgram, ActivityEventDTO activityEventDTO) {
        //get program
        Optional<Program> optionalProgram = programRepository.findById(clientProgram.getProgramId());
        Option<Program> programOption = Option.ofOptional(optionalProgram);

        return programOption.flatMap(program -> processIncentiveWithProgram(program, activityEventDTO))
            .onEmpty(() -> {
            throw new BadRequestAlertException("program Id can not null", "UserEvent", "ProgramIdNull");
        }).toJavaOptional();
    }

    private Option<IncentiveEventPointDTO> processIncentiveWithProgram(Program program, ActivityEventDTO activityEventDTO) {
        // should be check tobacco
        if (!program.getStatus().equalsIgnoreCase(ProgramStatus.Active.name())) {
            throw new BadRequestAlertException("Program is not active.", "Program", "Program Status");
        }

        if (program.isIsTobaccoSurchargeManagement() == null || !program.isIsTobaccoSurchargeManagement()) {
            throw new BadRequestAlertException("Program is not a tobacco program.", "Program", "Program Tobacco");
        }

        //create user event queue for each participant
         Option<ParticipantDTO> participantDTOOption = Option.ofOptional(activityEventDTO.getParticipants().stream()
            .findFirst());

         return participantDTOOption.flatMap(participantDTO -> {
             UserEventQueue userEventQueue = createUserEventQueue(activityEventDTO, program.getId());
             if (isNullOrEmpty(activityEventDTO.getCreatedByAdmin())) {
                 userEventQueue.setCreatedBy(participantDTO.getParticipantId());
             } else {
                 userEventQueue.setCreatedByAdmin(activityEventDTO.getCreatedByAdmin());
             }
             Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(participantDTO.getParticipantId(), program.getId());
             Option<ProgramUser> programUserOption = Option.ofOptional(programUserOptional);

             return programUserOption.flatMap(programUser -> processIncentiveWithProgramUser(programUser, userEventQueue, program))
                 .onEmpty(() -> handleWithNotCreatedProgramUserYet(activityEventDTO, participantDTO, userEventQueue, program));
         })
             .onEmpty(() -> {
                throw new BadRequestAlertException("Participant Id can not be null or empty.","ActivityEventDTO", "participants");
             });

    }

    private Option<IncentiveEventPointDTO>  handleWithNotCreatedProgramUserYet(ActivityEventDTO activityEventDTO, ParticipantDTO participantDTO, UserEventQueue userEventQueue, Program program) {
        //create program User
        ProgramUser programUser = createNewProgramUser(activityEventDTO, participantDTO);
        programUser = programUserRepository.saveAndFlush(programUser);

        // process by Program User
        return processIncentiveWithProgramUser(programUser, userEventQueue, program);
    }

    private Option<IncentiveEventPointDTO> processIncentiveWithProgramUser(ProgramUser programUser, UserEventQueue userEventQueue, Program program) {

        // save user event queue
        userEventQueue.setProgramUserId(programUser.getId());
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Save Event Code = Path for wellmetric tobaco ras

        //TODO: should process point in queue, tasks
        IncentiveEventPointDTO incentiveEventPointDTO = processPointByUserEvent(userEventQueue, program , programUser);

        return Option.of(incentiveEventPointDTO);
    }

    private IncentiveEventPointDTO processPointByUserEvent(UserEventQueue userEvent, Program program, ProgramUser programUser) {

        // CHECK TObacco Ras Dealine
        if (program.getTobaccoProgramDeadline() != null && userEvent.getEventDate().isAfter(program.getTobaccoProgramDeadline())) {
            userEvent.setErrorMessage("Tobacco Ras Program End.");
            userEvent.setExecuteStatus(IncentiveStatusEnum.Error.toString());
            userEventQueueRepository.saveAndFlush(userEvent);
            return userIncentiveBusinessRule.createUserEventWithEmptyPoint(userEvent, programUser, "Tobacco Ras Deadline expired.");
        }

        //CHECK Tobacco Attestation
        if (program.getTobaccoAttestationDeadline() != null && userEvent.getEventDate().isAfter(program.getTobaccoAttestationDeadline())) {
            userEvent.setErrorMessage("Tobacco Attestation Program End.");
            userEvent.setExecuteStatus(IncentiveStatusEnum.Error.toString());
            userEventQueueRepository.saveAndFlush(userEvent);
            return userIncentiveBusinessRule.createUserEventWithEmptyPoint(userEvent, programUser, "Tobacco Attestation  expired.");
        }

        //calculate point and level
        BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program, applicationProperties);

        if(programUser.getSubgroupId() != null && (programUser.getSubgroupId().equals("undefined") || programUser.getSubgroupId().equals("0")))
            programUser.setSubgroupId("");

        IncentiveEventPointDTO incentiveEventPointDTO = userIncentiveBusinessRule.calculatePointAndLevelByCategoryPointAndSubCategoryPoint(userEvent, programUser, Constants.WELLMETRICS, totalUserPoint, programUser.getSubgroupId());

        return incentiveEventPointDTO;
    }

    private UserEventQueue createUserEventQueue(ActivityEventDTO activityEventDTO, Long programId) {

        UserEventQueue userEventQueue = new UserEventQueue();
        userEventQueue.setEventId(activityEventDTO.getEventId());
        userEventQueue.setEventCode(activityEventDTO.getEventCode());
        userEventQueue.setProgramId(programId);
        if (activityEventDTO.getIsHpContent() != null && activityEventDTO.getIsHpContent()) {
            userEventQueue.setEventCategory(Constants.HPCONTENT);
        } else {
            userEventQueue.setEventCategory(Constants.ACTIVITIES);
        }

        userEventQueue.setExecuteStatus(IncentiveStatusEnum.New.name());
        userEventQueue.setEventDate(activityEventDTO.getEventDate());
        return userEventQueue;
    }


    private ProgramUser createNewProgramUser(ActivityEventDTO eventQueue, ParticipantDTO participantDTO) {
        ProgramUser user = new ProgramUser();
        user.setClientId(eventQueue.getClientId());
        user.setParticipantId(participantDTO.getParticipantId());
        user.setEmail(participantDTO.getEmail());
        user.setFirstName(participantDTO.getFirstName());
        user.setLastName(participantDTO.getLastName());
        user.setProgramId(eventQueue.getProgramId());
        user.setCurrentLevel(1);
        user.setTotalUserPoint(BigDecimal.valueOf(0));
        user.setSubgroupId(participantDTO.getSubgroupId());
        return programUserRepository.saveAndFlush(user);
    }


    private void handleClientProgramEmpty() {
        throw new BadRequestAlertException("program is invalid", "UserEvent", "ProgramIdNull");
    }
}
