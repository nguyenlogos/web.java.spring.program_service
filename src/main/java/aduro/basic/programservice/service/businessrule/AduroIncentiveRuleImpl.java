package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.UserEventQueue;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.repository.ClientProgramRepository;
import aduro.basic.programservice.repository.ProgramRepository;
import aduro.basic.programservice.repository.ProgramUserRepository;
import aduro.basic.programservice.repository.UserEventQueueRepository;
import aduro.basic.programservice.service.businessrule.validate.AduroIncentiveValidator;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AduroIncentiveRuleImpl implements AduroIncentiveRule {

    private final Logger log = LoggerFactory.getLogger(AduroIncentiveRuleImpl.class);

    private final AduroIncentiveValidator validator;
    private final ClientProgramRepository clientProgramRepository;
    private final ProgramRepository programRepository;
    private final ProgramUserRepository programUserRepository;
    private final UserEventQueueRepository userEventQueueRepository;
    private final ApplicationProperties applicationProperties;
    private final UserIncentiveBusinessRule userIncentiveBusinessRule;
    private final CustomActivityAdapter customActivityAdapter;

    public AduroIncentiveRuleImpl(AduroIncentiveValidator validator, ClientProgramRepository clientProgramRepository, ProgramRepository programRepository, ProgramUserRepository programUserRepository, UserEventQueueRepository userEventQueueRepository, ApplicationProperties applicationProperties, UserIncentiveBusinessRule userIncentiveBusinessRule, CustomActivityAdapter customActivityAdapter) {
        this.validator = validator;
        this.clientProgramRepository = clientProgramRepository;
        this.programRepository = programRepository;
        this.programUserRepository = programUserRepository;
        this.userEventQueueRepository = userEventQueueRepository;
        this.applicationProperties = applicationProperties;
        this.userIncentiveBusinessRule = userIncentiveBusinessRule;
        this.customActivityAdapter = customActivityAdapter;
    }


    @Override
    public Optional<IncentiveEventPointDTO> processIncentiveEvent(IncentiveEventDTO incentiveEventDTO) {

        //step 1 -> validate
        Validation<Seq<String>, IncentiveEventDTO> validation = validator.validEventDTO(incentiveEventDTO.getEventCode(),
            incentiveEventDTO.getEventId(),
            incentiveEventDTO.getClientId(),
            incentiveEventDTO.getEventDate(),
            incentiveEventDTO.getParticipantId());

        /// get participant type

        if (!validation.isValid()) {
            throw new BadRequestAlertException(validation.toString(), "IncentiveEventDTO", "IncentiveEventDTO");
        }

        //step 2 ->  get client program
        Optional<ClientProgram> optionalClientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(incentiveEventDTO.getClientId(), ProgramStatus.Active.toString(), incentiveEventDTO.isAsProgramTester());
        Option<ClientProgram> optionProgram = Option.ofOptional(optionalClientProgram);

        return  optionProgram.map(clientProgram -> processIncentiveWithClientProgram(clientProgram, incentiveEventDTO))
            .onEmpty(this::handleClientProgramEmpty).getOrElse(Optional.of(new IncentiveEventPointDTO()));
    }

    private void handleClientProgramEmpty() {
        throw new BadRequestAlertException("program is invalid", "UserEvent", "ProgramIdNull");
    }

    private Optional<IncentiveEventPointDTO> processIncentiveWithClientProgram(ClientProgram clientProgram, IncentiveEventDTO incentiveEventDTO) {
        //get program
        Optional<Program> optionalProgram = programRepository.findById(clientProgram.getProgramId());
        Option<Program> programOption = Option.ofOptional(optionalProgram);

        return programOption.flatMap(program -> processIncentiveWithProgram(program, incentiveEventDTO))
            .onEmpty(() -> {
                throw new BadRequestAlertException("program Id can not null", "UserEvent", "ProgramIdNull");
            }).toJavaOptional();
    }

    private UserEventQueue createUserEventQueue(IncentiveEventDTO incentiveEventDTO, Long programId) {

        UserEventQueue userEventQueue = new UserEventQueue();
        userEventQueue.setEventId(incentiveEventDTO.getEventId());
        userEventQueue.setEventCode(incentiveEventDTO.getEventCode());
        userEventQueue.setProgramId(programId);
        userEventQueue.setExecuteStatus(IncentiveStatusEnum.New.name());
        userEventQueue.setEventCategory(incentiveEventDTO.getEventCategory());
        userEventQueue.setEventDate(incentiveEventDTO.getEventDate());
        return userEventQueue;
    }


    private ProgramUser createNewProgramUser(IncentiveEventDTO eventQueue, String participantId) {
        ProgramUser user = new ProgramUser();
        user.setClientId(eventQueue.getClientId());
        user.setParticipantId(participantId);
        user.setEmail(eventQueue.getEmail());
        user.setFirstName(eventQueue.getFirstName());
        user.setLastName(eventQueue.getLastName());
        user.setProgramId(eventQueue.getProgramId());
        user.setCurrentLevel(1);
        user.setTotalUserPoint(BigDecimal.valueOf(0));
        user.setSubgroupId(eventQueue.getSubgroupId());
        return programUserRepository.saveAndFlush(user);
    }

    private Option<IncentiveEventPointDTO> processIncentiveWithProgram(Program program, IncentiveEventDTO incentiveEventDTO) {
        // should program active
        if (!program.getStatus().equalsIgnoreCase(ProgramStatus.Active.name())) {
            throw new BadRequestAlertException("Program is not active.", "Program", "Program Status");
        }

        log.debug("->>> INCENTIVE: processIncentiveWithPrograms");
        //create user event queue for each participant
        //find program user by participantId
        // create event queue for participantID
        UserEventQueue userEventQueue = createUserEventQueue(incentiveEventDTO, program.getId());
        userEventQueue.setCreatedBy(incentiveEventDTO.getParticipantId());

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(incentiveEventDTO.getParticipantId(), program.getId());
        Option<ProgramUser> programUserOption = Option.ofOptional(programUserOptional);

        return programUserOption.flatMap(programUser -> processIncentiveWithProgramUser(programUser, userEventQueue, program))
            .onEmpty(() -> handleWithNotCreatedProgramUserYet(incentiveEventDTO, userEventQueue, program));
    }

    private Option<IncentiveEventPointDTO>  handleWithNotCreatedProgramUserYet(IncentiveEventDTO incentiveEventDTO, UserEventQueue userEventQueue, Program program) {
        //create program User
        ProgramUser programUser = createNewProgramUser(incentiveEventDTO, incentiveEventDTO.getParticipantId());
        programUser = programUserRepository.saveAndFlush(programUser);

        // process by Program User
        return processIncentiveWithProgramUser(programUser, userEventQueue, program);
    }

    private Option<IncentiveEventPointDTO> processIncentiveWithProgramUser(ProgramUser programUser, UserEventQueue userEventQueue, Program program) {

        log.debug("->>> INCENTIVE: processIncentiveWithProgramUser");
        // save user event queue
        userEventQueue.setProgramUserId(programUser.getId());
        userEventQueueRepository.save(userEventQueue);

        //TODO: should process point in queue, tasks
        IncentiveEventPointDTO incentiveEventPointDTO = processPointByUserEvent(userEventQueue, program , programUser);

        return Option.of(incentiveEventPointDTO);
    }

    private IncentiveEventPointDTO processPointByUserEvent(UserEventQueue userEventQueue, Program program, ProgramUser programUser) {
        //calculate point and level
        BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program, applicationProperties);

        if(programUser.getSubgroupId() != null && (programUser.getSubgroupId().equals("undefined") || programUser.getSubgroupId().equals("0")))
            programUser.setSubgroupId("");

        log.debug("->>> INCENTIVE: processPointByUserEvent");

        IncentiveEventPointDTO incentiveEventPointDTO = userIncentiveBusinessRule.calculatePointAndLevelByCategoryPointAndSubCategoryPoint(userEventQueue, programUser, userEventQueue.getEventCategory(), totalUserPoint, programUser.getSubgroupId());

        return incentiveEventPointDTO;
    }


}
