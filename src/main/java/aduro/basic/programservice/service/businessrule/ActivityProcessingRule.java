package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.domain.ProgramSubCategoryPoint;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.UserEvent;
import aduro.basic.programservice.service.dto.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ActivityProcessingRule {

    APIResponse<ActivityUpdatePointResponseDto> fetchingEventProgressForOnceTimeTracking(ProgramUser programUser, CustomActivityDTO customActivity, long userId, SupportableActivityType type);

    UserDto getUserAMP(String participantId);

    /*
     * API Get Custom activity By Id
     *
     * */
     CustomActivityDTO getCustomActivity(long customActivityId);


    ProgramSubCategoryPoint getPointConfigurationByProgramIdAndSubCode(long programId, String activityType, String categoryCode);

    APIResponse<ProcessIncentiveActivityResponse> processIncentiveOnceTimeTracking(ProcessIncentiveActivityDto dto,
                                                                                   ProgramUser programUser,
                                                                                   List<UserEvent> userEvents, CustomActivityDTO customActivity, SupportableActivityType type);

    APIResponse<ProcessIncentiveActivityResponse> processIncentiveEmployerVerified(ProcessIncentiveActivityDto dto,
                                                                                   ProgramUser programUser,
                                                                                   List<UserEvent> userEvents,
                                                                                   UserDto userDto,
                                                                                   CustomActivityDTO customActivity, SupportableActivityType type) throws Exception;

    APIResponse<ActivityUpdatePointResponseDto> fetchingEventMultipleTracking(String participantId, long programId, SupportableActivityType type,  CustomActivityDTO customActivity, UserDto userAMP);

    APIResponse<ProcessIncentiveActivityResponse> processIncentiveMultipleTracing(ProcessIncentiveActivityDto dto,
                                                                                  ProgramUser programUser,
                                                                                  SupportableActivityType type,
                                                                                  CustomActivityDTO customActivity,
                                                                                  UserDto userAMP,
                                                                                  APIResponse<ProcessIncentiveActivityResponse> result) throws Exception;
}
