package aduro.basic.programservice.service.businessrule.validate;

import aduro.basic.programservice.service.dto.ActivityEventDTO;
import aduro.basic.programservice.service.dto.IncentiveEventDTO;
import aduro.basic.programservice.service.dto.ParticipantDTO;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class AduroIncentiveValidator {

    String EVENT_CODE_ERR = "Event Code can not null or empty.";
    String EVENT_ID_ERR = "Event Id can not null or empty.";
    String EVENT_DATE_ERR = "Event Date can not null or empty.";
    String CLIENT_ID_ERR = "Client Id can not null or empty.";
    String PARTICIPANT_ID_ERR = "Participant id can not null or empty.";

    public Validation<Seq<String>, IncentiveEventDTO> validEventDTO(String eventCode, String eventId, String clientId, Instant eventDate, String participantId) {
        return Validation.combine(
            validateField(eventCode, EVENT_CODE_ERR),
            validateField(eventId, EVENT_ID_ERR),
            validateField(clientId, CLIENT_ID_ERR),
            validateDate(eventDate, EVENT_DATE_ERR),
            validateField(participantId, PARTICIPANT_ID_ERR)
        )
            .ap(IncentiveEventDTO::new);
    }

    private Validation<String, String> validateField(String field, String error) {
        return (field != null && !field.isEmpty())
            ? Validation.valid(field)
            : Validation.invalid(error);
    }

    private Validation<String, Instant> validateDate(Instant field, String error) {
        return field != null
            ? Validation.valid(field)
            : Validation.invalid(error);
    }
}
