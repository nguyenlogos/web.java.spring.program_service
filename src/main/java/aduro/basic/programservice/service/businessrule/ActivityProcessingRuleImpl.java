package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.adp.domain.*;
import aduro.basic.programservice.adp.repository.*;
import aduro.basic.programservice.adp.service.AmpUserActivityProgressService;
import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.NotificationAdapter;
import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.ap.dto.NotificationItemRequest;
import aduro.basic.programservice.ap.dto.NotificationItemResponse;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.domain.enumeration.RecurrenceEnum;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.helpers.DateHelpers;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.service.ActivityEventQueueService;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.UserEventMapper;
import aduro.basic.programservice.service.util.JsonHelper;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.amazonaws.util.CollectionUtils.isNullOrEmpty;

@Service
@Transactional
public class ActivityProcessingRuleImpl implements ActivityProcessingRule {

    public static final String INCENTIVE_PROCESSING_ERR = "INCENTIVE_PROCESSING_ERR";

    public static final int PARTICIPANT_NOT_FOUND = 1003;
    public static final int EMPLOYER_NOT_FOUND = 1002;
    public static final int ACTIVITY_NOT_FOUND = 1004;
    public static final int INVALID_INCENTIVE_DATE = 1006;
    public static final int INVALID_INCENTIVE_DATE_FORMAT = 1007;
    public static final int PARTICIPANT_NOT_ACTIVATED = 1008;
    public static final String INCENTIVE_DATA = "Incentive_Data";
    private static final String ACTIVITY_GOT_POINT_AWARDED = "ACTIVITY_GOT_POINT_AWARDED";
    public static final String PROGRAM_NOT_CONFIGURED_POINT = "PROGRAM_NOT_CONFIGURED_POINT";
    public static final String MARKED_COMPLETION = "Marked Completion";


    private final Logger log = LoggerFactory.getLogger(ActivityProcessingRuleImpl.class);

    @Autowired
    ProgramRepository programRepository;

    @Autowired
    CustomActivityAdapter customActivityAdapter;

    @Autowired
    UserEventRepository userEventRepository;

    @Autowired
    UserEventMapper userEventMapper;

    @Autowired
    ProgramUserRepository programUserRepository;

    @Autowired
    ActivityIncentiveRuleImpl activityIncentiveRule;

    @Autowired
    AmpUserActivityProgressRepository ampUserActivityProgressRepository;

    @Autowired
    AmpUserActivityProgressService ampUserActivityProgressService;

    @Autowired
    AmpUserActivityProgressHistoryRepository ampUserActivityProgressHistoryRepository;

    @Autowired
    AmpUserActivityIncentiveRepository ampUserActivityIncentiveRepository;

    @Autowired
    JsonHelper jsonHelper;

    @Autowired
    ActivityEventQueueService activityEventQueueService;

    @Autowired
    RpDataTransactionErrorRepository transactionErrorRepository;

    @Autowired
    AdpProcessingErrorRepository adpProcessingErrorRepository;

    @Autowired
    RpDataTransactionRepository rpDataTransactionRepository;

    @Autowired
    ActivityEventQueueRepository activityEventQueueRepository;

    @Autowired
    AdpEmployeeProfileRepository employeeProfileRepository;

    @Autowired
    UserIncentiveBusinessRule userIncentiveBusinessRule;

    @Autowired
    UserEventQueueRepository userEventQueueRepository;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    NotificationAdapter notificationAdapter;

    /*
     * get missing point event for once time tracking, information, assessment, employer-verified
     *
     * */
    @Override
    public APIResponse<ActivityUpdatePointResponseDto> fetchingEventProgressForOnceTimeTracking(ProgramUser programUser,
                                                                                                CustomActivityDTO customActivity,
                                                                                                long userId, SupportableActivityType type) {

        APIResponse<ActivityUpdatePointResponseDto> result = new APIResponse<>();
        List<UserTrackingDataDto> userTrackingDataList = new ArrayList<>();
        ActivityUpdatePointResponseDto responseDto = ActivityUpdatePointResponseDto.builder()
            .build();
        List<AmpUserActivityProgress> progressUserCompleted = ampUserActivityProgressRepository.findByUserIdAndCustomActivityId(userId, customActivity.getId());
        List<UserEvent> userEvents = userEventRepository.findUserEventsByProgramUserIdAndEventId(programUser.getId(), String.valueOf(customActivity.getId())).collect(Collectors.toList());

        String categoryCode = type.compareTo(SupportableActivityType.OTHER_ASSESSMENT) == 0 ? Constants.ASSESSMENTS : Constants.ACTIVITIES;

        ProgramSubCategoryPoint programSubCategoryPoint = getPointConfigurationByProgramIdAndSubCode(programUser.getProgramId(), type.getValue(), categoryCode);

        // user not get point awarded;
        if (programSubCategoryPoint == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setResult(responseDto);
            result.getErrors().add("PROGRAM_NOT_CONFIGURED_POINT");
            result.setMessage("Program not configured  points for this type activity.");
            return result;
        }

        if (type.compareTo(SupportableActivityType.EMPLOYER_VERIFIED) == 0) {

            if (!userEvents.isEmpty()) {
                result.setHttpStatus(HttpStatus.OK);
                userTrackingDataList.add(
                    UserTrackingDataDto.builder()
                        .trackingDate(Instant.now())
                        .trackingUnit("Marked Completion")
                        .trackingValue(Long.valueOf(1))
                        .hasEarnedPoint(true)
                        .expectedPoint(userEvents.get(0).getEventPoint())
                        .build());

                responseDto.setActivityName(customActivity.getTitle());
                responseDto.setTrackingType(buildTrackingType(customActivity, type));
                responseDto.setTotalPotentialPoint(userEvents.get(0).getEventPoint());
                responseDto.setUserTrackingDataList(userTrackingDataList);
                result.setResult(responseDto);
                return result;
            }
        } else {

            if (progressUserCompleted.isEmpty()) {
                result.setHttpStatus(HttpStatus.OK);
                result.setResult(responseDto);
                result.setMessage("User Not Joined Yet.");
                return result;
            }


            if (progressUserCompleted.get(0).getCompletedAt() != null) {
                // user have completed and get incentive
                if (!userEvents.isEmpty()) {
                    result.setHttpStatus(HttpStatus.OK);
                    result.setMessage("User has earned the point awarded.");

                    userTrackingDataList.add(
                        UserTrackingDataDto.builder()
                            .trackingDate(Instant.now())
                            .trackingUnit("Marked Completion")
                            .trackingValue(Long.valueOf(1))
                            .hasEarnedPoint(true)
                            .expectedPoint(userEvents.get(0).getEventPoint())
                            .build());

                    responseDto.setActivityName(customActivity.getTitle());
                    responseDto.setTrackingType(buildTrackingType(customActivity, type));
                    responseDto.setTotalPotentialPoint(userEvents.get(0).getEventPoint());
                    responseDto.setUserTrackingDataList(userTrackingDataList);
                    result.setResult(responseDto);
                    return result;
                }
            }
        }

        // check completion cap
        long completionCap = userEventRepository.countAllByEventCodeAndProgramUserId(type.getValue(), programUser.getId());
        BigDecimal expectedPoint;
        boolean isMatchCompletionCap = false;
        if (completionCap >= programSubCategoryPoint.getCompletionsCap()) {
            isMatchCompletionCap = true;
        }

        expectedPoint = isMatchCompletionCap ? BigDecimal.valueOf(0) : programSubCategoryPoint.getValuePoint();

        userTrackingDataList.add(
            UserTrackingDataDto.builder()
                .trackingDate(Instant.now())
                .trackingUnit("Marked Completion")
                .trackingValue(Long.valueOf(1))
                .hasEarnedPoint(false)
                .expectedPoint(expectedPoint)
                .build());

        responseDto.setActivityName(customActivity.getTitle());
        responseDto.setTrackingType(buildTrackingType(customActivity, type));
        responseDto.setTotalPotentialPoint(expectedPoint);
        responseDto.setUserTrackingDataList(userTrackingDataList);

        result.setHttpStatus(HttpStatus.OK);
        result.setResult(responseDto);
        result.setMessage(isMatchCompletionCap ? "User matched completion cap." : "get missing tracking success.");
        return result;
    }

    private String buildTrackingType(CustomActivityDTO customActivity, SupportableActivityType type) {

        String completion = "Completion";
        String completions = "Completions";
        String unit = customActivity.getQuantityUnit();
        if (isTrackingCompletionType(customActivity)) {
            unit = customActivity.getQuantityCompletions() <= 1 ? completion : completions;
        }
        switch (type) {
            case OTHER_ASSESSMENT:
                return "Assessment Completion";
            case INFORMATION:
                return "Information Completion";
            case EMPLOYER_VERIFIED:
                return "Employer Verified Completion";
            case SELF_TRACKING:
                return String.format("Self-Tracking, %s %s", customActivity.getQuantityCompletions(), unit);
            case PASSIVE_TRACKING:
                return String.format("Passive, %s %s", customActivity.getQuantityCompletions(), unit);
        }
        return "";
    }

    /*
     *
     *  get user from AMP
     * */
    @Override
    public UserDto getUserAMP(String participantId) {
        try {
            UserDto user = customActivityAdapter.getUser(participantId);
            if (user == null) return null;
            return user;
        } catch (Exception e) {
            log.debug("can not get user by participantId");
            return null;
        }
    }

    /*
     * API Get Custom activity By Id
     *
     * */
    @Override
    public CustomActivityDTO getCustomActivity(long customActivityId) {
        try {
            CustomActivityDTO customActivityDTO = customActivityAdapter.getCustomActivityDetail(customActivityId);
            return customActivityDTO;
        } catch (Exception e) {
            log.debug("can not get activity by Id");
            return null;
        }
    }


    /*
     *
     *
     * get program sub category point
     * */
    @Override
    public ProgramSubCategoryPoint getPointConfigurationByProgramIdAndSubCode(long programId, String activityType, String categoryCode) {

        Program program = programRepository.getOne(programId);

        // get category point by categoryCode
        Optional<ProgramCategoryPoint> categoryPointOptional = program.getProgramCategoryPoints().stream()
            .filter(programCategoryPoint -> programCategoryPoint.getCategoryCode().equals(categoryCode))
            .findFirst();

        if (!categoryPointOptional.isPresent()) {
            return null;
        }

        ProgramCategoryPoint programCategoryPoint = categoryPointOptional.get();

        Optional<ProgramSubCategoryPoint> programSubCategoryPointOptional = programCategoryPoint.getProgramSubCategoryPoints().stream()
            .filter(programSubCategoryPoint -> programSubCategoryPoint.getCode().equalsIgnoreCase(activityType))
            .findFirst();

        return programSubCategoryPointOptional.orElse(null);

    }

    private String getActivityRecurrence (String recurrence) {
        String text = "";
        if (recurrence.contains(("daily"))) {
            text = "daily";
        } else if (recurrence.contains("weekly")) {
            text = "weekly";
        } else if (recurrence.contains("monthly")) {
            text = "monthly";
        } else if (recurrence.contains("once")) {
            text = "once";
        }
        return text;
    }

    @Override
    public APIResponse<ActivityUpdatePointResponseDto> fetchingEventMultipleTracking(String participantId, long programId, SupportableActivityType type, CustomActivityDTO customActivity, UserDto userAMP) {

        APIResponse<ActivityUpdatePointResponseDto> result = new APIResponse<>();

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(participantId, programId);
        if (!programUserOptional.isPresent()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Not Found Participant");
            result.getErrors().add("PARTICIPANT_NOT_FOUND");
            return result;
        }

        ProgramUser programUser = programUserOptional.get();

        if (customActivity.getRecurrence() == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Recurrence can not null");
            result.getErrors().add("Recurrence can not null");
            return result;
        }

        RecurrenceEnum recurrenceEnum = RecurrenceEnum.fromString(getActivityRecurrence(
            customActivity.getRecurrence().toString()
        ));

        List<AmpUserActivityProgressHistory> trackingHistories = ampUserActivityProgressHistoryRepository.findAllByUserIdAndCustomActivityId(userAMP.getUserId(), customActivity.getId());
        List<AmpUserActivityProgress> progressUserCompleted = ampUserActivityProgressRepository.findByUserIdAndCustomActivityId(userAMP.getUserId(), customActivity.getId());

        // user not completed
        if (progressUserCompleted.isEmpty()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("User Not Joined Yet.");
            result.getErrors().add("USER_NOT_JOINED_YET");
            return result;
        }

        ProgramSubCategoryPoint programSubCategoryPoint = getPointConfigurationByProgramIdAndSubCode(programUser.getProgramId(), type.getValue(), Constants.ACTIVITIES);

        // user not get point awarded;
        if (programSubCategoryPoint == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(PROGRAM_NOT_CONFIGURED_POINT);
            result.setMessage("Program not configured  points for this type activity.");
            return result;
        }

        AmpUserActivityProgress ampUserActivityProgress = progressUserCompleted.get(0);

        Instant joinedDate = ampUserActivityProgress.getCreatedAt();
        Instant today = Instant.now();

        // get range Date user tracking
        Date startOfRange = Date.from(joinedDate.plus(-14, ChronoUnit.DAYS));

        if (customActivity.getStartDate().isAfter(startOfRange.toInstant())) {
            startOfRange = Date.from(customActivity.getStartDate());
        }

        // participant tracking 48 hours and 1 day for loop
        Date endOfActivity = Date.from(customActivity.getEndDate().plus(2, ChronoUnit.DAYS));

        Date endOfRange = Date.from(today);

        if (today.isAfter(endOfActivity.toInstant())) {
            endOfRange = endOfActivity;
        }

        // endOfRange will be the last day in week
//        endOfRange = DateHelpers.getLastDayOfWeek(endOfRange);

        boolean isCompletionType = isTrackingCompletionType(customActivity);

        // check completion cap
        BigDecimal expectedPoint = getExpectedPoint(type, programUser, programSubCategoryPoint);

        switch (recurrenceEnum) {
            case ONCE:
                return getMissingPointForOnceTracking(type, result, programUser, customActivity, trackingHistories, progressUserCompleted.get(0), startOfRange, endOfRange, expectedPoint, isCompletionType);
            case WEEKLY:
                return getMissingPointForWeeklyTracking(type, result, programUser, customActivity, trackingHistories, startOfRange, endOfRange, expectedPoint, isCompletionType, userAMP);
            case DAILY:
                return getMissingPointForDailyTracking(type, result, programUser, customActivity, trackingHistories, startOfRange, endOfRange, expectedPoint, isCompletionType, userAMP);
            case MONTHLY:
                return getMissingPointForMonthTracking(type, result, programUser, customActivity, trackingHistories, startOfRange, endOfRange, expectedPoint, isCompletionType, userAMP);
        }
        return result;
    }

    private APIResponse<ActivityUpdatePointResponseDto> getMissingPointForMonthTracking(SupportableActivityType type,
                                                                                        APIResponse<ActivityUpdatePointResponseDto> result,
                                                                                        ProgramUser programUser,
                                                                                        CustomActivityDTO customActivity,
                                                                                        List<AmpUserActivityProgressHistory> activityProgressHistories, Date startOfRange, Date endOfRange,
                                                                                        BigDecimal expectedPoint, Boolean isCompletionType, UserDto userAMP) {
        //monthly get the last week
//        endOfRange = DateHelpers.getLastDayOfWeek(endOfRange);

        List<DataProgressInDurationDto> progressInWeeks = new ArrayList<>();

        DataProgressInDurationDto currentWeek = DataProgressInDurationDto.builder()
            .activityProgressHistories(new ArrayList<>())
            .build();


        for (Date date = startOfRange;
             DateHelpers.getStarOfDay(date).before(endOfRange);
             date = DateHelpers.getDateAfter(date, 1)) {

            for (int t = 0; t < activityProgressHistories.size(); t++) {
                AmpUserActivityProgressHistory historyAt = activityProgressHistories.get(t);

                if (historyAt != null) {
                    Instant trackingDateHistory = activityProgressHistories.get(t).getTrackingDate();
                    if (trackingDateHistory == null) {
                        continue;
                    }
                    Date startOfDateHistory = DateHelpers.getStarOfDay(Date.from(trackingDateHistory));
                    Date StartOfDay = DateHelpers.getStarOfDay(date);
                    if (DateUtils.isSameDay(startOfDateHistory, StartOfDay)) {
                        currentWeek.getActivityProgressHistories().add(activityProgressHistories.get(t));
                    }
                }
            }

            Date lastDayOfMonth = DateHelpers.getMonthEnd(date);

            Date today = Date.from(Instant.now());

            if (DateUtils.isSameDay(date, lastDayOfMonth) || DateUtils.isSameDay(date, today)) {

                Date startOfMonth = DateHelpers.getStarOfDay(DateHelpers.getMonthStart(date));

                Date endOfMonth = DateHelpers.getStarOfDay(DateHelpers.getMonthEnd(date));

                currentWeek.setStartDay(startOfMonth);

                currentWeek.setEndDay(endOfMonth);

                List<UserEvent> eventsInWeek = userEventRepository.findByProgramUserIdAndEventIdAndEventDateBetween(programUser.getId(), String.valueOf(customActivity.getId()), startOfMonth.toInstant(), endOfMonth.toInstant());

                List<AmpUserActivityProgressHistory> histories = ampUserActivityProgressHistoryRepository.findAllByUserIdAndCustomActivityIdAndTrackingDateBetween(userAMP.getUserId(), customActivity.getId(), startOfMonth.toInstant(), endOfMonth.toInstant());

                long trackingValueRemain = getTrackingValueRemain(RecurrenceEnum.MONTHLY, customActivity, isCompletionType, histories);

                UserTrackingDataDto dataDto;

                // activity histories not empty
                if (isCompletionType) {
                    dataDto = createTrackingDataDto(endOfMonth.toInstant(), trackingValueRemain,MARKED_COMPLETION, false, expectedPoint);
                    currentWeek.setIsCompletedRequired(false);
                    if (currentWeek.getActivityProgressHistories().size() >= customActivity.getQuantityCompletions() && !eventsInWeek.isEmpty()) {
                        dataDto = createTrackingDataDto(eventsInWeek.get(0).getEventDate(), trackingValueRemain, MARKED_COMPLETION, true, eventsInWeek.get(0).getEventPoint());
                        currentWeek.setIsCompletedRequired(true);
                    }

                } else {
                    //units diff completions
                    // if  endOfMonth is after today, tracking date is calculated from today. and the date is not tracked.
                    dataDto = createTrackingDataDto(endOfMonth.toInstant(), trackingValueRemain, customActivity.getQuantityUnit(), false, expectedPoint);
                    currentWeek.setIsCompletedRequired(false);
                    Integer sumTrackingValues = currentWeek.getActivityProgressHistories().stream().map(AmpUserActivityProgressHistory::getTrackingValue)
                        .reduce(0, (a, b) -> a + b);
                    if (sumTrackingValues >= customActivity.getQuantityCompletions() && !eventsInWeek.isEmpty()) {
                        dataDto = createTrackingDataDto(eventsInWeek.get(0).getEventDate(), trackingValueRemain,customActivity.getQuantityUnit(), true, eventsInWeek.get(0).getEventPoint());
                        currentWeek.setIsCompletedRequired(true);
                    }
                }

                currentWeek.setTrackingDataDto(dataDto);

                progressInWeeks.add(currentWeek);

                currentWeek = DataProgressInDurationDto.builder()
                    .activityProgressHistories(new ArrayList<>())
                    .build();
            }
        }

        ActivityUpdatePointResponseDto responseDto = ActivityUpdatePointResponseDto.builder().build();
        responseDto.setActivityName(customActivity.getTitle());
        responseDto.setTrackingType(buildTrackingType(customActivity, type));
        responseDto.setTotalPotentialPoint(expectedPoint);
        responseDto.setProgressInRanges(progressInWeeks);
        result.setHttpStatus(HttpStatus.OK);
        result.setResult(responseDto);
        result.setMessage("get missing Monthly tracking success.");
        return result;
    }

    private APIResponse<ActivityUpdatePointResponseDto> getMissingPointForDailyTracking(SupportableActivityType type,
                                                                                        APIResponse<ActivityUpdatePointResponseDto> result,
                                                                                        ProgramUser programUser,
                                                                                        CustomActivityDTO customActivity,
                                                                                        List<AmpUserActivityProgressHistory> activityProgressHistories, Date startOfRange, Date endOfRange,
                                                                                        BigDecimal expectedPoint, Boolean isCompletionType, UserDto userAMP) {
        List<DataProgressInDurationDto> progressInDays = new ArrayList<>();

        DataProgressInDurationDto currentWeek = DataProgressInDurationDto.builder()
            .activityProgressHistories(new ArrayList<>())
            .build();

        for (Date date = startOfRange; date.before(endOfRange); date = DateHelpers.getDateAfter(date, 1)) {

            for (int t = 0; t < activityProgressHistories.size(); t++) {
                AmpUserActivityProgressHistory historyAt = activityProgressHistories.get(t);

                if (historyAt != null) {
                    Instant trackingDateHistory = activityProgressHistories.get(t).getTrackingDate();
                    if (trackingDateHistory == null) {
                        continue;
                    }
                    Date startOfDateHistory = DateHelpers.getStarOfDay(Date.from(trackingDateHistory));
                    Date StartOfDay = DateHelpers.getStarOfDay(date);
                    if (DateUtils.isSameDay(startOfDateHistory, StartOfDay)) {
                        currentWeek.getActivityProgressHistories().add(activityProgressHistories.get(t));
                    }
                }
            }

            Date startOfDay = DateHelpers.getStarOfDay(date);

            Date endOfDay = DateHelpers.getEndOfDay(date);

            currentWeek.setStartDay(DateHelpers.getUTCDateFromSystemDate(startOfDay));

            currentWeek.setEndDay(DateHelpers.getUTCDateFromSystemDate(endOfDay));

            List<UserEvent> eventsInWeek = userEventRepository.findByProgramUserIdAndEventIdAndEventDateBetween(programUser.getId(), String.valueOf(customActivity.getId()), startOfDay.toInstant(), endOfDay.toInstant());

            UserTrackingDataDto dataDto;

            List<AmpUserActivityProgressHistory> histories = ampUserActivityProgressHistoryRepository.findAllByUserIdAndCustomActivityIdAndTrackingDateBetween(userAMP.getUserId(), customActivity.getId(), startOfDay.toInstant(), endOfDay.toInstant());

            long trackingValueRemain = getTrackingValueRemain(RecurrenceEnum.DAILY, customActivity, isCompletionType, histories);

            // activity histories not empty
            if (isCompletionType) {
                dataDto = createTrackingDataDto(startOfDay.toInstant(), trackingValueRemain, MARKED_COMPLETION, false, expectedPoint);
                currentWeek.setIsCompletedRequired(false);
                if (currentWeek.getActivityProgressHistories().size() >= customActivity.getQuantityCompletions() && !eventsInWeek.isEmpty()) {
                    dataDto = createTrackingDataDto(eventsInWeek.get(0).getEventDate(), trackingValueRemain, MARKED_COMPLETION, true, eventsInWeek.get(0).getEventPoint());
                    currentWeek.setIsCompletedRequired(true);
                }

            } else {
                //units diff completions
                dataDto = createTrackingDataDto(startOfDay.toInstant(), trackingValueRemain, customActivity.getQuantityUnit(), false, expectedPoint);
                Integer sumTrackingValues = currentWeek.getActivityProgressHistories().stream().map(AmpUserActivityProgressHistory::getTrackingValue)
                    .reduce(0, (a, b) -> a + b);
                currentWeek.setIsCompletedRequired(false);
                if (sumTrackingValues >= customActivity.getQuantityCompletions() && !eventsInWeek.isEmpty()) {
                    dataDto = createTrackingDataDto(eventsInWeek.get(0).getEventDate(), trackingValueRemain, customActivity.getQuantityUnit(), true, eventsInWeek.get(0).getEventPoint());
                    currentWeek.setIsCompletedRequired(true);
                }
            }

            currentWeek.setTrackingDataDto(dataDto);

            progressInDays.add(currentWeek);

            currentWeek = DataProgressInDurationDto.builder()
                .activityProgressHistories(new ArrayList<>())
                .build();
        }

        ActivityUpdatePointResponseDto responseDto = ActivityUpdatePointResponseDto.builder().build();
        responseDto.setActivityName(customActivity.getTitle());
        responseDto.setTrackingType(buildTrackingType(customActivity, type));
        responseDto.setTotalPotentialPoint(expectedPoint);
        responseDto.setProgressInRanges(progressInDays);
        result.setHttpStatus(HttpStatus.OK);
        result.setResult(responseDto);
        result.setMessage("get missing daily tracking success.");
        return result;

    }

    private APIResponse<ActivityUpdatePointResponseDto> getMissingPointForWeeklyTracking(SupportableActivityType type,
                                                                                         APIResponse<ActivityUpdatePointResponseDto> result,
                                                                                         ProgramUser programUser,
                                                                                         CustomActivityDTO customActivity,
                                                                                         List<AmpUserActivityProgressHistory> activityProgressHistories, Date startOfRange, Date endOfRange,
                                                                                         BigDecimal expectedPoint, Boolean isCompletionType, UserDto useAMP

    ) {

        ///weekly get the last week
       endOfRange = DateHelpers.getLastDayOfWeek(endOfRange);

        List<DataProgressInDurationDto> progressInWeeks = new ArrayList<>();

        DataProgressInDurationDto currentWeek = DataProgressInDurationDto.builder()
            .activityProgressHistories(new ArrayList<>())
            .build();

        for (Date date = startOfRange; date.before(endOfRange); date = DateHelpers.getDateAfter(date, 1)) {

            int dayOfWeek = DateHelpers.getDayOfWeek(date);

            for (int t = 0; t < activityProgressHistories.size(); t++) {
                AmpUserActivityProgressHistory historyAt = activityProgressHistories.get(t);

                if (historyAt != null) {
                    Instant trackingDateHistory = activityProgressHistories.get(t).getTrackingDate();
                    if (trackingDateHistory == null) {
                        continue;
                    }
                    Date startOfDateHistory = DateHelpers.getStarOfDay(Date.from(trackingDateHistory));
                    Date StartOfDay = DateHelpers.getStarOfDay(date);
                    if (DateUtils.isSameDay(startOfDateHistory, StartOfDay)) {
                        currentWeek.getActivityProgressHistories().add(activityProgressHistories.get(t));
                    }
                }

            }
            if ((dayOfWeek == DayOfWeek.SATURDAY.getValue())) {

                Date startOfWeek = DateHelpers.getStarOfDay(DateHelpers.getFirstDayOfWeekInUS(date));

                Date endOfWeek = DateHelpers.lastDayOfWeek(startOfWeek);

                currentWeek.setStartDay(startOfWeek);

                currentWeek.setEndDay(DateHelpers.getStarOfDay(endOfWeek));

                List<UserEvent> eventsInWeek = userEventRepository.findByProgramUserIdAndEventIdAndEventDateBetween(programUser.getId(), String.valueOf(customActivity.getId()), startOfWeek.toInstant(), endOfWeek.toInstant());

                List<AmpUserActivityProgressHistory> histories = ampUserActivityProgressHistoryRepository.findAllByUserIdAndCustomActivityIdAndTrackingDateBetween(useAMP.getUserId(), customActivity.getId(), startOfWeek.toInstant(), endOfWeek.toInstant());

                long trackingValueRemain = getTrackingValueRemain(RecurrenceEnum.WEEKLY, customActivity, isCompletionType, histories);

                // check completed weekly
                // activity histories null or empty
                UserTrackingDataDto dataDto;

                if (isCompletionType) {
                    currentWeek.setIsCompletedRequired(false);
                    dataDto = createTrackingDataDto(endOfWeek.toInstant(), trackingValueRemain,MARKED_COMPLETION, false, expectedPoint);
                    if (currentWeek.getActivityProgressHistories().size() >= customActivity.getQuantityCompletions() && !eventsInWeek.isEmpty()) {
                        dataDto = createTrackingDataDto(eventsInWeek.get(0).getEventDate(),trackingValueRemain, MARKED_COMPLETION, true, eventsInWeek.get(0).getEventPoint());
                        currentWeek.setIsCompletedRequired(true);
                    }


                } else {
                    //units diff completions
                    dataDto = createTrackingDataDto(endOfWeek.toInstant(), trackingValueRemain, customActivity.getQuantityUnit(), false, expectedPoint);
                    currentWeek.setIsCompletedRequired(false);
                    Integer sumTrackingValues = currentWeek.getActivityProgressHistories().stream().map(AmpUserActivityProgressHistory::getTrackingValue)
                        .reduce(0, (a, b) -> a + b);
                    if (sumTrackingValues >= customActivity.getQuantityCompletions() && !eventsInWeek.isEmpty()) {
                        dataDto = createTrackingDataDto(eventsInWeek.get(0).getEventDate(), trackingValueRemain,customActivity.getQuantityUnit(), true, eventsInWeek.get(0).getEventPoint());
                        currentWeek.setIsCompletedRequired(true);
                    }
                }

                currentWeek.setTrackingDataDto(dataDto);

                progressInWeeks.add(currentWeek);

                currentWeek = DataProgressInDurationDto.builder()
                    .activityProgressHistories(new ArrayList<>())
                    .build();
            }
        }

        ActivityUpdatePointResponseDto responseDto = ActivityUpdatePointResponseDto.builder().build();
        responseDto.setActivityName(customActivity.getTitle());
        responseDto.setTrackingType(buildTrackingType(customActivity, type));
        responseDto.setTotalPotentialPoint(expectedPoint);
        responseDto.setProgressInRanges(progressInWeeks);
        result.setHttpStatus(HttpStatus.OK);
        result.setResult(responseDto);
        result.setMessage("get missing weekly tracking success.");
        return result;
    }

    private BigDecimal getExpectedPoint(SupportableActivityType type, ProgramUser programUser, ProgramSubCategoryPoint programSubCategoryPoint) {
        long completionCap = userEventRepository.countAllByEventCodeAndProgramUserId(type.getValue(), programUser.getId());
        BigDecimal expectedPoint;

        boolean isMatchCompletionCap = false;
        if (completionCap >= programSubCategoryPoint.getCompletionsCap()) {
            isMatchCompletionCap = true;
        }

        expectedPoint = isMatchCompletionCap ? BigDecimal.valueOf(0) : programSubCategoryPoint.getValuePoint();
        return expectedPoint;
    }

    private boolean isTrackingCompletionType(CustomActivityDTO customActivity) {
        if (customActivity.getType().equalsIgnoreCase(SupportableActivityType.PASSIVE_TRACKING.name())) {
            return false;
        }
        return customActivity.getQuantityUnit() == null || customActivity.getQuantityUnit().equalsIgnoreCase("");
    }

    /*
     * Self-tracking and passive tracking
     * calculate missing point for once tracking
     *
     * */
    private APIResponse<ActivityUpdatePointResponseDto> getMissingPointForOnceTracking(SupportableActivityType type,
                                                                                       APIResponse<ActivityUpdatePointResponseDto> result,
                                                                                       ProgramUser programUser,
                                                                                       CustomActivityDTO customActivity,
                                                                                       List<AmpUserActivityProgressHistory> activityProgressHistories,
                                                                                       AmpUserActivityProgress onceActivityProgress, Date startOfRange, Date endOfRange,
                                                                                       BigDecimal expectedPoint, Boolean isCompletionType

    ) {

        List<UserEvent> userEvents = userEventRepository.findUserEventsByProgramUserIdAndEventId(programUser.getId(), String.valueOf(customActivity.getId())).collect(Collectors.toList());

        List<DataProgressInDurationDto> progressInWeeks = new ArrayList<>();

        ActivityUpdatePointResponseDto responseDto = ActivityUpdatePointResponseDto.builder().build();
        responseDto.setActivityName(customActivity.getTitle());
        responseDto.setTrackingType(buildTrackingType(customActivity, type));
        responseDto.setTotalPotentialPoint(expectedPoint);

        DataProgressInDurationDto dataProgressDto = DataProgressInDurationDto.builder()
            .activityProgressHistories(activityProgressHistories)
            .startDay(DateHelpers.getUTCDateFromSystemDate(startOfRange))
            .endDay(DateHelpers.getUTCDateFromSystemDate(endOfRange))
            .build();

        long trackingValueRemain = getTrackingValueRemain(RecurrenceEnum.ONCE, customActivity, isCompletionType, activityProgressHistories);


        // user have completed and hav point or not
        if (onceActivityProgress.getCompletedAt() != null) {

            if (!userEvents.isEmpty()) {
                result.setMessage("User got Point Awarded.");
                result.getErrors().add(ACTIVITY_GOT_POINT_AWARDED);

                UserTrackingDataDto dto = createTrackingDataDto(userEvents.get(0).getEventDate(), trackingValueRemain, isCompletionType ? MARKED_COMPLETION : customActivity.getQuantityUnit(), true, userEvents.get(0).getEventPoint());
                dataProgressDto.setTrackingDataDto(dto);
                dataProgressDto.setIsCompletedRequired(true);

            } else {

                // completed which not point
                UserTrackingDataDto dto = createTrackingDataDto(Instant.now(), trackingValueRemain, isCompletionType ? MARKED_COMPLETION : customActivity.getQuantityUnit(), false, expectedPoint);
                dataProgressDto.setTrackingDataDto(dto);
                dataProgressDto.setIsCompletedRequired(false);
                result.setMessage("User has not the point awarded yet.");

            }
            progressInWeeks.add(dataProgressDto);
            responseDto.setProgressInRanges(progressInWeeks);
            result.setResult(responseDto);
            result.setHttpStatus(HttpStatus.OK);
            return result;
        }



        // in progress
        UserTrackingDataDto dto = createTrackingDataDto(Instant.now(), trackingValueRemain, isCompletionType ? MARKED_COMPLETION : customActivity.getQuantityUnit(), false, expectedPoint);
        dataProgressDto.setTrackingDataDto(dto);
        dataProgressDto.setIsCompletedRequired(false);

        progressInWeeks.add(dataProgressDto);
        responseDto.setProgressInRanges(progressInWeeks);

        result.setHttpStatus(HttpStatus.OK);
        result.setResult(responseDto);
        result.setMessage("get missing once tracking success.");
        return result;
    }

    private long getTrackingValueRemain(RecurrenceEnum type, CustomActivityDTO customActivity, Boolean isCompletionType, List<AmpUserActivityProgressHistory> histories) {
        long trackingValue = 1;
        long total = 0;
        long remain = 0;
        switch (type) {
            case ONCE:
                trackingValue = customActivity.getQuantityCompletions();
                if (!histories.isEmpty()) {
                    total = histories.stream().map(AmpUserActivityProgressHistory::getTrackingValue)
                        .reduce(0, Integer::sum);
                }
                if (total == 0 || (total - trackingValue) < 0) {
                    remain =  Math.abs(total - trackingValue);
                }
                return remain;
            case DAILY:
                trackingValue = customActivity.getQuantityCompletions();
                total = histories.stream().map(AmpUserActivityProgressHistory::getTrackingValue)
                    .reduce(0, Integer::sum);
                if (total == 0 || (total - trackingValue) < 0) {
                    remain =  Math.abs(total - trackingValue);
                }
                return remain;

            case WEEKLY:
            case MONTHLY:
                trackingValue = customActivity.getQuantityCompletions();
                if (!histories.isEmpty()) {
                    total = histories.stream().map(AmpUserActivityProgressHistory::getTrackingValue)
                        .reduce(0, Integer::sum);
                }
                if (total == 0 || (total - trackingValue) < 0) {
                    remain =  Math.abs(total - trackingValue);
                }
                return remain;
        }

        return trackingValue;
    }

    private UserTrackingDataDto createTrackingDataDto(Instant eventDate, Long trackingValue, String trackingUnit, boolean earnedPoint, BigDecimal expectedPoint) {
        return UserTrackingDataDto.builder()
            .trackingDate(eventDate)
            .trackingUnit(trackingUnit)
            .trackingValue(trackingValue)
            .hasEarnedPoint(earnedPoint)
            .expectedPoint(expectedPoint)
            .build();
    }


    /*
     * process Incentive with the activity like information
     * APIResponse<ProcessIncentiveActivityResponse> success response with status 200
     * APIResponse<ProcessIncentiveActivityResponse> error response with status != 200
     * */
    @Override
    public APIResponse<ProcessIncentiveActivityResponse> processIncentiveOnceTimeTracking(ProcessIncentiveActivityDto dto,
                                                                                          ProgramUser programUser,
                                                                                          List<UserEvent> userEvents, CustomActivityDTO customActivity,
                                                                                          SupportableActivityType type) {

        APIResponse<ProcessIncentiveActivityResponse> result = new APIResponse<>();

        UserDto user = getUserAMP(dto.getParticipantId());

        //validate tracing date
        Instant trackingDate = validateTrackingDate(dto, customActivity);

        if (trackingDate == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage("The Activity have expired or not ready to start.");
            return result;
        }

        List<AmpUserActivityProgress> userActivityProgress = ampUserActivityProgressRepository.findByUserIdAndCustomActivityId(user.getUserId(), Long.parseLong(dto.getActivityId()));

        if (userActivityProgress.isEmpty()) {
            result.setMessage("Participant not joined yet this activity.");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            return result;
        }

        if (userActivityProgress.get(0).getCompletedAt() != null && userEvents.isEmpty()) {
            // in completed, not get incentive yet
            return processIncentiveActivityByType(dto, programUser, type, result, user);
        }

        // in progress, not get incentive yet
        if (userActivityProgress.get(0).getCompletedAt() == null && userEvents.isEmpty()) {
            return handleMarkCompleteActivityByType(programUser, dto, result, customActivity, type);
        }

        result.setHttpStatus(HttpStatus.OK);
        result.getErrors().add(ACTIVITY_GOT_POINT_AWARDED);
        result.setMessage("This user got point awarded.");
        return result;

    }

    /*
     *
     * Validate tracking date, if true, tracking != null
     * */
    private Instant validateTrackingDate(ProcessIncentiveActivityDto dto, CustomActivityDTO customActivityDto) {

        Instant trackingDate = dto.getEventDate();
        Instant allowEndDate = customActivityDto.getEndDate().plus(2, ChronoUnit.DAYS);
        if (trackingDate.isBefore(customActivityDto.getStartDate())) {
            return null;
        }
        if (trackingDate.isAfter(customActivityDto.getEndDate())) {
            if (trackingDate.isBefore(allowEndDate)) {
                return allowEndDate;
            }
            return null;
        }
        return trackingDate;
    }

    /*
    * TrackingDate is available or not
    *
    * */
    private Instant isDateAvailableInRangeOfActivity(CustomActivityDTO customActivityDto, Instant trackingDate) {

        Instant allowEndDate = customActivityDto.getEndDate().plus(2, ChronoUnit.DAYS);

        if (trackingDate.isBefore(customActivityDto.getStartDate())) {
            return null;
        }

        if (trackingDate.isAfter(Instant.now())) {
            return null;
        }

        if (trackingDate.isAfter(customActivityDto.getEndDate())) {
            if (trackingDate.isBefore(allowEndDate)) {
                return allowEndDate;
            }
            return null;
        }


        return trackingDate;
    }


    /*
     * Handle mark completed before send incentive.
     *
     * */
    @Transactional
    public APIResponse<ProcessIncentiveActivityResponse> handleMarkCompleteActivityByType(ProgramUser programUser,
                                                                                          ProcessIncentiveActivityDto dto,
                                                                                          APIResponse<ProcessIncentiveActivityResponse> result,
                                                                                          CustomActivityDTO customActivity, SupportableActivityType type) {
        // validate condition
        Instant trackingDate = validateTrackingDate(dto, customActivity);
        if (trackingDate == null) {
            result.setMessage("Tracking Date is not valid.");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            return result;
        }

        UserDto user = getUserAMP(dto.getParticipantId());

        if (user == null) {
            result.setMessage("Participant can not found from AMP.");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            return result;
        }

        Instant startOfDay = DateHelpers.getStarOfDay(Date.from(trackingDate)).toInstant();
        Instant endOfDay = DateHelpers.getEndOfDay(Date.from(trackingDate)).toInstant();

        // once time tracking can not have in histories table.
        List<AmpUserActivityProgressHistory> progressHistories = ampUserActivityProgressHistoryRepository.findByUserIdAndCustomActivityIdAndTrackingDateBetween(user.getUserId(), Long.parseLong(dto.getActivityId()), trackingDate, startOfDay, endOfDay);
        if (!progressHistories.isEmpty()) {
            result.setMessage("Tracking date existed in histories progress.");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            return result;
        }

        List<AmpUserActivityProgress> userActivityProgress = ampUserActivityProgressRepository.findByUserIdAndCustomActivityId(user.getUserId(), Long.parseLong(dto.getActivityId()));
        if (userActivityProgress.isEmpty()) {
            result.setMessage("Tracking Progress not existed. participant maybe not joined yet.");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            return result;
        }

        // get mark completion for information activity, assessment..
        AmpUserActivityProgress ampUserActivityProgress = userActivityProgress.get(0);
        ampUserActivityProgress.setCompletedAt(trackingDate);
        ampUserActivityProgressRepository.save(ampUserActivityProgress);

        return processIncentiveActivityByType(dto, programUser, type, result, user);

    }


    /*
     *
     * handle processing incentive if activity has completed.
     * */

    @Transactional
    public APIResponse<ProcessIncentiveActivityResponse> processIncentiveActivityByType(ProcessIncentiveActivityDto dto,
                                                                                        ProgramUser programUser,
                                                                                        SupportableActivityType activityType,
                                                                                        APIResponse<ProcessIncentiveActivityResponse> result,
                                                                                        UserDto userDto) {
        // support information and assessment
        ParticipantDTO participantDTO = new ParticipantDTO();
        participantDTO.setEmail(programUser.getEmail());
        participantDTO.setFirstName(programUser.getFirstName());
        participantDTO.setLastName(programUser.getLastName());
        participantDTO.setParticipantId(dto.getParticipantId());
        participantDTO.setSubgroupId(programUser.getSubgroupId());
        participantDTO.setSubgroupName(programUser.getSubgroupName());
        List<ParticipantDTO> participants = new ArrayList<>();
        participants.add(participantDTO);

        String categoryCode = activityType.compareTo(SupportableActivityType.OTHER_ASSESSMENT) == 0 ? Constants.ASSESSMENTS : Constants.ACTIVITIES;

        ActivityEventDTO activityEventDTO = ActivityEventDTO.builder()
            .clientId(dto.getClientId())
            .participants(participants)
            .eventCategory(categoryCode)
            .eventCode(activityType.toString())
            .clientName(programUser.getClientName())
            .eventDate(dto.getEventDate())
            .eventId(dto.getActivityId())
            .createdByAdmin(dto.getCreatedBy())
            .build();

        Optional<IncentiveEventPointDTO> incentiveDto = activityIncentiveRule.processIncentive(activityEventDTO);
        if (!incentiveDto.isPresent()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage("Incentive processing has error.");
            return result;
        }

        String jsonResponse = jsonHelper.toJsonString(incentiveDto.get());

        AmpUserActivityIncentive ampUserActivityIncentive = AmpUserActivityIncentive.builder()
            .createdAt(Instant.now())
            .targetId(String.valueOf(userDto.getUserId()))
            .customActivityId(Long.valueOf(activityEventDTO.getEventId()))
            .status("OK")
            .payload(jsonHelper.toJsonString(activityEventDTO))
            .response(jsonResponse)
            .targetType(Constants.USER_TYPE)
            .updatedAt(Instant.now())
            .trackingDate(dto.getEventDate())
            .build();

        ampUserActivityIncentiveRepository.save(ampUserActivityIncentive);

        ProcessIncentiveActivityResponse response = ProcessIncentiveActivityResponse.builder()
            .earnLevel(incentiveDto.get().getCurrentLevel())
            .earnedPoint(incentiveDto.get().getPoint())
            .totalPoint(incentiveDto.get().getTotalPoint())
            .rewards(incentiveDto.get().getRewards())
            .build();

        result.setHttpStatus(HttpStatus.OK);
        result.setResult(response);
        result.setMessage("Incentive has processed successfully.");
        return result;
    }

    private List<AdpProcessingError> getProcessingErrors(int code) {
        return adpProcessingErrorRepository.findAllByDataType(INCENTIVE_DATA).stream()
            .filter(p -> p.getCode() == code).collect(Collectors.toList());
    }

    @Override
    @Transactional
    public APIResponse<ProcessIncentiveActivityResponse> processIncentiveEmployerVerified(ProcessIncentiveActivityDto dto,
                                                                                          ProgramUser programUser,
                                                                                          List<UserEvent> userEvents,
                                                                                          UserDto userDto,
                                                                                          CustomActivityDTO customActivity,
                                                                                          SupportableActivityType type) throws Exception {


        APIResponse<ProcessIncentiveActivityResponse> result = new APIResponse<>();

        ///validate type of employer verified
        if (type.compareTo(SupportableActivityType.EMPLOYER_VERIFIED) != 0) {

            // saving transaction error
            RpDataTransactionError error = RpDataTransactionError.builder()
                .errorCode(ACTIVITY_NOT_FOUND)
                .message("This type activity has not supported yet.")
                .clientAdminId(dto.getCreatedBy())
                .clientId(dto.getClientId())
                .createdDate(Instant.now())
                .dataType(INCENTIVE_DATA)
                .field("id")
                .value(String.valueOf(customActivity.getId()))
                .build();

            transactionErrorRepository.save(error);

            result.setMessage("This type activity has not supported yet.");
            result.getErrors().add("TYPE_ACTIVITY_NOT_SUPPORT");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }

        //validate participant;
        Optional<AdpEmployeeProfile> participantOptional = employeeProfileRepository.findParticipantById(dto.getParticipantId());
        if (!participantOptional.isPresent()) {
            result.setMessage("Participant is not found.");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(Constants.ERR_USER_NOT_FOUND);
            return result;
        }

        AdpEmployeeProfile profile = participantOptional.get();
        if (profile.getAccountCreated() != 1) {
            result.setMessage("Participant has not created yet.");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(Constants.ERR_USER_NOT_FOUND);
            return result;
        }


        //validate completed activity
        List<AmpUserActivityProgress> userActivityProgress = ampUserActivityProgressRepository.findByUserIdAndCustomActivityId(userDto.getUserId(), Long.parseLong(dto.getActivityId()));

        if (!userActivityProgress.isEmpty() && userActivityProgress.get(0).getCompletedAt() != null) {

            // completed and got point awarded
            if (!userEvents.isEmpty()) {
                result.setMessage("Participant have completed this activity.");
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                result.getErrors().add(INCENTIVE_PROCESSING_ERR);
                return result;
            }

        }

        // completed and check configuration point
        ProgramSubCategoryPoint programSubCategoryPoint = getPointConfigurationByProgramIdAndSubCode(programUser.getProgramId(), type.getValue(), Constants.ACTIVITIES);
        if (programSubCategoryPoint == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Program  is not configured  points for this type activity.");
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            return result;
        }

        //completed and missing point or not do anything yet.
        String activityCode = customActivity.getActivityCode();

        //validate program
        Optional<Program> program = programRepository.findById(dto.getProgramId());
        if (!program.isPresent()) {
            result.setMessage("Employer Verified is not valid.");
            result.getErrors().add("EMPLOYER_NOT_VALID");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }

        // currently support program active
        if (!program.get().getStatus().equalsIgnoreCase(ProgramStatus.Active.name())) {
            result.setMessage("Program Not Active.");
            result.getErrors().add("PROGRAM_NOT_VALID");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }

        //validate eventDate
        Instant programStartDate = program.get().getStartDate();
        Instant programEndDate = program.get().getResetDate();
        if (dto.getEventDate().isBefore(programStartDate) || dto.getEventDate().isAfter(programEndDate)) {
            // saving transaction error
            List<AdpProcessingError> processingErrors = getProcessingErrors(INVALID_INCENTIVE_DATE);
            AdpProcessingError processingError = processingErrors.isEmpty() ? processingErrors.get(0) : AdpProcessingError.builder()
                .code(9000)
                .name("Internal error.")
                .build();

            RpDataTransactionError error = RpDataTransactionError.builder()
                .errorCode(processingError.getCode())
                .message(processingError.getName())
                .clientAdminId(dto.getCreatedBy())
                .clientId(dto.getClientId())
                .createdDate(Instant.now())
                .dataType("Incentive_Data")
                .field("createdDate")
                .value(String.valueOf(customActivity.getId()))
                .build();

            transactionErrorRepository.save(error);
        }

        // validate activity tracking date in range
        Instant trackingDate = validateTrackingDate(dto, customActivity);
        if (trackingDate == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage("The Activity have expired.");
            return result;
        }
        /// verify data
        CheckEmployerVerifiedDTO verifiedDTO = new CheckEmployerVerifiedDTO();
        verifiedDTO.setClientId(dto.getClientId());
        verifiedDTO.setActivityCode(activityCode);

        CheckEmployerVerifiedDTO checkVerifiedDto = activityEventQueueService.validateEmployerVierified(verifiedDTO);
        if (!checkVerifiedDto.isValid()) {
            result.setMessage("Employer Verified is not valid.");
            result.getErrors().add("EMPLOYER_VERIFIED_NOT_VALID");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }

        // create transaction
        RpDataTransaction transaction = RpDataTransaction.builder()
            .clientAdminId(dto.getCreatedBy())
            .clientFtpId(null)
            .clientId(dto.getClientId())
            .createdAt(Instant.now())
            .dataType(RpDataTransaction.DataType.incentive_data)
            .dataVendorId(null)
            .deletedAt(null)
            .encryptedFileId(null)
            .endDate(Instant.now())
            .errorRowCount(0)
            .errorsCount(0)
            .type("inbound")
            .method("web")
            .rowCount(0)
            .processRowCount(0)
            .successRowCount(0)
            .errorRowCount(0)
            .rowChangedCount(0)
            .rowInsertedCount(0)
            .errorRowCount(0)
            .tboCount(0)
            .tboRemoveCount(0)
            .rehiredCount(0)
            .terminatedCount(0)
            .exportedChangesToLimeade(false)
            .isCompleted(true)
            .isEncrypted(false)
            .isTest(false)
            .exportedToLimeade(false)
            .exportedToEgnyte(false)
            .retryCount(0)
            .build();

        transaction = rpDataTransactionRepository.saveAndFlush(transaction);

        // create Activity event queue
        Instant today = Instant.now();
        ActivityEventQueue activityEventDto = new ActivityEventQueue();
        activityEventDto.setActivityCode(activityCode);
        activityEventDto.setClientId(dto.getClientId());
        activityEventDto.setCreatedDate(today);
        activityEventDto.setEmail(userDto.getEmail());
        activityEventDto.setExecuteStatus(IncentiveStatusEnum.New.name());
        activityEventDto.setFirstName(profile.getFirstName());
        activityEventDto.setLastName(profile.getLastName());
        activityEventDto.setSubgroupId(userDto.getSubgroupId());
        activityEventDto.setParticipantId(dto.getParticipantId());
        activityEventDto.setTransactionId(transaction.getId());
        // processing incentive
        activityEventQueueRepository.saveAndFlush(activityEventDto);

        // create user event queue
        UserEventQueue userEventQueue = createUserEventQueue(Instant.now(), dto.getActivityId(), SupportableActivityType.EMPLOYER_VERIFIED.name(), program.get().getId(), programUser.getId());
        userEventQueue.setCreatedByAdmin(dto.getCreatedBy());
        userEventQueueRepository.save(userEventQueue);

        //calculate point and level
        BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program.get(), applicationProperties);

        if (programUser.getSubgroupId() != null && (programUser.getSubgroupId().equals("undefined") || programUser.getSubgroupId().equals("0")))
            programUser.setSubgroupId("");

        IncentiveEventPointDTO incentiveEventPointDTO = userIncentiveBusinessRule.calculatePointAndLevelByCategoryPointAndSubCategoryPoint(userEventQueue, programUser, Constants.ACTIVITIES, totalUserPoint, programUser.getSubgroupId());

        ProcessIncentiveActivityResponse response = ProcessIncentiveActivityResponse.builder()
            .earnedPoint(incentiveEventPointDTO.getPoint())
            .rewards(incentiveEventPointDTO.getRewards())
            .totalPoint(incentiveEventPointDTO.getTotalPoint()).build();


        // update progress
        if (!userActivityProgress.isEmpty()) {
            AmpUserActivityProgress progress = userActivityProgress.get(0);
            progress.setCompletedAt(Instant.now());
            ampUserActivityProgressRepository.save(progress);
        } else {
            AmpUserActivityProgress ampUserActivityProgress = new AmpUserActivityProgress();
            ampUserActivityProgress.setCompletedAt(Instant.now());
            ampUserActivityProgress.setCreatedAt(Instant.now());
            ampUserActivityProgress.setType(type.getValue());
            ampUserActivityProgress.setUpdatedAt(Instant.now());
            ampUserActivityProgress.setUserId(userDto.getUserId());
            ampUserActivityProgress.setCustomActivityId(customActivity.getId());
            ampUserActivityProgressRepository.save(ampUserActivityProgress);
        }

        //notify keep old logic
        NotificationItemRequest notificationItemRequest = new NotificationItemRequest();
        notificationItemRequest.setRewardContent("Congratulations!You received points for completing an activity within your Aduro Wellness program");
        notificationItemRequest.setParticipantId(activityEventDto.getParticipantId());
        notificationItemRequest.setTitle("Points Earned!\uD83C\uDF89");
        notificationItemRequest.setNotificationType("EMPLOYER_VERIFIED");
        CompletableFuture<NotificationItemResponse> orderResponseCompletableFuture = notificationAdapter.AddNotificationItem(notificationItemRequest);

        // update activity queue
        activityEventDto.setExecuteStatus(IncentiveStatusEnum.Completed.toString());
        activityEventQueueRepository.saveAndFlush(activityEventDto);

        result.setResult(response);
        result.setMessage("The activity has earned the point awarded.");
        result.setHttpStatus(HttpStatus.OK);

        return result;
    }


    @Override
    @Transactional
    public APIResponse<ProcessIncentiveActivityResponse> processIncentiveMultipleTracing(ProcessIncentiveActivityDto dto,
                                                                                         ProgramUser programUser,
                                                                                         SupportableActivityType type,
                                                                                         CustomActivityDTO customActivity,
                                                                                         UserDto userAMP,
                                                                                         APIResponse<ProcessIncentiveActivityResponse> result) throws Exception {

        Optional<Program> programOptional = programRepository.findById(dto.getProgramId());
        if (!programOptional.isPresent()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage("Program is not found.");
            return result;
        }

        Program program = programOptional.get();

        if (dto.getDataProgress() == null){
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage("Tracking data is not existed.");
            return result;
        }

        DataProgressInDurationDto dataProgress = dto.getDataProgress();
        if (dataProgress == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage("DateProgress is required.");
            return result;
        }

        List<UserEvent> userEvents = userEventRepository.findByProgramUserIdAndEventIdAndEventDateBetween(programUser.getId(), dto.getActivityId(), dataProgress.getStartDay().toInstant(), dataProgress.getEndDay().toInstant());

        if (!userEvents.isEmpty()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage("User had the point awarded.");
            return result;
        }

        ProgramSubCategoryPoint programSubCategoryPoint = getPointConfigurationByProgramIdAndSubCode(programUser.getProgramId(), type.getValue(), Constants.ACTIVITIES);

        // user not get point awarded;
        if (programSubCategoryPoint == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(PROGRAM_NOT_CONFIGURED_POINT);
            result.setMessage("Program not configured  points for this type activity.");
            return result;
        }

        //mark completion
        if (customActivity.getRecurrence() == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Recurrence can not null");
            result.getErrors().add("Recurrence can not null");
            return result;
        }

        RecurrenceEnum recurrenceEnum = RecurrenceEnum.fromString(getActivityRecurrence(
            customActivity.getRecurrence().toString()
        ));

        if (recurrenceEnum == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("Recurrence can not null");
            result.getErrors().add("Recurrence can not null");
            return result;
        }

        List<AmpUserActivityProgress> joinedProgress = ampUserActivityProgressRepository.findByUserIdAndCustomActivityId(userAMP.getUserId(), Long.valueOf(dto.getActivityId()));

        if (joinedProgress.isEmpty()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.setMessage("User Not Joined Yet.");
            result.getErrors().add("USER_NOT_JOINED_YET");
            return result;
        }

        List<UserEvent> userEventList = userEventRepository.findByProgramUserIdAndEventIdAndEventDateBetween(programUser.getId(), dto.getActivityId(), dataProgress.getStartDay().toInstant(), dataProgress.getEndDay().toInstant());
        if (!userEventList.isEmpty()) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage(String.format("user had received point in range %s - %s", dataProgress.getStartDay(), dataProgress.getEndDay()));
            return result;
        }

        AmpUserActivityProgress userActivityProgress = joinedProgress.get(0);

        boolean isCompletionType = isTrackingCompletionType(customActivity);

        boolean isCompletedActivity = dataProgress.getIsCompletedRequired();

        List<Date> dateProgressHistories = new ArrayList<>();

        if (dataProgress.getActivityProgressHistories() != null && !dataProgress.getActivityProgressHistories().isEmpty()) {
            dateProgressHistories = dataProgress.getActivityProgressHistories().stream()
                .map(a -> Date.from(a.getTrackingDate())).collect(Collectors.toList());
        }

        UserTrackingDataDto trackingDataDto = dataProgress.getTrackingDataDto();
        if (trackingDataDto == null) {
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            result.getErrors().add(INCENTIVE_PROCESSING_ERR);
            result.setMessage(String.format("Tracking Data is not null."));
            return result;
        }

        Long remainCompletions = trackingDataDto.getTrackingValue();

        List<AmpUserActivityProgressHistory> needToProcess = new ArrayList<>();

        UserEventQueue userEventQueue = new UserEventQueue();

        switch (recurrenceEnum) {
            case ONCE:

                //mark complete
                log.info("admin mark complete once activity.");

                // not completed then mark completed
                if (!isCompletedActivity && userActivityProgress.getCompletedAt()== null) {
                    userActivityProgress.setCompletedAt(dto.getEventDate());
                    ampUserActivityProgressRepository.save(userActivityProgress);

                }

                //remain completions is = 0 not do anything
                if (remainCompletions == 0) {
                    userEventQueue = createUserEventQueue(trackingDataDto.getTrackingDate(), dto.getActivityId(), type.getValue(), dto.getProgramId(), programUser.getId());
                    break;
                }

                // get history for once
                Instant trackingDate = trackingDataDto.getTrackingDate();
                List<Instant> trackingAvailableDateList = new ArrayList<>();

                if (isCompletionType) {
                    // find the tracking date not have data history for n completions
                    int count = 0;
                    for(Date date = Date.from(Instant.now()); date.after(DateHelpers.getUTCDateFromSystemDate(dataProgress.getStartDay())); date = DateHelpers.findPrevDay(date)) {
                        if (!isDateInRange(dateProgressHistories, date)) {
                            trackingAvailableDateList.add(date.toInstant());
                            count++;
                        }

                        if (count >= remainCompletions) {
                            break;
                        }
                    }

                    if (trackingAvailableDateList.size() < remainCompletions) {
                        result.setHttpStatus(HttpStatus.BAD_REQUEST);
                        result.getErrors().add(INCENTIVE_PROCESSING_ERR);
                        result.setMessage(String.format("Can not force to update tracking history for Activity, because the number of available date is less more than remain completions %s", trackingDataDto.getTrackingValue()));
                        return result;
                    }

                    for (Instant trackingAvailableDate : trackingAvailableDateList) {
                        // once completion type and not type of completion
                        AmpUserActivityProgressHistory onceHistory = AmpUserActivityProgressHistory.builder()
                            .userId(userAMP.getUserId())
                            .updatedAt(Instant.now())
                            .createdAt(Instant.now())
                            .trackingMethod("manual")
                            .trackingValue(1)
                            .customActivityId(customActivity.getId())
                            .activityProgressId(userActivityProgress.getId())
                            .trackingUnit(null)
                            .trackingDate(trackingAvailableDate)
                            .trackingCompleted(true)
                            .build();
                        needToProcess.add(onceHistory);
                    }

                } else {
                    // find for one day available
                    final Date availableDate = Date.from(trackingDataDto.getTrackingDate());

                    for(Date date = Date.from(Instant.now());
                        date.after(dataProgress.getStartDay());
                        date = DateHelpers.findPrevDay(date)) {

                        Instant verifiedDate = isDateAvailableInRangeOfActivity(customActivity, date.toInstant());

                        if (verifiedDate != null) {
                            Optional<AmpUserActivityProgressHistory> history = dataProgress.getActivityProgressHistories().stream()
                                .filter(d -> Date.from(d.getTrackingDate()).equals(availableDate)).findFirst();

                            if (history.isPresent()) {
                                AmpUserActivityProgressHistory progressHistory = history.get();
                                Integer trackingValue = progressHistory.getTrackingValue();
                                progressHistory.setTrackingValue(Math.toIntExact(trackingValue + trackingDataDto.getTrackingValue()));
                                needToProcess.add(progressHistory);

                            } else {
                                // once completion type and not type of completion
                                AmpUserActivityProgressHistory onceHistory = AmpUserActivityProgressHistory.builder()
                                    .userId(userAMP.getUserId())
                                    .updatedAt(Instant.now())
                                    .createdAt(Instant.now())
                                    .trackingMethod("manual")
                                    .trackingValue(isCompletionType ? 1 : Math.toIntExact(remainCompletions))
                                    .customActivityId(customActivity.getId())
                                    .activityProgressId(userActivityProgress.getId())
                                    .trackingUnit(dataProgress.getTrackingDataDto().getTrackingUnit())
                                    .trackingDate(verifiedDate)
                                    .trackingCompleted(true)
                                    .build();
                                needToProcess.add(onceHistory);
                            }
                            break;
                        }

                    }
                }

                // create queue
                userEventQueue = createUserEventQueue(Instant.now(), dto.getActivityId(), type.getValue(), dto.getProgramId(), programUser.getId());
                break;


            case WEEKLY:
            case DAILY:
            case MONTHLY:
                //mark complete
                log.info("admin mark complete weekly activity.");

                List<Instant> availableDates = new ArrayList<>();

                Instant trackingDateWeekly = trackingDataDto.getTrackingDate();

                //remain completions is = 0 not do anything
                if (remainCompletions == 0) {
                    userEventQueue = createUserEventQueue(dateProgressHistories.isEmpty() ? Instant.now() : dateProgressHistories.get(0).toInstant(), dto.getActivityId(), type.getValue(), dto.getProgramId(), programUser.getId());
                    break;
                }

                if (isCompletionType) {
                    int count = 0;
                    int numberDays = getNumberAvailableDaysIfActivityIsCompletion(recurrenceEnum, DateHelpers.getUTCDateFromSystemDate(dataProgress.getEndDay())); //AMP 1 week can not mark complete over 7 time.

                    for(Date date = dataProgress.getEndDay();
                        DateHelpers.getEndOfDay(date).after(dataProgress.getStartDay());
                        date = DateHelpers.findPrevDay(date)) {
                        if (count < remainCompletions && count < numberDays && !isDateInRange(dateProgressHistories, date)) {
                            Instant verifiedDate = isDateAvailableInRangeOfActivity(customActivity, date.toInstant());
                            if (verifiedDate != null) {
                                availableDates.add(DateHelpers.getStarOfDay(date).toInstant());
                                count++;
                            }
                        }
                        if (count >= remainCompletions) {
                            break;
                        }
                    }

                    if (availableDates.size() < remainCompletions) {
                        result.setHttpStatus(HttpStatus.BAD_REQUEST);
                        result.getErrors().add(INCENTIVE_PROCESSING_ERR);
                        result.setMessage(String.format("Can not force to update tracking history for Activity, because the number of available date is less than remain completions %s", trackingDataDto.getTrackingValue()));
                        return result;
                    }

                    //create history with available dates
                    for (Instant availableDate : availableDates) {
                         AmpUserActivityProgressHistory history = AmpUserActivityProgressHistory.builder()
                                .userId(userAMP.getUserId())
                                .updatedAt(Instant.now())
                                .createdAt(Instant.now())
                                .trackingMethod("manual")
                                .trackingValue(1)
                                .customActivityId(customActivity.getId())
                                .activityProgressId(userActivityProgress.getId())
                                .trackingUnit(dataProgress.getTrackingDataDto().getTrackingUnit())
                                .trackingDate(availableDate)
                                .trackingCompleted(true)
                                .build();
                            needToProcess.add(history);
                    }

                } else {

                    // find for one day available

                    final Date availableDate = Date.from(trackingDateWeekly);

                    for(Date date = DateHelpers.getUTCDateFromSystemDate(dataProgress.getEndDay());
                        date.after(DateHelpers.getUTCDateFromSystemDate(dataProgress.getStartDay()));
                        date = DateHelpers.findPrevDay(date)) {

                        Instant verifiedDate = isDateAvailableInRangeOfActivity(customActivity, date.toInstant());

                        if (verifiedDate != null) {

                            trackingDateWeekly = DateHelpers.getStarOfDay(date).toInstant();

                            Optional<AmpUserActivityProgressHistory> history = dataProgress.getActivityProgressHistories().stream()
                                .filter(d -> Date.from(d.getTrackingDate()).equals(availableDate)).findFirst();

                            if (history.isPresent()) {

                                AmpUserActivityProgressHistory progressHistory = history.get();
                                Integer trackingValue = progressHistory.getTrackingValue();
                                progressHistory.setTrackingValue(Math.toIntExact(trackingValue + trackingDataDto.getTrackingValue()));
                                needToProcess.add(progressHistory);

                            } else {

                                AmpUserActivityProgressHistory weeklyHistory = AmpUserActivityProgressHistory.builder()
                                    .userId(userAMP.getUserId())
                                    .updatedAt(Instant.now())
                                    .createdAt(Instant.now())
                                    .trackingMethod("manual")
                                    .trackingValue(Math.toIntExact(remainCompletions))
                                    .customActivityId(customActivity.getId())
                                    .activityProgressId(userActivityProgress.getId())
                                    .trackingUnit(dataProgress.getTrackingDataDto().getTrackingUnit())
                                    .trackingDate(trackingDateWeekly)
                                    .trackingCompleted(true)
                                    .build();
                                needToProcess.add(weeklyHistory);
                            }
                            break;
                        }

                    }

                }

                if(remainCompletions > 0) {
                    userEventQueue = createUserEventQueue(needToProcess.isEmpty() ? Instant.now() : needToProcess.get(needToProcess.size() - 1).getTrackingDate(), dto.getActivityId(), type.getValue(), dto.getProgramId(), programUser.getId());
                }

                break;
        }

        log.info("System is marking history tracking for user.");



        if (!needToProcess.isEmpty()) {
            ampUserActivityProgressHistoryRepository.saveAll(needToProcess);
        }

        userEventQueue.setCreatedByAdmin(dto.getCreatedBy());

        //calculate point and level
        BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program, applicationProperties);

        if (programUser.getSubgroupId() != null && (programUser.getSubgroupId().equals("undefined") || programUser.getSubgroupId().equals("0")))
            programUser.setSubgroupId("");

        IncentiveEventPointDTO incentiveEventPointDTO = userIncentiveBusinessRule.calculatePointAndLevelByCategoryPointAndSubCategoryPoint(userEventQueue, programUser, Constants.ACTIVITIES, totalUserPoint, programUser.getSubgroupId());

        if (incentiveEventPointDTO != null) {
            AmpUserActivityIncentive ampUserActivityIncentive = AmpUserActivityIncentive.builder()
                .createdAt(Instant.now())
                .targetId(String.valueOf(userAMP.getUserId()))
                .customActivityId(customActivity.getId())
                .status("OK")
                .payload(jsonHelper.toJsonString(dto))
                .response(jsonHelper.toJsonString(incentiveEventPointDTO))
                .targetType(Constants.USER_TYPE)
                .updatedAt(Instant.now())
                .trackingDate(userEventQueue.getEventDate())
                .build();
            ampUserActivityIncentiveRepository.saveAndFlush(ampUserActivityIncentive);

        }


        ProcessIncentiveActivityResponse response = ProcessIncentiveActivityResponse.builder()
            .earnedPoint(incentiveEventPointDTO.getPoint())
            .rewards(incentiveEventPointDTO.getRewards())
            .totalPoint(incentiveEventPointDTO.getTotalPoint()).build();

        result.setResult(response);
        result.setMessage("The activity has earned the point awarded.");
        result.setHttpStatus(HttpStatus.OK);
        return result;

    }

    private int getNumberAvailableDaysIfActivityIsCompletion(RecurrenceEnum recurrenceEnum, Date target) {
        switch (recurrenceEnum) {
            case DAILY:
                return 1;
            case WEEKLY:
                return 7;
            case MONTHLY:
                return DateHelpers.getNumberDaysOfMonth(target);
        }
        return 0;
    }

    private boolean isDateInRange(List<Date> histories, Date target) {
        List<Date> dates = histories.stream()
            .filter(d -> DateUtils.isSameDay(d, DateHelpers.getStarOfDay(target))).collect(Collectors.toList());
        return !dates.isEmpty();
    }

    private UserEventQueue createUserEventQueue(Instant eventDate, String customActivityId, String eventCode, Long programId, Long programUserId) {

        UserEventQueue userEventQueue = new UserEventQueue();
        userEventQueue.setEventId(customActivityId);
        userEventQueue.setEventCode(eventCode);
        userEventQueue.setProgramId(programId);
        userEventQueue.setExecuteStatus(IncentiveStatusEnum.New.name());
        userEventQueue.setEventCategory(Constants.ACTIVITIES);
        userEventQueue.setEventDate(eventDate);
        userEventQueue.setProgramUserId(programUserId);
        return userEventQueue;
    }
}
