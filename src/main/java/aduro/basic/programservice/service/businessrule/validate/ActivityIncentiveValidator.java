package aduro.basic.programservice.service.businessrule.validate;


import aduro.basic.programservice.service.dto.ActivityEventDTO;
import aduro.basic.programservice.service.dto.ParticipantDTO;
import io.vavr.collection.CharSeq;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

@Component
public class ActivityIncentiveValidator {

    String EVENT_CODE_ERR = "Event Code can not null or empty.";
    String EVENT_ID_ERR = "Event Id can not null or empty.";
    String EVENT_DATE_ERR = "Event Date can not null or empty.";
    String CLIENT_ID_ERR = "Client Id can not null or empty.";
    String PARTICIPANT_ID_ERR = "Participant Ids can not null or empty.";

    public Validation<Seq<String>, ActivityEventDTO> validEventDTO(String eventCode, String eventId, String clientId, Instant eventDate, List<ParticipantDTO> participantDTOList) {
        return Validation.combine(
            validateField(eventCode, EVENT_CODE_ERR),
            validateField(eventId, EVENT_ID_ERR),
            validateField(clientId, CLIENT_ID_ERR),
            validateDate(eventDate, EVENT_DATE_ERR),
            validateParticipants(participantDTOList)
        )
            .ap(ActivityEventDTO::new);
    }

    private Validation<String, List<ParticipantDTO>> validateParticipants(List<ParticipantDTO> participantDTOList) {
        return !participantDTOList.isEmpty()
            ? Validation.valid(participantDTOList)
            : Validation.invalid(PARTICIPANT_ID_ERR);
    }

    private Validation<String, String> validateField(String field, String error) {
        return (field != null && !field.isEmpty())
            ? Validation.valid(field)
            : Validation.invalid(error);
    }

    private Validation<String, Instant> validateDate(Instant field, String error) {
        return field != null
            ? Validation.valid(field)
            : Validation.invalid(error);
    }

}
