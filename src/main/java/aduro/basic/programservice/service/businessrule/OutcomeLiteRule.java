package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.UserEvent;
import aduro.basic.programservice.domain.WellmetricEventQueue;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;


public interface OutcomeLiteRule {
    void processingIncentive();
}
