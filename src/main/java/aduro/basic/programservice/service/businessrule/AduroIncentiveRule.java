package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.service.dto.IncentiveEventDTO;
import aduro.basic.programservice.service.dto.IncentiveEventPointDTO;

import java.util.Optional;

public interface AduroIncentiveRule {

    Optional<IncentiveEventPointDTO> processIncentiveEvent(IncentiveEventDTO activityEventDTO);
}
