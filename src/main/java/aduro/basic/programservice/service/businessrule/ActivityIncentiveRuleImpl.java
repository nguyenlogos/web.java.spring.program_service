package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.UserEventQueue;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.service.businessrule.validate.ActivityIncentiveValidator;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.util.RandomUtil;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import io.vavr.control.Validation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

@Service
@Transactional
public class ActivityIncentiveRuleImpl implements ActivityIncentiveRule {
    private final ActivityIncentiveValidator validator;

    private final ClientProgramRepository clientProgramRepository;

    private final ProgramRepository programRepository;

    private final ApplicationProperties applicationProperties;

    private final ProgramUserRepository programUserRepository;

    private final UserEventQueueRepository userEventQueueRepository;

    private final UserIncentiveBusinessRule userIncentiveBusinessRule;

    private final UserEventRepository userEventRepository;

    private final UserRewardRepository userRewardRepository;

    private final CustomActivityAdapter customActivityAdapter;

    public ActivityIncentiveRuleImpl(ActivityIncentiveValidator validator, ClientProgramRepository clientProgramRepository, ProgramRepository programRepository, ApplicationProperties applicationProperties, ProgramUserRepository programUserRepository, UserEventQueueRepository userEventQueueRepository, UserIncentiveBusinessRule userIncentiveBusinessRule, UserEventRepository userEventRepository, UserRewardRepository userRewardRepository, CustomActivityAdapter customActivityAdapter) {
        this.validator = validator;
        this.clientProgramRepository = clientProgramRepository;
        this.programRepository = programRepository;
        this.applicationProperties = applicationProperties;
        this.programUserRepository = programUserRepository;
        this.userEventQueueRepository = userEventQueueRepository;
        this.userIncentiveBusinessRule = userIncentiveBusinessRule;
        this.userEventRepository = userEventRepository;
        this.userRewardRepository = userRewardRepository;
        this.customActivityAdapter = customActivityAdapter;
    }

    @Override
    public Optional<IncentiveEventPointDTO> processIncentive(ActivityEventDTO activityEventDTO) {
        Validation<Seq<String>, ActivityEventDTO> validation = validator.validEventDTO(activityEventDTO.getEventCode(),
            activityEventDTO.getEventId(),
            activityEventDTO.getClientId(),
            activityEventDTO.getEventDate(),
            activityEventDTO.getParticipants());
        if (!validation.isValid()) {
            throw new BadRequestAlertException(validation.toString(), "ActivityEventDTO", "EventDTO");
        }

        //get client program
        List<ClientProgram> clientPrograms = clientProgramRepository.findByClientIdAndProgramStatus(activityEventDTO.getClientId(), ProgramStatus.Active.toString());

        if (CollectionUtils.isEmpty(clientPrograms)) {
            throw new BadRequestAlertException("Program is not active.", "Program", "Program Status");
        }

        return processIncentiveWithProgram(clientPrograms, activityEventDTO);

    }

    private Optional<IncentiveEventPointDTO> processIncentiveWithProgram(List<ClientProgram> clientPrograms, ActivityEventDTO activityEventDTO) {
        // should be check tobacco

        IncentiveEventPointDTO firstIncentive = null;
        for (ParticipantDTO participantDTO : activityEventDTO.getParticipants()) {
            Program program = clientPrograms.stream()
                .map(cp -> cp.getProgram())
                .filter(p -> participantDTO.isAsProgramTester() == p.getQaVerify())
                .findFirst().orElse(null);

            if (program == null) {
                continue;
            }

            UserEventQueue userEventQueue = createUserEventQueue(activityEventDTO, program.getId());
            if (isNullOrEmpty(activityEventDTO.getCreatedByAdmin())) {
                userEventQueue.setCreatedBy(participantDTO.getParticipantId());
            } else {
                userEventQueue.setCreatedByAdmin(activityEventDTO.getCreatedByAdmin());
            }

            Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(participantDTO.getParticipantId(), program.getId());
            Option<ProgramUser> programUserOption = Option.ofOptional(programUserOptional);

            Option<IncentiveEventPointDTO> incentiveDtos = programUserOption.flatMap(programUser -> processIncentiveWithProgramUser(activityEventDTO, programUser, userEventQueue, program))
                .onEmpty(() -> handleWithNotCreatedProgramUserYet(activityEventDTO, participantDTO, userEventQueue, program));

            if (incentiveDtos.isDefined() && firstIncentive == null) {
                firstIncentive = incentiveDtos.getOrNull();
            }
        }

        // TODO: maybe consider to return a list of incentives in the future.
        // Now set the first user as response
        return Optional.of(firstIncentive);
    }

    private Option<IncentiveEventPointDTO>  handleWithNotCreatedProgramUserYet(ActivityEventDTO activityEventDTO, ParticipantDTO participantDTO, UserEventQueue userEventQueue, Program program) {
        //create program User
        ProgramUser programUser = createNewProgramUser(activityEventDTO, participantDTO);
        programUser = programUserRepository.saveAndFlush(programUser);

        // process by Program User
        return processIncentiveWithProgramUser(activityEventDTO, programUser, userEventQueue, program);
    }

    private Option<IncentiveEventPointDTO> processIncentiveWithProgramUser(ActivityEventDTO activityEventDTO, ProgramUser programUser, UserEventQueue userEventQueue, Program program) {

        // save user event queue
        userEventQueue.setProgramUserId(programUser.getId());
        userEventQueueRepository.save(userEventQueue);

        //TODO: should process point in queue, tasks
        IncentiveEventPointDTO incentiveEventPointDTO = processPointByUserEvent(activityEventDTO, userEventQueue, program , programUser);

        return Option.of(incentiveEventPointDTO);
    }

    private IncentiveEventPointDTO processPointByUserEvent(ActivityEventDTO activityEventDTO, UserEventQueue userEvent, Program program, ProgramUser programUser) {
        //calculate point and level
        BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program, applicationProperties);

        if(programUser.getSubgroupId() != null && (programUser.getSubgroupId().equals("undefined") || programUser.getSubgroupId().equals("0")))
            programUser.setSubgroupId("");

        IncentiveEventPointDTO incentiveEventPointDTO = userIncentiveBusinessRule.calculatePointAndLevelByCategoryPointAndSubCategoryPoint(userEvent, programUser, activityEventDTO.getEventCategory(), totalUserPoint, programUser.getSubgroupId());

        return incentiveEventPointDTO;
    }

    private UserEventQueue createUserEventQueue(ActivityEventDTO activityEventDTO, Long programId) {

        UserEventQueue userEventQueue = new UserEventQueue();
        userEventQueue.setEventId(activityEventDTO.getEventId());
        userEventQueue.setEventCode(activityEventDTO.getEventCode());
        userEventQueue.setProgramId(programId);
        userEventQueue.setExecuteStatus(IncentiveStatusEnum.New.name());
        if (RandomUtil.isNullOrEmpty(activityEventDTO.getEventCategory())) {
            userEventQueue.setEventCategory(Constants.ACTIVITIES);
        } else {
            userEventQueue.setEventCategory(activityEventDTO.getEventCategory());
        }
        userEventQueue.setEventDate(activityEventDTO.getEventDate());

        return userEventQueue;
    }


    private ProgramUser createNewProgramUser(ActivityEventDTO eventQueue, ParticipantDTO participantDTO) {
        ProgramUser user = new ProgramUser();
        user.setClientId(eventQueue.getClientId());
        user.setParticipantId(participantDTO.getParticipantId());
        user.setEmail(participantDTO.getEmail());
        user.setFirstName(participantDTO.getFirstName());
        user.setLastName(participantDTO.getLastName());
        user.setProgramId(eventQueue.getProgramId());
        user.setCurrentLevel(1);
        user.setTotalUserPoint(BigDecimal.valueOf(0));
        user.setSubgroupId(participantDTO.getSubgroupId());
        return programUserRepository.saveAndFlush(user);
    }


    private void handleClientProgramEmpty() {
        throw new BadRequestAlertException("program is invalid", "UserEvent", "ProgramIdNull");
    }
}
