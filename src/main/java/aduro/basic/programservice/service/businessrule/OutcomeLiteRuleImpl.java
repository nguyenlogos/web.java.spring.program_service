package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.domain.enumeration.BioCategory;
import aduro.basic.programservice.domain.enumeration.ResultStatus;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.repository.ProgramDao;
import aduro.basic.programservice.repository.ProgramRepository;
import aduro.basic.programservice.repository.ProgramUserRepository;
import aduro.basic.programservice.repository.UserEventQueueRepository;
import aduro.basic.programservice.repository.extensions.ProgramHealthyRangeRepositoryExtension;
import aduro.basic.programservice.repository.extensions.ProgramSubCategoryConfigurationRepositoryExtension;
import aduro.basic.programservice.repository.extensions.WellmetricEventQueueRepositoryExtension;
import aduro.basic.programservice.service.dto.BioCategoryKey;
import aduro.basic.programservice.service.dto.IncentiveStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class OutcomeLiteRuleImpl implements  OutcomeLiteRule {

    private final Logger log = LoggerFactory.getLogger(OutcomeLiteRuleImpl.class);

    private final WellmetricEventQueueRepositoryExtension wellmetricEventQueueRepositoryExtension;

    private final ProgramHealthyRangeRepositoryExtension programHealthyRangeRepositoryExtension;

    private final ProgramSubCategoryConfigurationRepositoryExtension programSubCategoryConfigurationRepositoryExtension;

    private final ProgramRepository programRepository;

    private final ProgramUserRepository programUserRepository;

    private final ApplicationProperties applicationProperties;

    private final UserIncentiveBusinessRule userIncentiveBusinessRule;

    private final UserEventQueueRepository userEventQueueRepository;

    static boolean isRunningOutcomeliteRule = false;
    @Autowired
    private ProgramDao programDao;

    public OutcomeLiteRuleImpl(WellmetricEventQueueRepositoryExtension wellmetricEventQueueRepositoryExtension,
                               ProgramHealthyRangeRepositoryExtension programHealthyRangeRepositoryExtension,
                               ProgramSubCategoryConfigurationRepositoryExtension programSubCategoryConfigurationRepositoryExtension, ProgramRepository programRepository, ProgramUserRepository programUserRepository, ApplicationProperties applicationProperties, UserIncentiveBusinessRule userIncentiveBusinessRule, UserEventQueueRepository userEventQueueRepository) {
        this.wellmetricEventQueueRepositoryExtension = wellmetricEventQueueRepositoryExtension;
        this.programHealthyRangeRepositoryExtension = programHealthyRangeRepositoryExtension;
        this.programSubCategoryConfigurationRepositoryExtension = programSubCategoryConfigurationRepositoryExtension;
        this.programRepository = programRepository;
        this.programUserRepository = programUserRepository;
        this.applicationProperties = applicationProperties;
        this.userIncentiveBusinessRule = userIncentiveBusinessRule;
        this.userEventQueueRepository = userEventQueueRepository;
    }

    public void processingIncentive() {
        log.debug("Staring processing wellmetric incentive");

        int currentPage = 0;
        int itemsPerPage = 5;
        if(isRunningOutcomeliteRule){
            return;
        }
        isRunningOutcomeliteRule = true;
        try {
            List<String> participants = programDao.getParticipantsNeedCalculateWmPoint(currentPage, itemsPerPage);
            while (participants != null && !participants.isEmpty()) {
                List<WellmetricEventQueue> wellmetricEvents = wellmetricEventQueueRepositoryExtension.findAllByExecuteStatusAndParticipantIdIn(IncentiveStatusEnum.New.name(), participants);
                if (!wellmetricEvents.isEmpty()) {
                    Map<BioCategoryKey, List<WellmetricEventQueue>> categoryKeyListMap = wellmetricEvents.stream()
                        .collect(Collectors.groupingBy(g -> new BioCategoryKey(g.getProgramId(), g.getParticipantId(), g.getEventDate())));
                    wellmetricEventQueueRepositoryExtension.updateStatusRunning(IncentiveStatusEnum.Running.name(), wellmetricEvents.stream().map(p -> p.getId()).collect(Collectors.toList()));
                    categoryKeyListMap.forEach(this::processQueues);
                }

                currentPage += 1;
                if (participants.size() < itemsPerPage) {
                    break;
                }
                participants = programDao.getParticipantsNeedCalculateWmPoint(currentPage, itemsPerPage);
            }
        }catch (Exception ex){
            log.error("processingIncentive", ex);
        }finally {
            isRunningOutcomeliteRule = false;
        }
    }

    private void processQueues(BioCategoryKey bioCategoryKey, List<WellmetricEventQueue> wellmetricEventQueues) {
        if (wellmetricEventQueues != null && !wellmetricEventQueues.isEmpty()) {
            processEventQueues(wellmetricEventQueues);
        }
    }

    private UserEventQueue createUserEvent(ProgramUser programUser, Program program) {
        UserEventQueue userEvent = new UserEventQueue();
        userEvent.setProgramUserId(programUser.getId());
        userEvent.setEventDate(Instant.now());
        userEvent.setEventCategory(Constants.WELLMETRICS);
        userEvent.setEventPoint(BigDecimal.valueOf(0.0));
        userEvent.setProgramUserId(programUser.getId());
        userEvent.setExecuteStatus(IncentiveStatusEnum.New.name());
        userEvent.setProgramId(program.getId());
        userEvent.setCreatedBy(programUser.getParticipantId());
        return userEvent;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void processEventQueues(List<WellmetricEventQueue> wellmetricEventQueues) {
        //get Program
        log.debug("Event Queues ->> Staring get Program");
        Optional<Program> programOptional =  wellmetricEventQueues.stream().findFirst().map(eq -> programRepository.findById(eq.getProgramId())).get();

        //if program not exist
        if (!programOptional.isPresent()) {
            List<WellmetricEventQueue> wellmetricEventQueueList = new ArrayList<>();
            wellmetricEventQueues.forEach(queue -> {
                queue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                queue.setErrorMessage("Client with id is not existed.");
                wellmetricEventQueueList.add(queue);
            });
            if (!CollectionUtils.isEmpty(wellmetricEventQueueList)) {
                wellmetricEventQueueRepositoryExtension.saveAll(wellmetricEventQueueList);
            }
            return;
        }

        Program program = programOptional.get();

        Optional<ProgramUser> optionalProgramUser = wellmetricEventQueues.stream().findFirst().map(queue -> programUserRepository.findProgramUserByParticipantIdAndProgramId(queue.getParticipantId(), program.getId())).get();

        // create program User if not exist
        List<UserEventQueue> userEvents = new ArrayList<>();

        ProgramUser programUser = optionalProgramUser.orElseGet(() -> createNewProgramUser(wellmetricEventQueues.stream().findFirst().get(), program.getId()));

        log.debug("Event Queues ->> Staring filter screening and register code");

        wellmetricEventQueues.stream()
            .filter(e -> e.getEventCode().toUpperCase().equals(Constants.WELLMETRIC_SCREENING) ||
             e.getEventCode().toUpperCase().equals(Constants.WELLMETRIC_REGISTRATION))
            .forEach(queue -> {
                UserEventQueue userEvent = createUserEvent(programUser, program);
                userEvent.setEventCategory(Constants.WELLMETRICS);
                userEvent.setEventCode(queue.getEventCode());
                userEvent.setEventId(queue.getEventId());
                userEvents.add(userEvent);
            });

        Map<String, List<WellmetricEventQueue>> listMap = wellmetricEventQueues.stream()
            .filter(e -> !e.getEventCode().toUpperCase().equals(Constants.WELLMETRIC_SCREENING))
            .filter(e -> !e.getEventCode().toUpperCase().equals(Constants.WELLMETRIC_REGISTRATION))
            .collect(Collectors.groupingBy(g -> g.getSubCategoryCode()));

        log.debug("Event Queues ->> Start create User Event Queue base on Configuration");

        List<ProgramSubCategoryConfiguration> configurationList = programSubCategoryConfigurationRepositoryExtension.findByProgramId(program.getId());

        for (Map.Entry<String, List<WellmetricEventQueue>> entry : listMap.entrySet()) {

            ProgramSubCategoryConfiguration configuration = configurationList.stream()
                .filter(c -> c != null && c.getSubCategoryCode().equalsIgnoreCase(entry.getKey()))
                .findFirst()
                .orElse(null);

            if (configuration == null) {
                List<WellmetricEventQueue> wellmetricEventQueueList = new ArrayList<>();
                entry.getValue().forEach(e -> {
                    e.setExecuteStatus(IncentiveStatusEnum.Error.name());
                    e.setErrorMessage("Program Healthy Range Configuration is not configured.");
                    wellmetricEventQueueList.add(e);
                });
                if (!CollectionUtils.isEmpty(wellmetricEventQueueList)) {
                    wellmetricEventQueueRepositoryExtension.saveAll(wellmetricEventQueueList);
                }
                return;
            }

            //CHOLESTEROLS
            if (entry.getKey().equals(BioCategory.CHOLESTEROL.getValue())) {
                entry.getValue().stream().filter( e -> e.getResult().equals(ResultStatus.PASS))
                    .forEach( e -> {
                        log.debug("Event Queues ->>Cholesterol Matching -> Create User Event Queue");
                        //create event
                        WellmetricEventQueue queue = entry.getValue().stream().findFirst().get();
                        UserEventQueue userEvent = createUserEvent(programUser, program);
                        userEvent.setEventCode(e.getEventCode());
                        userEvent.setEventId(queue.getId().toString());
                        userEvents.add(userEvent);
                    });
            }

            //BLOOD PRESSURE
            if (entry.getKey().equals(BioCategory.BLOOD_PRESSURE.getValue())) {

                if (configuration.isIsBloodPressureSingleTest()) {
                    //create event
                    //User get’s incentivized if both systolic and diastolic are in range.
                    boolean isEmpty = entry.getValue().stream().filter(e -> e.getResult().equals(ResultStatus.FAIL))
                        .collect(Collectors.toList()).isEmpty();
                    if (isEmpty) {
                        log.debug("Event Queues ->>Blood Pressure Single Test -> Create User Event Queue");
                        WellmetricEventQueue queue = entry.getValue().stream().findFirst().get();
                        UserEventQueue userEvent = createUserEvent(programUser, program);
                        userEvent.setEventCode(BioCategory.BLOOD_PRESSURE.getValue());
                        userEvent.setEventId(queue.getId().toString());
                        userEvents.add(userEvent);
                    }

                }

                if (configuration.isIsBloodPressureIndividualTest()) {
                    entry.getValue().stream().filter(e -> e.getResult().equals(ResultStatus.PASS))
                        .forEach(e -> {
                            log.debug("Event Queues ->>Blood Pressure Individual Test-> Create User Event Queue");
                            //create event
                        UserEventQueue userEvent = createUserEvent(programUser, program);
                        userEvent.setEventCode(e.getEventCode());
                        userEvent.setEventId(e.getId().toString());
                        userEvents.add(userEvent);
                    });
                }

            }

            //BODY COMPOSITION
            if (entry.getKey().equals(BioCategory.BODY_COMPOSITION.getValue())) {

                List<ProgramHealthyRange> ranges = programHealthyRangeRepositoryExtension.findByProgramId(program.getId())
                    .stream()
                    .filter( p -> p.getSubCategoryCode().equals(BioCategory.BODY_COMPOSITION.getValue()))
                    .filter(ProgramHealthyRange::isIsApplyPoint).collect(Collectors.toList());

                //Waist and BodyComposition is configured.
                if (!CollectionUtils.isEmpty(ranges) && ranges.size() == 2) {
                    log.debug("Event Queues ->>Body Composition Single Test -> Create User Event Queue");
                    if (configuration.isIsBmiAwardedInRange()) {
                        long existedCount = entry.getValue().stream().filter(e -> e.getResult().equals(ResultStatus.PASS)).count();
                        if (existedCount >= 1) {
                            entry.getValue().stream().findFirst()
                                .ifPresent( q -> {
                                    UserEventQueue userEvent = createUserEvent(programUser, program);
                                    userEvent.setEventCode(BioCategory.BODY_COMPOSITION.getValue());
                                    userEvent.setEventId(q.getId().toString());
                                    userEvents.add(userEvent);
                                });
                        }
                    }

                } else {
                    //Waist Circumference is being used as the test
                    //OR
                    //Body Composition Is being used as the test

                    if (!CollectionUtils.isEmpty(ranges) && configuration.isIsBmiAwardedInRange()) {
                        log.debug("Event Queues ->>Body Composition - BMI individual test-> Create User Event Queue");
                        //BMI Pass In Range
                        entry.getValue().stream()
                            .filter( e -> e.getResult().equals(ResultStatus.PASS))
                            .findFirst()
                            .ifPresent(e -> {
                                //create event
                                UserEventQueue userEvent = createUserEvent(programUser, program);
                                userEvent.setEventCode(e.getEventCode());
                                userEvent.setEventId(e.getId().toString());
                                userEvents.add(userEvent);
                            });
                    }
                }

            }

            //GLUCOSE
            if (entry.getKey().equals(BioCategory.GLUCOSE.getValue())) {
                    List<ProgramHealthyRange> ranges = programHealthyRangeRepositoryExtension.findByProgramId(program.getId())
                        .stream().filter( p -> p.getSubCategoryCode().equals(BioCategory.GLUCOSE.getValue()))
                        .filter(ProgramHealthyRange::isIsApplyPoint).collect(Collectors.toList());

                    if (!CollectionUtils.isEmpty(ranges) && ranges.size() == 2) {
                        if (configuration.isIsGlucoseAwardedInRange()) {
                            // 2 range configured, Points are earned if EITHER type of test is submitted and within range.
                            // have one event Code
                            //create event
                            log.debug("Event Queues ->>Glucose-> Create User Event Queue");
                            // one of them pass, it will earn point for one event
                            int existedPassItem = entry.getValue().stream().filter(e -> e.getResult().equals(ResultStatus.PASS))
                                .collect(Collectors.toList()).size();
                            if (existedPassItem >= 1) {
                                entry.getValue().stream()
                                    .findFirst().ifPresent( e -> {
                                    //create event
                                    log.debug("Event Queues ->>Glucose-> Create User Event Queue");
                                    UserEventQueue userEvent = createUserEvent(programUser, program);
                                    userEvent.setEventCode(BioCategory.GLUCOSE.getValue());
                                    userEvent.setEventId(e.getId().toString());
                                    userEvents.add(userEvent);
                                });
                            }
                        }
                    } else {
                        // fasting or non-fasting is passed
                        // admin check in Range
                        if (!CollectionUtils.isEmpty(ranges) && configuration.isIsGlucoseAwardedInRange()) {

                            entry.getValue().stream().filter( e -> e.getResult().equals(ResultStatus.PASS))
                                .forEach(e -> {
                                    //create event
                                    log.debug("Event Queues ->>Glucose-> Create User Event Queue");

                                    UserEventQueue userEvent = createUserEvent(programUser, program);
                                    userEvent.setEventCode(e.getEventCode());
                                    userEvent.setEventId(e.getId().toString());
                                    userEvents.add(userEvent);
                                });
                        }
                    }
            }

        }

        userEventQueueRepository.saveAll(userEvents);

        List<WellmetricEventQueue> wellmetricEventQueueList = new ArrayList<>();
        wellmetricEventQueues.forEach(e -> {
            e.setExecuteStatus(IncentiveStatusEnum.Completed.name());
            wellmetricEventQueueList.add(e);
        });

        if (!CollectionUtils.isEmpty(wellmetricEventQueueList)) {
            wellmetricEventQueueRepositoryExtension.saveAll(wellmetricEventQueueList);
        }

        userEvents.stream().forEach(userEvent -> processPointByUserEvent(userEvent, program, programUser));

    }

    private ProgramUser createNewProgramUser(WellmetricEventQueue eventQueue, long programId) {
        ProgramUser user = new ProgramUser();
        user.setClientId(eventQueue.getClientId());
        user.setParticipantId(eventQueue.getParticipantId());
        user.setEmail(eventQueue.getEmail());
        user.setFirstName(eventQueue.getEmail());
        user.setLastName(eventQueue.getEmail());
        user.setProgramId(programId);
        user.setCurrentLevel(1);
        user.setTotalUserPoint(BigDecimal.valueOf(0));
        user.setSubgroupId(eventQueue.getSubgroupId());
        return programUserRepository.saveAndFlush(user);
    }

    private void processPointByUserEvent(UserEventQueue userEvent, Program program, ProgramUser programUser) {
        //calculate point and level
        BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program, applicationProperties);

        if(programUser.getSubgroupId() != null && (programUser.getSubgroupId().equals("undefined") || programUser.getSubgroupId().equals("0")))
            programUser.setSubgroupId("");

        userIncentiveBusinessRule.calculatePointAndLevelByCategoryPointAndSubCategoryPoint(userEvent, programUser, Constants.WELLMETRICS, totalUserPoint, programUser.getSubgroupId());
    }
}
