package aduro.basic.programservice.service.businessrule;


import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.NotificationBuilder;
import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.domain.enumeration.CohortStatus;
import aduro.basic.programservice.domain.enumeration.CollectionStatus;
import aduro.basic.programservice.domain.enumeration.ProcessStep;
import aduro.basic.programservice.domain.enumeration.ProgramSNSEvent;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.helpers.DateHelpers;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.NotificationCenterRepositoryExtension;
import aduro.basic.programservice.repository.extensions.ProgramUserLevelProgressRepositoryExtension;
import aduro.basic.programservice.repository.extensions.WellmetricEventQueueRepositoryExtension;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.sns.PublisherService;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@Service
@Transactional
public class UserIncentiveBusinessRule {

    private final Logger log = LoggerFactory.getLogger(UserIncentiveBusinessRule.class);

    @Autowired
    private ProgramUserRepository programUserRepository;

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private UserRewardRepository userRewardRepository;

    @Autowired
    private WellmetricEventQueueRepositoryExtension wellmetricEventQueueRepositoryExtension;

    @Autowired
    private NotificationCenterRepositoryExtension notificationCenterRepositoryExtension;

    @Autowired
    private UserEventQueueRepository userEventQueueRepository;

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private ProgramUserLevelProgressRepositoryExtension programUserLevelProgressRepository;

    @Autowired
    private ProgramActivityRepository programActivityRepository;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;

    @Autowired
    private ProgramLevelActivityRepository programLevelActivityRepository;

    @Autowired
    private ProgramLevelPathRepository programLevelPathRepository;

    @Autowired
    private ProgramRequirementItemsRepository programRequirementItemsRepository;

    @Autowired
    private ProgramLevelRewardRepository programLevelRewardRepository;

    @Autowired
    private ProgramUserCohortRepository programUserCohortRepository;

    @Autowired
    private ProgramUserCollectionProgressRepository programUserCollectionProgressRepository;

    @Autowired
    private ProgramCategoryPointRepository programCategoryPointRepository;

    @Autowired
    private ProgramLevelRepository programLevelRepository;

    @Autowired
    private PublisherService publisherService;

    private UserEvent createUserEvent(UserEventQueue userEventQueue) {
        UserEvent userEvent = new UserEvent();
        userEvent.setEventCategory(userEventQueue.getEventCategory());
        userEvent.setEventCode(userEventQueue.getEventCode());
        userEvent.setEventPoint(BigDecimal.ZERO);
        userEvent.setEventId(userEventQueue.getEventId());
        userEvent.setEventDate(userEventQueue.getEventDate());
        userEvent.setCreatedBy(userEventQueue.getCreatedBy());
        userEvent.setCreatedByAdmin(userEventQueue.getCreatedByAdmin());
        userEvent.setMigration(userEventQueue.isMigration());
        return userEvent;
    }

    public  IncentiveEventPointDTO createUserEventWithEmptyPoint(UserEventQueue userEventQueue,
                                                                 ProgramUser programUser, String message) {

        IncentiveEventPointDTO incentiveEventPointDTO = new IncentiveEventPointDTO();
        try {

            //starting calculate incentive
            userEventQueue.setCurrentStep("Create User Event By Queue with 0 point");
            userEventQueue.setReadAt(Instant.now());

            UserEvent userEvent = createUserEvent(userEventQueue);

            Optional<Program> programOptional = programRepository.findById(userEventQueue.getProgramId());


            Program program =  programOptional.orElseGet(null);
            if (program == null) {
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                userEventQueue.setErrorMessage("Program can not find.");
                userEventQueueRepository.save(userEventQueue);
                return incentiveEventPointDTO;
            }

            userEvent.setProgramUser(programUser);
            userEventRepository.save(userEvent);
            userEventQueueRepository.save(userEventQueue);

            incentiveEventPointDTO.setPoint(BigDecimal.valueOf(0));
            incentiveEventPointDTO.setTotalPoint(programUser.getTotalUserPoint());
            incentiveEventPointDTO.setMessage(message);
        } catch (Exception e) {
            userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
            userEventQueue.setErrorMessage(e.getMessage());
            userEventQueueRepository.save(userEventQueue);
        }

        return incentiveEventPointDTO;
    }

    //TODO: should refactor logic, push message on queue
    @Transactional
    public IncentiveEventPointDTO calculatePointAndLevelByCategoryPointAndSubCategoryPoint(UserEventQueue userEventQueue,
                                                                                           ProgramUser programUser,
                                                                                           String categoryCode,
                                                                                           BigDecimal totalUserPoint,
                                                                                           String subgroupId) {
        IncentiveEventPointDTO incentiveEventPointDTO = new IncentiveEventPointDTO();
        List<String> processStatues = new ArrayList<>();
        try {

            log.debug("->>>>> INCENTIVE: starting to incentive");

            //1.starting calculate incentive
            processStatues.add(ProcessStep.CREATE_USER_EVENT.toString());
            userEventQueue.setReadAt(Instant.now());
            UserEvent userEvent = createUserEvent(userEventQueue);

            //2.check userEvent in the same day.
            Instant startOfDate = DateHelpers.getStarOfDay(Date.from(userEvent.getEventDate())).toInstant();
            Instant endOfDate = DateHelpers.getEndOfDay(Date.from(userEvent.getEventDate())).toInstant();
            List<UserEvent> userEvents = userEventRepository.findByProgramUserIdAndEventIdAndEventCodeAndEventDateBetween(programUser.getId(), userEvent.getEventId(), userEvent.getEventCode(), startOfDate, endOfDate);
            if (!CollectionUtils.isEmpty(userEvents)) {
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                userEventQueue.setErrorMessage("UserEvent is duplicated.");
                processStatues.add(ProcessStep.USER_EVENT_DUPLICATE.toString());
                userEventQueue.setCurrentStep(String.join("->", processStatues));
                userEventQueueRepository.save(userEventQueue);
                return incentiveEventPointDTO;
            }

            //fetch all configuration program
            Optional<Program> programOptional = programRepository.findById(userEventQueue.getProgramId());
            // 3. get program
            if (!programOptional.isPresent()) {
                log.debug("->>>>> INCENTIVE: Program is null");
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                userEventQueue.setErrorMessage("Program can not find.");
                processStatues.add(ProcessStep.PROGRAM_NOT_FOUND.toString());
                userEventQueue.setCurrentStep(String.join("->", processStatues));
                userEventQueueRepository.save(userEventQueue);
                return incentiveEventPointDTO;
            }

            Program program =  programOptional.get();

            if (Objects.isNull(program.isIsUsePoint()) || !program.isIsUsePoint()) {
                log.debug("->>>>> INCENTIVE: Program not use point");
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                userEventQueue.setErrorMessage("Program not use point");
                processStatues.add(ProcessStep.PROGRAM_NOT_USER_POINT.toString());
                userEventQueue.setCurrentStep(String.join("->", processStatues));
                userEventQueueRepository.save(userEventQueue);
                return incentiveEventPointDTO;
            }

            //4. get completion cap

            if (Objects.isNull(categoryCode)) {
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                userEventQueue.setErrorMessage("Category Code can not null.");
                processStatues.add(ProcessStep.CATEGORY_NOT_FOUND.toString());
                userEventQueue.setCurrentStep(String.join("->", processStatues));
                userEventQueueRepository.save(userEventQueue);
                return incentiveEventPointDTO;
            }

            Optional<ProgramCategoryPoint> categoryPointOptional = programCategoryPointRepository.findByProgramIdAndCategoryCode(program.getId(), categoryCode);
            if (!categoryPointOptional.isPresent()) {
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                userEventQueue.setErrorMessage("Category Point can not null.");
                processStatues.add(ProcessStep.CATEGORY_POINT_NOT_FOUND.toString());
                userEventQueue.setCurrentStep(String.join("->", processStatues));
                userEventQueueRepository.save(userEventQueue);
                return incentiveEventPointDTO;
            }

            ProgramCategoryPoint programCategoryPoint =  categoryPointOptional.get();

            long completeCap = userEventRepository.countAllByEventCodeAndProgramUserId(userEvent.getEventCode(), programUser.getId());

            //4.1 get current program level of user
            int currentLevel = programUser.getCurrentLevel();

            Optional<ProgramLevel> currentProgramLevelOptional = programLevelRepository.findAllByProgramIdAndCurrentLevel(program.getId(), currentLevel);

            if (!currentProgramLevelOptional.isPresent()) {
                processStatues.add(ProcessStep.CURRENT_PROGRAM_LEVEL_NOT_GET.toString());
                userEventQueue.setCurrentStep(String.join("->", processStatues));
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                userEventQueueRepository.save(userEventQueue);
                return incentiveEventPointDTO;
            }

            //5. if program has bonus configuration then we will get bonus point in custom activity to calculate the points.
            if (programCategoryPoint.getCategoryCode().equalsIgnoreCase(Constants.BONUS) && userEventQueue.getEventCode().contains(Constants.BONUS_ACTIVITY)) {

                //5.1 total user event bonus
                BigDecimal totalUserEventBonus = userEventRepository
                    .findUserEventsByProgramUserIdAndEventIdAndEventCategory(programUser.getId(), userEvent.getEventId(), Constants.BONUS)
                    .stream().filter(u -> u.getEventPoint() != null).map(u -> u.getEventPoint())
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

                ProgramActivity programActivity = programActivityRepository
                    .findByProgramIdAndActivityIdAndBonusPointsEnabledIsTrue(program.getId(), userEventQueue.getEventId());
                BigDecimal valuePoints = programActivity != null ? programActivity.getBonusPoints() : BigDecimal.valueOf(0);

                if (totalUserEventBonus.compareTo(valuePoints) > 0) {
                    log.debug("->>>>> INCENTIVE: User have earned point bonus points is greater than bonus configuration");
                    incentiveEventPointDTO.setPoint(BigDecimal.valueOf(0));
                    processStatues.add(ProcessStep.USER_BONUS_POINT_GREATER_BONUS_CONFIGURATION.toString());
                } else {
                    CustomActivityDTO customActivityDTO = customActivityAdapter.getCustomActivityDetail(Long.parseLong(programActivity.getActivityId()));
                    long times = getActivityDurationTimes(customActivityDTO);

                    BigDecimal dividedBonusPoints = valuePoints.divide(new BigDecimal(times), 2, RoundingMode.HALF_UP);

                    if (dividedBonusPoints.compareTo(new BigDecimal(1)) <= 0) {
                        dividedBonusPoints = dividedBonusPoints.setScale(0, RoundingMode.CEILING);
                    }

                    if (totalUserEventBonus.add(dividedBonusPoints).compareTo(valuePoints) > 0) {
                        log.debug("->>>>> INCENTIVE: User have earned point bonus points is greater than bonus configuration");
                        processStatues.add(ProcessStep.USER_BONUS_POINT_GREATER_BONUS_CONFIGURATION.toString());
                        incentiveEventPointDTO.setPoint(BigDecimal.valueOf(0));
                    } else {
                        userEvent.setEventPoint(CommonUtils.roundPoint(dividedBonusPoints));
                        userEventQueue.setEventPoint(dividedBonusPoints);
                        incentiveEventPointDTO.setPoint(CommonUtils.roundPoint(dividedBonusPoints));
                        processStatues.add(ProcessStep.USER_EARN_BONUS_POINT.toString());
                    }
                }
            } else if (userEvent.getEventCode().equalsIgnoreCase(Constants.ACTIVITY_WELLMETRICS_SCREENING)) {
                // set the point is zero if type is activity wellmetric
                BigDecimal expectedPoint = BigDecimal.ZERO;
                userEvent.setEventPoint(expectedPoint);
                userEventQueue.setEventPoint(expectedPoint);
                incentiveEventPointDTO.setPoint(expectedPoint);
                processStatues.add(ProcessStep.ACTIVITY_WELLMETRICS_SCREENING.toString());
            } else {
                // 6. if category code not bonus
                Set<ProgramSubCategoryPoint> programSubCategoryPoints = programCategoryPoint.getProgramSubCategoryPoints();

                //6.1 get sub category by eventCode
                Optional<ProgramSubCategoryPoint> programSubCategoryPointOptional = programSubCategoryPoints.stream()
                    .filter(programSubCategoryPoint -> programSubCategoryPoint.getCode().equalsIgnoreCase(userEvent.getEventCode()))
                    .findFirst();

                if (!programSubCategoryPointOptional.isPresent()) {
                    processStatues.add(ProcessStep.GET_SUB_CATEGORY_ERROR.toString());
                    userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
                    userEventQueue.setErrorMessage("Sub Category Point can not null.");
                    userEventQueue.setCurrentStep(String.join("->", processStatues));
                    userEventQueueRepository.save(userEventQueue);
                    return incentiveEventPointDTO;
                }

                // 6.2 program sub category existed, get point
                ProgramSubCategoryPoint programSubCategoryPoint = programSubCategoryPointOptional.get();

                // 6.3 not match cap, get exact point from sub-category
                boolean isNotMatchCap = (completeCap < programSubCategoryPoint.getCompletionsCap());
                BigDecimal expectedPoint = isNotMatchCap ? CommonUtils.roundPoint(programSubCategoryPoint.getValuePoint()) : BigDecimal.ZERO;
                userEvent.setEventPoint(expectedPoint);
                userEventQueue.setEventPoint(expectedPoint);
                incentiveEventPointDTO.setPoint(expectedPoint);
                if (isNotMatchCap) {
                    processStatues.add(ProcessStep.USER_EARN_POINT_NORMAL.toString() + completeCap);
                } else {
                    processStatues.add(ProcessStep.USER_MATCH_COMPLETION_CAP.toString() + completeCap);
                }
            }

            //7. get level user
            // 7.1 sum all user event level

            BigDecimal currentTotalUserPoint = userEventRepository.findAllByProgramUserId(programUser.getId())
                .stream().filter(u -> u.getEventPoint() != null)
                .map(UserEvent::getEventPoint)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

            BigDecimal finalTotalPoint = currentTotalUserPoint.add(userEvent.getEventPoint());

            //7.2 total point set program user
            if(totalUserPoint.compareTo(finalTotalPoint)>= 0)
                programUser.setTotalUserPoint(finalTotalPoint);
            else
                programUser.setTotalUserPoint(totalUserPoint);

            processStatues.add(ProcessStep.CALCULATE_USER_TOTAL_POINT.toString());

            //TODO: remove if not need
            programUserRepository.save(programUser);

            // 7.3 saving user event
            userEvent.setProgramUser(programUser);
            userEventRepository.save(userEvent);

            //7.3.1
            //publish message to sns if user event success
            publisherService.publishWithEvent(ProgramSNSEvent.PROGRAM_INCENTIVE,
                IncentiveSnsEventDto.builder()
                    .programId(program.getId())
                    .eventCode(userEvent.getEventCode())
                    .aduroId(programUser.getParticipantId())
                    .timestamp(Instant.now())
                    .build()
                );

            // return total point for payload
            incentiveEventPointDTO.setTotalPoint(programUser.getTotalUserPoint());

            // 7.4 do complete cohorts if have
            doCompletedCollectionsInCohorts(programUser);

            //8. calculate level
            //8.2 archive level or not
            ProgramLevel currentProgramLevel = currentProgramLevelOptional.get();

            boolean upLevel = this.isLevelUpByUserEvent(currentProgramLevel, programUser, subgroupId, userEvent);

            if (!upLevel) {
                processStatues.add(ProcessStep.FINISH_NOT_ARCHIVE_LEVEL.toString());
                userEventQueue.setCurrentStep(String.join("->", processStatues));
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Completed.name());
                userEventQueueRepository.save(userEventQueue);
                return incentiveEventPointDTO;
            }

            //9. get rewards
            List<UserReward> userRewards = doGetRewardByUserEvent(userEvent.getEventId(), currentProgramLevel, programUser, subgroupId, program);

            //save rewards
            if (!CollectionUtils.isEmpty(userRewards)) {
                List<UserReward> currentUserRewards = userRewardRepository.findByProgramUserIdAndProgramLevel(programUser.getId(), currentLevel);
                processStatues.add(ProcessStep.USER_EARN_REWARD.toString());
                if (CollectionUtils.isEmpty(currentUserRewards)) {
                    userRewardRepository.saveAll(userRewards);
                }
            }

            List<UserReward> rewards = userRewardRepository.findUserRewardByParticipantIdAndClientIdAndEventId(programUser.getParticipantId(), programUser.getClientId(), userEventQueue.getEventId());
            if (!CollectionUtils.isEmpty(rewards)) {
                List<String> rewardString = rewards.stream()
                    .filter(u -> Objects.nonNull(u.getRewardMessage()))
                    .map(UserReward::getRewardMessage).collect(Collectors.toList());
                incentiveEventPointDTO.setRewards(rewardString);
            }

            // increase user level up
            Integer newLevel = currentLevel + 1;
            processStatues.add(ProcessStep.USER_EARN_LEVEL.toString());
            programUser.setCurrentLevel(newLevel);

            incentiveEventPointDTO.setCurrentLevel(newLevel);

            //10. set level progress
            List<ProgramUserLevelProgress> existedProgress = programUserLevelProgressRepository.findByProgramUserIdAndLevelFrom(programUser.getId(), currentLevel);
            if (existedProgress.isEmpty()) {
                ProgramUserLevelProgress programUserLevelProgress = new ProgramUserLevelProgress();
                programUserLevelProgress.setCreatedAt(Instant.now());
                programUserLevelProgress.setHasReward(!userRewards.isEmpty());
                programUserLevelProgress.setLevelFrom(currentLevel);
                programUserLevelProgress.setLevelTo(newLevel);
                programUserLevelProgress.setLevelUpAt(Instant.now());
                programUserLevelProgress.setProgramUserId(programUser.getId());
                programUserLevelProgress.setUserEventId(userEvent.getId());
                programUserLevelProgressRepository.save(programUserLevelProgress);

                processStatues.add(ProcessStep.SAVING_LEVEL_PROGRESS.toString());
            }

            //11.  send notification
            NotificationCenter notificationCenter = NotificationBuilder.notificationLevelUp(programUser.getParticipantId(), Instant.now(), newLevel, userEvent.getEventId());
            notificationCenterRepositoryExtension.save(notificationCenter);

            processStatues.add(ProcessStep.NOTIFICATION_USER_LEVEL.toString());

            //12. update queue process
            processStatues.add(ProcessStep.FINISH_WITH_NEW_LEVEL.toString());
            userEventQueue.setExecuteStatus(IncentiveStatusEnum.Completed.name());
            userEventQueue.setCurrentStep(String.join("->", processStatues));

            // 13. saving user
            programUserRepository.save(programUser);

            //14. save user event queue
            userEventQueueRepository.save(userEventQueue);

            return incentiveEventPointDTO;

        } catch (Exception e) {
            userEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.name());
            userEventQueue.setErrorMessage(e.getMessage());
            userEventQueue.setCurrentStep(String.join("->", processStatues));
            userEventQueueRepository.save(userEventQueue);
        }
        return incentiveEventPointDTO;
    }

    private long getActivityDurationTimes (CustomActivityDTO customActivityDTO) {
        long times = 1;
        LocalDateTime startDate = LocalDateTime.ofInstant(customActivityDTO.getStartDate(), ZoneId.systemDefault());
        LocalDateTime endDate = LocalDateTime.ofInstant(customActivityDTO.getEndDate(), ZoneId.systemDefault());
        if (customActivityDTO.getRecurrence() != null) {
            String recurrence = customActivityDTO.getRecurrence().toString().toLowerCase();
            if (recurrence.contains("daily")) {
                times = Math.abs(endDate.until(startDate, ChronoUnit.DAYS));
            } else if (recurrence.contains("weekly")) {
                times = Math.abs(endDate.until(startDate, ChronoUnit.WEEKS));
            } else if (recurrence.contains("monthly")) {
                times = Math.abs(endDate.until(startDate, ChronoUnit.MONTHS));
            }
        }
        return times;
    }

    @Transactional
    public void doCompletedCollectionsInCohorts(ProgramUser programUser) {

        List<ProgramUserCohort> userCohorts = programUserCohortRepository.findProgramUserCohortByProgramUserIdAndStatusIsNotIn(programUser.getId(), Arrays.asList(CohortStatus.PENDING, CohortStatus.DISABLED, CohortStatus.COMPLETED));

        if (CollectionUtils.isEmpty(userCohorts)) {
            return;
        }

        List<ProgramUserCollectionProgress> totalCollectionProgressList = new ArrayList<>();

        for (ProgramUserCohort userCohort : userCohorts) {

            List<ProgramUserCollectionProgress> userCollectionProgresses = programUserCollectionProgressRepository.getAllByProgramUserCohortId(userCohort.getId());

            ProgramCohort programCohort = userCohort.getProgramCohort();

            Set<ProgramCohortCollection> programCohortCollections = programCohort.getProgramCohortCollections();

            List<ProgramCohortCollection> collectionLevels = new ArrayList<>();

            for (ProgramCohortCollection programCohortCollection : programCohortCollections) {

                collectionLevels.add(programCohortCollection);

                //condition pass level current
                ProgramCollection programCollection = programCohortCollection.getProgramCollection();

                int requiredCompletion = (isNull(programCohortCollection.getRequiredCompletion()) || Integer.valueOf(0).equals(programCohortCollection.getRequiredCompletion())) ? 0 : programCohortCollection.getRequiredCompletion();

                List<String> idItems = programCollection.getProgramCollectionContents()
                    .stream()
                    .filter( c -> Objects.nonNull(c.getItemId()))
                    .map(ProgramCollectionContent::getItemId)
                    .collect(Collectors.toList());


                List<UserEvent> completedEvents = userEventRepository.findUserEventsByProgramUserIdAndEventCategoryInAndEventIdIn(programUser.getId(), getRequiredCategoryCode(), idItems);

                if (CollectionUtils.isEmpty(completedEvents)) {
                    continue;
                }

                Optional<ProgramUserCollectionProgress> collectionProgressOptional = userCollectionProgresses.stream()
                    .filter(p -> p.getCollectionId().equals(programCollection.getId()))
                    .findFirst();

                ProgramUserCollectionProgress userCollectionProgress = collectionProgressOptional.orElseGet(() -> {

                    ProgramUserCollectionProgress newProgress = new ProgramUserCollectionProgress()
                        .collectionId(programCollection.getId())
                        .programUserCohort(userCohort)
                        .createdDate(Instant.now())
                        .modifiedDate(Instant.now())
                        .totalRequiredItems(isNull(programCohortCollection.getRequiredCompletion()) ? 0 : programCohortCollection.getRequiredCompletion())
                        .status(CollectionStatus.DOING)
                        .currentProgress(0L);

                    userCollectionProgresses.add(newProgress);
                    return newProgress;
                });

                userCollectionProgress.setCurrentProgress((long) completedEvents.size());
                if (completedEvents.size() >= requiredCompletion) {
                    userCollectionProgress.setModifiedDate(Instant.now());
                    userCollectionProgress.setStatus(CollectionStatus.DONE);
                }
            }

            totalCollectionProgressList.addAll(userCollectionProgresses);

            if (!CollectionUtils.isEmpty(collectionLevels)) {

                List<String> collectionIds = collectionLevels.stream()
                    .map(c -> c.getProgramCollection().getId().toString())
                    .collect(Collectors.toList());

                List<ProgramUserCollectionProgress> cohortProgressList = userCollectionProgresses.stream()
                    .filter(programUserCollectionProgress -> collectionIds.contains(programUserCollectionProgress.getCollectionId().toString()))
                    .filter(p -> p.getStatus().equals(CollectionStatus.DOING))
                    .collect(Collectors.toList());

                userCohort.setStatus(cohortProgressList.isEmpty() ? CohortStatus.COMPLETED : CohortStatus.INPROGRESS);

            } else {
                userCohort.setStatus(CohortStatus.COMPLETED);
            }
        }

        programUserCohortRepository.saveAll(userCohorts);

        programUserCollectionProgressRepository.saveAll(totalCollectionProgressList);
    }

    public boolean isCompletedCohorts(ProgramUser programUser) {

        List<ProgramUserCohort> programUserCohorts = programUserCohortRepository.getProgramUserInProgramCohort(programUser.getId(),  Arrays.asList(CohortStatus.PENDING, CohortStatus.DISABLED));

        boolean isCompleteCohort = true;

        for (ProgramUserCohort programUserCohort : programUserCohorts) {

            ProgramCohort programCohort =  programUserCohort.getProgramCohort();
            Set<ProgramCohortCollection> programCohortCollections = programCohort.getProgramCohortCollections();
            if (CollectionUtils.isEmpty(programCohortCollections))  {
                continue;
            }

            List<ProgramCohortCollection> collectionOfCurrentLevels = programCohortCollections.stream()
                .filter(p -> Objects.nonNull(p.getRequiredLevel()) && p.getRequiredLevel().equals(programUser.getCurrentLevel()))
                .collect(Collectors.toList());

            if (CollectionUtils.isEmpty(collectionOfCurrentLevels)) {
                continue;
            }

            List<Long> collectOfIds = collectionOfCurrentLevels
                .stream()
                .map(p -> p.getProgramCollection().getId())
                .collect(Collectors.toList());

            List<ProgramUserCollectionProgress> progresses = programUserCollectionProgressRepository.findByProgramUserCohortIdAndCollectionIdIn(programUserCohort.getId(), collectOfIds);

            long count = progresses.stream()
                .filter(p -> p.getStatus().equals(CollectionStatus.DOING))
                .count();

            isCompleteCohort = count == 0;
        }

        return isCompleteCohort;
    }


    @Transactional
    public List<ProgramLevelActivity> getRequiredActivityBySubgroup(ProgramLevel currentProgramLevel, String subId) {
        List<ProgramLevelActivity> programActivities = programLevelActivityRepository.findProgramLevelActivitiesByProgramLevelId(currentProgramLevel.getId());
        return programActivities.stream()
            .filter(item -> CommonUtils.nullOrEqualSubgroupId(subId).test(item.getSubgroupId())).collect(Collectors.toList());
    }

    @Transactional
    public List<ProgramLevelPath> getRequiredPathBySubgroup(ProgramLevel currentProgramLevel, String subgroupId) {

        List<ProgramLevelPath> programLevelPaths = programLevelPathRepository.findProgramLevelPathsByProgramLevelId(currentProgramLevel.getId());

        return programLevelPaths.stream()
            .filter(item -> CommonUtils.nullOrEqualSubgroupId(subgroupId).test(item.getSubgroupId())).collect(Collectors.toList());
    }

    @Transactional
    public List<ProgramLevelReward> getProgramLevelRewardsBySubgroup(ProgramLevel currentProgramLevel, String subgroupId) {

        List<ProgramLevelReward> programLevelRewards = programLevelRewardRepository.findProgramLevelRewardsByProgramLevelId(currentProgramLevel.getId());

        List<ProgramLevelReward> itemsAllSubgroups = programLevelRewards.stream()
            .filter(a -> a.getSubgroupId() == null || a.getSubgroupId().equals("")).collect(Collectors.toList());

        List<ProgramLevelReward> rewardsSubgroups = programLevelRewards.stream()
            .filter(a -> a.getSubgroupId() != null && a.getSubgroupId().equals(subgroupId)).collect(Collectors.toList());


        itemsAllSubgroups.forEach(reward -> {
            Optional<ProgramLevelReward> existedRewardTypeOptional = rewardsSubgroups.stream()
                .filter(item -> item.getRewardType().equalsIgnoreCase(reward.getRewardType()))
                .findFirst();
            if (!existedRewardTypeOptional.isPresent()) {
                rewardsSubgroups.add(reward);
            }
        });

        return rewardsSubgroups;
    }

    /*
     * Build reward description by UserReward object
     * */
    public String buildRewardString(UserReward reward) {
        String content = "";

        if(reward.getRewardType().equals(RewardTypeEnum.TangoCard.toString())){
            content += String.format("$%s %s",reward.getRewardAmount(),"gift card");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.HealthSavings.toString())){
            content += String.format("$%s/%s %s",reward.getRewardAmount(), reward.getRewardCode()," added to your HSA");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.PayrollCredits.toString())){
            content += String.format("$%s %s",reward.getRewardAmount()," added to your payroll");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.PaidTimeOff.toString())){
            content += String.format("%s %s %s",reward.getRewardAmount(), reward.getRewardCode()," of paid time off");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.Custom.toString())){
            content += reward.getRewardCode();
        }
        else if (reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoSurcharge.toString())) {
            content += String.format("$%s %s",reward.getRewardAmount()," of tobacco surcharge");
        }
        else if (reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoPayrollCredit.toString())) {
            content += String.format("$%s %s",reward.getRewardAmount(), " of tobacco payroll credit");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.TobaccoCustom.toString())){
            content += reward.getRewardCode();
        }
        return content;
    }


    /*
     * Build reward description by reward object
     * */
    public String buildRewardDescription(ProgramLevelReward reward) {
        if(reward.getRewardType().equals(RewardTypeEnum.TangoCard.toString())){
            return String.format("$%s %s",reward.getRewardAmount(),"gift card");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.HealthSavings.toString())){
            return String.format("$%s/%s %s",reward.getRewardAmount(), reward.getDescription()," added to your HSA");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.PayrollCredits.toString())){
            return String.format("$%s %s",reward.getRewardAmount()," added to your payroll");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.PaidTimeOff.toString())){
            return String.format("%s %s %s",reward.getRewardAmount(), reward.getDescription()," of paid time off");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.Custom.toString())){
            return reward.getDescription();
        }
        else if (reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoSurcharge.toString())) {
            return String.format("$%s %s %s",reward.getRewardAmount(), reward.getDescription()," of tobacco surcharge");
        }
        else if (reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoPayrollCredit.toString())) {
            return String.format("$%s %s %s",reward.getRewardAmount(), reward.getDescription()," of tobacco payroll credit");
        }
        else if(reward.getRewardType().equals(RewardTypeEnum.TobaccoCustom.toString())){
            return reward.getDescription();
        }
        return "";
    }

    private List<String> getRequiredCategoryCode() {
        List<String> categories = new ArrayList<>();
        categories.add(Constants.ACTIVITIES);
        categories.add(Constants.ASSESSMENTS);
        categories.add(Constants.BONUS);
        categories.add(Constants.WELLMETRICS);
        categories.add(Constants.PREVENTIVE_SCREENINGS);
        categories.add(Constants.HPCONTENT);
        return categories;
    }

    /*
     * This function invoke when need to verify level of user
     * Base On Activity and Path required
     *Current Level
     * */
    @Transactional
    public Boolean  isLevelUpByUserEvent(ProgramLevel currentProgramLevel, ProgramUser programUser, String subgroupId, UserEvent userEvent) {
        boolean upLevel = true;
        if(currentProgramLevel.getEndPoint().compareTo(programUser.getTotalUserPoint().intValue())>0)
            upLevel = false;


        //TODO: Should tracking what activities have completed by participantId
        if (upLevel) {
            List<String> listRequired  =  getRequiredActivityBySubgroup(currentProgramLevel, subgroupId).stream().map(a->a.getActivityId()).collect(Collectors.toList());
            if(listRequired.size() > 0){
                List<UserEvent> userEvents  =  userEventRepository.findUserEventsByProgramUserIdAndEventCategoryInAndEventIdIn(programUser.getId(), getRequiredCategoryCode(), listRequired);
                List<String> userEventIds = userEvents.stream().map(u->u.getEventId()).collect(Collectors.toList());
                if(!listRequired.stream().allMatch(r->userEventIds.contains(r)))
                    upLevel = false;
            }
        }

        // check required path
        if(upLevel){
            List<String> listRequired  =  getRequiredPathBySubgroup(currentProgramLevel, subgroupId).stream().map(a->a.getPathId()).collect(Collectors.toList());
            if(listRequired.size() > 0){
                List<UserEvent> userEvents  =  userEventRepository.findUserEventsByProgramUserIdAndEventCategoryInAndEventIdIn(programUser.getId(), getRequiredCategoryCode(),listRequired);
                userEvents.add(userEvent);
                List<String> userEventIds = userEvents.stream().map(u->u.getEventId()).collect(Collectors.toList());
                if(!listRequired.stream().allMatch(r->userEventIds.contains(r)))
                    upLevel = false;
            }
        }

        // if user is tobacco user , user will require to complete RAS
        if(upLevel) {
            if (programUser.isIsTobaccoUser() !=null && programUser.isIsTobaccoUser()) {
                // check requirement RAS for tobacco user
                List<String> requirements = getRequirementRASBySubgroup(currentProgramLevel, subgroupId);
                if(!requirements.isEmpty()){
                    List<UserEvent> userEvents  =  userEventRepository.findUserEventsByProgramUserIdAndEventIdIn(programUser.getId(), requirements);
                    userEvents.add(userEvent);
                    List<String> userEventIds = userEvents.stream().map(u->u.getEventId()).collect(Collectors.toList());
                    if(!requirements.stream().allMatch(r->userEventIds.contains(r)))
                        upLevel = false;
                }
            }
        }

        // if program full outcomes will check
        if (upLevel) {
            upLevel = isCompletedCohorts(programUser);
        }
        return upLevel;
    }

    public List<String> getAllRequiredItems(ProgramLevel currentProgramLevel, String subgroupId) {
        List<ProgramLevelPath> paths = getRequiredPathBySubgroup(currentProgramLevel, subgroupId);

        List<String> requiredRasItems = getRequirementRASBySubgroup(currentProgramLevel, subgroupId);

        List<ProgramLevelActivity> requiredActivityBySubgroup = getRequiredActivityBySubgroup(currentProgramLevel, subgroupId);

        List<String> requiredItems = new ArrayList<>();

        if (!paths.isEmpty()) {
            List<String> pathItems = paths.stream().map(p -> p.getPathId()).collect(Collectors.toList());
            requiredItems.addAll(pathItems);
        }

        if (requiredRasItems.isEmpty()) {
            requiredItems.addAll(requiredRasItems);
        }

        if (!requiredActivityBySubgroup.isEmpty()) {
            List<String> items = requiredActivityBySubgroup.stream().map(p -> p.getActivityId()).collect(Collectors.toList());
            requiredItems.addAll(items);
        }

        return requiredItems;

    }

    public List<ProgramRequirementItems> getRequiredItemsBySubgroup(ProgramLevel currentProgramLevel, String subgroupId) {
        return currentProgramLevel.getProgramRequirementItems().stream()
            .filter(item -> CommonUtils.nullOrEqualSubgroupId(subgroupId).test(item.getSubgroupId())).collect(Collectors.toList());
    }

    /*
     * Invoke in cate user tobacco check level up.
     * */
    public List<String> getRequirementRASBySubgroup(ProgramLevel currentProgramLevel, String subgroupId) {

        Option<String> subgroupOption = Option.of(subgroupId);

        List<ProgramRequirementItems> requirementItems = programRequirementItemsRepository.findByProgramLevelId(currentProgramLevel.getId());

        BiFunction<String, ProgramLevel, List<String>> condition = (id, level) -> requirementItems.stream()
            .filter(a -> CommonUtils.nullOrEqualSubgroupId(subgroupId).test(a.getSubgroupId())).map(a -> a.getItemId()).collect(Collectors.toList());

        String  subId = subgroupOption.getOrElse("");

        return condition.apply(subId, currentProgramLevel);
    }

    /*
     * Invoke when user has level up, this function to get reward for user.
     *
     * */
    public List<UserReward>  doGetRewardByUserEvent(String eventId, ProgramLevel currentProgramLevel, ProgramUser programUser, String subgroupId, Program program) {
        List<UserReward> userRewards = new ArrayList<>();
        List<ProgramLevelReward> programLevelRewards = getProgramLevelRewardsBySubgroup(currentProgramLevel,  subgroupId);
        //Check with subgroup

        if (!programLevelRewards.isEmpty()) {
            List<ProgramLevelReward> finalRewards = programLevelRewards;

            if (programUser.isIsTobaccoUser() == null) {
                finalRewards = programLevelRewards.stream()
                    .filter(reward -> reward.getRewardType() != null && !reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoSurcharge.toString()))
                    .filter(reward -> reward.getRewardType() != null && !reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoPayrollCredit.toString()))
                    .filter(reward -> reward.getRewardType() != null && !reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoCustom.toString()))
                    .collect(Collectors.toList());
            }

            if (!finalRewards.isEmpty()) {

                finalRewards.stream().map(levelReward -> createUserReward(levelReward, programUser, program, Instant.now()))
                    .forEach(userReward -> {
                        userReward.setRewardMessage(buildRewardString(userReward));
                        userReward.setEventId(eventId);
                        userRewards.add(userReward);
                    });
            }
        }
        return userRewards;
    }

    /*
     * create User reward
     * */
    public UserReward createUserReward(ProgramLevelReward levelReward, ProgramUser user, Program program, Instant attemptedAt) {
        UserReward  userReward = new UserReward();
        userReward.setProgramUser(user);
        userReward.setProgram(program);
        userReward.setClientId(user.getClientId());
        userReward.setParticipantId(user.getParticipantId());
        userReward.setEmail(user.getEmail());
        userReward.setLevelCompletedDate(Instant.now());
        userReward.setProgramLevel(user.getCurrentLevel());
        if (program.getQaVerify() && levelReward.getRewardType().equals(RewardTypeEnum.TangoCard.name())){
            userReward.setStatus(IncentiveStatusEnum.Completed.toString());
        }else {
            userReward.setStatus(IncentiveStatusEnum.New.toString());
        }
        userReward.setRewardType(levelReward.getRewardType());
        userReward.setRewardAmount(levelReward.getRewardAmount());
        userReward.setRewardCode(levelReward.getDescription());
        userReward.setCampaignId(levelReward.getCampaignId());
        userReward.setEventHappenAt(attemptedAt);
        return userReward;
    }
}
