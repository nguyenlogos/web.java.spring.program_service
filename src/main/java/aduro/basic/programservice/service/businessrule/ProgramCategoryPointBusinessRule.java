package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.domain.Category;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramCategoryPoint;
import aduro.basic.programservice.domain.ProgramSubCategoryPoint;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.repository.ProgramRepository;
import aduro.basic.programservice.service.dto.ProgramCategoryPointDTO;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProgramCategoryPointBusinessRule {

    @Autowired
    private ApplicationProperties applicationProperties;


    @Autowired
    private ProgramRepository programRepository;

    public ProgramCategoryPoint CaculatePoint(ProgramCategoryPoint p, BigDecimal userPoint) {

        if(userPoint == null || p.getPercentPoint() == null) {
            p.setValuePoint(BigDecimal.valueOf(0.0));
        } else {
            BigDecimal totalPoint = userPoint.multiply(applicationProperties.getTotalPercentEconomy());
            p.setValuePoint(CommonUtils.roundNearestFive(p.getPercentPoint().multiply(totalPoint).doubleValue()));
        }
        return p;
    }

    public ProgramSubCategoryPoint CaculateSubPoint(ProgramSubCategoryPoint subPoint, ProgramCategoryPoint programCategoryPoint) {
        if(subPoint.getPercentPoint() == null || subPoint.getCompletionsCap() == null || subPoint.getCompletionsCap() == 0) {
            subPoint.setValuePoint(BigDecimal.valueOf(0.0));
        } else {
            Double totalPoint = subPoint.getPercentPoint().multiply(programCategoryPoint.getValuePoint()).doubleValue();
            Double times = 1.0;
            if (subPoint.getCompletionsCap() != 0) {
                times = subPoint.getCompletionsCap().doubleValue();
            }
            Double finalResult = totalPoint/times;
            subPoint.setValuePoint(CommonUtils.roundNearestFive(finalResult));
        }
        return subPoint;
    }

    @Transactional
    public BigDecimal calculateEconomyPoint(Long programId) {

        BigDecimal calculatePoint = BigDecimal.valueOf(0.0);

        // calculate economy point
        Optional<Program> programOptional = programRepository.findById(programId);
        if (!programOptional.isPresent()) {
            return calculatePoint;
        }

        Program program = programOptional.get();

        BigDecimal userPoint = programOptional.get().getUserPoint();

        BigDecimal defaultEconomyPoint =  userPoint == null ? BigDecimal.ZERO : applicationProperties.getTotalPercentEconomy().multiply(userPoint);

        List<ProgramCategoryPoint> categoryPoints = new ArrayList<>(program.getProgramCategoryPoints());

        if (categoryPoints.isEmpty()) {
            return  defaultEconomyPoint;
        }

        for (ProgramCategoryPoint dto : categoryPoints) {

            BigDecimal totalSubCategoryPoint = dto.getValuePoint();

            if (!dto.getProgramSubCategoryPoints().isEmpty()) {

                BigDecimal sum = dto.getProgramSubCategoryPoints().stream()
                    .map(ProgramSubCategoryPoint::getValuePoint)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

                if (sum.compareTo(totalSubCategoryPoint) > 0) {
                    totalSubCategoryPoint = sum;
                }
            }

            if (totalSubCategoryPoint != null) {
                calculatePoint = calculatePoint.add(totalSubCategoryPoint);
            }

        }

        return calculatePoint.compareTo(defaultEconomyPoint) > 0 ? calculatePoint : defaultEconomyPoint;
    }

}
