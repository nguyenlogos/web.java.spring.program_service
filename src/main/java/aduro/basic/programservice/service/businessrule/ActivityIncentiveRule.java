package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.service.dto.ActivityEventDTO;
import aduro.basic.programservice.service.dto.IncentiveEventPointDTO;

import java.util.Optional;

public interface ActivityIncentiveRule {
    Optional<IncentiveEventPointDTO> processIncentive(ActivityEventDTO activityEventDTO);
}
