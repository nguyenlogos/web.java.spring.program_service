package aduro.basic.programservice.service.businessrule;

import aduro.basic.programservice.ap.NotificationBuilder;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.domain.enumeration.ProcessLevelType;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.NotificationCenterRepositoryExtension;
import aduro.basic.programservice.repository.extensions.ProgramUserLevelProgressRepositoryExtension;
import aduro.basic.programservice.repository.extensions.UserEventQueueRepositoryExtension;
import aduro.basic.programservice.service.dto.IncentiveStatusEnum;
import aduro.basic.programservice.service.jobs.ScheduledTasksImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserLevelServiceImpl implements UserLevelService {

    private final Logger log = LoggerFactory.getLogger(UserLevelServiceImpl.class);

    private static Integer maxRetryCount = 5;

    private static int pageSize = 50;

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private UserEventQueueRepositoryExtension userEventQueueRepository;

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private UserIncentiveBusinessRule userIncentiveBusinessRule;

    @Autowired
    private CronJobLoaderRepository cronJobLoaderRepository;

    @Autowired
    private ProgramUserRepository programUserRepository;

    @Autowired
    private ProcessLevelParticipantQueueRepository processLevelParticipantQueueRepository;

    @Autowired
    private ProgramLevelRepository programLevelRepository;

    @Autowired
    private UserRewardRepository userRewardRepository;

    @Autowired
    private ProgramUserLevelProgressRepositoryExtension programUserLevelProgressRepository;

    @Autowired
    private NotificationCenterRepositoryExtension notificationCenterRepositoryExtension;

    @Autowired
    private JobExecutionTrackingRepository jobExecutionTrackingRepository;

    @Transactional
    public void processCalculateLevelInQueue() {

        log.info("->>>> START processCalculateLevelInQueue");

        ProcessLevelParticipantQueue currentQueue = null;
        try {

            int pageNo = 0;
            boolean hasReachMax = false;

            while (!hasReachMax) {

                Pageable paging = PageRequest.of(pageNo, pageSize);

                List<ProcessLevelParticipantQueue> availableQueues = processLevelParticipantQueueRepository.findByStatusAndAttemptCountLessThanEqualAndIsTerminatedOrderByCreatedAt( IncentiveStatusEnum.New.name(), maxRetryCount, false, paging);

                currentQueue = handleIncentiveQueue(availableQueues);

                if (currentQueue == null) return;

                hasReachMax = availableQueues.size() < pageSize;
                pageNo++;
            }

        } catch (Exception e) {
            log.info("->>>> process level user error = {}", e.getMessage());
            if (currentQueue != null) {
                currentQueue.setReadAt(Instant.now());
                currentQueue.setAttemptCount(currentQueue.getAttemptCount() + 1);
                currentQueue.setErrorMessage(e.getMessage());
                currentQueue.setStatus(IncentiveStatusEnum.Error.name());
                currentQueue.setErrorMessage(e.getMessage());
            }
        }
    }


    private ProcessLevelParticipantQueue handleIncentiveQueue(List<ProcessLevelParticipantQueue> availableQueues) throws Exception {

        ProcessLevelParticipantQueue currentQueue = null;

        if (availableQueues.isEmpty()) {
            log.info("->>>> process level participant in queue is empty.");
            return null;
        }

        List<ProgramUser> programUsers = new ArrayList<>();
        List<UserReward> allUserRewards = new ArrayList<>();
        List<ProgramUserLevelProgress> levelProgresses = new ArrayList<>();
        List<NotificationCenter> notificationCenters = new ArrayList<>();

        for (ProcessLevelParticipantQueue queue : availableQueues) {

            currentQueue = queue;
            queue.setAttemptCount(queue.getAttemptCount() + 1);
            queue.setUpdatedAt(Instant.now());
            queue.setReadAt(Instant.now());

            Optional<ProgramUser> programUserOptional = programUserRepository.findById(queue.getProgramUserId());
            if (!programUserOptional.isPresent()) {
                queue.setErrorMessage("ProgramUser Not Existed.");
                queue.setStatus(IncentiveStatusEnum.New.name());
                continue;
            }
            ProgramUser programUser = programUserOptional.get();

            if (currentQueue.getCurrentLevel() <= programUser.getCurrentLevel()) {
                queue.setErrorMessage("New Current Level is less than current level user.");
                queue.setStatus(IncentiveStatusEnum.Error.name());
                continue;
            }

            // do update level
            Integer currentLevel = queue.getCurrentLevel();
            programUser.setCurrentLevel(queue.getCurrentLevel());
            programUsers.add(programUser);

            // find user rewards
            Optional<ProgramLevel> programLevelOptional = programLevelRepository.findById(queue.getProgramLevelId());
            if (!programLevelOptional.isPresent()) {
                queue.setErrorMessage("ProgramLevel Not Existed.");
                queue.setStatus(IncentiveStatusEnum.Error.name());
                continue;
            }

            ProgramLevel programLevel = programLevelOptional.get();

            List<UserEvent> userEvents = userEventRepository.findByProgramUserIdOrderByEventDateDesc(programUser.getId());
            if (userEvents.isEmpty()) {
                queue.setErrorMessage("UserEvent is Empty.");
                queue.setStatus(IncentiveStatusEnum.Error.name());
                continue;
            }

            Optional<Program> programOptional = programRepository.findById(programUser.getProgramId());
            if (!programOptional.isPresent()){
                log.info("->>>> program error: ", programUsers.size());
                queue.setErrorMessage("Program Not Existed.");
                queue.setStatus(IncentiveStatusEnum.Error.name());
                continue;
            }

            Program program = programOptional.get();

            List<UserReward> archivedUserRewards = new ArrayList<>();

            List<UserReward> userRewards = userIncentiveBusinessRule.doGetRewardByUserEvent(userEvents.get(0).getEventId(), programLevel, programUser, programUser.getSubgroupId(), program);

            List<UserReward> userRewardList = userRewardRepository.findByProgramUserIdAndProgramLevel(programUser.getId(), programLevel.getLevelOrder());

            if (userRewardList.isEmpty()) {
                archivedUserRewards.addAll(userRewards);
            } else {

                List<String> rewardTypes = userRewardList.stream().map(UserReward::getRewardType).collect(Collectors.toList());

                if (!userRewards.isEmpty()) {
                    // find item not inserted yet
                    List<UserReward> rewardList = userRewards.stream().filter(userReward -> !rewardTypes.contains(userReward.getRewardType()))
                        .collect(Collectors.toList());
                    if (!rewardList.isEmpty()) {
                        archivedUserRewards.addAll(userRewards);
                    }
                }
            }

            allUserRewards.addAll(archivedUserRewards);

            // get program level progress
            ProgramUserLevelProgress programUserLevelProgress = new ProgramUserLevelProgress()
                .programUserId(programUser.getId())
                .levelFrom(queue.getArchiveLevel())
                .levelTo(queue.getCurrentLevel())
                .levelUpAt(Instant.now())
                .createdAt(Instant.now())
                .hasReward(userRewards.isEmpty())
                .userEventId(userEvents.get(0).getId());

            levelProgresses.add(programUserLevelProgress);

            queue.setNumberOfReward(allUserRewards.size());
            queue.setStatus(IncentiveStatusEnum.Completed.name());

            NotificationCenter notificationCenter = NotificationBuilder.notificationLevelUp(programUser.getParticipantId(), Instant.now(), currentLevel + 1, userEvents.get(0).getEventId());
            notificationCenters.add(notificationCenter);
        }

        programUserRepository.saveAll(programUsers);

        log.info("->>>> process save level user count = {}", programUsers.size());

        userRewardRepository.saveAll(allUserRewards);

        log.info("->>>> process save user reward count = {}", allUserRewards.size());

        programUserLevelProgressRepository.saveAll(levelProgresses);

        log.info("->>>> process notification = {}", notificationCenters.size());
        notificationCenterRepositoryExtension.saveAll(notificationCenters);


        log.info("->>>> saving queue available participant  = {}", availableQueues.size());
        processLevelParticipantQueueRepository.saveAll(availableQueues);
        return currentQueue;
    }

    @Transactional
    public void processParticipantOfClient(List<CronJobLoader> loaders) {

        List<ProcessLevelParticipantQueue> queueList = new ArrayList<>();

        for (CronJobLoader loader : loaders) {

            JobExecutionTracking tracking = JobExecutionTracking.builder()
                .createdAt(Instant.now())
                .updatedAt(Instant.now())
                .name(Constants.JOB_NAME_INCENTIVE)
                .errorMessage(null)
                .processStatus(IncentiveStatusEnum.New)
                .clientId(loader.getClientId())
                .numberOfRecords(0)
                .build();

            try {

                tracking = jobExecutionTrackingRepository.saveAndFlush(tracking);

                log.info("->>>> starting loader = {}", loader.getId());
                //load program
                Optional<Program> programOptional = programRepository.findById(loader.getProgramId());

                if (!programOptional.isPresent()) {
                    log.info("->>>> program is not existed.");
                    tracking.setErrorMessage("Program is not existed.");
                    jobExecutionTrackingRepository.saveAndFlush(tracking);
                    continue;
                }

                Program program = programOptional.get();

                List<ProgramLevel> programLevels = programLevelRepository.findProgramLevelsByProgram_Id(program.getId());

                // program don't has point or levels
                if (!program.isIsUsePoint() || programLevels.isEmpty()) {
                    tracking.setErrorMessage("Program does not have point.");
                    jobExecutionTrackingRepository.saveAndFlush(tracking);
                    continue;
                }

                //fetching users
                int pageNo = 0;
                boolean hasReachMax = false;
                boolean isRunning = false;
                while (!hasReachMax) {
                    log.info("->>>> fetching users with pageNo = {}", pageNo);

                    if (!isRunning) {
                        tracking.setProcessStatus(IncentiveStatusEnum.Running);
                        tracking = jobExecutionTrackingRepository.saveAndFlush(tracking);
                        isRunning = true;
                    }


                    Pageable paging = PageRequest.of(pageNo, pageSize);
                    List<ProgramUser> programUsers = programUserRepository.findProgramUserByClientIdAndProgramId(loader.getClientId(), loader.getProgramId(), paging);
                    if (programUsers.isEmpty()) {
                        break;
                    }

                    // validate user
                    for (ProgramLevel programLevel : programLevels) {

                        log.info("->>>> calculate level = {}", programLevel.getLevelOrder());

                        //user have point > end-point-> potential increase
                        List<ProgramUser> potentialUsers = programUsers.stream()
                            .filter(user -> user.getTotalUserPoint().compareTo(BigDecimal.valueOf(programLevel.getEndPoint())) > 0)
                            .collect(Collectors.toList());

                        UserEvent tmpEvent = new UserEvent().eventId(Instant.now().toString()); // avoid null.

                        List<ProgramUser> archiveLevelUsers = potentialUsers.stream()
                            .filter(u -> userIncentiveBusinessRule.isLevelUpByUserEvent(programLevel, u, u.getSubgroupId(), tmpEvent))
                            .collect(Collectors.toList());

                        if (archiveLevelUsers.isEmpty()) {
                            log.info("->>>> No user archive level in page = {}", pageNo);
                            continue;
                        }

                        log.info("->>>> Number of user archive level in page = {} and count = {}", pageNo, archiveLevelUsers.size());

                        // calculate to level queue
                        for (ProgramUser archiveLevelUser : archiveLevelUsers) {
                            Integer newCurrentLevel = programLevel.getLevelOrder() + 1;
                            if (newCurrentLevel > archiveLevelUser.getCurrentLevel()) {
                                ProcessLevelParticipantQueue queue = ProcessLevelParticipantQueue.builder()
                                    .archiveLevel(programLevel.getLevelOrder())
                                    .currentLevel(newCurrentLevel)
                                    .attemptCount(0)
                                    .createdAt(Instant.now())
                                    .programLevelId(programLevel.getId())
                                    .updatedAt(Instant.now())
                                    .programUserId(archiveLevelUser.getId())
                                    .isTerminated(false)
                                    .status(IncentiveStatusEnum.New.name())
                                    .build();
                                queueList.add(queue);
                            }
                        }



                    }

                    hasReachMax = programUsers.size() < pageSize;
                    pageNo++;
                }

                log.info("->>>> Queue user archive level = {}", queueList.size());



                // insert to table
                List<ProcessLevelParticipantQueue> needToSaveQueues = new ArrayList<>();

                if (!queueList.isEmpty()) {
                    for (ProcessLevelParticipantQueue participantQueue : queueList) {
                        List<ProcessLevelParticipantQueue> existedParticipant = processLevelParticipantQueueRepository.findByProgramUserIdAndCurrentLevelAndArchiveLevel(participantQueue.getProgramUserId(), participantQueue.getCurrentLevel(), participantQueue.getArchiveLevel());
                        if (existedParticipant.isEmpty()) {
                            needToSaveQueues.add(participantQueue);
                        }
                    }

                    if (!needToSaveQueues.isEmpty()) {
                        processLevelParticipantQueueRepository.saveAll(needToSaveQueues);
                    }
                }

                loader.setIsCompleted(true);
                loader.setReadAt(Instant.now());
                loader.setAttemptCount(loader.getAttemptCount() + 1);
                cronJobLoaderRepository.save(loader);


                if (!queueList.isEmpty()) {
                    tracking.setNumberOfRecords(queueList.size());
                    List<String> participantIds = queueList.stream().map(q -> q.getProgramUserId().toString()).collect(Collectors.toList());
                    String items = participantIds.stream().collect(Collectors.joining(","));
                    tracking.setPayload(items);
                    jobExecutionTrackingRepository.saveAndFlush(tracking);

                    if (isRunning) {
                        tracking.setProcessStatus(IncentiveStatusEnum.Sending);
                        tracking = jobExecutionTrackingRepository.saveAndFlush(tracking);
                    }

                    processCalculateLevelInQueue();

                    log.info("->>>> END Process Calculate Level");
                }

                tracking.setProcessStatus(IncentiveStatusEnum.Completed);
                tracking = jobExecutionTrackingRepository.saveAndFlush(tracking);

            } catch (Exception e) {
                log.info("->>>> process level user error = {}", e.getMessage());
                if (tracking != null) {
                    tracking.setErrorMessage(e.getMessage() != null ? e.getMessage() : "Internal error in queue.");
                    tracking.setProcessStatus(IncentiveStatusEnum.Error);
                    jobExecutionTrackingRepository.save(tracking);
                }

            }

        }
    }


    @Override
    @Transactional
    public void calculateUserArchivedLevel() {
        CronJobLoader currentCronJobLoaderIndividual = null;
        ProcessLevelParticipantQueue currentQueue = null;
        try {

            List<CronJobLoader> cronJobLoaders = cronJobLoaderRepository.findByIsCompletedAndIsForce(false, true);
            if (cronJobLoaders.isEmpty()) {
                log.info("->>>> process cronJobLoaders empty.");
                return;
            }

            // type = All -> load all participants of that clients
            List<CronJobLoader> loaderClients = cronJobLoaders.stream()
                .filter(c -> c.getType().equals(ProcessLevelType.ALL))
                .collect(Collectors.toList());

            new Thread(() -> {
                processParticipantOfClient(loaderClients);
                log.info("->>>> END process participant in clients");
            }).start();


            List<CronJobLoader> participantLoaders = cronJobLoaders.stream()
                .filter(c -> c.getType().equals(ProcessLevelType.INDIVIDUAL) && c.getAttemptCount() <= maxRetryCount)
                .collect(Collectors.toList());

            List<ProcessLevelParticipantQueue> queueList = new ArrayList<>();

            for (CronJobLoader participantLoader : participantLoaders) {

                currentCronJobLoaderIndividual = participantLoader;

                participantLoader.setReadAt(Instant.now());

                participantLoader.setAttemptCount(participantLoader.getAttemptCount() + 1);

                participantLoader.setReadAt(Instant.now());

                participantLoader.setAttemptCount(participantLoader.getAttemptCount() + 1);

                Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(participantLoader.getParticipantId(), participantLoader.getProgramId());

                if (!programUserOptional.isPresent()) {
                    log.info("->>>> process participant = {} not existed.", participantLoader.getParticipantId());
                    participantLoader.setErrorMessage("participant is not existed.");
                    continue;
                }

                ProgramUser programUser = programUserOptional.get();

                //load program
                Optional<Program> programOptional = programRepository.findById(participantLoader.getProgramId());

                if (!programOptional.isPresent()) {
                    log.info("->>>> program is not existed.");
                    participantLoader.setErrorMessage("Program is not existed.");
                    continue;
                }

                Program program = programOptional.get();

                // program don't has point or levels
                if (!program.isIsUsePoint() || program.getProgramLevels().isEmpty()) { continue; }

                List<ProgramLevel> programLevels = new ArrayList<>(program.getProgramLevels());
                if (programLevels.isEmpty()) {
                    log.info("->>>> program level is empty.");
                    participantLoader.setErrorMessage("Program level is empty.");
                    continue;
                }

                List<ProgramLevel> expectedProgramLevels = programLevels.stream().filter(p -> p.getLevelOrder() >= programUser.getCurrentLevel()).collect(Collectors.toList());
                if (expectedProgramLevels.isEmpty()) {
                    log.info("->>>> Expected program level is not correct.");
                    participantLoader.setErrorMessage("Expected Program level is not correct.");
                    continue;
                }

                for (ProgramLevel expectedProgramLevel : expectedProgramLevels) {
                    UserEvent tmpEvent = new UserEvent().eventId(Instant.now().toString());// avoid null.

                    Boolean isLevelUp = userIncentiveBusinessRule.isLevelUpByUserEvent(expectedProgramLevel, programUser, programUser.getSubgroupId(), tmpEvent);

                    List<String> allRequiredItems = userIncentiveBusinessRule.getAllRequiredItems(expectedProgramLevel, programUser.getSubgroupId());

                    String items = allRequiredItems.stream().collect(Collectors.joining(","));

                    if (isLevelUp) {

                        // set completed level
                        participantLoader.setRequiredItems(items);
                        participantLoader.setIsPassRequired(true);

                        // create queue
                        ProcessLevelParticipantQueue queue = ProcessLevelParticipantQueue.builder()
                            .archiveLevel(programUser.getCurrentLevel() + 1)
                            .currentLevel(programUser.getCurrentLevel())
                            .attemptCount(0)
                            .createdAt(Instant.now())
                            .programLevelId(expectedProgramLevel.getId())
                            .updatedAt(Instant.now())
                            .programUserId(programUser.getId())
                            .status(IncentiveStatusEnum.New.name())
                            .isTerminated(false)
                            .build();
                        queueList.add(queue);

                    } else {
                        participantLoader.setErrorMessage("User not match increase level yet.");
                        participantLoader.setIsPassRequired(false);
                        participantLoader.setRequiredItems(items);
                    }
                }
                participantLoader.setIsCompleted(true);
            }


            if (queueList.size() > 0) {
                log.info("->>>> Queue participant increase level = {}", queueList.size());
                processLevelParticipantQueueRepository.saveAll(queueList);
            }


            log.info("->>>> Queue cron participant loader handled = {}", participantLoaders.size());
            if (!participantLoaders.isEmpty()) {
                cronJobLoaderRepository.saveAll(participantLoaders);
            }

        } catch (Exception e) {
            log.info("->>>> process participant in error {}", e.getMessage());
            if (currentCronJobLoaderIndividual != null){
                currentCronJobLoaderIndividual.setErrorMessage(e.getMessage());
                cronJobLoaderRepository.save(currentCronJobLoaderIndividual);
            }

            if (currentQueue != null) {
                currentQueue.setReadAt(Instant.now());
                currentQueue.setAttemptCount(currentQueue.getAttemptCount() + 1);
                currentQueue.setErrorMessage(e.getMessage());
                currentQueue.setStatus(IncentiveStatusEnum.Error.name());
                currentQueue.setErrorMessage(e.getMessage());
            }
        }
    }
}
