package aduro.basic.programservice.service;

import java.util.concurrent.CompletableFuture;

public interface IServiceCommand {
    public Object Execute();
    public CompletableFuture<Object> ExecuteAysnc();
}
