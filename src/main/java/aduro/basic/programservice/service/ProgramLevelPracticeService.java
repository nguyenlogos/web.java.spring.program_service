package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramLevelPracticeDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramLevelPractice}.
 */
public interface ProgramLevelPracticeService {

    /**
     * Save a programLevelPractice.
     *
     * @param programLevelPracticeDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramLevelPracticeDTO save(ProgramLevelPracticeDTO programLevelPracticeDTO);

    /**
     * Get all the programLevelPractices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramLevelPracticeDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programLevelPractice.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramLevelPracticeDTO> findOne(Long id);

    /**
     * Delete the "id" programLevelPractice.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
