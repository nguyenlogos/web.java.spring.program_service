package aduro.basic.programservice.service;

import aduro.basic.programservice.domain.ProgramMemberConsentDetail;
import aduro.basic.programservice.service.dto.ProgramMemberConsentDetailDTO;
import java.util.List;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramMemberConsentDetail}.
 */
public interface ProgramMemberConsentDetailService {
    ProgramMemberConsentDetailDTO createConsentAccept(ProgramMemberConsentDetailDTO programMemberConsentDetailDTO);
    List<ProgramMemberConsentDetail> getConsentAcceptByDuring(Long consentId, Long programId);
    List<ProgramMemberConsentDetailDTO> getConsentByParticipantIdAndClientIdAndConsentIdAndProgramId(String participantId, String clientId, Long consentId, Long programId);
    List<ProgramMemberConsentDetailDTO> getConsentByParticipantIdAndClientIdAndConsentId(String participantId, String clientId, Long consentId);
}
