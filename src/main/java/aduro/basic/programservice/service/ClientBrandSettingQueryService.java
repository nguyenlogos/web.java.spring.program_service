package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ClientBrandSetting;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ClientBrandSettingRepository;
import aduro.basic.programservice.service.dto.ClientBrandSettingCriteria;
import aduro.basic.programservice.service.dto.ClientBrandSettingDTO;
import aduro.basic.programservice.service.mapper.ClientBrandSettingMapper;

/**
 * Service for executing complex queries for {@link ClientBrandSetting} entities in the database.
 * The main input is a {@link ClientBrandSettingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientBrandSettingDTO} or a {@link Page} of {@link ClientBrandSettingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientBrandSettingQueryService extends QueryService<ClientBrandSetting> {

    private final Logger log = LoggerFactory.getLogger(ClientBrandSettingQueryService.class);

    private final ClientBrandSettingRepository clientBrandSettingRepository;

    private final ClientBrandSettingMapper clientBrandSettingMapper;

    public ClientBrandSettingQueryService(ClientBrandSettingRepository clientBrandSettingRepository, ClientBrandSettingMapper clientBrandSettingMapper) {
        this.clientBrandSettingRepository = clientBrandSettingRepository;
        this.clientBrandSettingMapper = clientBrandSettingMapper;
    }

    /**
     * Return a {@link List} of {@link ClientBrandSettingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientBrandSettingDTO> findByCriteria(ClientBrandSettingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ClientBrandSetting> specification = createSpecification(criteria);
        return clientBrandSettingMapper.toDto(clientBrandSettingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientBrandSettingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientBrandSettingDTO> findByCriteria(ClientBrandSettingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ClientBrandSetting> specification = createSpecification(criteria);
        return clientBrandSettingRepository.findAll(specification, page)
            .map(clientBrandSettingMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ClientBrandSettingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ClientBrandSetting> specification = createSpecification(criteria);
        return clientBrandSettingRepository.count(specification);
    }

    /**
     * Function to convert ClientBrandSettingCriteria to a {@link Specification}.
     */
    private Specification<ClientBrandSetting> createSpecification(ClientBrandSettingCriteria criteria) {
        Specification<ClientBrandSetting> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientBrandSetting_.id));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), ClientBrandSetting_.clientId));
            }
            if (criteria.getSubPathUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubPathUrl(), ClientBrandSetting_.subPathUrl));
            }
            if (criteria.getWebLogoUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getWebLogoUrl(), ClientBrandSetting_.webLogoUrl));
            }
            if (criteria.getPrimaryColorValue() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPrimaryColorValue(), ClientBrandSetting_.primaryColorValue));
            }
            if (criteria.getClientName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientName(), ClientBrandSetting_.clientName));
            }
            if (criteria.getClientUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientUrl(), ClientBrandSetting_.clientUrl));
            }
        }
        return specification;
    }
}
