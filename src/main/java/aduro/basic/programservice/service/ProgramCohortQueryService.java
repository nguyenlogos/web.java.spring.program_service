package aduro.basic.programservice.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.JoinType;

import aduro.basic.programservice.repository.ProgramCohortCollectionRepository;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionDTO;
import aduro.basic.programservice.service.dto.ProgramCohortWithCollectionDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortCollectionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramCohortRepository;
import aduro.basic.programservice.service.dto.ProgramCohortCriteria;
import aduro.basic.programservice.service.dto.ProgramCohortDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortMapper;

/**
 * Service for executing complex queries for {@link ProgramCohort} entities in the database.
 * The main input is a {@link ProgramCohortCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramCohortDTO} or a {@link Page} of {@link ProgramCohortDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramCohortQueryService extends QueryService<ProgramCohort> {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortQueryService.class);

    private final ProgramCohortRepository programCohortRepository;

    private final ProgramCohortCollectionRepository programCohortCollectionRepository;

    private final ProgramCohortMapper programCohortMapper;

    private final ProgramCohortCollectionMapper programCohortCollectionMapper;

    public ProgramCohortQueryService(
        ProgramCohortRepository programCohortRepository,
        ProgramCohortMapper programCohortMapper,
        ProgramCohortCollectionRepository programCohortCollectionRepository,
        ProgramCohortCollectionMapper programCohortCollectionMapper
    ) {
        this.programCohortRepository = programCohortRepository;
        this.programCohortMapper = programCohortMapper;
        this.programCohortCollectionRepository = programCohortCollectionRepository;
        this.programCohortCollectionMapper = programCohortCollectionMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramCohortDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramCohortDTO> findByCriteria(ProgramCohortCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramCohort> specification = createSpecification(criteria);
        return programCohortMapper.toDto(programCohortRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramCohortDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramCohortDTO> findByCriteria(ProgramCohortCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramCohort> specification = createSpecification(criteria);
        return programCohortRepository.findAll(specification, page)
            .map(programCohortMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramCohortCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramCohort> specification = createSpecification(criteria);
        return programCohortRepository.count(specification);
    }

    /**
     * Return a {@link List} of {@link ProgramCohortWithCollectionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramCohortWithCollectionDTO> findByCriteriaWithCollections(ProgramCohortCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        List<ProgramCohortWithCollectionDTO> result = new ArrayList<>();
        final Specification<ProgramCohort> specification = createSpecification(criteria);
        List<ProgramCohort> programCohorts = programCohortRepository.findAll(specification);
        for (ProgramCohort cohort : programCohorts) {
            List<ProgramCohortCollectionDTO> cohortCollectionDTOs =
                programCohortCollectionMapper.toDto(programCohortCollectionRepository.findAllByProgramCohort(cohort));
            ProgramCohortWithCollectionDTO cohortWithCollectionDTO
                = new ProgramCohortWithCollectionDTO(programCohortMapper.toDto(cohort));
            cohortWithCollectionDTO.setCohortCollections(cohortCollectionDTOs);
            result.add(cohortWithCollectionDTO);
        }
        return result;
    }

    /**
     * Function to convert ProgramCohortCriteria to a {@link Specification}.
     */
    private Specification<ProgramCohort> createSpecification(ProgramCohortCriteria criteria) {
        Specification<ProgramCohort> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramCohort_.id));
            }
            if (criteria.getCohortName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCohortName(), ProgramCohort_.cohortName));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramCohort_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramCohort_.modifiedDate));
            }
            if (criteria.getIsDefault() != null) {
                specification = specification.and(buildSpecification(criteria.getIsDefault(), ProgramCohort_.isDefault));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramCohort_.program, JoinType.LEFT).get(Program_.id)));
            }
            if (criteria.getProgramCohortRuleId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCohortRuleId(),
                    root -> root.join(ProgramCohort_.programCohortRules, JoinType.LEFT).get(ProgramCohortRule_.id)));
            }
            if (criteria.getProgramCohortCollectionId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCohortCollectionId(),
                    root -> root.join(ProgramCohort_.programCohortCollections, JoinType.LEFT).get(ProgramCohortCollection_.id)));
            }
        }
        return specification;
    }
}
