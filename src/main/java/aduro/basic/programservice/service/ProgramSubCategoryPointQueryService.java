package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramSubCategoryPoint;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramSubCategoryPointRepository;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointCriteria;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryPointMapper;

/**
 * Service for executing complex queries for {@link ProgramSubCategoryPoint} entities in the database.
 * The main input is a {@link ProgramSubCategoryPointCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramSubCategoryPointDTO} or a {@link Page} of {@link ProgramSubCategoryPointDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramSubCategoryPointQueryService extends QueryService<ProgramSubCategoryPoint> {

    private final Logger log = LoggerFactory.getLogger(ProgramSubCategoryPointQueryService.class);

    private final ProgramSubCategoryPointRepository programSubCategoryPointRepository;

    private final ProgramSubCategoryPointMapper programSubCategoryPointMapper;

    public ProgramSubCategoryPointQueryService(ProgramSubCategoryPointRepository programSubCategoryPointRepository, ProgramSubCategoryPointMapper programSubCategoryPointMapper) {
        this.programSubCategoryPointRepository = programSubCategoryPointRepository;
        this.programSubCategoryPointMapper = programSubCategoryPointMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramSubCategoryPointDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramSubCategoryPointDTO> findByCriteria(ProgramSubCategoryPointCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramSubCategoryPoint> specification = createSpecification(criteria);
        return programSubCategoryPointMapper.toDto(programSubCategoryPointRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramSubCategoryPointDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramSubCategoryPointDTO> findByCriteria(ProgramSubCategoryPointCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramSubCategoryPoint> specification = createSpecification(criteria);
        return programSubCategoryPointRepository.findAll(specification, page)
            .map(programSubCategoryPointMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramSubCategoryPointCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramSubCategoryPoint> specification = createSpecification(criteria);
        return programSubCategoryPointRepository.count(specification);
    }

    /**
     * Function to convert ProgramSubCategoryPointCriteria to a {@link Specification}.
     */
    private Specification<ProgramSubCategoryPoint> createSpecification(ProgramSubCategoryPointCriteria criteria) {
        Specification<ProgramSubCategoryPoint> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramSubCategoryPoint_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), ProgramSubCategoryPoint_.code));
            }
            if (criteria.getPercentPoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentPoint(), ProgramSubCategoryPoint_.percentPoint));
            }
            if (criteria.getValuePoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValuePoint(), ProgramSubCategoryPoint_.valuePoint));
            }
            if (criteria.getCompletionsCap() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCompletionsCap(), ProgramSubCategoryPoint_.completionsCap));
            }
            if (criteria.getProgramCategoryPointId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCategoryPointId(),
                    root -> root.join(ProgramSubCategoryPoint_.programCategoryPoint, JoinType.LEFT).get(ProgramCategoryPoint_.id)));
            }
        }
        return specification;
    }
}
