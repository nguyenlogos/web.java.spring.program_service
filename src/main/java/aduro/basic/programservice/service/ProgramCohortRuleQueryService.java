package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramCohortRule;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramCohortRuleRepository;
import aduro.basic.programservice.service.dto.ProgramCohortRuleCriteria;
import aduro.basic.programservice.service.dto.ProgramCohortRuleDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortRuleMapper;

/**
 * Service for executing complex queries for {@link ProgramCohortRule} entities in the database.
 * The main input is a {@link ProgramCohortRuleCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramCohortRuleDTO} or a {@link Page} of {@link ProgramCohortRuleDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramCohortRuleQueryService extends QueryService<ProgramCohortRule> {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortRuleQueryService.class);

    private final ProgramCohortRuleRepository programCohortRuleRepository;

    private final ProgramCohortRuleMapper programCohortRuleMapper;

    public ProgramCohortRuleQueryService(ProgramCohortRuleRepository programCohortRuleRepository, ProgramCohortRuleMapper programCohortRuleMapper) {
        this.programCohortRuleRepository = programCohortRuleRepository;
        this.programCohortRuleMapper = programCohortRuleMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramCohortRuleDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramCohortRuleDTO> findByCriteria(ProgramCohortRuleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramCohortRule> specification = createSpecification(criteria);
        return programCohortRuleMapper.toDto(programCohortRuleRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramCohortRuleDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramCohortRuleDTO> findByCriteria(ProgramCohortRuleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramCohortRule> specification = createSpecification(criteria);
        return programCohortRuleRepository.findAll(specification, page)
            .map(programCohortRuleMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramCohortRuleCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramCohortRule> specification = createSpecification(criteria);
        return programCohortRuleRepository.count(specification);
    }

    /**
     * Function to convert ProgramCohortRuleCriteria to a {@link Specification}.
     */
    private Specification<ProgramCohortRule> createSpecification(ProgramCohortRuleCriteria criteria) {
        Specification<ProgramCohortRule> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramCohortRule_.id));
            }
            if (criteria.getDataInputType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDataInputType(), ProgramCohortRule_.dataInputType));
            }
            if (criteria.getRules() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRules(), ProgramCohortRule_.rules));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramCohortRule_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramCohortRule_.modifiedDate));
            }
            if (criteria.getProgramCohortId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCohortId(),
                    root -> root.join(ProgramCohortRule_.programCohort, JoinType.LEFT).get(ProgramCohort_.id)));
            }
        }
        return specification;
    }
}
