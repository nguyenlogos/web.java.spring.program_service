package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.NotificationCenter;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.NotificationCenterRepository;
import aduro.basic.programservice.service.dto.NotificationCenterCriteria;
import aduro.basic.programservice.service.dto.NotificationCenterDTO;
import aduro.basic.programservice.service.mapper.NotificationCenterMapper;

/**
 * Service for executing complex queries for {@link NotificationCenter} entities in the database.
 * The main input is a {@link NotificationCenterCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NotificationCenterDTO} or a {@link Page} of {@link NotificationCenterDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NotificationCenterQueryService extends QueryService<NotificationCenter> {

    private final Logger log = LoggerFactory.getLogger(NotificationCenterQueryService.class);

    private final NotificationCenterRepository notificationCenterRepository;

    private final NotificationCenterMapper notificationCenterMapper;

    public NotificationCenterQueryService(NotificationCenterRepository notificationCenterRepository, NotificationCenterMapper notificationCenterMapper) {
        this.notificationCenterRepository = notificationCenterRepository;
        this.notificationCenterMapper = notificationCenterMapper;
    }

    /**
     * Return a {@link List} of {@link NotificationCenterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NotificationCenterDTO> findByCriteria(NotificationCenterCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<NotificationCenter> specification = createSpecification(criteria);
        return notificationCenterMapper.toDto(notificationCenterRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NotificationCenterDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NotificationCenterDTO> findByCriteria(NotificationCenterCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<NotificationCenter> specification = createSpecification(criteria);
        return notificationCenterRepository.findAll(specification, page)
            .map(notificationCenterMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NotificationCenterCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<NotificationCenter> specification = createSpecification(criteria);
        return notificationCenterRepository.count(specification);
    }

    /**
     * Function to convert NotificationCenterCriteria to a {@link Specification}.
     */
    private Specification<NotificationCenter> createSpecification(NotificationCenterCriteria criteria) {
        Specification<NotificationCenter> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), NotificationCenter_.id));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), NotificationCenter_.title));
            }
            if (criteria.getParticipantId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParticipantId(), NotificationCenter_.participantId));
            }
            if (criteria.getExecuteStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExecuteStatus(), NotificationCenter_.executeStatus));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), NotificationCenter_.createdAt));
            }
            if (criteria.getAttemptCount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAttemptCount(), NotificationCenter_.attemptCount));
            }
            if (criteria.getLastAttemptTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastAttemptTime(), NotificationCenter_.lastAttemptTime));
            }
            if (criteria.getErrorMessage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getErrorMessage(), NotificationCenter_.errorMessage));
            }
            if (criteria.getContent() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContent(), NotificationCenter_.content));
            }
            if (criteria.getNotificationType() != null) {
                specification = specification.and(buildSpecification(criteria.getNotificationType(), NotificationCenter_.notificationType));
            }
            if (criteria.getEventId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventId(), NotificationCenter_.eventId));
            }
            if (criteria.getIsReady() != null) {
                specification = specification.and(buildSpecification(criteria.getIsReady(), NotificationCenter_.isReady));
            }
            if (criteria.getReceiverName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReceiverName(), NotificationCenter_.receiverName));
            }
        }
        return specification;
    }
}
