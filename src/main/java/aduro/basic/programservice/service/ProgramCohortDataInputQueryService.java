package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramCohortDataInput;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramCohortDataInputRepository;
import aduro.basic.programservice.service.dto.ProgramCohortDataInputCriteria;
import aduro.basic.programservice.service.dto.ProgramCohortDataInputDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortDataInputMapper;

/**
 * Service for executing complex queries for {@link ProgramCohortDataInput} entities in the database.
 * The main input is a {@link ProgramCohortDataInputCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramCohortDataInputDTO} or a {@link Page} of {@link ProgramCohortDataInputDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramCohortDataInputQueryService extends QueryService<ProgramCohortDataInput> {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortDataInputQueryService.class);

    private final ProgramCohortDataInputRepository programCohortDataInputRepository;

    private final ProgramCohortDataInputMapper programCohortDataInputMapper;

    public ProgramCohortDataInputQueryService(ProgramCohortDataInputRepository programCohortDataInputRepository, ProgramCohortDataInputMapper programCohortDataInputMapper) {
        this.programCohortDataInputRepository = programCohortDataInputRepository;
        this.programCohortDataInputMapper = programCohortDataInputMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramCohortDataInputDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramCohortDataInputDTO> findByCriteria(ProgramCohortDataInputCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramCohortDataInput> specification = createSpecification(criteria);
        return programCohortDataInputMapper.toDto(programCohortDataInputRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramCohortDataInputDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramCohortDataInputDTO> findByCriteria(ProgramCohortDataInputCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramCohortDataInput> specification = createSpecification(criteria);
        return programCohortDataInputRepository.findAll(specification, page)
            .map(programCohortDataInputMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramCohortDataInputCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramCohortDataInput> specification = createSpecification(criteria);
        return programCohortDataInputRepository.count(specification);
    }

    /**
     * Function to convert ProgramCohortDataInputCriteria to a {@link Specification}.
     */
    private Specification<ProgramCohortDataInput> createSpecification(ProgramCohortDataInputCriteria criteria) {
        Specification<ProgramCohortDataInput> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramCohortDataInput_.id));
            }
            if (criteria.getNameDataInput() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNameDataInput(), ProgramCohortDataInput_.nameDataInput));
            }
            if (criteria.getInputType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInputType(), ProgramCohortDataInput_.inputType));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramCohortDataInput_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramCohortDataInput_.modifiedDate));
            }
        }
        return specification;
    }
}
