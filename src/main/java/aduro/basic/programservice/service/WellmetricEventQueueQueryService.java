package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.WellmetricEventQueue;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.WellmetricEventQueueRepository;
import aduro.basic.programservice.service.dto.WellmetricEventQueueCriteria;
import aduro.basic.programservice.service.dto.WellmetricEventQueueDTO;
import aduro.basic.programservice.service.mapper.WellmetricEventQueueMapper;

/**
 * Service for executing complex queries for {@link WellmetricEventQueue} entities in the database.
 * The main input is a {@link WellmetricEventQueueCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link WellmetricEventQueueDTO} or a {@link Page} of {@link WellmetricEventQueueDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class WellmetricEventQueueQueryService extends QueryService<WellmetricEventQueue> {

    private final Logger log = LoggerFactory.getLogger(WellmetricEventQueueQueryService.class);

    private final WellmetricEventQueueRepository wellmetricEventQueueRepository;

    private final WellmetricEventQueueMapper wellmetricEventQueueMapper;

    public WellmetricEventQueueQueryService(WellmetricEventQueueRepository wellmetricEventQueueRepository, WellmetricEventQueueMapper wellmetricEventQueueMapper) {
        this.wellmetricEventQueueRepository = wellmetricEventQueueRepository;
        this.wellmetricEventQueueMapper = wellmetricEventQueueMapper;
    }

    /**
     * Return a {@link List} of {@link WellmetricEventQueueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<WellmetricEventQueueDTO> findByCriteria(WellmetricEventQueueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<WellmetricEventQueue> specification = createSpecification(criteria);
        return wellmetricEventQueueMapper.toDto(wellmetricEventQueueRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link WellmetricEventQueueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<WellmetricEventQueueDTO> findByCriteria(WellmetricEventQueueCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<WellmetricEventQueue> specification = createSpecification(criteria);
        return wellmetricEventQueueRepository.findAll(specification, page)
            .map(wellmetricEventQueueMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(WellmetricEventQueueCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<WellmetricEventQueue> specification = createSpecification(criteria);
        return wellmetricEventQueueRepository.count(specification);
    }

    /**
     * Function to convert WellmetricEventQueueCriteria to a {@link Specification}.
     */
    private Specification<WellmetricEventQueue> createSpecification(WellmetricEventQueueCriteria criteria) {
        Specification<WellmetricEventQueue> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), WellmetricEventQueue_.id));
            }
            if (criteria.getParticipantId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParticipantId(), WellmetricEventQueue_.participantId));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), WellmetricEventQueue_.clientId));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), WellmetricEventQueue_.email));
            }
            if (criteria.getErrorMessage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getErrorMessage(), WellmetricEventQueue_.errorMessage));
            }
            if (criteria.getResult() != null) {
                specification = specification.and(buildSpecification(criteria.getResult(), WellmetricEventQueue_.result));
            }
            if (criteria.getEventCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventCode(), WellmetricEventQueue_.eventCode));
            }
            if (criteria.getGender() != null) {
                specification = specification.and(buildSpecification(criteria.getGender(), WellmetricEventQueue_.gender));
            }
            if (criteria.getAttemptCount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAttemptCount(), WellmetricEventQueue_.attemptCount));
            }
            if (criteria.getLastAttemptTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastAttemptTime(), WellmetricEventQueue_.lastAttemptTime));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), WellmetricEventQueue_.createdAt));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), WellmetricEventQueue_.subgroupId));
            }
            if (criteria.getHealthyValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHealthyValue(), WellmetricEventQueue_.healthyValue));
            }
            if (criteria.getExecuteStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExecuteStatus(), WellmetricEventQueue_.executeStatus));
            }
            if (criteria.getEventId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventId(), WellmetricEventQueue_.eventId));
            }
            if (criteria.getHasIncentive() != null) {
                specification = specification.and(buildSpecification(criteria.getHasIncentive(), WellmetricEventQueue_.hasIncentive));
            }
            if (criteria.getIsWaitingPush() != null) {
                specification = specification.and(buildSpecification(criteria.getIsWaitingPush(), WellmetricEventQueue_.isWaitingPush));
            }
            if (criteria.getAttemptedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAttemptedAt(), WellmetricEventQueue_.attemptedAt));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramId(), WellmetricEventQueue_.programId));
            }
            if (criteria.getEventDate() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventDate(), WellmetricEventQueue_.eventDate));
            }
            if (criteria.getSubCategoryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubCategoryCode(), WellmetricEventQueue_.subCategoryCode));
            }
        }
        return specification;
    }
}
