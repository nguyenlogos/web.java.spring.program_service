package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramUserLevelProgress;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramUserLevelProgressRepository;
import aduro.basic.programservice.service.dto.ProgramUserLevelProgressCriteria;
import aduro.basic.programservice.service.dto.ProgramUserLevelProgressDTO;
import aduro.basic.programservice.service.mapper.ProgramUserLevelProgressMapper;

/**
 * Service for executing complex queries for {@link ProgramUserLevelProgress} entities in the database.
 * The main input is a {@link ProgramUserLevelProgressCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramUserLevelProgressDTO} or a {@link Page} of {@link ProgramUserLevelProgressDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramUserLevelProgressQueryService extends QueryService<ProgramUserLevelProgress> {

    private final Logger log = LoggerFactory.getLogger(ProgramUserLevelProgressQueryService.class);

    private final ProgramUserLevelProgressRepository programUserLevelProgressRepository;

    private final ProgramUserLevelProgressMapper programUserLevelProgressMapper;

    public ProgramUserLevelProgressQueryService(ProgramUserLevelProgressRepository programUserLevelProgressRepository, ProgramUserLevelProgressMapper programUserLevelProgressMapper) {
        this.programUserLevelProgressRepository = programUserLevelProgressRepository;
        this.programUserLevelProgressMapper = programUserLevelProgressMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramUserLevelProgressDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramUserLevelProgressDTO> findByCriteria(ProgramUserLevelProgressCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramUserLevelProgress> specification = createSpecification(criteria);
        return programUserLevelProgressMapper.toDto(programUserLevelProgressRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramUserLevelProgressDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramUserLevelProgressDTO> findByCriteria(ProgramUserLevelProgressCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramUserLevelProgress> specification = createSpecification(criteria);
        return programUserLevelProgressRepository.findAll(specification, page)
            .map(programUserLevelProgressMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramUserLevelProgressCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramUserLevelProgress> specification = createSpecification(criteria);
        return programUserLevelProgressRepository.count(specification);
    }

    /**
     * Function to convert ProgramUserLevelProgressCriteria to a {@link Specification}.
     */
    private Specification<ProgramUserLevelProgress> createSpecification(ProgramUserLevelProgressCriteria criteria) {
        Specification<ProgramUserLevelProgress> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramUserLevelProgress_.id));
            }
            if (criteria.getUserEventId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserEventId(), ProgramUserLevelProgress_.userEventId));
            }
            if (criteria.getProgramUserId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramUserId(), ProgramUserLevelProgress_.programUserId));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), ProgramUserLevelProgress_.createdAt));
            }
            if (criteria.getLevelUpAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevelUpAt(), ProgramUserLevelProgress_.levelUpAt));
            }
            if (criteria.getLevelFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevelFrom(), ProgramUserLevelProgress_.levelFrom));
            }
            if (criteria.getLevelTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevelTo(), ProgramUserLevelProgress_.levelTo));
            }
            if (criteria.getHasReward() != null) {
                specification = specification.and(buildSpecification(criteria.getHasReward(), ProgramUserLevelProgress_.hasReward));
            }
        }
        return specification;
    }
}
