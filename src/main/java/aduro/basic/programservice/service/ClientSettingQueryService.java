package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ClientSetting;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ClientSettingRepository;
import aduro.basic.programservice.service.dto.ClientSettingCriteria;
import aduro.basic.programservice.service.dto.ClientSettingDTO;
import aduro.basic.programservice.service.mapper.ClientSettingMapper;

/**
 * Service for executing complex queries for {@link ClientSetting} entities in the database.
 * The main input is a {@link ClientSettingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientSettingDTO} or a {@link Page} of {@link ClientSettingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientSettingQueryService extends QueryService<ClientSetting> {

    private final Logger log = LoggerFactory.getLogger(ClientSettingQueryService.class);

    private final ClientSettingRepository clientSettingRepository;

    private final ClientSettingMapper clientSettingMapper;

    public ClientSettingQueryService(ClientSettingRepository clientSettingRepository, ClientSettingMapper clientSettingMapper) {
        this.clientSettingRepository = clientSettingRepository;
        this.clientSettingMapper = clientSettingMapper;
    }

    /**
     * Return a {@link List} of {@link ClientSettingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientSettingDTO> findByCriteria(ClientSettingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ClientSetting> specification = createSpecification(criteria);
        return clientSettingMapper.toDto(clientSettingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientSettingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientSettingDTO> findByCriteria(ClientSettingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ClientSetting> specification = createSpecification(criteria);
        return clientSettingRepository.findAll(specification, page)
            .map(clientSettingMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ClientSettingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ClientSetting> specification = createSpecification(criteria);
        return clientSettingRepository.count(specification);
    }

    /**
     * Function to convert ClientSettingCriteria to a {@link Specification}.
     */
    private Specification<ClientSetting> createSpecification(ClientSettingCriteria criteria) {
        Specification<ClientSetting> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientSetting_.id));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), ClientSetting_.clientId));
            }
            if (criteria.getClientName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientName(), ClientSetting_.clientName));
            }
            if (criteria.getClientEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientEmail(), ClientSetting_.clientEmail));
            }
            if (criteria.getTangoAccountIdentifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTangoAccountIdentifier(), ClientSetting_.tangoAccountIdentifier));
            }
            if (criteria.getTangoCustomerIdentifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTangoCustomerIdentifier(), ClientSetting_.tangoCustomerIdentifier));
            }
            if (criteria.getTangoCurrentBalance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTangoCurrentBalance(), ClientSetting_.tangoCurrentBalance));
            }
            if (criteria.getTangoThresholdBalance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTangoThresholdBalance(), ClientSetting_.tangoThresholdBalance));
            }
            if (criteria.getTangoCreditToken() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTangoCreditToken(), ClientSetting_.tangoCreditToken));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), ClientSetting_.updatedDate));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUpdatedBy(), ClientSetting_.updatedBy));
            }
        }
        return specification;
    }
}
