package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.TermAndCondition;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.TermAndConditionRepository;
import aduro.basic.programservice.service.dto.TermAndConditionCriteria;
import aduro.basic.programservice.service.dto.TermAndConditionDTO;
import aduro.basic.programservice.service.mapper.TermAndConditionMapper;

/**
 * Service for executing complex queries for {@link TermAndCondition} entities in the database.
 * The main input is a {@link TermAndConditionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TermAndConditionDTO} or a {@link Page} of {@link TermAndConditionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TermAndConditionQueryService extends QueryService<TermAndCondition> {

    private final Logger log = LoggerFactory.getLogger(TermAndConditionQueryService.class);

    private final TermAndConditionRepository termAndConditionRepository;

    private final TermAndConditionMapper termAndConditionMapper;

    public TermAndConditionQueryService(TermAndConditionRepository termAndConditionRepository, TermAndConditionMapper termAndConditionMapper) {
        this.termAndConditionRepository = termAndConditionRepository;
        this.termAndConditionMapper = termAndConditionMapper;
    }

    /**
     * Return a {@link List} of {@link TermAndConditionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TermAndConditionDTO> findByCriteria(TermAndConditionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TermAndCondition> specification = createSpecification(criteria);
        return termAndConditionMapper.toDto(termAndConditionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TermAndConditionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TermAndConditionDTO> findByCriteria(TermAndConditionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TermAndCondition> specification = createSpecification(criteria);
        return termAndConditionRepository.findAll(specification, page)
            .map(termAndConditionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TermAndConditionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TermAndCondition> specification = createSpecification(criteria);
        return termAndConditionRepository.count(specification);
    }

    /**
     * Function to convert TermAndConditionCriteria to a {@link Specification}.
     */
    private Specification<TermAndCondition> createSpecification(TermAndConditionCriteria criteria) {
        Specification<TermAndCondition> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), TermAndCondition_.id));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), TermAndCondition_.clientId));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), TermAndCondition_.subgroupId));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), TermAndCondition_.title));
            }
            if (criteria.getContent() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContent(), TermAndCondition_.content));
            }
            if (criteria.getIsTargetSubgroup() != null) {
                specification = specification.and(buildSpecification(criteria.getIsTargetSubgroup(), TermAndCondition_.isTargetSubgroup));
            }
            if (criteria.getSubgroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupName(), TermAndCondition_.subgroupName));
            }
        }
        return specification;
    }
}
