package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramBiometricData;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramBiometricDataRepository;
import aduro.basic.programservice.service.dto.ProgramBiometricDataCriteria;
import aduro.basic.programservice.service.dto.ProgramBiometricDataDTO;
import aduro.basic.programservice.service.mapper.ProgramBiometricDataMapper;

/**
 * Service for executing complex queries for {@link ProgramBiometricData} entities in the database.
 * The main input is a {@link ProgramBiometricDataCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramBiometricDataDTO} or a {@link Page} of {@link ProgramBiometricDataDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramBiometricDataQueryService extends QueryService<ProgramBiometricData> {

    private final Logger log = LoggerFactory.getLogger(ProgramBiometricDataQueryService.class);

    private final ProgramBiometricDataRepository programBiometricDataRepository;

    private final ProgramBiometricDataMapper programBiometricDataMapper;

    public ProgramBiometricDataQueryService(ProgramBiometricDataRepository programBiometricDataRepository, ProgramBiometricDataMapper programBiometricDataMapper) {
        this.programBiometricDataRepository = programBiometricDataRepository;
        this.programBiometricDataMapper = programBiometricDataMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramBiometricDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramBiometricDataDTO> findByCriteria(ProgramBiometricDataCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramBiometricData> specification = createSpecification(criteria);
        return programBiometricDataMapper.toDto(programBiometricDataRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramBiometricDataDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramBiometricDataDTO> findByCriteria(ProgramBiometricDataCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramBiometricData> specification = createSpecification(criteria);
        // expect return all ps bio data 
        Pageable pageable =  PageRequest.of(0, 100, page.getSort());
        return programBiometricDataRepository.findAll(specification, pageable)
            .map(programBiometricDataMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramBiometricDataCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramBiometricData> specification = createSpecification(criteria);
        return programBiometricDataRepository.count(specification);
    }

    /**
     * Function to convert ProgramBiometricDataCriteria to a {@link Specification}.
     */
    private Specification<ProgramBiometricData> createSpecification(ProgramBiometricDataCriteria criteria) {
        Specification<ProgramBiometricData> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramBiometricData_.id));
            }
            if (criteria.getBiometricCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBiometricCode(), ProgramBiometricData_.biometricCode));
            }
            if (criteria.getBiometricName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBiometricName(), ProgramBiometricData_.biometricName));
            }
            if (criteria.getMaleMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaleMin(), ProgramBiometricData_.maleMin));
            }
            if (criteria.getMaleMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaleMax(), ProgramBiometricData_.maleMax));
            }
            if (criteria.getFemaleMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFemaleMin(), ProgramBiometricData_.femaleMin));
            }
            if (criteria.getFemaleMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFemaleMax(), ProgramBiometricData_.femaleMax));
            }
            if (criteria.getUnidentifiedMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUnidentifiedMin(), ProgramBiometricData_.unidentifiedMin));
            }
            if (criteria.getUnidentifiedMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUnidentifiedMax(), ProgramBiometricData_.unidentifiedMax));
            }
        }
        return specification;
    }
}
