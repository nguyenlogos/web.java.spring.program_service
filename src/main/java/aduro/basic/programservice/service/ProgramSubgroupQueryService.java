package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramSubgroup;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramSubgroupRepository;
import aduro.basic.programservice.service.dto.ProgramSubgroupCriteria;
import aduro.basic.programservice.service.dto.ProgramSubgroupDTO;
import aduro.basic.programservice.service.mapper.ProgramSubgroupMapper;

/**
 * Service for executing complex queries for {@link ProgramSubgroup} entities in the database.
 * The main input is a {@link ProgramSubgroupCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramSubgroupDTO} or a {@link Page} of {@link ProgramSubgroupDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramSubgroupQueryService extends QueryService<ProgramSubgroup> {

    private final Logger log = LoggerFactory.getLogger(ProgramSubgroupQueryService.class);

    private final ProgramSubgroupRepository programSubgroupRepository;

    private final ProgramSubgroupMapper programSubgroupMapper;

    public ProgramSubgroupQueryService(ProgramSubgroupRepository programSubgroupRepository, ProgramSubgroupMapper programSubgroupMapper) {
        this.programSubgroupRepository = programSubgroupRepository;
        this.programSubgroupMapper = programSubgroupMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramSubgroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramSubgroupDTO> findByCriteria(ProgramSubgroupCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramSubgroup> specification = createSpecification(criteria);
        return programSubgroupMapper.toDto(programSubgroupRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramSubgroupDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramSubgroupDTO> findByCriteria(ProgramSubgroupCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramSubgroup> specification = createSpecification(criteria);
        return programSubgroupRepository.findAll(specification, page)
            .map(programSubgroupMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramSubgroupCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramSubgroup> specification = createSpecification(criteria);
        return programSubgroupRepository.count(specification);
    }

    /**
     * Function to convert ProgramSubgroupCriteria to a {@link Specification}.
     */
    private Specification<ProgramSubgroup> createSpecification(ProgramSubgroupCriteria criteria) {
        Specification<ProgramSubgroup> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramSubgroup_.id));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), ProgramSubgroup_.subgroupId));
            }
            if (criteria.getSubgroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupName(), ProgramSubgroup_.subgroupName));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramSubgroup_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }
}
