package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCohortRuleDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramCohortRule}.
 */
public interface ProgramCohortRuleService {

    /**
     * Save a programCohortRule.
     *
     * @param programCohortRuleDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramCohortRuleDTO save(ProgramCohortRuleDTO programCohortRuleDTO);

    /**
     * Get all the programCohortRules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramCohortRuleDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programCohortRule.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCohortRuleDTO> findOne(Long id);

    /**
     * Delete the "id" programCohortRule.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
