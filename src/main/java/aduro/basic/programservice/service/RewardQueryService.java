package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.Reward;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.RewardRepository;
import aduro.basic.programservice.service.dto.RewardCriteria;
import aduro.basic.programservice.service.dto.RewardDTO;
import aduro.basic.programservice.service.mapper.RewardMapper;

/**
 * Service for executing complex queries for {@link Reward} entities in the database.
 * The main input is a {@link RewardCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link RewardDTO} or a {@link Page} of {@link RewardDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class RewardQueryService extends QueryService<Reward> {

    private final Logger log = LoggerFactory.getLogger(RewardQueryService.class);

    private final RewardRepository rewardRepository;

    private final RewardMapper rewardMapper;

    public RewardQueryService(RewardRepository rewardRepository, RewardMapper rewardMapper) {
        this.rewardRepository = rewardRepository;
        this.rewardMapper = rewardMapper;
    }

    /**
     * Return a {@link List} of {@link RewardDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<RewardDTO> findByCriteria(RewardCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Reward> specification = createSpecification(criteria);
        return rewardMapper.toDto(rewardRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link RewardDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<RewardDTO> findByCriteria(RewardCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Reward> specification = createSpecification(criteria);
        return rewardRepository.findAll(specification, page)
            .map(rewardMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(RewardCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Reward> specification = createSpecification(criteria);
        return rewardRepository.count(specification);
    }

    /**
     * Function to convert RewardCriteria to a {@link Specification}.
     */
    private Specification<Reward> createSpecification(RewardCriteria criteria) {
        Specification<Reward> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Reward_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Reward_.description));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Reward_.code));
            }
        }
        return specification;
    }
}
