package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramSubCategoryConfigurationRepository;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationCriteria;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationDTO;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryConfigurationMapper;

/**
 * Service for executing complex queries for {@link ProgramSubCategoryConfiguration} entities in the database.
 * The main input is a {@link ProgramSubCategoryConfigurationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramSubCategoryConfigurationDTO} or a {@link Page} of {@link ProgramSubCategoryConfigurationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramSubCategoryConfigurationQueryService extends QueryService<ProgramSubCategoryConfiguration> {

    private final Logger log = LoggerFactory.getLogger(ProgramSubCategoryConfigurationQueryService.class);

    private final ProgramSubCategoryConfigurationRepository programSubCategoryConfigurationRepository;

    private final ProgramSubCategoryConfigurationMapper programSubCategoryConfigurationMapper;

    public ProgramSubCategoryConfigurationQueryService(ProgramSubCategoryConfigurationRepository programSubCategoryConfigurationRepository, ProgramSubCategoryConfigurationMapper programSubCategoryConfigurationMapper) {
        this.programSubCategoryConfigurationRepository = programSubCategoryConfigurationRepository;
        this.programSubCategoryConfigurationMapper = programSubCategoryConfigurationMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramSubCategoryConfigurationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramSubCategoryConfigurationDTO> findByCriteria(ProgramSubCategoryConfigurationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramSubCategoryConfiguration> specification = createSpecification(criteria);
        return programSubCategoryConfigurationMapper.toDto(programSubCategoryConfigurationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramSubCategoryConfigurationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramSubCategoryConfigurationDTO> findByCriteria(ProgramSubCategoryConfigurationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramSubCategoryConfiguration> specification = createSpecification(criteria);
        return programSubCategoryConfigurationRepository.findAll(specification, page)
            .map(programSubCategoryConfigurationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramSubCategoryConfigurationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramSubCategoryConfiguration> specification = createSpecification(criteria);
        return programSubCategoryConfigurationRepository.count(specification);
    }

    /**
     * Function to convert ProgramSubCategoryConfigurationCriteria to a {@link Specification}.
     */
    private Specification<ProgramSubCategoryConfiguration> createSpecification(ProgramSubCategoryConfigurationCriteria criteria) {
        Specification<ProgramSubCategoryConfiguration> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramSubCategoryConfiguration_.id));
            }
            if (criteria.getSubCategoryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubCategoryCode(), ProgramSubCategoryConfiguration_.subCategoryCode));
            }
            if (criteria.getIsBloodPressureSingleTest() != null) {
                specification = specification.and(buildSpecification(criteria.getIsBloodPressureSingleTest(), ProgramSubCategoryConfiguration_.isBloodPressureSingleTest));
            }
            if (criteria.getIsBloodPressureIndividualTest() != null) {
                specification = specification.and(buildSpecification(criteria.getIsBloodPressureIndividualTest(), ProgramSubCategoryConfiguration_.isBloodPressureIndividualTest));
            }
            if (criteria.getIsGlucoseAwardedInRange() != null) {
                specification = specification.and(buildSpecification(criteria.getIsGlucoseAwardedInRange(), ProgramSubCategoryConfiguration_.isGlucoseAwardedInRange));
            }
            if (criteria.getIsBmiAwardedInRange() != null) {
                specification = specification.and(buildSpecification(criteria.getIsBmiAwardedInRange(), ProgramSubCategoryConfiguration_.isBmiAwardedInRange));
            }
            if (criteria.getIsBmiAwardedFasting() != null) {
                specification = specification.and(buildSpecification(criteria.getIsBmiAwardedFasting(), ProgramSubCategoryConfiguration_.isBmiAwardedFasting));
            }
            if (criteria.getIsBmiAwardedNonFasting() != null) {
                specification = specification.and(buildSpecification(criteria.getIsBmiAwardedNonFasting(), ProgramSubCategoryConfiguration_.isBmiAwardedNonFasting));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramId(), ProgramSubCategoryConfiguration_.programId));
            }
        }
        return specification;
    }
}
