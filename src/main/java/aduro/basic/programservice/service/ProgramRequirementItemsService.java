package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramRequirementItemsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramRequirementItems}.
 */
public interface ProgramRequirementItemsService {

    /**
     * Save a programRequirementItems.
     *
     * @param programRequirementItemsDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramRequirementItemsDTO save(ProgramRequirementItemsDTO programRequirementItemsDTO);

    /**
     * Get all the programRequirementItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramRequirementItemsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programRequirementItems.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramRequirementItemsDTO> findOne(Long id);

    /**
     * Delete the "id" programRequirementItems.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
