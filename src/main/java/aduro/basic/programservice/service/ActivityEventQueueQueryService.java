package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ActivityEventQueue;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ActivityEventQueueRepository;
import aduro.basic.programservice.service.dto.ActivityEventQueueCriteria;
import aduro.basic.programservice.service.dto.ActivityEventQueueDTO;
import aduro.basic.programservice.service.mapper.ActivityEventQueueMapper;

/**
 * Service for executing complex queries for {@link ActivityEventQueue} entities in the database.
 * The main input is a {@link ActivityEventQueueCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ActivityEventQueueDTO} or a {@link Page} of {@link ActivityEventQueueDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ActivityEventQueueQueryService extends QueryService<ActivityEventQueue> {

    private final Logger log = LoggerFactory.getLogger(ActivityEventQueueQueryService.class);

    private final ActivityEventQueueRepository activityEventQueueRepository;

    private final ActivityEventQueueMapper activityEventQueueMapper;

    public ActivityEventQueueQueryService(ActivityEventQueueRepository activityEventQueueRepository, ActivityEventQueueMapper activityEventQueueMapper) {
        this.activityEventQueueRepository = activityEventQueueRepository;
        this.activityEventQueueMapper = activityEventQueueMapper;
    }

    /**
     * Return a {@link List} of {@link ActivityEventQueueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ActivityEventQueueDTO> findByCriteria(ActivityEventQueueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ActivityEventQueue> specification = createSpecification(criteria);
        return activityEventQueueMapper.toDto(activityEventQueueRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ActivityEventQueueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ActivityEventQueueDTO> findByCriteria(ActivityEventQueueCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ActivityEventQueue> specification = createSpecification(criteria);
        return activityEventQueueRepository.findAll(specification, page)
            .map(activityEventQueueMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ActivityEventQueueCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ActivityEventQueue> specification = createSpecification(criteria);
        return activityEventQueueRepository.count(specification);
    }

    /**
     * Function to convert ActivityEventQueueCriteria to a {@link Specification}.
     */
    private Specification<ActivityEventQueue> createSpecification(ActivityEventQueueCriteria criteria) {
        Specification<ActivityEventQueue> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ActivityEventQueue_.id));
            }
            if (criteria.getParticipantId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParticipantId(), ActivityEventQueue_.participantId));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), ActivityEventQueue_.clientId));
            }
            if (criteria.getActivityCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivityCode(), ActivityEventQueue_.activityCode));
            }
            if (criteria.getFirstName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFirstName(), ActivityEventQueue_.firstName));
            }
            if (criteria.getLastName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastName(), ActivityEventQueue_.lastName));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), ActivityEventQueue_.email));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ActivityEventQueue_.createdDate));
            }
            if (criteria.getTransactionId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTransactionId(), ActivityEventQueue_.transactionId));
            }
            if (criteria.getExecuteStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExecuteStatus(), ActivityEventQueue_.executeStatus));
            }
            if (criteria.getErrorMessage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getErrorMessage(), ActivityEventQueue_.errorMessage));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), ActivityEventQueue_.subgroupId));
            }
        }
        return specification;
    }
}
