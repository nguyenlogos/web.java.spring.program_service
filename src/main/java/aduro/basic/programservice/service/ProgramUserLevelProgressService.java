package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramUserLevelProgressDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramUserLevelProgress}.
 */
public interface ProgramUserLevelProgressService {

    /**
     * Save a programUserLevelProgress.
     *
     * @param programUserLevelProgressDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramUserLevelProgressDTO save(ProgramUserLevelProgressDTO programUserLevelProgressDTO);

    /**
     * Get all the programUserLevelProgresses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramUserLevelProgressDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programUserLevelProgress.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramUserLevelProgressDTO> findOne(Long id);

    /**
     * Delete the "id" programUserLevelProgress.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
