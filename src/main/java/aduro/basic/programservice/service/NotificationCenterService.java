package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.NotificationCenterDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.NotificationCenter}.
 */
public interface NotificationCenterService {

    /**
     * Save a notificationCenter.
     *
     * @param notificationCenterDTO the entity to save.
     * @return the persisted entity.
     */
    NotificationCenterDTO save(NotificationCenterDTO notificationCenterDTO);

    /**
     * Get all the notificationCenters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<NotificationCenterDTO> findAll(Pageable pageable);


    /**
     * Get the "id" notificationCenter.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<NotificationCenterDTO> findOne(Long id);

    /**
     * Delete the "id" notificationCenter.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
