package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramLevelPath;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramLevelPathRepository;
import aduro.basic.programservice.service.dto.ProgramLevelPathCriteria;
import aduro.basic.programservice.service.dto.ProgramLevelPathDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelPathMapper;

/**
 * Service for executing complex queries for {@link ProgramLevelPath} entities in the database.
 * The main input is a {@link ProgramLevelPathCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramLevelPathDTO} or a {@link Page} of {@link ProgramLevelPathDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramLevelPathQueryService extends QueryService<ProgramLevelPath> {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelPathQueryService.class);

    private final ProgramLevelPathRepository programLevelPathRepository;

    private final ProgramLevelPathMapper programLevelPathMapper;

    public ProgramLevelPathQueryService(ProgramLevelPathRepository programLevelPathRepository, ProgramLevelPathMapper programLevelPathMapper) {
        this.programLevelPathRepository = programLevelPathRepository;
        this.programLevelPathMapper = programLevelPathMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramLevelPathDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramLevelPathDTO> findByCriteria(ProgramLevelPathCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramLevelPath> specification = createSpecification(criteria);
        return programLevelPathMapper.toDto(programLevelPathRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramLevelPathDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramLevelPathDTO> findByCriteria(ProgramLevelPathCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramLevelPath> specification = createSpecification(criteria);
        return programLevelPathRepository.findAll(specification, page)
            .map(programLevelPathMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramLevelPathCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramLevelPath> specification = createSpecification(criteria);
        return programLevelPathRepository.count(specification);
    }

    /**
     * Function to convert ProgramLevelPathCriteria to a {@link Specification}.
     */
    private Specification<ProgramLevelPath> createSpecification(ProgramLevelPathCriteria criteria) {
        Specification<ProgramLevelPath> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramLevelPath_.id));
            }
            if (criteria.getPathId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPathId(), ProgramLevelPath_.pathId));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ProgramLevelPath_.name));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), ProgramLevelPath_.subgroupId));
            }
            if (criteria.getSubgroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupName(), ProgramLevelPath_.subgroupName));
            }
            if (criteria.getPathType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPathType(), ProgramLevelPath_.pathType));
            }
            if (criteria.getPathCategory() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPathCategory(), ProgramLevelPath_.pathCategory));
            }
            if (criteria.getProgramLevelId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramLevelId(),
                    root -> root.join(ProgramLevelPath_.programLevel, JoinType.LEFT).get(ProgramLevel_.id)));
            }
        }
        return specification;
    }
}
