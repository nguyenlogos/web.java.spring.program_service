package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramCollectionService;
import aduro.basic.programservice.domain.ProgramCollection;
import aduro.basic.programservice.repository.ProgramCollectionRepository;
import aduro.basic.programservice.service.dto.ProgramCollectionDTO;
import aduro.basic.programservice.service.mapper.ProgramCollectionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramCollection}.
 */
@Service
@Transactional
public class ProgramCollectionServiceImpl implements ProgramCollectionService {

    private final Logger log = LoggerFactory.getLogger(ProgramCollectionServiceImpl.class);

    private final ProgramCollectionRepository programCollectionRepository;

    private final ProgramCollectionMapper programCollectionMapper;

    public ProgramCollectionServiceImpl(ProgramCollectionRepository programCollectionRepository, ProgramCollectionMapper programCollectionMapper) {
        this.programCollectionRepository = programCollectionRepository;
        this.programCollectionMapper = programCollectionMapper;
    }

    /**
     * Save a programCollection.
     *
     * @param programCollectionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramCollectionDTO save(ProgramCollectionDTO programCollectionDTO) {
        log.debug("Request to save ProgramCollection : {}", programCollectionDTO);
        ProgramCollection programCollection = programCollectionMapper.toEntity(programCollectionDTO);
        programCollection = programCollectionRepository.save(programCollection);
        return programCollectionMapper.toDto(programCollection);
    }

    /**
     * Get all the programCollections.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramCollectionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramCollections");
        return programCollectionRepository.findAll(pageable)
            .map(programCollectionMapper::toDto);
    }


    /**
     * Get one programCollection by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramCollectionDTO> findOne(Long id) {
        log.debug("Request to get ProgramCollection : {}", id);
        return programCollectionRepository.findById(id)
            .map(programCollectionMapper::toDto);
    }

    /**
     * Delete the programCollection by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramCollection : {}", id);
        programCollectionRepository.deleteById(id);
    }
}
