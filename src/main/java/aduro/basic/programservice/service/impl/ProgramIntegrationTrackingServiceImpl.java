package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramIntegrationTrackingService;
import aduro.basic.programservice.domain.ProgramIntegrationTracking;
import aduro.basic.programservice.repository.ProgramIntegrationTrackingRepository;
import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingDTO;
import aduro.basic.programservice.service.mapper.ProgramIntegrationTrackingMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramIntegrationTracking}.
 */
@Service
@Transactional
public class ProgramIntegrationTrackingServiceImpl implements ProgramIntegrationTrackingService {

    private final Logger log = LoggerFactory.getLogger(ProgramIntegrationTrackingServiceImpl.class);

    private final ProgramIntegrationTrackingRepository programIntegrationTrackingRepository;

    private final ProgramIntegrationTrackingMapper programIntegrationTrackingMapper;

    public ProgramIntegrationTrackingServiceImpl(ProgramIntegrationTrackingRepository programIntegrationTrackingRepository, ProgramIntegrationTrackingMapper programIntegrationTrackingMapper) {
        this.programIntegrationTrackingRepository = programIntegrationTrackingRepository;
        this.programIntegrationTrackingMapper = programIntegrationTrackingMapper;
    }

    /**
     * Save a programIntegrationTracking.
     *
     * @param programIntegrationTrackingDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramIntegrationTrackingDTO save(ProgramIntegrationTrackingDTO programIntegrationTrackingDTO) {
        log.debug("Request to save ProgramIntegrationTracking : {}", programIntegrationTrackingDTO);
        ProgramIntegrationTracking programIntegrationTracking = programIntegrationTrackingMapper.toEntity(programIntegrationTrackingDTO);
        programIntegrationTracking = programIntegrationTrackingRepository.save(programIntegrationTracking);
        return programIntegrationTrackingMapper.toDto(programIntegrationTracking);
    }

    /**
     * Get all the programIntegrationTrackings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramIntegrationTrackingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramIntegrationTrackings");
        return programIntegrationTrackingRepository.findAll(pageable)
            .map(programIntegrationTrackingMapper::toDto);
    }


    /**
     * Get one programIntegrationTracking by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramIntegrationTrackingDTO> findOne(Long id) {
        log.debug("Request to get ProgramIntegrationTracking : {}", id);
        return programIntegrationTrackingRepository.findById(id)
            .map(programIntegrationTrackingMapper::toDto);
    }

    /**
     * Delete the programIntegrationTracking by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramIntegrationTracking : {}", id);
        programIntegrationTrackingRepository.deleteById(id);
    }
}
