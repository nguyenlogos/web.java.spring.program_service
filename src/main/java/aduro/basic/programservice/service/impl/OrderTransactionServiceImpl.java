package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.OrderTransactionService;
import aduro.basic.programservice.domain.OrderTransaction;
import aduro.basic.programservice.repository.OrderTransactionRepository;
import aduro.basic.programservice.service.dto.OrderTransactionDTO;
import aduro.basic.programservice.service.mapper.OrderTransactionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderTransaction}.
 */
@Service
@Transactional
public class OrderTransactionServiceImpl implements OrderTransactionService {

    private final Logger log = LoggerFactory.getLogger(OrderTransactionServiceImpl.class);

    private final OrderTransactionRepository orderTransactionRepository;

    private final OrderTransactionMapper orderTransactionMapper;

    public OrderTransactionServiceImpl(OrderTransactionRepository orderTransactionRepository, OrderTransactionMapper orderTransactionMapper) {
        this.orderTransactionRepository = orderTransactionRepository;
        this.orderTransactionMapper = orderTransactionMapper;
    }

    /**
     * Save a orderTransaction.
     *
     * @param orderTransactionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public OrderTransactionDTO save(OrderTransactionDTO orderTransactionDTO) {
        log.debug("Request to save OrderTransaction : {}", orderTransactionDTO);
        OrderTransaction orderTransaction = orderTransactionMapper.toEntity(orderTransactionDTO);
        orderTransaction = orderTransactionRepository.save(orderTransaction);
        return orderTransactionMapper.toDto(orderTransaction);
    }

    /**
     * Get all the orderTransactions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<OrderTransactionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderTransactions");
        return orderTransactionRepository.findAll(pageable)
            .map(orderTransactionMapper::toDto);
    }


    /**
     * Get one orderTransaction by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OrderTransactionDTO> findOne(Long id) {
        log.debug("Request to get OrderTransaction : {}", id);
        return orderTransactionRepository.findById(id)
            .map(orderTransactionMapper::toDto);
    }

    /**
     * Delete the orderTransaction by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderTransaction : {}", id);
        orderTransactionRepository.deleteById(id);
    }
}
