package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.repository.ProgramMemberConsentDetailRepository;
import aduro.basic.programservice.service.ProgramMemberConsentDetailService;
import aduro.basic.programservice.service.dto.ProgramMemberConsentDetailDTO;
import aduro.basic.programservice.domain.ProgramMemberConsentDetail;
import aduro.basic.programservice.service.mapper.ProgramMemberConsentDetailMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing {@link ProgramMemberConsentDetail}.
 */
@Service
@Transactional
public class ProgramMemberConsentDetailServiceImpl implements ProgramMemberConsentDetailService {

    private final Logger log = LoggerFactory.getLogger(ProgramMemberConsentDetailServiceImpl.class);

    private final ProgramMemberConsentDetailRepository programMemberConsentDetailRepository;

    private final ProgramMemberConsentDetailMapper programMemberConsentDetailMapper;

    public ProgramMemberConsentDetailServiceImpl(ProgramMemberConsentDetailRepository programMemberConsentDetailRepository, ProgramMemberConsentDetailMapper programMemberConsentDetailMapper) {
        this.programMemberConsentDetailRepository = programMemberConsentDetailRepository;
        this.programMemberConsentDetailMapper = programMemberConsentDetailMapper;
    }


    @Override
    public ProgramMemberConsentDetailDTO createConsentAccept(ProgramMemberConsentDetailDTO programMemberConsentDetailDTO) {
        log.debug("Request to save TermAndCondition : {}", programMemberConsentDetailDTO);
        ProgramMemberConsentDetail programMemberConsentDetail = programMemberConsentDetailMapper.toEntity(programMemberConsentDetailDTO);
        programMemberConsentDetail = programMemberConsentDetailRepository.save(programMemberConsentDetail);
        return programMemberConsentDetailMapper.toDto(programMemberConsentDetail);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProgramMemberConsentDetail> getConsentAcceptByDuring(Long consentId, Long programId)
    {
        List<ProgramMemberConsentDetail> programMemberConsentDetail = programMemberConsentDetailRepository.findByConsentIdAndProgramId(consentId, programId);
        return programMemberConsentDetail;
    }

    @Override
    @Transactional(readOnly = true)
    public List<ProgramMemberConsentDetailDTO> getConsentByParticipantIdAndClientIdAndConsentIdAndProgramId(String participantId, String clientId, Long consentId, Long programId)
    {
        List<ProgramMemberConsentDetail> programMemberConsentDetail = programMemberConsentDetailRepository.findByParticipantIdAndClientIdAndConsentIdAndProgramIdOrderByCreatedDateDesc(participantId, clientId, consentId, programId);
        return programMemberConsentDetailMapper.toDto(programMemberConsentDetail);
    }
    @Override
    @Transactional(readOnly = true)
    public List<ProgramMemberConsentDetailDTO> getConsentByParticipantIdAndClientIdAndConsentId(String participantId, String clientId, Long consentId)
    {
        List<ProgramMemberConsentDetail> programMemberConsentDetail = programMemberConsentDetailRepository.findByParticipantIdAndClientIdAndConsentIdOrderByCreatedDateDesc(participantId, clientId, consentId);
        return programMemberConsentDetailMapper.toDto(programMemberConsentDetail);
    }
}
