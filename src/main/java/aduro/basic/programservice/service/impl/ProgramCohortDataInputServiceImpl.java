package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramCohortDataInputService;
import aduro.basic.programservice.domain.ProgramCohortDataInput;
import aduro.basic.programservice.repository.ProgramCohortDataInputRepository;
import aduro.basic.programservice.service.dto.ProgramCohortDataInputDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortDataInputMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramCohortDataInput}.
 */
@Service
@Transactional
public class ProgramCohortDataInputServiceImpl implements ProgramCohortDataInputService {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortDataInputServiceImpl.class);

    private final ProgramCohortDataInputRepository programCohortDataInputRepository;

    private final ProgramCohortDataInputMapper programCohortDataInputMapper;

    public ProgramCohortDataInputServiceImpl(ProgramCohortDataInputRepository programCohortDataInputRepository, ProgramCohortDataInputMapper programCohortDataInputMapper) {
        this.programCohortDataInputRepository = programCohortDataInputRepository;
        this.programCohortDataInputMapper = programCohortDataInputMapper;
    }

    /**
     * Save a programCohortDataInput.
     *
     * @param programCohortDataInputDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramCohortDataInputDTO save(ProgramCohortDataInputDTO programCohortDataInputDTO) {
        log.debug("Request to save ProgramCohortDataInput : {}", programCohortDataInputDTO);
        ProgramCohortDataInput programCohortDataInput = programCohortDataInputMapper.toEntity(programCohortDataInputDTO);
        programCohortDataInput = programCohortDataInputRepository.save(programCohortDataInput);
        return programCohortDataInputMapper.toDto(programCohortDataInput);
    }

    /**
     * Get all the programCohortDataInputs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramCohortDataInputDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramCohortDataInputs");
        return programCohortDataInputRepository.findAll(pageable)
            .map(programCohortDataInputMapper::toDto);
    }


    /**
     * Get one programCohortDataInput by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramCohortDataInputDTO> findOne(Long id) {
        log.debug("Request to get ProgramCohortDataInput : {}", id);
        return programCohortDataInputRepository.findById(id)
            .map(programCohortDataInputMapper::toDto);
    }

    /**
     * Delete the programCohortDataInput by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramCohortDataInput : {}", id);
        programCohortDataInputRepository.deleteById(id);
    }
}
