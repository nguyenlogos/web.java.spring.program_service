package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramHealthyRangeService;
import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.repository.ProgramHealthyRangeRepository;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.mapper.ProgramHealthyRangeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramHealthyRange}.
 */
@Service
@Transactional
public class ProgramHealthyRangeServiceImpl implements ProgramHealthyRangeService {

    private final Logger log = LoggerFactory.getLogger(ProgramHealthyRangeServiceImpl.class);

    private final ProgramHealthyRangeRepository programHealthyRangeRepository;

    private final ProgramHealthyRangeMapper programHealthyRangeMapper;

    public ProgramHealthyRangeServiceImpl(ProgramHealthyRangeRepository programHealthyRangeRepository, ProgramHealthyRangeMapper programHealthyRangeMapper) {
        this.programHealthyRangeRepository = programHealthyRangeRepository;
        this.programHealthyRangeMapper = programHealthyRangeMapper;
    }

    /**
     * Save a programHealthyRange.
     *
     * @param programHealthyRangeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramHealthyRangeDTO save(ProgramHealthyRangeDTO programHealthyRangeDTO) {
        log.debug("Request to save ProgramHealthyRange : {}", programHealthyRangeDTO);
        ProgramHealthyRange programHealthyRange = programHealthyRangeMapper.toEntity(programHealthyRangeDTO);
        programHealthyRange = programHealthyRangeRepository.save(programHealthyRange);
        return programHealthyRangeMapper.toDto(programHealthyRange);
    }

    /**
     * Get all the programHealthyRanges.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramHealthyRangeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramHealthyRanges");
        return programHealthyRangeRepository.findAll(pageable)
            .map(programHealthyRangeMapper::toDto);
    }


    /**
     * Get one programHealthyRange by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramHealthyRangeDTO> findOne(Long id) {
        log.debug("Request to get ProgramHealthyRange : {}", id);
        return programHealthyRangeRepository.findById(id)
            .map(programHealthyRangeMapper::toDto);
    }

    /**
     * Delete the programHealthyRange by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramHealthyRange : {}", id);
        programHealthyRangeRepository.deleteById(id);
    }
}
