package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.service.ClientProgramRewardSettingService;
import aduro.basic.programservice.domain.ClientProgramRewardSetting;
import aduro.basic.programservice.repository.ClientProgramRewardSettingRepository;
import aduro.basic.programservice.service.dto.ClientProgramRewardSettingDTO;
import aduro.basic.programservice.service.mapper.ClientProgramRewardSettingMapper;
import aduro.basic.programservice.tangocard.CustomerAdapter;
import aduro.basic.programservice.tangocard.dto.AccountRequest;
import aduro.basic.programservice.tangocard.dto.AccountResponse;
import aduro.basic.programservice.tangocard.dto.CustomerRequest;
import aduro.basic.programservice.tangocard.dto.CustomerResponse;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link ClientProgramRewardSetting}.
 */
@Service
@Transactional
public class ClientProgramRewardSettingServiceImpl implements ClientProgramRewardSettingService {

    private final Logger log = LoggerFactory.getLogger(ClientProgramRewardSettingServiceImpl.class);

    private final ClientProgramRewardSettingRepository clientProgramRewardSettingRepository;

    private final ClientProgramRewardSettingMapper clientProgramRewardSettingMapper;

    @Autowired
    private CustomerAdapter customerAdapter;

    public ClientProgramRewardSettingServiceImpl(ClientProgramRewardSettingRepository clientProgramRewardSettingRepository, ClientProgramRewardSettingMapper clientProgramRewardSettingMapper) {
        this.clientProgramRewardSettingRepository = clientProgramRewardSettingRepository;
        this.clientProgramRewardSettingMapper = clientProgramRewardSettingMapper;
    }

    /**
     * Save a clientProgramRewardSetting.
     *
     * @param clientProgramRewardSettingDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ClientProgramRewardSettingDTO save(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) {
        log.debug("Request to save ClientProgramRewardSetting : {}", clientProgramRewardSettingDTO);
        if(isNullOrEmpty(clientProgramRewardSettingDTO.getClientName()) || isNullOrEmpty(clientProgramRewardSettingDTO.getClientId()) || isNullOrEmpty(clientProgramRewardSettingDTO.getProgramName()))
            throw new BadRequestAlertException("Missing required fields", "ClientProgramRewardSetting", "ClientProgramRewardSetting");

        clientProgramRewardSettingDTO.setUpdatedDate(Instant.now());

        ClientProgramRewardSetting clientProgramRewardSetting = clientProgramRewardSettingMapper.toEntity(clientProgramRewardSettingDTO);


        clientProgramRewardSetting = clientProgramRewardSettingRepository.save(clientProgramRewardSetting);
        return clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);
    }

    /**
     * Get all the clientProgramRewardSettings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ClientProgramRewardSettingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientProgramRewardSettings");
        return clientProgramRewardSettingRepository.findAll(pageable)
            .map(clientProgramRewardSettingMapper::toDto);
    }


    /**
     * Get one clientProgramRewardSetting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ClientProgramRewardSettingDTO> findOne(Long id) throws Exception {
        log.debug("Request to get ClientProgramRewardSetting : {}", id);
        Optional<ClientProgramRewardSettingDTO> optionalClientProgramRewardSettingDTO =  clientProgramRewardSettingRepository.findById(id)
            .map(clientProgramRewardSettingMapper::toDto);

        if(optionalClientProgramRewardSettingDTO.isPresent()){
            ClientProgramRewardSettingDTO dto = optionalClientProgramRewardSettingDTO.get();

          /*  if(isNullOrEmpty(dto.getAccessToken())) return optionalClientProgramRewardSettingDTO;
            CompletableFuture<FundingSourceDTO> response = teamAdapter.GetFundingSourceAsync(optionalClientProgramRewardSettingDTO.get().getAccessToken());
            FundingSourceDTO fundingSourceDTO = response.get();
            if(fundingSourceDTO.getFunding_sources().size()>0){
                FundingSourceResponse fd = fundingSourceDTO.getFunding_sources().get(0);
                dto.setFundingId(fd.getId());
                dto.setMethod(fd.getMethod());
                dto.setAvailable_cents(fd.getMeta().getAvailable_cents());
                return  Optional.of(dto);
            }*/


        }
        return optionalClientProgramRewardSettingDTO;


    }

    /**
     * Delete the clientProgramRewardSetting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClientProgramRewardSetting : {}", id);
        clientProgramRewardSettingRepository.deleteById(id);
    }

    @Override
    public ClientProgramRewardSettingDTO generateAccessToken(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception {

        if(clientProgramRewardSettingDTO.getId() == null || clientProgramRewardSettingDTO.getId() == 0 || isNullOrEmpty(clientProgramRewardSettingDTO.getClientName()))
            throw new BadRequestAlertException("Setting not found", "ClientProgramRewardSetting", "ClientProgramRewardSetting");
        //generate access token
      /*  CompletableFuture<TeamTokenReponse> response = teamAdapter.GenerateTokenAsync(clientProgramRewardSettingDTO.getTeamId());
        TeamTokenReponse teamTokenReponse = response.get();*/

        //update token
        ClientProgramRewardSetting clientProgramRewardSetting = clientProgramRewardSettingRepository.getOne(clientProgramRewardSettingDTO.getId());
       // clientProgramRewardSetting.setAccessToken(teamTokenReponse.getAccess_token());
        clientProgramRewardSetting = clientProgramRewardSettingRepository.save(clientProgramRewardSetting);
        return clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);

    }

    @Override
    public ClientProgramRewardSettingDTO createTremendousTeam(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception {
        if(isNullOrEmpty(clientProgramRewardSettingDTO.getClientName()) || isNullOrEmpty(clientProgramRewardSettingDTO.getProgramName())
            || clientProgramRewardSettingDTO.getProgramId() == null || clientProgramRewardSettingDTO.getProgramId() == 0)
            throw new BadRequestAlertException("Missing required fields", "ClientProgramRewardSetting", "ClientProgramRewardSetting");

     //   clientProgramRewardSettingDTO.setTeamName(String.format("%s-%s",clientProgramRewardSettingDTO.getClientName(),clientProgramRewardSettingDTO.getProgramName()));
        /*TeamRequest teamRequest = new TeamRequest();
        teamRequest.setName(clientProgramRewardSettingDTO.getTeamName());
        teamRequest.setWebsite(clientProgramRewardSettingDTO.getWebsite());*/

      /*  //create tremendous team
        CompletableFuture<TeamDetailDTO> response = teamAdapter.CreateTeamAysnc(teamRequest);
        TeamDetailDTO teamDetailDTO = null;
        try{
             teamDetailDTO =  response.get();

        }catch(Exception ex){
            throw new BadRequestAlertException(ex.getMessage(), "ClientProgramRewardSetting", "ClientProgramRewardSetting");
        }

        clientProgramRewardSettingDTO.setTeamId(teamDetailDTO.getOrganization().getId());*/
        return save(clientProgramRewardSettingDTO);
    }

    @Override
    public ClientProgramRewardSettingDTO createTangoAccount(ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception {

        if(isNullOrEmpty(clientProgramRewardSettingDTO.getClientName()) || isNullOrEmpty(clientProgramRewardSettingDTO.getClientId())
            || clientProgramRewardSettingDTO.getProgramId() == null || clientProgramRewardSettingDTO.getProgramId() == 0 || isNullOrEmpty(clientProgramRewardSettingDTO.getClientEmail()))
            throw new BadRequestAlertException("Missing required fields", "ClientProgramRewardSetting", "ClientProgramRewardSetting");

        if(!isNullOrEmpty(clientProgramRewardSettingDTO.getAccountIdentifier()))
            return clientProgramRewardSettingDTO;

        //check tango customer exist
        Optional<ClientProgramRewardSetting> optionalClientProgramRewardSetting = clientProgramRewardSettingRepository.findClientProgramRewardSettingByClientId(clientProgramRewardSettingDTO.getClientId());
        if(!optionalClientProgramRewardSetting.isPresent()) {
            //create customer first
            CustomerRequest  customerRequest = new CustomerRequest();
            customerRequest.setCustomerIdentifier(clientProgramRewardSettingDTO.getClientId());
            customerRequest.setDisplayName(clientProgramRewardSettingDTO.getClientName());

            CompletableFuture<CustomerResponse> customerResponse = customerAdapter.CreateCustomerAysnc(customerRequest);

            CustomerResponse rp = customerResponse.get();
            clientProgramRewardSettingDTO.setCustomerIdentifier(rp.getCustomerIdentifier());
        }
        //create account
        AccountRequest accountRequest = new AccountRequest();
        accountRequest.setAccountIdentifier(String.format("%s-%s",clientProgramRewardSettingDTO.getClientName(), clientProgramRewardSettingDTO.getProgramId()));
        accountRequest.setContactEmail(clientProgramRewardSettingDTO.getClientEmail());
        accountRequest.setDisplayName(String.format("%s-%s",clientProgramRewardSettingDTO.getClientName(), clientProgramRewardSettingDTO.getProgramId()));
        CompletableFuture<AccountResponse> accountResponseCompletableFuture = customerAdapter.CreateAccountAysnc(clientProgramRewardSettingDTO.getCustomerIdentifier(), accountRequest);
        clientProgramRewardSettingDTO.setAccountIdentifier(accountResponseCompletableFuture.get().getAccountIdentifier());

        ClientProgramRewardSetting clientProgramRewardSetting = clientProgramRewardSettingRepository.save(clientProgramRewardSettingMapper.toEntity(clientProgramRewardSettingDTO));
        return clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);
    }




}
