package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.adp.domain.AmpUserActivityProgress;
import aduro.basic.programservice.adp.repository.AmpUserActivityProgressRepository;
import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.NotificationBuilder;
import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.domain.enumeration.*;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.WellmetricEventQueueRepositoryExtension;
import aduro.basic.programservice.service.UserEventService;
import aduro.basic.programservice.service.UserEventTotalPointsDto;
import aduro.basic.programservice.service.businessrule.ActivityIncentiveRule;
import aduro.basic.programservice.service.businessrule.AduroIncentiveRule;
import aduro.basic.programservice.service.businessrule.TobaccoRule;
import aduro.basic.programservice.service.businessrule.UserIncentiveBusinessRule;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.UserEventMapper;
import aduro.basic.programservice.service.mapper.UserRewardMapper;
import aduro.basic.programservice.service.util.RandomUtil;
import aduro.basic.programservice.sns.PublisherService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jamsesso.jsonlogic.JsonLogic;
import io.github.jamsesso.jsonlogic.JsonLogicException;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link UserEvent}.
 */
@Service
@Transactional
public class UserEventServiceImpl implements UserEventService {

    private final Logger log = LoggerFactory.getLogger(UserEventServiceImpl.class);

    private final UserEventRepository userEventRepository;

    @Autowired
    private ProgramUserRepository programUserRepository;


    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    private final UserEventMapper userEventMapper;

    @Autowired
    private UserIncentiveBusinessRule userIncentiveBusinessRule;


    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private WellmetricEventQueueRepositoryExtension wellmetricEventQueueRepository;

    @Autowired
    private NotificationCenterRepository notificationCenterRepository;

    @Autowired
    private UserRewardMapper userRewardMapper;

    @Autowired
    private UserRewardRepository userRewardRepository;

    @Autowired
    private AmpUserActivityProgressRepository ampUserActivityProgressRepository;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;

    @Autowired
    private TobaccoRule tobaccoRule;

    @Autowired
    private AduroIncentiveRule aduroIncentiveRule;

    @Autowired
    private ActivityIncentiveRule activityIncentiveRule;

    @Autowired
    private UserEventQueueRepository userEventQueueRepository;

    @Autowired
    private ProgramUserCohortRepository programUserCohortRepository;

    @Autowired
    private ProgramUserLogsRepository programUserLogsRepository;

    @Autowired
    private ProgramUserCollectionProgressRepository programUserCollectionProgressRepository;

    @Autowired
    private PublisherService publisherService;

    public UserEventServiceImpl(UserEventRepository userEventRepository, UserEventMapper userEventMapper) {
        this.userEventRepository = userEventRepository;
        this.userEventMapper = userEventMapper;
    }

    /**
     * Save a userEvent.
     *
     * @param userEventDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserEventDTO save(UserEventDTO userEventDTO) {
        log.debug("Request to save UserEvent : {}", userEventDTO);

        if(userEventDTO.getEventCode() == null || userEventDTO.getEventCode() == "")
            throw new BadRequestAlertException("Event Code can not null", "UserEvent", "UserEventCodeNull");
        if(userEventDTO.getEventId() == null || userEventDTO.getEventId() == "")
            throw new BadRequestAlertException("Event Id can not null", "UserEvent", "UserEventIDNull");

        UserEvent userEvent = userEventMapper.toEntity(userEventDTO);
        userEvent = userEventRepository.save(userEvent);


        return userEventMapper.toDto(userEvent);
    }

    /**
     * Get all the userEvents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserEventDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserEvents");
        return userEventRepository.findAll(pageable)
            .map(userEventMapper::toDto);
    }


    /**
     * Get one userEvent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserEventDTO> findOne(Long id) {
        log.debug("Request to get UserEvent : {}", id);
        return userEventRepository.findById(id)
            .map(userEventMapper::toDto);
    }

    /**
     * Delete the userEvent by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserEvent : {}", id);
        userEventRepository.deleteById(id);
    }

    @Override
    public  IncentiveEventPointDTO handleActivityIncentiveEvent(ActivityEventDTO activityEventDTO) {

        log.debug("incentiveEvent : {}", activityEventDTO);
        if(activityEventDTO.getEventCode() == null || activityEventDTO.getEventCode().equals(""))
            throw new BadRequestAlertException("Event Code can not null", "UserEvent", "UserEventCodeNull");

        // Starting to incentive for tobacco attestation
        if (activityEventDTO.getEventCode().toUpperCase().equals(Constants.ACTIVITY_TOBACCO_ATTESTATION)||
            activityEventDTO.getEventCode().toUpperCase().equals(Constants.WELLMETRIC_TOBACCO_RAS)) {
            log.debug("///STARTING INCENTIVE TOBACCO : {}", activityEventDTO.getEventCode());
            Option<IncentiveEventPointDTO> incentiveEventPointDTOOption = Option.ofOptional(tobaccoRule.processIncentive(activityEventDTO));
            return incentiveEventPointDTOOption.getOrElse(new IncentiveEventPointDTO());
        }

        else if (activityEventDTO.getEventCode().toUpperCase().equals(Constants.HUMAN_PERFORMANCE_ASSESSMENT) ||
            activityEventDTO.getEventCode().toUpperCase().equals(Constants.OTHER_ASSESSMENT) ||
            activityEventDTO.getEventCode().toUpperCase().equals(Constants.FLOURING_INDEX)
        ) {
            log.debug("///STARTING INCENTIVE ASSESSMENTS : {}", activityEventDTO.getEventCode());
            activityEventDTO.setEventCategory(Constants.ASSESSMENTS);
            Option<IncentiveEventPointDTO> incentiveEventPointDTOOption = Option.ofOptional(activityIncentiveRule.processIncentive(activityEventDTO));
            return incentiveEventPointDTOOption.getOrElse(new IncentiveEventPointDTO());
        }

        log.debug("///STARTING INCENTIVE ACTIVITIES : {}", activityEventDTO.getEventCode());
        if (RandomUtil.isNullOrEmpty(activityEventDTO.getEventCategory())) {
            activityEventDTO.setEventCategory(Constants.ACTIVITIES);
        }
        Option<IncentiveEventPointDTO> incentiveEventPointDTOOption = Option.ofOptional(activityIncentiveRule.processIncentive(activityEventDTO));
        return incentiveEventPointDTOOption.getOrElse(new IncentiveEventPointDTO());

    }

    //*
    // This function invoke when want to handle incentive event for One-One, HPContent, HPInteraction
    // */

    @Override
    public IncentiveEventPointDTO processingIncentiveEventByEventCategory(IncentiveEventDTO incentiveEventDTO) {
        Option<String> eventCategory = Option.of(incentiveEventDTO.getEventCategory());
        return eventCategory.map(category -> aduroIncentiveRule.processIncentiveEvent(incentiveEventDTO))
            .get().orElse(new IncentiveEventPointDTO());
    }

    @Override
    public List<UserEventDTO> getAllUserEventByParticipantIdAndClientIdAndEventId(String participantId, String clientId, String eventId, Map<String, String> queryParams) {

        if (clientId.isEmpty()) {
            throw new BadRequestAlertException("ClientId can not empty or null.", "UserEvent", "ClientId");
        }

        if (participantId.isEmpty()) {
            throw new BadRequestAlertException("ParticipantId can not empty or null.", "UserEvent", "ParticipantId");
        }

        if (eventId.isEmpty()) {
            throw new BadRequestAlertException("EventId can not empty or null.", "UserEvent", "EventId");
        }
        UserDto user = customActivityAdapter.parseUserParams(participantId, queryParams);;
        if( user == null){
            throw new BadRequestAlertException("Participant is Not Found", "ActivityEventDTO", "getAllUserEventByParticipantIdAndClientIdAndEventId");
        }

        Optional<ClientProgram> optionalClientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.toString(), user.isAsProgramTester());
        if(!optionalClientProgram.isPresent())
            throw new BadRequestAlertException("program is invalid", "UserEvent", "ProgramIdNull");

        Program program = optionalClientProgram.get().getProgram();

        Option<ProgramUser>  userOption= Option.ofOptional(
            programUserRepository.findProgramUserByParticipantIdAndProgramId(participantId, program.getId())
        );

        List<UserEventDTO> res = new ArrayList<>();
        userOption.peek(programUser -> {
            List<UserEvent> userEvents = userEventRepository.findUserEventsByProgramUserIdAndEventId(programUser.getId(), eventId).collect(Collectors.toList());
            res.addAll(userEventMapper.toDto(userEvents));
        });
        return  res;
    }

    @Override
    public UserEventTotalPointsDto getTotalBonusPointsByCustomActivityId(String participantId, String clientId, String eventId, String eventCategory, Map<String, String> queryParams) {
        if (clientId.isEmpty()) {
            throw new BadRequestAlertException("ClientId can not empty or null.", "UserEvent", "ClientId");
        }

        if (participantId.isEmpty()) {
            throw new BadRequestAlertException("ParticipantId can not empty or null.", "UserEvent", "ParticipantId");
        }

        if (eventId.isEmpty()) {
            throw new BadRequestAlertException("EventId can not empty or null.", "UserEvent", "EventId");
        }

        if (eventCategory.isEmpty()) {
            throw new BadRequestAlertException("Event Category can not empty or null.", "UserEvent", "EventCategory");
        }

        UserDto user = customActivityAdapter.parseUserParams(participantId, queryParams);;
        if(user == null){
            throw new BadRequestAlertException("User is Not Found", "customActivityAdapter", "processIncentiveEvent");
        }

        Optional<ClientProgram> optionalClientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.toString(), user.isAsProgramTester());
        if(!optionalClientProgram.isPresent())
            throw new BadRequestAlertException("program is invalid", "UserEvent", "ProgramIdNull");

        Program program = optionalClientProgram.get().getProgram();

        Option<ProgramUser>  userOption= Option.ofOptional(
            programUserRepository.findProgramUserByParticipantIdAndProgramId(participantId, program.getId())
        );

        UserEventTotalPointsDto userEventTotalPointsDto = new UserEventTotalPointsDto();
        userOption.peek(programUser -> {
            BigDecimal totalUserEventBonus = userEventRepository
                .findUserEventsByProgramUserIdAndEventIdAndEventCategory(programUser.getId(), eventId, eventCategory.toUpperCase())
                .stream().filter(u -> u.getEventPoint() != null).map(u -> u.getEventPoint())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
            userEventTotalPointsDto.setTotalUserEventPoints(totalUserEventBonus);
        });
        return userEventTotalPointsDto;
    }


    //*
    // This function invoke when want to handle incentive event for WELLMETRIC
    // */

    @Override
    public  IncentiveEventPointDTO handleIncentiveEventForWellmetric(IncentiveEventDTO incentiveEventDTO) {

        log.debug("incentive Event : {}", incentiveEventDTO);
        if(incentiveEventDTO.getEventCode() == null || incentiveEventDTO.getEventCode().equals(""))
            throw new BadRequestAlertException("Event Code can not null", "UserEvent", "UserEventCodeNull");

        if(incentiveEventDTO.getEventId() == null || incentiveEventDTO.getEventId().equals(""))
            throw new BadRequestAlertException("Event Id can not null", "UserEvent", "UserEventIDNull");
        if(incentiveEventDTO.getParticipantId() == null || incentiveEventDTO.getParticipantId().equals(""))
            throw new BadRequestAlertException("participant Id can not null", "UserEvent", "ParticipantIdNull");
        if(incentiveEventDTO.getClientId() == null || incentiveEventDTO.getClientId().equals(""))
            throw new BadRequestAlertException("client Id can not null", "UserEvent", "clientId");
        if(incentiveEventDTO.getEventDate() == null )
            throw new BadRequestAlertException("Date can not null", "UserEvent", "EventDate");

        Optional<ClientProgram> optionalClientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(incentiveEventDTO.getClientId(), ProgramStatus.Active.toString(), incentiveEventDTO.isAsProgramTester());
        if(!optionalClientProgram.isPresent())
            throw new BadRequestAlertException("program is invalid", "UserEvent", "ProgramIdNull");

        Optional<Program>  optionalProgram = programRepository.findOneWithEagerRelationships(optionalClientProgram.get().getProgramId());

        if(!optionalProgram.isPresent())
            throw new BadRequestAlertException("program Id can not null", "UserEvent", "ProgramIdNull");

        Program program = optionalProgram.get();

        if(incentiveEventDTO.getSubgroupId() != null && (incentiveEventDTO.getSubgroupId().equals("undefined") || incentiveEventDTO.getSubgroupId().equals("0")))
            incentiveEventDTO.setSubgroupId("");


        log.debug("WM incentive eventCode: {}", incentiveEventDTO.getEventCode());
        log.debug("WM incentive participantId 1: {}", incentiveEventDTO.getParticipantId());

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndClientIdAndProgramId(incentiveEventDTO.getParticipantId(), incentiveEventDTO.getClientId(), program.getId());
        ProgramUser programUser = null;
        if (programUserOptional.isPresent()) {
            programUser = programUserOptional.get();
        } else {
            log.info("programUsers not existed. Auto create one.");
            programUser = new ProgramUser();
            programUser.setClientId(incentiveEventDTO.getClientId());
            programUser.setParticipantId(incentiveEventDTO.getParticipantId());
            programUser.setEmail(incentiveEventDTO.getEmail());
            programUser.setFirstName(incentiveEventDTO.getFirstName());
            programUser.setLastName(incentiveEventDTO.getLastName());
            programUser.setProgramId(program.getId());
            programUser.setCurrentLevel(1);
            programUser.setTotalUserPoint(BigDecimal.valueOf(0));
            programUser.setSubgroupId(incentiveEventDTO.getSubgroupId());
            programUserRepository.saveAndFlush(programUser);
        }

        if (incentiveEventDTO.getEventCode().toUpperCase().equals(Constants.WELLMETRIC_SCREENING)) {
            log.debug("WM incentive participantId 2: {}", incentiveEventDTO.getParticipantId());
            /* mark complete user's custom activity wellmetrics when they receive the screening result
            make sure that 1 participant only has 1 screening result
             */
            if (updateUserActivity(incentiveEventDTO, program, programUser)) {
                return handleIncentiveScreeningResult(incentiveEventDTO, program);
            }
        }
        else  if (incentiveEventDTO.getEventCode().toUpperCase().equals(Constants.PREVENTIVE_SCREENINGS_EVENT_CODE)){
            HashMap<PreventiveScreeningType, Instant> preventiveDateMaps = getPreventiveValueMap(incentiveEventDTO.getPreventiveDto());

            if (updateUserPreventiveActivity(incentiveEventDTO, program, programUser, preventiveDateMaps)) {
                return handleIncentivePreventiveResult(incentiveEventDTO, program, preventiveDateMaps);
            }

        }else if (incentiveEventDTO.getEventCode().toUpperCase().equals(Constants.APPEAL_SCREENINGS_EVENT_CODE)){
            return handleIncentiveAppealResult(incentiveEventDTO, program);
        }

        IncentiveEventPointDTO result = new IncentiveEventPointDTO();
        result.setMessage(String.format("Not support this type = %s", incentiveEventDTO.getEventCode()));
        return result;
    }

    /**
     * Update user activity
     * @param incentiveEventDTO
     * @param program
     * @return true if has activity updated, false if not
     */
    @Transactional
    public boolean updateUserActivity(IncentiveEventDTO incentiveEventDTO, Program program, ProgramUser programUser) {
        List<AmpUserActivityProgress> ampUserActivityProgressList = null;
        try {

            AmpUserActivityProgress ampUserActivityProgress = null;

            UserDto user = customActivityAdapter.getUser(incentiveEventDTO.getParticipantId());
            log.debug("WM incentive participantId user: {}", user != null ? user.getUserId() : 0);

            // get all wm custom activity of program
            List<CustomActivityDTO> customActivitiesFull = customActivityAdapter.getCustomActivityByTypeAndProgramId(Constants.ACTIVITY_WELLMETRICS_SCREENING, program.getId());

            Date dateOfTest = incentiveEventDTO.getWellmetricBiometricDTO().getDateOfTest();
            List<CustomActivityDTO> customActivities = customActivitiesFull;
            if(dateOfTest != null) {
                // only get custom activity by date of test, if not, get all

                Instant dateOfTestIns = dateOfTest.toInstant();
                customActivities = customActivitiesFull.stream().filter(t -> t.getStartDate().isBefore(dateOfTestIns) && t.getEndDate().isAfter(dateOfTestIns)).collect(Collectors.toList());
                if(customActivities.isEmpty()){
                    // in case get all is the exception case so temporary solve for all activity
                    customActivities = customActivitiesFull;
                }
            }
            // only get progress in program
            List<Long> wmCustomActivityIds = customActivities.stream().map(c -> c.getId()).collect(Collectors.toList());
            if(!wmCustomActivityIds.isEmpty()) {
                ampUserActivityProgressList = ampUserActivityProgressRepository.findByUserIdAndTypeAndCustomActivityIdInAndCompletedAtIsNull(user.getUserId(), Constants.ACTIVITY_WELLMETRICS_SCREENING, wmCustomActivityIds);
            }
            log.debug("WM incentive ampUserActivityProgressList size: {}", !CollectionUtils.isEmpty(ampUserActivityProgressList) ? ampUserActivityProgressList.size() : 0);
            if (!CollectionUtils.isEmpty(ampUserActivityProgressList)) {

                for (AmpUserActivityProgress userActivityProgress : ampUserActivityProgressList) {
                    userActivityProgress.setCompletedAt(Instant.now());

                }
                ampUserActivityProgressRepository.saveAll(ampUserActivityProgressList);

            } else {
                ampUserActivityProgressList = new ArrayList<>();
                for (CustomActivityDTO customActivity : customActivities) {
                    ampUserActivityProgress = AmpUserActivityProgress.builder()
                        .userId(user.getUserId())
                        .customActivityId(customActivity.getId())
                        .completedAt(incentiveEventDTO.getEventDate())
                        .createdAt(Instant.now())
                        .updatedAt(Instant.now())
                        .type(Constants.ACTIVITY_WELLMETRICS_SCREENING)
                        .build();
                    ampUserActivityProgressList.add(ampUserActivityProgress);
                }

                ampUserActivityProgressRepository.saveAll(ampUserActivityProgressList);
            }
            for (AmpUserActivityProgress userActivityProgress : ampUserActivityProgressList) {
                Seq<UserEvent> eventExistedWellmetricActivity = userEventRepository.findUserEventsByProgramUserIdAndEventId(programUser.getId(), String.valueOf(userActivityProgress.getCustomActivityId()));
                if (eventExistedWellmetricActivity.isEmpty()) {
                    //TODO: only support one wm  for 1 program
                    createWellmetricActivityEvent(Constants.ACTIVITY_WELLMETRICS_SCREENING, incentiveEventDTO, program, programUser, userActivityProgress);
                }
            }
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            log.error("participant Id can not find {}", incentiveEventDTO.getParticipantId());
        }
        return false;
    }

    private void createWellmetricActivityEvent(String eventCode, IncentiveEventDTO incentiveEventDTO, Program program, ProgramUser programUser, AmpUserActivityProgress ampUserActivityProgress) {
        // fixing date: 24/2/2021 AP-8726
        //create user_event_queue for activity wellmetric
        UserEventQueue userEventQueue = new UserEventQueue();
        userEventQueue.setProgramUserId(programUser.getId());
        userEventQueue.setEventDate(Instant.now());
        userEventQueue.setEventCategory(Constants.ACTIVITIES);
        userEventQueue.setEventPoint(BigDecimal.valueOf(0.0));
        userEventQueue.setExecuteStatus(IncentiveStatusEnum.Completed.name());
        userEventQueue.setProgramId(program.getId());
        // set biometric event id to preventive event id for tracking
        userEventQueue.setPreventiveEventId(incentiveEventDTO.getEventId());
        userEventQueue.setEventCode(eventCode);
        userEventQueue.setEventId(String.valueOf(ampUserActivityProgress.getCustomActivityId()));
        userEventQueue.setErrorMessage("Wellmetric Screening flow");
        userEventQueue.setCreatedBy(incentiveEventDTO.getParticipantId());
        userEventQueue = userEventQueueRepository.saveAndFlush(userEventQueue);

        // create a 1 user event queue, 1 user event//save user event and calculate point
        UserEvent userEvent = new UserEvent();
        userEvent.setProgramUser(programUser);
        userEvent.setEventId(userEventQueue.getEventId());
        userEvent.setEventCode(userEventQueue.getEventCode());
        userEvent.setEventDate(userEventQueue.getEventDate());
        userEvent.setEventPoint(BigDecimal.valueOf(0.0));
        // set biometric event id to preventive event id for tracking
        userEvent.setBiometricEventId(incentiveEventDTO.getEventId());
        userEvent.setCreatedBy(incentiveEventDTO.getParticipantId());
        userEvent.setEventCategory(userEventQueue.getEventCategory());
        userEventRepository.saveAndFlush(userEvent);
    }

    @Transactional
    public boolean updateUserPreventiveActivity(IncentiveEventDTO incentiveEventDTO, Program program, ProgramUser programUser, HashMap<PreventiveScreeningType, Instant> preventiveDateMaps) {
        List<AmpUserActivityProgress> ampUserActivityProgressList;
        if(incentiveEventDTO.getPreventiveDto() == null ){
            log.error("Preventive Dto is empty {}", incentiveEventDTO);
            return false;
        }
        try {

            UserDto user = customActivityAdapter.getUser(incentiveEventDTO.getParticipantId());
            log.debug("WM incentive participantId user: {}", user != null ? user.getUserId() : 0);
            // use activityId to query instead of preventive type
            // but should validate activity type id preventive
            // find all custom activity which having date range in preventive date
            List<PreventiveActivityDto> preventiveActivityDtos = incentiveEventDTO.getPreventiveDto().getEligibleActivities();

            // filter
            for(PreventiveScreeningType screeningType: preventiveDateMaps.keySet()){

                PreventiveActivityDto customActivityDto = getPreventiveActivity(preventiveActivityDtos, screeningType, preventiveDateMaps.get(screeningType));
                if(customActivityDto != null){
                    ampUserActivityProgressList = ampUserActivityProgressRepository.findByUserIdAndCustomActivityId(user.getUserId(), (long) customActivityDto.getId());
                    log.debug("WM incentive ampUserActivityProgressList size: {}", !CollectionUtils.isEmpty(ampUserActivityProgressList) ? ampUserActivityProgressList.size() : 0);
                    if (!CollectionUtils.isEmpty(ampUserActivityProgressList)) {
                        ampUserActivityProgressList.forEach(a -> {
                            // check type is preventive then update completed
                            if(a.getType().equals(Constants.PREVENTIVE_CUSTOM_ACTIVITY_TYPE)) {
                                a.setCompletedAt(Instant.now());
                            }

                            Seq<UserEvent> eventExistedWellmetricActivity = userEventRepository.findUserEventsByProgramUserIdAndEventId(programUser.getId(), String.valueOf(a.getCustomActivityId()));
                            if (eventExistedWellmetricActivity.isEmpty()) {
                                createWellmetricActivityEvent(Constants.PREVENTIVE_CUSTOM_ACTIVITY_TYPE, incentiveEventDTO, program, programUser, a);
                            }

                        });
                        ampUserActivityProgressRepository.saveAll(ampUserActivityProgressList);

                    }
                }
            }

            return true;

        } catch (Exception e) {
            e.printStackTrace();
            log.error("participant Id can not find {}", incentiveEventDTO.getParticipantId());
        }
        return false;
    }



    /*
    *   Handle get incentive for outcome lite and screening by form
    * when submit form of biometric, it will send incentive with key WELLMETRIC_SCREENING
    * base on completion cap will calculate points by event code
    * if has wellmetric activity, will complete it with 0 point ( confirm with pm)
    * if program is outcome life, will incentive by ranges.
    * */
    public IncentiveEventPointDTO handleIncentiveScreeningResult(IncentiveEventDTO incentiveEventDTO, Program program) {

        Set<ProgramHealthyRange> programHealthyRanges = program.getProgramHealthyRanges().stream()
            .filter(CommonUtils.distinctByKey(ProgramHealthyRange::getBiometricCode))
            .collect(Collectors.toSet());

        String eventId = incentiveEventDTO.getEventId();

        String participantId = incentiveEventDTO.getParticipantId();

        List<WellmetricEventQueue> existedEventQueues = wellmetricEventQueueRepository.findAllByParticipantIdAndEventId(participantId, eventId);

        boolean isExistedQueue = false;
        if (!CollectionUtils.isEmpty(existedEventQueues)) {
            isExistedQueue = true;
        }

        List<WellmetricEventQueue> queues = new ArrayList<>();
        log.debug("Starting calculate queue for register and screening");


        if (!isExistedQueue) {
            //Handle create Incentive for screening and register
            Optional<ProgramCategoryPoint> optionalProgramCategoryPoint =
                program.getProgramCategoryPoints().stream()
                    .filter(programCategoryPoint -> programCategoryPoint.getCategoryCode().equals(Constants.WELLMETRICS))
                    .findFirst();

            if (!optionalProgramCategoryPoint.isPresent()) {
                throw  new BadRequestAlertException("categoryPoint is null.", "Wellmetric", "categoryPoint");
            }

            ProgramCategoryPoint categoryPoint = optionalProgramCategoryPoint.get();


            //create wellmetric queue event for register and screening
            categoryPoint.getProgramSubCategoryPoints().stream()
                .filter(programSubCategoryPoint -> (programSubCategoryPoint != null)
                    && (programSubCategoryPoint.getCode().equalsIgnoreCase(Constants.WELLMETRIC_SCREENING)
                    || programSubCategoryPoint.getCode().equalsIgnoreCase(Constants.WELLMETRIC_REGISTRATION)))
                .forEach(subPoint -> {
                    WellmetricEventQueue queue = createWellmetricQueueForScreening(incentiveEventDTO, program, subPoint.getCode());
                    queue.setResult(ResultStatus.PASS);
                    queue.setIsWaitingPush(true);
                    queues.add(queue);
                });
        }

        boolean isValid = !programHealthyRanges.isEmpty() && program.getProgramType() != null &&
            (program.getProgramType().equals(ProgramType.OUTCOMES_LITE) ||
                program.getProgramType().equals(ProgramType.FULL_OUTCOMES));

        IncentiveEventPointDTO incentiveEventPointDTO = new IncentiveEventPointDTO();

        if (!isValid) {
            wellmetricEventQueueRepository.saveAll(queues);
            incentiveEventPointDTO.setMessage("Program is not a outcome lite or full outcomes.");
            return  incentiveEventPointDTO;
        }

        log.debug("Starting to  calculate healthy range result");

        // handle create data in wellmetric queue

        WellmetricBiometricDTO wellmetricBiometricDTO = incentiveEventDTO.getWellmetricBiometricDTO();


        for (ProgramHealthyRange programHealthyRange : programHealthyRanges) {

            //check exist
            WellmetricEventQueue queue;

            if (isExistedQueue) {
                Optional<WellmetricEventQueue> eventQueue = existedEventQueues.stream()
                    .filter(q -> q.getEventCode().equalsIgnoreCase(programHealthyRange.getBiometricCode())).findFirst();
                queue = eventQueue.orElseGet(WellmetricEventQueue::new);
            } else {
                queue = new WellmetricEventQueue();
            }

            setBiometricDataForQueue(queue, incentiveEventDTO, program, programHealthyRange.getBiometricCode(), isExistedQueue);

            Gender gender = wellmetricBiometricDTO.getGender();
            if (gender == Gender.UNDEFINED) {
                queue.setGender(Gender.UNIDENTIFIED);
            } else {
                queue.setGender(gender);
            }
            Float value = getHealthyValue(programHealthyRange.getBiometricCode(), wellmetricBiometricDTO);
            queue.setHealthyValue(value);
            boolean isPassed = validateHealthyValue(gender, programHealthyRange, value);
            queue.setResult(isPassed ? ResultStatus.PASS : ResultStatus.FAIL);
            queue.setIsWaitingPush(true);
            queue.setSubCategoryCode(programHealthyRange.getSubCategoryCode());
            queues.add(queue);
        }

        // add all
        wellmetricEventQueueRepository.saveAll(queues);

        incentiveEventPointDTO.setMessage("Biometric Integration Successfully!");

        boolean isFullOutcomes = program.getProgramType() != null &&
                program.getProgramType().equals(ProgramType.FULL_OUTCOMES);

        //categorize user into cohorts
        if (!isFullOutcomes) {
            return incentiveEventPointDTO;
        }

        List<WellmetricEventQueue> bioEventQueues = queues.stream()
            .filter(q -> !q.getEventCode().equalsIgnoreCase(Constants.WELLMETRIC_SCREENING))
            .filter(q -> !q.getEventCode().equalsIgnoreCase(Constants.WELLMETRIC_REGISTRATION))
            .collect(Collectors.toList());

        if (CollectionUtils.isEmpty(bioEventQueues)) {
            return incentiveEventPointDTO;
        }

        int numberOfPass = 0;
        int numberOfFail = 0;

        // Blood Pressure
        List<String> bpBioFields = Arrays.asList(BiometricCode.BP_Diastolic__c.name(), BiometricCode.BP_Systolic__c.name());

        List<WellmetricEventQueue> bpBioQueues = bioEventQueues.stream()
            .filter(p -> bpBioFields.contains(p.getEventCode()))
            .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(bpBioQueues)) {
            int countPassBP = 0;
            int countFailBP = 0;

            long countBPPassInRange = bpBioQueues.stream()
                .filter(q -> q.getResult().equals(ResultStatus.PASS)).count();
            if (bpBioQueues.size() == 2) {
                countPassBP =(countBPPassInRange == 2) ? 1 : 0;
                countFailBP = (countBPPassInRange == 2) ? 0 : 1;
            } else {
                // if sys or diastolic
                countPassBP =(countBPPassInRange == 1) ? 1 : 0;
                countFailBP = (countBPPassInRange == 1) ? 0 : 1;
            }

            numberOfPass += countPassBP;
            numberOfFail += countFailBP;
        }

        // Glucose
        if (wellmetricBiometricDTO.isFasting()) {
            List<WellmetricEventQueue> fastingQueues =  bioEventQueues.stream()
                .filter(p -> p.getEventCode().equalsIgnoreCase(BiometricCode.RFpg__c.name()) && p.getResult().equals(ResultStatus.PASS))
                .collect(Collectors.toList());
            int countOfPass = fastingQueues.isEmpty() ? 0 : 1;
            int countOfFail = fastingQueues.isEmpty() ? 1 : 0;
            numberOfPass += countOfPass;
            numberOfFail += countOfFail;
        } else {
            List<WellmetricEventQueue> fastingQueues =  bioEventQueues.stream()
                .filter(p -> p.getEventCode().equalsIgnoreCase(BiometricCode.Non_Fasting_Y_N__c.name()) && p.getResult().equals(ResultStatus.PASS))
                .collect(Collectors.toList());
            int countOfPass = fastingQueues.isEmpty() ? 0 : 1;
            int countOfFail = fastingQueues.isEmpty() ? 1 : 0;
            numberOfPass += countOfPass;
            numberOfFail += countOfFail;
        }

        //get CB
        List<String> bodyCompositions = Arrays.asList(BiometricCode.Waist__c.name(), BiometricCode.BMI__c.name());
        List<WellmetricEventQueue> cbHealthyRanges = bioEventQueues.stream()
            .filter(p -> bodyCompositions.contains(p.getEventCode()))
            .collect(Collectors.toList());

        // if existed the both, count as 1 Pass or Fail
        if (!CollectionUtils.isEmpty(cbHealthyRanges)) {
            int countPassCB = 0;
            int countFailCB = 0;

            long countCBPass = cbHealthyRanges.stream()
                .filter(q -> q.getResult().equals(ResultStatus.PASS)).count();

            if (cbHealthyRanges.size() == 2) {
                countPassCB =(countCBPass == 2) ? 1 : 0;
                countFailCB = (countCBPass == 2) ? 0 : 1;
            } else {
                // if waist or BMI
                countPassCB =(countCBPass == 1) ? 1 : 0;
                countFailCB = (countCBPass == 1) ? 0 : 1;
            }
            numberOfPass += countPassCB;
            numberOfFail += countFailCB;
        }


        // filter items not special case (BP,  Glucose, Body Composition)
        List<String> calculatedMetrics = Arrays.asList(
            BiometricCode.BP_Diastolic__c.name(),
            BiometricCode.BP_Systolic__c.name(),
            BiometricCode.RFpg__c.name(),
            BiometricCode.Non_Fasting_Y_N__c.name(),
            BiometricCode.Waist__c.name(),
            BiometricCode.BMI__c.name());

        List<WellmetricEventQueue> streamOfQueueWithoutBPAndGlucose = bioEventQueues.stream()
            .filter(p -> !calculatedMetrics.contains(p.getEventCode()))
            .collect(Collectors.toList());

        int countOfPass = (int) streamOfQueueWithoutBPAndGlucose.stream()
            .filter(q -> q.getResult().equals(ResultStatus.PASS)).count();

        int countOfFail = (int) streamOfQueueWithoutBPAndGlucose.stream()
            .filter(q -> q.getResult().equals(ResultStatus.FAIL)).count();

        numberOfPass += countOfPass;
        numberOfFail += countOfFail;

        // define json logic
        JsonLogic jsonLogic = new JsonLogic();
        Map<String, Integer> data = new HashMap<>();
        data.put(BioStatement.PASS.name(), numberOfPass); //PASS > 1
        data.put(BioStatement.FAIL.name(), numberOfFail); // FAIL > 1

        ProgramUserLogs logs = new ProgramUserLogs()
            .code(Constants.CATEGORIZE_BIO_USER)
            .createdDate(Instant.now())
            .modifiedDate(Instant.now())
            .message("")
            .participantId(incentiveEventDTO.getParticipantId())
            .programId(program.getId());

        List<String> trackingMessages =  new ArrayList<>();

        // get all cohorts in program

        Set<ProgramCohort> programCohorts = program.getProgramCohorts();

        if (CollectionUtils.isEmpty(programCohorts)) {
            trackingMessages.add(ProcessStep.PROGRAM_COHORT_EMPTY.toString());
            programUserLogsRepository.save(logs);
            return incentiveEventPointDTO;
        }

        Optional<ProgramCohort> programCohortDefaultOptional = programCohorts.stream()
            .filter(c -> c.isIsDefault() != null && c.isIsDefault()).findFirst();

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndClientIdAndProgramId(incentiveEventDTO.getParticipantId(), incentiveEventDTO.getClientId(), program.getId());


        if (!programUserOptional.isPresent()) {
            //write log
            trackingMessages.add("User program is not existed.");
            programUserLogsRepository.save(logs);
            return incentiveEventPointDTO;
        }

        ProgramUser programUser = programUserOptional.get();

        if (!programCohortDefaultOptional.isPresent()) {
            // write log
            trackingMessages.add(ProcessStep.PROGRAM_USER_EMPTY.toString());
            programUserLogsRepository.save(logs);
            return incentiveEventPointDTO;
        }

        ProgramCohort defaultCohort = programCohortDefaultOptional.get();

        List<ProgramUserCohort> programCohortInRules = new ArrayList<>();

        // get all not default cohorts 1 default cohort va 2 cohort
        List<ProgramCohort> programCohortList = programCohorts.stream()
            .filter(p -> Objects.isNull(p.isIsDefault()) || !p.isIsDefault())
            .collect(Collectors.toList());

        trackingMessages.add(ProcessStep.PROGRAM_COHORTS_HAS_NOT_DEFAULT.toString());

        // user have belonged to cohorts
        List<ProgramUserCohort> programUserCohorts= programUserCohortRepository.getProgramUserInProgramCohort(programUser.getId(), Arrays.asList(CohortStatus.PENDING, CohortStatus.DISABLED));

        List<String> existedCohortIds = programUserCohorts.stream()
            .map(p ->p.getId().toString())
            .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(programUserCohorts)) {
            trackingMessages.add(ProcessStep.PROGRAM_USERS_HAS_BELONGED_COHORT_ID.toString() + String.join("_", existedCohortIds));
        }

        Optional<ProgramUserCohort> programUserDefaultCohortOptional = programUserCohorts.stream()
            .filter(p -> Objects.nonNull(p.getProgramCohort().isIsDefault()) && p.getProgramCohort().isIsDefault())
            .findFirst();

        ProgramUserCohort programUserDefaultCohort = programUserDefaultCohortOptional.orElse(null);
        List<Long> eventDefaults = new ArrayList<>();
        if (programUserDefaultCohort != null) {
            eventDefaults.add(programUserDefaultCohort.getId());
        }

        // get non default
        Optional<ProgramUserCohort> programUserCohortOptional = programUserCohorts.stream()
            .filter(p -> !eventDefaults.contains(p.getId()))
            .findFirst();

        // foreach cohorts with default = false
        for (ProgramCohort programCohort : programCohortList) {

            // create user in cohort
            // 1 cohort -> collection a - required completion 2, collection b - required 2 completion,
            Set<ProgramCohortCollection> programCohortCollections = programCohort.getProgramCohortCollections();

            int totalOfRequired = 0;

            Optional<Integer> sumRequiredNumber = programCohortCollections.stream().map(c -> {
                if (c.getRequiredCompletion() != null){
                    return c.getRequiredCompletion();
                }
                return 0;
            }).reduce(Integer::sum);


            if (sumRequiredNumber.isPresent()) {
                totalOfRequired = sumRequiredNumber.get();
            }

            trackingMessages.add(ProcessStep.COHORT_ID_HAS_REQUIRED_ITEMS.toString()+ programCohort.getId()+ "_" + totalOfRequired);

            ProgramUserCohort programUserCohort = null;

            // 5 metrics, Pass 1/5
            Set<ProgramCohortRule> programCohortRules = programCohort.getProgramCohortRules();

            if (programCohortRules.isEmpty()) {
                trackingMessages.add(ProcessStep.PROGRAM_COHORT_RULE_EMPTY.toString() + programCohort.getId());
                continue;
            }

            //currently 1 rule for biometric
            for (ProgramCohortRule programCohortRule : programCohortRules) {
                try {
                    Boolean result =  (Boolean) jsonLogic.apply(programCohortRule.getRules(), data);
                    //PASS >1, total pass = 2
                    if (result) {

                        trackingMessages.add(ProcessStep.NEW_COHORT_CALCULATED.toString()+ programCohort.getId());

                        if (programUserCohortOptional.isPresent()) {
                            ProgramUserCohort existedUserCohort = programUserCohortOptional.get();
                            trackingMessages.add(ProcessStep.EXIST_COHORT_ID.toString()+  existedUserCohort.getId());
                            // if  new cohorts is present
                            if (existedUserCohort.getProgramCohort().getId().equals(programCohort.getId())) {
                                existedUserCohort.setModifiedDate(Instant.now());
                                trackingMessages.add(ProcessStep.NEW_COHORT_EQUAL_EXISTED_COHORT_ID.toString() + existedUserCohort.getId() + " Fail = " + numberOfFail + " Pass = " + numberOfPass);
                                programUserCohort = existedUserCohort;
                            }  else {
                                // only 1 cohort not default
                                trackingMessages.add(ProcessStep.USER_PLACED_NEW_COHORT_ALTHOUGH_EXIST_COHORT.toString()+ programCohort.getId());

                                //create 1  cohort pending
                                programUserCohort  = createNewUserCohort(programUser, programCohort, numberOfFail, numberOfPass, totalOfRequired);
                                programUserCohort.setStatus(CohortStatus.PENDING);

                            }
                        } else {

                            //tracking if have default, disable, with another cohort
                            if (programUserDefaultCohortOptional.isPresent()) {
                                // disable default cohorts to keep tracking histories
                                ProgramUserCohort userDefaultCohort = programUserDefaultCohortOptional.get();
                                userDefaultCohort.setStatus(CohortStatus.DISABLED);
                                programUserCohortRepository.save(userDefaultCohort);
                                trackingMessages.add(ProcessStep.PROGRAM_USER_DEFAULT_COHORT_DISABLE.toString()+ programUserDefaultCohort.getId());
                            }

                            if(!CollectionUtils.isEmpty(programCohortInRules))  {
                                programCohortInRules.forEach(userCohort -> {
                                    userCohort.setStatus(CohortStatus.PENDING);
                                });

                                trackingMessages.add(ProcessStep.USER_PLACED_COHORT_BEFORE_ONE_UPLOAD.toString());
                            }

                            // create new cohort
                            programUserCohort  = createNewUserCohort(programUser, programCohort, numberOfFail, numberOfPass, totalOfRequired);

                        }
                        programCohortInRules.add(programUserCohort);
                    }

                } catch (JsonLogicException e) {
                    e.printStackTrace();
                    trackingMessages.add(ProcessStep.JSON_LOGIC_APPLY_DATA_ERROR.toString());
                }
            }
        }

        // should place in default cohort if not belonged to cohorts
        // user not belonged any cohorts
        if (CollectionUtils.isEmpty(programCohortInRules) && !programUserCohortOptional.isPresent()) {

            trackingMessages.add(ProcessStep.USER_NOT_PLACE_ANY_COHORT.toString());

            ProgramUserCohort defaultUserCohort;

            if (programUserDefaultCohortOptional.isPresent()) {
                defaultUserCohort = programUserDefaultCohortOptional.get();
                defaultUserCohort.setModifiedDate(Instant.now());
                defaultUserCohort.setNumberOfFailure(numberOfFail);
                defaultUserCohort.setNumberOfPass(numberOfPass);
                programCohortInRules.add(defaultUserCohort);
                trackingMessages.add(ProcessStep.USER_UPDATE_DEFAULT_COHORT.toString());
            } else {
                // create new default cohort
                Set<ProgramCohortCollection> programCohortCollections = defaultCohort.getProgramCohortCollections();

                Integer sumOfRequired = programCohortCollections.stream()
                    .filter(p -> Objects.nonNull(p.getRequiredCompletion()))
                    .map(ProgramCohortCollection::getRequiredCompletion)
                    .reduce(Integer::sum).orElse(0);

                defaultUserCohort = createNewUserCohort(programUser, defaultCohort, numberOfFail, numberOfPass, sumOfRequired);

                trackingMessages.add(ProcessStep.USER_PLACED_DEFAULT_COHORT.toString());

            }

            programCohortInRules.add(defaultUserCohort);

        }

        // pending or in-progress or completed
        programCohortInRules = programUserCohortRepository.saveAll(programCohortInRules);

        if (!CollectionUtils.isEmpty(programCohortInRules)) {
            publisherService.publishWithEvent(ProgramSNSEvent.PROGRAM_USER_COHORT_UPDATED,
                UserCohortUpdatedEventDto.builder()
                    .programId(program.getId())
                    .aduroId(programUser.getParticipantId())
                    .timestamp(Instant.now())
                    .build()
            );
        }

        //set up progress collection for participant
        List<ProgramUserCollectionProgress> collectionProgressList = new ArrayList<>();

        for (ProgramUserCohort programUserCohort : programCohortInRules) {

            ProgramCohort programCohort = programUserCohort.getProgramCohort();

            Set<ProgramCohortCollection> programCohortCollections = programCohort.getProgramCohortCollections();

            CollectionStatus collectionStatus;

            for (ProgramCohortCollection programCohortCollection : programCohortCollections) {

                if (programUserCohort.getStatus().equals(CohortStatus.PENDING)) {
                    collectionStatus = CollectionStatus.NOT_DOING;
                } else {
                    collectionStatus = (Objects.isNull(programCohortCollection.getRequiredCompletion()) || programCohortCollection.getRequiredCompletion() == 0) ? CollectionStatus.DONE : CollectionStatus.DOING;
                }

                ProgramCollection programCollection = programCohortCollection.getProgramCollection();

                Optional<ProgramUserCollectionProgress> progressUserCollectionOptional = programUserCollectionProgressRepository.findByProgramUserCohortIdAndCollectionId(programUserCohort.getId(), programCollection.getId());

                if (progressUserCollectionOptional.isPresent()) {

                    ProgramUserCollectionProgress userCollectionProgress = progressUserCollectionOptional.get();
                    userCollectionProgress.setModifiedDate(Instant.now());
                    collectionProgressList.add(userCollectionProgress);

                } else {
                    ProgramUserCollectionProgress userCollectionProgress = new ProgramUserCollectionProgress()
                        .collectionId(programCollection.getId())
                        .programUserCohort(programUserCohort)
                        .createdDate(Instant.now())
                        .modifiedDate(Instant.now())
                        .totalRequiredItems(programCohortCollection.getRequiredCompletion() == null ? 0 : programCohortCollection.getRequiredCompletion())
                        .status(collectionStatus)
                        .currentProgress(0L);
                    collectionProgressList.add(userCollectionProgress);
                }
            }
        }

        if (!CollectionUtils.isEmpty(collectionProgressList)) {
            trackingMessages.add(ProcessStep.USER_COLLECTION_HAS_PROGRESS.toString());
            programUserCollectionProgressRepository.saveAll(collectionProgressList);
        }

        //add trigger notification
        trackingMessages.add(ProcessStep.CREATE_NOTIFICATION.toString());

        NotificationCenter notificationCenter = NotificationBuilder.notificationWellmetricBuilder(incentiveEventDTO.getParticipantId(), incentiveEventDTO.getEventId());
        notificationCenterRepository.save(notificationCenter);

        logs.setMessage(String.join("->", trackingMessages));
        programUserLogsRepository.save(logs);

        return incentiveEventPointDTO;
    }

    private ProgramUserCohort createNewUserCohort(ProgramUser programUser, ProgramCohort programCohort, Integer numberOfFail, Integer numberOfPass,Integer totalOfRequired) {
        return new ProgramUserCohort()
            .programUser(programUser)
            .programCohort(programCohort)
            .createdDate(Instant.now())
            .modifiedDate(Instant.now())
            .numberOfFailure(numberOfFail)
            .numberOfPass(numberOfPass)
            .progress((long) 0)
            .status(totalOfRequired == 0 ? CohortStatus.COMPLETED: CohortStatus.INPROGRESS);
    }

    /*
     *   Handle get incentive for preventive result
     * */
    public IncentiveEventPointDTO handleIncentivePreventiveResult(IncentiveEventDTO incentiveEventDTO, Program program, HashMap<PreventiveScreeningType, Instant> preventiveDateMaps) {

        PreventiveDto preventiveDto = incentiveEventDTO.getPreventiveDto();
        List<PreventiveActivityDto> preventiveActivityDtos = preventiveDto.getEligibleActivities();

        // Handle create Incentive for screening and register
        Optional<ProgramCategoryPoint> optionalProgramCategoryPoint =
            program.getProgramCategoryPoints().stream()
                .filter(programCategoryPoint -> programCategoryPoint.getCategoryCode().equals(Constants.PREVENTIVE_SCREENINGS))
                .findFirst();

        if (!optionalProgramCategoryPoint.isPresent()) {
            throw  new BadRequestAlertException("categoryPoint is null.", "Preventive", "categoryPoint");
        }

        // get and check program user and program category point
        ProgramCategoryPoint categoryPoint = optionalProgramCategoryPoint.get();

        Optional<ProgramUser> optionalProgramUser = programUserRepository.findProgramUserByParticipantIdAndProgramId(incentiveEventDTO.getParticipantId(), program.getId());
        if(!optionalProgramUser.isPresent()){
            throw  new BadRequestAlertException("Program user is null.", "Preventive", "ProgramUser");
        }

        ProgramUser programUser = optionalProgramUser.get();
        IncentiveEventPointDTO incentiveEventPointDTO = new IncentiveEventPointDTO();
        incentiveEventPointDTO.setMessage("Biometric Integration Successfully!");

        for(PreventiveScreeningType screeningType: preventiveDateMaps.keySet()) {
            if(screeningType.equals(PreventiveScreeningType.FluVaccine) ||
                screeningType.equals(PreventiveScreeningType.CovidVaccine1of2) ||
                screeningType.equals(PreventiveScreeningType.CovidVaccine2of2) ||
                screeningType.equals(PreventiveScreeningType.CovidVaccineSingleDose) ||
                screeningType.equals(PreventiveScreeningType.CovidVaccineBooster)){
                continue;
            }
            PreventiveActivityDto customActivityDto = getPreventiveActivity(preventiveActivityDtos, screeningType, preventiveDateMaps.get(screeningType));
            Instant datePoint = preventiveDateMaps.get(screeningType);

            BigDecimal point = BigDecimal.valueOf(0);

            log.debug("Starting calculate queue for register and screening");

            // create a 1 user event queue, 1 user event

            List<UserEventQueue> existedEvents = new ArrayList<>();

            if(customActivityDto != null) {
                existedEvents = userEventQueueRepository.findByProgramUserIdAndEventIdAndProgramIdAndEventCode(programUser.getId(),
                    String.valueOf(customActivityDto.getId()), program.getId(), Constants.PREVENTIVE_SCREENINGS);
            }
            UserEventQueue userEventQueue = new UserEventQueue();
            if (existedEvents.isEmpty()) {
                userEventQueue.setProgramUserId(programUser.getId());
                userEventQueue.setEventDate(Instant.now());
                userEventQueue.setEventCategory(Constants.PREVENTIVE_SCREENINGS);
                userEventQueue.setEventPoint(BigDecimal.valueOf(0.0));
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.New.name());
                userEventQueue.setProgramId(program.getId());
                userEventQueue.setEventCode(incentiveEventDTO.getEventCode());
                if(customActivityDto != null) {
                    userEventQueue.setEventId(String.valueOf(customActivityDto.getId()));
                }
                userEventQueue.setCreatedBy(incentiveEventDTO.getParticipantId());
                userEventQueue.setPreventiveEventId(incentiveEventDTO.getEventId());
                userEventQueue = userEventQueueRepository.saveAndFlush(userEventQueue);
            } else {
                userEventQueue = existedEvents.get(existedEvents.size() - 1);
            }


            //calculate point and level
            BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program, applicationProperties);


            if(programUser.getSubgroupId() != null && (programUser.getSubgroupId().equals("undefined") || programUser.getSubgroupId().equals("0"))) {
                programUser.setSubgroupId("");
            }

            // check condition to have point and add user event for calculate incentive
            // if program is QC status, should check program QC Date
            boolean isInvalidWithQCProgram = program.getQaVerify() && datePoint != null
                && ((program.getProgramQAStartDate() == null || datePoint.isBefore(program.getProgramQAStartDate()))
                || (program.getProgramQAEndDate() == null || datePoint.isAfter(program.getProgramQAEndDate())));

            if (customActivityDto == null || isInvalidWithQCProgram || !(datePoint != null && datePoint.compareTo(program.getStartDate()) >= 0
                && datePoint.compareTo(program.getResetDate()) <= 0)){
                // not able to calculate point, but should track to a new user event record
                point.add(categoryPoint.getValuePoint());
                userEventQueueRepository.saveAndFlush(userEventQueue);

                if(customActivityDto!= null) {
                    UserEvent userEvent = new UserEvent();
                    userEvent.setEventCategory(Constants.PREVENTIVE_SCREENINGS);
                    userEvent.setEventCode(incentiveEventDTO.getEventCode());
                    userEvent.setEventId(String.valueOf(customActivityDto.getId()));
                    userEvent.setBiometricEventId(incentiveEventDTO.getEventId());
                    userEvent.setEventDate(incentiveEventDTO.getEventDate());
                    userEvent.setEventPoint(BigDecimal.ZERO);
                    userEvent.setProgramUser(programUser);
                    userEvent.setCreatedBy(incentiveEventDTO.getParticipantId());
                    userEventRepository.save(userEvent);
                }
                userEventQueue.setExecuteStatus(IncentiveStatusEnum.Completed.name());
                userEventQueue.setErrorMessage("Preventive date is not in program date range");

                userEventQueueRepository.save(userEventQueue);
                continue;
            }

            // calculate point
            incentiveEventPointDTO = userIncentiveBusinessRule.calculatePointAndLevelByCategoryPointAndSubCategoryPoint(userEventQueue, programUser,
                Constants.PREVENTIVE_SCREENINGS, totalUserPoint, programUser.getSubgroupId());
            incentiveEventPointDTO.setMessage("Biometric Integration Successfully!");
        }

        return incentiveEventPointDTO;
    }

    /*
     *   Handle get incentive for preventive result
     * */
    private IncentiveEventPointDTO handleIncentiveAppealResult(IncentiveEventDTO incentiveEventDTO, Program program) {

        AppealDto appealDto = incentiveEventDTO.getAppealDto();

        log.debug("Starting calculate Appeal queue for register and screening");

        // get and check program user and program category point
        Optional<ProgramUser> optionalProgramUser = programUserRepository.findProgramUserByParticipantIdAndProgramId(incentiveEventDTO.getParticipantId(), program.getId());
        if(!optionalProgramUser.isPresent()){
            throw  new BadRequestAlertException("Program user is null.", "Appeal", "ProgramUser");
        }

        // list wellmetric event queue
        List<WellmetricEventQueue> wellmetricEventQueues = wellmetricEventQueueRepository.findAllByParticipantIdAndEventId(incentiveEventDTO.getParticipantId(), appealDto.getWmEventId());

        // check each wmevent queue has appeal status = true, -> assign point if not
        for(WellmetricEventQueue wellmetricEventQueue: wellmetricEventQueues){
            BiometricCode code = null;
            try {
                code = BiometricCode.valueOf(wellmetricEventQueue.getEventCode());
            }catch(Exception ex){
                continue;
            }
            boolean isAppeal = false;
            switch (code){
                case BP_Diastolic__c:
                case BP_Systolic__c:
                    isAppeal = appealDto.isBloodPressure();
                    break;
                case Waist__c:
                    isAppeal = appealDto.isWaist();
                    break;
                case RFpg__c:
                    isAppeal = appealDto.isFastingGlucose();
                    break;
                case Non_Fasting_Y_N__c:
                    break;
                case BMI__c:
                    isAppeal = appealDto.isBmi();
                    break;
                case TC_HDL_Ratio__c:
                    isAppeal = appealDto.isTc() && appealDto.isHdl();
                    break;
                case RCho__c:
                    isAppeal = appealDto.isTc();
                    break;
                case RHdl__c:
                    isAppeal = appealDto.isHdl();
                    break;
                case RTrig__c:
                    isAppeal = appealDto.isTg();
                    break;
                case waist_to_height_ratio:
                    isAppeal = appealDto.isWthr();
                    break;
                case RLdl__c:
                    isAppeal = appealDto.isLdl();
                    break;
                case HR_Pulse__c:
                    isAppeal = appealDto.isHeartRate();
                    break;
                case a1c:
                    isAppeal = appealDto.isA1C();
                    break;
                case tobacco:
                    isAppeal = appealDto.isTobacco();
                    break;
            }
            if(isAppeal){
                wellmetricEventQueue.setResult(ResultStatus.PASS);
                wellmetricEventQueue.setExecuteStatus(IncentiveStatusEnum.New.toString());
                wellmetricEventQueue.setAppealApplied(true);
                // mark appeal processed, use for Data Ops trace
                wellmetricEventQueue.setAppealEventId(incentiveEventDTO.getEventId());
            }
        }

        wellmetricEventQueueRepository.saveAll(wellmetricEventQueues);

        // calculate point
        IncentiveEventPointDTO incentiveEventPointDTO = new IncentiveEventPointDTO();
        incentiveEventPointDTO.setMessage("Appeal queue generated successfully");
        return incentiveEventPointDTO;
    }

    private PreventiveActivityDto getPreventiveActivity(List<PreventiveActivityDto> preventiveActivityDtos, PreventiveScreeningType screeningType, Instant preventiveDate) {
        List<PreventiveActivityDto> activitiesDto = preventiveActivityDtos.stream().filter(p -> p.getScreeningType().equalsIgnoreCase(screeningType.toString())).collect(Collectors.toList());
        for(PreventiveActivityDto activityDto: activitiesDto) {
            Instant startTime = activityDto.getStartDate() != null ? activityDto.getStartDate().toInstant() : null;
            Instant endTime = activityDto.getEndDate() != null ? activityDto.getEndDate().toInstant() : null;
            if (startTime == null || endTime == null) {
                continue;
            }
            if (startTime.isBefore(preventiveDate) && endTime.isAfter(preventiveDate)) {
                return activityDto;
            }
        }
        return null;
    }
    private HashMap<PreventiveScreeningType, Instant> getPreventiveValueMap(PreventiveDto preventiveDto) {

        HashMap<PreventiveScreeningType, Instant> preventiveDateMap = new HashMap<>();
        for(PreventiveActivityDto activityDto: preventiveDto.getEligibleActivities()) {
            PreventiveScreeningType screeningType = PreventiveScreeningType.fromString(activityDto.getScreeningType());
            Instant datePoint = null;
            if (screeningType != null) {
                switch (screeningType) {
                    case AnnualPCPExam:
                        datePoint = preventiveDto.getDateAnnualPCP() != null ? preventiveDto.getDateAnnualPCP().toInstant() : null;
                        break;
                    case FollowupPCPExam:
                        datePoint = preventiveDto.getDateFollowupPCP() != null ? preventiveDto.getDateFollowupPCP().toInstant() : null;
                        break;
                    case Mamography:
                        datePoint = preventiveDto.getDateMamogram() != null ? preventiveDto.getDateMamogram().toInstant() : null;
                        break;
                    case Colonoscopy:
                        datePoint = preventiveDto.getDateColonoscopy() != null ? preventiveDto.getDateColonoscopy().toInstant() : null;
                        break;
                    case ColorectalCancer:
                        datePoint = preventiveDto.getDateColorectalCancer() != null ? preventiveDto.getDateColorectalCancer().toInstant() : null;
                        break;
                    case ProstateCancer:
                        datePoint = preventiveDto.getDateProstateCancer() != null ? preventiveDto.getDateProstateCancer().toInstant() : null;
                        break;
                    case CervicalCancer:
                        datePoint = preventiveDto.getDateCervicalCancer() != null ? preventiveDto.getDateCervicalCancer().toInstant() : null;
                        break;
                    case VisionScreening:
                        datePoint = preventiveDto.getDateOfVision() != null ? preventiveDto.getDateOfVision().toInstant() : null;
                        break;
                    case DentalVisit:
                        datePoint = preventiveDto.getDentalVisitDate() != null ? preventiveDto.getDentalVisitDate().toInstant() : null;
                        break;
                    case PrenatalVisit:
                        datePoint = preventiveDto.getDatePreNatal() != null ? preventiveDto.getDatePreNatal().toInstant() : null;
                        break;
                    case HemoglobinA1CTest:
                        datePoint = preventiveDto.getDateHemoglobinA1c() != null ? preventiveDto.getDateHemoglobinA1c().toInstant() : null;
                        break;
                    case CreatinineUrineProteinTest:
                        datePoint = preventiveDto.getDateUrineProtein() != null ? preventiveDto.getDateUrineProtein().toInstant() : null;
                        break;
                    case DilatedEyeExam:
                        datePoint = preventiveDto.getDateDialedEyeExam() != null ? preventiveDto.getDateDialedEyeExam().toInstant() : null;
                        break;
                    case FluVaccine:
                        datePoint = preventiveDto.getFluVaccineDate() != null ? preventiveDto.getFluVaccineDate().toInstant() : null;
                        break;
                    case CovidVaccine1of2:
                        datePoint = preventiveDto.getCovidVaccineFirstDate() != null ? preventiveDto.getCovidVaccineFirstDate().toInstant() : null;
                        break;
                    case CovidVaccine2of2:
                        datePoint = preventiveDto.getCovidVaccineSecondDate() != null ? preventiveDto.getCovidVaccineSecondDate().toInstant() : null;
                        break;
                    case CovidVaccineSingleDose:
                        datePoint = preventiveDto.getCovidVaccineSingleDoseDate() != null ? preventiveDto.getCovidVaccineSingleDoseDate().toInstant() : null;
                        break;
                    case CovidVaccineBooster:
                        datePoint = preventiveDto.getCovidVaccineBoosterDate() != null ? preventiveDto.getCovidVaccineBoosterDate().toInstant() : null;
                        break;
                    default:
                        return null;
                }
                if (datePoint != null) {
                    preventiveDateMap.put(screeningType, datePoint);
                }
            }
        }
        return preventiveDateMap;
    }

    private WellmetricEventQueue createWellmetricQueueForScreening(IncentiveEventDTO incentiveEventDTO,
                                                                   Program program,
                                                                   String eventCode) {
        WellmetricEventQueue queue = new WellmetricEventQueue();
        setBiometricDataForQueue(queue, incentiveEventDTO, program, eventCode, false);
        return queue;
    }

    private void setBiometricDataForQueue(WellmetricEventQueue queue,
                                          IncentiveEventDTO incentiveEventDTO,
                                          Program program,
                                          String eventCode,
                                          boolean isUpdate
    ) {
        queue.setSubgroupId(incentiveEventDTO.getSubgroupId());
        queue.setExecuteStatus(IncentiveStatusEnum.New.toString());
        queue.setCreatedAt(isUpdate ? queue.getCreatedAt() : Instant.now());
        queue.setParticipantId(incentiveEventDTO.getParticipantId());
        queue.setEmail(incentiveEventDTO.getEmail());
        queue.setClientId(incentiveEventDTO.getClientId());
        queue.setEventCode(eventCode);
        queue.setAttemptCount(isUpdate ? queue.getAttemptCount() : 0);
        queue.setResult(ResultStatus.UNAVAILABLE);
        queue.setLastAttemptTime(Instant.now());
        queue.setEventId(incentiveEventDTO.getEventId());
        queue.setProgramId(program.getId());
        queue.setAttemptedAt(incentiveEventDTO.getEventDate());
        queue.setGender(Gender.UNIDENTIFIED);
        queue.setHealthyValue(Float.valueOf(0));
        queue.setModifiedDate(Instant.now());
        LocalDateTime localDateTime = LocalDateTime.ofInstant(incentiveEventDTO.getEventDate(), ZoneOffset.UTC);
        String formatted = DateTimeFormatter.ofPattern(Constants.YYYY_MM_DD).format(localDateTime);
        queue.setEventDate(isUpdate ? queue.getEventDate() : formatted);
    }

    public boolean validateHealthyValue(Gender gender, ProgramHealthyRange programHealthyRange, float value) {
        switch (gender) {
            case MALE:
                return (value >= programHealthyRange.getMaleMin() && value <= programHealthyRange.getMaleMax());
            case FEMALE:
                return (value >= programHealthyRange.getFemaleMin() && value <= programHealthyRange.getFemaleMax());
            case UNIDENTIFIED: case UNDEFINED:
                return (value >= programHealthyRange.getUnidentifiedMin() && value <= programHealthyRange.getUnidentifiedMax());
        }
        return false;
    }

    private float getFloatValue(Float input, float defaultVal){
        return input != null ? input : defaultVal;
    }
    public Float getHealthyValue(String code, WellmetricBiometricDTO dto) {
        BiometricCode bioCode = null;
        try {
            bioCode = BiometricCode.valueOf(code);
        }catch (IllegalArgumentException ex){
            log.error("BioCode is not valid", code);
            return 0f;
        }
        switch (bioCode){
            case BP_Systolic__c:
                return getFloatValue(dto.getSystolic(), 0);
            case BP_Diastolic__c:
                return getFloatValue(dto.getDiastolic(), 0);
            case TC_HDL_Ratio__c:
                return getFloatValue(dto.getTcHdl(), 0);
            case Waist__c:
                return getFloatValue(dto.getWaist(), 0);
            case RFpg__c:
                return getFloatValue(dto.isFasting() ? dto.getFastingYN() : 0, 0);
            case Non_Fasting_Y_N__c:
                return getFloatValue(dto.isFasting() ? 0 : dto.getFastingYN(), 0);
            case BMI__c:
                return getFloatValue(dto.getBmi(), 0);
            case RCho__c:
                return getFloatValue(dto.getTc(), 0);
            case RHdl__c:
                return getFloatValue(dto.getHdl(), 0);
            case RTrig__c:
                return getFloatValue(dto.getTriglycerides(), 0);

            case waist_to_height_ratio:
                return getFloatValue(dto.getWthr(), 0);
            case RLdl__c:
                return getFloatValue(dto.getLdl(), 0);
            case HR_Pulse__c:
                return getFloatValue(dto.getHeartRate(), 0);
            case a1c:
                return getFloatValue(dto.getA1C(), 0);
            case tobacco:
                return getFloatValue(dto.getTobaccoUse(), 0);
            default:
                break;
        }

        return 0f;
    }


    @Transactional(readOnly = true)
    public IncentiveResponseDTO findIncentiveResultByParticipantIdAndClientIdAndEventId(String participantId, String clientId, String eventId, boolean isProgramTester) {

        Optional<ClientProgram> optionalClientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.toString(), isProgramTester);
        if(!optionalClientProgram.isPresent())
            throw new BadRequestAlertException("ClientProgram is invalid", "UserEvent", "ClientId");

        Optional<Program>  optionalProgram = programRepository.findOneWithEagerRelationships(optionalClientProgram.get().getProgramId());

        if (!optionalProgram.isPresent()) {
            throw new BadRequestAlertException("Program is invalid", "UserEvent", "ProgramIdNull");
        }

        Optional<ProgramUser> optionalProgramUser =  programUserRepository.findProgramUserByParticipantIdAndProgramId(participantId, optionalProgram.get().getId());

        if (!optionalProgramUser.isPresent()) {
            throw new BadRequestAlertException("ProgramUser is invalid", "UserEvent", "ProgramUser");
        }
        Program program = optionalClientProgram.get().getProgram();
        ProgramUser programUser = optionalProgramUser.get();
        Integer levelUp =  programUser.getCurrentLevel();
        Integer totalLevel = program.getProgramLevels().size();

        List<UserReward> userRewards = userRewardRepository.findUserRewardByParticipantIdAndClientIdAndEventId(participantId, clientId, eventId);

        return new IncentiveResponseDTO(levelUp, totalLevel, userRewards != null ? userRewardMapper.toDto(userRewards) : new ArrayList<>());
    }

}
