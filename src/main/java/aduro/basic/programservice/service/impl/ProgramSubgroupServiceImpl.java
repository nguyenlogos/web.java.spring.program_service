package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramSubgroupService;
import aduro.basic.programservice.domain.ProgramSubgroup;
import aduro.basic.programservice.repository.ProgramSubgroupRepository;
import aduro.basic.programservice.service.dto.ProgramSubgroupDTO;
import aduro.basic.programservice.service.mapper.ProgramSubgroupMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramSubgroup}.
 */
@Service
@Transactional
public class ProgramSubgroupServiceImpl implements ProgramSubgroupService {

    private final Logger log = LoggerFactory.getLogger(ProgramSubgroupServiceImpl.class);

    private final ProgramSubgroupRepository programSubgroupRepository;

    private final ProgramSubgroupMapper programSubgroupMapper;

    public ProgramSubgroupServiceImpl(ProgramSubgroupRepository programSubgroupRepository, ProgramSubgroupMapper programSubgroupMapper) {
        this.programSubgroupRepository = programSubgroupRepository;
        this.programSubgroupMapper = programSubgroupMapper;
    }

    /**
     * Save a programSubgroup.
     *
     * @param programSubgroupDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramSubgroupDTO save(ProgramSubgroupDTO programSubgroupDTO) {
        log.debug("Request to save ProgramSubgroup : {}", programSubgroupDTO);
        ProgramSubgroup programSubgroup = programSubgroupMapper.toEntity(programSubgroupDTO);
        programSubgroup = programSubgroupRepository.save(programSubgroup);
        return programSubgroupMapper.toDto(programSubgroup);
    }

    /**
     * Get all the programSubgroups.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramSubgroupDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramSubgroups");
        return programSubgroupRepository.findAll(pageable)
            .map(programSubgroupMapper::toDto);
    }


    /**
     * Get one programSubgroup by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramSubgroupDTO> findOne(Long id) {
        log.debug("Request to get ProgramSubgroup : {}", id);
        return programSubgroupRepository.findById(id)
            .map(programSubgroupMapper::toDto);
    }

    /**
     * Delete the programSubgroup by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramSubgroup : {}", id);
        programSubgroupRepository.deleteById(id);
    }
}
