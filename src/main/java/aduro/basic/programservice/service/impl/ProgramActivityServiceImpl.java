package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.NotificationAdapter;
import aduro.basic.programservice.ap.dto.*;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramActivity;
import aduro.basic.programservice.domain.ProgramCollectionContent;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.service.ProgramActivityService;
import aduro.basic.programservice.service.dto.ProgramActivityDTO;
import aduro.basic.programservice.service.dto.ProgramStatus;
import aduro.basic.programservice.service.mapper.ProgramActivityMapper;
import aduro.basic.programservice.sns.PublisherService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ProgramActivity}.
 */
@Service
@Transactional
public class ProgramActivityServiceImpl implements ProgramActivityService {

    private final Logger log = LoggerFactory.getLogger(ProgramActivityServiceImpl.class);

    private final ProgramActivityRepository programActivityRepository;

    @Autowired
    private ProgramLevelActivityRepository programLevelActivityRepository;

    @Autowired
    private ProgramLevelRepository programLevelRepository;

    @Autowired
    private NotificationAdapter notificationAdapter;

    @Autowired
    private ProgramUserRepository programUserRepository;

    @Autowired
    private ProgramRepository programRepository;

    private final ProgramActivityMapper programActivityMapper;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;

    @Autowired
    private ProgramCollectionContentRepository  programCollectionContentRepository;

    @Autowired
    private PublisherService publisherService;

    public ProgramActivityServiceImpl(ProgramActivityRepository programActivityRepository, ProgramActivityMapper programActivityMapper) {
        this.programActivityRepository = programActivityRepository;
        this.programActivityMapper = programActivityMapper;
    }

    /**
     * Save a programActivity.
     *
     * @param programActivityDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramActivityDTO save(ProgramActivityDTO programActivityDTO) {
        log.debug("Request to save ProgramActivity : {}", programActivityDTO);

        if(programActivityDTO.getActivityId() == null || programActivityDTO.getActivityId().equals("")){
            throw new BadRequestAlertException("acitivity is invalid", "ProgramActivity", "acivityid");
        }

        ProgramActivity programActivity;
        if (programActivityDTO.isBonusPointsEnabled() != null && programActivityDTO.isBonusPointsEnabled()) {
            // update program bonus points
            programActivity = programActivityRepository.findByProgramIdAndActivityId(programActivityDTO.getProgramId(), programActivityDTO.getActivityId());
            programActivity.setBonusPointsEnabled(programActivityDTO.isBonusPointsEnabled());
            programActivity.setBonusPoints(programActivityDTO.getBonusPoints());
        } else {
            programActivity = programActivityMapper.toEntity(programActivityDTO);
        }

        programActivity = programActivityRepository.save(programActivity);

        log.debug("Request to save ProgramActivity ASSESSMENT begin");
        if (programActivityDTO.getProgramId() != null && programActivityDTO.getActivityType() != null && programActivityDTO.getActivityType().equalsIgnoreCase("ASSESSMENT")) {
            Program program = programRepository.getOne(programActivityDTO.getProgramId());
            log.debug("Request to save ProgramActivity ASSESSMENT program: {}", program);
            if (program != null && program.getStatus() != null && program.getStatus().equalsIgnoreCase(ProgramStatus.Active.toString())) {
                // reset cache of program from Platform
                publisherService.publishProgramUpdatedEvent("", program.getId());

                // TODO need to check Assessment Activity and send push notification to participants
                // TODO need to add to queue later
                List<String> participantIds = programUserRepository.getProgramUsersByProgramId(program.getId()).stream()
                    .filter(Objects::nonNull)
                    .map(p -> p.getParticipantId())
                    .collect(Collectors.toList());
                log.debug("Request to save ProgramActivity ASSESSMENT participantIds: {}", participantIds);
                if (!CollectionUtils.isEmpty(participantIds)) {
                    try {
                        //notify
                        NotificationItemRequest notificationItemRequest = new NotificationItemRequest();
                        notificationItemRequest.setParticipantIds(participantIds);
                        notificationItemRequest.setActivityId(programActivity.getActivityId());
                        log.debug("Request to save ProgramActivity ASSESSMENT notificationItemRequest: {}", notificationItemRequest);
                        notificationAdapter.AddHPANotificationItem(notificationItemRequest);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return programActivityMapper.toDto(programActivity);
    }

    @Override
    public ProgramActivityDTO saveCustomActivity(CustomActivityDTO customActivityDTO) {
        ProgramActivityDTO programActivityDTO = new ProgramActivityDTO();
        try {
            CustomActivityResponseDto customActivityResponseDto = customActivityAdapter.AddCustomActivityToProgram(customActivityDTO);
            if (customActivityResponseDto == null || customActivityResponseDto.getCustomActivity() == null) {
                return programActivityDTO;
            }
            CustomActivityResultDto resultCustomActivity = customActivityResponseDto.getCustomActivity();
            programActivityDTO.setActivityId(String.valueOf(resultCustomActivity.getId()));
            programActivityDTO.setActivityCode(StringUtils.isEmpty(resultCustomActivity.getActivityCode()) ? resultCustomActivity.getTitle() : resultCustomActivity.getActivityCode());
            programActivityDTO.setProgramId(customActivityDTO.getProgramId());
            programActivityDTO.setProgramPhaseId(customActivityDTO.getProgramPhaseId());

            // save program activity
            programActivityDTO = this.save(programActivityDTO);
            programActivityDTO.setTitle(resultCustomActivity.getTitle());
            programActivityDTO.setActivityType(resultCustomActivity.getType());
            programActivityDTO.setSizeOfActivity(resultCustomActivity.getSizeOfActivity());
            programActivityDTO.setCategory(resultCustomActivity.getCategory());
            programActivityDTO.setIsCustomized(customActivityDTO.isCustomized());
            programActivityDTO.setProgramName(customActivityDTO.getProgramName());
            programActivityDTO.setStartDate(customActivityDTO.getStartDate());
            programActivityDTO.setEndDate(customActivityDTO.getEndDate());
        } catch (Exception e) {
            log.error("Error Request to save saveCustomActivity: {}", customActivityDTO);
        }
        return programActivityDTO;
    }

    /**
     * Get all the programActivities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramActivityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramActivities");
        return programActivityRepository.findAll(pageable)
            .map(programActivityMapper::toDto);
    }


    /**
     * Get one programActivity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramActivityDTO> findOne(Long id) {
        log.debug("Request to get ProgramActivity : {}", id);
        return programActivityRepository.findById(id)
            .map(programActivityMapper::toDto);
    }

    /**
     * Delete the programActivity by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public boolean delete(Long id) {
        log.debug("Request to delete ProgramActivity : {}", id);
        List<ProgramLevel> programLevels = programLevelRepository.findProgramLevelsByProgram_Id(id);
        List<Long> levelIds = programLevels.stream().map(l->l.getId()).collect(Collectors.toList());
        ProgramActivity activity = programActivityRepository.getOne(id);

        List<String> customActivitiesIds = new ArrayList<>();
        customActivitiesIds.add(activity.getActivityId());

        // check custom activities do not have any joined users
        boolean invalidToDeleteCustomActivity = customActivityAdapter.invalidDeleteUserJoinedCustomActivity(customActivitiesIds);
        if (invalidToDeleteCustomActivity) {
            return false;
        }

        List<ProgramCollectionContent> programCollectionContents = programCollectionContentRepository.findAllByContentTypeAndItemIdIn(activity.getProgramId(), Constants.ACTIVITY, customActivitiesIds);
        if (!programCollectionContents.isEmpty()) {
            programCollectionContentRepository.deleteAll(programCollectionContents);
        }

        programLevelActivityRepository.deleteProgramLevelActivitiesByActivityIdAndProgramLevelIdIn(activity.getActivityId() ,levelIds);

        programActivityRepository.deleteById(id);

        return true;
    }

    public boolean deleteProgramActivityByActivityIdAndProgramId(String activityId, Long programId) {
        log.debug("Request to delete ProgramActivity : {}", activityId);
        List<String> customActivitiesIds = new ArrayList<>();
        customActivitiesIds.add(activityId);

        // check custom activities do not have any joined users
        boolean invalidToDeleteCustomActivity = customActivityAdapter.invalidDeleteUserJoinedCustomActivity(customActivitiesIds);
        if (invalidToDeleteCustomActivity) {
            return false;
        }

        // delete in collections
        List<ProgramCollectionContent> programCollectionContents = programCollectionContentRepository.findAllByContentTypeAndItemIdIn(programId, Constants.ACTIVITY, customActivitiesIds);
        if (!programCollectionContents.isEmpty()) {
            programCollectionContentRepository.deleteAll(programCollectionContents);
        }

        //delete program level activity first.
        programActivityRepository.deleteProgramActivityByActivityIdAndProgramId(activityId, programId);
        return true;
    }

    @Override
    public List<ProgramActivityDTO> addBulkActivity(Long programId, List<ProgramActivityDTO> programActivityList) {

        List<ProgramActivity> currentList = programActivityRepository.findProgramActivitiesByProgramIdAndProgramPhaseIdIsNotNull(programId);

        for (ProgramActivityDTO dto:programActivityList
             ) {

            if(dto.getActivityId() == null || dto.getActivityId().equals("")){
                throw new BadRequestAlertException("acitivity is invalid", "ProgramActivity", "acivityId");
            }
            if(currentList.stream().anyMatch(a->a.getActivityId().equals(dto.getActivityId()))) continue;
            ProgramActivity programActivity = programActivityRepository.save( programActivityMapper.toEntity(dto));
            dto.setId(programActivity.getId());
        }

        return programActivityList;
    }

    @Override
    public boolean deleteBulkProgramActivities(List<Long> ids, Long programId) {
        if (CollectionUtils.isEmpty(ids)) {
            return true;
        }

        List<ProgramLevel> programLevels = programLevelRepository.findProgramLevelsByProgram_Id(programId);
        List<Long> levelIds = programLevels.stream().map(l->l.getId()).collect(Collectors.toList());
        List<ProgramActivity> programActivityList = programActivityRepository.findByIdIn(ids);

        if (CollectionUtils.isEmpty(programActivityList)) {
            return true;
        }

        List<String> customActivitiesIds = programActivityList.stream()
            .map(pa -> pa.getActivityId()).collect(Collectors.toList());

        // check custom activities do not have any joined users
        boolean invalidToDeleteCustomActivity = customActivityAdapter.invalidDeleteUserJoinedCustomActivity(customActivitiesIds);
        if (invalidToDeleteCustomActivity) {
            return false;
        }

        try {
            // delete activity is assigned to level
            for (ProgramActivity programActivity : programActivityList) {
                programLevelActivityRepository.deleteProgramLevelActivitiesByActivityIdAndProgramLevelIdIn(programActivity.getActivityId(),levelIds);
            }

            // delete program activities
            programActivityRepository.deleteAll(programActivityList);

            DeleteCustomActivityDto deleteCustomActivityDto = new DeleteCustomActivityDto();
            deleteCustomActivityDto.setProgramId(programId);
            deleteCustomActivityDto.setCustomActivityIds(customActivitiesIds);

            // delete custom activities in Platform
            customActivityAdapter.deleteCustomActivitiesInProgram(deleteCustomActivityDto);

            // delete in collections
            List<ProgramCollectionContent> programCollectionContents = programCollectionContentRepository.findAllByContentTypeAndItemIdIn(programId, Constants.ACTIVITY, customActivitiesIds);
            if (!programCollectionContents.isEmpty()) {
                programCollectionContentRepository.deleteAll(programCollectionContents);
            }

        } catch (Exception e) {
            log.error("Request to delete deleteBulkProgramActivities : {}", e.getMessage());
        }
        return true;
    }

    @Override
    public List<CustomActivityDTO> getCustomActivitiesByProgram(String type, Long programId) throws Exception {

        List<CustomActivityDTO> listActivities = customActivityAdapter.getCustomActivityByTypeAndProgramId(type, programId);

        List<ProgramActivity> programActivities = programActivityRepository.findProgramActivitiesByProgramId(programId);

        List<String> ids = programActivities.stream()
            .map(ProgramActivity::getActivityId)
            .collect(Collectors.toList());

        List<CustomActivityDTO> activityDTOList = new ArrayList<>();

        // prevent delete program success but custom_ativities still not deleted.
        for (CustomActivityDTO item : listActivities) {
            if (ids.contains(String.valueOf(item.getId()))) {
                activityDTOList.add(item);
            }
        }

        return activityDTOList;
    }

    @Override
    public boolean checkExitScreeningTypeInPhase(ProgramActivityDTO programActivity, CustomActivityDTO customActivityDTO) {
        String screeningType = "";
        Long programId = programActivity != null ? programActivity.getProgramId() : customActivityDTO.getProgramId();
        Long programPhaseId = programActivity != null ? programActivity.getProgramPhaseId() : customActivityDTO.getProgramPhaseId();
        try {
            if (programActivity != null) {
                // in case of phase change
                CustomActivityDTO customActivityDetail = customActivityAdapter.getCustomActivityDetail(Long.valueOf(programActivity.getActivityId()));
                screeningType = customActivityDetail.getScreeningType();
            }
            screeningType = screeningType.isEmpty() ? customActivityDTO.getScreeningType() : screeningType;
            if (screeningType.isEmpty()) {
                return true;
            }
            if(programPhaseId == 0){
                ProgramActivity programActivitiesPhase =  programActivityRepository.findByProgramIdAndActivityId(programId,String.valueOf(customActivityDTO.getId()));
                programPhaseId = programActivitiesPhase.getProgramPhaseId();
            }
            List<CustomActivityDTO> listActivities = customActivityAdapter.getCustomActivityByTypeAndProgramId(Constants.PREVENTIVE_CUSTOM_ACTIVITY_TYPE, programId);
            List<ProgramActivity> programActivities = programActivityRepository.findProgramActivitiesByProgramPhaseId(programPhaseId);
            List<String> ids = programActivities.stream()
                .map(ProgramActivity::getActivityId)
                .collect(Collectors.toList());

            for (CustomActivityDTO item : listActivities) {
                if (ids.contains(String.valueOf(item.getId())) && screeningType.equals(item.getScreeningType())) {
                    return false;
                }
            }
        } catch (Exception e) {
            log.error("error :", e.getMessage());
        }
        return true;
    }

}
