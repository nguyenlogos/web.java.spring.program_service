package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.NotificationBuilder;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.domain.enumeration.CohortStatus;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.NotificationCenterRepositoryExtension;
import aduro.basic.programservice.repository.extensions.ProgramUserLevelProgressRepositoryExtension;
import aduro.basic.programservice.service.ProgramUserService;
import aduro.basic.programservice.service.businessrule.UserIncentiveBusinessRule;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.*;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.vavr.collection.Seq;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

/**
 * Service Implementation for managing {@link ProgramUser}.
 */
@Service
@Transactional
public class ProgramUserServiceImpl implements ProgramUserService {

    private final Logger log = LoggerFactory.getLogger(ProgramUserServiceImpl.class);
    private static final String PARAM_SHOW_USER_EVENTS = "showUserEvents";

    private final ProgramUserRepository programUserRepository;

    private final ProgramUserMapper programUserMapper;

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private  UserEventMapper userEventMapper;

    @Autowired
    private ProgramLevelPathMapper programLevelPathMapper;

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private ProgramRequirementItemsMapper programRequirementItemsMapper;

    @Autowired
    private NotificationCenterRepositoryExtension notificationCenterRepositoryExtension;

    @Autowired
    private ProgramUserLevelProgressRepositoryExtension programUserLevelProgressRepositoryExtension;

    private final CustomActivityAdapter customActivityAdapter;

    private final UserIncentiveBusinessRule userIncentiveBusinessRule;

    @Autowired
    private UserRewardRepository userRewardRepository;

    @Autowired
    private UserRewardMapper userRewardMapper;

    @Autowired
    private ProgramUserCohortRepository programUserCohortRepository;

    @Autowired
    private ProgramUserCohortMapper programUserCohortMapper;

    @Autowired
    private ProgramUserCollectionProgressRepository programUserCollectionProgressRepository;

    @Autowired
    private ProgramCollectionRepository programCollectionRepository;

    @Autowired
    private ProgramCollectionMapper programCollectionMapper;

    public ProgramUserServiceImpl(ProgramUserRepository programUserRepository, ProgramUserMapper programUserMapper, CustomActivityAdapter customActivityAdapter, UserIncentiveBusinessRule userIncentiveBusinessRule) {
        this.programUserRepository = programUserRepository;
        this.programUserMapper = programUserMapper;
        this.customActivityAdapter = customActivityAdapter;
        this.userIncentiveBusinessRule = userIncentiveBusinessRule;
    }

    /**
     * Save a programUser.
     *
     * @param programUserDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramUserDTO save(ProgramUserDTO programUserDTO) {
        log.debug("Request to save ProgramUser : {}", programUserDTO);
        ProgramUser programUser = programUserMapper.toEntity(programUserDTO);
        programUser = programUserRepository.save(programUser);
        return programUserMapper.toDto(programUser);
    }

    /**
     * Get all the programUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramUserDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramUsers");
        return programUserRepository.findAll(pageable)
            .map(programUserMapper::toDto);
    }


    /**
     * Get one programUser by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramUserDTO> findOne(Long id) {
        log.debug("Request to get ProgramUser : {}", id);
        return programUserRepository.findById(id)
            .map(programUserMapper::toDto);
    }

    /**
     * Delete the programUser by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramUser : {}", id);
        programUserRepository.deleteById(id);
    }


    public ProgramUserDTO getCurrentUserProgramInfo(String participantId, String clientId, String subgroupId, Map<String, String> queryParams) {

        UserDto user = customActivityAdapter.parseUserParams(participantId, queryParams);
        if(user == null){
            throw new BadRequestAlertException("Participant is Not Found", "ActivityEventDTO", "getCurrentUserProgramInfo");
        }

        Optional<ClientProgram> optionalClientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.toString(), user.isAsProgramTester());
        if(!optionalClientProgram.isPresent())
            throw new BadRequestAlertException("program is invalid", "UserEvent", "ProgramIdNull");

        ClientProgram clientProgram = optionalClientProgram.get();

        // check program qc date
        Instant curDate = Instant.now();
        if(clientProgram.getProgram().getQaVerify() && (clientProgram.getProgram().getProgramQAStartDate().isAfter(curDate) ||
            clientProgram.getProgram().getProgramQAEndDate().isBefore(curDate))){
            throw new BadRequestAlertException("program is not in QA verifying time", "UserEvent", "OutOfVefiryTime");
        }
        boolean showUserEvents = (queryParams != null && queryParams.containsKey(PARAM_SHOW_USER_EVENTS)) && Boolean.parseBoolean(queryParams.get(PARAM_SHOW_USER_EVENTS));

        Optional<Program>  optionalProgram = programRepository.findOneWithEagerRelationships(clientProgram.getProgramId());

        if(!optionalProgram.isPresent())
            throw new BadRequestAlertException("program not found", "programUser", "program");

        Optional<ProgramUser> optionalProgramUser = programUserRepository.getCurrentUserProgram(participantId, clientId, clientProgram.getProgramId());
        ProgramUser programUser;
        if(optionalProgramUser.isPresent()) {
            programUser = optionalProgramUser.get();
        } else {
            programUser = new ProgramUser();
            try {
                if (user != null) {
                    programUser.setParticipantId(participantId);
                    programUser.setSubgroupName(user.getSubgroupName());
                    programUser.setEmail(user.getEmail());
                    programUser.setFirstName(user.getFirstName());
                    programUser.setLastName(user.getLastName());
                    programUser.setClientId(clientId);
                    programUser.setProgramId(optionalClientProgram.get().getProgramId());
                    programUser.setCurrentLevel(1);
                    programUser.setSubgroupId(subgroupId);
                    programUser.setTotalUserPoint(BigDecimal.valueOf(0));
                    programUserRepository.save(programUser);
                }
            } catch (Exception e) {
                log.error("participant Id can not find {}", participantId);
            }
        }

        ProgramUserDTO dto =  programUserMapper.toDto(programUser);
        //mapping current status
        //getProgram detail
        Program program = optionalProgram.get();

        Comparator<ProgramLevel> compareBylevel = (ProgramLevel o1, ProgramLevel o2) -> o1.getLevelOrder().compareTo( o2.getLevelOrder() );

        List<ProgramLevel> programLevelList = new ArrayList<>(program.getProgramLevels()) ;

        Collections.sort(programLevelList, compareBylevel);

        List<UserEvent> userEventList = new ArrayList<>();
        if (showUserEvents && programUser.getUserEvents() != null) {
            userEventList = programUser.getUserEvents().stream().collect(Collectors.toList());
            dto.setUserEvents(userEventMapper.toDto(userEventList));
        }

        List<UserReward> userRewardList = programUser.getUserRewards().stream().collect(Collectors.toList());

        List<ProgramUserLevelProgress> allProgramUserLevelProgressList = programUserLevelProgressRepositoryExtension.findByProgramUserIdOrderByCreatedAtAsc(programUser.getId());

        for (ProgramLevel level: programLevelList) {

            UserLevelDTO userLevelDTO = new UserLevelDTO();
            userLevelDTO.setLevel(level.getLevelOrder());
            userLevelDTO.setEndPoint(level.getEndPoint());
            userLevelDTO.setName(level.getDescription());
            userLevelDTO.setDeadline(level.getEndDate());

            if (programUser.getCurrentLevel() != 1) {
                List<ProgramUserLevelProgress> programUserLevelProgressList = allProgramUserLevelProgressList.stream()
                    .filter(p -> p != null && p.getLevelFrom().equals(userLevelDTO.getLevel()))
                    .collect(Collectors.toList());

                if(!CollectionUtils.isEmpty(programUserLevelProgressList)) {
                    ProgramUserLevelProgress programUserLevelProgress = programUserLevelProgressList.get(programUserLevelProgressList.size() - 1);
                    userLevelDTO.setLevelCompletedDate(programUserLevelProgress.getLevelUpAt());
                }  else {
                    if (userLevelDTO.getLevel() < programUser.getCurrentLevel()) {
                        Optional<UserReward> programLevelUser =  userRewardList.stream().filter(p->p.getProgramLevel().equals(userLevelDTO.getLevel())).findFirst();
                        if(programLevelUser.isPresent()) {
                            userLevelDTO.setLevelCompletedDate(programLevelUser.get().getLevelCompletedDate());
                            ProgramUserLevelProgress programUserLevelProgress = new ProgramUserLevelProgress();
                            programUserLevelProgress.setCreatedAt(Instant.now());
                            programUserLevelProgress.setHasReward(true);
                            programUserLevelProgress.setLevelFrom(userLevelDTO.getLevel() - 1);
                            programUserLevelProgress.setLevelTo(userLevelDTO.getLevel());
                            programUserLevelProgress.setLevelUpAt(Instant.now());
                            programUserLevelProgress.setProgramUserId(programUser.getId());
                            List<UserEvent> latestUserEvents = userEventRepository.findByProgramUserIdOrderByEventDateDesc(programUser.getId());
                            BigDecimal sumPointLevel = BigDecimal.valueOf(0);
                            UserEvent userEvent = null;
                            if (!latestUserEvents.isEmpty()) {
                                userEvent = latestUserEvents.get(0);
                                for (UserEvent e : latestUserEvents) {
                                    sumPointLevel.add(e.getEventPoint() != null ? e.getEventPoint() : BigDecimal.valueOf(0));
                                    if (sumPointLevel.intValue() >= userLevelDTO.getEndPoint()) {
                                        userEvent = e;
                                        break;
                                    }
                                }
                            }
                            programUserLevelProgress.setUserEventId(userEvent != null ? userEvent.getId() : 0);
                            programUserLevelProgressRepositoryExtension.save(programUserLevelProgress);
                        }
                    }

                }
            }


            List<ProgramRequirementItems> requirementItems = userIncentiveBusinessRule.getRequiredItemsBySubgroup(level, subgroupId);

            List<ProgramLevelActivity> requiredActivities = userIncentiveBusinessRule.getRequiredActivityBySubgroup(level, subgroupId);

            List<ProgramLevelPath> requiredPaths = userIncentiveBusinessRule.getRequiredPathBySubgroup(level, subgroupId);

            userLevelDTO.setUserLevelPaths(programLevelPathMapper.toDto(requiredPaths));

            if (programUser.isIsTobaccoUser() != null && programUser.isIsTobaccoUser()) {
                userLevelDTO.setTobaccoRequirementItems(programRequirementItemsMapper.toDto(requirementItems));

                for(ProgramRequirementItemsDTO pathDto :  userLevelDTO.getTobaccoRequirementItems()){
                    if (pathDto.getItemCode().equalsIgnoreCase(Constants.PATH)) {
                        Optional<UserEvent> optionalUserEvent = userEventList.stream().filter(e->e.getEventId().equals(pathDto.getItemId())
                            && (e.getEventCode().equals(Constants.PATHS) || e.getEventCode().equals(Constants.WELLMETRIC_TOBACCO_RAS))).findFirst();
                        if(optionalUserEvent.isPresent()){
                            UserEvent userEvent = optionalUserEvent.get();
                            pathDto.setCompletedDate(userEvent.getEventDate());
                            pathDto.setPoint(userEvent.getEventPoint());
                        }
                    } else {
                        Optional<UserEvent> optionalUserEvent = userEventList.stream().filter(e -> e.getEventId().equals(pathDto.getItemId()) &&  e.getEventCategory() != null && e.getEventCategory().equals(Constants.ACTIVITIES)).findFirst();
                        if(optionalUserEvent.isPresent()){
                            UserEvent userEvent = optionalUserEvent.get();
                            pathDto.setCompletedDate(userEvent.getEventDate());
                            pathDto.setPoint(userEvent.getEventPoint());
                        }
                    }
                }
            } else {
                userLevelDTO.setTobaccoRequirementItems(new ArrayList<>());
            }


            for(ProgramLevelPathDTO pathDto :  userLevelDTO.getUserLevelPaths()){
                Optional<UserEvent> optionalUserEvent = userEventList.stream().filter(e->e.getEventId().equals(pathDto.getPathId()) && e.getEventCode().equals(Constants.PATHS)).findFirst();
                if(optionalUserEvent.isPresent()){
                    UserEvent userEvent = optionalUserEvent.get();
                    pathDto.setCompletedDate(userEvent.getEventDate());
                    pathDto.setPoint(userEvent.getEventPoint());
                }

            }

            for (ProgramLevelActivity activity: requiredActivities) {
                UserLevelActivityDTO  userLevelActivityDTO = new UserLevelActivityDTO();
                userLevelActivityDTO.setActivityCode(activity.getActivityCode());
                userLevelActivityDTO.setActivityId(activity.getActivityId());
                userLevelActivityDTO.setLevel(level.getLevelOrder());

                if (!CollectionUtils.isEmpty(userEventList)) {
                    Optional<UserEvent> optionalUserEvent = userEventList.stream().filter(e -> e.getEventId().equals(activity.getActivityId()) &&  e.getEventCategory() != null && e.getEventCategory().equals(Constants.ACTIVITIES)).findFirst();
                    if(optionalUserEvent.isPresent()){
                        UserEvent userEvent = optionalUserEvent.get();
                        userLevelActivityDTO.setCompleted(true);
                        userLevelActivityDTO.setCompletedDate(userEvent.getEventDate());
                        userLevelActivityDTO.setPoint(userEvent.getEventPoint());
                    }
                }

                userLevelDTO.getUserLevelActivities().add(userLevelActivityDTO);

            }



            List<ProgramLevelReward> programLevelRewards = userIncentiveBusinessRule.getProgramLevelRewardsBySubgroup(level,subgroupId);


            for (ProgramLevelReward reward: programLevelRewards) {
                UserLevelRewardDTO userLevelRewardDTO = new UserLevelRewardDTO();
                userLevelRewardDTO.setLevel(level.getLevelOrder());
                userLevelRewardDTO.setCode(reward.getCode());
                userLevelRewardDTO.setDescription(reward.getDescription());
                userLevelRewardDTO.setRewardAmount(reward.getRewardAmount());

                //Set description for mobile display reward info

                if(reward.getRewardType().equals(RewardTypeEnum.TangoCard.toString())){
                    userLevelRewardDTO.setDescription(String.format("$%s %s",reward.getRewardAmount(),"gift card"));
                }
                else if(reward.getRewardType().equals(RewardTypeEnum.HealthSavings.toString())){
                    userLevelRewardDTO.setDescription(String.format("$%s/%s %s",reward.getRewardAmount(), reward.getDescription()," added to your HSA"));
                }
                else if(reward.getRewardType().equals(RewardTypeEnum.PayrollCredits.toString())){
                    userLevelRewardDTO.setDescription(String.format("$%s %s",reward.getRewardAmount()," added to your payroll"));
                }
                else if(reward.getRewardType().equals(RewardTypeEnum.PaidTimeOff.toString())){
                    userLevelRewardDTO.setDescription(String.format("%s %s %s",reward.getRewardAmount(), reward.getDescription()," of paid time off"));
                }
                else if(reward.getRewardType().equals(RewardTypeEnum.Custom.toString())){
                    userLevelRewardDTO.setDescription(reward.getDescription());
                }
                else if(reward.getRewardType().equals(RewardTypeEnum.TobaccoSurcharge.toString())){
                    userLevelRewardDTO.setDescription(String.format("$%s %s %s",reward.getRewardAmount(), reward.getDescription()," of tobacco surcharge"));
                }
                else if(reward.getRewardType().equals(RewardTypeEnum.TobaccoPayrollCredit.toString())){
                    userLevelRewardDTO.setDescription(String.format("$%s %s %s",reward.getRewardAmount(), reward.getDescription()," of tobacco payroll credit"));
                }else if(reward.getRewardType().equals(RewardTypeEnum.TobaccoCustom.toString())){
                    userLevelRewardDTO.setDescription(reward.getDescription());
                }


                if(programUser.getCurrentLevel().compareTo(level.getLevelOrder())>0)
                    userLevelRewardDTO.setCompleted(true);
                else userLevelRewardDTO.setCompleted(false);

                if (programUser.isIsTobaccoUser() != null && programUser.isIsTobaccoUser()) {
                    if (reward.getRewardType().equals(RewardTypeEnum.TobaccoSurcharge.toString()) ||
                        reward.getRewardType().equals(RewardTypeEnum.TobaccoPayrollCredit.toString()) ||
                        reward.getRewardType().equals(RewardTypeEnum.TobaccoCustom.toString())) {
                        userLevelDTO.getUserLevelTobaccoRewards().add(userLevelRewardDTO);
                    } else {
                        userLevelDTO.getUserLevelRewards().add(userLevelRewardDTO);
                    }
                } else  {
                    if (!(reward.getRewardType().equals(RewardTypeEnum.TobaccoSurcharge.toString()) ||
                        reward.getRewardType().equals(RewardTypeEnum.TobaccoPayrollCredit.toString()) ||
                        reward.getRewardType().equals(RewardTypeEnum.TobaccoCustom.toString()))){

                        userLevelDTO.getUserLevelRewards().add(userLevelRewardDTO);
                    }
                }
            }

            dto.getUserlevels().add(userLevelDTO);
        }
        return dto;
    }


    @Override
    public List<ProgramUserWithActivityStatusDTO> getProgramUserWithActivityStatus(Long programId, String clientId, String customActivityId) {
        List<ProgramUser> programUsers =  programUserRepository.getProgramUserWithEventId(customActivityId.toString(), clientId, programId);

        List<ProgramUserWithActivityStatusDTO> programUserWithActivityStatusDTOList = new ArrayList<>();
        for (ProgramUser programUser: programUsers
             ) {

            ProgramUserWithActivityStatusDTO dto = new ProgramUserWithActivityStatusDTO();
            dto.setActivityStatus("Completed");
            dto.setCustomActivityId(customActivityId);
            dto.setFirstName(programUser.getFirstName());
            dto.setLastName(programUser.getLastName());
            dto.setEmail(programUser.getEmail());
            dto.setParticipantId(programUser.getParticipantId());

            programUserWithActivityStatusDTOList.add(dto);
        }

        return programUserWithActivityStatusDTOList;
    }

    @Override
    public ProgramUserDTO updateTobaccoUser(TobaccoUserPayload tobaccoUserPayload) {
        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(tobaccoUserPayload.getParticipantId(), tobaccoUserPayload.getProgramId());
        Option<ProgramUser> programUserOption = Option.ofOptional(programUserOptional);
        ProgramUser user = programUserOption.peek(programUser -> {
            programUser.setIsTobaccoUser(tobaccoUserPayload.getIsTobaccoUser());
            programUserRepository.save(programUser);

            NotificationCenter notificationCenter = NotificationBuilder.notificationRequiredTobaccoUser(programUser.getParticipantId());
            notificationCenterRepositoryExtension.save(notificationCenter);

        }).onEmpty(() -> {

            throw new BadRequestAlertException("Server can not find User with aduroId " + tobaccoUserPayload.getParticipantId() + " programId " + tobaccoUserPayload.getProgramId(), "ProgramUser", "TobaccoUser");
        }).get();

        return programUserMapper.toDto(user);
    }

    @Override
    public List<ProgramPayload> getAllProgramByParticipantIdAndClientId(String participantId, String clientId) {
        Option<String> participantIdOption = Option.of(participantId);
        Option<String> clientIdOption = Option.of(clientId);
        if (!participantIdOption.isDefined()) {
            throw new BadRequestAlertException("ParticipantId can not null or empty.", "Program User", "ParticipantId");
        }
        if (!clientIdOption.isDefined()) {
            throw new BadRequestAlertException("ClientId can not null or empty.", "Program User", "ClientId");
        }
        return programUserRepository.findProgramUserByParticipantIdAndClientId(participantId, clientId)
            .map(programUser -> {
                ProgramPayload programPayload = new ProgramPayload();
                Optional<Program> programOptional = programRepository.findById(programUser.getProgramId());
                if (programOptional.isPresent()) {
                    programPayload.setProgramId(programOptional.get().getId());
                    programPayload.setProgramName(programOptional.get().getName());
                    programPayload.setEndDate(programOptional.get().getResetDate());
                    programPayload.setStartDate(programOptional.get().getStartDate());
                    programPayload.setStatus(programOptional.get().getStatus());
                }
                return  programPayload;
            }).filter(programPayload -> (programPayload.getProgramId() != null || programPayload.getProgramName() != null)).collect(Collectors.toList());
    }

    @Override
    public List<ProgramUserDTO> getAllProgramsByClientIdAndProgramId(String clientId, Long programId) {
        return programUserRepository.findProgramUserByClientIdAndProgramId(clientId, programId)
            .map(user -> programUserMapper.toDto(user)).collect(Collectors.toList());
    }

    @Override
    public boolean verifySubgroupClient(String subgroupId, String clientId) {
        // find clientId ProgramId Activie
        long numberProgramUser = programUserRepository.countAllByClientIdAndSubgroupId(clientId, subgroupId);
        return !(numberProgramUser > 0);
    }

    @Override
    public List<ProgramUserDTO> updateSubgroupsForParticipant(UpdatedSubgroupParticipantPayload participantPayload) {

        String participantId = participantPayload.getParticipantId();
        boolean isAddNew = participantPayload.getIsAddNew();

        if (participantPayload.getSubgroup() != null) {
            ProgramSubgroupDTO subgroupDTO = participantPayload.getSubgroup();
            Seq<ProgramUser> updatedItems = programUserRepository.findProgramUserByParticipantId(participantId);
            if (isAddNew) {
                    List<ProgramUser> res = updatedItems
                    .map( programUser -> {
                        programUser.setSubgroupId(subgroupDTO.getSubgroupId());
                        programUser.setSubgroupName(subgroupDTO.getSubgroupName());
                        return programUserRepository.saveAndFlush(programUser);
                    }).collect(Collectors.toList());
                return res.isEmpty() ? new ArrayList<>() : programUserMapper.toDto(res);
            } else {
                List<ProgramUser> res = updatedItems
                    .map( programUser -> {
                        programUser.setSubgroupId(null);
                        programUser.setSubgroupName(null);
                        return programUserRepository.saveAndFlush(programUser);
                    }).collect(Collectors.toList());
                return updatedItems.isEmpty() ? new ArrayList<>() : programUserMapper.toDto(res);
            }

        }
        return new ArrayList<>();
    }

    @Override
    public List<EarnedUserLevel> extractEarnedLevel(String clientId, Long programId) {

        List<EarnedUserLevel> results = new ArrayList<>();

        List<ProgramUser> programUsers = programUserRepository.findProgramUserByClientIdAndProgramId(clientId, programId).collect(Collectors.toList());
        if (programUsers.isEmpty()) {
            return results;
        }

        for (ProgramUser programUser : programUsers) {

            EarnedUserLevel earnedUserLevel = EarnedUserLevel.builder()
                .build();
            // current user == 1
            if (programUser.getCurrentLevel() == 1) {
                continue;
            }

            // > 1
            Program program = programRepository.getOne(programUser.getProgramId());

            // don't have program levels
            if (program.getProgramLevels().isEmpty()) {
                continue;
            }

            earnedUserLevel.setId(programUser.getId());
            earnedUserLevel.setClientId(programUser.getClientId());
            earnedUserLevel.setClientName(programUser.getClientName());
            earnedUserLevel.setCurrentLevel(programUser.getCurrentLevel());
            earnedUserLevel.setIsTobaccoUser(programUser.isIsTobaccoUser());
            earnedUserLevel.setParticipantId(programUser.getParticipantId());
            earnedUserLevel.setProgramId(programUser.getProgramId());
            earnedUserLevel.setSubgroupId(programUser.getSubgroupId());
            earnedUserLevel.setSubgroupName(programUser.getSubgroupName());

            List<ProgramLevel> programLevels = program.getProgramLevels().stream()
                .filter(programLevel -> programLevel.getLevelOrder() < programUser.getCurrentLevel()).collect(Collectors.toList());

            if (programLevels.isEmpty()) {
                continue;
            }

            List<ExtractedEarnLevelDto> earnLevels = new ArrayList<>();

            for (ProgramLevel programLevel : programLevels) {

                Instant dateCompleted = Instant.now();

                List<UserReward> userRewards = userRewardRepository.findByProgramUserIdAndProgramLevel(programUser.getId(), programLevel.getLevelOrder());

                List<ProgramUserLevelProgress> progressLevels = programUserLevelProgressRepositoryExtension.findByProgramUserIdAndLevelFrom(programUser.getId(), programLevel.getLevelOrder());

                if (!progressLevels.isEmpty()) {
                    dateCompleted = progressLevels.get(0).getLevelUpAt();
                } else {
                    if (!userRewards.isEmpty()) {
                        dateCompleted = userRewards.get(0).getLevelCompletedDate();
                    }
                }

                ExtractedEarnLevelDto earnLevelDto = ExtractedEarnLevelDto.builder()
                    .id(programLevel.getId())
                    .archivedDate(dateCompleted)
                    .name(programLevel.getName())
                    .levelOrder(programLevel.getLevelOrder())
                    .userRewardList(userRewardMapper.toDto(userRewards))
                    .build();

                earnLevels.add(earnLevelDto);
            }
            earnedUserLevel.setEarnedLevels(earnLevels);

            results.add(earnedUserLevel);
        }

        return results;
    }

    @Override
    public APIResponse<MemberProgramDto> subscribeProgram(JoinProgramRequest programRequest) {

        APIResponse<MemberProgramDto> response = new APIResponse<>();

        Optional<ClientProgram> clientProgramOptional = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(programRequest.getClientId(), ProgramStatus.Active.name(), programRequest.isAsProgramTester());
        if (!clientProgramOptional.isPresent()) {
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            response.setMessage("Not Found Client Program.");
            response.getErrors().add(Constants.CLIENT_PROGRAM_ERR);
            return response;
        }

        ClientProgram clientProgram = clientProgramOptional.get();
        Program program = clientProgram.getProgram();
        if (program == null) {
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            response.setMessage("Program Not Found.");
            response.getErrors().add(Constants.PROGRAM_NOT_FOUND);
            return response;
        }

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(programRequest.getParticipantId(), program.getId());
        ProgramUser programUser = new ProgramUser();
        if (!programUserOptional.isPresent()) {
            programUser.setSubgroupId(programRequest.getSubgroupId());
            programUser.setSubgroupName(programRequest.getSubgroupName());
            programUser.setEmail(programRequest.getEmail());
            programUser.setFirstName(programRequest.getFirstName());
            programUser.setLastName(programRequest.getLastName());
            programUser.setParticipantId(programRequest.getParticipantId());
            programUser.setClientId(programRequest.getClientId());
            programUser.setProgramId(program.getId());
            programUser.setCurrentLevel(1);
            programUser.setTotalUserPoint(BigDecimal.valueOf(0));
            programUserRepository.save(programUser);
        } else {
            programUser = programUserOptional.get();
        }

        List<ProgramUserCohort> programUserCohorts = programUserCohortRepository.findProgramUserCohortByProgramUserIdAndStatusIsNotIn(programUser.getId(), Arrays.asList(CohortStatus.PENDING, CohortStatus.DISABLED));

        List<ProgramUserCohortDTO> userCohorts = programUserCohortMapper.toDto(programUserCohorts);


        Set<ProgramCohort> programCohorts = programUserCohorts.stream().map(ProgramUserCohort::getProgramCohort).collect(Collectors.toSet());

        List<ProgramCohortCollection> programCohortCollectionList = new ArrayList<>();

        for (ProgramCohort programCohort : programCohorts) {
            Set<ProgramCohortCollection> programCohortCollections = programCohort.getProgramCohortCollections();
            programCohortCollectionList.addAll(programCohortCollections);
        }

        Set<ProgramLevel> programLevels = program.getProgramLevels();

        List<ProgramLevelCollectionDto> requiredLevelCollections = new ArrayList<>();

        for (ProgramLevel programLevel : programLevels) {

            List<ProgramCohortCollection> requiredCollections = programCohortCollectionList.stream()
                .filter(p ->  Objects.nonNull(p.getRequiredLevel()) && p.getRequiredLevel().equals(programLevel.getLevelOrder())).collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(programCohortCollectionList)) {

                ProgramLevelCollectionDto dto = ProgramLevelCollectionDto.builder().build();
                dto.setId(programLevel.getId());
                dto.setDescription(programLevel.getDescription());
                dto.setEndDate(programLevel.getEndDate());
                dto.setStartPoint(programLevel.getStartPoint());
                dto.setEndPoint(programLevel.getEndPoint());
                dto.setIconPath(programLevel.getIconPath());
                dto.setLevelOrder(programLevel.getLevelOrder());

                List<ProgramCollectionDTO> programCollectionDTOList = new ArrayList<>();

                for (ProgramCohortCollection requiredCollection : requiredCollections) {

                    ProgramCollection programCollection = requiredCollection.getProgramCollection();

                    ProgramCollectionDTO programCollectionDTO = programCollectionMapper.toDto(programCollection);

                    programCollectionDTO.setProgramCohortId(requiredCollection.getProgramCohort().getId());

                    programCollectionDTO.setNumberOfCompletion(requiredCollection.getRequiredCompletion());

                    //get user cohort Id;
                    Optional<ProgramUserCohort> programUserCohortOptional = programUserCohorts.stream()
                        .filter(u -> u.getProgramCohort().getId().equals(requiredCollection.getProgramCohort().getId()))
                        .findFirst();

                    if (!programUserCohortOptional.isPresent()) {
                        continue;
                    }

                    ProgramUserCohort programUserCohort = programUserCohortOptional.get();

                    Optional<ProgramUserCollectionProgress> userCohortCollectionProgressOptional = programUserCollectionProgressRepository.findByProgramUserCohortIdAndCollectionId(programUserCohort.getId(), programCollection.getId());
                    if (!userCohortCollectionProgressOptional.isPresent()) {
                        continue;
                    }
                    programCollectionDTO.setCollectionStatus(userCohortCollectionProgressOptional.get().getStatus());
                    programCollectionDTOList.add(programCollectionDTO);
                }

                Optional<Integer> totalCompletion = requiredCollections.stream()
                    .filter(programCohortCollection -> Objects.nonNull(programCohortCollection.getRequiredCompletion()))
                    .map(ProgramCohortCollection::getRequiredCompletion).reduce(Integer::sum);

                Integer integer = totalCompletion.orElse(0);

                dto.setNumberOfRequired(integer);
                dto.setRequiredCollections(programCollectionDTOList);
                requiredLevelCollections.add(dto);
            }

        }

        List<ProgramCohortCollection> nonRequiredCollections = programCohortCollectionList.stream().filter(p -> isNull(p.getRequiredLevel())).collect(Collectors.toList());

        List<ProgramCollectionDTO> nonRequiredCollectionDtoList = new ArrayList<>();

        for (ProgramCohortCollection requiredCollection : nonRequiredCollections) {

            ProgramCollection programCollection = requiredCollection.getProgramCollection();

            ProgramCollectionDTO programCollectionDTO = programCollectionMapper.toDto(programCollection);

            programCollectionDTO.setProgramCohortId(requiredCollection.getProgramCohort().getId());

            nonRequiredCollectionDtoList.add(programCollectionDTO);
        }

        MemberProgramDto model = MemberProgramDto.builder()
            .clientId(programUser.getClientId())
            .participantId(programUser.getParticipantId())
            .currentLevel(programUser.getCurrentLevel())
            .totalPoint(programUser.getTotalUserPoint())
            .programUserCohorts(userCohorts)
            .programLevelCollections(requiredLevelCollections)
            .programId(program.getId())
            .nonRequiredCollections(nonRequiredCollectionDtoList)
        .build();

        response.setMessage("success");
        response.setResult(model);
        response.setHttpStatus(HttpStatus.OK);
        return response;
    }

    @Override
    public APIResponse<ItemCollectionResponseDto> getInformationAfterScreening(String participantId, String clientId) {

        ItemCollectionResponseDto dto = ItemCollectionResponseDto
            .builder()
            .build();

        APIResponse<ItemCollectionResponseDto> response = new APIResponse<>();

        if (Objects.isNull(participantId)) {
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            response.setMessage("ParticipantId can not be null.");
            response.getErrors().add(Constants.ID_NOT_FOUND);
            return response;
        }

        ///
        UserDto user = null;
        try {
            user = customActivityAdapter.getUser(participantId);
        } catch (Exception e) {
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            response.setMessage("ParticipantId is Not Found");
            response.getErrors().add(Constants.ERR_USER_NOT_FOUND);
            return response;
        }

        Optional<ClientProgram> clientProgramOptional = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.name(), user.isAsProgramTester());
        if (!clientProgramOptional.isPresent()) {
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            response.setMessage("Not Found Client Program.");
            response.getErrors().add(Constants.CLIENT_PROGRAM_ERR);
            return response;
        }

        ClientProgram clientProgram = clientProgramOptional.get();
        Program program = clientProgram.getProgram();
        if (program == null) {
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            response.setMessage("Program Not Found.");
            response.getErrors().add(Constants.PROGRAM_NOT_FOUND);
            return response;
        }

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(participantId, program.getId());

        if (!programUserOptional.isPresent()) {
            response.setHttpStatus(HttpStatus.BAD_REQUEST);
            response.setMessage("User Not Found.");
            response.getErrors().add(Constants.ERR_USER_NOT_FOUND);
            return response;
        }

        ProgramUser programUser = programUserOptional.get();

        List<ProgramUserCohort> programUserCohorts = programUserCohortRepository.getByProgramUserId(programUser.getId());

        List<ProgramCollectionContent> items = new ArrayList<>();

        if (CollectionUtils.isEmpty(programUserCohorts)) {

            dto.setScreening(false);

            //fetching all items in all collection
            List<ProgramCollection> programCollections = programCollectionRepository.getAllByProgramId(programUser.getProgramId());

            for (ProgramCollection userCollection : programCollections) {
                if (!CollectionUtils.isEmpty(userCollection.getProgramCollectionContents())) {
                    items.addAll(userCollection.getProgramCollectionContents());
                }
            }

        } else {

            dto.setScreening(true);

            List<ProgramCollection> userCollections = programUserCohorts.stream()
                .map(ProgramUserCohort::getProgramCohort)
                .flatMap(p -> p.getProgramCohortCollections().stream().map(ProgramCohortCollection::getProgramCollection))
                .collect(Collectors.toList());

            for (ProgramCollection userCollection : userCollections) {
                if (!CollectionUtils.isEmpty(userCollection.getProgramCollectionContents())){
                    items.addAll(userCollection.getProgramCollectionContents());
                }
            }
        }

        List<String> activities = items.stream()
            .filter(i -> Objects.nonNull(i.getContentType()) && i.getContentType().equalsIgnoreCase(Constants.ACTIVITY))
            .map(ProgramCollectionContent::getItemId)
            .collect(Collectors.toList());

        List<String> paths = items.stream()
            .filter(i -> Objects.nonNull(i.getContentType()) && i.getContentType().equalsIgnoreCase(Constants.PATH))
            .map(ProgramCollectionContent::getItemId)
            .collect(Collectors.toList());

        dto.setActivities(activities.stream()
            .filter(Objects::nonNull)
            .map(Integer::parseInt).collect(Collectors.toList()));

        dto.setPaths(paths.stream()
            .filter(Objects::nonNull)
            .map(Integer::parseInt).collect(Collectors.toList()));

        response.setResult(dto);

        response.setMessage("Success fetching");

        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    @Override
    @Transactional
    public void switchSubgroupClient(EmployeeDto payload) {
        if (Objects.isNull(payload)) {
            throw new BadRequestAlertException("Payload is not null.", "ProgramUser", "ChangeSubgroup");
        }

        if (Objects.isNull(payload.getClientId())) {
            throw new BadRequestAlertException("Client is not null.", "ProgramUser", "ClientId");
        }

        if (Objects.isNull(payload.getParticipantId())) {
            throw new BadRequestAlertException("ParticipantId is not null.", "ProgramUser", "ClientId");
        }

        Seq<ProgramUser> programUsers = programUserRepository.findProgramUserByParticipantIdAndClientId(payload.getParticipantId(), payload.getClientId());

        String newSubgroup = (payload.getSubgroupId() == null || payload.getSubgroupId().equals("")) ? null : payload.getSubgroupId();

        if (CollectionUtils.isEmpty(programUsers.collect(Collectors.toList()))) {

            List<ClientProgram> clientPrograms = clientProgramRepository.findByClientIdAndProgramStatus(payload.getClientId(), ProgramStatus.Active.name());
            if (CollectionUtils.isEmpty(clientPrograms)) {
                throw new BadRequestAlertException("Program Active Not Found", "Program", payload.getClientId());
            }

            List<ProgramUser> programUserList = new ArrayList<>();
            for (ClientProgram clientProgram : clientPrograms) {
                ProgramUser programUser = new ProgramUser();
                Program program = clientProgram.getProgram();
                programUser.setSubgroupId(payload.getSubgroupId());
                programUser.setSubgroupName(payload.getSubgroupName());
                programUser.setParticipantId(payload.getParticipantId());
                programUser.setClientId(payload.getClientId());
                programUser.setProgramId(program.getId());
                programUser.setCurrentLevel(1);
                programUser.setTotalUserPoint(BigDecimal.valueOf(0));
                programUserList.add(programUser);
            }
            programUserRepository.saveAll(programUserList);

        } else {

            for (ProgramUser programUser : programUsers) {
                if (Objects.isNull(programUser.getSubgroupId())) {
                    if (Objects.nonNull(newSubgroup)) {
                        programUser.setSubgroupId(payload.getSubgroupId());
                        programUser.setSubgroupName(payload.getSubgroupName());
                    }
                } else {
                    if (Objects.isNull(newSubgroup) || !newSubgroup.equalsIgnoreCase(programUser.getSubgroupId())) {
                        programUser.setSubgroupId(payload.getSubgroupId());
                        programUser.setSubgroupName(payload.getSubgroupName());
                    }
                }
            }
            programUserRepository.saveAll(programUsers);
        }

    }

    @Override
    @Transactional
    public JoinRequestResponseDto joinProgramWithRequest(JoinProgramRequest programRequest) {
        if (programRequest.getClientId() == null) {
            throw new BadRequestAlertException("ClientId can not be null.", "JoinProgram", "ClientIdIsNull");
        }
        if (programRequest.getParticipantId() == null) {
            throw new BadRequestAlertException("ParticipantId can not be null.", "JoinProgram", "ParticipantIsNull");
        }

        JoinRequestResponseDto dto = JoinRequestResponseDto.builder().build();

        UserDto user = null;
        try {
            user = customActivityAdapter.getUser(programRequest.getParticipantId());
        } catch (Exception e) {
            throw new BadRequestAlertException("Participant is Not Found", "JoinProgramRequest", "GetParticpantInfo");
        }

        Optional<ClientProgram> clientProgramOptional = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(programRequest.getClientId(), ProgramStatus.Active.name(), user.isAsProgramTester());
        if (!clientProgramOptional.isPresent()) {
            return dto;
        }
        ClientProgram clientProgram = clientProgramOptional.get();
        Program program = clientProgram.getProgram();
        if (program == null) {
            return dto;
        }

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(programRequest.getParticipantId(), program.getId());
        ProgramUser programUser = new ProgramUser();
        if (!programUserOptional.isPresent()) {
            programUser.setSubgroupId(programRequest.getSubgroupId());
            programUser.setSubgroupName(programRequest.getSubgroupName());
            programUser.setEmail(programRequest.getEmail());
            programUser.setFirstName(programRequest.getFirstName());
            programUser.setLastName(programRequest.getLastName());
            programUser.setParticipantId(programRequest.getParticipantId());
            programUser.setClientId(programRequest.getClientId());
            programUser.setProgramId(program.getId());
            programUser.setCurrentLevel(1);
            programUser.setTotalUserPoint(BigDecimal.valueOf(0));
            programUserRepository.save(programUser);
        } else {
            programUser = programUserOptional.get();
        }

         dto = JoinRequestResponseDto.builder()
            .clientId(programUser.getClientId())
            .currentLevel(programUser.getCurrentLevel())
            .totalPoint(programUser.getTotalUserPoint())
            .participantId(programUser.getParticipantId())
            .programId(program.getId())
            .build();

        return dto;
    }

    @Override
    @Transactional
    public void processCreateProgramUsers(ProgramUserCreateRequest request) {

        Optional<ClientProgram> clientProgramOptional = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(request.getClientId(), ProgramStatus.Active.name(), request.isProgramTester());
        if (!clientProgramOptional.isPresent()) {
            throw new BadRequestAlertException("Client Program  not existed.", "ClientProgram", request.getClientId());
        }

        ClientProgram clientProgram = clientProgramOptional.get();
        Program program = clientProgram.getProgram();
        if (program == null) {
            throw new BadRequestAlertException("Program is not active.", "ClientProgram", request.getClientId());
        }

        Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(request.getAduroId(), program.getId());
        ProgramUser programUser = new ProgramUser();
        if (!programUserOptional.isPresent()) {
            programUser.setSubgroupId(request.getSubgroupId());
            programUser.setSubgroupName(request.getSubgroupName());
            programUser.setEmail(request.getEmail());
            programUser.setFirstName(request.getFirstName());
            programUser.setLastName(request.getLastName());
            programUser.setParticipantId(request.getAduroId());
            programUser.setClientId(request.getClientId());
            programUser.setCurrentLevel(1);
            programUser.setProgramId(program.getId());
            programUser.setTotalUserPoint(BigDecimal.valueOf(0));
            programUserRepository.save(programUser);
        }
    }

    @Override
    @Transactional
    public ProgramUserDTO getProgramUserByParticipantIdAndClientId(String participantId, String clientId) {
        ProgramUser programUser = programUserRepository.findProgramUserByParticipantIdAndClientId(participantId, clientId).collect(Collectors.toList()).get(0);
        return programUserMapper.toDto(programUser);
    };
}
