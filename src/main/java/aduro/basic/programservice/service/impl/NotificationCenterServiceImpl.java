package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.NotificationCenterService;
import aduro.basic.programservice.domain.NotificationCenter;
import aduro.basic.programservice.repository.NotificationCenterRepository;
import aduro.basic.programservice.service.dto.NotificationCenterDTO;
import aduro.basic.programservice.service.mapper.NotificationCenterMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NotificationCenter}.
 */
@Service
@Transactional
public class NotificationCenterServiceImpl implements NotificationCenterService {

    private final Logger log = LoggerFactory.getLogger(NotificationCenterServiceImpl.class);

    private final NotificationCenterRepository notificationCenterRepository;

    private final NotificationCenterMapper notificationCenterMapper;

    public NotificationCenterServiceImpl(NotificationCenterRepository notificationCenterRepository, NotificationCenterMapper notificationCenterMapper) {
        this.notificationCenterRepository = notificationCenterRepository;
        this.notificationCenterMapper = notificationCenterMapper;
    }

    /**
     * Save a notificationCenter.
     *
     * @param notificationCenterDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public NotificationCenterDTO save(NotificationCenterDTO notificationCenterDTO) {
        log.debug("Request to save NotificationCenter : {}", notificationCenterDTO);
        NotificationCenter notificationCenter = notificationCenterMapper.toEntity(notificationCenterDTO);
        notificationCenter = notificationCenterRepository.save(notificationCenter);
        return notificationCenterMapper.toDto(notificationCenter);
    }

    /**
     * Get all the notificationCenters.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<NotificationCenterDTO> findAll(Pageable pageable) {
        log.debug("Request to get all NotificationCenters");
        return notificationCenterRepository.findAll(pageable)
            .map(notificationCenterMapper::toDto);
    }


    /**
     * Get one notificationCenter by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<NotificationCenterDTO> findOne(Long id) {
        log.debug("Request to get NotificationCenter : {}", id);
        return notificationCenterRepository.findById(id)
            .map(notificationCenterMapper::toDto);
    }

    /**
     * Delete the notificationCenter by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete NotificationCenter : {}", id);
        notificationCenterRepository.deleteById(id);
    }
}
