package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramSubCategoryConfigurationService;
import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import aduro.basic.programservice.repository.ProgramSubCategoryConfigurationRepository;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationDTO;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryConfigurationMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramSubCategoryConfiguration}.
 */
@Service
@Transactional
public class ProgramSubCategoryConfigurationServiceImpl implements ProgramSubCategoryConfigurationService {

    private final Logger log = LoggerFactory.getLogger(ProgramSubCategoryConfigurationServiceImpl.class);

    private final ProgramSubCategoryConfigurationRepository programSubCategoryConfigurationRepository;

    private final ProgramSubCategoryConfigurationMapper programSubCategoryConfigurationMapper;

    public ProgramSubCategoryConfigurationServiceImpl(ProgramSubCategoryConfigurationRepository programSubCategoryConfigurationRepository, ProgramSubCategoryConfigurationMapper programSubCategoryConfigurationMapper) {
        this.programSubCategoryConfigurationRepository = programSubCategoryConfigurationRepository;
        this.programSubCategoryConfigurationMapper = programSubCategoryConfigurationMapper;
    }

    /**
     * Save a programSubCategoryConfiguration.
     *
     * @param programSubCategoryConfigurationDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramSubCategoryConfigurationDTO save(ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO) {
        log.debug("Request to save ProgramSubCategoryConfiguration : {}", programSubCategoryConfigurationDTO);
        ProgramSubCategoryConfiguration programSubCategoryConfiguration = programSubCategoryConfigurationMapper.toEntity(programSubCategoryConfigurationDTO);
        programSubCategoryConfiguration = programSubCategoryConfigurationRepository.save(programSubCategoryConfiguration);
        return programSubCategoryConfigurationMapper.toDto(programSubCategoryConfiguration);
    }

    /**
     * Get all the programSubCategoryConfigurations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramSubCategoryConfigurationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramSubCategoryConfigurations");
        return programSubCategoryConfigurationRepository.findAll(pageable)
            .map(programSubCategoryConfigurationMapper::toDto);
    }


    /**
     * Get one programSubCategoryConfiguration by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramSubCategoryConfigurationDTO> findOne(Long id) {
        log.debug("Request to get ProgramSubCategoryConfiguration : {}", id);
        return programSubCategoryConfigurationRepository.findById(id)
            .map(programSubCategoryConfigurationMapper::toDto);
    }

    /**
     * Delete the programSubCategoryConfiguration by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramSubCategoryConfiguration : {}", id);
        programSubCategoryConfigurationRepository.deleteById(id);
    }
}
