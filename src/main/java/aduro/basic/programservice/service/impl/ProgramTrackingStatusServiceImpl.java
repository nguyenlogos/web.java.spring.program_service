package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramTrackingStatusService;
import aduro.basic.programservice.domain.ProgramTrackingStatus;
import aduro.basic.programservice.repository.ProgramTrackingStatusRepository;
import aduro.basic.programservice.service.dto.ProgramTrackingStatusDTO;
import aduro.basic.programservice.service.mapper.ProgramTrackingStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramTrackingStatus}.
 */
@Service
@Transactional
public class ProgramTrackingStatusServiceImpl implements ProgramTrackingStatusService {

    private final Logger log = LoggerFactory.getLogger(ProgramTrackingStatusServiceImpl.class);

    private final ProgramTrackingStatusRepository programTrackingStatusRepository;

    private final ProgramTrackingStatusMapper programTrackingStatusMapper;

    public ProgramTrackingStatusServiceImpl(ProgramTrackingStatusRepository programTrackingStatusRepository, ProgramTrackingStatusMapper programTrackingStatusMapper) {
        this.programTrackingStatusRepository = programTrackingStatusRepository;
        this.programTrackingStatusMapper = programTrackingStatusMapper;
    }

    /**
     * Save a programTrackingStatus.
     *
     * @param programTrackingStatusDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramTrackingStatusDTO save(ProgramTrackingStatusDTO programTrackingStatusDTO) {
        log.debug("Request to save ProgramTrackingStatus : {}", programTrackingStatusDTO);
        ProgramTrackingStatus programTrackingStatus = programTrackingStatusMapper.toEntity(programTrackingStatusDTO);
        programTrackingStatus = programTrackingStatusRepository.save(programTrackingStatus);
        return programTrackingStatusMapper.toDto(programTrackingStatus);
    }

    /**
     * Get all the programTrackingStatuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramTrackingStatusDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramTrackingStatuses");
        return programTrackingStatusRepository.findAll(pageable)
            .map(programTrackingStatusMapper::toDto);
    }


    /**
     * Get one programTrackingStatus by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramTrackingStatusDTO> findOne(Long id) {
        log.debug("Request to get ProgramTrackingStatus : {}", id);
        return programTrackingStatusRepository.findById(id)
            .map(programTrackingStatusMapper::toDto);
    }

    /**
     * Delete the programTrackingStatus by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramTrackingStatus : {}", id);
        programTrackingStatusRepository.deleteById(id);
    }
}
