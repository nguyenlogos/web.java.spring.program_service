package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.adp.domain.AmpUserActivityProgress;
import aduro.basic.programservice.adp.repository.AmpUserActivityProgressRepository;
import aduro.basic.programservice.adp.service.AmpUserActivityProgressService;
import aduro.basic.programservice.ap.ActivityAdapter;
import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.NotificationAdapter;
import aduro.basic.programservice.ap.dto.*;
import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.config.Settings;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.helpers.DateHelpers;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.ProgramUserLevelProgressRepositoryExtension;
import aduro.basic.programservice.service.ActivityEventQueueService;
import aduro.basic.programservice.service.businessrule.UserIncentiveBusinessRule;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.ActivityEventQueueMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link ActivityEventQueue}.
 */
@Service
@Transactional
public class ActivityEventQueueServiceImpl implements ActivityEventQueueService {

    private final Logger log = LoggerFactory.getLogger(ActivityEventQueueServiceImpl.class);

    private final ActivityEventQueueRepository activityEventQueueRepository;

    private final ActivityEventQueueMapper activityEventQueueMapper;

    private final String EMPLOYER_VERIFIED = "EMPLOYER_VERIFIED";

    private final String BONUS_ACTIVITY_EMPLOYER_VERIFIED = "BONUS_ACTIVITY_EMPLOYER_VERIFIED";

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    @Autowired
    private ActivityAdapter activityAdapter;

    @Autowired
    private Settings settings;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;


    @Autowired
    NotificationAdapter notificationAdapter;

    @Autowired
    ProgramRepository programRepository;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private ProgramUserRepository programUserRepository;

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private UserIncentiveBusinessRule userIncentiveBusinessRule;

    @Autowired
    private UserRewardRepository userRewardRepository;

    @Autowired
    private AmpUserActivityProgressRepository ampUserActivityProgressRepository;

    @Autowired
    private AmpUserActivityProgressService ampUserActivityProgressService;

    @Autowired
    private ProgramUserLevelProgressRepositoryExtension programUserLevelProgressRepository;

    @Autowired
    private ProgramActivityRepository programActivityRepository;

    public ActivityEventQueueServiceImpl(ActivityEventQueueRepository activityEventQueueRepository, ActivityEventQueueMapper activityEventQueueMapper) {
        this.activityEventQueueRepository = activityEventQueueRepository;
        this.activityEventQueueMapper = activityEventQueueMapper;
    }

    /**
     * Save a activityEventQueue.
     *
     * @param activityEventQueueDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ActivityEventQueueDTO save(ActivityEventQueueDTO activityEventQueueDTO) {
        log.debug("Request to save ActivityEventQueue : {}", activityEventQueueDTO);
        ActivityEventQueue activityEventQueue = activityEventQueueMapper.toEntity(activityEventQueueDTO);
        activityEventQueue.setExecuteStatus(ActivityEventQueueStatus.New.toString());
        activityEventQueue = activityEventQueueRepository.save(activityEventQueue);
        return activityEventQueueMapper.toDto(activityEventQueue);
    }


    @Override
    public List<ActivityEventQueueDTO> saveList(List<ActivityEventQueueDTO> activityEventQueueDTOs) {
        log.debug("Request to save ActivityEventQueue : {}", activityEventQueueDTOs);
        List<ActivityEventQueue> activityEventQueues = activityEventQueueMapper.toEntity(activityEventQueueDTOs);
        activityEventQueues.forEach(a->a.setExecuteStatus(ActivityEventQueueStatus.New.toString()));
        activityEventQueues = activityEventQueueRepository.saveAll(activityEventQueues);
        return activityEventQueueMapper.toDto(activityEventQueues);
    }


    /**
     * Get all the activityEventQueues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ActivityEventQueueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ActivityEventQueues");
        return activityEventQueueRepository.findAll(pageable)
            .map(activityEventQueueMapper::toDto);
    }


    /**
     * Get one activityEventQueue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ActivityEventQueueDTO> findOne(Long id) {
        log.debug("Request to get ActivityEventQueue : {}", id);
        return activityEventQueueRepository.findById(id)
            .map(activityEventQueueMapper::toDto);
    }

    /**
     * Delete the activityEventQueue by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ActivityEventQueue : {}", id);
        activityEventQueueRepository.deleteById(id);
    }

    @Override
    public CheckEmployerVerifiedDTO validateEmployerVierified(CheckEmployerVerifiedDTO verfifiedDTO) throws Exception {

        if(isNullOrEmpty(verfifiedDTO.getClientId()) || isNullOrEmpty(verfifiedDTO.getActivityCode()))
            throw new BadRequestAlertException("client id or activity code are empty", "EmployerVerified", "idnull");

        if (verfifiedDTO.getCustomActivityId() == null) {
            throw new BadRequestAlertException("Invalid custom activity id", "EmployerVerified", "IdNull");
        }

        CustomActivityDTO activityDTO = customActivityAdapter.getCustomActivityDetail(verfifiedDTO.getCustomActivityId());
        if (activityDTO == null) {
            throw new BadRequestAlertException("Invalid custom activity id", "EmployerVerified", "idInvalid");
        }

        if (!activityDTO.getActivityCode().equalsIgnoreCase(verfifiedDTO.getActivityCode())) {
            throw new BadRequestAlertException("Invalid custom activity code", "EmployerVerified", "ActivityCodeInvalid");
        }

        //Validate client with program active.

        Optional<ClientProgram> cp = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(verfifiedDTO.getClientId(), ProgramStatus.Active.toString(), false);
        if(!cp.isPresent())
            throw new BadRequestAlertException("Program is not active", "EmployerVerified", "idnull");

        Program program = cp.get().getProgram();

        verfifiedDTO.setProgramStartDate(program.getStartDate());
        verfifiedDTO.setProgramEndDate(program.getResetDate());
        verfifiedDTO.setValid(true);

        //check type activity
        if (!activityDTO.getType().equalsIgnoreCase(Constants.EMPLOYER_VERIFIED)) {
            throw new BadRequestAlertException("Type of This activity is not employer verified.", "EmployerVerified", "TypeInvalid");
        }

        if (activityDTO.getProgramId() != program.getId()) {
            throw new BadRequestAlertException("Program of This Activity and Client Program is not the same.", "EmployerVerified", "ProgramInvalid");
        }
        return verfifiedDTO;

    }

    /**
     * Update status program base on todate
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 */11 * * * *")
    @SchedulerLock(name = "processEmployerVierifiedActivity")
    public void processEmployerVierifiedActivity() throws Exception {
        if(!settings.isScronJobEnabled()){
            return;
        }
        log.info("Start process employer verified activity" + Instant.now().toString());
        Pageable pageRequest = PageRequest.of(0, 50);
        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findByExecuteStatus(ActivityEventQueueStatus.New.toString(), pageRequest);

        if (CollectionUtils.isEmpty(activityEventQueueList)) {
            return;
        }
        Map<String, UserDto> userDtoMap = new HashMap<>();
        SearchUserRequest userRequest = new SearchUserRequest();
        List<String> aduroIds = activityEventQueueList.stream()
                .map(item -> item.getParticipantId()).collect(Collectors.toList());
        userRequest.setAduroIds(aduroIds);

        SearchUserResponse searchUserResponse = customActivityAdapter.searchUsers(userRequest);
        if (!CollectionUtils.isEmpty(searchUserResponse.getUsers())) {
            // put the user result into map
            searchUserResponse.getUsers().forEach(user -> {
                userDtoMap.put(user.getAduroId(), user);
            });
        }

        for (ActivityEventQueue item: activityEventQueueList) {
            Integer count = item.getAttemptedCount() != null ? item.getAttemptedCount(): 0;

            if (count > 5) {
                item.setErrorMessage("Processing is over 5 times with errors.");
                item.setExecuteStatus(IncentiveStatusEnum.Error.name());
                activityEventQueueRepository.save(item);
                return;
            }

            item.setAttemptedCount(count + 1);

            if (!userDtoMap.containsKey(item.getParticipantId())) {
                item.setErrorMessage("Participant is Not Found");
                item.setExecuteStatus(IncentiveStatusEnum.Error.name());
                activityEventQueueRepository.save(item);
                return;
            }

            UserDto user = userDtoMap.get(item.getParticipantId());
            Optional<ClientProgram> optionalClientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(activityEventQueueList.get(0).getClientId(), ProgramStatus.Active.toString(), user.isAsProgramTester());
            if(!optionalClientProgram.isPresent()) {
                item.setErrorMessage("Client program is not found.");
                item.setExecuteStatus(IncentiveStatusEnum.Error.name());
                activityEventQueueRepository.save(item);
                return;
            }

            Optional<Program>  optionalProgram = programRepository.findById(optionalClientProgram.get().getProgramId());
            if(!optionalProgram.isPresent()) {
                item.setErrorMessage("Program is not found.");
                item.setExecuteStatus(IncentiveStatusEnum.Error.name());
                activityEventQueueRepository.save(item);
                return;
            }

            if (item.getCustomActivityId() == null) {
                item.setErrorMessage("Param Employer verified is null.");
                item.setExecuteStatus(IncentiveStatusEnum.Error.name());
                activityEventQueueRepository.save(item);
                return;
            }

            CustomActivityDTO customActivityDTO;
            try {
                customActivityDTO = customActivityAdapter.getCustomActivityDetail(item.getCustomActivityId());
                if(customActivityDTO == null || !customActivityDTO.getType().equalsIgnoreCase(Constants.EMPLOYER_VERIFIED)) {
                    item.setErrorMessage("Employer verified is not found from AMP.");
                    item.setExecuteStatus(IncentiveStatusEnum.Error.name());
                    activityEventQueueRepository.save(item);
                    return;
                }
            } catch (Exception ex) {
                item.setErrorMessage("Error AMP=" + ex.getMessage());
                item.setExecuteStatus(IncentiveStatusEnum.Error.name());
                activityEventQueueRepository.save(item);
                return;
            }

            Program program = optionalProgram.get();
            BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program, applicationProperties);

            ProgramSubCategoryPoint programSubCategoryPoint = null;
            for (ProgramCategoryPoint categoryPoint: program.getProgramCategoryPoints()) {
                for (ProgramSubCategoryPoint subPoint:categoryPoint.getProgramSubCategoryPoints()){
                    if(subPoint.getCode().equals(EMPLOYER_VERIFIED)){
                        programSubCategoryPoint = subPoint;
                        break;
                    }
                }
            }

            ProgramActivity programActivity = programActivityRepository.findByProgramIdAndActivityId(program.getId(), String.valueOf(customActivityDTO.getId()));

            if (programSubCategoryPoint != null || programActivity != null && programActivity.isBonusPointsEnabled() != null && programActivity.isBonusPointsEnabled()) {
                processEmployerVierifiedForOneParticipant(item, customActivityDTO.getId(), program, totalUserPoint, programSubCategoryPoint);
            }
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void processEmployerVierifiedForOneParticipant(ActivityEventQueue activityEventQueue, long customActivityId, Program program, BigDecimal totalUserPoint, ProgramSubCategoryPoint subPoint) throws Exception {

        try{
            UserDto user = customActivityAdapter.getUser(activityEventQueue.getParticipantId());

            if(user != null && user.getUserId() >0 ) {

                // if user already complete this activity, it can not able to get incentive anymore
                List<AmpUserActivityProgress> history = ampUserActivityProgressRepository.findByUserIdAndCustomActivityId(user.getUserId(), customActivityId);

                if (!history.isEmpty() && history.get(0).getCompletedAt() != null) {
                    activityEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.toString());
                    activityEventQueue.setErrorMessage("User has already completed this activity.");
                    activityEventQueueRepository.save(activityEventQueue);
                    return;
                }

                //add incentive
                Optional<ProgramUser> optionalProgramUser =  programUserRepository.findProgramUserByParticipantIdAndProgramId(activityEventQueue.getParticipantId(), program.getId());
                ProgramUser programUser;

                //create new program user if first event
                if(!optionalProgramUser.isPresent()){
                    programUser = new ProgramUser();
                    programUser.setParticipantId(activityEventQueue.getParticipantId());
                    programUser.setClientId(activityEventQueue.getClientId());
                    programUser.setEmail(activityEventQueue.getEmail());
                    programUser.setFirstName(activityEventQueue.getFirstName());
                    programUser.setLastName(activityEventQueue.getLastName());
                    programUser.setProgramId(program.getId());
                    programUser.setCurrentLevel(1);
                    programUser.setTotalUserPoint(BigDecimal.valueOf(0));
                    programUser.setSubgroupId(activityEventQueue.getSubgroupId());
                    programUser = programUserRepository.save(programUser);
                }else {
                    programUser = optionalProgramUser.get();
                }

                ProgramActivity programActivity = programActivityRepository.findByProgramIdAndActivityId(program.getId(), String.valueOf(customActivityId));
                if (programActivity == null) {
                    activityEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.toString());
                    activityEventQueue.setErrorMessage("Activity is not found.");
                    activityEventQueueRepository.save(activityEventQueue);
                    return;
                }

                //save user event and calculate point
                UserEvent userEvent = new UserEvent();
                userEvent.setProgramUser(programUser);
                userEvent.setEventId(Long.toString(customActivityId));
                userEvent.setEventDate(activityEventQueue.getCreatedDate());

                if (programActivity.isBonusPointsEnabled() != null
                    && programActivity.isBonusPointsEnabled()) {
                    if (subPoint == null) {
                        // initial sub category in case it is empty
                        subPoint = new ProgramSubCategoryPoint();
                        subPoint.setName(Constants.BONUS);
                        subPoint.setCode(Constants.BONUS);
                    }

                    userEvent.setEventCode(BONUS_ACTIVITY_EMPLOYER_VERIFIED);
                    userEvent.setEventCategory(Constants.BONUS);
                } else {
                    userEvent.setEventCode(EMPLOYER_VERIFIED);
                    userEvent.setEventPoint(BigDecimal.valueOf(0.0));
                    userEvent.setEventCategory("ACTIVITIES");
                }

                if(subPoint != null){
                    if (programActivity.isBonusPointsEnabled() != null && programActivity.isBonusPointsEnabled()) {
                        BigDecimal totalUserEventBonus = userEventRepository
                            .findUserEventsByProgramUserIdAndEventIdAndEventCategory(programUser.getId(), userEvent.getEventId(), Constants.BONUS)
                            .stream().filter(u -> u.getEventPoint() != null).map(u -> u.getEventPoint())
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                        BigDecimal valuePoints = totalUserEventBonus.compareTo(programActivity.getBonusPoints()) > 0 ? BigDecimal.valueOf(0) : programActivity.getBonusPoints();
                        userEvent.setEventPoint(valuePoints);
                    } else if(program.isIsUsePoint() != null && program.isIsUsePoint()) {
                        long completeCap = userEventRepository.countAllByEventCodeAndProgramUserId(userEvent.getEventCode(), programUser.getId());
                        if(completeCap<subPoint.getCompletionsCap())
                            userEvent.setEventPoint(CommonUtils.roundPoint(subPoint.getValuePoint()));
                    }

                    BigDecimal currentTotalUserPoint = userEventRepository.findAllByProgramUserId(programUser.getId())
                        .stream().filter(u -> u.getEventPoint() != null)
                        .map(UserEvent::getEventPoint)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                    BigDecimal finalTotalPoint = currentTotalUserPoint.add(userEvent.getEventPoint());
                    if(totalUserPoint.compareTo(finalTotalPoint)>= 0)
                        programUser.setTotalUserPoint(finalTotalPoint);
                    else
                        programUser.setTotalUserPoint(totalUserPoint);

                    // prevent duplicate data event
                    Instant startOfDate = DateHelpers.getStarOfDay(Date.from(userEvent.getEventDate())).toInstant();
                    Instant endOfDate = DateHelpers.getEndOfDay(Date.from(userEvent.getEventDate())).toInstant();
                    List<UserEvent> userEventsInDay = userEventRepository.findByProgramUserIdAndEventIdAndEventCodeAndEventDateBetween(programUser.getId(), userEvent.getEventId(), userEvent.getEventCode(), startOfDate, endOfDate);
                    if (!userEventsInDay.isEmpty()) {
                        activityEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.toString());
                        activityEventQueue.setErrorMessage("UserEvent is duplicated.");
                        activityEventQueueRepository.save(activityEventQueue);
                        return;
                    }

                    // save user event to check level up
                    userEventRepository.save(userEvent);

                    // do complete cohorts if have
                    userIncentiveBusinessRule.doCompletedCollectionsInCohorts(programUser);

                    int currentLevel = programUser.getCurrentLevel();
                    ProgramLevel currentProgramLevel;
                    Optional<ProgramLevel> optionalProgramLevel = program.getProgramLevels().stream().filter(l -> l.getLevelOrder().equals(currentLevel)).findFirst();
                    if(optionalProgramLevel.isPresent()){
                        boolean upLevel = true;
                        currentProgramLevel = optionalProgramLevel.get();

                        //check required point
                        if(program.isIsUsePoint() != null && program.isIsUsePoint()){
                            if(currentProgramLevel.getEndPoint().compareTo(programUser.getTotalUserPoint().intValue())>0)
                                upLevel = false;
                        }
                        //check required activity
                        if(upLevel){
                            List<String> listRequired  =  userIncentiveBusinessRule.getRequiredActivityBySubgroup(currentProgramLevel, activityEventQueue.getSubgroupId()).stream().map(a->a.getActivityId()).collect(Collectors.toList());
                            if(listRequired.size() > 0){
                                List<UserEvent> userEvents  =  userEventRepository.findUserEventsByProgramUserIdAndEventIdIn(programUser.getId(), listRequired);
                                List<String> userEventIds = userEvents.stream().map(u->u.getEventId()).collect(Collectors.toList());
                                if(!listRequired.stream().allMatch(r->userEventIds.contains(r)))
                                    upLevel = false;
                            }
                        }

                        if (programUser.isIsTobaccoUser() != null && programUser.isIsTobaccoUser()) {
                            if(upLevel){
                                List<String> listRequired  =  userIncentiveBusinessRule.getRequirementRASBySubgroup(currentProgramLevel, activityEventQueue.getSubgroupId());
                                if(listRequired.size() > 0){
                                    List<UserEvent> userEvents  =  userEventRepository.findUserEventsByProgramUserIdAndEventIdIn(programUser.getId(), listRequired);
                                    List<String> userEventIds = userEvents.stream().map(u->u.getEventId()).collect(Collectors.toList());
                                    if(!listRequired.stream().allMatch(r->userEventIds.contains(r)))
                                        upLevel = false;
                                }
                            }
                        }

                        //calculate user in cohorts
                        if (upLevel) {
                            upLevel = userIncentiveBusinessRule.isCompletedCohorts(programUser);
                        }


                        //upgrade if User up level
                        // Add all rewards
                        if(upLevel && currentProgramLevel != null) {

                            List<UserReward> userRewards = new ArrayList<>();

                            List<ProgramLevelReward> levelRewards = userIncentiveBusinessRule.getProgramLevelRewardsBySubgroup(currentProgramLevel, activityEventQueue.getSubgroupId());

                            List<ProgramLevelReward> programLevelRewards = new ArrayList<>();

                            if (programUser.isIsTobaccoUser() == null) {
                                programLevelRewards = levelRewards.stream()
                                    .filter(reward -> reward.getRewardType() != null && !reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoSurcharge.toString()))
                                    .filter(reward -> reward.getRewardType() != null && !reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoPayrollCredit.toString()))
                                    .filter(reward -> reward.getRewardType() != null && !reward.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoCustom.toString()))
                                    .collect(Collectors.toList());
                            } else {
                                programLevelRewards = levelRewards;
                            }

                            if (!programLevelRewards.isEmpty()) {
                                for (ProgramLevelReward rw: programLevelRewards) {
                                    UserReward  userReward = new UserReward();
                                    userReward.setProgramUser(programUser);
                                    userReward.setProgram(program);
                                    userReward.setClientId(activityEventQueue.getClientId());
                                    userReward.setParticipantId(activityEventQueue.getParticipantId());
                                    userReward.setEmail(activityEventQueue.getEmail());
                                    userReward.setLevelCompletedDate(Instant.now());
                                    userReward.setProgramLevel(programUser.getCurrentLevel());
                                    userReward.setStatus(IncentiveStatusEnum.New.toString());
                                    userReward.setRewardType(rw.getRewardType());
                                    userReward.setRewardAmount(rw.getRewardAmount());
                                    userReward.setRewardCode(rw.getDescription());
                                    userReward.setCampaignId(rw.getCampaignId());
                                    userRewards.add(userReward);
                                }

                                //save rewards
                                if (!CollectionUtils.isEmpty(userRewards)) {
                                    List<UserReward> currentUserRewards = userRewardRepository.findByProgramUserIdAndProgramLevel(programUser.getId(), currentLevel);
                                    if (CollectionUtils.isEmpty(currentUserRewards)) {
                                        userRewardRepository.saveAll(userRewards);
                                    }
                                }
                            }

                            Integer presentLevel = programUser.getCurrentLevel();

                            List<ProgramUserLevelProgress> progressList = programUserLevelProgressRepository.findByProgramUserIdAndLevelFrom(programUser.getId(), presentLevel);

                            programUser.setCurrentLevel(programUser.getCurrentLevel() + 1);

                            if (progressList.isEmpty()) {
                                ProgramUserLevelProgress programUserLevelProgress = new ProgramUserLevelProgress();
                                programUserLevelProgress.setCreatedAt(Instant.now());
                                programUserLevelProgress.setHasReward(!userRewards.isEmpty());
                                programUserLevelProgress.setLevelFrom(presentLevel);
                                programUserLevelProgress.setLevelTo(presentLevel + 1);
                                programUserLevelProgress.setLevelUpAt(Instant.now());
                                programUserLevelProgress.setProgramUserId(programUser.getId());
                                programUserLevelProgress.setUserEventId(userEvent.getId());
                                programUserLevelProgressRepository.save(programUserLevelProgress);
                            }
                        }

                    }

                }

                programUserRepository.save(programUser);

                //Add progress and history
                if (history.isEmpty()) {
                    ampUserActivityProgressService.AddEmployerVerifiedActivity(activityEventQueue, customActivityId, user.getUserId());
                } else {
                    AmpUserActivityProgress progress = history.get(0);
                    ampUserActivityProgressService.updateEmployerVerifiedActivity(progress, customActivityId, user.getUserId());
                }

                //notify
                NotificationItemRequest notificationItemRequest = new NotificationItemRequest();
                notificationItemRequest.setRewardContent("Congratulations!You received points for completing an activity within your Aduro Wellness program");
                notificationItemRequest.setParticipantId(activityEventQueue.getParticipantId());
                notificationItemRequest.setTitle("Points Earned!\uD83C\uDF89");
                notificationItemRequest.setNotificationType(EMPLOYER_VERIFIED);
                CompletableFuture<NotificationItemResponse> orderResponseCompletableFuture =  notificationAdapter.AddNotificationItem(notificationItemRequest);
                activityEventQueue.setExecuteStatus(IncentiveStatusEnum.Completed.toString());
            }
        }catch (Exception ex){
            ex.printStackTrace();
            activityEventQueue.setExecuteStatus(IncentiveStatusEnum.Error.toString());
            activityEventQueue.setErrorMessage(ex.getMessage());
        }finally {

        }
        activityEventQueueRepository.save(activityEventQueue);

    }



}
