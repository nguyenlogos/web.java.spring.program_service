package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramUserCollectionProgressService;
import aduro.basic.programservice.domain.ProgramUserCollectionProgress;
import aduro.basic.programservice.repository.ProgramUserCollectionProgressRepository;
import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressDTO;
import aduro.basic.programservice.service.mapper.ProgramUserCollectionProgressMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramUserCollectionProgress}.
 */
@Service
@Transactional
public class ProgramUserCollectionProgressServiceImpl implements ProgramUserCollectionProgressService {

    private final Logger log = LoggerFactory.getLogger(ProgramUserCollectionProgressServiceImpl.class);

    private final ProgramUserCollectionProgressRepository programUserCollectionProgressRepository;

    private final ProgramUserCollectionProgressMapper programUserCollectionProgressMapper;

    public ProgramUserCollectionProgressServiceImpl(ProgramUserCollectionProgressRepository programUserCollectionProgressRepository, ProgramUserCollectionProgressMapper programUserCollectionProgressMapper) {
        this.programUserCollectionProgressRepository = programUserCollectionProgressRepository;
        this.programUserCollectionProgressMapper = programUserCollectionProgressMapper;
    }

    /**
     * Save a programUserCollectionProgress.
     *
     * @param programUserCollectionProgressDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramUserCollectionProgressDTO save(ProgramUserCollectionProgressDTO programUserCollectionProgressDTO) {
        log.debug("Request to save ProgramUserCollectionProgress : {}", programUserCollectionProgressDTO);
        ProgramUserCollectionProgress programUserCollectionProgress = programUserCollectionProgressMapper.toEntity(programUserCollectionProgressDTO);
        programUserCollectionProgress = programUserCollectionProgressRepository.save(programUserCollectionProgress);
        return programUserCollectionProgressMapper.toDto(programUserCollectionProgress);
    }

    /**
     * Get all the programUserCollectionProgresses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramUserCollectionProgressDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramUserCollectionProgresses");
        return programUserCollectionProgressRepository.findAll(pageable)
            .map(programUserCollectionProgressMapper::toDto);
    }


    /**
     * Get one programUserCollectionProgress by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramUserCollectionProgressDTO> findOne(Long id) {
        log.debug("Request to get ProgramUserCollectionProgress : {}", id);
        return programUserCollectionProgressRepository.findById(id)
            .map(programUserCollectionProgressMapper::toDto);
    }

    /**
     * Delete the programUserCollectionProgress by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramUserCollectionProgress : {}", id);
        programUserCollectionProgressRepository.deleteById(id);
    }
}
