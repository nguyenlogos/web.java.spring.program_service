package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.adp.repository.AdpEmployeeProfileRepository;
import aduro.basic.programservice.adp.repository.AdpEmployeeRepository;
import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.ClientDto;
import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.cc.ClientCenterRest;
import aduro.basic.programservice.cc.EmployeeServiceRest;
import aduro.basic.programservice.cc.dto.CCClientProgramDto;
import aduro.basic.programservice.cc.dto.ESDemoGraphicResponseDto;
import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.config.Settings;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.domain.enumeration.ActivityType;
import aduro.basic.programservice.domain.enumeration.ProgramSNSEvent;
import aduro.basic.programservice.domain.enumeration.ProgramType;
import aduro.basic.programservice.helpers.APITracker;
import aduro.basic.programservice.helpers.DateHelpers;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.ProgramCollectionExtensionRepository;
import aduro.basic.programservice.repository.extensions.ProgramHealthyRangeRepositoryExtension;
import aduro.basic.programservice.service.ClientProgramService;
import aduro.basic.programservice.service.HealthyRangePayloadMapper;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.businessrule.ProgramCategoryPointBusinessRule;
import aduro.basic.programservice.service.businessrule.UserIncentiveBusinessRule;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.*;
import aduro.basic.programservice.sns.PublisherService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import io.vavr.control.Option;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static aduro.basic.programservice.config.Constants.ACTIVITY;
import static aduro.basic.programservice.config.Constants.CLIENT_CENTER;
import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link Program}.
 */
@Service
@Transactional
public class ProgramServiceImpl implements ProgramService {

    private final Logger log = LoggerFactory.getLogger(ProgramServiceImpl.class);
    private final ProgramRepository programRepository;
    private final static int DAYS_OF_YEAR = 365;

    @Autowired
    private ProgramCategoryPointRepository programCategoryPointRepository;

    @Autowired
    private ProgramSubCategoryPointRepository programSubCategoryPointRepository;


    @Autowired
    private ClientProgramRepository clientProgramRepository;

    @Autowired
    private ProgramCategoryPointBusinessRule programCategoryPointBusinessRule;

    private final ProgramMapper programMapper;


    @Autowired
    private  ProgramCategoryPointMapper programCategoryPointMapper;

    @Autowired
    private  ProgramSubCategoryPointMapper programSubCategoryPointMapper;

    @Autowired
    private ProgramActivityMapper programActivityMapper;

    @Autowired
    private ProgramLevelMapper programLevelMapper;

    @Autowired
    private ProgramSubgroupMapper programSubgroupMapper;

    @Autowired
    private ProgramLevelRewardMapper programLevelRewardMapper;

    @Autowired
    private  ClientProgramMapper clientProgramMapper;

    @Autowired
    private ProgramSubgroupRepository programSubgroupRepository;


    @Autowired
    private ProgramLevelRepository programLevelRepository;

    @Autowired
    private ProgramLevelRewardRepository programLevelRewardRepository;


    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private ClientProgramService clientProgramService;

    @Autowired
    private  ProgramPhaseRepository  programPhaseRepository;

    @Autowired
    private  ProgramActivityRepository  programActivityRepository;

    @Autowired
    private  ProgramLevelActivityRepository  programLevelActivityRepository;

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private ProgramHealthyRangeMapper programHealthyRangeMapper;

    @Autowired
    private ProgramHealthyRangeRepositoryExtension programHealthyRangeRepository;

    @Autowired
    private UserIncentiveBusinessRule userIncentiveBusinessRule;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;

    @Autowired
    private ProgramSubCategoryConfigurationRepository programSubCategoryConfigurationRepository;

    @Autowired
    private HealthyRangePayloadMapper healthyRangePayloadMapper;


    @Autowired
    private ProgramSubCategoryConfigurationMapper programSubCategoryConfigurationMapper;

    @Autowired
    private ProgramUserRepository programUserRepository;

    @Autowired
    private ProgramRequirementItemsMapper programRequirementItemsMapper;

    @Autowired
    private ProgramLevelPathMapper programLevelPathMapper;

    @Autowired
    private ProgramLevelActivityMapper programLevelActivityMapper;

    @Autowired
    private ProgramLevelPathRepository programLevelPathRepository;

    @Autowired
    private ProgramRequirementItemsRepository programRequirementItemsRepository;

    @Autowired
    private TermAndConditionRepository termAndConditionRepository;

    @Autowired
    private AdpEmployeeRepository adpEmployeeRepository;

    @Autowired
    private AdpEmployeeProfileRepository adpEmployeeProfileRepository;

    @Autowired
    private EmployeeServiceRest employeeServiceRest;

    private final ClientBrandSettingRepository clientBrandSettingRepository;

    private final ClientCenterRest clientCenterRest;

    private final ProgramIntegrationTrackingRepository programIntegrationTrackingRepository;

    private final APITracker apiTracker;

    @Autowired
    private ProgramTrackingStatusRepository programTrackingStatusRepository;

    @Autowired
    private ProgramCollectionExtensionRepository programCollectionExtensionRepository;

    @Autowired
    private ProgramCohortCollectionRepository programCohortCollectionRepository;

    @Autowired
    ProgramCohortRepository programCohortRepository;

    @Autowired
    ProgramCohortRuleRepository programCohortRuleRepository;

    @Autowired
    private ProgramCollectionContentRepository programCollectionContentRepository;

    @Autowired
    private Settings settings;

    @Autowired
    private PublisherService publisherService;


    public ProgramServiceImpl(ProgramRepository programRepository, ProgramMapper programMapper, ClientBrandSettingRepository clientBrandSettingRepository, ClientCenterRest clientCenterRest, ProgramIntegrationTrackingRepository programIntegrationTrackingRepository, APITracker apiTracker) {
        this.programRepository = programRepository;
        this.programMapper = programMapper;
        this.clientBrandSettingRepository = clientBrandSettingRepository;
        this.clientCenterRest = clientCenterRest;
        this.programIntegrationTrackingRepository = programIntegrationTrackingRepository;
        this.apiTracker = apiTracker;
    }

    /*public ProgramDTO uploadProgramLogo(long id,MultipartFile file) {

        Optional<Program> program = programRepository.findById(id);
        if(program.isPresent()) {
            Program p = program.get();
            String url = amazonS3Client.upload(file);
            p.setLogoUrl(url);
            programRepository.save(p);
            return programMapper.toDto(p);
        }else{
            throw new BadRequestAlertException("Program is not exist", "Program", "programnexist");
        }
    }
*/
   /* @Override
    public ProgramDTO uploadProgramLogo(ProgramDTO programDTO) {
        Optional<Program> program = programRepository.findById(programDTO.getId());
        if(program.isPresent()) {
            Program p = program.get();
            p.setLogoUrl(programDTO.getLogoUrl());
            programRepository.save(p);
            return programMapper.toDto(p);
        }else{
            throw new BadRequestAlertException("Program is not exist", "Program", "ProgramId");
        }
    }*/

    private ProgramDTO handleCreate(ProgramDTO programDTO) {

        if(programDTO.getCreatedBy() == null || programDTO.getCreatedBy().isEmpty()){
            throw new BadRequestAlertException("CreatedBy is missing", "program", "CreatedBy");
        }

        if(programRepository.existsProgramByName(programDTO.getName()) && null == null)
            throw new BadRequestAlertException("Program Name is exist", "program", "ProgramName");


        programDTO.setCreatedDate(Instant.now());
        programDTO.setStatus(ProgramStatus.Draft.toString());
        programDTO.setLastModifiedDate(Instant.now());
        programDTO.setIsTemplate(programDTO.getIsTemplate());
        programDTO.setQaVerify(false);
        programDTO.setProgramQAStartDate(null);
        programDTO.setProgramQAEndDate(null);
        programDTO.setProgramType(programDTO.getProgramType() != null ? programDTO.getProgramType() : ProgramType.PARTICIPANT_PROGRAM);

        // program category points have data. can not save program
        List<ProgramCategoryPointDTO> temporaryCategories = new ArrayList<>();
        if (!CollectionUtils.isEmpty(programDTO.getProgramCategoryPoints())){
            // save temporary
            temporaryCategories = programDTO.getProgramCategoryPoints();
            // set empty to list
            programDTO.setProgramCategoryPoints(new ArrayList<>());
        }

        Program program = programMapper.toEntity(programDTO);

        program.setEconomyPoint(BigDecimal.ZERO);

        program = programRepository.save(program);
        final Long programId = program.getId();

        // set program category point if have
        List<ProgramCategoryPoint> needSaveProgramCategoryPoint = new ArrayList<>();
        List<ProgramSubCategoryPoint> needSaveProgramSubCategoryPoints = new ArrayList<>();
        Map<String, List<ProgramSubCategoryPoint>> programSubcategoryPointMapper = new HashMap<>();

        if (!temporaryCategories.isEmpty()) {

            for (ProgramCategoryPointDTO temporaryCategory : temporaryCategories) {
                temporaryCategory.setProgramId(programId);
                temporaryCategory.setId(null);

                List<ProgramSubCategoryPointDTO> temporarySubCategoryPoints = new ArrayList<>();

                if(!CollectionUtils.isEmpty(temporaryCategory.getProgramSubCategoryPoints())) {
                    temporarySubCategoryPoints = temporaryCategory.getProgramSubCategoryPoints();
                    temporaryCategory.setProgramSubCategoryPoints(new ArrayList<>());
                }

                //get parent category
                ProgramCategoryPoint programCategoryPoint = programCategoryPointMapper.toEntity(temporaryCategory);
                needSaveProgramCategoryPoint.add(programCategoryPoint);

                List<ProgramSubCategoryPoint> programSubCategoryPoints = new ArrayList<>();

                if (!CollectionUtils.isEmpty(temporarySubCategoryPoints)) {
                    for (ProgramSubCategoryPointDTO temporarySubCategoryPoint : temporarySubCategoryPoints) {
                        temporarySubCategoryPoint.setId(null);
                        programSubCategoryPoints.add(programSubCategoryPointMapper.toEntity(temporarySubCategoryPoint));
                    }
                }

                programSubcategoryPointMapper.put(programCategoryPoint.getCategoryCode(), programSubCategoryPoints);

            }

            //save parent category

            List<ProgramCategoryPoint> savedProgramCategoryPoints = programCategoryPointRepository.saveAll(needSaveProgramCategoryPoint);

            for (ProgramCategoryPoint savedProgramCategoryPoint : savedProgramCategoryPoints) {

                List<ProgramSubCategoryPoint> programSubCategoryPoints = programSubcategoryPointMapper.get(savedProgramCategoryPoint.getCategoryCode());

                Set<ProgramSubCategoryPoint> collectOfSubCategoryPoints = programSubCategoryPoints.stream().map(t -> {
                    t.setProgramCategoryPointId(savedProgramCategoryPoint.getId());
                    t.setProgramCategoryPoint(savedProgramCategoryPoint);
                    t.setId(null);
                    return t;
                }).collect(Collectors.toSet());

                savedProgramCategoryPoint.setProgramSubCategoryPoints(collectOfSubCategoryPoints);

                needSaveProgramSubCategoryPoints.addAll(collectOfSubCategoryPoints);
            }

            programSubCategoryPointRepository.saveAll(needSaveProgramSubCategoryPoints);

            program.setProgramCategoryPoints(new HashSet<>(savedProgramCategoryPoints));

            program = programRepository.save(program);
        }

        //Create default level for new program
        ProgramLevel programLevel = new ProgramLevel();
        programLevel.setProgram(program);
        programLevel.setLevelOrder(1);
        programLevel.setEndPoint(0);
        programLevel.setName("Level 1");

        programLevelRepository.save(programLevel);
        program.addLevel(programLevel);
        ProgramDTO dto = programMapper.toDto(program);

        dto.setProgramCategoryPoints(programCategoryPointMapper.toDto(new ArrayList<>(program.getProgramCategoryPoints())));

        dto.setProgramLevels (programLevelMapper.toDto(new ArrayList<>(program.getProgramLevels())));

        program.setEconomyPoint(programCategoryPointBusinessRule.calculateEconomyPoint(program.getId()));
        programRepository.save(program);

        // process healthy ranges
        List<ProgramHealthyRangePayLoad> programHealthyRangeDtoS = programDTO.getProgramHealthyRanges();

        Map<String, List<ProgramHealthyRangePayLoad>> programHealthyRangesMapper = programHealthyRangeDtoS.stream()
            .collect(Collectors.groupingBy(payLoad -> payLoad.getProgramSubCategoryConfigurationDTO().getSubCategoryCode()));

        // find configuration for ranges;
        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = new ArrayList<>();

        for (Map.Entry<String, List<ProgramHealthyRangePayLoad>> mapObject : programHealthyRangesMapper.entrySet()) {
            Optional<ProgramHealthyRangePayLoad> programHealthyRangePayLoadOptional = mapObject.getValue().stream().findFirst();
            if (programHealthyRangePayLoadOptional.isPresent()) {

                ProgramSubCategoryConfiguration programSubCategoryConfiguration = programSubCategoryConfigurationMapper
                    .toEntity(programHealthyRangePayLoadOptional.get().getProgramSubCategoryConfigurationDTO());

                programSubCategoryConfiguration.setProgramId(programId);
                programSubCategoryConfiguration.setId(null);
                programSubCategoryConfigurationList.add(programSubCategoryConfiguration);
            }
        }

        //save all list configuration
        programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.saveAll(programSubCategoryConfigurationList);

        List<ProgramHealthyRange> programHealthyRangeList = new ArrayList<>();

        for (Map.Entry<String, List<ProgramHealthyRangePayLoad>> mapObject : programHealthyRangesMapper.entrySet()) {
            List<ProgramHealthyRangePayLoad> ranges = mapObject.getValue();

            Optional<ProgramSubCategoryConfiguration> subCategoryConfigurationOptional = programSubCategoryConfigurationList.stream()
                .filter(c -> c.getSubCategoryCode().equalsIgnoreCase(mapObject.getKey())).findFirst();

            if (subCategoryConfigurationOptional.isPresent()) {
                ProgramSubCategoryConfiguration subCategoryConfiguration = subCategoryConfigurationOptional.get();
                Set<ProgramHealthyRange> programHealthyRangeSet = ranges.stream().map(r -> healthyRangePayloadMapper.toProgramHealthyRangeDTO(r)).map(d -> {
                    d.setProgramId(subCategoryConfiguration.getProgramId());
                    d.setSubCategoryId(subCategoryConfiguration.getId());
                    d.setSubCategoryCode(subCategoryConfiguration.getSubCategoryCode());
                    d.setId(null);
                    return programHealthyRangeMapper.toEntity(d);
                }).collect(Collectors.toSet());

                programHealthyRangeList.addAll(programHealthyRangeSet);
            }
        }

        //save healthy ranges
        programHealthyRangeList = programHealthyRangeRepository.saveAll(programHealthyRangeList);

        List<ProgramHealthyRangePayLoad> programHealthyRangePayLoads = healthyRangePayloadMapper.toHealthyPayloadList(programHealthyRangeMapper.toDto(programHealthyRangeList));

        dto.setProgramHealthyRanges(programHealthyRangePayLoads);

        //handle save client brand setting
        Set<ClientProgram> clientPrograms = program.getClientPrograms();
        if (!clientPrograms.isEmpty()) {
            Optional<ClientProgram> clientProgramOptional = clientPrograms.stream().findFirst();

            if (clientProgramOptional.isPresent()) {
                ClientProgram clientProgram = clientProgramOptional.get();
                Optional<ClientBrandSetting> clientBrandSettingOptional = clientBrandSettingRepository.findClientBrandSettingByClientId(clientProgram.getClientId());
                if (clientBrandSettingOptional.isPresent()) {
                    ClientBrandSetting clientBrandSetting = clientBrandSettingOptional.get();
                    clientBrandSetting.setPrimaryColorValue(programDTO.getColorValue());
                    clientBrandSettingRepository.save(clientBrandSetting);
                }
            }
        }

        // process Level
        List<ProgramLevelDTO> programLevelList = programDTO.getProgramLevels();
        List<ProgramLevel> programLevels = new ArrayList<>();
        Map<Integer, List<ProgramLevelActivity>> programLevelActivityMaps = new HashMap<>();
        Map<Integer, List<ProgramLevelPath>> programLevelPathMaps = new HashMap<>();
        Map<Integer, List<ProgramRequirementItems>> programRequiredItemshMaps = new HashMap<>();
        Map<Integer, List<ProgramLevelReward>> programLevelRewardMaps = new HashMap<>();

        if (!programLevelList.isEmpty()) {
            for (ProgramLevelDTO programLevelDto : programLevelList) {
                programLevelDto.setProgramId(programId);
                programLevelDto.setId(null);
                programLevels.add(
                    programLevelMapper.toEntity(programLevelDto)
                );
                programLevelActivityMaps.put(programLevelDto.getLevelOrder(), programLevelActivityMapper.toEntity(programLevelDto.getProgramLevelActivities()));
                programLevelPathMaps.put(programLevelDto.getLevelOrder(), programLevelPathMapper.toEntity(programLevelDto.getProgramLevelPaths()));
                programRequiredItemshMaps.put(programLevelDto.getLevelOrder(), programRequirementItemsMapper.toEntity(programLevelDto.getProgramRequirementItems()));
                programLevelRewardMaps.put(programLevelDto.getLevelOrder(), programLevelRewardMapper.toEntity(programLevelDto.getProgramLevelRewards()));
            }
        }

        //delete default;
        List<ProgramLevel> programLevelsDefaults = programLevelRepository.findProgramLevelsByProgram_Id(programId);

        for (ProgramLevel programLevelsDefault : programLevelsDefaults) {
            programLevelRepository.delete(programLevelsDefault);
        }

        //save program levels
        programLevels = programLevelRepository.saveAll(programLevels);

        List<ProgramLevelActivity> totalLevelActivities = new ArrayList<>();
        List<ProgramLevelPath> totalLevelPaths = new ArrayList<>();
        List<ProgramRequirementItems> totalRequiredItems = new ArrayList<>();
        List<ProgramLevelReward> totalRewards = new ArrayList<>();

        for (ProgramLevel level : programLevels) {
            List<ProgramLevelActivity> programLevelActivities = programLevelActivityMaps.get(level.getLevelOrder());

            programLevelActivities
                .forEach(a -> {
                    a.setProgramLevel(level);
                    a.setProgramLevelId(level.getId());
                    a.setId(null);
                });
            totalLevelActivities.addAll(programLevelActivities);

            List<ProgramLevelPath> programLevelPaths = programLevelPathMaps.get(level.getLevelOrder());

            programLevelPaths.forEach( p -> {
                p.setProgramLevel(level);
                p.setId(null);
            });
            totalLevelPaths.addAll(programLevelPaths);

            List<ProgramRequirementItems> programRequirementItems = programRequiredItemshMaps.get(level.getLevelOrder());

            programRequirementItems.forEach( p -> {
                p.setProgramLevel(level);
                p.setProgramLevelId(level.getId());
                p.setId(null);
            });
            totalRequiredItems.addAll(programRequirementItems);

            List<ProgramLevelReward> programLevelRewards = programLevelRewardMaps.get(level.getLevelOrder());
            programLevelRewards.forEach(p -> {
                p.setProgramLevel(level);
                p.setProgramLevelId(level.getId());
                p.setId(null);
            });

            totalRewards.addAll(programLevelRewards);
        }

        totalLevelActivities = programLevelActivityRepository.saveAll(totalLevelActivities);
        totalLevelPaths = programLevelPathRepository.saveAll(totalLevelPaths);
        totalRequiredItems = programRequirementItemsRepository.saveAll(totalRequiredItems);
        totalRewards = programLevelRewardRepository.saveAll(totalRewards);

        for (ProgramLevel level : programLevels) {
            level.setProgramRequirementItems(totalRequiredItems.stream().filter( f -> f.getProgramLevelId().equals(Long.valueOf(level.getLevelOrder()))).collect(Collectors.toSet()));
            level.setProgramLevelPaths(totalLevelPaths.stream().filter( f -> f.getProgramLevel().getLevelOrder().equals(level.getLevelOrder())).collect(Collectors.toSet()));
            level.setActivities(totalLevelActivities.stream().filter( f -> f.getProgramLevel().getLevelOrder().equals(level.getLevelOrder())).collect(Collectors.toSet()));
            level.setProgramLevelRewards(totalRewards.stream().filter(f -> f.getProgramLevel().getLevelOrder().equals(level.getLevelOrder())).collect(Collectors.toSet()));
        }

        dto.setProgramLevels(programLevelMapper.toDto(programLevels));

        //create tracking status
        trackingProgramChangesStatus(program.getStatus(), programId);

        dto.setActiveDate(getActiveDate(programId));
        return dto;

    }

    private void cloneProgramCollections(List<ProgramCollection> programCollectionList, Program program, List<CloneActivityDTO> cloneActivityList) {

        if (CollectionUtils.isEmpty(programCollectionList)){
            return;
        }

        int batchSize = 10;
        List<ProgramCollectionContent> itemContents = new ArrayList<>();

        for (ProgramCollection dto : programCollectionList) {

            ProgramCollection programCollectionDto = new ProgramCollection();
            programCollectionDto.setId(null);
            programCollectionDto.setProgram(program);
            programCollectionDto.setDisplayName(dto.getDisplayName());
            programCollectionDto.setCreatedDate(Instant.now());
            programCollectionDto.setModifiedDate(Instant.now());
            programCollectionDto.setLongDescription(dto.getLongDescription());

            programCollectionExtensionRepository.save(programCollectionDto);

             List<ProgramCollectionContent> contents = new ArrayList<>();
            for (ProgramCollectionContent cc : dto.getProgramCollectionContents()) {

                Function<String, String> getNewCustomActivityId = (old) -> {
                    Optional<CloneActivityDTO> existedCloneActivityOptional = cloneActivityList.stream()
                        .filter(a -> a.getOldCustomActivityId().equalsIgnoreCase(old))
                        .findFirst();
                    return existedCloneActivityOptional.map(CloneActivityDTO::getNewCustomActivityId).orElse(null);
                };

                String newCustomActivity = null;

                if (cc.getContentType().equalsIgnoreCase(Constants.ACTIVITY)) {
                    newCustomActivity = getNewCustomActivityId.apply(cc.getItemId());

                    if (Objects.isNull(newCustomActivity)) {
                        continue;
                    }
                }

                ProgramCollectionContent c = new ProgramCollectionContent();
                c.setId(null);
                c.setProgramCollection(programCollectionDto);
                c.setCreatedDate(Instant.now());
                c.setModifiedDate(Instant.now());
                c.setContentType(cc.getContentType());
                c.setItemType(cc.getItemType());
                c.setHasRequired(cc.isHasRequired());
                c.setItemIcon(cc.getItemIcon());
                c.setItemId(cc.getContentType().equalsIgnoreCase(ACTIVITY) ? newCustomActivity : cc.getItemId());
                c.setItemName(cc.getItemName());
                contents.add(c);
            }

            itemContents.addAll(contents);
        }

        if (itemContents.size() > batchSize) {
            List<ProgramCollectionContent> savingObjects = new ArrayList<>();
            for (int i = 0; i < itemContents.size(); i++) {

                savingObjects.add(itemContents.get(i));

                if (i % batchSize == 0 && i > 0) {
                    programCollectionContentRepository.saveAll(savingObjects);
                    savingObjects.clear();
                }
            }
            if (!CollectionUtils.isEmpty(savingObjects)) {
                programCollectionContentRepository.saveAll(savingObjects);
            }
        } else {
            programCollectionContentRepository.saveAll(itemContents);
        }
    }


    private void cloneProgramCohorts(Set<ProgramCohort> programCohorts, Program program) {

        List<ProgramCollection> newCollections = programCollectionExtensionRepository.findAllByProgramId(program.getId());

        for (ProgramCohort cohort : programCohorts) {
            ProgramCohort programCohort = new ProgramCohort();
            programCohort.setId(null);
            programCohort.setProgram(program);
            programCohort.setModifiedDate(Instant.now());
            programCohort.setCreatedDate(Instant.now());
            programCohort.setCohortName("Clone of " + programCohort.getCohortName());
            programCohort.setCohortName(cohort.getCohortName());
            programCohort.setCreatedDate(Instant.now());
            programCohort.setModifiedDate(Instant.now());
            programCohort.setIsDefault(cohort.isIsDefault());
            programCohort.setCohortDescription(cohort.getCohortDescription());
            programCohortRepository.save(programCohort);

            List<ProgramCohortRule> programCohortRules = new ArrayList<>();

            Set<ProgramCohortRule> cohortProgramCohortRules = cohort.getProgramCohortRules();

            for (ProgramCohortRule cohortRule : cohortProgramCohortRules) {
                ProgramCohortRule programCohortRule = new ProgramCohortRule();
                programCohortRule.setId(null);
                programCohortRule.setModifiedDate(Instant.now());
                programCohortRule.setCreatedDate(Instant.now());
                programCohortRule.setDataInputType(cohortRule.getDataInputType());
                programCohortRule.setRules(cohortRule.getRules());
                programCohortRule.setProgramCohort(programCohort);
                programCohortRules.add(programCohortRule);
            }

            programCohortRuleRepository.saveAll(programCohortRules);

            Set<ProgramCohortCollection> newCohortCollections = new HashSet<>();

            for (ProgramCohortCollection programCohortCollection : cohort.getProgramCohortCollections()) {

                Optional<ProgramCollection> programCollectionOptional = newCollections.stream()
                    .filter(c -> c.getDisplayName().equalsIgnoreCase(programCohortCollection.getProgramCollection().getDisplayName()))
                    .findFirst();

                if (!programCollectionOptional.isPresent()) {
                    continue;
                }

                ProgramCohortCollection cohortCollection = new ProgramCohortCollection()
                    .programCollection(programCollectionOptional.get())
                    .programCohort(programCohort)
                    .createdDate(Instant.now())
                    .modifiedDate(Instant.now())
                    .requiredCompletion(programCohortCollection.getRequiredCompletion())
                    .requiredLevel(programCohortCollection.getRequiredLevel());

                newCohortCollections.add(cohortCollection);

            }

            programCohortCollectionRepository.saveAll(newCohortCollections);
        }
    }

    private List<ProgramCategoryPointDTO> cloneProgramCategoryPoint(Set<ProgramCategoryPoint> programCategoryPoints,Program program){

        // set program category point if have
        List<ProgramCategoryPoint> programCategoryPointList = new ArrayList<>(programCategoryPoints);

        List<ProgramCategoryPointDTO> res = new ArrayList<>();
        for (ProgramCategoryPoint category : programCategoryPointList) {

            ProgramCategoryPoint newProgramCategoryPoint = new ProgramCategoryPoint()
                .percentPoint(category.getPercentPoint())
                .valuePoint(category.getValuePoint())
                .locked(category.getLocked())
                .categoryCode(category.getCategoryCode())
                .categoryName(category.getCategoryName())
                .program(program)
                .programId(program.getId());

            ProgramCategoryPoint programCategoryPoint = programCategoryPointRepository.save(newProgramCategoryPoint);


            Set<ProgramSubCategoryPoint> collectOfSubCategoryPoints = category.getProgramSubCategoryPoints().stream().map(t -> {
                ProgramSubCategoryPoint programSubCategoryPoint = new ProgramSubCategoryPoint()
                    .programCategoryPoint(newProgramCategoryPoint)
                    .percentPoint(t.getPercentPoint())
                    .valuePoint(t.getValuePoint())
                    .code(t.getCode())
                    .completionsCap(t.getCompletionsCap());
                programSubCategoryPoint.setProgramCategoryPointId(newProgramCategoryPoint.getId());
                programSubCategoryPoint.setName(t.getName());
                programSubCategoryPoint.setLocked(t.getLocked());
                return programSubCategoryPoint;
            }).collect(Collectors.toSet());

            programSubCategoryPointRepository.saveAll(collectOfSubCategoryPoints);

            programCategoryPoint.setProgramSubCategoryPoints(collectOfSubCategoryPoints);

            res.add(programCategoryPointMapper.toDto(programCategoryPoint));
        }
        return res;
    }

    private List<ProgramLevelDTO> cloneProgramLevel(Set<ProgramLevel> programLevels, Program program, List<CloneActivityDTO> cloneActivityList) {

        //save program levels
        Set<ProgramLevel> programLevelSet = programLevels.stream().map(programLevel -> {
            ProgramLevel newLevel = new ProgramLevel();
            newLevel.setProgramId(program.getId());
            newLevel.setProgram(program);
            newLevel.setId(null);
            newLevel.setEndPoint(programLevel.getEndPoint());
            newLevel.setLevelOrder(programLevel.getLevelOrder());
            newLevel.setName(programLevel.getName());
            newLevel.setDescription(programLevel.getDescription());
            newLevel.setIconPath(programLevel.getIconPath());
            newLevel.setStartPoint(programLevel.getStartPoint());
            return newLevel;
        }).collect(Collectors.toSet());

        List<ProgramLevel> programLevelList = programLevelRepository.saveAll(programLevelSet);

        List<ProgramLevelDTO> res = new ArrayList<>();


        Function<String, String> getNewCustomActivityId = (old) -> {
            Optional<CloneActivityDTO> existedCloneActivityOptional = cloneActivityList.stream()
                .filter(a -> a.getOldCustomActivityId().equalsIgnoreCase(old))
                .findFirst();
            return existedCloneActivityOptional.map(CloneActivityDTO::getNewCustomActivityId).orElse(null);
        };

        for (ProgramLevel level : programLevelList) {

            Optional<ProgramLevel> programLevelOptional = programLevels.stream()
                .filter(p -> p.getLevelOrder().equals(level.getLevelOrder()))
                .findFirst();

            if (!programLevelOptional.isPresent()) {
                continue;
            }

            ProgramLevelDTO programLevelDTO = programLevelMapper.toDto(level);

            List<ProgramLevelActivity> programLevelActivities = new ArrayList<>(programLevelOptional.get().getProgramLevelActivities());

            Set<ProgramLevelActivity> newCollectionLevelActivity = new HashSet<>();

            for (ProgramLevelActivity a : programLevelActivities) {
                String newCustomActivity = getNewCustomActivityId.apply(a.getActivityId());

                if (Objects.isNull(newCustomActivity)) {
                    continue;
                }

                ProgramLevelActivity programLevelActivity = new ProgramLevelActivity()
                    .subgroupId(a.getSubgroupId())
                    .activityId(newCustomActivity)
                    .programLevel(level)
                    .programLevelId(level.getId())
                    .subgroupName(a.getSubgroupName())
                    .activityCode(a.getActivityCode());
                newCollectionLevelActivity.add(programLevelActivity);
            }

            List<ProgramLevelActivity> programLevelActivities1 = programLevelActivityRepository.saveAll(newCollectionLevelActivity);

            programLevelDTO.setProgramLevelActivities(programLevelActivityMapper.toDto(programLevelActivities1));

            List<ProgramLevelPath> programLevelPaths = new ArrayList<>(programLevelOptional.get().getProgramLevelPaths());

            Set<ProgramLevelPath> newCollectionPaths = programLevelPaths.stream().map(p -> new ProgramLevelPath()
                .name(p.getName())
                .subgroupId(p.getSubgroupId())
                .subgroupName(p.getSubgroupName())
                .programLevel(level)
                .pathCategory(p.getPathCategory())
                .pathId(p.getPathId())
                .pathType(p.getPathType())

            ).collect(Collectors.toSet());

            List<ProgramLevelPath> programLevelPaths1 = programLevelPathRepository.saveAll(newCollectionPaths);

            programLevelDTO.setProgramLevelPaths(programLevelPathMapper.toDto(programLevelPaths1));

            List<ProgramRequirementItems> programRequirementItems = new ArrayList<>(programLevelOptional.get().getProgramRequirementItems());

            Set<ProgramRequirementItems> collectRequiredItems = new HashSet<>();

            for (ProgramRequirementItems p : programRequirementItems) {

                String newCustomActivity = null;

                if (p.getTypeOfItem().equalsIgnoreCase(Constants.ACTIVITY)) {
                    newCustomActivity = getNewCustomActivityId.apply(p.getItemId());

                    if (Objects.isNull(newCustomActivity)) {
                        continue;
                    }
                }

                ProgramRequirementItems programRequirementItem = new ProgramRequirementItems()
                    .subgroupId(p.getSubgroupId())
                    .subgroupName(p.getSubgroupName())
                    .itemCode(p.getItemCode())
                    .itemId(p.getTypeOfItem().equalsIgnoreCase(Constants.ACTIVITY) ? newCustomActivity : p.getItemId())
                    .programLevel(level)
                    .nameOfItem(p.getNameOfItem())
                    .createdAt(Instant.now())
                    .programLevelId(level.getId())
                    .typeOfItem(p.getTypeOfItem());

                collectRequiredItems.add(programRequirementItem);
            }

            List<ProgramRequirementItems> programRequirementItems1 = programRequirementItemsRepository.saveAll(collectRequiredItems);
            programLevelDTO.setProgramRequirementItems(programRequirementItemsMapper.toDto(programRequirementItems1));

            List<ProgramLevelReward> programLevelRewards = new ArrayList<>(programLevelOptional.get().getProgramLevelRewards());

            Set<ProgramLevelReward> collectRewards = programLevelRewards.stream().map(p -> new ProgramLevelReward()
                .programLevel(level)
                .subgroupId(p.getSubgroupId())
                .subgroupName(p.getSubgroupName())
                .rewardType(p.getRewardType())
                .rewardAmount(p.getRewardAmount())
                .description(p.getDescription())
                .campaignId(p.getCampaignId())
                .programLevelId(level.getId())
                .code(p.getCode())
                .quantity(p.getQuantity())
            ).collect(Collectors.toSet());

            List<ProgramLevelReward> rewards = programLevelRewardRepository.saveAll(collectRewards);

            programLevelDTO.setProgramLevelRewards(programLevelRewardMapper.toDto(rewards));

            res.add(programLevelDTO);
        }

        return res;
    }

    private void cloneProgramHealthyRanges(Set<ProgramHealthyRange> programHealthyRanges, List<ProgramSubCategoryConfiguration> programSubCategoryConfigurations, Program program){
        Set<ProgramSubCategoryConfiguration> collect = new HashSet<>();

        for (ProgramSubCategoryConfiguration sb : programSubCategoryConfigurations) {
            ProgramSubCategoryConfiguration configuration = new ProgramSubCategoryConfiguration();
            configuration.setProgramId(program.getId());
            configuration.setSubCategoryCode(sb.getSubCategoryCode());
            configuration.setIsBloodPressureIndividualTest(sb.isIsBloodPressureIndividualTest());
            configuration.setIsBloodPressureSingleTest(sb.isIsBloodPressureSingleTest());
            configuration.setIsBmiAwardedFasting(sb.isIsBmiAwardedFasting());
            configuration.setIsBmiAwardedInRange(sb.isIsBmiAwardedInRange());
            configuration.setIsBmiAwardedNonFasting(sb.isIsBmiAwardedNonFasting());
            configuration.setIsGlucoseAwardedInRange(sb.isIsGlucoseAwardedInRange());
            collect.add(configuration);
        }

        programSubCategoryConfigurationRepository.saveAll(collect);

        List<ProgramHealthyRange> programHealthyRangeList = new ArrayList<>(programHealthyRanges);

        List<ProgramHealthyRange> totalHealthyRangeList = new ArrayList<>();
        for (ProgramHealthyRange healthyRange : programHealthyRangeList) {

            Optional<ProgramSubCategoryConfiguration> programSubCategoryConfigurationOptional = collect.stream()
                .filter(p -> p.getSubCategoryCode().equalsIgnoreCase(healthyRange.getSubCategoryCode())).findFirst();

            if (!programSubCategoryConfigurationOptional.isPresent()) {
                continue;
            }

            ProgramSubCategoryConfiguration subCategoryConfiguration = programSubCategoryConfigurationOptional.get();
            ProgramHealthyRange programHealthyRange = new ProgramHealthyRange();
            programHealthyRange.setId(null);
            programHealthyRange.setMaleMin( healthyRange.getMaleMin() );
            programHealthyRange.setMaleMax( healthyRange.getMaleMax() );
            programHealthyRange.setFemaleMin( healthyRange.getFemaleMin() );
            programHealthyRange.setFemaleMax( healthyRange.getFemaleMax() );
            programHealthyRange.setUnidentifiedMin( healthyRange.getUnidentifiedMin() );
            programHealthyRange.setUnidentifiedMax( healthyRange.getUnidentifiedMax() );
            programHealthyRange.setIsApplyPoint( healthyRange.isIsApplyPoint() );
            programHealthyRange.setBiometricCode( healthyRange.getBiometricCode() );
            programHealthyRange.setSubCategoryCode( healthyRange.getSubCategoryCode() );
            programHealthyRange.setSubCategoryId( healthyRange.getSubCategoryId() );
            programHealthyRange.setSubCategoryId(subCategoryConfiguration.getId());
            programHealthyRange.setProgram(program);
            totalHealthyRangeList.add(programHealthyRange);
        }
        programHealthyRangeRepository.saveAll(totalHealthyRangeList);

    }

    @Transactional
    void trackingProgramChangesStatus(String programStatus, Long programId) {

        ProgramTrackingStatus trackingStatus = new ProgramTrackingStatus()
            .programStatus(programStatus)
            .createdDate(Instant.now())
            .modifiedDate(Instant.now())
            .programId(programId);
        programTrackingStatusRepository.save(trackingStatus);
    }

    private  Instant getActiveDate(Long programId) {
        List<ProgramTrackingStatus> trackingActiveStatus = programTrackingStatusRepository.findByProgramIdAndProgramStatus(programId, ProgramStatus.Active.name());
        return  trackingActiveStatus.isEmpty() ? null : trackingActiveStatus.get(trackingActiveStatus.size() - 1).getCreatedDate();
    }

    private List<ProgramHealthyRangePayLoad> getLatestHealthyRangePayload(long programId) {
        List<ProgramHealthyRangeDTO> programHealthyRangeDTOList = programHealthyRangeMapper.toDto(new ArrayList<>(programHealthyRangeRepository.findByProgramId(programId)));
        if (programHealthyRangeDTOList.isEmpty()) {
            return new ArrayList<>();
        }
        return healthyRangePayloadMapper.toHealthyPayloadList(programHealthyRangeDTOList);
    }

    private ProgramDTO handleUpdate(ProgramDTO programDTO) {

        if(programDTO.getLastModifiedBy() == null || programDTO.getLastModifiedBy().isEmpty()){
            throw new BadRequestAlertException("LastModifiedBy is missing", "program", "LastModifiedBy");
        }

        Optional<Program> optionalProgram = programRepository.findById(programDTO.getId());

        if(!optionalProgram.isPresent()) throw new BadRequestAlertException("program  is not exist", "program", "client_program");
        Program currentProgram = optionalProgram.get();

        if(!currentProgram.getIsTemplate() &&  currentProgram.getStatus().equals(ProgramStatus.Completed.toString())) throw new BadRequestAlertException("program  can not edit due to completed", "program", "client_program");

        Boolean isChangePoint = false;

        List<ClientProgram> relatedClientPrograms = validateDateRange(programDTO, currentProgram);

        relatedClientPrograms = validateProgramQADateRange(programDTO, currentProgram, relatedClientPrograms);

        validateUniqueForProgramQAVerify(programDTO, currentProgram, relatedClientPrograms);

        if(programDTO.getUserPoint() == null) programDTO.setUserPoint(BigDecimal.valueOf(0));
        if(currentProgram.getUserPoint() == null) currentProgram.setUserPoint(BigDecimal.valueOf(0));

        if(programDTO.getUserPoint().compareTo(currentProgram.getUserPoint())!= 0)
            isChangePoint = true;

        currentProgram.setName(programDTO.getName());
        currentProgram.setLastModifiedBy(programDTO.getLastModifiedBy());
        currentProgram.setLastModifiedDate(Instant.now());

        currentProgram.setDescription(programDTO.getDescription());
        currentProgram.setIsEligible(programDTO.isIsEligible());
        currentProgram.setIsFunctionally(programDTO.isIsFunctionally());

        currentProgram.setIsRegisteredForPlatform(programDTO.isIsRegisteredForPlatform());
        currentProgram.setIsRetriggerEmail(programDTO.isIsRetriggerEmail());
        currentProgram.setUserPoint(programDTO.getUserPoint());

        currentProgram.setIsScheduledScreening(programDTO.isIsScheduledScreening());
        currentProgram.setStartDate(programDTO.getStartDate());
        currentProgram.setResetDate(programDTO.getResetDate());

        currentProgram.setIsSentRegistrationEmail(programDTO.isIsSentRegistrationEmail());
        currentProgram.setLastSent(programDTO.getLastSent());

        currentProgram.setIsScreen(programDTO.isIsScreen());
        currentProgram.setIsUsePoint(programDTO.isIsUsePoint());
        currentProgram.setIsHp(programDTO.isIsHp());
        currentProgram.setIsWellMatric(programDTO.isIsWellMatric());
        currentProgram.setIsCoaching(programDTO.isIsCoaching());
        currentProgram.setLogoUrl(programDTO.getLogoUrl());
        currentProgram.setIsUseLevel(programDTO.isIsUseLevel());

        currentProgram.setIsPreview(programDTO.isIsPreview());
        currentProgram.setPreviewDate(programDTO.getPreviewDate());
        currentProgram.setStartTimeZone(programDTO.getStartTimeZone());
        currentProgram.setEndTimeZone(programDTO.getEndTimeZone());
        currentProgram.setPreviewTimeZone(programDTO.getPreviewTimeZone());
        currentProgram.setLevelStructure(programDTO.getLevelStructure());
        currentProgram.setProgramLength(programDTO.getProgramLength());

        currentProgram.setIsHPSF(programDTO.isIsHPSF());
        currentProgram.setWellmetricSchedulerV1Url(programDTO.getWellmetricSchedulerV1Url());
        currentProgram.setIsHTK(programDTO.isIsHTK());
        currentProgram.setLabcorpAccountNumber(programDTO.getLabcorpAccountNumber());
        currentProgram.setLabcorpFileUrl(programDTO.getLabcorpFileUrl());
        currentProgram.setIsLabcorp(programDTO.isIsLabcorp());

        currentProgram.setColorValue(programDTO.getColorValue());
        currentProgram.setBiometricDeadlineDate(programDTO.getBiometricDeadlineDate());
        currentProgram.setBiometricLookbackDate(programDTO.getBiometricLookbackDate());

        currentProgram.setLandingBackgroundImageUrl(programDTO.getLandingBackgroundImageUrl());

        currentProgram.setIsTemplate(programDTO.getIsTemplate());

        //full-outcomes-program

        if (programDTO.getProgramType() != null && !currentProgram.getProgramType().equals(programDTO.getProgramType())) {
            programHasActivatedBefore(programDTO.getId());
        }

        // update program by type if status = active
        if (programDTO.getStatus().equalsIgnoreCase(ProgramStatus.Active.name()) && programDTO.getProgramType() != null) {
            handleUpdateProgramByType(programDTO.getProgramType(), currentProgram.getId());
        }

        //create tracking status if have
        if (!currentProgram.getStatus().equalsIgnoreCase(programDTO.getStatus())) {
            trackingProgramChangesStatus(programDTO.getStatus(), programDTO.getId());


        }

        currentProgram.setStatus(programDTO.getStatus());

        if (programDTO.getProgramType() != null) {
            currentProgram.setProgramType(programDTO.getProgramType());
        }

        currentProgram.setIsTobaccoSurchageManagement(programDTO.getTobaccoSurchargeManagement());

        currentProgram.setTobaccoAttestationDeadline(programDTO.getTobaccoAttestationDeadline());

        currentProgram.setTobaccoProgramDeadline(programDTO.getTobaccoProgramDeadline());

        currentProgram.setProgramRegisterMessage(programDTO.getProgramRegisterMessage());

        currentProgram.setIsAwardedForTobaccoAttestation(programDTO.isAwardedForTobaccoAttestation());

        currentProgram.setIsAwardedForTobaccoRas(programDTO.isAwardedForTobaccoRas());

        currentProgram.setAppealsFormFileUrl(programDTO.getAppealsFormFileUrl());
        currentProgram.setAppealsFormText(programDTO.getAppealsFormText());
        currentProgram.setIsAppealsForm(programDTO.getIsAppealsForm());

        currentProgram.setProgramQAStartDate(programDTO.getProgramQAStartDate());
        currentProgram.setProgramQAEndDate(programDTO.getProgramQAEndDate());

        currentProgram.setAsProgramQaVerify( programDTO.getStatus(), programDTO.getProgramQAStartDate(), programDTO.getProgramQAEndDate() );

        currentProgram.setEconomyPoint(programCategoryPointBusinessRule.calculateEconomyPoint(programDTO.getId()));
        currentProgram.setExtractable(programDTO.isExtractable());
        currentProgram.setHpsfFormType(programDTO.getHpsfFormType());

        currentProgram = programRepository.save(currentProgram);


        //handle save client brand setting
        Set<ClientProgram> clientPrograms = currentProgram.getClientPrograms();
        if (!clientPrograms.isEmpty()) {
            clientPrograms.stream().findFirst()
                .ifPresent(clientProgram -> {
                    Optional<ClientBrandSetting> clientBrandSetting = clientBrandSettingRepository.findClientBrandSettingByClientId(clientProgram.getClientId());
                    clientBrandSetting.ifPresent(c -> {
                        c.setPrimaryColorValue(programDTO.getColorValue());
                        clientBrandSettingRepository.save(c);
                    });
                });
        }



        // update healthy ranges

        if (!programDTO.getProgramHealthyRanges().isEmpty()) {
            //save healthy range
            programDTO.getProgramHealthyRanges().forEach(programHealthyRangeDTO -> {
                Optional<ProgramHealthyRange> healthyRangObj = programHealthyRangeRepository.findByProgramIdAndAndBiometricCode(programHealthyRangeDTO.getProgramId(), programHealthyRangeDTO.getBiometricCode());
                if (healthyRangObj.isPresent() && (programHealthyRangeDTO.getId() == null || programHealthyRangeDTO.getId() == 0)) {
                    throw new BadRequestAlertException("ProgramId And Biometric Cod have existed. It can not be created", programHealthyRangeDTO.getProgramId().toString(), programHealthyRangeDTO.getBiometricCode());
                }

                if (!programHealthyRangeDTO.getProgramId().equals(programDTO.getId())) {
                    throw new BadRequestAlertException("ProgramId of Healthy Range can not be updated.", "Program Healthy Range", programHealthyRangeDTO.getId().toString());
                }
            });


            //save sub-category healthyRange and configuration
            List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = new ArrayList<>();

            List<ProgramHealthyRange> programHealthyRangeList = new ArrayList<>();

            Map<String, List<ProgramHealthyRangePayLoad>> collectionsOfHealthyRangePayloads = programDTO.getProgramHealthyRanges().stream()
                .collect(Collectors.groupingBy(payLoad -> payLoad.getProgramSubCategoryConfigurationDTO().getSubCategoryCode()));

            collectionsOfHealthyRangePayloads.forEach((configuration, programHealthyRangePayLoads) -> {

                // get configuration save first;
                if (CollectionUtils.isEmpty(programHealthyRangePayLoads)) { return; }

                //get first configuration
                ProgramHealthyRangePayLoad programHealthyRangePayLoad = programHealthyRangePayLoads.get(0);

                ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO = programHealthyRangePayLoad.getProgramSubCategoryConfigurationDTO();

                if (Objects.isNull(programSubCategoryConfigurationDTO)) { return; }

                ProgramSubCategoryConfiguration programSubCategoryConfiguration = programSubCategoryConfigurationMapper
                                .toEntity(programHealthyRangePayLoad.getProgramSubCategoryConfigurationDTO());

                programSubCategoryConfigurationList.add(programSubCategoryConfiguration);

                List<ProgramHealthyRange> healthyRangeList = programHealthyRangePayLoads.stream()
                    .map(p -> programHealthyRangeMapper.toEntity(p)).collect(Collectors.toList());

                programHealthyRangeList.addAll(healthyRangeList);

            });

            programSubCategoryConfigurationRepository.saveAll(programSubCategoryConfigurationList);

            programHealthyRangeRepository.saveAll(programHealthyRangeList);

        }



        //Update Points if change point
        if(isChangePoint){
            List<ProgramSubCategoryPoint> programSubCategoryPointList = new ArrayList<>();
            List<ProgramCategoryPoint> programCategoryPointList =  programCategoryPointRepository.findAllByProgram_Id(currentProgram.getId());
            for (ProgramCategoryPoint programCategoryPoint: programCategoryPointList) {
                programCategoryPointBusinessRule.CaculatePoint(programCategoryPoint, currentProgram.getUserPoint());
                for (ProgramSubCategoryPoint subPoint:programCategoryPoint.getProgramSubCategoryPoints()) {
                    programCategoryPointBusinessRule.CaculateSubPoint(subPoint,programCategoryPoint);
                    programSubCategoryPointList.add(subPoint);
                }
            }
            if (!CollectionUtils.isEmpty(programCategoryPointList)) {
                programSubCategoryPointRepository.saveAll(programSubCategoryPointList);
            }
            programCategoryPointRepository.saveAll(programCategoryPointList);
        }

        ProgramDTO dto = programMapper.toDto(currentProgram);
        dto.setProgramCategoryPoints(programCategoryPointMapper.toDto(new ArrayList<>(currentProgram.getProgramCategoryPoints())));
        dto.setProgramActivities(programActivityMapper.toDto(new ArrayList<>(currentProgram.getProgramActivities())));
        dto.setProgramLevels (programLevelMapper.toDto(new ArrayList<>(currentProgram.getProgramLevels())));
        dto.setProgramHealthyRanges(getLatestHealthyRangePayload(dto.getId()));

        // Set status to `QA Verify` if program is used for QA Verify
        dto.setStatusBeforeSendingIfProgramIsQAVerify();

        dto.isExtractable(currentProgram.getExtractable());


        ClientDto clientDto = new ClientDto();
        clientDto.setProgramId(currentProgram.getId());
        dto.setActiveDate(getActiveDate(programDTO.getId()));

        // reset cache of program from Platform
        if (!CollectionUtils.isEmpty(relatedClientPrograms) &&
            currentProgram.getStatus().equalsIgnoreCase(ProgramStatus.Active.toString())) {
            ClientProgram clientProgram = relatedClientPrograms.get(0);
            publisherService.publishProgramUpdatedEvent(clientProgram.getClientId(), currentProgram.getId());
        }

        return dto;
    }

    //validate program have activated before
    private void programHasActivatedBefore(Long programId) {

        List<ProgramTrackingStatus> trackingActiveStatus = programTrackingStatusRepository.findByProgramIdAndProgramStatus(programId, ProgramStatus.Active.name());
        if (!CollectionUtils.isEmpty(trackingActiveStatus)) {
           throw new BadRequestAlertException("Program Type can not change when program have activated before.", "PROGRAM", Constants.PROGRAM_TYPE_ERR);
        }
    }

    private boolean validateProgramBeforeJobRunActivated(Program program) {

        ProgramType programType = program.getProgramType();

        if (program.getIsTemplate()) {
            return false;
        }

        Set<ClientProgram> clientPrograms = program.getClientPrograms();

        if (CollectionUtils.isEmpty(clientPrograms)) {
            return false;
        }

        if (!programType.equals(ProgramType.FULL_OUTCOMES)) {
            return true;
        }

        Set<ProgramHealthyRange> healthyRanges = programHealthyRangeRepository.findByProgramId(program.getId());

        if (CollectionUtils.isEmpty(healthyRanges)) {
            return false;
        }

        List<ProgramCohort> programCohorts = programCohortRepository.findAllByProgramId(program.getId());
        if (CollectionUtils.isEmpty(programCohorts)) {
            return false;
        }

        List<ProgramCohort> defaultCohorts = programCohorts.stream().filter(ProgramCohort::isIsDefault).collect(Collectors.toList());
        if (defaultCohorts.size() != 1) {
            return false;
        }

        return true;

    }

    // handle remove data not use by program type
    private void handleUpdateProgramByType(ProgramType programType, Long programId) {

        switch (programType) {
            case FULL_OUTCOMES:
                // validate program full outcomes
                Set<ProgramHealthyRange> healthyRanges = programHealthyRangeRepository.findByProgramId(programId);
                if (CollectionUtils.isEmpty(healthyRanges)) {
                    throw new BadRequestAlertException("Program Health Ranges not configured yet.", "PROGRAM", Constants.FULL_OUTCOMES_ERR);
                }
                List<ProgramCohort> programCohorts = programCohortRepository.findAllByProgramId(programId);
                if (CollectionUtils.isEmpty(programCohorts)) {
                    throw new BadRequestAlertException("Program Cohorts not configured yet.", "PROGRAM", Constants.FULL_OUTCOMES_ERR);
                }

                List<ProgramCohort> defaultCohorts = programCohorts.stream().filter(ProgramCohort::isIsDefault).collect(Collectors.toList());
                if (defaultCohorts.isEmpty()) {
                    throw new BadRequestAlertException("Program default Cohorts not configured yet.", "PROGRAM", Constants.FULL_OUTCOMES_ERR);
                }

                if (defaultCohorts.size() > 1) {
                    throw new BadRequestAlertException("Program can not have more than one default cohort", "PROGRAM", Constants.FULL_OUTCOMES_ERR);
                }
                break;
            case PARTICIPANT_PROGRAM:

                // don't have healthy ranges
                Set<ProgramHealthyRange> programHealthyRanges = programHealthyRangeRepository.findByProgramId(programId);
                programHealthyRangeRepository.deleteAll(programHealthyRanges);

                List<ProgramSubCategoryConfiguration> configurationHealthRanges = programSubCategoryConfigurationRepository.findAllByProgramId(programId);
                programSubCategoryConfigurationRepository.deleteAll(configurationHealthRanges);

                removeAllSectionOfFullOutcomes(programId);

                break;

            case OUTCOMES_LITE:
                removeAllSectionOfFullOutcomes(programId);
                break;
        }
    }

    // remove all section for full outcomes if active program  is not full outcomes
    private void removeAllSectionOfFullOutcomes(Long programId) {
        // don't have cohorts
        List<ProgramCohort> programCohorts = programCohortRepository.findAllByProgramId(programId);

        List<ProgramCohortCollection> programCohortCollections = new ArrayList<>();

        List<ProgramCohortRule> programCohortRules = new ArrayList<>();

        for (ProgramCohort programCohort : programCohorts) {

            List<ProgramCohortCollection> cohortCollections = new ArrayList<>(programCohort.getProgramCohortCollections());
            programCohortCollections.addAll(cohortCollections);

            List<ProgramCohortRule> cohortRules = new ArrayList<>(programCohort.getProgramCohortRules());
            programCohortRules.addAll(cohortRules);
        }

        programCohortCollectionRepository.deleteAll(programCohortCollections);

        programCohortRuleRepository.deleteAll(programCohortRules);

        programCohortRepository.deleteAll(programCohorts);

        // don't have collections
        List<ProgramCollection> programCollections = programCollectionExtensionRepository.findAllByProgramId(programId);
        programCollectionExtensionRepository.deleteAll(programCollections);
    }

    private void validateUniqueForProgramQAVerify(ProgramDTO programDTO, Program currentProgram, List<ClientProgram> relatedClientPrograms) {
        // Validate unique if it is program qa verify:
        // 1. Status is changed to QA Verify or
        // 2. QA Date Range is changed
        Boolean isStatusChangedToQaVerify = isStatusChangedToQaVerify(programDTO, currentProgram);

        Boolean isQADateRangeChanged = isQADateRangeChanged(programDTO, currentProgram);

        if ( isStatusChangedToQaVerify || (isQADateRangeChanged && currentProgram.isQaDateSetForQaVerify( programDTO.getStatus(), programDTO.getProgramQAStartDate(), programDTO.getProgramQAEndDate()))) {
            if ( relatedClientPrograms == null ) {
                relatedClientPrograms = clientProgramRepository.findClientProgramsByProgramId(programDTO.getId());
            }

            for ( ClientProgram client: relatedClientPrograms) {
                List<ClientProgram> clientProgramList = clientProgramRepository.findClientProgramsByClientIdAndProgramStatusAndProgramQaVerify(client.getClientId(), ProgramStatus.Active.toString(), true);
                checkExistedProgram(programDTO, clientProgramList, "Client has another QA verify program");
            }
        }
    }

    private Boolean isStatusChangedToQaVerify(ProgramDTO programDTO, Program currentProgram) {
        Boolean isStatusChangedToQaVerify = false;

        if ( programDTO.getStatus().equals( ProgramStatus.QA_Verify.toString() )) {
            isStatusChangedToQaVerify = ( currentProgram == null || currentProgram.getStatus() == null
                || !(currentProgram.getStatus().equals( ProgramStatus.Active.toString() ) && currentProgram.getQaVerify()));
        }
        return isStatusChangedToQaVerify;
    }

    private Boolean isStatusChangedToDraftOrQAVerify(ProgramDTO programDTO, Program currentProgram) {
        Boolean isStatusChangedToDraftOrQAVerify = false;

        if ( programDTO.getStatus().equals( ProgramStatus.QA_Verify.toString() ) || programDTO.getStatus().equals( ProgramStatus.Draft.toString() )) {
            isStatusChangedToDraftOrQAVerify = ( currentProgram == null || currentProgram.getStatus() == null
                || !(currentProgram.getStatus().equals( ProgramStatus.Active.toString() ) && currentProgram.getQaVerify()));
        }
        return isStatusChangedToDraftOrQAVerify;
    }

    private Boolean isQADateRangeChanged(ProgramDTO programDTO, Program currentProgram) {
        Boolean isQADateRangeChanged = false;
        if (programDTO.getProgramQAStartDate() != null && programDTO.getProgramQAEndDate() != null) {
            isQADateRangeChanged = (currentProgram == null || currentProgram.getProgramQAStartDate() == null
                || currentProgram.getProgramQAEndDate() == null
                || currentProgram.getProgramQAStartDate().compareTo(programDTO.getProgramQAStartDate()) != 0
                || currentProgram.getProgramQAEndDate().compareTo(programDTO.getProgramQAEndDate()) != 0);
        }
        return isQADateRangeChanged;
    }

    private List<ClientProgram> validateProgramQADateRange(ProgramDTO programDTO, Program currentProgram, List<ClientProgram> relatedClientPrograms) {
        // Validate QAVerify Program if QADates range is changed
        // or program status is changed to Draft or QaVerify
        Boolean isQADateRangeChanged = isQADateRangeChanged(programDTO, currentProgram);
        Boolean isStatusChangedToDraftOrQAVerify = isStatusChangedToDraftOrQAVerify(programDTO, currentProgram);

        if ((isQADateRangeChanged || isStatusChangedToDraftOrQAVerify)
            && ( programDTO.getStatus().equals( ProgramStatus.Draft.toString() )
                || programDTO.getStatus().equals( ProgramStatus.QA_Verify.toString() ))) {
            if ( relatedClientPrograms == null ) {
                relatedClientPrograms = clientProgramRepository.findClientProgramsByProgramId(programDTO.getId());
            }

            for ( ClientProgram client: relatedClientPrograms) {
                List<ClientProgram> clientProgramList = clientProgramRepository.findClientProgramByQADateRange(client.getClientId(), programDTO.getProgramQAStartDate(), programDTO.getProgramQAEndDate());
                checkExistedProgram(programDTO, clientProgramList, "Client has another program which overlaps the program QA date range");
            }
        }
        return relatedClientPrograms;
    }

    private List<ClientProgram> validateDateRange(ProgramDTO programDTO, Program currentProgram) {
        List<ClientProgram> relatedClientPrograms = null;

        Boolean isChangeDateRange = false;

        if( programDTO.getStartDate() != null && programDTO.getResetDate() != null){

            isChangeDateRange = (currentProgram == null || currentProgram.getStartDate()==null || currentProgram.getResetDate() ==null||  currentProgram.getStartDate().compareTo(programDTO.getStartDate()) != 0) || (currentProgram.getResetDate().compareTo(programDTO.getResetDate()) != 0);
        }

        if (programDTO.getStatus().equalsIgnoreCase(ProgramStatus.Active.name())) {

            Set<ClientProgram> clientPrograms = currentProgram.getClientPrograms();

            if (!clientPrograms.isEmpty()) {

                // active only 1 client program
                Optional<String> clientIdOptional = clientPrograms.stream()
                    .map(ClientProgram::getClientId)
                    .findFirst();

                ArrayList<String> status = new ArrayList<>();

                status.add(ProgramStatus.Active.toString());

                // get client program is not qc
                Optional<ClientProgram> cp = clientProgramRepository.findByClientIdAndProgramQaVerifyAndProgramStatusIn(clientIdOptional.get(), false, status);
                if (cp.isPresent()) {
                    throw new BadRequestAlertException("Client have another active program", "client_program", clientIdOptional.get());
                }
            } else {
                throw new BadRequestAlertException("Client not assign active program yet.", "client_program", "Program");
            }
        }

        //Validate client with date range if change
        if((programDTO.getStatus().equals(ProgramStatus.Draft.toString()) || programDTO.getStatus().equals(ProgramStatus.Active.toString())) & isChangeDateRange){
            relatedClientPrograms = clientProgramRepository.findClientProgramsByProgramId(programDTO.getId());

            for (ClientProgram client: relatedClientPrograms) {
                List<ClientProgram> clientProgramList = clientProgramRepository.findClientProgramByDateRange(client.getClientId(), programDTO.getStartDate(), programDTO.getResetDate());
                checkExistedProgram(programDTO, clientProgramList, "Client has another program which overlaps the date range");
            }

        }
        return relatedClientPrograms;
    }

    private static void checkExistedProgram(ProgramDTO programDTO, List<ClientProgram> clientProgramList, String msg) {
        Optional<ClientProgram> clientProgram = clientProgramList.stream().filter(c -> !c.getProgramId().equals(programDTO.getId())).findFirst();
        if (clientProgram.isPresent()) {
            ClientProgram cp = clientProgram.get();
            String str = "{\"programId\":\"" + cp.getProgramId() + "\",\"programName\":\"" + cp.getProgram().getName() + "\"}";
            throw new BadRequestAlertException(msg, "client_program", str);
        }
    }


    private ProgramSubCategoryConfiguration getConditionGroup(ProgramHealthyRangePayLoad dto) {
        return healthyRangePayloadMapper.getProgramSubCategoryConfiguration(dto);
    }

    /**
     * Save a program.
     *
     * @param programDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramDTO save(ProgramDTO programDTO) {

        boolean isNew = (programDTO.getId() == null || programDTO.getId() == 0);
        //validate programm
        log.debug("Request to save Program : {}", programDTO);
        if(programDTO.getStartDate() != null){
            if(programDTO.getResetDate() == null ||  programDTO.getStartDate().isAfter(programDTO.getResetDate())){
                throw new BadRequestAlertException("start date after reset date", "program", "dateRange");
            }
        }

        if(programDTO.getIsTemplate() ==null) programDTO.setIsTemplate(false);

        if(isNew) {
            return handleCreate(programDTO);
        }else{

            return handleUpdate(programDTO);
        }
    }

    /**
     * Get all the programs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Programs");
        return programRepository.findAll(pageable)
            .map(programMapper::toDto);
    }



    /**
     * Get one program by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramDTO> findOne(Long id) {
        log.debug("Request to get Program : {}", id);
        return programRepository.findById(id)
            .map(programMapper::toDto);
    }

    /**
     * Delete the program by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Program : {}", id);
        Program program = programRepository.getOne(id);
        if(program == null || !program.getIsTemplate())
              throw new BadRequestAlertException("Can not delete program", "program", "notexist");

        programRepository.deleteById(id);
    }

    /**
     * Get one program details by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public ProgramDTO getProgramDetail(Long id, boolean showProgramActivity) {
        log.debug("Request to get Program : {}", id);

        Optional<Program> p = programRepository.findOneWithEagerRelationships(id);

        if(!p.isPresent()) throw new BadRequestAlertException("program is not exist", "program", "notexist");
        Program program = p.get();
        ProgramDTO dto =  programMapper.toDto(program);
        List<ClientProgram> clientPrograms = clientProgramRepository.findClientProgramsByProgramId(id);
        dto.setClientPrograms(clientProgramMapper.toDto(clientPrograms));

        dto.setTotalPercentEconomy(applicationProperties.getTotalPercentEconomy());

        dto.setProgramCategoryPoints(programCategoryPointMapper.toDto(new ArrayList<>(program.getProgramCategoryPoints())));
        if (showProgramActivity) {
            List<ProgramActivity> programActivityList = programActivityRepository.findProgramActivitiesByProgramIdAndProgramPhaseIdIsNotNull(program.getId());
            dto.setProgramActivities(programActivityMapper.toDto(new ArrayList<>(programActivityList)));
        }
        dto.setProgramLevels(programLevelMapper.toDto(new ArrayList<>(program.getProgramLevels())));
        dto.setProgramSubgroups(programSubgroupMapper.toDto(new ArrayList<>(program.getProgramSubgroups())));
        List<ProgramHealthyRangePayLoad> payLoadList = program.getProgramHealthyRanges().stream()
            .map(this::createHealthyRangePayload).collect(Collectors.toList());
        dto.setProgramHealthyRanges(payLoadList);

        // Set status to `QA Verify` if program is used for QA Verify
        dto.setStatusBeforeSendingIfProgramIsQAVerify();

        dto.setActiveDate(getActiveDate(program.getId()));
        dto.setIsOutcomesLite(program.getProgramType().equals(ProgramType.OUTCOMES_LITE));
        return dto;
    }

    @Transactional(readOnly = true)
    public ProgramDTO getProgramDetail(Long id, boolean enableDemoGraphic, boolean showProgramActivity) {
        ProgramDTO dto =  getProgramDetail(id, showProgramActivity);
        if (enableDemoGraphic) {
            String employerId = "0";
            if (!CollectionUtils.isEmpty(dto.getClientPrograms())) {
                employerId = dto.getClientPrograms().get(0).getClientId();
            }
            dto.setDemographics(getDemoGraphicList(employerId));
        }
        dto.setIsOutcomesLite(dto.getProgramType().equals(ProgramType.OUTCOMES_LITE));
        return dto;
    }

    private List<DemoGraphicValueDto> getDemoGraphics (List<String> items) {
        List<DemoGraphicValueDto> demoGraphicItems = new ArrayList<>();
        items.forEach(value -> {
            DemoGraphicValueDto valueDto = new DemoGraphicValueDto();
            valueDto.setColumnValue(value);
            demoGraphicItems.add(valueDto);
        });
        return demoGraphicItems;
    }

    private List<DemoGraphicDto> getDemoGraphicList (String employerId) {
        ESDemoGraphicResponseDto esDemoGraphicResponseDtoList;
        List<DemoGraphicDto> demoGraphicDtoList = new ArrayList<>();
        Map<String, List<DemoGraphicValueDto>> demoGraphicValueDtoMap = new HashMap<>();
        try {
            esDemoGraphicResponseDtoList = employeeServiceRest.getEmployerDemographic(employerId);

            if (esDemoGraphicResponseDtoList != null && !CollectionUtils.isEmpty(esDemoGraphicResponseDtoList.getData())) {
                esDemoGraphicResponseDtoList.getData().forEach(demoItem -> {
                    if (!demoGraphicValueDtoMap.containsKey(demoItem.getDemographicType())) {
                        demoGraphicValueDtoMap.put(demoItem.getDemographicType(), new ArrayList<>());
                    }
                    DemoGraphicValueDto valueDto = new DemoGraphicValueDto();
                    valueDto.setColumnValue(demoItem.getDemographicValue());
                    demoGraphicValueDtoMap.get(demoItem.getDemographicType()).add(valueDto);
                });
            }
        } catch (Exception e) {
            log.error("getDemoGraphicList : {}", e.getMessage());
        }

        // map data to demographic properties
        demoGraphicValueDtoMap.forEach((k, value) -> {
            DemoGraphicDto demoGraphicDto = new DemoGraphicDto();
            demoGraphicDto.setColumnHeader(k);
            demoGraphicDto.setColumnValues(value);
            demoGraphicDtoList.add(demoGraphicDto);
        });
        return demoGraphicDtoList;
    }

    private ProgramHealthyRangePayLoad createHealthyRangePayload(ProgramHealthyRange programHealthyRange) {
        return healthyRangePayloadMapper.toPayload(programHealthyRangeMapper.toDto(programHealthyRange));
    }

    /**
     * Get one program details by id.
     *
     * @param clientId the id of the entity.
     * @return the entity.
     */


    //TODO: 2 issues need to be refactor in here
    /*
    * - api heavy, assign program user should be create another function
    * - create another service for Client Center and AP BE
    * */
    @Transactional
    public ProgramDTO getProgramDetailByClienId(String clientId, String subgroupId, String participantId, boolean showProgramActivity, boolean isQc) {
        log.debug("Request to get Program clieng Id: {}", clientId);

        ProgramDTO dto = new ProgramDTO();
        dto.setStatus(ProgramStatus.Draft.toString());
      //  dto.setClientId(clientId);

        ArrayList<String> status = new ArrayList<>();

        status.add(ProgramStatus.Active.toString());

        // get client program is not qc
        Optional<ClientProgram> cp = clientProgramRepository.findByClientIdAndProgramQaVerifyAndProgramStatusIn(clientId, isQc, status);

        if(!cp.isPresent()) return dto;

        Optional<Program> p = programRepository.findOneWithEagerRelationships(cp.get().getProgramId());

        if(!p.isPresent()) return dto;
        Program program = p.get();
        dto =  programMapper.toDto(program);

        //get client brand setting
        ClientBrandSetting clientBrandSetting = Option.ofOptional(clientBrandSettingRepository.findClientBrandSettingByClientId(clientId))
            .getOrElse(new ClientBrandSetting());
        dto.setSubPathUrl(clientBrandSetting.getSubPathUrl());

        dto.setTotalPercentEconomy(applicationProperties.getTotalPercentEconomy());
        dto.setProgramCategoryPoints(programCategoryPointMapper.toDto(new ArrayList<>(program.getProgramCategoryPoints())));

        if (showProgramActivity) {
            List<ProgramActivity> programActivityList = programActivityRepository.findProgramActivitiesByProgramIdAndProgramPhaseIdIsNotNull(program.getId());
            dto.setProgramActivities(programActivityMapper.toDto(new ArrayList<>(programActivityList)));
        }

        dto.setProgramSubgroups(programSubgroupMapper.toDto(new ArrayList<>(program.getProgramSubgroups())));

        program.getProgramLevels().forEach(programLevel -> {
            List<ProgramLevelActivity> programLevelActivities = userIncentiveBusinessRule.getRequiredActivityBySubgroup(programLevel, subgroupId);
            programLevel.setActivities(new HashSet<>(programLevelActivities));

            List<ProgramLevelReward> programLevelRewards = userIncentiveBusinessRule.getProgramLevelRewardsBySubgroup(programLevel, subgroupId);

            programLevel.setProgramLevelRewards(new HashSet<>(programLevelRewards));
        });

        List<ProgramLevelDTO> programLevelDTOList = program.getProgramLevels().stream().map(programLevel -> {
            ProgramLevelDTO programLevelDTO = programLevelMapper.toDto(programLevel);
            List<ProgramRequirementItemsDTO> requiredItems = programLevel.getProgramRequirementItems().stream().map(item -> programRequirementItemsMapper.toDto(item)).collect(Collectors.toList());
            programLevelDTO.setProgramRequirementItems(requiredItems);

            //fix programLevelLevelOrder for path
            List<ProgramLevelPathDTO> programLevelPathDTOList = programLevel.getProgramLevelPaths().stream().map(item -> programLevelPathMapper.toDto(item)).collect(Collectors.toList());
            programLevelDTO.setProgramLevelPaths(programLevelPathDTOList);

            //fix programLevelLevelOrder for activity
            List<ProgramLevelActivityDTO> programLevelActivityDTOList = programLevel.getProgramLevelActivities().stream().map(item -> programLevelActivityMapper.toDto(item)).collect(Collectors.toList());
            programLevelDTO.setProgramLevelActivities(programLevelActivityDTOList);

            return programLevelDTO;
        }).collect(Collectors.toList());

        dto.setProgramLevels (programLevelDTOList);

        dto.getProgramLevels().stream().forEach(
            programLevelDTO -> {

                programLevelDTO.getProgramLevelRewards().forEach(programLevelRewardDTO -> {
                    programLevelRewardDTO.setDescription(userIncentiveBusinessRule.buildRewardDescription(programLevelRewardMapper.toEntity(programLevelRewardDTO)));
                });

                List<ProgramLevelRewardDTO> rewardDTOList = programLevelDTO.getProgramLevelRewards().stream().filter(r -> r != null && (r.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoSurcharge.name()) || r.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoPayrollCredit.name()) || r.getRewardType().equalsIgnoreCase(RewardTypeEnum.TobaccoCustom.name()))).collect(Collectors.toList());
                programLevelDTO.getProgramLevelTobaccoRewards().addAll(rewardDTOList);

            }

        );



        List<ProgramLevelActivity> programLevelActivityList = new ArrayList<>();
        for (ProgramLevel level: program.getProgramLevels()
        ) {

            programLevelActivityList.addAll(level.getProgramLevelActivities());
        }
        setRequiredActivityWithLevel(dto, programLevelActivityList, subgroupId);

        if(!isNullOrEmpty(participantId)){
            dto =  checkCompletionAndSetPointForActivity(dto, participantId);
        }
        dto.setProgramHealthyRanges(healthyRangePayloadMapper.toHealthyPayloadList(programHealthyRangeMapper.toDto(new ArrayList<>(program.getProgramHealthyRanges()))));

        dto.setActiveDate(getActiveDate(program.getId()));

        dto.setIsOutcomesLite(dto.getProgramType().equals(ProgramType.OUTCOMES_LITE));

        return dto;
    }

    private ProgramDTO checkCompletionAndSetPointForActivity(ProgramDTO dto, String participantId) {
        for (ProgramCategoryPointDTO categoryPointDTO: dto.getProgramCategoryPoints()) {
            for (ProgramSubCategoryPointDTO subCategoryPointDTO: categoryPointDTO.getProgramSubCategoryPoints()) {
                if(subCategoryPointDTO.getValuePoint() == null)
                    subCategoryPointDTO.setValuePoint(BigDecimal.valueOf(0.0));
                if(subCategoryPointDTO.getActualPoint() == null)
                    subCategoryPointDTO.setActualPoint(BigDecimal.valueOf(0.0));
                if(subCategoryPointDTO.getValuePoint().equals(0)) continue;
                Integer completionCount = userEventRepository.countAllByEventCodeAndProgramUserProgramIdAndProgramUserParticipantId(subCategoryPointDTO.getCode(), dto.getId(), participantId);
                if(subCategoryPointDTO.getCompletionsCap() == null || completionCount >= subCategoryPointDTO.getCompletionsCap()) {
                    subCategoryPointDTO.setActualPoint(BigDecimal.valueOf(0.0));
                } else {
                    subCategoryPointDTO.setActualPoint(subCategoryPointDTO.getValuePoint());
                }
            }
        }
        return dto;
    }



    private void setRequiredActivityWithLevel(ProgramDTO dto, List<ProgramLevelActivity> programLevelActivityList, String subgroupId){
        dto.getProgramActivities().forEach( a-> {
            List<Integer> levels =  programLevelActivityList.stream()
                .filter(la-> (isNullOrEmpty(la.getSubgroupId()) && la.getActivityId().equals(a.getActivityId()))
                    || !isNullOrEmpty(la.getSubgroupId()) && la.getActivityId().equals(a.getActivityId()) && la.getSubgroupId().equals(subgroupId))
                .map(la->la.getProgramLevel().getLevelOrder()).collect(Collectors.toList());
            a.setAssignToLevel(Joiner.on(',').join(levels));
        });
    }

    @Override
    public ProgramDTO exportProgramTemplate(Long programId, ProgramTemplateDTO programTemplateDTO) {

        if(isNullOrEmpty( programTemplateDTO.getName()))
            throw new BadRequestAlertException("Missing template name","ProgramTemplate","TemplateName");
        if(isNullOrEmpty( programTemplateDTO.getCreatedBy()))
            throw new BadRequestAlertException("Missing created by","ProgramTemplate","CreatedBy");

        Optional<Program> optionalProgram = programRepository.findOneWithEagerRelationships(programId);
        if(!optionalProgram.isPresent())
            throw new BadRequestAlertException("Program Id is invalid","ProgramTemplate","ProgramId");

        Program p = optionalProgram.get();

        ProgramDTO dto =  programMapper.toDto(p);
        dto.setProgramLevels (programLevelMapper.toDto(new ArrayList<>(p.getProgramLevels())));
        dto.getProgramLevels().forEach(l->l.setProgramLevelActivities(null));
        dto.setIsTemplate(true);
        dto.setCreatedDate(Instant.now());
        dto.setLastModifiedDate(Instant.now());
        dto.setCreatedBy(programTemplateDTO.getCreatedBy());
        dto.setLastModifiedBy(programTemplateDTO.getCreatedBy());
        dto.setName(programTemplateDTO.getName());

        dto.setStartDate(null);
        dto.setResetDate(null);
        dto.setStatus(ProgramStatus.Template.toString());

        dto.setProgramCategoryPoints (programCategoryPointMapper.toDto(new ArrayList<>(p.getProgramCategoryPoints())));
        dto.setId(null);

        Program program = programMapper.toEntity(dto);

        program = programRepository.save(program);

        dto.setId(program.getId());

        //level + reward + category + sub
        CloneChildrenInProgram(dto, program);

        dto.setIsOutcomesLite(dto.getProgramType().equals(ProgramType.OUTCOMES_LITE));

        return dto;
    }

    @Override
    public ProgramDTO createProgramFromTemplate(Long templateId, ProgramTemplateDTO programTemplateDTO) {

        if(isNullOrEmpty( programTemplateDTO.getName()))
            throw new BadRequestAlertException("Missing program name","ProgramTemplate","TemplateName");
        if(isNullOrEmpty( programTemplateDTO.getCreatedBy()))
            throw new BadRequestAlertException("Missing created by","ProgramTemplate","CreatedBy");

        Optional<Program> optionalProgram = programRepository.findOneWithEagerRelationships(templateId);
        if(!optionalProgram.isPresent())
            throw new BadRequestAlertException("Program template Id is invalid","ProgramTemplate","programId");

        Program p = optionalProgram.get();

        Pageable pageable = PageRequest.of(0, 1);
        List<ClientProgram> clientProgramList = clientProgramRepository.findLastResetDateByClientId(programTemplateDTO.getClientId(), pageable);
        if (!CollectionUtils.isEmpty(clientProgramList)) {
            ClientProgram clientProgram = clientProgramList.get(0);
            Program existingProgram = clientProgram.getProgram();
            if (existingProgram != null) {
                Instant newStartDate = existingProgram.getResetDate();
                newStartDate = newStartDate.plus(1, ChronoUnit.DAYS);
                Instant newEndDate = newStartDate.plus(DAYS_OF_YEAR, ChronoUnit.DAYS);

                // adjust the start date of program to avoid the overlaps the date range
                p.setStartDate(newStartDate);
                p.setResetDate(newEndDate);
            }
        }

        //create new program entity
        ProgramDTO programDTO = programMapper.toDto(p);
        programDTO.setId(null);
        programDTO.setName("Clone of " + programTemplateDTO.getName());
        programDTO.setCreatedBy(programTemplateDTO.getCreatedBy());
        programDTO.setLastModifiedBy(programTemplateDTO.getLastModifiedBy());
        programDTO.setProgramCategoryPoints(new ArrayList<>());
        programDTO.setIsTemplate(false);
        //Create default level for new program
        Program programEntity = programMapper.toEntity(programDTO);
        Program program = programRepository.save(programEntity);

        //clone category point
        List<ProgramCategoryPointDTO> programCategoryPointDTOS = cloneProgramCategoryPoint(p.getProgramCategoryPoints(), program);

        //clone program activities and program phases
        List<ProgramActivity> programActivities = new ArrayList<>();
        List<ProgramLevelDTO> programLevelDTOList = new ArrayList<>();
        try {
            CloneCustomActivityRequest request = CloneCustomActivityRequest.builder()
                .fromProgramId(templateId)
                .newProgramId(program.getId()).build();
            CustomActivityMapperResponseDto responseDto = customActivityAdapter.cloneProgramPhases(request);

            // clone phases
            List<ProgramPhase> programPhases = programPhaseRepository.findProgramPhasesByProgramId(p.getId());
            programActivities = cloneProgramPhase(programPhases, program, responseDto.getOldToNewMapping());

            //clone level of program
            programLevelDTOList = cloneProgramLevel(p.getProgramLevels(), program, responseDto.getOldToNewMapping());

            // clone collections
            cloneProgramCollections(new ArrayList<>(p.getProgramCollections()), program, responseDto.getOldToNewMapping());

        } catch (Exception e) {
            e.printStackTrace();
        }

        // clone program healthy ranges
        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.findAllByProgramId(p.getId());
        cloneProgramHealthyRanges(p.getProgramHealthyRanges(), programSubCategoryConfigurationList, program);


        //clone cohorts
        cloneProgramCohorts(p.getProgramCohorts(), program);

        ProgramDTO dto = programMapper.toDto(program);

        dto.setProgramCategoryPoints(programCategoryPointDTOS);

        dto.setProgramLevels(programLevelDTOList);

        dto.setProgramActivities(programActivityMapper.toDto(programActivities));

        //Assign to client if have
        if(!isNullOrEmpty(programTemplateDTO.getClientId()) && !isNullOrEmpty(programTemplateDTO.getClientName()))  {
            ClientProgramDTO clientProgramDTO = new ClientProgramDTO();
            clientProgramDTO.setClientId(programTemplateDTO.getClientId());
            clientProgramDTO.setClientName(programTemplateDTO.getClientName());
            clientProgramDTO.setProgramId(program.getId());
            clientProgramDTO = clientProgramService.save(clientProgramDTO);
            program.getClientPrograms().add(clientProgramMapper.toEntity(clientProgramDTO));

            // Add subgroup for program
            for (ProgramSubgroupDTO programSubgroupDTO:programTemplateDTO.getProgramSubgroups()
            ) {
                programSubgroupDTO.setProgramId(dto.getId());
            }

            List<ProgramSubgroup> programSubgroups = programSubgroupMapper.toEntity(programTemplateDTO.getProgramSubgroups());
            dto.setProgramSubgroups(new ArrayList<>(programSubgroupMapper.toDto(programSubgroupRepository.saveAll(programSubgroups))));
        }

        dto.setIsOutcomesLite(dto.getProgramType().equals(ProgramType.OUTCOMES_LITE));

        return dto;

    }

    private List<ProgramActivity> cloneProgramPhase(List<ProgramPhase> programPhases, Program program, List<CloneActivityDTO> cloneActivityList) {

        Set<ProgramPhase> collect = programPhases.stream().map(p -> {
            ProgramPhase programPhase = new ProgramPhase();
            programPhase.setId(null);
            programPhase.setProgram(program);
            programPhase.setPhaseOrder(p.getPhaseOrder());
            programPhase.setEndDate(p.getEndDate());
            programPhase.setStartDate(p.getStartDate());
            return programPhase;
        }).collect(Collectors.toSet());

        programPhaseRepository.saveAll(collect);

        List<ProgramActivity> totalProgramActivities = new ArrayList<>();

        for (ProgramPhase programPhase : programPhases) {

            Optional<ProgramPhase> programPhaseOptional = collect.stream()
                .filter(c -> c.getPhaseOrder().equals(programPhase.getPhaseOrder()))
                .findFirst();

            if (!programPhaseOptional.isPresent()) {
                continue;
            }

            ProgramPhase programPhaseMapping = programPhaseOptional.get();

            List<ProgramActivity> programActivities = programActivityRepository.findProgramActivitiesByProgramPhaseId(programPhase.getId());
            List<ProgramActivity> newProgramActivity = new ArrayList<>();
            for (ProgramActivity pa : programActivities) {
                Optional<CloneActivityDTO> activityOptional = cloneActivityList.stream()
                    .filter(a -> a.getOldCustomActivityId().equalsIgnoreCase(pa.getActivityId()))
                    .findFirst();

                if (!activityOptional.isPresent()) {
                    continue;
                }
                CloneActivityDTO cloneActivityDTO = activityOptional.get();
                ProgramActivity programActivity = new ProgramActivity();
                programActivity.setProgramPhase(programPhaseMapping);
                programActivity.setProgram(program);
                programActivity.setIsCustomized(pa.isIsCustomized());
                programActivity.setProgramPhaseId(programPhaseMapping.getId());
                programActivity.setBonusPointsEnabled(pa.isBonusPointsEnabled());
                programActivity.setBonusPoints(pa.getBonusPoints());
                programActivity.setProgramId(pa.getProgramId());
                programActivity.setId(null);
                programActivity.setActivityId(cloneActivityDTO.getNewCustomActivityId());
                programActivity.setActivityCode(pa.getActivityCode());
                programActivity.setMasterId(pa.getMasterId());
                newProgramActivity.add(programActivity);
            }

            totalProgramActivities.addAll(newProgramActivity);
        }

        int batchSize = 10;
        if (totalProgramActivities.size() > batchSize) {
            List<ProgramActivity> savingObjects = new ArrayList<>();
            for (int i = 0; i < totalProgramActivities.size(); i++) {

                savingObjects.add(totalProgramActivities.get(i));

                if (i % batchSize == 0 && i > 0) {
                    programActivityRepository.saveAll(savingObjects);
                    savingObjects.clear();
                }
            }
            if (!CollectionUtils.isEmpty(savingObjects)) {
                programActivityRepository.saveAll(savingObjects);
            }
        } else {
            programActivityRepository.saveAll(totalProgramActivities);
        }

        return totalProgramActivities;
    }


    private void CloneChildrenInProgram(ProgramDTO dto, Program program){
        //level + reward
        for (ProgramLevelDTO levelDto: dto.getProgramLevels()) {
            levelDto.setProgramId(program.getId());
            levelDto.setId(null);
            ProgramLevel lv = programLevelRepository.save(programLevelMapper.toEntity(levelDto));
            levelDto.setId(lv.getId());
            for (ProgramLevelRewardDTO rewardDto: levelDto.getProgramLevelRewards()
            ) {
                rewardDto.setId(null);
                rewardDto.setProgramLevelId(lv.getId());
                ProgramLevelReward reward =  programLevelRewardRepository.save(programLevelRewardMapper.toEntity(rewardDto));
                rewardDto.setId(reward.getId());
            }
           // programLevelRewardRepository.saveAll(programLevelRewardMapper.toEntity(levelDto.getProgramLevelRewards()));
        }
        //category and sub
        for (ProgramCategoryPointDTO programCategoryPointDTO: dto.getProgramCategoryPoints()) {
            programCategoryPointDTO.setId(null);
            programCategoryPointDTO.setProgramId(program.getId());
            ProgramCategoryPoint categoryPoint = programCategoryPointRepository.save(programCategoryPointMapper.toEntity(programCategoryPointDTO));
            programCategoryPointDTO.setId(categoryPoint.getId());
            for (ProgramSubCategoryPointDTO programSubCategoryPointDTO: programCategoryPointDTO.getProgramSubCategoryPoints()
            ) {
                programSubCategoryPointDTO.setId(null);
                programSubCategoryPointDTO.setProgramCategoryPointId(categoryPoint.getId());
                ProgramSubCategoryPoint sub = programSubCategoryPointRepository.save(programSubCategoryPointMapper.toEntity(programSubCategoryPointDTO));
                programSubCategoryPointDTO.setId(sub.getId());
            }
           // programSubCategoryPointRepository.saveAll(programSubCategoryPointMapper.toEntity(programCategoryPointDTO.getProgramSubCategoryPoints()));
        }



    }
    @Override
    public ProgramDTO cloneProgramActivites(Long templateId, Long programId, List<CloneActivityDTO> cloneActivityDTOS) {

        //Clone program activities

        Program program = new Program();
        program.setId(programId);

        List<ProgramActivity> programActivities = programActivityRepository.findProgramActivitiesByProgramIdAndProgramPhaseIdIsNotNull(templateId);
        List<ProgramLevelActivity> programLevelActivities = programLevelActivityRepository.findProgramLevelActivitiesByProgramLevelProgramId(templateId);


        List<ProgramPhase> newProgramPhases = programPhaseRepository.findProgramPhasesByProgramId(programId);
        List<ProgramLevel> newProgramLevels = programLevelRepository.findProgramLevelsByProgram_Id(programId);

        for (CloneActivityDTO cloneDto: cloneActivityDTOS
             ) {
            //Add program activites
            Optional<ProgramActivity> optionalProgramActivity = programActivities.stream().filter(a->a.getActivityId().equals(cloneDto.getOldCustomActivityId())).findFirst();
            if(optionalProgramActivity.isPresent()){

                ProgramActivity currentProgramActivity = optionalProgramActivity.get();
                ProgramActivity programActivity = new ProgramActivity();
                programActivity.setActivityId(cloneDto.getNewCustomActivityId());
                programActivity.setActivityCode(currentProgramActivity.getActivityCode());
                programActivity.setProgram(program);

                Optional<ProgramPhase> optionalProgramPhase  = newProgramPhases.stream().filter(a->a.getPhaseOrder().equals(currentProgramActivity.getProgramPhase().getPhaseOrder())).findFirst();
                if(optionalProgramPhase.isPresent()) {
                    programActivity.setProgramPhase(optionalProgramPhase.get());
                }
                programActivity.setMasterId(currentProgramActivity.getMasterId());

                programActivityRepository.save(programActivity);

            }

            //Add level activites
            List<ProgramLevelActivity> programLevelActivityList = programLevelActivities.stream().filter(c->c.getActivityId().equals(cloneDto.getOldCustomActivityId())).collect(Collectors.toList());

            for (ProgramLevelActivity currentProgramLevelActivity: programLevelActivityList) {
                ProgramLevelActivity programLevelActivity = new ProgramLevelActivity();

                Optional<ProgramLevel> optionalProgramLevel  = newProgramLevels .stream().filter(a->a.getLevelOrder().equals(currentProgramLevelActivity.getProgramLevel().getLevelOrder())).findFirst();
                if(optionalProgramLevel.isPresent()) {
                    programLevelActivity.setProgramLevel(optionalProgramLevel.get());
                }

                programLevelActivity.setActivityId(cloneDto.getNewCustomActivityId());
                programLevelActivity.setActivityCode(currentProgramLevelActivity.getActivityCode());

                programLevelActivityRepository.save(programLevelActivity);
            }

        }

        return getProgramDetail(programId, true);

    }


    /**
     * Update status program base on todate
     * <p>
     * This is scheduled to get fired every 30 minutes of hour.
     */
    @Scheduled(cron = "0 */30 * * * *")
    @SchedulerLock(name = "processProgramStatus")
    public void checkProgramStatus() throws Exception {
        if(!settings.isScronJobEnabled()){
            return;
        }
        log.info("Start schedule daily check program status:" + Instant.now().toString());
        Instant curDateIns = DateHelpers.getCurrentDateTimeUTC().toInstant();
        Instant truncated = curDateIns.plus(1, ChronoUnit.MINUTES);

        List<String> statuses = Arrays.asList(ProgramStatus.Draft.toString(), ProgramStatus.Active.toString());
        // get all draft and qa verify program
        List<Program> draftPrograms = programRepository.findProgramsByStartDateBeforeAndStatusInAndIsTemplate(truncated, statuses, false);

        draftPrograms.stream().forEach(program -> {
            // not process active program
            if(program.getStatus().equals(ProgramStatus.Active.toString()) && !program.getQaVerify()){
                return;
            }

            boolean isValid = validateProgramBeforeJobRunActivated(program);

            if (!isValid) {
                return;
            }

            log.info("set program is active: {}", program.getId());
            program.setStatus(ProgramStatus.Active.toString());

            program.setLastModifiedDate(Instant.now());

            programRepository.save(program);

            CCClientProgramDto ccClientProgramDto = new CCClientProgramDto();
            ccClientProgramDto.setId(program.getId().toString());
            ccClientProgramDto.setStatus(ProgramStatus.Active.name());
            List<ClientProgram> clientPrograms = new ArrayList<>(program.getClientPrograms());
            ccClientProgramDto.setClientPrograms(clientPrograms);

            trackingProgramChangesStatus(program.getStatus(),  program.getId());

            try {
                CompletableFuture<Integer> orderResponseCompletableFuture =  clientCenterRest.triggerProgramActiveEmail(ccClientProgramDto);
                orderResponseCompletableFuture.thenAcceptAsync(notificationItemResponse -> {
                    if (notificationItemResponse != null) {
                        log.debug("Program active successfully!, {}", Instant.now());
                    }
                }).exceptionally(e -> {
                    apiTracker.trackIntegrationService(e.getMessage(), "500", ccClientProgramDto.toString(), CLIENT_CENTER, clientCenterRest.getBaseUrl());
                    return null;
                });
            } catch (Exception e) {
                log.debug(e.getMessage());

            }

            // send update level
            //increase query client program
            if (!CollectionUtils.isEmpty(clientPrograms)) {
                ClientProgram clientProgram = clientPrograms.get(0);
                publisherService.publishWithEvent(ProgramSNSEvent.PROGRAM_UPDATED,
                    ProgramStatusChangeMessage.builder()
                        .programId(program.getId())
                        .clientId(clientProgram.getClientId())
                        .timeStamp(Instant.now())
                        .build()
                );
            }

        });

        programRepository.findProgramsByResetDateBeforeAndStatusAndIsTemplate(truncated, ProgramStatus.Active.toString(), false)
            .forEach(p->{
                log.info("set program is completed: {}", p.getId());
                p.setStatus(ProgramStatus.Completed.toString());
                p.setQaVerify(false);
                p.setLastModifiedDate(Instant.now());
                programRepository.save(p);


                // send update level
                //increase query client program
                List<ClientProgram> clientPrograms = clientProgramRepository.findClientProgramsByProgramId(p.getId());
                if (!CollectionUtils.isEmpty(clientPrograms)) {
                    ClientProgram clientProgram = clientPrograms.get(0);
                    publisherService.publishWithEvent(ProgramSNSEvent.PROGRAM_UPDATED,
                        ProgramStatusChangeMessage.builder()
                            .programId(p.getId())
                            .clientId(clientProgram.getClientId())
                            .timeStamp(Instant.now())
                            .build()
                    );
                }

            });

    }

    /**
     * Update status program base on todate
     * <p>
     * This is scheduled to get fired every 31 minutes
     */
    @Scheduled(cron = "0 */31 * * * *")
    @SchedulerLock(name = "processProgramStatusQC")
    public void checkProgramStatusQc() throws Exception {
        if(!settings.isScronJobEnabled()){
            return;
        }
        log.info("Start schedule daily check program status qc:" + Instant.now().toString());
        Instant curDateUtc = DateHelpers.getCurrentDateTimeUTC().toInstant();
        Instant truncated = curDateUtc.plus(1, ChronoUnit.MINUTES);

        List<String> statuses = Arrays.asList(ProgramStatus.Draft.toString());
        // get all draft and qa verify program
        List<Program> draftPrograms = programRepository.findProgramsByProgramQAStartDateBeforeAndStatusInAndIsTemplate(truncated, statuses, false);
        draftPrograms.stream().forEach(program -> {
            // not process active program
            if(program.getQaVerify()){
                return;
            }

            boolean isValidProgram = validateProgramBeforeJobRunActivated(program);
            if (!isValidProgram) {
                return;
            }

            log.info("set program is active: {}", program.getId());
            Instant curDateIns = DateHelpers.getCurrentDateTimeUTC().toInstant();
            // check set qa verify
            if(program.getProgramQAEndDate() != null &&
                program.getProgramQAStartDate() != null &&
                program.getProgramQAStartDate().isBefore(curDateIns)
                && program.getProgramQAEndDate().isAfter(curDateIns)){
                program.setStatus(ProgramStatus.Active.toString());
                program.setQaVerify(true);
            }

            programRepository.save(program);

            trackingProgramChangesStatus(program.getStatus(),  program.getId());

        });

        programRepository.findProgramsByProgramQAEndDateBeforeAndStatusAndQaVerifyAndIsTemplate(truncated, ProgramStatus.Active.toString(), true, false)
            .forEach(p->{
                log.info("set program is completed verify: {}", p.getId());
                p.setStatus(ProgramStatus.Draft.toString());
                p.setQaVerify(false);
                p.setLastModifiedDate(Instant.now());
                programRepository.save(p);

                trackingProgramChangesStatus(p.getStatus(),  p.getId());
            });
    }


    @Override
    public List<CustomActivityDTO> getCustomActivitiesByProgramId(ActivityType type, Long programId) throws Exception {
        Option<Program> programOption = Option.ofOptional(
            programRepository.findById(programId)
        );

        List<CustomActivityDTO> customActivityList = customActivityAdapter.getCustomActivityByTypeAndProgramId(type.name(), programId);

        List<String> activityIds = customActivityList
            .stream().map(CustomActivityDTO::getId)
            .map(Object::toString)
            .collect(Collectors.toList());
        //TODO: technical dept, should storage activity type in program activity

        programOption.peek(program -> {
            List<String> idsResult = programActivityRepository.findProgramActivityIdsByProgramId(programId);
            customActivityList.stream().filter(c -> idsResult.contains(c)).collect(Collectors.toList());
        });
        return customActivityList;
    }

    @Override
    public UpdatedSubgroupClientPayload updateProgramByNewSubgroupId(UpdatedSubgroupClientPayload updatedSubgroupClientPayload) {


        String clientId = updatedSubgroupClientPayload.getClientId();

        ArrayList<String> programStatus = new ArrayList<>();
        programStatus.add(ProgramStatus.Draft.name());
        programStatus.add(ProgramStatus.Active.name());

        List<ClientProgram> clientPrograms = clientProgramRepository.findClientProgramsByClientIdAndProgramStatusIn(clientId, programStatus);

        if (!updatedSubgroupClientPayload.getAddNewSubgroups().isEmpty()) {
            updatedSubgroupClientPayload.getAddNewSubgroups().forEach(subgroupDTO -> {
                String subgroupId = subgroupDTO.getSubgroupId();
                String subgroupName = subgroupDTO.getSubgroupName();

                clientPrograms.forEach(clientProgram -> {
                    ProgramSubgroupDTO programSubgroupDTO = new ProgramSubgroupDTO();
                    programSubgroupDTO.setSubgroupName(subgroupName);
                    programSubgroupDTO.setSubgroupId(subgroupId);
                    Program program = clientProgram.getProgram();
                    programSubgroupDTO.setProgramId(program.getId());
                    programSubgroupDTO.setProgramName(program.getName());
                    programSubgroupRepository.save(programSubgroupMapper.toEntity(programSubgroupDTO));
                });

            });
        }

        if (!updatedSubgroupClientPayload.getDeleteSubgroups().isEmpty()) {
            updatedSubgroupClientPayload.getDeleteSubgroups().forEach(subgroupDTO -> {

                String subgroupId = subgroupDTO.getSubgroupId();

                clientPrograms.forEach(clientProgram -> {
                    Program program = clientProgram.getProgram();

                    program.getProgramSubgroups().stream()
                        .filter(programSubgroup -> programSubgroup.getSubgroupId() != null & programSubgroup.getSubgroupId().equalsIgnoreCase(subgroupId))
                        .forEach(programSubgroupRepository::delete);

                    program.getProgramLevels().stream().forEach(programLevel -> {

                        programLevel.getProgramLevelActivities().stream()
                            .filter(programLevelActivity -> programLevelActivity.getSubgroupId() != null)
                            .filter( p -> p.getSubgroupId().equalsIgnoreCase(subgroupId))
                            .forEach(programLevelActivityRepository::delete);

                        programLevel.getProgramLevelPaths().stream()
                            .filter(programLevelPath -> programLevelPath.getSubgroupId()!= null)
                            .filter( p -> p.getSubgroupId().equalsIgnoreCase(subgroupId))
                            .forEach(programLevelPathRepository::delete);

                        programLevel.getProgramLevelRewards().stream()
                            .filter(programLevelReward -> programLevelReward.getSubgroupId() != null)
                            .filter( p -> p.getSubgroupId().equalsIgnoreCase(subgroupId))
                            .forEach(programLevelRewardRepository::delete);

                        programLevel.getProgramRequirementItems().stream()
                            .filter(programRequirementItems -> programRequirementItems.getSubgroupId() != null)
                            .filter( p -> p.getSubgroupId().equalsIgnoreCase(subgroupId))
                            .forEach(programRequirementItemsRepository::delete);
                    });


                    termAndConditionRepository.findTermAndConditionsByClientId(clientId)
                        .stream().filter(termAndCondition -> termAndCondition.getSubgroupId() != null)
                        .filter(t -> t.getSubgroupId().equalsIgnoreCase(subgroupId))
                        .forEach(termAndConditionRepository::delete);
                });
            });
        }

        updatedSubgroupClientPayload.setMessage("Update Subgroup Success.");

        return updatedSubgroupClientPayload;
    }

    @Transactional
    public String checkProgramStatusManually() throws Exception {
        log.info("Start schedule daily check program status:" + Instant.now().toString());
        Instant curDateUtc = DateHelpers.getCurrentDateTimeUTC().toInstant();

        Instant truncated = curDateUtc.plus(1, ChronoUnit.MINUTES);


       List<CCClientProgramDto> clientProgramDtoList = new ArrayList<>();
        List<String> statuses = Arrays.asList(ProgramStatus.Draft.toString(), ProgramStatus.Active.toString());
        AtomicReference<String> result = new AtomicReference<>("Not Run");
        // get all draft and qa verify program
        programRepository.findProgramsByStartDateBeforeAndStatusInAndIsTemplate(truncated, statuses,false)
            .forEach(p->{

                log.info("set program is active: {}", p.getId());
                if(p.getStatus().equals(ProgramStatus.Active.toString()) && !p.getQaVerify()){
                    return;
                }

                boolean isValidProgram = validateProgramBeforeJobRunActivated(p);

                if (!isValidProgram) {
                    result.set("program can not active");
                    return;
                }

                p.setStatus(ProgramStatus.Active.toString());
                p.setQaVerify(false);
                programRepository.save(p);

                CCClientProgramDto ccClientProgramDto = new CCClientProgramDto();
                ccClientProgramDto.setId(p.getId().toString());
                ccClientProgramDto.setStatus(ProgramStatus.Active.name());
                List<ClientProgram> clientPrograms = new ArrayList<>(p.getClientPrograms());
                ccClientProgramDto.setClientPrograms(clientPrograms);
                clientProgramDtoList.add(ccClientProgramDto);

            });

        List<String> status = new ArrayList<>();
        if (!clientProgramDtoList.isEmpty()) {

            for (CCClientProgramDto ccClientProgramDto : clientProgramDtoList) {
                status.add(ccClientProgramDto.getId());
                clientCenterRest.triggerProgramActiveEmail(ccClientProgramDto);
                log.debug(ccClientProgramDto.toString());

            }
        }
        if (status.size() > 0) {
            return String.format("Trigger email Successfully: %d/ %d", status.size(), clientProgramDtoList.size());
        }

        return result.get();
    }
}
