package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramRequirementItemsService;
import aduro.basic.programservice.domain.ProgramRequirementItems;
import aduro.basic.programservice.repository.ProgramRequirementItemsRepository;
import aduro.basic.programservice.service.dto.ProgramRequirementItemsDTO;
import aduro.basic.programservice.service.mapper.ProgramRequirementItemsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramRequirementItems}.
 */
@Service
@Transactional
public class ProgramRequirementItemsServiceImpl implements ProgramRequirementItemsService {

    private final Logger log = LoggerFactory.getLogger(ProgramRequirementItemsServiceImpl.class);

    private final ProgramRequirementItemsRepository programRequirementItemsRepository;

    private final ProgramRequirementItemsMapper programRequirementItemsMapper;

    public ProgramRequirementItemsServiceImpl(ProgramRequirementItemsRepository programRequirementItemsRepository, ProgramRequirementItemsMapper programRequirementItemsMapper) {
        this.programRequirementItemsRepository = programRequirementItemsRepository;
        this.programRequirementItemsMapper = programRequirementItemsMapper;
    }

    /**
     * Save a programRequirementItems.
     *
     * @param programRequirementItemsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramRequirementItemsDTO save(ProgramRequirementItemsDTO programRequirementItemsDTO) {
        log.debug("Request to save ProgramRequirementItems : {}", programRequirementItemsDTO);
        ProgramRequirementItems programRequirementItems = programRequirementItemsMapper.toEntity(programRequirementItemsDTO);
        programRequirementItems = programRequirementItemsRepository.save(programRequirementItems);
        return programRequirementItemsMapper.toDto(programRequirementItems);
    }

    /**
     * Get all the programRequirementItems.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramRequirementItemsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramRequirementItems");
        return programRequirementItemsRepository.findAll(pageable)
            .map(programRequirementItemsMapper::toDto);
    }


    /**
     * Get one programRequirementItems by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramRequirementItemsDTO> findOne(Long id) {
        log.debug("Request to get ProgramRequirementItems : {}", id);
        return programRequirementItemsRepository.findById(id)
            .map(programRequirementItemsMapper::toDto);
    }

    /**
     * Delete the programRequirementItems by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramRequirementItems : {}", id);
        programRequirementItemsRepository.deleteById(id);
    }
}
