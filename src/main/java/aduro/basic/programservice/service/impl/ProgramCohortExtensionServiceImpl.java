package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.ProgramCollectionExtensionRepository;
import aduro.basic.programservice.service.ProgramCohortExtensionService;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionDTO;
import aduro.basic.programservice.service.dto.ProgramCohortExtensionDto;
import aduro.basic.programservice.service.dto.ProgramCohortRuleDTO;
import aduro.basic.programservice.service.dto.ProgramStatus;
import aduro.basic.programservice.service.mapper.ProgramCohortCollectionMapper;
import aduro.basic.programservice.service.mapper.ProgramCohortRuleMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProgramCohortExtensionServiceImpl implements ProgramCohortExtensionService {

    @Autowired
    ProgramRepository programRepository;

    @Autowired
    ProgramCohortRepository programCohortRepository;

    @Autowired
    ProgramCohortRuleMapper programCohortRuleMapper;

    @Autowired
    ProgramCohortRuleRepository programCohortRuleRepository;

    @Autowired
    ProgramCohortCollectionRepository programCohortCollectionRepository;

    @Autowired
    ProgramCohortCollectionMapper programCohortCollectionMapper;

    @Autowired
    ProgramCollectionExtensionRepository programCollectionRepository;

    @Autowired
    ProgramCohortDataInputRepository programCohortDataInputRepository;

    @Autowired
    private ProgramTrackingStatusRepository programTrackingStatusRepository;

    @Autowired
    private ProgramUserCohortRepository programUserCohortRepository;

    @Autowired
    private ProgramCollectionContentRepository programCollectionContentRepository;

    @Override
    public ProgramCohortExtensionDto save(ProgramCohortExtensionDto programCohortDto) {

        boolean isCreated = false;
        if (programCohortDto.getId() == null || programCohortDto.getId() == 0) {
            isCreated = true;
        }

        if (isCreated) {
            return handleCreateProgramCohort(programCohortDto);
        }

        return handleUpdateProgramCohort(programCohortDto);
    }

    private ProgramCohortExtensionDto handleUpdateProgramCohort(ProgramCohortExtensionDto programCohortDto) {

        //validate program
        Program program = programRepository.getOne(programCohortDto.getProgramId());

        ProgramCohort programCohort = programCohortRepository.getOne(programCohortDto.getId());

        if (!programCohort.getCohortName().equalsIgnoreCase(programCohortDto.getCohortName())) {

            boolean existsByCohortName = programCohortRepository.existsByCohortNameAndProgram(programCohortDto.getCohortName(), program);

            if (existsByCohortName) {
                throw new BadRequestAlertException("Cohort Name already in use, enter a unique name", "Cohort", Constants.COHORT_NAME_EXISTED);
            }
            programCohort.setCohortName(programCohortDto.getCohortName());
        }

        programCohort.setIsDefault(programCohortDto.getIsDefault());

        programCohort.setModifiedDate(Instant.now());

        programCohort.setCohortDescription(programCohortDto.getCohortDescription());

        // program can not have more than one default cohort
        if (programCohortDto.getIsDefault()) {
            List<ProgramCohort> programCohorts = programCohortRepository.findAllByProgramIdAndIsDefault(program.getId(), true);
            if (!programCohorts.isEmpty() && programCohorts.size() > 1) {
                throw  new BadRequestAlertException("Program can not have already more then one default cohort.", "Program", Constants.COHORT_ERROR_DEFAULT);
            }
        }

        programCohort = programCohortRepository.save(programCohort);

        // rules can not update if program have activated before

        boolean hasProgramActivated = isNotEditedCohort(program);

        // program has activated before
        if (hasProgramActivated) {
            throw new BadRequestAlertException("Cohorts can not be changed when users have placed.", "PROGRAM", Constants.COHORT_NOT_CHANGED);
        }

        if (programCohort.isIsDefault()) {

            programCohortRuleRepository.deleteAllByProgramCohort(programCohort);

            programCohortDto.setProgramCohortRules(new ArrayList<>());

        }  else {

            // handle create or update rules
            List<ProgramCohortRuleDTO> programCohortRuleDtos = validateProgramCohortRule(programCohortDto.getProgramCohortRules());

            List<ProgramCohortRule> programCohortExistedRules = programCohortRuleRepository.findAllByProgramCohort(programCohort);

            List<Long> updateRulesIds = programCohortRuleDtos.stream()
                .filter(c -> c.getId() != null)
                .map(ProgramCohortRuleDTO::getId).collect(Collectors.toList());


            List<ProgramCohortRule> deleteRulesItems = programCohortExistedRules.stream()
                .filter(c -> !updateRulesIds.contains(c.getId())).collect(Collectors.toList());

            programCohortRuleRepository.deleteAll(deleteRulesItems);

            List<ProgramCohortRule> programCohortRules = programCohortRuleMapper.toEntity(programCohortRuleDtos);

            programCohortRules.forEach(o -> o.setModifiedDate(Instant.now()));

            programCohortRules = programCohortRuleRepository.saveAll(programCohortRules);

            programCohortDto.setProgramCohortRules(programCohortRuleMapper.toDto(programCohortRules));
        }

        // Handle cohort collection
        List<ProgramCohortCollectionDTO> programCohortCollections = programCohortDto.getProgramCohortCollections();

        List<ProgramCohortCollectionDTO> satisfiedProgramCohortCollections = validateProgramCohortCollection(programCohortCollections, program);

        List<Long> updateIds = programCohortCollections.stream()
            .filter(c -> c.getId() != null)
            .map(ProgramCohortCollectionDTO::getId).collect(Collectors.toList());

        // finding remove items
        List<ProgramCohortCollection> collectionOfCohorts = programCohortCollectionRepository.findAllByProgramCohort(programCohort);

        List<ProgramCohortCollection> deleteItems = collectionOfCohorts.stream()
            .filter(c -> !updateIds.contains(c.getId())).collect(Collectors.toList());

        programCohortCollectionRepository.deleteAll(deleteItems);


        List<ProgramCohortCollection> updatedList = programCohortCollectionRepository.saveAll(programCohortCollectionMapper.toEntity(satisfiedProgramCohortCollections));

        programCohortDto.setProgramCohortCollections(programCohortCollectionMapper.toDto(updatedList));

        programCohortDto.setCreatedDate(programCohort.getCreatedDate());

        programCohortDto.setModifiedDate(programCohort.getModifiedDate());

        programCohortDto.setIsNotEditable(isNotEditedCohort(program));

        return programCohortDto;
    }

    private List<ProgramCohortRuleDTO> validateProgramCohortRule(List<ProgramCohortRuleDTO> rules) {

        List<ProgramCohortDataInput> dataInputSupport = programCohortDataInputRepository.findAll();

        for (ProgramCohortRuleDTO rule : rules) {

            if (!CollectionUtils.isEmpty(dataInputSupport)) {

                List<ProgramCohortDataInput> cohortDataInputs = dataInputSupport.stream()
                    .filter(o -> Objects.nonNull(o.getInputType()) && o.getInputType().equalsIgnoreCase(rule.getDataInputType()))
                    .collect(Collectors.toList());

                if (cohortDataInputs.isEmpty()) {
                    throw new BadRequestAlertException("Cohort Data Input is not supported.", "ProgramCollection", Constants.COHORT_DATA_INPUT_NOT_SUPPORTED);
                }
            }

            //find differences
            if (Objects.isNull(rule.getRules())) {
                throw new BadRequestAlertException("Criteria can not null.", "ProgramCollection", Constants.COHORT_DATA_INPUT_NOT_SUPPORTED);
            }

            if (Objects.nonNull(rule.getId())) {
                if (Objects.isNull(rule.getCreatedDate())) {
                    ProgramCohortRule one = programCohortRuleRepository.getOne(rule.getId());
                    rule.setCreatedDate(one.getCreatedDate());
                }
            } else {
                rule.setCreatedDate(Instant.now());

            }
            rule.setModifiedDate(Instant.now());
        }

        return  rules;
    }

    private List<ProgramCohortCollectionDTO> validateProgramCohortCollection(List<ProgramCohortCollectionDTO> programCohortCollections, Program program) {

        List<ProgramCohortCollectionDTO> satisfiedProgramCohortCollections = new ArrayList<>();

        int sizeOfLevels = program.getProgramLevels().size();

        for (ProgramCohortCollectionDTO dto : programCohortCollections) {

            if (Objects.isNull(dto.getProgramCollectionId())) {
                throw new BadRequestAlertException("Collection Id must not null.", "ProgramCollection", Constants.COLLECTION_NOT_FOUND);
            }

            List<ProgramCohortCollectionDTO> duplicatedCollections = satisfiedProgramCohortCollections.stream()
                .filter(p -> p.getProgramCollectionId() != null && p.getProgramCollectionId().equals(dto.getProgramCollectionId()))
                .collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(duplicatedCollections)) {
                String errorMessage = String.format("Collection with Id = %s have added in cohort.", dto.getProgramCollectionId());
                throw new BadRequestAlertException(errorMessage, "ProgramCollection", Constants.COLLECTION_HAS_EXISTED);
            }

            if (Objects.nonNull(dto.getRequiredCompletion())) {

                ProgramCollection programCollection = programCollectionRepository.getOne(dto.getProgramCollectionId());

                int sizeOfItems = programCollection.getProgramCollectionContents().size();

                if (dto.getRequiredCompletion() > sizeOfItems || dto.getRequiredCompletion() <= 0) {
                    throw new BadRequestAlertException("Required completions may not exceed the number of items in the collection.", "ProgramCollection", Constants.COLLECTION_REQUIRED_COMPLETION);
                }
            }

            if (Objects.nonNull(dto.getRequiredLevel())) {

                if (dto.getRequiredLevel() > sizeOfLevels || dto.getRequiredLevel() <= 0) {
                    throw new BadRequestAlertException("Required Level can not more than total of Level in program.", "ProgramCollection", Constants.COLLECTION_REQUIRED_LEVEL);
                }
            }

            if (Objects.nonNull(dto.getId())) {
                if (Objects.isNull(dto.getCreatedDate())) {
                    Optional<ProgramCohortCollection> programCohortCollectionOptional = programCohortCollectionRepository.findById(dto.getId());
                    ProgramCohortCollection programCollectionContent = programCohortCollectionOptional.get();
                    dto.setCreatedDate(programCollectionContent.getCreatedDate());
                }

            } else {
                dto.setCreatedDate(Instant.now());
            }

            dto.setModifiedDate(Instant.now());

            satisfiedProgramCohortCollections.add(dto);
        }

        return satisfiedProgramCohortCollections;
    }


    private ProgramCohortExtensionDto handleCreateProgramCohort(ProgramCohortExtensionDto programCohortDto) {

        //validate program
        Program program = programRepository.getOne(programCohortDto.getProgramId());

        boolean existsCohortName = programCohortRepository.existsByCohortNameAndProgram(programCohortDto.getCohortName(), program);

        if (existsCohortName) {
            throw new BadRequestAlertException("Cohort Name already in use a program, enter a unique name", "Cohort", Constants.COHORT_NAME_EXISTED);
        }

        // create program cohort
        ProgramCohort programCohort = ProgramCohort.builder()
            .id(null)
            .cohortName(programCohortDto.getCohortName())
            .createdDate(Instant.now())
            .modifiedDate(Instant.now())
            .isDefault(programCohortDto.getIsDefault())
            .cohortDescription(programCohortDto.getCohortDescription())
            .program(program)
            .build();

        programCohort = programCohortRepository.save(programCohort);

        // program can not have more than one default cohort
        if (programCohortDto.getIsDefault()) {
            List<ProgramCohort> programCohorts = programCohortRepository.findAllByProgramIdAndIsDefault(program.getId(), true);
            if (!programCohorts.isEmpty() && programCohorts.size() > 1) {
                throw  new BadRequestAlertException("Program can not have already more then one default cohort.", "Program", Constants.COHORT_ERROR_DEFAULT);
            }
        }

        programCohortDto.setId(programCohort.getId());

        List<ProgramCohortRuleDTO> programCohortRuleDtos = validateProgramCohortRule(programCohortDto.getProgramCohortRules());

        List<ProgramCohortRule> programCohortRules = programCohortRuleMapper.toEntity(programCohortRuleDtos);

        for (ProgramCohortRule programCohortRule : programCohortRules) {
            programCohortRule.setId(null);
            programCohortRule.setCreatedDate(Instant.now());
            programCohortRule.setModifiedDate(Instant.now());
            programCohortRule.setProgramCohort(programCohort);
        }

        programCohortRules = programCohortRuleRepository.saveAll(programCohortRules);

        programCohortDto.setProgramCohortRules(programCohortRuleMapper.toDto(programCohortRules));

        List<ProgramCohortCollectionDTO> programCohortCollections = programCohortDto.getProgramCohortCollections();

        List<ProgramCohortCollectionDTO> programCohortCollectionEntities = validateProgramCohortCollection(programCohortCollections, program);

        for (ProgramCohortCollectionDTO collectionEntity : programCohortCollectionEntities) {
            collectionEntity.setId(null);
            collectionEntity.setCreatedDate(Instant.now());
            collectionEntity.setModifiedDate(Instant.now());
            collectionEntity.setProgramCohortId(programCohort.getId());
        }

        List<ProgramCohortCollection> entities = programCohortCollectionRepository.saveAll(programCohortCollectionMapper.toEntity(programCohortCollectionEntities));

        programCohortDto.setProgramCohortCollections(programCohortCollectionMapper.toDto(entities));
        programCohortDto.setCreatedDate(programCohort.getCreatedDate());
        programCohortDto.setModifiedDate(programCohort.getModifiedDate());
        programCohortDto.setIsNotEditable(isNotEditedCohort(program));
        return  programCohortDto;
    }

    @Override
    public Optional<ProgramCohortExtensionDto> findOne(Long id) {

        if (Objects.isNull(id)) {
            throw new BadRequestAlertException("Id can not be null.", "ProgramCohort", Constants.ID_NOT_FOUND);
        }

        Optional<ProgramCohort> programCohortOptional = programCohortRepository.findById(id);

        if (!programCohortOptional.isPresent()) {
            throw new BadRequestAlertException("Cohort can not found.", "ProgramCohort", Constants.COHORT_NOT_FOUND);
        }

        ProgramCohort programCohort = programCohortOptional.get();

        Program program = programCohort.getProgram();

        Boolean hasProgramActivated = isNotEditedCohort(program);

        Set<ProgramCohortCollection> programCohortCollections = programCohort.getProgramCohortCollections();

        Set<ProgramCohortRule> programCohortRules = programCohort.getProgramCohortRules();

        ProgramCohortExtensionDto dto = ProgramCohortExtensionDto.builder()
            .cohortName(programCohort.getCohortName())
            .createdDate(programCohort.getCreatedDate())
            .modifiedDate(programCohort.getModifiedDate())
            .isDefault(programCohort.isIsDefault())
            .id(programCohort.getId())
            .cohortDescription(programCohort.getCohortDescription())
            .programId(programCohort.getProgram().getId())
            .programCohortCollections(programCohortCollectionMapper.toDto(new ArrayList<>(programCohortCollections)))
            .programCohortRules(programCohortRuleMapper.toDto(new ArrayList<>(programCohortRules)))
            .isNotEditable(hasProgramActivated)
            .build();

        return Optional.of(dto);
    }

    @Override
    public void delete(Long id) {

        if (Objects.isNull(id)) {
            throw new BadRequestAlertException("Id can not be null.", "ProgramCohort", Constants.ID_NOT_FOUND);
        }

        Optional<ProgramCohort> programCohortOptional = programCohortRepository.findById(id);

        if (!programCohortOptional.isPresent()) {
            throw new BadRequestAlertException("Cohort can not found.", "ProgramCohort", Constants.COHORT_NOT_FOUND);
        }

        ProgramCohort programCohort = programCohortOptional.get();

        Program program = programCohort.getProgram();

        Boolean hasProgramActivated = isNotEditedCohort(program);

        if (hasProgramActivated) {
            throw new BadRequestAlertException("Program can not change when program have activated before.", "PROGRAM", Constants.PROGRAM_HAS_ACTIVATED);
        }

        programCohortCollectionRepository.deleteAll(programCohort.getProgramCohortCollections());

        programCohortRuleRepository.deleteAll(programCohort.getProgramCohortRules());

        programCohortRepository.delete(programCohort);
    }

    @Override
    public void removeCollectionFromCohort(Long id) {

        if (Objects.isNull(id)) {
            throw new BadRequestAlertException("Id can not be null.", "ProgramCohort", Constants.ID_NOT_FOUND);
        }

        Optional<ProgramCohortCollection> programCohortCollectionOptional = programCohortCollectionRepository.findById(id);
        if (!programCohortCollectionOptional.isPresent()) {
            throw new BadRequestAlertException("Collection not found.", "ProgramCohort", Constants.ID_NOT_FOUND);
        }

        ProgramCohortCollection programCohortCollection = programCohortCollectionOptional.get();

        ProgramCohort programCohort = programCohortCollection.getProgramCohort();

        Program program = programCohort.getProgram();

        Boolean hasProgramActivated = isNotEditedCohort(program);

        if (hasProgramActivated) {
            throw new BadRequestAlertException("Program can not change when program have activated before.", "PROGRAM", Constants.PROGRAM_HAS_ACTIVATED);
        }

        programCohortCollectionRepository.delete(programCohortCollection);
    }

    private Boolean isNotEditedCohort(Program program) {

        List<ProgramTrackingStatus> trackingActiveStatus = programTrackingStatusRepository.findByProgramIdAndProgramStatus(program.getId(), ProgramStatus.Active.name());

        List<Long> cohortIds = program.getProgramCohorts().stream().map(ProgramCohort::getId).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(trackingActiveStatus)) {
            long count = programUserCohortRepository.countByProgramCohortIdIn(cohortIds);
            return count > 0;
        }
        return false;
//        throw new BadRequestAlertException("Program can not change when program have activated before.", "PROGRAM", Constants.PROGRAM_TYPE_ERR);
    }

    @Override
    public List<ProgramCohortExtensionDto> save(List<ProgramCohortExtensionDto> programCohortDtoList) {

        long countDefaultCohorts = programCohortDtoList.stream()
            .filter(c -> Objects.nonNull(c.getIsDefault()) && c.getIsDefault()).count();

        if (countDefaultCohorts > 1) {
            throw new BadRequestAlertException("Only one cohort may be a Default Cohort", "Cohorts", Constants.COHORT_ERROR_DEFAULT);
        }

        // validate collection assigned
        for (ProgramCohortExtensionDto dto : programCohortDtoList) {
            if (!dto.getIsDefault()) {
                if (CollectionUtils.isEmpty(dto.getProgramCohortCollections())) {
                    throw  new BadRequestAlertException("Each cohort must have at least one collection assigned", "Program", Constants.COHORT_ERROR_DEFAULT);
                }

                if (CollectionUtils.isEmpty(dto.getProgramCohortRules())) {
                    throw  new BadRequestAlertException("Select at least one Data input.", "Program", Constants.COHORT_ERROR_DEFAULT);
                }

                long countDataInputs = dto.getProgramCohortRules()
                    .stream()
                    .filter(r -> Objects.isNull(r.getDataInputType()))
                    .count();

                if (countDataInputs > 0) {
                    throw  new BadRequestAlertException("Select at least one Data input.", "Program", Constants.COHORT_ERROR_DEFAULT);
                }

                long countRulesNull = dto.getProgramCohortRules()
                    .stream()
                    .filter(r -> Objects.isNull(r.getRules()))
                    .count();
                
                if (countRulesNull > 0) {
                    throw  new BadRequestAlertException("Missing criteria for one or more statements.", "Program", Constants.COHORT_ERROR_DEFAULT);
                }
            }

        }

        List<ProgramCohortExtensionDto> result = new ArrayList<>();
        for (ProgramCohortExtensionDto dto : programCohortDtoList) {
            ProgramCohortExtensionDto programCohortExtensionDto = save(dto);
            result.add(programCohortExtensionDto);
        }
        return result;
    }

    @Override
    public List<ProgramCohortExtensionDto> getAllProgramCohortByProgramId(Long programId) {

        if (Objects.isNull(programId)) {
            throw new BadRequestAlertException("Program Id  can not be null.", "ProgramCohort", Constants.ID_NOT_FOUND);
        }

        Program program = programRepository.getOne(programId);

        Boolean hasProgramActivated = isNotEditedCohort(program);

        Set<ProgramCohort> programCohorts = program.getProgramCohorts();

        List<ProgramCohortExtensionDto> result = new ArrayList<>();

        for (ProgramCohort programCohort : programCohorts) {

            List<ProgramCohortCollection> programCohortCollections = programCohortCollectionRepository.findAllByProgramCohort(programCohort);

            List<ProgramCohortRule> programCohortRules = programCohortRuleRepository.findAllByProgramCohort(programCohort);

            List<ProgramCohortCollectionDTO> programCohortCollectionDtoList = programCohortCollectionMapper.toDto(new ArrayList<>(programCohortCollections));

            List<ProgramCohortRuleDTO> programCohortRuleDTOList = programCohortRuleMapper.toDto(new ArrayList<>(programCohortRules));

            for (ProgramCohortCollectionDTO programCohortCollection : programCohortCollectionDtoList) {
                ProgramCollection collection = programCollectionRepository.getOne(programCohortCollection.getProgramCollectionId());
                programCohortCollection.setProgramCollectionName(collection.getDisplayName());
            }

            ProgramCohortExtensionDto dto = ProgramCohortExtensionDto.builder()
                .cohortName(programCohort.getCohortName())
                .createdDate(programCohort.getCreatedDate())
                .modifiedDate(programCohort.getModifiedDate())
                .isDefault(programCohort.isIsDefault())
                .id(programCohort.getId())
                .cohortDescription(programCohort.getCohortDescription())
                .programId(programCohort.getProgram().getId())
                .programCohortCollections(programCohortCollectionDtoList)
                .programCohortRules(programCohortRuleDTOList)
                .isNotEditable(hasProgramActivated)
                .build();

            result.add(dto);
        }

        return  result;
    }
}
