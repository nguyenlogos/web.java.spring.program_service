package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.adp.repository.AmpUserActivityProgressRepository;
import aduro.basic.programservice.ap.PathAdapter;
import aduro.basic.programservice.ap.dto.CheckDeletePathDto;
import aduro.basic.programservice.ap.dto.PathDTO;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.ProgramCollectionExtensionRepository;
import aduro.basic.programservice.service.ProgramCollectionExtensionService;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.ProgramCollectionContentMapper;
import aduro.basic.programservice.service.mapper.ProgramCollectionMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ProgramCollection}.
 */
@Service
@Transactional
public class ProgramCollectionExtensionServiceImpl implements ProgramCollectionExtensionService {

    private final Logger log = LoggerFactory.getLogger(ProgramCollectionExtensionServiceImpl.class);

    private final ProgramCollectionMapper programCollectionMapper;

    private final ProgramRepository programRepository;

    private final ProgramCollectionExtensionRepository programCollectionRepository;

    private final ProgramCollectionContentMapper programCollectionContentMapper;

    private final ProgramCollectionContentRepository programCollectionContentRepository;

    private final ProgramTrackingStatusRepository programTrackingStatusRepository;

    private final ProgramCohortCollectionRepository programCohortCollectionRepository;

    private final ProgramUserCohortRepository programUserCohortRepository;

    private final ProgramUserRepository programUserRepository;

    private final ClientProgramRepository clientProgramRepository;

    private final AmpUserActivityProgressRepository ampUserActivityProgressRepository;

    private final PathAdapter pathAdapter;

    private static final String ENTITY_NAME = "programCollection";

    public ProgramCollectionExtensionServiceImpl(ProgramCollectionExtensionRepository programCollectionRepository, ProgramCollectionMapper programCollectionMapper, ProgramRepository programRepository, ProgramCollectionContentMapper programCollectionContentMapper, ProgramCollectionContentRepository programCollectionContentRepository, ProgramTrackingStatusRepository programTrackingStatusRepository, ProgramCohortCollectionRepository programCohortCollectionRepository, ProgramUserCohortRepository programUserCohortRepository, ProgramUserRepository programUserRepository, ClientProgramRepository clientProgramRepository, AmpUserActivityProgressRepository ampUserActivityProgressRepository, PathAdapter pathAdapter) {
        this.programCollectionRepository = programCollectionRepository;
        this.programCollectionMapper = programCollectionMapper;
        this.programRepository = programRepository;
        this.programCollectionContentMapper = programCollectionContentMapper;
        this.programCollectionContentRepository = programCollectionContentRepository;
        this.programTrackingStatusRepository = programTrackingStatusRepository;
        this.programCohortCollectionRepository = programCohortCollectionRepository;
        this.programUserCohortRepository = programUserCohortRepository;
        this.programUserRepository = programUserRepository;
        this.clientProgramRepository = clientProgramRepository;
        this.ampUserActivityProgressRepository = ampUserActivityProgressRepository;
        this.pathAdapter = pathAdapter;
    }


    @Override
    public ProgramCollectionExtensionDto save(ProgramCollectionExtensionDto programCollectionExtensionDto) {

        if (programCollectionExtensionDto.getProgramId() == null || programCollectionExtensionDto.getProgramId() < 0) {
            throw new BadRequestAlertException("Program must assign to collection.", ENTITY_NAME, Constants.PROGRAM_NOT_FOUND);
        }

        if (CollectionUtils.isEmpty(programCollectionExtensionDto.getProgramCollectionContents()) || programCollectionExtensionDto.getProgramCollectionContents().size() > 20) {
            throw new BadRequestAlertException("Minimum 1 item required and Maximum 20 items exceeded in collection.", ENTITY_NAME, Constants.PROGRAM_NOT_FOUND);
        }

        Optional<Program> programOptional = programRepository.findById(programCollectionExtensionDto.getProgramId());

        if (!programOptional.isPresent()) {
            throw new BadRequestAlertException("Program not found.", ENTITY_NAME, Constants.PROGRAM_NOT_FOUND);
        }

        //create program collection
        boolean isCreate = programCollectionExtensionDto.getId() == null || programCollectionExtensionDto.getId() == 0;

        ProgramCollection programCollection = null;

        List<ProgramCollectionContent> programCollectionContentEntities = new ArrayList<>();

        if (isCreate) {
            programCollection = new ProgramCollection()
                .createdDate(Instant.now())
                .modifiedDate(Instant.now())
                .displayName(programCollectionExtensionDto.getDisplayName())
                .longDescription(programCollectionExtensionDto.getLongDescription())
                .program(programOptional.get());

            if (!CollectionUtils.isEmpty(programCollectionExtensionDto.getProgramCollectionContents())) {
                List<ProgramCollectionContentDTO> programCollectionContents = programCollectionExtensionDto.getProgramCollectionContents();
                programCollectionContentEntities = programCollectionContentMapper.toEntity(programCollectionContents);
            }
        } else {


            Optional<ProgramCollection> programCollectionOptional = programCollectionRepository.findById(programCollectionExtensionDto.getId());

            if (programCollectionOptional.isPresent()) {
                programCollection = programCollectionOptional.get();
                programCollection.setDisplayName(programCollectionExtensionDto.getDisplayName());
                programCollection.setLongDescription(programCollectionExtensionDto.getLongDescription());
                programCollection.setModifiedDate(Instant.now());
            }

            if (programCollection == null) {
                throw new BadRequestAlertException("Program collection not existed.", ENTITY_NAME, Constants.COLLECTION_NOT_FOUND);
            }

            Program program = programCollection.getProgram();
            // find items need to delete

            // list need to save
            List<ProgramCollectionContentDTO> programCollectionContents = programCollectionExtensionDto.getProgramCollectionContents();
            programCollectionContentEntities = programCollectionContentMapper.toEntity(programCollectionContents);

            //list current in db
            List<ProgramCollectionContent> dbContents = programCollectionContentRepository.findByProgramCollection(programCollection);


            List<ProgramCollectionContent> deleteItems = new ArrayList<>();

            for (ProgramCollectionContent obj : dbContents) {
                if (!programCollectionContentEntities.stream()
                    .map(ProgramCollectionContent::getId)
                    .collect(Collectors.toList()).contains(obj.getId())) {
                    deleteItems.add(obj);
                }
            }

            //delete not need
            for (ProgramCollectionContent deleteItem : deleteItems) {

                //check delete items
                if (hasProgramActive(program)) {
                    String error = "";
                    if (deleteItem.getContentType().equalsIgnoreCase(Constants.ACTIVITY)) {
                        long countUsersProgresses = ampUserActivityProgressRepository.countByCustomActivityId(Long.valueOf(deleteItem.getItemId()));
                        if (countUsersProgresses > 0) {
                            error = String.format("User have joined this activity %s", deleteItem.getItemName());
                            throw new BadRequestAlertException(error, ENTITY_NAME, Constants.PROGRAM_HAS_ACTIVATED);
                        }
                    } else if(deleteItem.getContentType().equalsIgnoreCase(Constants.PATH)) {
                        CheckPathInProgressRequest pathDTO = new CheckPathInProgressRequest(Long.valueOf(deleteItem.getItemId()));
                        try {
                            CheckDeletePathDto checkDeletePathDto = pathAdapter.verifyPathById(pathDTO);
                            if (!checkDeletePathDto.isIsRemovable()) {
                                error = String.format("User have joined this path %s", deleteItem.getItemName());
                                throw new BadRequestAlertException(error, ENTITY_NAME, Constants.PROGRAM_HAS_ACTIVATED);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            error = String.format("API check Path error: %s", deleteItem.getItemName());
                            throw new BadRequestAlertException(error, ENTITY_NAME, Constants.PROGRAM_HAS_ACTIVATED);
                        }
                    }
                }
                programCollectionContentRepository.delete(deleteItem);
            }

        }

        programCollection = programCollectionRepository.save(programCollection);

        programCollectionExtensionDto.setId(programCollection.getId());

        for (ProgramCollectionContent entity : programCollectionContentEntities) {
            if (Objects.isNull(entity.getId())) {
                //create new
                entity.setCreatedDate(Instant.now());
            } else {
                Optional<ProgramCollectionContent> programCollectionContentOptional = programCollectionContentRepository.findById(entity.getId());
                if (programCollectionContentOptional.isPresent()) {

                    ProgramCollectionContent programCollectionContent = programCollectionContentOptional.get();
                    entity.setCreatedDate(programCollectionContent.getCreatedDate());
                }
            }
            entity.setProgramCollection(programCollection);
            entity.setModifiedDate(Instant.now());
        }

        programCollectionContentEntities = programCollectionContentRepository.saveAll(programCollectionContentEntities);

        programCollectionExtensionDto.setProgramCollectionContents(programCollectionContentMapper.toDto(programCollectionContentEntities));

        programCollectionExtensionDto.setCreatedDate(programCollection.getCreatedDate());
        programCollectionExtensionDto.setModifiedDate(programCollection.getModifiedDate());
        return programCollectionExtensionDto;
    }

    @Override
    public Optional<ProgramCollectionExtensionWithCompletionDto> findOneWithCompletions(Long id, String participantId, String clientId) {
        if (id == null || id < 0) {
            throw  new BadRequestAlertException("Id can not null.", ENTITY_NAME, Constants.ID_NOT_FOUND);
        } else if (participantId == null) {
            throw  new BadRequestAlertException("participantId can not null.", ENTITY_NAME, Constants.ID_NOT_FOUND);
        } else if (clientId == null) {
            throw  new BadRequestAlertException("clientId can not null.", ENTITY_NAME, Constants.ID_NOT_FOUND);
        }

        Optional<ProgramCollection> programCollectionOptional = programCollectionRepository.findById(id);

        if (!programCollectionOptional.isPresent()) {
            throw  new BadRequestAlertException("Program collection can not null.", ENTITY_NAME, Constants.COLLECTION_NOT_FOUND);
        }

        AtomicReference<Integer> numberOfCompletion = new AtomicReference<>();
        ArrayList<String> status = new ArrayList<>();
        status.add(ProgramStatus.Active.toString());
        Optional<ClientProgram> cp = clientProgramRepository.findByClientIdAndProgramQaVerifyAndProgramStatusIn(clientId, false, status);

        try {
            Optional<Program> p = programRepository.findOneWithEagerRelationships(cp.get().getProgramId());
            Optional<ProgramUser> user = programUserRepository.findProgramUserByParticipantIdAndProgramId(participantId, p.get().getId());
            List<ProgramUserCohort> programUserCohorts = programUserCohortRepository.getByProgramUserId(user.get().getId());
            List<ProgramCohortCollection> programCohortCollections = programCohortCollectionRepository.findAllByProgramCollectionId(id);

            if (!programUserCohorts.isEmpty() && !programCohortCollections.isEmpty()) {
                programUserCohorts.forEach(programUserCohort -> programCohortCollections.forEach(programCohortCollection -> {
                    if (programUserCohort.getProgramCohort().getId().equals(programCohortCollection.getProgramCohort().getId())) {
                        numberOfCompletion.set(programCohortCollection.getRequiredCompletion());
                    }
                }));
            } else {
                numberOfCompletion.set(0);
            }
        } catch (Exception e) {
            numberOfCompletion.set(0);
        }

        ProgramCollection programCollection = programCollectionOptional.get();

        boolean hasContents = !programCollection.getProgramCollectionContents().isEmpty();

        List<ProgramCollectionContentDTO> programCollectionContents = hasContents ? programCollectionContentMapper.toDto(new ArrayList<>(programCollection.getProgramCollectionContents())) : new ArrayList<>();

        ProgramCollectionExtensionWithCompletionDto dto = ProgramCollectionExtensionWithCompletionDto.builder()
            .createdDate(programCollection.getCreatedDate())
            .modifiedDate(programCollection.getModifiedDate())
            .displayName(programCollection.getDisplayName())
            .longDescription(programCollection.getLongDescription())
            .id(programCollection.getId())
            .programId(programCollection.getProgram().getId())
            .numberOfCompletion(numberOfCompletion.get())
            .programCollectionContents(programCollectionContents)
            .build();

        return Optional.of(dto);
    }

    @Override
    public Optional<ProgramCollectionExtensionDto> findOne(Long id) {
        if (id == null || id < 0) {
            throw  new BadRequestAlertException("Id can not null.", ENTITY_NAME, Constants.ID_NOT_FOUND);
        }

        Optional<ProgramCollection> programCollectionOptional = programCollectionRepository.findById(id);

        if (!programCollectionOptional.isPresent()) {
            throw  new BadRequestAlertException("Program collection can not null.", ENTITY_NAME, Constants.COLLECTION_NOT_FOUND);
        }

        ProgramCollection programCollection = programCollectionOptional.get();

        boolean hasContents = !programCollection.getProgramCollectionContents().isEmpty();

        List<ProgramCollectionContentDTO> programCollectionContents = hasContents ? programCollectionContentMapper.toDto(new ArrayList<>(programCollection.getProgramCollectionContents())) : new ArrayList<>();

        ProgramCollectionExtensionDto dto = ProgramCollectionExtensionDto.builder()
            .createdDate(programCollection.getCreatedDate())
            .modifiedDate(programCollection.getModifiedDate())
            .displayName(programCollection.getDisplayName())
            .longDescription(programCollection.getLongDescription())
            .id(programCollection.getId())
            .programId(programCollection.getProgram().getId())
            .programCollectionContents(programCollectionContents)
            .build();

        return Optional.of(dto);
    }

    @Override
    public DeleteCollectionRequestDto delete(DeleteCollectionRequestDto dto) {


        if (Objects.isNull(dto.getId())) {
            throw  new BadRequestAlertException("Id can not null.", ENTITY_NAME, Constants.ID_NOT_FOUND);
        }

        Optional<ProgramCollection> programCollectionOptional = programCollectionRepository.findById(dto.getId());

        if (!programCollectionOptional.isPresent()) {
            throw  new BadRequestAlertException("Program collection can not null.", ENTITY_NAME, Constants.COLLECTION_NOT_FOUND);
        }

        ProgramCollection programCollection = programCollectionOptional.get();

        Program program = programCollection.getProgram();

        if (hasProgramActive(program)) {
            throw new BadRequestAlertException("Program collection can not be removed.", ENTITY_NAME, Constants.PROGRAM_HAS_ACTIVATED);
        }

        List<ProgramCohortCollection> collectionAssignedCohorts = programCohortCollectionRepository.findAllByProgramCollection(programCollection);

        boolean isDeleteParent = false;

        if (Objects.isNull(dto.getIsDeleteConfirmation())) {

            if (CollectionUtils.isEmpty(collectionAssignedCohorts)) {

                dto.setSuccess(true);

                isDeleteParent = true;

            } else {
                dto.setIsDeleteConfirmation(false);
                dto.setCohorts(collectionAssignedCohorts.stream().map(c -> c.getProgramCohort().getCohortName()).collect(Collectors.toList()));
                dto.setSuccess(false);
            }

        } else if (dto.getIsDeleteConfirmation()) {

            //delete
            programCohortCollectionRepository.deleteAll(collectionAssignedCohorts);

            isDeleteParent = true;
        }

        if (isDeleteParent) {

            boolean hasContents = !programCollection.getProgramCollectionContents().isEmpty();

            if (hasContents) {
                programCollectionContentRepository.deleteAll(programCollection.getProgramCollectionContents());
            }
            programCollectionRepository.delete(programCollection);

            dto.setSuccess(true);
        }

        return dto;
    }

    private boolean hasProgramActive(Program program) {
        List<ProgramTrackingStatus> trackingActiveStatus = programTrackingStatusRepository.findByProgramIdAndProgramStatus(program.getId(), ProgramStatus.Active.name());
        return !CollectionUtils.isEmpty(trackingActiveStatus);
    }


    @Override
    public List<ProgramCollectionExtensionDto> getByProgramId(Long programId) {

        if (Objects.isNull(programId)) {
            throw  new BadRequestAlertException("ProgramId can not null.", ENTITY_NAME, Constants.ID_NOT_FOUND);
        }

        List<ProgramCollectionExtensionDto> result = new ArrayList<>();

        List<ProgramCollection> programCollections = programCollectionRepository.findAllByProgramId(programId);

        for (ProgramCollection programCollection : programCollections) {

            List<ProgramCollectionContent> programCollectionContents = programCollectionContentRepository.findByProgramCollection(programCollection);

            ProgramCollectionExtensionDto dto = ProgramCollectionExtensionDto.builder()
                .id(programCollection.getId())
                .longDescription(programCollection.getLongDescription())
                .displayName(programCollection.getDisplayName())
                .createdDate(programCollection.getCreatedDate())
                .modifiedDate(programCollection.getModifiedDate())
                .programId(programId)
                .programCollectionContents(programCollectionContentMapper.toDto(new ArrayList<>(programCollectionContents)))
                .build();

            result.add(dto);
        }

        return result;
    }

    @Override
    public List<ProgramCollectionExtensionDto> saveList(List<ProgramCollectionExtensionDto> collections) {
        List<ProgramCollectionExtensionDto> res = new ArrayList<>();
        for (ProgramCollectionExtensionDto collection : collections) {
            ProgramCollectionExtensionDto dto = save(collection);
            res.add(dto);
        }
        return res;
    }


    @Override
    public RemovableCollectionContentDto checkDeleteItemIdInCollection(RemovableCollectionContentDto dto) {

        dto.setRemovable(true);

        if (dto.getContentType().equalsIgnoreCase(Constants.ACTIVITY)) {
            long countUsersProgresses = ampUserActivityProgressRepository.countByCustomActivityId(Long.valueOf(dto.getItemId()));
            if (countUsersProgresses > 0) {
                dto.setRemovable(false);
            }
        } else if(dto.getContentType().equalsIgnoreCase(Constants.PATH)) {
            CheckPathInProgressRequest request = CheckPathInProgressRequest.builder()
                .pathId(Long.valueOf(dto.getItemId()))
                .build();
            try {
                CheckDeletePathDto checkDeletePathDto = pathAdapter.verifyPathById(request);
                if (!checkDeletePathDto.isIsRemovable()) {
                   dto.setRemovable(false);
                }
            } catch (Exception e) {
                dto.setRemovable(false);
            }
        }

        return dto;
    }
}
