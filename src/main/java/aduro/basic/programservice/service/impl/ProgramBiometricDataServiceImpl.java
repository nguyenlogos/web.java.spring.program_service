package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramBiometricDataService;
import aduro.basic.programservice.domain.ProgramBiometricData;
import aduro.basic.programservice.repository.ProgramBiometricDataRepository;
import aduro.basic.programservice.service.dto.ProgramBiometricDataDTO;
import aduro.basic.programservice.service.mapper.ProgramBiometricDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramBiometricData}.
 */
@Service
@Transactional
public class ProgramBiometricDataServiceImpl implements ProgramBiometricDataService {

    private final Logger log = LoggerFactory.getLogger(ProgramBiometricDataServiceImpl.class);

    private final ProgramBiometricDataRepository programBiometricDataRepository;

    private final ProgramBiometricDataMapper programBiometricDataMapper;

    public ProgramBiometricDataServiceImpl(ProgramBiometricDataRepository programBiometricDataRepository, ProgramBiometricDataMapper programBiometricDataMapper) {
        this.programBiometricDataRepository = programBiometricDataRepository;
        this.programBiometricDataMapper = programBiometricDataMapper;
    }

    /**
     * Save a programBiometricData.
     *
     * @param programBiometricDataDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramBiometricDataDTO save(ProgramBiometricDataDTO programBiometricDataDTO) {
        log.debug("Request to save ProgramBiometricData : {}", programBiometricDataDTO);
        ProgramBiometricData programBiometricData = programBiometricDataMapper.toEntity(programBiometricDataDTO);
        programBiometricData = programBiometricDataRepository.save(programBiometricData);
        return programBiometricDataMapper.toDto(programBiometricData);
    }

    /**
     * Get all the programBiometricData.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramBiometricDataDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramBiometricData");
        return programBiometricDataRepository.findAll(pageable)
            .map(programBiometricDataMapper::toDto);
    }


    /**
     * Get one programBiometricData by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramBiometricDataDTO> findOne(Long id) {
        log.debug("Request to get ProgramBiometricData : {}", id);
        return programBiometricDataRepository.findById(id)
            .map(programBiometricDataMapper::toDto);
    }

    /**
     * Delete the programBiometricData by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramBiometricData : {}", id);
        programBiometricDataRepository.deleteById(id);
    }
}
