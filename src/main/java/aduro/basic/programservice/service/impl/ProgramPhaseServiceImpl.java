package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.CustomActivityPhaseDto;
import aduro.basic.programservice.ap.dto.CustomActivityPhaseResultDto;
import aduro.basic.programservice.ap.dto.DeleteCustomActivityDto;
import aduro.basic.programservice.domain.ProgramActivity;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.domain.ProgramPhase;
import aduro.basic.programservice.repository.ProgramActivityRepository;
import aduro.basic.programservice.repository.ProgramLevelActivityRepository;
import aduro.basic.programservice.repository.ProgramPhaseRepository;
import aduro.basic.programservice.service.ProgramPhaseService;
import aduro.basic.programservice.service.dto.ProgramPhaseDTO;
import aduro.basic.programservice.service.mapper.ProgramPhaseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ProgramPhase}.
 */
@Service
@Transactional
public class ProgramPhaseServiceImpl implements ProgramPhaseService {

    private final Logger log = LoggerFactory.getLogger(ProgramPhaseServiceImpl.class);

    private static final String ERROR_DELETE_PHASE_MESSAGE = "Can not delete program phase";
    private static final String ERROR_INVALID_DELETE_ACTIVITY = "Activity cannot be removed because there are users currently enrolled this activity.";

    private final ProgramPhaseRepository programPhaseRepository;

    private final ProgramPhaseMapper programPhaseMapper;

    @Autowired
    private ProgramActivityRepository programActivityRepository;

    @Autowired
    private ProgramLevelActivityRepository programLevelActivityRepository;

    private CustomActivityAdapter customActivityAdapter;

    public ProgramPhaseServiceImpl(ProgramPhaseRepository programPhaseRepository, ProgramPhaseMapper programPhaseMapper, CustomActivityAdapter customActivityAdapter) {
        this.programPhaseRepository = programPhaseRepository;
        this.programPhaseMapper = programPhaseMapper;
        this.customActivityAdapter = customActivityAdapter;
    }

    /**
     * Save a programPhase.
     *
     * @param programPhaseDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramPhaseDTO save(ProgramPhaseDTO programPhaseDTO) {
        log.debug("Request to save ProgramPhase : {}", programPhaseDTO);
        ProgramPhaseDTO updateProgramPhaseDTO = null;
        CustomActivityPhaseResultDto customActivityPhaseResultDto = null;
        ProgramPhase programPhase;
        try {
            programPhase = programPhaseMapper.toEntity(programPhaseDTO);
            programPhase = programPhaseRepository.save(programPhase);

            List<ProgramActivity> programActivities = programActivityRepository.findProgramActivitiesByProgramPhaseId(programPhase.getId());
            if (!CollectionUtils.isEmpty(programActivities)) {
                List<CustomActivityPhaseDto> customActivityPhaseDtoList = new ArrayList<>();
                programActivities.forEach(pa -> {
                    CustomActivityPhaseDto custItem = new CustomActivityPhaseDto();
                    custItem.setId(pa.getActivityId());
                    custItem.setStartDate(programPhaseDTO.getStartDate());
                    custItem.setEndDate(programPhaseDTO.getEndDate());

                    customActivityPhaseDtoList.add(custItem);
                });
                customActivityPhaseResultDto = customActivityAdapter.updateDateCustomActivities(customActivityPhaseDtoList);

            }

            updateProgramPhaseDTO = programPhaseMapper.toDto(programPhase);
            if (customActivityPhaseResultDto != null) {
                updateProgramPhaseDTO.setCustomActivities(customActivityPhaseResultDto.getCustomActivities());
            }

        } catch (Exception e) {
            log.error("Error Request to save ProgramPhase : {}", programPhaseDTO);
        }

        return updateProgramPhaseDTO;
    }

    /**
     * Get all the programPhases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramPhaseDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramPhases");
        return programPhaseRepository.findAll(pageable)
            .map(programPhaseMapper::toDto);
    }


    /**
     * Get one programPhase by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramPhaseDTO> findOne(Long id) {
        log.debug("Request to get ProgramPhase : {}", id);
        return programPhaseRepository.findById(id)
            .map(programPhaseMapper::toDto);
    }

    /**
     * Delete the programPhase by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public String delete(Long id) {
        log.debug("Request to delete ProgramPhase : {}", id);

        ProgramPhase programPhase = programPhaseRepository.getOne(id);

        if(programPhase == null ) {
            return ERROR_DELETE_PHASE_MESSAGE;
        }

        List<ProgramLevel> programLevels = new ArrayList<>(programPhase.getProgram().getProgramLevels());
        List<Long> levelIds = programLevels.stream().map(l->l.getId()).collect(Collectors.toList());

        List<ProgramActivity>  programActivities = programActivityRepository.findProgramActivitiesByProgramPhaseId(id);
        List<Long> ids = programActivities.stream().map(l->l.getId()).collect(Collectors.toList());
        List<ProgramActivity> programActivityList = programActivityRepository.findByIdIn(ids);

        List<String> customActivitiesIds = programActivityList.stream()
            .map(pa -> pa.getActivityId()).collect(Collectors.toList());

        // check custom activities do not have any joined users
        boolean invalidToDeleteCustomActivity = customActivityAdapter.invalidDeleteUserJoinedCustomActivity(customActivitiesIds);
        if (invalidToDeleteCustomActivity) {
            return ERROR_INVALID_DELETE_ACTIVITY;
        }

        try {
            if (!CollectionUtils.isEmpty(programActivityList)) {
                // delete activity is assigned to level
                for (ProgramActivity programActivity : programActivityList) {
                    programLevelActivityRepository.deleteProgramLevelActivitiesByActivityIdAndProgramLevelIdIn(programActivity.getActivityId(), levelIds);
                }

                // delete program activities
                programActivityRepository.deleteAll(programActivityList);

                DeleteCustomActivityDto deleteCustomActivityDto = new DeleteCustomActivityDto();
                deleteCustomActivityDto.setProgramId(programPhase.getProgram().getId());
                deleteCustomActivityDto.setCustomActivityIds(customActivitiesIds);

                // delete custom activities in Platform
                customActivityAdapter.deleteCustomActivitiesInProgram(deleteCustomActivityDto);
            }

            // delete program phase
            programPhaseRepository.deleteById(id);
        } catch (Exception e) {
            log.error("Request to delete deleteBulkProgramActivities : {}", e.getMessage());
        }
        return "";
    }
}
