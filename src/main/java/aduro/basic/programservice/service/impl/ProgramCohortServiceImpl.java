package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramCohortService;
import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.repository.ProgramCohortRepository;
import aduro.basic.programservice.service.dto.ProgramCohortDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramCohort}.
 */
@Service
@Transactional
public class ProgramCohortServiceImpl implements ProgramCohortService {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortServiceImpl.class);

    private final ProgramCohortRepository programCohortRepository;

    private final ProgramCohortMapper programCohortMapper;

    public ProgramCohortServiceImpl(ProgramCohortRepository programCohortRepository, ProgramCohortMapper programCohortMapper) {
        this.programCohortRepository = programCohortRepository;
        this.programCohortMapper = programCohortMapper;
    }

    /**
     * Save a programCohort.
     *
     * @param programCohortDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramCohortDTO save(ProgramCohortDTO programCohortDTO) {
        log.debug("Request to save ProgramCohort : {}", programCohortDTO);
        ProgramCohort programCohort = programCohortMapper.toEntity(programCohortDTO);
        programCohort = programCohortRepository.save(programCohort);
        return programCohortMapper.toDto(programCohort);
    }

    /**
     * Get all the programCohorts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramCohortDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramCohorts");
        return programCohortRepository.findAll(pageable)
            .map(programCohortMapper::toDto);
    }


    /**
     * Get one programCohort by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramCohortDTO> findOne(Long id) {
        log.debug("Request to get ProgramCohort : {}", id);
        return programCohortRepository.findById(id)
            .map(programCohortMapper::toDto);
    }

    /**
     * Delete the programCohort by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramCohort : {}", id);
        programCohortRepository.deleteById(id);
    }
}
