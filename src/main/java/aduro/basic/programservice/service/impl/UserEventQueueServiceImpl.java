package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.config.Settings;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.ProgramUserLevelProgressRepositoryExtension;
import aduro.basic.programservice.service.UserEventQueueService;
import aduro.basic.programservice.service.businessrule.UserIncentiveBusinessRule;
import aduro.basic.programservice.service.dto.ActivityEventQueueStatus;
import aduro.basic.programservice.service.dto.IncentiveStatusEnum;
import aduro.basic.programservice.service.dto.UserEventQueueDTO;
import aduro.basic.programservice.service.mapper.UserEventQueueMapper;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link UserEventQueue}.
 */
@Service
@Transactional
public class UserEventQueueServiceImpl implements UserEventQueueService {

    private final Logger log = LoggerFactory.getLogger(UserEventQueueServiceImpl.class);

    private final static int MAX_USER_EVENT_QUEUE_ATTEMPT = 4;

    private final UserEventQueueRepository userEventQueueRepository;

    private final UserEventQueueMapper userEventQueueMapper;

    private final UserIncentiveBusinessRule userIncentiveBusinessRule;

    private ProgramUserRepository programUserRepository;

    private ProgramRepository programRepository;

    private ApplicationProperties applicationProperties;

    private SupportAdditionalUserPointQueueRepository supportAdditionalUserPointQueueRepository;

    private UserEventRepository userEventRepository;

    private UserRewardRepository userRewardRepository;

    private ProgramUserLevelProgressRepositoryExtension programUserLevelProgressRepository;

    private ProgramActivityRepository programActivityRepository;

    private ProgramLevelRepository programLevelRepository;

    @Autowired
    private Settings settings;

    public UserEventQueueServiceImpl(UserEventQueueRepository userEventQueueRepository,
                                     UserEventQueueMapper userEventQueueMapper,
                                     UserIncentiveBusinessRule userIncentiveBusinessRule,
                                     SupportAdditionalUserPointQueueRepository supportAdditionalUserPointQueueRepository,
                                     UserEventRepository userEventRepository,
                                     ApplicationProperties applicationProperties,
                                     ProgramRepository programRepository,
                                     ProgramUserRepository programUserRepository,
                                     UserRewardRepository userRewardRepository,
                                     ProgramUserLevelProgressRepositoryExtension programUserLevelProgressRepository,
                                     ProgramActivityRepository programActivityRepository,
                                     ProgramLevelRepository programLevelRepository
    ) {
        this.userEventQueueRepository = userEventQueueRepository;
        this.userEventQueueMapper = userEventQueueMapper;
        this.userIncentiveBusinessRule = userIncentiveBusinessRule;
        this.userEventRepository = userEventRepository;
        this.supportAdditionalUserPointQueueRepository = supportAdditionalUserPointQueueRepository;
        this.applicationProperties = applicationProperties;
        this.programRepository = programRepository;
        this.programUserRepository = programUserRepository;
        this.userRewardRepository = userRewardRepository;
        this.programUserLevelProgressRepository = programUserLevelProgressRepository;
        this.programActivityRepository = programActivityRepository;
        this.programLevelRepository = programLevelRepository;
    }

    /**
     * Save a userEventQueue.
     *
     * @param userEventQueueDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public UserEventQueueDTO save(UserEventQueueDTO userEventQueueDTO) {
        log.debug("Request to save UserEventQueue : {}", userEventQueueDTO);
        UserEventQueue userEventQueue = userEventQueueMapper.toEntity(userEventQueueDTO);
        userEventQueue = userEventQueueRepository.save(userEventQueue);
        return userEventQueueMapper.toDto(userEventQueue);
    }

    /**
     * Get all the userEventQueues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserEventQueueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserEventQueues");
        return userEventQueueRepository.findAll(pageable)
            .map(userEventQueueMapper::toDto);
    }


    /**
     * Get one userEventQueue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserEventQueueDTO> findOne(Long id) {
        log.debug("Request to get UserEventQueue : {}", id);
        return userEventQueueRepository.findById(id)
            .map(userEventQueueMapper::toDto);
    }

    /**
     * Delete the userEventQueue by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserEventQueue : {}", id);
        userEventQueueRepository.deleteById(id);
    }

    @Scheduled(cron = "0 */5 * * * *")
    @SchedulerLock(name = "processUserEventQueue")
    public void processUserEventQueue() throws Exception {
        if(!settings.isScronJobEnabled()){
            return;
        }
        log.info("Start schedule for processUserEventQueue: " + Instant.now().toString());
        List<UserEventQueue> results =  userEventQueueRepository.findByEventPointIsNotNullAndExecuteStatusAndAttemptCountLessThan(IncentiveStatusEnum.Retry.toString(), MAX_USER_EVENT_QUEUE_ATTEMPT);

        if (!CollectionUtils.isEmpty(results)) {
            List<Long> programUserIds = results.stream()
                .map(r -> r.getProgramUserId()).collect(Collectors.toList());

            // generate map data for program users
            List<ProgramUser> programUserList = programUserRepository.findByIdIn(programUserIds);
            Map<Long, ProgramUser> programUserMap = new HashMap<>();
            programUserList.forEach(pu -> {
                programUserMap.put(pu.getId(), pu);
            });

            // generate map data for programs
            List<Long> programIds = results.stream()
                .map(r -> r.getProgramId()).collect(Collectors.toList());

            List<Program> programList = programRepository.findByIdIn(programIds);
            Map<Long, Program> programMap = new HashMap<>();
            programList.forEach(p -> {
                programMap.put(p.getId(), p);
            });

            for (UserEventQueue userEventQueue : results) {
                userEventQueue.setAttemptCount(userEventQueue.getAttemptCount() + 1);
                try {
                    ProgramUser programUser = programUserMap.get(userEventQueue.getProgramUserId());

                    Program program = programMap.get(programUser.getProgramId());

                    //calculate point and level
                    BigDecimal totalUserPoint = CommonUtils.getEconomyPointByProgram(program, applicationProperties);

                    if(programUser.getSubgroupId() != null && (programUser.getSubgroupId().equals("undefined") || programUser.getSubgroupId().equals("0")))
                        programUser.setSubgroupId("");

                    userIncentiveBusinessRule.calculatePointAndLevelByCategoryPointAndSubCategoryPoint(
                        userEventQueue,
                        programUser,
                        userEventQueue.getEventCategory(),
                        totalUserPoint,
                        programUser.getSubgroupId()
                    );
                } catch (Exception ex) {
                    log.error("processUserEventQueue error: " + ex.getMessage());
                    userEventQueue.setErrorMessage(ex.getMessage());
                }
            }

            userEventQueueRepository.saveAll(results);
        }
    }

    @Scheduled(cron = "0 */25 * * * *")
    @SchedulerLock(name = "processSupportAdditionalUserPointQueue")
    public void processSupportAdditionalUserPointQueue() throws Exception {
        if(!settings.isScronJobEnabled()){
            return;
        }
        boolean enabledSupportAdditionalUserPointQueue = applicationProperties.getProcessAddUserPointEnabled();
        if (!enabledSupportAdditionalUserPointQueue) {
            return;
        }
        log.info("Start schedule for processSupportAdditionalUserPointQueue: " + Instant.now().toString());

        List<SupportAdditionalUserPointQueue> userPointQueueList = supportAdditionalUserPointQueueRepository.findSupportAdditionalUserPointQueueByExecuteStatus(ActivityEventQueueStatus.New.toString());
        if (CollectionUtils.isEmpty(userPointQueueList)) {
            return;
        }

        Program program = null;
        for (SupportAdditionalUserPointQueue userPointQueue : userPointQueueList) {
            Long programId = userPointQueue.getProgramId();
            if (program == null) {
                Optional<Program> programOptional = programRepository.findById(programId);
                if(!programOptional.isPresent()) {
                    userPointQueue.setExecuteStatus(ActivityEventQueueStatus.Error.toString());
                    userPointQueue.setErrorMessage("Program is not found!");
                    userPointQueue.setUpdatedDate(Instant.now());
                    supportAdditionalUserPointQueueRepository.save(userPointQueue);
                    return;
                }
                program = programOptional.get();
            }

            Optional<ProgramUser> programUserOptional = programUserRepository.findProgramUserByParticipantIdAndProgramId(userPointQueue.getParticipantId(), programId);
            if (!programUserOptional.isPresent()) {
                userPointQueue.setExecuteStatus(ActivityEventQueueStatus.Error.toString());
                userPointQueue.setErrorMessage("Program User is not found!");
                userPointQueue.setUpdatedDate(Instant.now());
                supportAdditionalUserPointQueueRepository.save(userPointQueue);
                return;
            }

            updateSupportAdditionalUserPointQueue(userPointQueue, program, programUserOptional.get());
        }

        supportAdditionalUserPointQueueRepository.saveAll(userPointQueueList);
    }

    @Transactional
    protected UserEvent getUserEvent (SupportAdditionalUserPointQueue userPointQueue, ProgramUser programUser) {
        UserEvent userEvent = null;
        if (userPointQueue.getUserEventId() != null) {
            Optional<UserEvent> userEventOptional = userEventRepository.findById(userPointQueue.getUserEventId());
            if (userEventOptional.isPresent()) {
                userEvent = userEventOptional.get();
            }
        } else {
            userEvent = userEventRepository.findUserEventsByProgramUserIdAndEventIdAndEventCategory(
                programUser.getId(),
                userPointQueue.getEventId(),
                userPointQueue.getEventCategory()
            ).stream().findFirst().orElse(null);
        }
        return userEvent;
    }

    @Transactional
    protected UserEventQueue getUserEventQueue (SupportAdditionalUserPointQueue userPointQueue, ProgramUser programUser) {
        UserEventQueue userEventQueue = null;
        if (userPointQueue.getUserEventQueueId() != null) {
            Optional<UserEventQueue> userEventQueueOptional = userEventQueueRepository.findById(userPointQueue.getUserEventQueueId());
            if (userEventQueueOptional.isPresent()) {
                userEventQueue = userEventQueueOptional.get();
            }
        } else {
            userEventQueue = userEventQueueRepository.findByProgramUserIdAndEventIdAndProgramIdAndEventCode(
                programUser.getId(),
                userPointQueue.getEventId(),
                userPointQueue.getProgramId(),
                userPointQueue.getEventCode()
            ).stream().findFirst().orElse(null);
        }
        return userEventQueue;
    }

    @Transactional
    protected void updateSupportAdditionalUserPointQueue (SupportAdditionalUserPointQueue userPointQueue, Program program, ProgramUser programUser) {
        try {
            BigDecimal totalUserPoint = program.getEconomyPoint();

            UserEvent userEvent = getUserEvent(userPointQueue, programUser);
            if (userEvent == null) {
                userPointQueue.setExecuteStatus(ActivityEventQueueStatus.Error.toString());
                userPointQueue.setErrorMessage("User event is not found!");
                userPointQueue.setUpdatedDate(Instant.now());
                supportAdditionalUserPointQueueRepository.save(userPointQueue);
                return;
            }

            BigDecimal valuePoints = userPointQueue.getAdditionalPoint();

            // get program activity bonus status if event category is BONUS
            if (userPointQueue.getUpdatedEventCategory().equals("BONUS")) {
                ProgramActivity programActivity = programActivityRepository.findByProgramIdAndActivityId(program.getId(), String.valueOf(userPointQueue.getEventId()));
                if (programActivity == null) {
                    userPointQueue.setExecuteStatus(ActivityEventQueueStatus.Error.toString());
                    userPointQueue.setErrorMessage("ProgramActivity is not found!");
                    userPointQueue.setUpdatedDate(Instant.now());
                    supportAdditionalUserPointQueueRepository.save(userPointQueue);
                    return;
                }
                if (programActivity.isBonusPointsEnabled() != null && programActivity.isBonusPointsEnabled()) {
                    BigDecimal totalUserEventBonus = userEventRepository
                        .findUserEventsByProgramUserIdAndEventIdAndEventCategory(programUser.getId(), userEvent.getEventId(), Constants.BONUS)
                        .stream().filter(u -> u.getEventPoint() != null).map(UserEvent::getEventPoint)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);
                    valuePoints = totalUserEventBonus.compareTo(userPointQueue.getAdditionalPoint()) > 0 ? BigDecimal.valueOf(0) : userPointQueue.getAdditionalPoint();
                }
            }

            // update user event with new event code and event category
            BigDecimal addUserEventPoint = userEvent.getEventPoint().add(valuePoints);
            userEvent.setEventPoint(addUserEventPoint);
            userEvent.setEventCode(userPointQueue.getUpdatedEventCode());
            userEvent.setEventCategory(userPointQueue.getUpdatedEventCategory());

            // update user event queue
            UserEventQueue userEventQueue = getUserEventQueue(userPointQueue, programUser);
            if (userEventQueue != null) {
                userEventQueue.setEventPoint(addUserEventPoint);
                userEventQueue.setEventCode(userPointQueue.getUpdatedEventCode());
                userEventQueue.setEventCategory(userPointQueue.getUpdatedEventCategory());
            }

            // update total point
            BigDecimal currentTotalUserPoint = userEventRepository.findAllByProgramUserId(programUser.getId())
                .stream().filter(u -> u.getEventPoint() != null)
                .map(UserEvent::getEventPoint)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

            BigDecimal addTotalUserPoints = currentTotalUserPoint.add(userEvent.getEventPoint());

            if(totalUserPoint.compareTo(addTotalUserPoints)>= 0)
                programUser.setTotalUserPoint(addTotalUserPoints);
            else
                programUser.setTotalUserPoint(totalUserPoint);

            int currentLevel = programUser.getCurrentLevel();
            Optional<ProgramLevel> currentProgramLevelOptional = programLevelRepository.findAllByProgramIdAndCurrentLevel(program.getId(), currentLevel);
            ProgramLevel currentProgramLevel = currentProgramLevelOptional.get();
            boolean upLevel = userIncentiveBusinessRule.isLevelUpByUserEvent(currentProgramLevel, programUser, programUser.getSubgroupId(), userEvent);

            //upgrade if User up level
            // Add all rewards
            if(upLevel) {
                // get rewards
                List<UserReward> userRewards = userIncentiveBusinessRule.doGetRewardByUserEvent(userEvent.getEventId(), currentProgramLevel, programUser, programUser.getSubgroupId(), program);
                if (!CollectionUtils.isEmpty(userRewards)) {
                    List<UserReward> currentUserRewards = userRewardRepository.findByProgramUserIdAndProgramLevel(programUser.getId(), currentLevel);
                    if (CollectionUtils.isEmpty(currentUserRewards)) {
                        userRewardRepository.saveAll(userRewards);
                    }
                }

                Integer newLevel = programUser.getCurrentLevel() + 1;
                programUser.setCurrentLevel(newLevel);

                // set level progress
                List<ProgramUserLevelProgress> existedProgress = programUserLevelProgressRepository.findByProgramUserIdAndLevelFrom(programUser.getId(), currentLevel);
                if (existedProgress.isEmpty()) {
                    ProgramUserLevelProgress programUserLevelProgress = new ProgramUserLevelProgress();
                    programUserLevelProgress.setCreatedAt(Instant.now());
                    programUserLevelProgress.setHasReward(!userRewards.isEmpty());
                    programUserLevelProgress.setLevelFrom(currentLevel);
                    programUserLevelProgress.setLevelTo(newLevel);
                    programUserLevelProgress.setLevelUpAt(Instant.now());
                    programUserLevelProgress.setProgramUserId(programUser.getId());
                    programUserLevelProgress.setUserEventId(userEvent.getId());
                    programUserLevelProgressRepository.save(programUserLevelProgress);
                }
            }

            programUserRepository.save(programUser);
            userEventRepository.save(userEvent);
            if (userEventQueue != null) {
                userEventQueueRepository.save(userEventQueue);
            }
            userPointQueue.setExecuteStatus(ActivityEventQueueStatus.Completed.toString());
            userPointQueue.setUpdatedDate(Instant.now());
        }catch (Exception ex) {
            log.error("processSupportAdditionalUserPointQueue error: " + ex.getMessage());
            userPointQueue.setExecuteStatus(ActivityEventQueueStatus.Error.toString());
            userPointQueue.setErrorMessage(ex.getMessage());
            userPointQueue.setUpdatedDate(Instant.now());
            supportAdditionalUserPointQueueRepository.save(userPointQueue);
        }
    }
}
