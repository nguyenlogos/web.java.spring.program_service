package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.TermAndConditionService;
import aduro.basic.programservice.domain.TermAndCondition;
import aduro.basic.programservice.repository.TermAndConditionRepository;
import aduro.basic.programservice.service.dto.TermAndConditionDTO;
import aduro.basic.programservice.service.mapper.TermAndConditionMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;
import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

/**
 * Service Implementation for managing {@link TermAndCondition}.
 */
@Service
@Transactional
public class TermAndConditionServiceImpl implements TermAndConditionService {

    private final Logger log = LoggerFactory.getLogger(TermAndConditionServiceImpl.class);

    private final TermAndConditionRepository termAndConditionRepository;

    private final TermAndConditionMapper termAndConditionMapper;

    public TermAndConditionServiceImpl(TermAndConditionRepository termAndConditionRepository, TermAndConditionMapper termAndConditionMapper) {
        this.termAndConditionRepository = termAndConditionRepository;
        this.termAndConditionMapper = termAndConditionMapper;
    }

    /**
     * Save a termAndCondition.
     *
     * @param termAndConditionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public TermAndConditionDTO save(TermAndConditionDTO termAndConditionDTO) {
        log.debug("Request to save TermAndCondition : {}", termAndConditionDTO);
        TermAndCondition termAndCondition = termAndConditionMapper.toEntity(termAndConditionDTO);
        termAndCondition = termAndConditionRepository.save(termAndCondition);
        return termAndConditionMapper.toDto(termAndCondition);
    }

    /**
     * Get all the termAndConditions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<TermAndConditionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TermAndConditions");
        return termAndConditionRepository.findAll(pageable)
            .map(termAndConditionMapper::toDto);
    }


    /**
     * Get one termAndCondition by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<TermAndConditionDTO> findOne(Long id) {
        log.debug("Request to get TermAndCondition : {}", id);
        return termAndConditionRepository.findById(id)
            .map(termAndConditionMapper::toDto);
    }

    /**
     * Delete the termAndCondition by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TermAndCondition : {}", id);
        termAndConditionRepository.deleteById(id);
    }

    @Override
    public List<TermAndConditionDTO> updateTermAndConditions(List<TermAndConditionDTO> termAndConditionDTOs) {
        List<TermAndConditionDTO> newList = new ArrayList<>();
        for (TermAndConditionDTO dto: termAndConditionDTOs) {
            if(isNullOrEmpty(dto.getClientId()))
                throw new BadRequestAlertException("client id empty", ENTITY_NAME, "idexists");
            newList.add(save(dto));
        }
        return newList;
    }

    @Override
    public TermAndConditionDTO getTermAndConditionByClient(String clientId, String subgroupId) {

        TermAndCondition returnTerm = new  TermAndCondition();
        List<TermAndCondition> termAndConditionList = termAndConditionRepository.findTermAndConditionsByClientId(clientId);

        Optional<TermAndCondition> optionalTermAndCondition = termAndConditionList.stream().filter(t->t.getSubgroupId() == null || t.getSubgroupId().equals("")).findFirst();

        //get termOfC common
        if (optionalTermAndCondition.isPresent()) {
            TermAndCondition termAndCondition = optionalTermAndCondition.get();
            if (termAndCondition.isIsTargetSubgroup()) {

                if(subgroupId == null || subgroupId.equals("undefined") || subgroupId.equals("0")){
                    returnTerm = termAndCondition;
                } else {
                    Optional<TermAndCondition> optionalTermAndConditionBySubgroup = termAndConditionList.stream().filter(t-> t.getSubgroupId() != null && t.getSubgroupId().equals(subgroupId)).findFirst();
                    returnTerm = optionalTermAndConditionBySubgroup.orElse(termAndCondition);
                }

            } else {
                returnTerm =termAndCondition;
            }
        } else {
            Optional<TermAndCondition> optionalTermAndConditionBySubgroup = termAndConditionList.stream().filter(t->t.getSubgroupId() !=null && t.getSubgroupId().equals(subgroupId)).findFirst();
            if(optionalTermAndConditionBySubgroup.isPresent()){
                returnTerm = optionalTermAndConditionBySubgroup.get();
            }
        }
        return termAndConditionMapper.toDto(returnTerm);
    }

    @Override
    public List<TermAndConditionDTO> getConsentByClient(String clientId) {
        List<TermAndCondition> consentList = termAndConditionRepository.findTermAndConditionsByClientIdAndIsTerminatedOrderByConsentOrderAsc(clientId, false);
        for (TermAndCondition item : consentList) {
            if (!Objects.isNull(item.getSubgroupId()) && !item.getSubgroupId().contains("[")) {
                item.setSubgroupId("[" + item.getSubgroupId() + "]");
            }
        }
        return termAndConditionMapper.toDto(consentList);
    }

    @Override
    public List<TermAndConditionDTO> getConsentByClientIdAndSubgroupIdAndIsTerminated(String clientId, String subgroupId, Boolean isTerminated) {
        List<TermAndCondition> consentList = termAndConditionRepository.findTermAndConditionsByClientIdAndSubgroupIdContainingAndIsTerminated(clientId, subgroupId, isTerminated);
        return termAndConditionMapper.toDto(consentList);
    }

    @Override
    public List<TermAndConditionDTO> getTermAndConditionsByClientIdAndSubgroupIdIsNull(String clientId) {
        List<TermAndCondition> consentList = termAndConditionRepository.findTermAndConditionsByClientIdAndSubgroupIdIsNullAndIsTerminated(clientId, false);
        return termAndConditionMapper.toDto(consentList);
    }
}
