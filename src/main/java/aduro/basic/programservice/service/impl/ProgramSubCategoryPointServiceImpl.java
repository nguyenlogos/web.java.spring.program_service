package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.service.ProgramSubCategoryPointService;
import aduro.basic.programservice.service.businessrule.ProgramCategoryPointBusinessRule;
import aduro.basic.programservice.service.dto.ProgramStatus;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryPointMapper;
import aduro.basic.programservice.sns.PublisherService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link ProgramSubCategoryPoint}.
 */
@Service
@Transactional
public class ProgramSubCategoryPointServiceImpl implements ProgramSubCategoryPointService {

    private final Logger log = LoggerFactory.getLogger(ProgramSubCategoryPointServiceImpl.class);


    @Autowired
    private  CategoryRepository categoryRepository;
    @Autowired
    private ProgramCategoryPointRepository programCategoryPointRepository;


    private final ProgramSubCategoryPointRepository programSubCategoryPointRepository;

    private final ProgramSubCategoryPointMapper programSubCategoryPointMapper;

    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private ProgramCategoryPointBusinessRule programCategoryPointBusinessRule;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;

    @Autowired
    private PublisherService publisherService;

    public ProgramSubCategoryPointServiceImpl(ProgramSubCategoryPointRepository programSubCategoryPointRepository, ProgramSubCategoryPointMapper programSubCategoryPointMapper) {
        this.programSubCategoryPointRepository = programSubCategoryPointRepository;
        this.programSubCategoryPointMapper = programSubCategoryPointMapper;
    }

    /**
     * Save a programSubCategoryPoint.
     *
     * @param programSubCategoryPointDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramSubCategoryPointDTO save(ProgramSubCategoryPointDTO programSubCategoryPointDTO) {
        log.debug("Request to save ProgramSubCategoryPoint : {}", programSubCategoryPointDTO);
        ProgramSubCategoryPointDTO resultSubcategoryPointDto;
        ProgramCategoryPoint categoryPoint = programCategoryPointRepository.getOne(programSubCategoryPointDTO.getProgramCategoryPointId());

        if(categoryPoint == null)
            throw new BadRequestAlertException("Category is not exist", "Sub-Category", "Sub-Category");

        //validate Sub category code - make sure available in master data
        SubCategory subCategory = subCategoryRepository.findByCode(programSubCategoryPointDTO.getCode());
        if(subCategory == null){
            throw new BadRequestAlertException("Sub Category is not exist", "Sub-Category", "categoryexist");
        }

        if(programSubCategoryPointDTO.getId() == null || programSubCategoryPointDTO.getId() == 0) {
            resultSubcategoryPointDto = HandleCreateSubCategoryPoint(programSubCategoryPointDTO, categoryPoint);
        }else{
            resultSubcategoryPointDto = HandleUpdateSubCategoryPoint(programSubCategoryPointDTO);
        }

        // reset cache of program from Platform
        Program program = categoryPoint.getProgram();
        if (program != null && program.getStatus().equalsIgnoreCase(ProgramStatus.Active.toString())) {
            publisherService.publishProgramUpdatedEvent("", program.getId());
        }

        return resultSubcategoryPointDto;
    }

    private ProgramSubCategoryPointDTO HandleUpdateSubCategoryPoint(ProgramSubCategoryPointDTO programSubCategoryPointDTO) {


        ProgramSubCategoryPoint subCategoryPoint = programSubCategoryPointRepository.getOne(programSubCategoryPointDTO.getId());
        if(subCategoryPoint == null)
            throw new BadRequestAlertException("sub-category-point  is not exist", "program", "client_program");

        subCategoryPoint.setLocked(programSubCategoryPointDTO.getLocked());
        subCategoryPoint.setPercentPoint(programSubCategoryPointDTO.getPercentPoint());
        subCategoryPoint.setCompletionsCap(programSubCategoryPointDTO.getCompletionsCap());

        //Caculate sub Point before save
        programCategoryPointBusinessRule.CaculateSubPoint(subCategoryPoint,subCategoryPoint.getProgramCategoryPoint());

        return programSubCategoryPointMapper.toDto(programSubCategoryPointRepository.save(subCategoryPoint));

    }

    private ProgramSubCategoryPointDTO HandleCreateSubCategoryPoint(ProgramSubCategoryPointDTO programSubCategoryPointDTO, ProgramCategoryPoint categoryPoint) {
        //validate programCategoryPoint for new
        ProgramSubCategoryPoint subCategoryPoint =
            programSubCategoryPointRepository.findByCodeAndProgramCategoryPointId(programSubCategoryPointDTO.getCode(), programSubCategoryPointDTO.getProgramCategoryPointId());
        if(subCategoryPoint != null) {
            throw new BadRequestAlertException(" Can not add category point,It is already exist.", "Category", "categoryexist");
        }
        ProgramSubCategoryPoint programSubCategoryPoint = programSubCategoryPointMapper.toEntity(programSubCategoryPointDTO);
        programSubCategoryPoint = programCategoryPointBusinessRule.CaculateSubPoint(programSubCategoryPoint,categoryPoint);
        return programSubCategoryPointMapper.toDto(programSubCategoryPointRepository.save(programSubCategoryPoint));
    }

    /**
     * Get all the programSubCategoryPoints.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramSubCategoryPointDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramSubCategoryPoints");
        return programSubCategoryPointRepository.findAll(pageable)
            .map(programSubCategoryPointMapper::toDto);
    }


    /**
     * Get one programSubCategoryPoint by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramSubCategoryPointDTO> findOne(Long id) {
        log.debug("Request to get ProgramSubCategoryPoint : {}", id);
        return programSubCategoryPointRepository.findById(id)
            .map(programSubCategoryPointMapper::toDto);
    }

    /**
     * Delete the programSubCategoryPoint by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramSubCategoryPoint : {}", id);
        programSubCategoryPointRepository.deleteById(id);
    }

    @Override
    public List<ProgramSubCategoryPointDTO> addAndRemoveProgramSubCategoryPoints(Long programCategoryPointId, List<ProgramSubCategoryPointDTO> programSubCategoryPointDTOs) {

        ProgramCategoryPoint categoryPoint = programCategoryPointRepository.getOne(programCategoryPointId);

        List<SubCategory> subCategoryList = subCategoryRepository.findSubCategoriesByCategoryCode(categoryPoint.getCategoryCode());

        if(categoryPoint == null)
            throw new BadRequestAlertException("Category point  is not exist", "Sub-Category", "Sub-Category");

        List<ProgramSubCategoryPoint> currentProgramSubCategoryPoins = programSubCategoryPointRepository.findProgramSubCategoryPointsByProgramCategoryPointId(programCategoryPointId);
        //handle remove
        for (ProgramSubCategoryPoint entity:currentProgramSubCategoryPoins) {
            if(programSubCategoryPointDTOs.stream().anyMatch(dto ->  dto.getCode().equals(entity.getCode()))) continue;
            programSubCategoryPointRepository.delete(entity);
        }

        //handle add
        for (ProgramSubCategoryPointDTO dto: programSubCategoryPointDTOs) {
            if(currentProgramSubCategoryPoins.stream().anyMatch(p->p.getCode().equals(dto.getCode())))
                continue;

            Optional<SubCategory> optionalSubCategory =  subCategoryList.stream().filter(s->s.getCode().equals(dto.getCode())).findFirst();

            if(!optionalSubCategory.isPresent())
                throw new BadRequestAlertException("Sub Category is not exist", "Sub-Category", "categoryexist");

            SubCategory subCategory = optionalSubCategory.get();
            dto.setProgramCategoryPointId(programCategoryPointId);
            dto.setName(subCategory.getName());
            dto.setCompletionsCap(1);
            dto.setLocked(false);

            ProgramSubCategoryPoint programSubCategoryPoint = programSubCategoryPointMapper.toEntity(dto);
            programSubCategoryPointRepository.save(programSubCategoryPoint);
        }

        currentProgramSubCategoryPoins = programSubCategoryPointRepository.findProgramSubCategoryPointsByProgramCategoryPointId(programCategoryPointId);

        // reset cache of program from Platform
        Program program = categoryPoint.getProgram();
        if (program != null && program.getStatus().equalsIgnoreCase(ProgramStatus.Active.toString())) {
            publisherService.publishProgramUpdatedEvent("", program.getId());
        }

        return programSubCategoryPointMapper.toDto(currentProgramSubCategoryPoins);
    }

    @Override
    public ProgramSubCategoryPointDTO getProgramSubCategoryPointByUser(String clientId, String participantId, String subCategoryCode) {

        ProgramSubCategoryPointDTO dto = new ProgramSubCategoryPointDTO();
        if(isNullOrEmpty(clientId) || isNullOrEmpty(subCategoryCode) || isNullOrEmpty(participantId))
            return dto;

        UserDto user = null;
        try {
            user = customActivityAdapter.getUser(participantId);
        } catch (Exception e) {
            throw new BadRequestAlertException("Participant is Not Found", "ActivityEventDTO", "getProgramSubCategoryPointByUser");
        }
        Optional<ClientProgram> cp = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.toString(), user.isAsProgramTester());
        if(!cp.isPresent()) return dto;

        ProgramSubCategoryPoint programSubCategoryPoint  = programSubCategoryPointRepository.findProgramSubCategoryPointsByCodeAndProgramCategoryPointProgramId(subCategoryCode, cp.get().getProgramId());
        if(programSubCategoryPoint == null) return dto;

        Integer completionCount = userEventRepository.countAllByEventCodeAndProgramUserProgramIdAndProgramUserParticipantId(subCategoryCode, cp.get().getProgramId(), participantId);

        ProgramSubCategoryPointDTO programSubCategoryPointDTO =  programSubCategoryPointMapper.toDto(programSubCategoryPoint);
        if(completionCount >= programSubCategoryPoint.getCompletionsCap()) programSubCategoryPointDTO.setValuePoint(BigDecimal.valueOf(0.0));

        return programSubCategoryPointDTO;
    }
}
