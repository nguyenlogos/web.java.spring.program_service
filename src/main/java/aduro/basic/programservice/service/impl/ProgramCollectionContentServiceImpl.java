package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramCollectionContentService;
import aduro.basic.programservice.domain.ProgramCollectionContent;
import aduro.basic.programservice.repository.ProgramCollectionContentRepository;
import aduro.basic.programservice.service.dto.ProgramCollectionContentDTO;
import aduro.basic.programservice.service.mapper.ProgramCollectionContentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramCollectionContent}.
 */
@Service
@Transactional
public class ProgramCollectionContentServiceImpl implements ProgramCollectionContentService {

    private final Logger log = LoggerFactory.getLogger(ProgramCollectionContentServiceImpl.class);

    private final ProgramCollectionContentRepository programCollectionContentRepository;

    private final ProgramCollectionContentMapper programCollectionContentMapper;

    public ProgramCollectionContentServiceImpl(ProgramCollectionContentRepository programCollectionContentRepository, ProgramCollectionContentMapper programCollectionContentMapper) {
        this.programCollectionContentRepository = programCollectionContentRepository;
        this.programCollectionContentMapper = programCollectionContentMapper;
    }

    /**
     * Save a programCollectionContent.
     *
     * @param programCollectionContentDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramCollectionContentDTO save(ProgramCollectionContentDTO programCollectionContentDTO) {
        log.debug("Request to save ProgramCollectionContent : {}", programCollectionContentDTO);
        ProgramCollectionContent programCollectionContent = programCollectionContentMapper.toEntity(programCollectionContentDTO);
        programCollectionContent = programCollectionContentRepository.save(programCollectionContent);
        return programCollectionContentMapper.toDto(programCollectionContent);
    }

    /**
     * Get all the programCollectionContents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramCollectionContentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramCollectionContents");
        return programCollectionContentRepository.findAll(pageable)
            .map(programCollectionContentMapper::toDto);
    }


    /**
     * Get one programCollectionContent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramCollectionContentDTO> findOne(Long id) {
        log.debug("Request to get ProgramCollectionContent : {}", id);
        return programCollectionContentRepository.findById(id)
            .map(programCollectionContentMapper::toDto);
    }

    /**
     * Delete the programCollectionContent by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramCollectionContent : {}", id);
        programCollectionContentRepository.deleteById(id);
    }
}
