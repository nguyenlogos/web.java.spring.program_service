package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ProgramRepository;
import aduro.basic.programservice.service.ProgramLevelService;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.repository.ProgramLevelRepository;
import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import aduro.basic.programservice.service.dto.ProgramLevelInfo;
import aduro.basic.programservice.service.mapper.ProgramLevelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramLevel}.
 */
@Service
@Transactional
public class ProgramLevelServiceImpl implements ProgramLevelService {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelServiceImpl.class);

    private final ProgramLevelRepository programLevelRepository;

    @Autowired
    private  ProgramRepository programRepository;

    private final ProgramLevelMapper programLevelMapper;

    public ProgramLevelServiceImpl(ProgramLevelRepository programLevelRepository, ProgramLevelMapper programLevelMapper) {
        this.programLevelRepository = programLevelRepository;
        this.programLevelMapper = programLevelMapper;
    }

    /**
     * Save a programLevel.
     *
     * @param programLevelDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramLevelDTO save(ProgramLevelDTO programLevelDTO) {
        log.debug("Request to save ProgramLevel : {}", programLevelDTO);
        ProgramLevel programLevel = programLevelMapper.toEntity(programLevelDTO);
        programLevel = programLevelRepository.save(programLevel);
        return programLevelMapper.toDto(programLevel);
    }

    /**
     * Get all the programLevels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramLevelDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramLevels");
        return programLevelRepository.findAll(pageable)
            .map(programLevelMapper::toDto);
    }


    /**
     * Get one programLevel by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramLevelDTO> findOne(Long id) {
        log.debug("Request to get ProgramLevel : {}", id);
        return programLevelRepository.findById(id)
            .map(programLevelMapper::toDto);
    }

    /**
     * Delete the programLevel by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramLevel : {}", id);
        programLevelRepository.deleteById(id);
    }


    /**
     * Update list Level
     *
     * @param programId the id of the entity.
     */

    public List<ProgramLevelDTO> updateAllLevels(Long programId, List<ProgramLevelDTO> programLevelDTOS){

        List<ProgramLevel> currentList = programLevelRepository.findProgramLevelsByProgram_Id(programId);


        programLevelDTOS.sort(Comparator.comparing(ProgramLevelDTO::getLevelOrder));
        currentList.sort(Comparator.comparing(ProgramLevel::getLevelOrder));

        for (int i = 0; i < programLevelDTOS.size(); i++) {
           ProgramLevelDTO dto =  programLevelDTOS.get(i);
           ProgramLevel programLevel;
           if(currentList.size() <= (i)) {
               // add new
               programLevel = programLevelMapper.toEntity(dto);
               programLevel.setId(null);
               programLevel.setLevelOrder(i+1);
               currentList.add(programLevel);
           }else {
               programLevel = currentList.get(i);
           }

           programLevel.setName(dto.getName());
           programLevel.setDescription(dto.getDescription());
           programLevel.setEndPoint(dto.getEndPoint());
           programLevel.setEndDate(dto.getEndDate());
        }
        programLevelRepository.saveAll(currentList);

        if(currentList.size() > programLevelDTOS.size()){
            for (int i = programLevelDTOS.size(); i< currentList.size(); i++){

                programLevelRepository.delete(currentList.get(i));
            }
        }


        currentList = programLevelRepository.findProgramLevelsByProgram_Id(programId);
        return programLevelMapper.toDto(currentList) ;
    }

    @Override
    public ProgramLevelInfo updateProgramLevelInfo(Long programId, ProgramLevelInfo programLevelInfo) {

        Optional<Program> optionalProgram = programRepository.findById(programId);
        if(optionalProgram.isPresent()){

            Program program = optionalProgram.get();

            program.setLevelStructure(programLevelInfo.getLevelStructure());
            program.setUserPoint(programLevelInfo.getUserPoint());
            program.setIsUsePoint(programLevelInfo.getIsUsePoint());
            program.setLastModifiedDate(Instant.now());
            program.setLastModifiedBy(programLevelInfo.getLastModifiedBy());
            programRepository.save(program);
            programLevelInfo.setProgramLevelDTOs(updateAllLevels(programId, programLevelInfo.getProgramLevels()));
        }
        return programLevelInfo;
    }
}
