package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ClientSettingService;
import aduro.basic.programservice.domain.ClientSetting;
import aduro.basic.programservice.repository.ClientSettingRepository;
import aduro.basic.programservice.service.dto.ClientSettingDTO;
import aduro.basic.programservice.service.mapper.ClientSettingMapper;
import aduro.basic.programservice.tangocard.AccountAdapter;
import aduro.basic.programservice.tangocard.CreditCardAdapter;
import aduro.basic.programservice.tangocard.CustomerAdapter;
import aduro.basic.programservice.tangocard.dto.*;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link ClientSetting}.
 */
@Service
@Transactional
public class ClientSettingServiceImpl implements ClientSettingService {

    private final Logger log = LoggerFactory.getLogger(ClientSettingServiceImpl.class);

    private final ClientSettingRepository clientSettingRepository;

    private final ClientSettingMapper clientSettingMapper;

    @Autowired
    private CustomerAdapter customerAdapter;

    @Autowired
    private AccountAdapter accountAdapter;

    @Autowired
    private CreditCardAdapter creditCardAdapter;

    public ClientSettingServiceImpl(ClientSettingRepository clientSettingRepository, ClientSettingMapper clientSettingMapper) {
        this.clientSettingRepository = clientSettingRepository;
        this.clientSettingMapper = clientSettingMapper;
    }

    /**
     * Save a clientSetting.
     *
     * @param clientSettingDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ClientSettingDTO save(ClientSettingDTO clientSettingDTO) throws Exception {
        log.debug("Request to save ClientSetting : {}", clientSettingDTO);
        if(isNullOrEmpty(clientSettingDTO.getClientName()) || isNullOrEmpty(clientSettingDTO.getClientId()) ||
            isNullOrEmpty(clientSettingDTO.getClientEmail()) || isNullOrEmpty(clientSettingDTO.getUpdatedBy()))
            throw new BadRequestAlertException("Missing required fields", "ClientSetting", "ClientProgramRewardSetting");

        clientSettingDTO.setUpdatedDate(Instant.now());

        if(clientSettingDTO.getId() != null && clientSettingDTO.getId()>0) return clientSettingDTO;

        //create customer first

        //check customer exist
        CustomerResponse customerResponse = null;
        try{
            CompletableFuture<CustomerResponse> customerResponseCompletableFuture = customerAdapter.GetCustomerDetailAsync(clientSettingDTO.getClientId());
            customerResponse  = customerResponseCompletableFuture.get();
        }catch(Exception ex){

        }

        if(customerResponse != null){
            clientSettingDTO.setTangoCustomerIdentifier(customerResponse.getCustomerIdentifier());
        }else{
            CustomerRequest customerRequest = new CustomerRequest();
            customerRequest.setCustomerIdentifier(clientSettingDTO.getClientId());
            customerRequest.setDisplayName(clientSettingDTO.getClientName());
            CompletableFuture<CustomerResponse> cResponse = customerAdapter.CreateCustomerAysnc(customerRequest);
            clientSettingDTO.setTangoCustomerIdentifier(cResponse.get().getCustomerIdentifier());
        }

        //check account exist
        AccountResponse accountResponse = null;
        try{
            CompletableFuture<AccountResponse> accountResponseCompletableFuture = accountAdapter.GetAccountDetailAsync(clientSettingDTO.getClientId());
            accountResponse  = accountResponseCompletableFuture.get();
        }catch(Exception ex){

        }

        if(accountResponse != null){
            clientSettingDTO.setTangoAccountIdentifier(accountResponse.getAccountIdentifier());
        }else{
            //create account
            AccountRequest accountRequest = new AccountRequest();
            String accountName = String.format("%s-%s",clientSettingDTO.getClientName(), "account" );

            accountRequest.setAccountIdentifier(clientSettingDTO.getClientId());
            accountRequest.setDisplayName(accountName);
            accountRequest.setContactEmail(clientSettingDTO.getClientEmail());

            CompletableFuture<AccountResponse> accountResponseCompletableFuture = customerAdapter.CreateAccountAysnc(clientSettingDTO.getTangoCustomerIdentifier(), accountRequest);
            clientSettingDTO.setTangoAccountIdentifier(accountResponseCompletableFuture.get().getAccountIdentifier());

        }

        ClientSetting clientSetting = clientSettingMapper.toEntity(clientSettingDTO);
        clientSetting = clientSettingRepository.save(clientSetting);
        return clientSettingMapper.toDto(clientSetting);
    }

    /**
     * Get all the clientSettings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ClientSettingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientSettings");
        return clientSettingRepository.findAll(pageable)
            .map(clientSettingMapper::toDto);
    }


    /**
     * Get one clientSetting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ClientSettingDTO> findOne(Long id) {
        log.debug("Request to get ClientSetting : {}", id);
        return clientSettingRepository.findById(id)
            .map(clientSettingMapper::toDto);
    }

    /**
     * Delete the clientSetting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClientSetting : {}", id);
        clientSettingRepository.deleteById(id);
    }

    @Override
    public ClientSettingDTO getByClientId(String clientId) throws Exception {
        log.debug("Request to get ClientSetting by clientId : {}", clientId);

        ClientSetting setting =  clientSettingRepository.findClientSettingByClientId(clientId);
        if(setting == null)
            return new ClientSettingDTO();

        CompletableFuture<AccountResponse> accountResponseCompletableFuture =  accountAdapter.GetAccountDetailAsync(setting.getTangoAccountIdentifier());
        AccountResponse accountResponse = accountResponseCompletableFuture.get();
        setting.setTangoCurrentBalance(accountResponse.getCurrentBalance());

        ClientSettingDTO dto =  clientSettingMapper.toDto(setting);

        dto.setTangoAccountName(accountResponse.getDisplayName());
        return dto;
    }

    @Override
    public List<CreditCardResponse> getCreditCardsByAccount(String customerIdentifier, String accountIdentifier) throws Exception {
        log.debug("Request to get list credit cards: {}", accountIdentifier);
        CompletableFuture<List<CreditCardResponse>> creditCardResponseCompletableFuture =    creditCardAdapter.getCreditCards();
        List<CreditCardResponse> creditCardResponseList = creditCardResponseCompletableFuture.get();
        creditCardResponseList =   creditCardResponseList.stream().filter(c->c.getAccountIdentifier().equals(accountIdentifier)).collect(Collectors.toList());
        return creditCardResponseList;
    }
    public ClientSettingDTO updateThresholdBalance(ClientSettingDTO clientSettingDTO) {
        ClientSetting clientSetting = clientSettingRepository.getOne(clientSettingDTO.getId());
        if(clientSetting != null){
            clientSetting.setTangoThresholdBalance(clientSettingDTO.getTangoThresholdBalance());
            clientSettingRepository.save(clientSetting);
        }
        return clientSettingDTO;
    }


}
