package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.service.ClientProgramService;
import aduro.basic.programservice.service.dto.ClientProgramDTO;
import aduro.basic.programservice.service.dto.ProgramSubgroupDTO;
import aduro.basic.programservice.service.mapper.ClientProgramMapper;
import aduro.basic.programservice.service.mapper.ProgramSubgroupMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ClientProgram}.
 */
@Service
@Transactional
public class ClientProgramServiceImpl implements ClientProgramService {

    private final Logger log = LoggerFactory.getLogger(ClientProgramServiceImpl.class);

    private final ClientProgramRepository clientProgramRepository;

    @Autowired
    private  ProgramRepository programRepository;

    @Autowired
    private ProgramSubgroupRepository programSubgroupRepository;

    @Autowired
    private ProgramLevelRewardRepository programLevelRewardRepository;

    @Autowired
    private ProgramLevelActivityRepository programLevelActivityRepository;

    @Autowired
    private ProgramSubgroupMapper  programSubgroupMapper;

    private final ClientProgramMapper clientProgramMapper;

    private final ProgramLevelPathRepository programLevelPathRepository;

    private final ProgramRequirementItemsRepository programRequirementItemsRepository;

    public ClientProgramServiceImpl(ClientProgramRepository clientProgramRepository, ClientProgramMapper clientProgramMapper, ProgramLevelPathRepository programLevelPathRepository, ProgramRequirementItemsRepository programRequirementItemsRepository) {
        this.clientProgramRepository = clientProgramRepository;
        this.clientProgramMapper = clientProgramMapper;
        this.programLevelPathRepository = programLevelPathRepository;
        this.programRequirementItemsRepository = programRequirementItemsRepository;
    }

    /**
     * Save a clientProgram.
     *
     * @param clientProgramDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ClientProgramDTO save(ClientProgramDTO clientProgramDTO) {
        log.debug("Request to save ClientProgram : {}", clientProgramDTO);
        validateClientBeforeAssign(clientProgramDTO);

        ClientProgram clientProgram = clientProgramMapper.toEntity(clientProgramDTO);
        clientProgram = clientProgramRepository.save(clientProgram);
        return clientProgramMapper.toDto(clientProgram);
    }


    ///
    private void validateClientBeforeAssign(ClientProgramDTO clientProgramDTO){

        if(clientProgramDTO.getClientId() == null || clientProgramDTO.getClientId().equals(""))
             throw new BadRequestAlertException("client id is missing", "ClientProgram", "ClientId");

        if(clientProgramDTO.getProgramId() == null || clientProgramDTO.getProgramId() == 0 )
            throw new BadRequestAlertException("program id is missing", "ClientProgram", "Programid");

        if(clientProgramDTO.getId() == null || clientProgramDTO.getId() ==0){
            //Get the program
           Optional<Program> p =  programRepository.findById(clientProgramDTO.getProgramId());
           if(!p.isPresent())  throw new BadRequestAlertException("program id is missing", "ClientProgram", "Programid");

            Program program = p.get();

            //Optional<Program> optionalProgram = programRepository.findClientProgramByDateRange(program.getStartDate(), program.getResetDate());

            List<ClientProgram> clientProgramList = clientProgramRepository.findClientProgramByDateRange(clientProgramDTO.getClientId(),program.getStartDate(), program.getResetDate());

            if(clientProgramList.size()>0) {
                ClientProgram c = clientProgramList.get(0);
                String str = "{\"programId\":\"" + c.getProgramId() + "\",\"programName\":\"" + c.getProgram().getName() +"\"}";
                throw new BadRequestAlertException("Client has another program which overlaps the date range", "client_program", str);
            }

            if(program.getProgramQAStartDate() != null && program.getProgramQAEndDate() != null) {
                List<ClientProgram> clientProgramQaVerifyList = clientProgramRepository.findClientProgramByQADateRange(clientProgramDTO.getClientId(), program.getProgramQAStartDate(), program.getProgramQAEndDate());
                if (clientProgramQaVerifyList.size() > 0) {
                    ClientProgram c = clientProgramQaVerifyList.get(0);
                    String str = "{\"programId\":\"" + c.getProgramId() + "\",\"programName\":\"" + c.getProgram().getName() + "\"}";
                    throw new BadRequestAlertException("Client has another program which overlaps the program QA date range", "client_program", str);
                }
            }
        }

    }

    /**
     * Get all the clientPrograms.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ClientProgramDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientPrograms");
        return clientProgramRepository.findAll(pageable)
            .map(clientProgramMapper::toDto);
    }


    /**
     * Get one clientProgram by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ClientProgramDTO> findOne(Long id) {
        log.debug("Request to get ClientProgram : {}", id);
        return clientProgramRepository.findById(id)
            .map(clientProgramMapper::toDto);
    }

    /**
     * Delete the clientProgram by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClientProgram : {}", id);
        clientProgramRepository.deleteById(id);
    }

    public ClientProgramDTO assignClientsToProgram(Long programId, List<ClientProgramDTO> clientProgramDTOList) {

       /* List<ClientProgram> currentList = clientProgramRepository.findClientProgramsByProgramId(programId);

        //handle delete
        for (ClientProgram entity:currentList) {
            if(clientProgramDTOList.stream().anyMatch(dto -> dto.getClientId().equals(entity.getClientId()))) continue;
            clientProgramRepository.delete(entity);
        }

        for (ClientProgramDTO dto:clientProgramDTOList ) {
            if(dto.getId() != null && dto.getId() != 0){
                continue;
            }

            dto.setProgramId(programId);
            save(dto);
        }

        currentList = clientProgramRepository.findClientProgramsByProgramId(programId);
        return clientProgramMapper.toDto(currentList) ;*/


       /*if(clientProgramDTOList.size()>0)
           return assignClientToProgram(programId, clientProgramDTOList.get(0));
       else {
           removeClientFromProgram(programId);
           return null;
       }*/

        removeClientFromProgram(programId);
        if(clientProgramDTOList.size()>0){
            return assignClientToProgram(programId, clientProgramDTOList.get(0));
        }
        return null;
    }

    private ClientProgramDTO assignClientToProgram(Long programId, ClientProgramDTO clientProgramDTO) {

        if(clientProgramDTO.getId() != null && clientProgramDTO.getId() != 0){
            return clientProgramDTO;
        }
        clientProgramDTO.setProgramId(programId);
     /*   //validate client
        List<ClientProgram> clientPrograms = clientProgramRepository.findClientProgramsByProgramId(programId);

        if(clientPrograms.size() >0)
            throw new BadRequestAlertException("Can not assign client program ...", "client_program", programId.toString());

        clientProgramRepository.deleteAll(clientPrograms);
        removeClientFromProgram(programId);*/

        for (ProgramSubgroupDTO dto:clientProgramDTO.getProgramSubgroups()
        ) {
            dto.setProgramId(programId);
        }

        List<ProgramSubgroup> programSubgroups = programSubgroupMapper.toEntity(clientProgramDTO.getProgramSubgroups());
        programSubgroupRepository.saveAll(programSubgroups);
        return save(clientProgramDTO);
    }
    private void removeClientFromProgram(Long programId){
        List<ClientProgram> currentList = clientProgramRepository.findClientProgramsByProgramId(programId);
        for (ClientProgram entity:currentList) {
            clientProgramRepository.delete(entity);

            //delete program subgroup.
            programSubgroupRepository.deleteProgramSubgroupsByProgramId(programId);
            //delete all rewards in subgroup.
            programLevelRewardRepository.deleteProgramLevelRewardsByProgramLevelProgramIdAndSubgroupIdNotNullAndSubgroupIdIsNotLike(programId,"All");
            //delete all required activity in subgroup
            programLevelActivityRepository.deleteProgramLevelActivitiesByProgramLevelProgramIdAndSubgroupIdIsNotNullAndSubgroupIdNotLike(programId, "All");

            entity.getProgram().getProgramLevels().stream()
                .forEach( programLevel -> {

                    programLevel.getProgramLevelPaths().stream().filter( p -> p.getSubgroupId() != null && !p.getSubgroupId().equalsIgnoreCase("All"))
                        .forEach( programLevelPathRepository::delete);

                    programLevel.getProgramRequirementItems().stream().filter( p -> p.getSubgroupId() != null && !p.getSubgroupId().equalsIgnoreCase("All"))
                        .forEach( programRequirementItemsRepository::delete);

                    programLevel.getProgramLevelRewards().stream().filter( p -> p.getSubgroupId() != null && !p.getSubgroupId().equalsIgnoreCase("All"))
                        .forEach( programLevelRewardRepository::delete);

                });
        }

    }

   /* @Override
    public List<ClientProgramDTO> findClientProgramByDateRange(String clientId, LocalDate startDate, LocalDate resetDate) {
        return clientProgramMapper.toDto(clientProgramRepository.findClientProgramByDateRange(clientId,startDate,resetDate));
    }*/
}
