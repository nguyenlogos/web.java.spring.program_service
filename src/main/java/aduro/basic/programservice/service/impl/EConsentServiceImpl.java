package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.EConsentService;
import aduro.basic.programservice.domain.EConsent;
import aduro.basic.programservice.repository.EConsentRepository;
import aduro.basic.programservice.service.dto.EConsentDTO;
import aduro.basic.programservice.service.mapper.EConsentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link EConsent}.
 */
@Service
@Transactional
public class EConsentServiceImpl implements EConsentService {

    private final Logger log = LoggerFactory.getLogger(EConsentServiceImpl.class);

    private final EConsentRepository eConsentRepository;

    private final EConsentMapper eConsentMapper;

    public EConsentServiceImpl(EConsentRepository eConsentRepository, EConsentMapper eConsentMapper) {
        this.eConsentRepository = eConsentRepository;
        this.eConsentMapper = eConsentMapper;
    }

    /**
     * Save a eConsent.
     *
     * @param eConsentDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public EConsentDTO save(EConsentDTO eConsentDTO) {
        log.debug("Request to save EConsent : {}", eConsentDTO);
        EConsent eConsent = eConsentMapper.toEntity(eConsentDTO);
        eConsent = eConsentRepository.save(eConsent);
        return eConsentMapper.toDto(eConsent);
    }

    /**
     * Get all the eConsents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<EConsentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EConsents");
        return eConsentRepository.findAll(pageable)
            .map(eConsentMapper::toDto);
    }


    /**
     * Get one eConsent by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<EConsentDTO> findOne(Long id) {
        log.debug("Request to get EConsent : {}", id);
        return eConsentRepository.findById(id)
            .map(eConsentMapper::toDto);
    }

    /**
     * Delete the eConsent by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete EConsent : {}", id);
        eConsentRepository.deleteById(id);
    }
}
