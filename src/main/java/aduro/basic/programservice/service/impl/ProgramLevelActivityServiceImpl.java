package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.ProgramCollectionContent;
import aduro.basic.programservice.repository.ProgramActivityRepository;
import aduro.basic.programservice.service.ProgramLevelActivityService;
import aduro.basic.programservice.domain.ProgramLevelActivity;
import aduro.basic.programservice.repository.ProgramLevelActivityRepository;
import aduro.basic.programservice.service.dto.APIResponse;
import aduro.basic.programservice.service.dto.ProgramLevelActivityDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelActivityMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link ProgramLevelActivity}.
 */
@Service
@Transactional
public class ProgramLevelActivityServiceImpl implements ProgramLevelActivityService {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelActivityServiceImpl.class);

    private final ProgramLevelActivityRepository programLevelActivityRepository;

    @Autowired
    private ProgramActivityRepository programActivityRepository;


    private final ProgramLevelActivityMapper programLevelActivityMapper;

    public ProgramLevelActivityServiceImpl(ProgramLevelActivityRepository programLevelActivityRepository, ProgramLevelActivityMapper programLevelActivityMapper) {
        this.programLevelActivityRepository = programLevelActivityRepository;
        this.programLevelActivityMapper = programLevelActivityMapper;
    }

    /**
     * Save a programLevelActivity.
     *
     * @param programLevelActivityDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramLevelActivityDTO save(ProgramLevelActivityDTO programLevelActivityDTO) {
        log.debug("Request to save ProgramLevelActivity : {}", programLevelActivityDTO);
        ProgramLevelActivity programLevelActivity = programLevelActivityMapper.toEntity(programLevelActivityDTO);
        programLevelActivity = programLevelActivityRepository.save(programLevelActivity);
        return programLevelActivityMapper.toDto(programLevelActivity);
    }

    /**
     * Get all the programLevelActivities.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramLevelActivityDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramLevelActivities");
        return programLevelActivityRepository.findAll(pageable)
            .map(programLevelActivityMapper::toDto);
    }


    /**
     * Get one programLevelActivity by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramLevelActivityDTO> findOne(Long id) {
        log.debug("Request to get ProgramLevelActivity : {}", id);
        return programLevelActivityRepository.findById(id)
            .map(programLevelActivityMapper::toDto);
    }

    /**
     * Delete the programLevelActivity by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramLevelActivity : {}", id);
        programLevelActivityRepository.deleteById(id);
    }

    /**
     * Update list Level
     *
     * @param programLevelId the id of the entity.
     */
    public List<ProgramLevelActivityDTO> updateLevelActivities(Long programLevelId, List<ProgramLevelActivityDTO> programLevelActivityDTOs){

        List<ProgramLevelActivity> currentLevelActivities = programLevelActivityRepository.findProgramLevelActivitiesByProgramLevelId(programLevelId);

        //handle delete
        for (ProgramLevelActivity entity:currentLevelActivities) {

            if(programLevelActivityDTOs.stream().anyMatch(dto -> dto.getId() != null &&  dto.getId().equals(entity.getId()))) continue;
            programLevelActivityRepository.delete(entity);
        }

        for (ProgramLevelActivityDTO dto:programLevelActivityDTOs) {
            if (dto.getId() == null || dto.getId() == 0) {
                if (dto.getActivityId() != null && dto.getActivityId() != "") {
                    programLevelActivityRepository.save(programLevelActivityMapper.toEntity(dto));
                } else {
                    throw new BadRequestAlertException("Activity id can not null", "ProgramLevelActivity", "program-level-reward");
                }
            }
        }

        currentLevelActivities = programLevelActivityRepository.findProgramLevelActivitiesByProgramLevelId(programLevelId);
        return programLevelActivityMapper.toDto(currentLevelActivities) ;
    }

    @Override
    public APIResponse<Boolean> saveWithList(List<ProgramLevelActivityDTO> programLevelActivities) {

        APIResponse<Boolean> result = new APIResponse<>();
        result.setResult(true);
        result.setHttpStatus(HttpStatus.CREATED);

        if (CollectionUtils.isEmpty(programLevelActivities)) {
            return result;
        }

        List<ProgramLevelActivityDTO> collectionOfProgramLevelActivity = programLevelActivities.stream()
            .filter(p -> Objects.nonNull(p.getDelete()) && p.getDelete())
            .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(collectionOfProgramLevelActivity)) {

            List<Long> deletedIds = collectionOfProgramLevelActivity.stream()
                .map(ProgramLevelActivityDTO::getId)
                .collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(collectionOfProgramLevelActivity)) {
                programLevelActivityRepository.deleteInBatch(programLevelActivityMapper.toEntity(collectionOfProgramLevelActivity));
            }

            List<ProgramLevelActivityDTO> savingProgramLevelActivity = programLevelActivities.stream()
                .filter(p -> !deletedIds.contains(p.getId()))
                .collect(Collectors.toList());

            if (!CollectionUtils.isEmpty(savingProgramLevelActivity)) {
                savingActivities(programLevelActivityMapper.toEntity(savingProgramLevelActivity));
            }
        } else {
            savingActivities(programLevelActivityMapper.toEntity(programLevelActivities));
        }
        return result;
    }

    @Transactional
    public void savingActivities(List<ProgramLevelActivity> programLevelActivities) {

        if (programLevelActivities.size() > Constants.BATCH_SIZE) {

            List<ProgramLevelActivity> savingObjects = new ArrayList<>();

            for (int i = 0; i < programLevelActivities.size(); i++) {

                savingObjects.add(programLevelActivities.get(i));

                if (i % Constants.BATCH_SIZE == 0 && i > 0) {
                    programLevelActivityRepository.saveAll(savingObjects);
                    savingObjects.clear();
                }
            }

            if (!CollectionUtils.isEmpty(savingObjects)) {
                programLevelActivityRepository.saveAll(savingObjects);
            }

        } else {
            programLevelActivityRepository.saveAll(programLevelActivities);
        }
    }
}
