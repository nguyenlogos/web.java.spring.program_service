package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramCohortCollectionService;
import aduro.basic.programservice.domain.ProgramCohortCollection;
import aduro.basic.programservice.repository.ProgramCohortCollectionRepository;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortCollectionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramCohortCollection}.
 */
@Service
@Transactional
public class ProgramCohortCollectionServiceImpl implements ProgramCohortCollectionService {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortCollectionServiceImpl.class);

    private final ProgramCohortCollectionRepository programCohortCollectionRepository;

    private final ProgramCohortCollectionMapper programCohortCollectionMapper;

    public ProgramCohortCollectionServiceImpl(ProgramCohortCollectionRepository programCohortCollectionRepository, ProgramCohortCollectionMapper programCohortCollectionMapper) {
        this.programCohortCollectionRepository = programCohortCollectionRepository;
        this.programCohortCollectionMapper = programCohortCollectionMapper;
    }

    /**
     * Save a programCohortCollection.
     *
     * @param programCohortCollectionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramCohortCollectionDTO save(ProgramCohortCollectionDTO programCohortCollectionDTO) {
        log.debug("Request to save ProgramCohortCollection : {}", programCohortCollectionDTO);
        ProgramCohortCollection programCohortCollection = programCohortCollectionMapper.toEntity(programCohortCollectionDTO);
        programCohortCollection = programCohortCollectionRepository.save(programCohortCollection);
        return programCohortCollectionMapper.toDto(programCohortCollection);
    }

    /**
     * Get all the programCohortCollections.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramCohortCollectionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramCohortCollections");
        return programCohortCollectionRepository.findAll(pageable)
            .map(programCohortCollectionMapper::toDto);
    }


    /**
     * Get one programCohortCollection by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramCohortCollectionDTO> findOne(Long id) {
        log.debug("Request to get ProgramCohortCollection : {}", id);
        return programCohortCollectionRepository.findById(id)
            .map(programCohortCollectionMapper::toDto);
    }

    /**
     * Delete the programCohortCollection by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramCohortCollection : {}", id);
        programCohortCollectionRepository.deleteById(id);
    }
}
