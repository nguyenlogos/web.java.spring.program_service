package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.domain.Reward;
import aduro.basic.programservice.repository.ProgramRepository;
import aduro.basic.programservice.repository.RewardRepository;
import aduro.basic.programservice.service.ProgramLevelRewardService;
import aduro.basic.programservice.domain.ProgramLevelReward;
import aduro.basic.programservice.repository.ProgramLevelRewardRepository;
import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import aduro.basic.programservice.service.dto.ProgramLevelRewardDTO;
import aduro.basic.programservice.service.dto.RewardTypeEnum;
import aduro.basic.programservice.service.dto.SubgroupRewardsDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelRewardMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link ProgramLevelReward}.
 */
@Service
@Transactional
public class ProgramLevelRewardServiceImpl implements ProgramLevelRewardService {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelRewardServiceImpl.class);

    private final ProgramLevelRewardRepository programLevelRewardRepository;

    private final ProgramLevelRewardMapper programLevelRewardMapper;

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private RewardRepository rewardRepository;

    public ProgramLevelRewardServiceImpl(ProgramLevelRewardRepository programLevelRewardRepository, ProgramLevelRewardMapper programLevelRewardMapper) {
        this.programLevelRewardRepository = programLevelRewardRepository;
        this.programLevelRewardMapper = programLevelRewardMapper;
    }

    /**
     * Save a programLevelReward.
     *
     * @param programLevelRewardDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramLevelRewardDTO save(ProgramLevelRewardDTO programLevelRewardDTO) {
        log.debug("Request to save ProgramLevelReward : {}", programLevelRewardDTO);

        if(isNullOrEmpty(programLevelRewardDTO.getRewardType()))
            throw new BadRequestAlertException("Reward type is required", "ProgramLevelReward", "Reward Type");

        /*if(programLevelRewardDTO.getRewardType().equals(RewardTypeEnum.Tremendous.toString())) {
            if(isNullOrEmpty(programLevelRewardDTO.getCode())){
                throw new BadRequestAlertException("Name is required", "ProgramLevelReward", "Code");
            }
            if(programLevelRewardDTO.getRewardAmount() == null || programLevelRewardDTO.getRewardAmount().equals(0)){
                throw new BadRequestAlertException("Amount is required", "ProgramLevelReward", "RewardAmount");
            }
        }else{
            if(isNullOrEmpty(programLevelRewardDTO.getCode())){
                throw new BadRequestAlertException("Code is required", "ProgramLevelReward", "Code");
            }
        }*/

        programLevelRewardDTO.setQuantity(1);

        if(isNullOrEmpty(programLevelRewardDTO.getDescription()))
            programLevelRewardDTO.setDescription(programLevelRewardDTO.getCode());

        //fixing AP-8597
        if (programLevelRewardDTO.getRewardType().equalsIgnoreCase(RewardTypeEnum.TangoCard.name())) {
            if (isNullOrEmpty(programLevelRewardDTO.getCampaignId())) {
                throw new BadRequestAlertException("The campaignId of Tango is required.", "TangoReward", "campaignId");
            }
        }

        ProgramLevelReward programLevelReward = programLevelRewardRepository.save(programLevelRewardMapper.toEntity(programLevelRewardDTO));
        return programLevelRewardMapper.toDto(programLevelReward);

    }

    /**
     * Get all the programLevelRewards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramLevelRewardDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramLevelRewards");
        return programLevelRewardRepository.findAll(pageable)
            .map(programLevelRewardMapper::toDto);
    }


    /**
     * Get one programLevelReward by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramLevelRewardDTO> findOne(Long id) {
        log.debug("Request to get ProgramLevelReward : {}", id);
        return programLevelRewardRepository.findById(id)
            .map(programLevelRewardMapper::toDto);
    }

    /**
     * Delete the programLevelReward by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramLevelReward : {}", id);
        programLevelRewardRepository.deleteById(id);
    }

    /**
     * Update list Level
     *
     * @param programLevelId the id of the entity.
     */
    public List<ProgramLevelRewardDTO> updateLevelRewards(Long programLevelId, List<ProgramLevelRewardDTO> programLevelRewardDTOs){
        List<Reward> rewardList =  rewardRepository.findAll();
        List<ProgramLevelReward> currentLevelRewards = programLevelRewardRepository.findProgramLevelRewardsByProgramLevelId(programLevelId);
        //handle delete
        for (ProgramLevelReward entity:currentLevelRewards) {
            if(programLevelRewardDTOs.stream().anyMatch(dto -> dto.getId()!=null  &&  dto.getId().equals(entity.getId()))) continue;
            programLevelRewardRepository.delete(entity);
        }

        for (ProgramLevelRewardDTO dto:programLevelRewardDTOs) {
           save(dto);
        }


        currentLevelRewards = programLevelRewardRepository.findProgramLevelRewardsByProgramLevelId(programLevelId);
        return programLevelRewardMapper.toDto(currentLevelRewards) ;

    }

    @Override
    public List<ProgramLevelRewardDTO> updateRewardsForAllLevel(Long programId, SubgroupRewardsDTO subgroupRewardsDTO) {

        //Get current all rewards
        List<ProgramLevelReward> currentLevelRewards = programLevelRewardRepository.findProgramLevelRewardsByProgramLevelProgramId(programId);

        //handle delete
        for (ProgramLevelReward entity:currentLevelRewards) {
            if(subgroupRewardsDTO.getProgramLevelRewards().stream().anyMatch(dto -> dto.getId()!=null  &&  dto.getId().equals(entity.getId()))) continue;
            programLevelRewardRepository.delete(entity);
        }

        for (ProgramLevelRewardDTO dto:subgroupRewardsDTO.getProgramLevelRewards()) {
            save(dto);
        }
        currentLevelRewards = programLevelRewardRepository.findProgramLevelRewardsByProgramLevelProgramId(programId);

        // save program apply reward
        Program program =  programRepository.getOne(programId);
        program.setLastModifiedDate(Instant.now());
        program.setApplyRewardAllSubgroup(subgroupRewardsDTO.getApplyRewardAllSubgroup());
        programRepository.save(program);

        return programLevelRewardMapper.toDto(currentLevelRewards) ;
    }



    /*public List<ProgramLevelRewardDTO> updateRewardsForSubgroup(Long programId, List<ProgramLevelRewardDTO> programLevelRewardDTOs) {



    }*/
}
