package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramUserCohortService;
import aduro.basic.programservice.domain.ProgramUserCohort;
import aduro.basic.programservice.repository.ProgramUserCohortRepository;
import aduro.basic.programservice.service.dto.ProgramUserCohortDTO;
import aduro.basic.programservice.service.mapper.ProgramUserCohortMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramUserCohort}.
 */
@Service
@Transactional
public class ProgramUserCohortServiceImpl implements ProgramUserCohortService {

    private final Logger log = LoggerFactory.getLogger(ProgramUserCohortServiceImpl.class);

    private final ProgramUserCohortRepository programUserCohortRepository;

    private final ProgramUserCohortMapper programUserCohortMapper;

    public ProgramUserCohortServiceImpl(ProgramUserCohortRepository programUserCohortRepository, ProgramUserCohortMapper programUserCohortMapper) {
        this.programUserCohortRepository = programUserCohortRepository;
        this.programUserCohortMapper = programUserCohortMapper;
    }

    /**
     * Save a programUserCohort.
     *
     * @param programUserCohortDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramUserCohortDTO save(ProgramUserCohortDTO programUserCohortDTO) {
        log.debug("Request to save ProgramUserCohort : {}", programUserCohortDTO);
        ProgramUserCohort programUserCohort = programUserCohortMapper.toEntity(programUserCohortDTO);
        programUserCohort = programUserCohortRepository.save(programUserCohort);
        return programUserCohortMapper.toDto(programUserCohort);
    }

    /**
     * Get all the programUserCohorts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramUserCohortDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramUserCohorts");
        return programUserCohortRepository.findAll(pageable)
            .map(programUserCohortMapper::toDto);
    }


    /**
     * Get one programUserCohort by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramUserCohortDTO> findOne(Long id) {
        log.debug("Request to get ProgramUserCohort : {}", id);
        return programUserCohortRepository.findById(id)
            .map(programUserCohortMapper::toDto);
    }

    /**
     * Delete the programUserCohort by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramUserCohort : {}", id);
        programUserCohortRepository.deleteById(id);
    }
}
