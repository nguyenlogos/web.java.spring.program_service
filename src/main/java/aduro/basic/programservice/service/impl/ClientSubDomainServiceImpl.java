package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ClientSubDomainService;
import aduro.basic.programservice.domain.ClientSubDomain;
import aduro.basic.programservice.repository.ClientSubDomainRepository;
import aduro.basic.programservice.service.dto.ClientSubDomainDTO;
import aduro.basic.programservice.service.mapper.ClientSubDomainMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ClientSubDomain}.
 */
@Service
@Transactional
public class ClientSubDomainServiceImpl implements ClientSubDomainService {

    private final Logger log = LoggerFactory.getLogger(ClientSubDomainServiceImpl.class);

    private final ClientSubDomainRepository clientSubDomainRepository;

    private final ClientSubDomainMapper clientSubDomainMapper;

    public ClientSubDomainServiceImpl(ClientSubDomainRepository clientSubDomainRepository, ClientSubDomainMapper clientSubDomainMapper) {
        this.clientSubDomainRepository = clientSubDomainRepository;
        this.clientSubDomainMapper = clientSubDomainMapper;
    }

    /**
     * Save a clientSubDomain.
     *
     * @param clientSubDomainDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ClientSubDomainDTO save(ClientSubDomainDTO clientSubDomainDTO) {
        log.debug("Request to save ClientSubDomain : {}", clientSubDomainDTO);
        ClientSubDomain clientSubDomain = clientSubDomainMapper.toEntity(clientSubDomainDTO);
        clientSubDomain = clientSubDomainRepository.save(clientSubDomain);
        return clientSubDomainMapper.toDto(clientSubDomain);
    }

    /**
     * Get all the clientSubDomains.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ClientSubDomainDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientSubDomains");
        return clientSubDomainRepository.findAll(pageable)
            .map(clientSubDomainMapper::toDto);
    }


    /**
     * Get one clientSubDomain by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ClientSubDomainDTO> findOne(Long id) {
        log.debug("Request to get ClientSubDomain : {}", id);
        return clientSubDomainRepository.findById(id)
            .map(clientSubDomainMapper::toDto);
    }

    /**
     * Delete the clientSubDomain by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClientSubDomain : {}", id);
        clientSubDomainRepository.deleteById(id);
    }
}
