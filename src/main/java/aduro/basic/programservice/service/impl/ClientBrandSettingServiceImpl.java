package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.ClientSubDomain;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ClientProgramRepository;
import aduro.basic.programservice.repository.ClientSubDomainRepository;
import aduro.basic.programservice.repository.extensions.ClientSubDomainRepositoryExtension;
import aduro.basic.programservice.service.ClientBrandSettingService;
import aduro.basic.programservice.domain.ClientBrandSetting;
import aduro.basic.programservice.repository.ClientBrandSettingRepository;
import aduro.basic.programservice.service.dto.ClientBrandSettingDTO;
import aduro.basic.programservice.service.dto.ProgramStatus;
import aduro.basic.programservice.service.mapper.ClientBrandSettingMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ClientBrandSetting}.
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ClientBrandSettingServiceImpl implements ClientBrandSettingService {

    private final Logger log = LoggerFactory.getLogger(ClientBrandSettingServiceImpl.class);

    private final ClientBrandSettingRepository clientBrandSettingRepository;

    private final ClientBrandSettingMapper clientBrandSettingMapper;

    private final ClientSubDomainRepositoryExtension clientSubDomainRepository;

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    public ClientBrandSettingServiceImpl(ClientBrandSettingRepository clientBrandSettingRepository, ClientBrandSettingMapper clientBrandSettingMapper, ClientSubDomainRepositoryExtension clientSubDomainRepository) {
        this.clientBrandSettingRepository = clientBrandSettingRepository;
        this.clientBrandSettingMapper = clientBrandSettingMapper;
        this.clientSubDomainRepository = clientSubDomainRepository;
    }

    /**
     * Save a clientBrandSetting.
     *
     * @param clientBrandSettingDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ClientBrandSettingDTO save(ClientBrandSettingDTO clientBrandSettingDTO) {
        log.debug("Request to save ClientBrandSetting : {}", clientBrandSettingDTO);
        ClientBrandSetting clientBrandSetting = clientBrandSettingMapper.toEntity(clientBrandSettingDTO);
        clientBrandSetting = clientBrandSettingRepository.save(clientBrandSetting);
        return clientBrandSettingMapper.toDto(clientBrandSetting);
    }

    /**
     * Get all the clientBrandSettings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ClientBrandSettingDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ClientBrandSettings");
        return clientBrandSettingRepository.findAll(pageable)
            .map(clientBrandSettingMapper::toDto);
    }


    /**
     * Get one clientBrandSetting by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ClientBrandSettingDTO> findOne(Long id) {
        log.debug("Request to get ClientBrandSetting : {}", id);
        return clientBrandSettingRepository.findById(id)
            .map(clientBrandSettingMapper::toDto);
    }

    /**
     * Delete the clientBrandSetting by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ClientBrandSetting : {}", id);
        clientBrandSettingRepository.deleteById(id);
    }

    @Override
    public Optional<ClientBrandSettingDTO> getClientBrandSettingByClientId(String clientId) {
        log.debug("Request to get ClientBrandSetting : {}", clientId);
        Optional<ClientBrandSettingDTO> optionalClientBrandSettingDTO = clientBrandSettingRepository.findClientBrandSettingByClientId(clientId)
            .map(clientBrandSettingMapper::toDto);

        if(!optionalClientBrandSettingDTO.isPresent()) return optionalClientBrandSettingDTO;

        return Optional.of(getClientBrandSettingDTO(optionalClientBrandSettingDTO.get()));
    }

    @Override
    public ClientBrandSettingDTO getClientBrandSettingBySubPathUrl(String subPathUrl) {
        log.debug("Request to get ClientBrandSetting : {}", subPathUrl);

        Optional<ClientBrandSettingDTO>  optionalClientBrandSettingDTO =  clientBrandSettingRepository.findClientBrandSettingBySubPathUrl(subPathUrl)
            .map(clientBrandSettingMapper::toDto);

       if(!optionalClientBrandSettingDTO.isPresent()) return new ClientBrandSettingDTO();

        return getClientBrandSettingDTO(optionalClientBrandSettingDTO.get());
    }

    @Override
    public ClientBrandSettingDTO saveClientBrandSetting(ClientBrandSettingDTO clientBrandSettingDTO) {

        if (clientBrandSettingDTO.getId() == null) {
            return saveClientBrandSettingWithDTO(clientBrandSettingDTO);
        } else {
            return updateClientBrandSettingWithDTO(clientBrandSettingDTO);
        }

    }

    private ClientBrandSettingDTO updateClientBrandSettingWithDTO(ClientBrandSettingDTO clientBrandSettingDTO) {

        ClientBrandSetting clientBrandSettingNeedUpdate = clientBrandSettingRepository.getOne(clientBrandSettingDTO.getId());

        String subDomainUrl = clientBrandSettingNeedUpdate.getSubPathUrl();

        if (!clientBrandSettingNeedUpdate.getSubPathUrl().equalsIgnoreCase(clientBrandSettingDTO.getSubPathUrl())) {
            Optional<ClientBrandSetting> clientBrandSettingData = clientBrandSettingRepository.findClientBrandSettingBySubPathUrl(clientBrandSettingDTO.getSubPathUrl());
            if (clientBrandSettingData.isPresent()) {
                throw new BadRequestAlertException("SubDomainUrl is existed", "ClientBrandSetting", "SubDomainUrl");
            }

            Option<ClientSubDomain> existed = clientSubDomainRepository.findClientSubDomainBySubDomainUrl(clientBrandSettingDTO.getSubPathUrl());
            if (existed.isDefined()) {
                throw new BadRequestAlertException("SubDomainUrl is existed", "ClientSubDomain", "SubDomainUrl");
            }
        }

        Option<ClientSubDomain> clientSubDomainExisted = clientSubDomainRepository.findClientSubDomainBySubDomainUrlAndClientId(subDomainUrl, clientBrandSettingDTO.getClientId());

        ClientBrandSetting clientBrandSetting = clientBrandSettingMapper.toEntity(clientBrandSettingDTO);

        ClientBrandSetting brandSetting = clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        Optional<ClientProgram> clientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientBrandSetting.getClientId(), ProgramStatus.Active.name(), false);

        if (clientSubDomainExisted.isDefined()) {
            ClientSubDomain clientSubDomain = clientSubDomainExisted.get();
            clientSubDomain.setClientId(clientBrandSetting.getClientId());
            clientSubDomain.setSubDomainUrl(clientBrandSetting.getSubPathUrl());
            clientSubDomain.setModifiedAt(Instant.now());
            if (clientProgram.isPresent()) {
                Program program = clientProgram.get().getProgram();
                if (program != null){
                    clientSubDomain.setProgramId(program.getId());
                    clientSubDomain.setProgramName(program.getName());
                }
            }
            clientSubDomainRepository.save(clientSubDomain);
        } else {
            ClientSubDomain clientSubDomain = new ClientSubDomain();
            clientSubDomain.setClientId(clientBrandSetting.getClientId());
            clientSubDomain.setSubDomainUrl(clientBrandSetting.getSubPathUrl());
            clientSubDomain.setCreatedAt(Instant.now());
            clientSubDomain.setModifiedAt(Instant.now());
            if (clientProgram.isPresent()) {
                Program program = clientProgram.get().getProgram();
                if (program != null){
                    clientSubDomain.setProgramId(program.getId());
                    clientSubDomain.setProgramName(program.getName());
                }
            }
            clientSubDomainRepository.save(clientSubDomain);
        }
        return clientBrandSettingMapper.toDto(brandSetting);
    }

    private ClientBrandSettingDTO saveClientBrandSettingWithDTO(ClientBrandSettingDTO clientBrandSettingDTO) {
        Optional<ClientBrandSetting> clientBrandSettingData = clientBrandSettingRepository.findClientBrandSettingBySubPathUrl(clientBrandSettingDTO.getSubPathUrl());
        if (clientBrandSettingData.isPresent()) {
            throw new BadRequestAlertException("SubDomainUrl is existed", "ClientBrandSetting", "SubDomainUrl");
        }

        Option<ClientSubDomain> existed = clientSubDomainRepository.findClientSubDomainBySubDomainUrl(clientBrandSettingDTO.getSubPathUrl());
        if (existed.isDefined()) {
            throw new BadRequestAlertException("SubDomainUrl is existed", "ClientSubDomain", "SubDomainUrl");
        }

        ClientBrandSetting clientBrandSetting = clientBrandSettingMapper.toEntity(clientBrandSettingDTO);
        ClientBrandSetting brandSetting = clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        ClientSubDomain clientSubDomain = new ClientSubDomain();
        clientSubDomain.setClientId(clientBrandSetting.getClientId());
        clientSubDomain.setSubDomainUrl(clientBrandSetting.getSubPathUrl());
        clientSubDomain.setCreatedAt(Instant.now());
        clientSubDomain.setModifiedAt(Instant.now());

        Optional<ClientProgram> clientProgram = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientBrandSetting.getClientId(), ProgramStatus.Active.name(), false);
        if (clientProgram.isPresent()) {
            Program program = clientProgram.get().getProgram();
            if (program != null){
                clientSubDomain.setProgramId(program.getId());
                clientSubDomain.setProgramName(program.getName());
            }
        }

        clientSubDomainRepository.save(clientSubDomain);
        return clientBrandSettingMapper.toDto(brandSetting);
    }

    private ClientBrandSettingDTO getClientBrandSettingDTO (ClientBrandSettingDTO  clientBrandSettingDTO) {
        ClientBrandSettingDTO dto = clientBrandSettingDTO != null ? clientBrandSettingDTO : new ClientBrandSettingDTO();

        List<ClientProgram> clientProgramList = clientProgramRepository.findByClientId(dto.getClientId());

        // find client brand setting
        Optional<ClientProgram> clientProgram = findClientBrandSetting(null, clientProgramList);

        if (clientProgram.isPresent()) {
            Program program = clientProgram.get().getProgram();
            dto.setWebLogoUrl(program.getLandingBackgroundImageUrl());
            dto.setLogoImageUrl(program.getLogoUrl());
            dto.setProgramRegisterMessage(program.getProgramRegisterMessage());
            dto.setProgramName(program.getName());
        }

        return dto;
    }

    private Optional<ClientProgram> findClientBrandSetting (Optional<ClientProgram> clientProgram, List<ClientProgram> clientProgramList) {
        if (clientProgram == null && CollectionUtils.isEmpty(clientProgramList)) {
            return Optional.empty();
        } else if (clientProgram != null && clientProgram.isPresent()) {
            return clientProgram;
        }

        // find active program
        Optional<ClientProgram> activeClientProgram = clientProgramList.stream()
            .filter(cp -> cp.getProgram().getStatus().equalsIgnoreCase(ProgramStatus.Active.toString()))
            .findFirst();
        if (activeClientProgram.isPresent()) {
            return findClientBrandSetting(activeClientProgram, clientProgramList);
        }

        // find draft program
        Optional<ClientProgram> draftClientProgram = clientProgramList.stream()
            .filter(cp -> cp.getProgram().getStatus().equalsIgnoreCase(ProgramStatus.Draft.toString()))
            .sorted(Comparator.nullsLast((cp1, cp2) -> cp2.getProgram().getCreatedDate().compareTo(cp1.getProgram().getCreatedDate())))
            .findFirst();
        if (draftClientProgram.isPresent()) {
            return findClientBrandSetting(draftClientProgram, clientProgramList);
        }

        // find completed program
        Optional<ClientProgram> completedClientProgram = clientProgramList.stream()
            .filter(cp -> cp.getProgram().getStatus().equalsIgnoreCase(ProgramStatus.Completed.toString()))
            .sorted(Comparator.nullsLast((cp1, cp2) -> cp2.getProgram().getCreatedDate().compareTo(cp1.getProgram().getCreatedDate())))
            .findFirst();
        return findClientBrandSetting(completedClientProgram, clientProgramList);
    }
}
