package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.service.ProgramCategoryPointService;
import aduro.basic.programservice.service.businessrule.ProgramCategoryPointBusinessRule;
import aduro.basic.programservice.service.dto.ProgramCategoryPointDTO;
import aduro.basic.programservice.service.dto.ProgramStatus;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;
import aduro.basic.programservice.service.mapper.ProgramCategoryPointMapper;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryPointMapper;
import aduro.basic.programservice.sns.PublisherService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link ProgramCategoryPoint}.
 */
@Service
@Transactional(rollbackFor = Throwable.class)
public class ProgramCategoryPointServiceImpl implements ProgramCategoryPointService {

    private final Logger log = LoggerFactory.getLogger(ProgramCategoryPointServiceImpl.class);

    private final ProgramCategoryPointRepository programCategoryPointRepository;

    private final ProgramCategoryPointMapper programCategoryPointMapper;


    @Autowired
    private ProgramCategoryPointBusinessRule programCategoryPointBusinessRule;

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProgramSubCategoryPointMapper programSubCategoryPointMapper;

    @Autowired
    private ProgramSubCategoryPointRepository programSubCategoryPoinRepository;

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;

    @Autowired
    private PublisherService publisherService;

    public ProgramCategoryPointServiceImpl(ProgramCategoryPointRepository programCategoryPointRepository, ProgramCategoryPointMapper programCategoryPointMapper) {
        this.programCategoryPointRepository = programCategoryPointRepository;
        this.programCategoryPointMapper = programCategoryPointMapper;
    }

    /**
     * Save a programCategoryPoint.
     *
     * @param programCategoryPointDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramCategoryPointDTO save(ProgramCategoryPointDTO programCategoryPointDTO) {
        log.debug("Request to save ProgramCategoryPoint : {}", programCategoryPointDTO);
        //validate category code - make sure available in master data
        Category category = categoryRepository.findByCode(programCategoryPointDTO.getCategoryCode());
        if(category == null){
            throw new BadRequestAlertException(" Category is not exist", "Category", "categoryexist");
        }

        if(programCategoryPointDTO.getId() == null || programCategoryPointDTO.getId() == 0) {
           return HandleCreateCategoryPoint(programCategoryPointDTO, category);
        }else{
           return  HandleUpdateCategoryPoint(programCategoryPointDTO, category);
        }

    }

    private ProgramCategoryPointDTO HandleUpdateCategoryPoint(ProgramCategoryPointDTO programCategoryPointDTO, Category category) {

        Optional<ProgramCategoryPoint> optionalProgramCategoryPoint = programCategoryPointRepository.findById(programCategoryPointDTO.getId());

        if(!optionalProgramCategoryPoint.isPresent())
            throw new BadRequestAlertException(" Can not update category point", "Category", "categoryexist");

        ProgramCategoryPoint  programCategoryPoint = optionalProgramCategoryPoint.get();
        Program program = programCategoryPoint.getProgram();



        programCategoryPoint.setLocked(programCategoryPointDTO.getLocked());
        programCategoryPoint.setPercentPoint(programCategoryPointDTO.getPercentPoint());
        programCategoryPoint.setLocked(programCategoryPointDTO.getLocked());

        List<SubCategory> subCategories = new ArrayList<>(category.getSubCategories());



        List<ProgramSubCategoryPoint> programSubCategoryPoints =  new ArrayList<>(programCategoryPoint.getProgramSubCategoryPoints());
        //Caculate Point before save
        programCategoryPointBusinessRule.CaculatePoint(programCategoryPoint, program.getUserPoint());

        programCategoryPoint = programCategoryPointRepository.save(programCategoryPoint);

        for (ProgramSubCategoryPointDTO subPointDto : programCategoryPointDTO.getProgramSubCategoryPoints())
        {
            if(subPointDto.getId() ==null || subPointDto.getId() ==0 ) continue;

            Optional<ProgramSubCategoryPoint> optionalProgramSubCategoryPoint = programSubCategoryPoints.stream().filter(s->s.getId().equals(subPointDto.getId())).findFirst();
            if(!optionalProgramSubCategoryPoint.isPresent())
                throw new BadRequestAlertException(" Sub-Category id is invalid", "Sub-Category", "categoryexist");
            ProgramSubCategoryPoint subCategoryPoint = optionalProgramSubCategoryPoint.get();

            subCategoryPoint.setLocked(subPointDto.getLocked());
            subCategoryPoint.setPercentPoint(subPointDto.getPercentPoint());
            subCategoryPoint.setCompletionsCap(subPointDto.getCompletionsCap());

            //Caculate sub Point before save
            programCategoryPointBusinessRule.CaculateSubPoint(subCategoryPoint,programCategoryPoint);
            subPointDto.setValuePoint(subCategoryPoint.getValuePoint());

            programSubCategoryPoinRepository.save(subCategoryPoint);

        }

        return programCategoryPointMapper.toDto(programCategoryPoint);
    }

    /*private  void CreateNewSubCategoryPoint(ProgramSubCategoryPointDTO subPointDto, List<SubCategory> subCategories, ProgramCategoryPoint programCategoryPoint){
        if(!subCategories.stream().anyMatch(s->s.getCode().equals(subPointDto.getCode())))
            throw new BadRequestAlertException(" Sub-Category Code is invalid", "Sub-Category", "categoryexist");
        if(programCategoryPoint.getProgramSubCategoryPoints().stream().anyMatch(s->s.getCode().equals(subPointDto.getCode())))
            throw new BadRequestAlertException(" Sub-Category Code is exist", "Sub-Category", "categoryexist");

        ProgramSubCategoryPoint subPoint = programSubCategoryPointMapper.toEntity(subPointDto);
        //Caculate sub Point before add
        subPoint = programCategoryPointBusinessRule.CaculateSubPoint(subPoint,programCategoryPoint);
        subPointDto.setValuePoint(subPoint.getValuePoint());
        programSubCategoryPoinRepository.save(subPoint);
    }*/


    private ProgramCategoryPointDTO HandleCreateCategoryPoint(ProgramCategoryPointDTO programCategoryPointDTO, Category category) {
        //validate programCategoryPoint for new
        ProgramCategoryPoint categoryPoint =
            programCategoryPointRepository.findByCategoryCodeAndProgram_Id(programCategoryPointDTO.getCategoryCode(), programCategoryPointDTO.getProgramId());
        if(categoryPoint != null) {
            throw new BadRequestAlertException(" Can not add category point,It is already exist.", "Category", "categoryexist");
        }
        ProgramCategoryPoint programCategoryPoint = programCategoryPointMapper.toEntity(programCategoryPointDTO);

        //Caculate Point before save
        Optional<Program> program = programRepository.findById(programCategoryPointDTO.getProgramId());
        if(program.isPresent())
            programCategoryPointBusinessRule.CaculatePoint(programCategoryPoint, program.get().getUserPoint());
        programCategoryPoint = programCategoryPointRepository.save(programCategoryPoint);

       /* List<SubCategory> subCategories = new ArrayList<>(category.getSubCategories());
        //Handle Create for sub-category
        for (ProgramSubCategoryPointDTO subPointDto : programCategoryPointDTO.getProgramSubCategoryPoints())
        {
            if(subPointDto.getId() == null ||  subPointDto.getId() ==0)
                CreateNewSubCategoryPoint(subPointDto, subCategories, programCategoryPoint);
        }*/
        ProgramCategoryPointDTO  dto = programCategoryPointMapper.toDto(programCategoryPoint);
        return dto;

    }


    /**
     * Get all the programCategoryPoints.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramCategoryPointDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramCategoryPoints");
        return programCategoryPointRepository.findAll(pageable)
            .map(programCategoryPointMapper::toDto);
    }


    /**
     * Get one programCategoryPoint by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramCategoryPointDTO> findOne(Long id) {
        log.debug("Request to get ProgramCategoryPoint : {}", id);
        return programCategoryPointRepository.findById(id)
            .map(programCategoryPointMapper::toDto);
    }

    /**
     * Delete the programCategoryPoint by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramCategoryPoint : {}", id);
        programCategoryPointRepository.deleteById(id);
    }

    /**
     * Delete the programCategoryPoint by id.
     *
     * @param ids the id of the entity.
     */
    @Override
    public void deletes(List<Long> ids) {
        log.debug("Request to delete ProgramCategoryPoint : {}", ids);
        for (Long id: ids
             ) {
            programCategoryPointRepository.deleteById(id);
        }
    }

    public List<ProgramCategoryPointDTO> updateProgramCategoryPoints(Long programId, List<ProgramCategoryPointDTO> programCategoryPointDTOs) {

        if(programCategoryPointDTOs.size() == 0)
            return programCategoryPointDTOs;

        List<Category> categories = categoryRepository.findAll();

        for (ProgramCategoryPointDTO dto: programCategoryPointDTOs) {
            Optional<Category> category = categories.stream().filter(c->c.getCode().equals(dto.getCategoryCode())).findFirst();

            if(!category.isPresent())
                throw new BadRequestAlertException(" Category is not exist", "Category", "categoryexist");
            HandleUpdateCategoryPoint(dto,category.get());
        }

        Optional<Program> programOptional = programRepository.findById(programId);
        if (!programOptional.isPresent()) {
            throw new BadRequestAlertException("Program is not null.", "Program", "ProgramNotExisted");
        }

        Program program = programOptional.get();

        BigDecimal bigDecimal = programCategoryPointBusinessRule.calculateEconomyPoint(program.getId());
        program.setEconomyPoint(bigDecimal);
        programRepository.save(program);

        if (program.getStatus().equalsIgnoreCase(ProgramStatus.Active.toString())) {
            publisherService.publishProgramUpdatedEvent("", programId);
        }

        return programCategoryPointDTOs;
    }

    public List<ProgramCategoryPointDTO> addAndRemoveProgramCategoryPoints(Long programId, List<ProgramCategoryPointDTO> programCategoryPointDTOs) {

        if(programCategoryPointDTOs.size() == 0)
            return programCategoryPointDTOs;

        List<Category> categories = categoryRepository.findAll();

        List<ProgramCategoryPoint> currentProgramCategoryPoints = programCategoryPointRepository.findAllByProgram_Id(programId);

        //handle Remove
        for (ProgramCategoryPoint entity:currentProgramCategoryPoints) {
            if(programCategoryPointDTOs.stream().anyMatch(dto ->  dto.getCategoryCode().equals(entity.getCategoryCode()))) continue;
            programCategoryPointRepository.delete(entity);
        }

        //handle add
        for (ProgramCategoryPointDTO dto: programCategoryPointDTOs) {
            if(currentProgramCategoryPoints.stream().anyMatch(p->p.getCategoryCode().equals(dto.getCategoryCode())))
                continue;
            Optional<Category> category = categories.stream().filter(c->c.getCode().equals(dto.getCategoryCode())).findFirst();
            if(!category.isPresent())
                throw new BadRequestAlertException(" Category is not exist", "Category", "categoryexist");

            dto.setProgramId(programId);
            dto.setCategoryName(category.get().getName());

            ProgramCategoryPoint c = programCategoryPointRepository.save(programCategoryPointMapper.toEntity(dto));
            dto.setId(c.getId());
        }

        currentProgramCategoryPoints = programCategoryPointRepository.findAllByProgram_Id(programId);

        // reset cache of program from Platform
        if (!CollectionUtils.isEmpty(currentProgramCategoryPoints)) {
            Program program = currentProgramCategoryPoints.stream()
                .map(c -> c.getProgram())
                .filter(p -> p.getStatus() != null && p.getStatus().equalsIgnoreCase(ProgramStatus.Active.toString()))
                .findFirst().orElse(null);
            if (program != null) {
                publisherService.publishProgramUpdatedEvent("", program.getId());
            }
        }

        return programCategoryPointMapper.toDto(currentProgramCategoryPoints);
    }

    @Override
    @Transactional(readOnly = true)
    public ProgramCategoryPointDTO getProgramCategoryPointByClientIdAnCategory(String clientId, String categoryCode) {

        ProgramCategoryPointDTO dto = new ProgramCategoryPointDTO();

        if(isNullOrEmpty(clientId) || isNullOrEmpty(categoryCode))
            return dto;

        ArrayList<String> status = new ArrayList<>();

        status.add(ProgramStatus.Active.toString());

        // get client program is not qc
        List<ClientProgram> cpList = clientProgramRepository.findByClientIdAndProgramStatusIn(clientId, status);


        if(cpList == null || cpList.isEmpty()) {
            return dto;
        }

        // expect 1 result
        for(ClientProgram cp: cpList) {
            ProgramCategoryPoint programCategoryPoint = programCategoryPointRepository.findByCategoryCodeAndProgram_Id(categoryCode, cp.getProgramId());
            if(programCategoryPoint == null) return dto;

            return programCategoryPointMapper.toDto(programCategoryPoint);
        }
        return new ProgramCategoryPointDTO();
    }

    @Override
    @Transactional(readOnly = true)
    public ProgramCategoryPointDTO getProgramCategoryPointByUser(String clientId, String participantId, String categoryCode, Map<String, String> queryParams) {
        ProgramCategoryPointDTO dto = new ProgramCategoryPointDTO();

        if(isNullOrEmpty(clientId) || isNullOrEmpty(categoryCode) || isNullOrEmpty(participantId))
            return dto;

        UserDto user = customActivityAdapter.parseUserParams(participantId, queryParams);;
        if(user == null){
            throw new BadRequestAlertException("Participant is Not Found", "ActivityEventDTO", "GetParticpantInfo");
        }
        Optional<ClientProgram> cp = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.toString(), user.isAsProgramTester());
        if(!cp.isPresent()) return dto;

        ClientProgram clientProgram = cp.get();
        return _getProgramCategoryPoint(clientProgram.getProgramId(), categoryCode, participantId);
    }

    @Override
    public ProgramCategoryPointDTO getProgramCategoryPointByProgramId(Long programId, String categoryCode) {
        return _getProgramCategoryPoint(programId, categoryCode, null);
    }

    private ProgramCategoryPointDTO _getProgramCategoryPoint(Long programId, String categoryCode, String participantId) {
        ProgramCategoryPointDTO dto = new ProgramCategoryPointDTO();
        ProgramCategoryPoint programCategoryPoint  = programCategoryPointRepository.findByCategoryCodeAndProgram_Id(categoryCode, programId);
        if(programCategoryPoint == null) return dto;
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointMapper.toDto(programCategoryPoint);

        if (isNullOrEmpty(participantId)) {
            return programCategoryPointDTO;
        }

        for (ProgramSubCategoryPointDTO subCategoryPointDTO: programCategoryPointDTO.getProgramSubCategoryPoints()) {
            if(subCategoryPointDTO.getValuePoint() == null)
                subCategoryPointDTO.setValuePoint(BigDecimal.valueOf(0.0));

            if(subCategoryPointDTO.getValuePoint().equals(0)) continue;

            Integer completionCount = userEventRepository.countAllByEventCodeAndProgramUserProgramIdAndProgramUserParticipantId(subCategoryPointDTO.getCode(), programId, participantId);
            if(subCategoryPointDTO.getCompletionsCap() == null || completionCount >= subCategoryPointDTO.getCompletionsCap()) {
                subCategoryPointDTO.setValuePoint(BigDecimal.valueOf(0.0));
            }

        }
        return programCategoryPointDTO;
    }


}
