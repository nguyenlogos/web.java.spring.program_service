package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.ap.NotificationAdapter;
import aduro.basic.programservice.ap.dto.NotificationItemRequest;
import aduro.basic.programservice.ap.dto.NotificationItemResponse;
import aduro.basic.programservice.cc.EmployeeServiceRest;
import aduro.basic.programservice.cc.dto.ESProfileDto;
import aduro.basic.programservice.cc.dto.ESResponseDto;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.repository.ClientSettingRepository;
import aduro.basic.programservice.repository.OrderTransactionRepository;
import aduro.basic.programservice.repository.ProgramUserRepository;
import aduro.basic.programservice.service.UserRewardService;
import aduro.basic.programservice.repository.UserRewardRepository;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.ClientProgramRewardSettingMapper;
import aduro.basic.programservice.service.mapper.UserRewardMapper;
import aduro.basic.programservice.service.util.RandomUtil;
import aduro.basic.programservice.tangocard.AccountAdapter;
import aduro.basic.programservice.tangocard.CatalogAdapter;
import aduro.basic.programservice.tangocard.OrderAdapter;
import aduro.basic.programservice.tangocard.dto.AccountResponse;
import aduro.basic.programservice.tangocard.dto.OrderRequest;
import aduro.basic.programservice.tangocard.dto.OrderResponse;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import aduro.basic.programservice.config.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link UserReward}.
 */
@Service
public class UserRewardServiceImpl implements UserRewardService {

    private final Logger log = LoggerFactory.getLogger(UserRewardServiceImpl.class);

    private final UserRewardRepository userRewardRepository;

    private final UserRewardMapper userRewardMapper;

    private final static String ERROR_ES_FAILED_TO_GET_INFO = "Error ES failed to get user information!";



    @Autowired
    OrderAdapter orderAdapter;

    @Autowired
    private Settings settings;

    @Autowired
    CatalogAdapter catalogAdapter;

    @Autowired
    ClientSettingRepository clientSettingRepository;

    @Autowired
    ClientProgramRewardSettingMapper clientProgramRewardSettingRepositoryMapper;

    @Autowired
    OrderTransactionRepository orderTransactionRepository;

    @Autowired
    NotificationAdapter notificationAdapter;

    @Autowired
    private AccountAdapter accountAdapter;

    private final EmployeeServiceRest employeeServiceRest;

    private final ProgramUserRepository programUserRepository;

    public UserRewardServiceImpl(UserRewardRepository userRewardRepository, UserRewardMapper userRewardMapper, EmployeeServiceRest employeeServiceRest, ProgramUserRepository programUserRepository) {
        this.userRewardRepository = userRewardRepository;
        this.userRewardMapper = userRewardMapper;
        this.employeeServiceRest = employeeServiceRest;
        this.programUserRepository = programUserRepository;
    }

    /**
     * Save a userReward.
     *
     * @param userRewardDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    @Transactional
    public UserRewardDTO save(UserRewardDTO userRewardDTO) {
        log.debug("Request to save UserReward : {}", userRewardDTO);
        UserReward userReward = userRewardMapper.toEntity(userRewardDTO);
        userReward = userRewardRepository.save(userReward);
        return userRewardMapper.toDto(userReward);
    }

    /**
     * Get all the userRewards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<UserRewardDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserRewards");
        return userRewardRepository.findAll(pageable)
            .map(userRewardMapper::toDto);
    }


    /**
     * Get one userReward by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<UserRewardDTO> findOne(Long id) {
        log.debug("Request to get UserReward : {}", id);
        return userRewardRepository.findById(id)
            .map(userRewardMapper::toDto);
    }

    /**
     * Delete the userReward by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserReward : {}", id);
        userRewardRepository.deleteById(id);
    }

    @Override
    public void triggerUserRewards() {
        try {
            this.processUserRewards();
        } catch (Exception e) {
            log.error("trigger reward error: " + e.getMessage());
        }
    }

    public void proccessTangoCardRewards() throws Exception {
        log.info("Start schedule daily make tango card rewards order: " + Instant.now().toString());

        List<UserReward> userRewards = userRewardRepository.findUserRewardsByStatusAndRewardTypeOrderByLevelCompletedDateAsc(IncentiveStatusEnum.New.toString(), RewardTypeEnum.TangoCard.toString());
        log.info("rewards count: " + userRewards.size());
        for (UserReward userReward: userRewards) {
            OrderTransaction orderTransaction = new OrderTransaction();
            try{
                userReward.setStatus(IncentiveStatusEnum.Running.toString());
                userRewardRepository.saveAndFlush(userReward);
                String uniqueID = UUID.randomUUID().toString();

                orderTransaction.setProgramId(userReward.getProgramUser().getProgramId());
                orderTransaction.setProgramName(userReward.getProgram().getName());
                orderTransaction.setClientName(userReward.getProgramUser().getClientName());
                orderTransaction.setCreatedDate(Instant.now());
                orderTransaction.setUserRewardId(userReward.getId());
                orderTransaction.setParticipantName(userReward.getProgramUser().getEmail());
                orderTransaction.setExternalOrderId(uniqueID.toString());

              ///  processTangoCardReward(userReward, orderTransaction);
                userReward.setStatus(IncentiveStatusEnum.Completed.toString());

            }catch (Exception ex){
                userReward.setStatus(IncentiveStatusEnum.Error.toString());
                orderTransaction.setMessage(ex.getMessage());
            }
            finally {
                userRewardRepository.save(userReward);
                orderTransactionRepository.save(orderTransaction);
            }

        }


    }



    private void processTangoCardReward(UserReward userReward, OrderTransaction orderTransaction, ClientSetting clientSetting, ESProfileDto employeeProfile) throws Exception {

     //   ClientSetting clientSetting = clientSettingRepository.findClientSettingByClientId(userReward.getClientId());

       /* if(clientSetting == null || isNullOrEmpty(clientSetting.getTangoAccountIdentifier()) || isNullOrEmpty(clientSetting.getTangoCustomerIdentifier()) )
            throw new Exception("Missing configuration client account for tango card");
*/
        if(isNullOrEmpty(userReward.getCampaignId()))
            throw new Exception("Missing reward item");

        String notes =  "";
        if (employeeProfile!= null && employeeProfile.getEmployeeId() != null) {
            notes = Constants.MEMBER_ID + ": " + employeeProfile.getEmployeeId();
        }
        OrderRequest orderRequest = new OrderRequest(clientSetting.getTangoAccountIdentifier(), userReward.getRewardAmount().floatValue(), clientSetting.getTangoCustomerIdentifier(), orderTransaction.getExternalOrderId()
        ,userReward.getEmail(),userReward.getProgramUser().getFirstName(), userReward.getProgramUser().getLastName(),true, clientSetting.getClientEmail(), clientSetting.getClientName(), clientSetting.getClientName(),userReward.getCampaignId(), notes);

        orderTransaction.setJsonContent(RandomUtil.convertObjectToJsonString(orderRequest));

        CompletableFuture<OrderResponse> orderResponseCompletableFuture =  orderAdapter.CreateOrderAsync(orderRequest);

        OrderResponse orderResponse = orderResponseCompletableFuture.get();

        userReward.setStatus(IncentiveStatusEnum.Completed.toString());

        userReward.setOrderId(orderResponse.getReferenceOrderID());
        userReward.setOrderDate(Instant.now());

        //Notify to end user for reward
        if(orderResponse.getReward() != null && orderResponse.getReward().getCredentialList() != null && orderResponse.getReward().getCredentialList().size()>0){
            String redemptionLink = orderResponse.getReward().getCredentialList().get(0).getValue();
            notifyUser(userReward, redemptionLink);
        }


        userRewardRepository.save(userReward);
    }

    @Scheduled(cron = "0 */15 * * * *")
    @SchedulerLock(name = "processUserRewards")
    public void processUserRewards() throws Exception {
        if(!settings.isScronJobEnabled()){
            return;
        }
        log.info("Start schedule for rewards order: " + Instant.now().toString());
        //Get all reward with status new.
        List<UserReward> userRewards = userRewardRepository.findUserRewardsByStatusOrderByLevelCompletedDateAsc(IncentiveStatusEnum.New.toString());
        log.info("rewards count: " + userRewards.size());
        for (UserReward userReward: userRewards) {
            log.info("user reward participant id: " + userReward.getParticipantId());
            ESResponseDto employeeProfileData = employeeServiceRest.getEmployeeProfile(userReward.getParticipantId());
            log.info("user reward employeeProfileData: " + employeeProfileData);
            boolean isTangoCard = userReward.getRewardType().equals(RewardTypeEnum.TangoCard.toString());
            log.info("user reward id: " + userReward.getId() + " tango card: " + isTangoCard);

            OrderTransaction orderTransaction = null;
            if (isTangoCard) {
                boolean isSave = true;
                try{
                    ClientSetting clientSetting = clientSettingRepository.findClientSettingByClientId(userReward.getClientId());
                    if(clientSetting == null) {
                        isSave =false;
                        continue;
                    }

                    CompletableFuture<AccountResponse> accountResponseCompletableFuture =  accountAdapter.GetAccountDetailAsync(clientSetting.getTangoAccountIdentifier());
                    AccountResponse accountResponse = accountResponseCompletableFuture.get();
                    if(accountResponse.getCurrentBalance().compareTo(userReward.getRewardAmount()) < 0){
                        // TODO need to notify the client in case their balance is 0.
                        isSave =false;
                        continue;
                    }

                    orderTransaction = generateOrderTransaction(userReward);
                    if (employeeProfileData != null && employeeProfileData.getData() != null) {
                        // update profile for program user
                        ESProfileDto employeeProfile = employeeProfileData.getData();
                        userReward.getProgramUser().setFirstName(employeeProfile.getFirstName());
                        userReward.getProgramUser().setLastName(employeeProfile.getLastName());
                        userReward.getProgramUser().setPhone(employeeProfile.getPhone());
                        programUserRepository.save(userReward.getProgramUser());

                        userReward.setStatus(IncentiveStatusEnum.Running.toString());
                        userRewardRepository.saveAndFlush(userReward);

                        processTangoCardReward(userReward, orderTransaction, clientSetting, employeeProfileData.getData());
                        userReward.setStatus(IncentiveStatusEnum.Completed.toString());
                    } else {
                        // save the error status if ES is failed
                        userReward.setStatus(IncentiveStatusEnum.Error.toString());
                        orderTransaction.setMessage(ERROR_ES_FAILED_TO_GET_INFO);
                    }
                }catch (Exception ex){
                    userReward.setStatus(IncentiveStatusEnum.Error.toString());
                    orderTransaction = generateOrderTransaction(userReward);
                    orderTransaction.setMessage(ex.getMessage());
                    log.error("reward error: " + ex.getMessage());
                }
                finally {
                    if(isSave){
                        userRewardRepository.save(userReward);

                        if (orderTransaction != null) {
                            orderTransactionRepository.save(orderTransaction);
                        }
                    }
                }
            } else {
                notifyUser(userReward,"");
                userReward.setStatus(IncentiveStatusEnum.Completed.toString());
                userRewardRepository.save(userReward);
            }
        }

    }

    private OrderTransaction generateOrderTransaction (UserReward userReward) {
        String uniqueID = UUID.randomUUID().toString();
        OrderTransaction orderTransaction = new OrderTransaction();
        orderTransaction.setProgramId(userReward.getProgramUser().getProgramId());
        orderTransaction.setProgramName(userReward.getProgram().getName());
        orderTransaction.setClientName(userReward.getProgramUser().getClientName());
        orderTransaction.setCreatedDate(Instant.now());
        orderTransaction.setUserRewardId(userReward.getId());
        orderTransaction.setParticipantName(userReward.getProgramUser().getEmail());
        orderTransaction.setExternalOrderId(uniqueID.toString());
        return orderTransaction;
    }


    private void notifyUser(UserReward reward, String redemptionLink) {
        try{
            String content = "Congratulations! You receviced a new reward from your Aduro Wellness program.\n";

            if(reward.getRewardType().equals(RewardTypeEnum.TangoCard.toString())){
                content += String.format("$%s %s",reward.getRewardAmount(),"gift card");
            }
            else if(reward.getRewardType().equals(RewardTypeEnum.HealthSavings.toString())){
                content += String.format("$%s/%s %s",reward.getRewardAmount(), reward.getRewardCode()," added to your HSA");
            }
            else if(reward.getRewardType().equals(RewardTypeEnum.PayrollCredits.toString())){
                content += String.format("$%s %s",reward.getRewardAmount()," added to your payroll");
            }
            else if(reward.getRewardType().equals(RewardTypeEnum.PaidTimeOff.toString())){
                content += String.format("$%s %s %s",reward.getRewardAmount(), reward.getRewardCode()," of paid time off");
            }
            else if(reward.getRewardType().equals(RewardTypeEnum.Custom.toString())){
                content += reward.getRewardCode();
            }

           /* if(!isNullOrEmpty(redemptionLink)){
                content = String.format("\n %s: %s",reward.getRewardCode(),redemptionLink);
            }*/

            NotificationItemRequest notificationItemRequest = new NotificationItemRequest();
            notificationItemRequest.setRewardContent(content);
            notificationItemRequest.setParticipantId(reward.getParticipantId());
            notificationItemRequest.setTitle("New Reward! \uD83C\uDF81");

            CompletableFuture<NotificationItemResponse> orderResponseCompletableFuture =  notificationAdapter.AddNotificationItem(notificationItemRequest);
            NotificationItemResponse rs = orderResponseCompletableFuture.get();
        }catch (Exception ex){

            log.error("notification error:" + reward.getId());
            log.error(ex.getMessage());
        }


    }

}
