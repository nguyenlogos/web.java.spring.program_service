package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.WellmetricEventQueueService;
import aduro.basic.programservice.domain.WellmetricEventQueue;
import aduro.basic.programservice.repository.WellmetricEventQueueRepository;
import aduro.basic.programservice.service.dto.WellmetricEventQueueDTO;
import aduro.basic.programservice.service.mapper.WellmetricEventQueueMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link WellmetricEventQueue}.
 */
@Service
@Transactional
public class WellmetricEventQueueServiceImpl implements WellmetricEventQueueService {

    private final Logger log = LoggerFactory.getLogger(WellmetricEventQueueServiceImpl.class);

    private final WellmetricEventQueueRepository wellmetricEventQueueRepository;

    private final WellmetricEventQueueMapper wellmetricEventQueueMapper;

    public WellmetricEventQueueServiceImpl(WellmetricEventQueueRepository wellmetricEventQueueRepository, WellmetricEventQueueMapper wellmetricEventQueueMapper) {
        this.wellmetricEventQueueRepository = wellmetricEventQueueRepository;
        this.wellmetricEventQueueMapper = wellmetricEventQueueMapper;
    }

    /**
     * Save a wellmetricEventQueue.
     *
     * @param wellmetricEventQueueDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public WellmetricEventQueueDTO save(WellmetricEventQueueDTO wellmetricEventQueueDTO) {
        log.debug("Request to save WellmetricEventQueue : {}", wellmetricEventQueueDTO);
        WellmetricEventQueue wellmetricEventQueue = wellmetricEventQueueMapper.toEntity(wellmetricEventQueueDTO);
        wellmetricEventQueue = wellmetricEventQueueRepository.save(wellmetricEventQueue);
        return wellmetricEventQueueMapper.toDto(wellmetricEventQueue);
    }

    /**
     * Get all the wellmetricEventQueues.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<WellmetricEventQueueDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WellmetricEventQueues");
        return wellmetricEventQueueRepository.findAll(pageable)
            .map(wellmetricEventQueueMapper::toDto);
    }


    /**
     * Get one wellmetricEventQueue by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<WellmetricEventQueueDTO> findOne(Long id) {
        log.debug("Request to get WellmetricEventQueue : {}", id);
        return wellmetricEventQueueRepository.findById(id)
            .map(wellmetricEventQueueMapper::toDto);
    }

    /**
     * Delete the wellmetricEventQueue by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete WellmetricEventQueue : {}", id);
        wellmetricEventQueueRepository.deleteById(id);
    }
}
