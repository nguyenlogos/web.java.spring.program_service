package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramLevelPracticeService;
import aduro.basic.programservice.domain.ProgramLevelPractice;
import aduro.basic.programservice.repository.ProgramLevelPracticeRepository;
import aduro.basic.programservice.service.dto.ProgramLevelPracticeDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelPracticeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramLevelPractice}.
 */
@Service
@Transactional
public class ProgramLevelPracticeServiceImpl implements ProgramLevelPracticeService {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelPracticeServiceImpl.class);

    private final ProgramLevelPracticeRepository programLevelPracticeRepository;

    private final ProgramLevelPracticeMapper programLevelPracticeMapper;

    public ProgramLevelPracticeServiceImpl(ProgramLevelPracticeRepository programLevelPracticeRepository, ProgramLevelPracticeMapper programLevelPracticeMapper) {
        this.programLevelPracticeRepository = programLevelPracticeRepository;
        this.programLevelPracticeMapper = programLevelPracticeMapper;
    }

    /**
     * Save a programLevelPractice.
     *
     * @param programLevelPracticeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramLevelPracticeDTO save(ProgramLevelPracticeDTO programLevelPracticeDTO) {
        log.debug("Request to save ProgramLevelPractice : {}", programLevelPracticeDTO);
        ProgramLevelPractice programLevelPractice = programLevelPracticeMapper.toEntity(programLevelPracticeDTO);
        programLevelPractice = programLevelPracticeRepository.save(programLevelPractice);
        return programLevelPracticeMapper.toDto(programLevelPractice);
    }

    /**
     * Get all the programLevelPractices.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramLevelPracticeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramLevelPractices");
        return programLevelPracticeRepository.findAll(pageable)
            .map(programLevelPracticeMapper::toDto);
    }


    /**
     * Get one programLevelPractice by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramLevelPracticeDTO> findOne(Long id) {
        log.debug("Request to get ProgramLevelPractice : {}", id);
        return programLevelPracticeRepository.findById(id)
            .map(programLevelPracticeMapper::toDto);
    }

    /**
     * Delete the programLevelPractice by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramLevelPractice : {}", id);
        programLevelPracticeRepository.deleteById(id);
    }
}
