package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.PathAdapter;
import aduro.basic.programservice.ap.dto.PathDTO;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.domain.ProgramLevelPath;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.service.ProgramLevelPathService;
import aduro.basic.programservice.service.dto.ProgramLevelPathDTO;
import aduro.basic.programservice.service.dto.ProgramStatus;
import aduro.basic.programservice.service.mapper.ProgramLevelPathMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

/**
 * Service Implementation for managing {@link ProgramLevelPath}.
 */
@Service
@Transactional
public class ProgramLevelPathServiceImpl implements ProgramLevelPathService {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelPathServiceImpl.class);

    private final ProgramLevelPathRepository programLevelPathRepository;

    private final ProgramLevelPathMapper programLevelPathMapper;

    private final ProgramRepository programRepository;

    private final ProgramUserRepository programUserRepository;

    private final ProgramLevelRepository programLevelRepository;

    private final CustomActivityAdapter customActivityAdapter;

    private final PathAdapter pathAdapter;

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    public ProgramLevelPathServiceImpl(ProgramLevelPathRepository programLevelPathRepository, ProgramLevelPathMapper programLevelPathMapper, ProgramRepository programRepository, ProgramUserRepository programUserRepository, ProgramLevelRepository programLevelRepository, CustomActivityAdapter customActivityAdapter, PathAdapter pathAdapter) {
        this.programLevelPathRepository = programLevelPathRepository;
        this.programLevelPathMapper = programLevelPathMapper;
        this.programRepository = programRepository;
        this.programUserRepository = programUserRepository;
        this.programLevelRepository = programLevelRepository;
        this.customActivityAdapter = customActivityAdapter;
        this.pathAdapter = pathAdapter;
    }

    /**
     * Save a programLevelPath.
     *
     * @param programLevelPathDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramLevelPathDTO save(ProgramLevelPathDTO programLevelPathDTO) {
        log.debug("Request to save ProgramLevelPath : {}", programLevelPathDTO);
        ProgramLevelPath programLevelPath = programLevelPathMapper.toEntity(programLevelPathDTO);
        programLevelPath = programLevelPathRepository.save(programLevelPath);

        // attach topic if program is assigned to client.
        if (programLevelPath.getProgramLevel() != null) {
            ProgramLevel programLevel = programLevelRepository.getOne(programLevelPath.getProgramLevel().getId());
            if (programLevel.getProgram().getClientPrograms() != null) {
                String clientId = programLevel.getProgram().getClientPrograms().stream()
                    .map(c -> c.getClientId())
                    .findFirst().orElse("");

                if (!StringUtils.isEmpty(clientId)) {
                    PathDTO pathDTO = new PathDTO();
                    pathDTO.setClientId(clientId);
                    pathDTO.setPathId(Long.valueOf(programLevelPathDTO.getPathId()));

                    try {
                        pathAdapter.attachPathToCompany(pathDTO);
                    } catch (Exception e) {
                        log.error("Request to save ProgramLevelPath : {}", e.getMessage());
                    }
                }
            }
        }

        return programLevelPathMapper.toDto(programLevelPath);
    }

    /**
     * Get all the programLevelPaths.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramLevelPathDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramLevelPaths");
        return programLevelPathRepository.findAll(pageable)
            .map(programLevelPathMapper::toDto);
    }


    /**
     * Get one programLevelPath by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramLevelPathDTO> findOne(Long id) {
        log.debug("Request to get ProgramLevelPath : {}", id);
        return programLevelPathRepository.findById(id)
            .map(programLevelPathMapper::toDto);
    }

    /**
     * Delete the programLevelPath by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramLevelPath : {}", id);
        programLevelPathRepository.deleteById(id);
    }

    @Override
    public List<ProgramLevelPathDTO> updateLevelPaths(Long programLevelId, List<ProgramLevelPathDTO> programLevelPathDTOs) {
        List<ProgramLevelPath> currentLevelPaths = programLevelPathRepository.findProgramLevelPathsByProgramLevelId(programLevelId);

        //handle delete
        for (ProgramLevelPath entity:currentLevelPaths) {

            if(programLevelPathDTOs.stream().anyMatch(dto -> dto.getId() != null &&  dto.getId().equals(entity.getId()))) continue;
            programLevelPathRepository.delete(entity);
        }

        for (ProgramLevelPathDTO dto:programLevelPathDTOs) {
            if (dto.getId() == null || dto.getId() == 0) {
                if (dto.getPathId() != null && dto.getPathId() != "") {
                    programLevelPathRepository.save(programLevelPathMapper.toEntity(dto));
                } else {
                    throw new BadRequestAlertException("Path id can not null", "ProgramLevelPath", "program-level-path");
                }
            }
        }

        currentLevelPaths = programLevelPathRepository.findProgramLevelPathsByProgramLevelId(programLevelId);
        return programLevelPathMapper.toDto(currentLevelPaths) ;

    }

    @Override
    @Transactional(readOnly = true)
    public List<ProgramLevelPathDTO> getProgramLevelPathsByClientIdAndParticipantId(String clientId, String participantId, @RequestParam Map<String, String> queryParams) {

        List<ProgramLevelPathDTO> programLevelPathDTOList = new ArrayList<>() ;

        if(isNullOrEmpty(clientId))
            return programLevelPathDTOList;

        UserDto user =  customActivityAdapter.parseUserParams(participantId, queryParams);;
        if( user == null){
            throw new BadRequestAlertException("Participant is Not Found", "CheckUser", "getProgramLevelPathsByClientIdAndParticipantId");
        }
        Optional<ClientProgram> cp = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.toString(), user.isAsProgramTester());

        if(!cp.isPresent()) return programLevelPathDTOList;

         return cp.flatMap(clientProgram -> programRepository.findById(clientProgram.getProgramId()))
            .flatMap(program -> programUserRepository.findProgramUserByParticipantIdAndProgramId(participantId, program.getId()))
            .flatMap(this::findProgramLevelByProgramUser)
            .flatMap(this::getProgramLevelPathDTOS)
             .orElse(new ArrayList<>());
    }

    private Optional<List<ProgramLevelPathDTO>> getProgramLevelPathDTOS(ProgramLevel programLevel) {
        return Optional.of(programLevelPathMapper.toDto(new ArrayList<>(programLevel.getProgramLevelPaths())));
    }

    /*get ProgramLevel of Participant By current level*/
    private Optional<ProgramLevel> findProgramLevelByProgramUser(ProgramUser programUser) {
        return programLevelRepository.findProgramLevelsByProgram_Id(programUser.getProgramId()).stream()
            .filter(programLevel -> programLevel.getLevelOrder().equals(programUser.getCurrentLevel())).findFirst();
    }
}
