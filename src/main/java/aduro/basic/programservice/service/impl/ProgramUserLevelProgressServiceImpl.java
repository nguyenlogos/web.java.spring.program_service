package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramUserLevelProgressService;
import aduro.basic.programservice.domain.ProgramUserLevelProgress;
import aduro.basic.programservice.repository.ProgramUserLevelProgressRepository;
import aduro.basic.programservice.service.dto.ProgramUserLevelProgressDTO;
import aduro.basic.programservice.service.mapper.ProgramUserLevelProgressMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramUserLevelProgress}.
 */
@Service
@Transactional
public class ProgramUserLevelProgressServiceImpl implements ProgramUserLevelProgressService {

    private final Logger log = LoggerFactory.getLogger(ProgramUserLevelProgressServiceImpl.class);

    private final ProgramUserLevelProgressRepository programUserLevelProgressRepository;

    private final ProgramUserLevelProgressMapper programUserLevelProgressMapper;

    public ProgramUserLevelProgressServiceImpl(ProgramUserLevelProgressRepository programUserLevelProgressRepository, ProgramUserLevelProgressMapper programUserLevelProgressMapper) {
        this.programUserLevelProgressRepository = programUserLevelProgressRepository;
        this.programUserLevelProgressMapper = programUserLevelProgressMapper;
    }

    /**
     * Save a programUserLevelProgress.
     *
     * @param programUserLevelProgressDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramUserLevelProgressDTO save(ProgramUserLevelProgressDTO programUserLevelProgressDTO) {
        log.debug("Request to save ProgramUserLevelProgress : {}", programUserLevelProgressDTO);
        ProgramUserLevelProgress programUserLevelProgress = programUserLevelProgressMapper.toEntity(programUserLevelProgressDTO);
        programUserLevelProgress = programUserLevelProgressRepository.save(programUserLevelProgress);
        return programUserLevelProgressMapper.toDto(programUserLevelProgress);
    }

    /**
     * Get all the programUserLevelProgresses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramUserLevelProgressDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramUserLevelProgresses");
        return programUserLevelProgressRepository.findAll(pageable)
            .map(programUserLevelProgressMapper::toDto);
    }


    /**
     * Get one programUserLevelProgress by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramUserLevelProgressDTO> findOne(Long id) {
        log.debug("Request to get ProgramUserLevelProgress : {}", id);
        return programUserLevelProgressRepository.findById(id)
            .map(programUserLevelProgressMapper::toDto);
    }

    /**
     * Delete the programUserLevelProgress by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramUserLevelProgress : {}", id);
        programUserLevelProgressRepository.deleteById(id);
    }
}
