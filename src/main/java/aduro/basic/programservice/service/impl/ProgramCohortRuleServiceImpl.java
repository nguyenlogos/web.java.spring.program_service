package aduro.basic.programservice.service.impl;

import aduro.basic.programservice.service.ProgramCohortRuleService;
import aduro.basic.programservice.domain.ProgramCohortRule;
import aduro.basic.programservice.repository.ProgramCohortRuleRepository;
import aduro.basic.programservice.service.dto.ProgramCohortRuleDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortRuleMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProgramCohortRule}.
 */
@Service
@Transactional
public class ProgramCohortRuleServiceImpl implements ProgramCohortRuleService {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortRuleServiceImpl.class);

    private final ProgramCohortRuleRepository programCohortRuleRepository;

    private final ProgramCohortRuleMapper programCohortRuleMapper;

    public ProgramCohortRuleServiceImpl(ProgramCohortRuleRepository programCohortRuleRepository, ProgramCohortRuleMapper programCohortRuleMapper) {
        this.programCohortRuleRepository = programCohortRuleRepository;
        this.programCohortRuleMapper = programCohortRuleMapper;
    }

    /**
     * Save a programCohortRule.
     *
     * @param programCohortRuleDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public ProgramCohortRuleDTO save(ProgramCohortRuleDTO programCohortRuleDTO) {
        log.debug("Request to save ProgramCohortRule : {}", programCohortRuleDTO);
        ProgramCohortRule programCohortRule = programCohortRuleMapper.toEntity(programCohortRuleDTO);
        programCohortRule = programCohortRuleRepository.save(programCohortRule);
        return programCohortRuleMapper.toDto(programCohortRule);
    }

    /**
     * Get all the programCohortRules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<ProgramCohortRuleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProgramCohortRules");
        return programCohortRuleRepository.findAll(pageable)
            .map(programCohortRuleMapper::toDto);
    }


    /**
     * Get one programCohortRule by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<ProgramCohortRuleDTO> findOne(Long id) {
        log.debug("Request to get ProgramCohortRule : {}", id);
        return programCohortRuleRepository.findById(id)
            .map(programCohortRuleMapper::toDto);
    }

    /**
     * Delete the programCohortRule by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProgramCohortRule : {}", id);
        programCohortRuleRepository.deleteById(id);
    }
}
