package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ClientProgramRewardSetting;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ClientProgramRewardSettingRepository;
import aduro.basic.programservice.service.dto.ClientProgramRewardSettingCriteria;
import aduro.basic.programservice.service.dto.ClientProgramRewardSettingDTO;
import aduro.basic.programservice.service.mapper.ClientProgramRewardSettingMapper;

/**
 * Service for executing complex queries for {@link ClientProgramRewardSetting} entities in the database.
 * The main input is a {@link ClientProgramRewardSettingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ClientProgramRewardSettingDTO} or a {@link Page} of {@link ClientProgramRewardSettingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ClientProgramRewardSettingQueryService extends QueryService<ClientProgramRewardSetting> {

    private final Logger log = LoggerFactory.getLogger(ClientProgramRewardSettingQueryService.class);

    private final ClientProgramRewardSettingRepository clientProgramRewardSettingRepository;

    private final ClientProgramRewardSettingMapper clientProgramRewardSettingMapper;

    public ClientProgramRewardSettingQueryService(ClientProgramRewardSettingRepository clientProgramRewardSettingRepository, ClientProgramRewardSettingMapper clientProgramRewardSettingMapper) {
        this.clientProgramRewardSettingRepository = clientProgramRewardSettingRepository;
        this.clientProgramRewardSettingMapper = clientProgramRewardSettingMapper;
    }

    /**
     * Return a {@link List} of {@link ClientProgramRewardSettingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ClientProgramRewardSettingDTO> findByCriteria(ClientProgramRewardSettingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ClientProgramRewardSetting> specification = createSpecification(criteria);
        return clientProgramRewardSettingMapper.toDto(clientProgramRewardSettingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ClientProgramRewardSettingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ClientProgramRewardSettingDTO> findByCriteria(ClientProgramRewardSettingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ClientProgramRewardSetting> specification = createSpecification(criteria);
        return clientProgramRewardSettingRepository.findAll(specification, page)
            .map(clientProgramRewardSettingMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ClientProgramRewardSettingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ClientProgramRewardSetting> specification = createSpecification(criteria);
        return clientProgramRewardSettingRepository.count(specification);
    }

    /**
     * Function to convert ClientProgramRewardSettingCriteria to a {@link Specification}.
     */
    private Specification<ClientProgramRewardSetting> createSpecification(ClientProgramRewardSettingCriteria criteria) {
        Specification<ClientProgramRewardSetting> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ClientProgramRewardSetting_.id));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), ClientProgramRewardSetting_.clientId));
            }
            if (criteria.getClientName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientName(), ClientProgramRewardSetting_.clientName));
            }
            if (criteria.getUpdatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdatedDate(), ClientProgramRewardSetting_.updatedDate));
            }
            if (criteria.getUpdatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUpdatedBy(), ClientProgramRewardSetting_.updatedBy));
            }
            if (criteria.getCustomerIdentifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCustomerIdentifier(), ClientProgramRewardSetting_.customerIdentifier));
            }
            if (criteria.getAccountIdentifier() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAccountIdentifier(), ClientProgramRewardSetting_.accountIdentifier));
            }
            if (criteria.getAccountThreshold() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAccountThreshold(), ClientProgramRewardSetting_.accountThreshold));
            }
            if (criteria.getCurrentBalance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCurrentBalance(), ClientProgramRewardSetting_.currentBalance));
            }
            if (criteria.getClientEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientEmail(), ClientProgramRewardSetting_.clientEmail));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ClientProgramRewardSetting_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }
}
