package aduro.basic.programservice.service;

import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.dto.ProgramHealthyRangePayLoad;

import java.util.List;
import java.util.Set;

public interface HealthyRangePayloadMapper {
    ProgramHealthyRangeDTO toProgramHealthyRangeDTO(ProgramHealthyRangePayLoad payLoad);
    ProgramHealthyRangePayLoad toPayload(ProgramHealthyRangeDTO dto);
    public ProgramSubCategoryConfiguration getProgramSubCategoryConfiguration(ProgramHealthyRangePayLoad payload);
    List<ProgramHealthyRangePayLoad> toHealthyPayloadList(List<ProgramHealthyRangeDTO> rangeSet);
}
