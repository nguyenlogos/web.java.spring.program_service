package aduro.basic.programservice.service;

import aduro.basic.programservice.domain.EarnedUserLevel;
import aduro.basic.programservice.service.dto.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramUser}.
 */
public interface ProgramUserService {

    /**
     * Save a programUser.
     *
     * @param programUserDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramUserDTO save(ProgramUserDTO programUserDTO);

    /**
     * Get all the programUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramUserDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramUserDTO> findOne(Long id);

    /**
     * Delete the "id" programUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);


    ProgramUserDTO getCurrentUserProgramInfo(String participantId, String clientId, String subgroupId, Map<String, String> queryParams);

    List<ProgramUserWithActivityStatusDTO> getProgramUserWithActivityStatus(Long programId, String clientId, String customActivityId);

    ProgramUserDTO updateTobaccoUser(TobaccoUserPayload tobaccoUserPayload);

    List<ProgramPayload> getAllProgramByParticipantIdAndClientId(String participantId, String clientId);
    boolean verifySubgroupClient(String subgroupId, String clientId);
    List<ProgramUserDTO> updateSubgroupsForParticipant(UpdatedSubgroupParticipantPayload participantPayload);

    List<ProgramUserDTO> getAllProgramsByClientIdAndProgramId(String clientId, Long programId);

    List<EarnedUserLevel> extractEarnedLevel(String clientId, Long programId);

    APIResponse<MemberProgramDto> subscribeProgram(JoinProgramRequest programRequest);

    APIResponse<ItemCollectionResponseDto> getInformationAfterScreening(String participantId, String clientId);

    void switchSubgroupClient(EmployeeDto payload);

    JoinRequestResponseDto joinProgramWithRequest(JoinProgramRequest programRequest);

    void processCreateProgramUsers(ProgramUserCreateRequest request);
    ProgramUserDTO getProgramUserByParticipantIdAndClientId(String participantId, String clientId);
}
