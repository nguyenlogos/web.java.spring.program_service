package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.DeleteCollectionRequestDto;
import aduro.basic.programservice.service.dto.ProgramCollectionExtensionDto;
import aduro.basic.programservice.service.dto.ProgramCollectionExtensionWithCompletionDto;
import aduro.basic.programservice.service.dto.RemovableCollectionContentDto;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramCollection}.
 */
public interface ProgramCollectionExtensionService {


    /**
     * Save a programCollection.
     *
     * @param programCollectionExtensionDto the entity to save.
     * @return the persisted entity.
     */
    ProgramCollectionExtensionDto save(ProgramCollectionExtensionDto programCollectionExtensionDto);


    /**
     * Get the "id" programCollection.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCollectionExtensionDto> findOne(Long id);

    /**
     * Delete the "id" programCollection.
     *
     * @param dto the entity.
     */
    DeleteCollectionRequestDto delete(DeleteCollectionRequestDto dto);

    List<ProgramCollectionExtensionDto> getByProgramId(Long programId);

    List<ProgramCollectionExtensionDto> saveList (List<ProgramCollectionExtensionDto> collections);

    Optional<ProgramCollectionExtensionWithCompletionDto> findOneWithCompletions(Long id, String participantId, String clientId);

    RemovableCollectionContentDto checkDeleteItemIdInCollection(RemovableCollectionContentDto dto);
}
