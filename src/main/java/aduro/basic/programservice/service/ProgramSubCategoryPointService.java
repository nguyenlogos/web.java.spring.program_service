package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCategoryPointDTO;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramSubCategoryPoint}.
 */
public interface ProgramSubCategoryPointService {

    /**
     * Save a programSubCategoryPoint.
     *
     * @param programSubCategoryPointDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramSubCategoryPointDTO save(ProgramSubCategoryPointDTO programSubCategoryPointDTO);

    /**
     * Get all the programSubCategoryPoints.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramSubCategoryPointDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programSubCategoryPoint.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramSubCategoryPointDTO> findOne(Long id);

    /**
     * Delete the "id" programSubCategoryPoint.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<ProgramSubCategoryPointDTO> addAndRemoveProgramSubCategoryPoints(Long programCategoryPointId, List<ProgramSubCategoryPointDTO> programSubCategoryPointDTOs);

    ProgramSubCategoryPointDTO getProgramSubCategoryPointByUser(String clientId, String participantId, String subCategoryCode);
}
