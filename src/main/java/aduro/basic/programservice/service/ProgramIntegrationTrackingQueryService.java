package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramIntegrationTracking;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramIntegrationTrackingRepository;
import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingCriteria;
import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingDTO;
import aduro.basic.programservice.service.mapper.ProgramIntegrationTrackingMapper;

/**
 * Service for executing complex queries for {@link ProgramIntegrationTracking} entities in the database.
 * The main input is a {@link ProgramIntegrationTrackingCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramIntegrationTrackingDTO} or a {@link Page} of {@link ProgramIntegrationTrackingDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramIntegrationTrackingQueryService extends QueryService<ProgramIntegrationTracking> {

    private final Logger log = LoggerFactory.getLogger(ProgramIntegrationTrackingQueryService.class);

    private final ProgramIntegrationTrackingRepository programIntegrationTrackingRepository;

    private final ProgramIntegrationTrackingMapper programIntegrationTrackingMapper;

    public ProgramIntegrationTrackingQueryService(ProgramIntegrationTrackingRepository programIntegrationTrackingRepository, ProgramIntegrationTrackingMapper programIntegrationTrackingMapper) {
        this.programIntegrationTrackingRepository = programIntegrationTrackingRepository;
        this.programIntegrationTrackingMapper = programIntegrationTrackingMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramIntegrationTrackingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramIntegrationTrackingDTO> findByCriteria(ProgramIntegrationTrackingCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramIntegrationTracking> specification = createSpecification(criteria);
        return programIntegrationTrackingMapper.toDto(programIntegrationTrackingRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramIntegrationTrackingDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramIntegrationTrackingDTO> findByCriteria(ProgramIntegrationTrackingCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramIntegrationTracking> specification = createSpecification(criteria);
        return programIntegrationTrackingRepository.findAll(specification, page)
            .map(programIntegrationTrackingMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramIntegrationTrackingCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramIntegrationTracking> specification = createSpecification(criteria);
        return programIntegrationTrackingRepository.count(specification);
    }

    /**
     * Function to convert ProgramIntegrationTrackingCriteria to a {@link Specification}.
     */
    private Specification<ProgramIntegrationTracking> createSpecification(ProgramIntegrationTrackingCriteria criteria) {
        Specification<ProgramIntegrationTracking> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramIntegrationTracking_.id));
            }
            if (criteria.getEndPoint() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEndPoint(), ProgramIntegrationTracking_.endPoint));
            }
            if (criteria.getPayload() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPayload(), ProgramIntegrationTracking_.payload));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), ProgramIntegrationTracking_.createdAt));
            }
            if (criteria.getErrorMessage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getErrorMessage(), ProgramIntegrationTracking_.errorMessage));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), ProgramIntegrationTracking_.status));
            }
            if (criteria.getServiceType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getServiceType(), ProgramIntegrationTracking_.serviceType));
            }
            if (criteria.getAttemptedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAttemptedAt(), ProgramIntegrationTracking_.attemptedAt));
            }
        }
        return specification;
    }
}
