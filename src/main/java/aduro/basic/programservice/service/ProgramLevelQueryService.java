package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramLevelRepository;
import aduro.basic.programservice.service.dto.ProgramLevelCriteria;
import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelMapper;

/**
 * Service for executing complex queries for {@link ProgramLevel} entities in the database.
 * The main input is a {@link ProgramLevelCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramLevelDTO} or a {@link Page} of {@link ProgramLevelDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramLevelQueryService extends QueryService<ProgramLevel> {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelQueryService.class);

    private final ProgramLevelRepository programLevelRepository;

    private final ProgramLevelMapper programLevelMapper;

    public ProgramLevelQueryService(ProgramLevelRepository programLevelRepository, ProgramLevelMapper programLevelMapper) {
        this.programLevelRepository = programLevelRepository;
        this.programLevelMapper = programLevelMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramLevelDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramLevelDTO> findByCriteria(ProgramLevelCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramLevel> specification = createSpecification(criteria);
        return programLevelMapper.toDto(programLevelRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramLevelDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramLevelDTO> findByCriteria(ProgramLevelCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramLevel> specification = createSpecification(criteria);
        return programLevelRepository.findAll(specification, page)
            .map(programLevelMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramLevelCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramLevel> specification = createSpecification(criteria);
        return programLevelRepository.count(specification);
    }

    /**
     * Function to convert ProgramLevelCriteria to a {@link Specification}.
     */
    private Specification<ProgramLevel> createSpecification(ProgramLevelCriteria criteria) {
        Specification<ProgramLevel> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramLevel_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), ProgramLevel_.description));
            }
            if (criteria.getStartPoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartPoint(), ProgramLevel_.startPoint));
            }
            if (criteria.getEndPoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndPoint(), ProgramLevel_.endPoint));
            }
            if (criteria.getLevelOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevelOrder(), ProgramLevel_.levelOrder));
            }
            if (criteria.getIconPath() != null) {
                specification = specification.and(buildStringSpecification(criteria.getIconPath(), ProgramLevel_.iconPath));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ProgramLevel_.name));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), ProgramLevel_.endDate));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramLevel_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }
}
