package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramPhaseDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramPhase}.
 */
public interface ProgramPhaseService {

    /**
     * Save a programPhase.
     *
     * @param programPhaseDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramPhaseDTO save(ProgramPhaseDTO programPhaseDTO);

    /**
     * Get all the programPhases.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramPhaseDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programPhase.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramPhaseDTO> findOne(Long id);

    /**
     * Delete the "id" programPhase.
     *
     * @param id the id of the entity.
     */
    String delete(Long id);
}
