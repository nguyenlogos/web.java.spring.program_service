package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCohortCollectionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramCohortCollection}.
 */
public interface ProgramCohortCollectionService {

    /**
     * Save a programCohortCollection.
     *
     * @param programCohortCollectionDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramCohortCollectionDTO save(ProgramCohortCollectionDTO programCohortCollectionDTO);

    /**
     * Get all the programCohortCollections.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramCohortCollectionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programCohortCollection.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCohortCollectionDTO> findOne(Long id);

    /**
     * Delete the "id" programCohortCollection.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
