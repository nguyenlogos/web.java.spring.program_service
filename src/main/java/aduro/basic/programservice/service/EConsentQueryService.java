package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.EConsent;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.EConsentRepository;
import aduro.basic.programservice.service.dto.EConsentCriteria;
import aduro.basic.programservice.service.dto.EConsentDTO;
import aduro.basic.programservice.service.mapper.EConsentMapper;

/**
 * Service for executing complex queries for {@link EConsent} entities in the database.
 * The main input is a {@link EConsentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link EConsentDTO} or a {@link Page} of {@link EConsentDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class EConsentQueryService extends QueryService<EConsent> {

    private final Logger log = LoggerFactory.getLogger(EConsentQueryService.class);

    private final EConsentRepository eConsentRepository;

    private final EConsentMapper eConsentMapper;

    public EConsentQueryService(EConsentRepository eConsentRepository, EConsentMapper eConsentMapper) {
        this.eConsentRepository = eConsentRepository;
        this.eConsentMapper = eConsentMapper;
    }

    /**
     * Return a {@link List} of {@link EConsentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<EConsentDTO> findByCriteria(EConsentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<EConsent> specification = createSpecification(criteria);
        return eConsentMapper.toDto(eConsentRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link EConsentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<EConsentDTO> findByCriteria(EConsentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<EConsent> specification = createSpecification(criteria);
        return eConsentRepository.findAll(specification, page)
            .map(eConsentMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(EConsentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<EConsent> specification = createSpecification(criteria);
        return eConsentRepository.count(specification);
    }

    /**
     * Function to convert EConsentCriteria to a {@link Specification}.
     */
    private Specification<EConsent> createSpecification(EConsentCriteria criteria) {
        Specification<EConsent> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), EConsent_.id));
            }
            if (criteria.getParticipantId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParticipantId(), EConsent_.participantId));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramId(), EConsent_.programId));
            }
            if (criteria.getHasConfirmed() != null) {
                specification = specification.and(buildSpecification(criteria.getHasConfirmed(), EConsent_.hasConfirmed));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), EConsent_.createdAt));
            }
            if (criteria.getTermAndConditionId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTermAndConditionId(), EConsent_.termAndConditionId));
            }
        }
        return specification;
    }
}
