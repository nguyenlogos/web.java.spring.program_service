package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.TermAndConditionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.TermAndCondition}.
 */
public interface TermAndConditionService {

    /**
     * Save a termAndCondition.
     *
     * @param termAndConditionDTO the entity to save.
     * @return the persisted entity.
     */
    TermAndConditionDTO save(TermAndConditionDTO termAndConditionDTO);

    /**
     * Get all the termAndConditions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TermAndConditionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" termAndCondition.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TermAndConditionDTO> findOne(Long id);

    /**
     * Delete the "id" termAndCondition.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<TermAndConditionDTO> updateTermAndConditions(List<TermAndConditionDTO> termAndConditionDTOs);

    TermAndConditionDTO getTermAndConditionByClient(String clientId, String subgroupId);

    List<TermAndConditionDTO> getConsentByClient(String clientId);

    List<TermAndConditionDTO> getConsentByClientIdAndSubgroupIdAndIsTerminated(String clientId, String subgroupId, Boolean isTerminated);

    List<TermAndConditionDTO> getTermAndConditionsByClientIdAndSubgroupIdIsNull(String clientId);
}
