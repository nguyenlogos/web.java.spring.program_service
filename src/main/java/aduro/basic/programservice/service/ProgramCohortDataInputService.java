package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCohortDataInputDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramCohortDataInput}.
 */
public interface ProgramCohortDataInputService {

    /**
     * Save a programCohortDataInput.
     *
     * @param programCohortDataInputDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramCohortDataInputDTO save(ProgramCohortDataInputDTO programCohortDataInputDTO);

    /**
     * Get all the programCohortDataInputs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramCohortDataInputDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programCohortDataInput.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCohortDataInputDTO> findOne(Long id);

    /**
     * Delete the "id" programCohortDataInput.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
