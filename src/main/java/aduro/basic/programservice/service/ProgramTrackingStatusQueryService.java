package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramTrackingStatus;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramTrackingStatusRepository;
import aduro.basic.programservice.service.dto.ProgramTrackingStatusCriteria;
import aduro.basic.programservice.service.dto.ProgramTrackingStatusDTO;
import aduro.basic.programservice.service.mapper.ProgramTrackingStatusMapper;

/**
 * Service for executing complex queries for {@link ProgramTrackingStatus} entities in the database.
 * The main input is a {@link ProgramTrackingStatusCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramTrackingStatusDTO} or a {@link Page} of {@link ProgramTrackingStatusDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramTrackingStatusQueryService extends QueryService<ProgramTrackingStatus> {

    private final Logger log = LoggerFactory.getLogger(ProgramTrackingStatusQueryService.class);

    private final ProgramTrackingStatusRepository programTrackingStatusRepository;

    private final ProgramTrackingStatusMapper programTrackingStatusMapper;

    public ProgramTrackingStatusQueryService(ProgramTrackingStatusRepository programTrackingStatusRepository, ProgramTrackingStatusMapper programTrackingStatusMapper) {
        this.programTrackingStatusRepository = programTrackingStatusRepository;
        this.programTrackingStatusMapper = programTrackingStatusMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramTrackingStatusDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramTrackingStatusDTO> findByCriteria(ProgramTrackingStatusCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramTrackingStatus> specification = createSpecification(criteria);
        return programTrackingStatusMapper.toDto(programTrackingStatusRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramTrackingStatusDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramTrackingStatusDTO> findByCriteria(ProgramTrackingStatusCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramTrackingStatus> specification = createSpecification(criteria);
        return programTrackingStatusRepository.findAll(specification, page)
            .map(programTrackingStatusMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramTrackingStatusCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramTrackingStatus> specification = createSpecification(criteria);
        return programTrackingStatusRepository.count(specification);
    }

    /**
     * Function to convert ProgramTrackingStatusCriteria to a {@link Specification}.
     */
    private Specification<ProgramTrackingStatus> createSpecification(ProgramTrackingStatusCriteria criteria) {
        Specification<ProgramTrackingStatus> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramTrackingStatus_.id));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramId(), ProgramTrackingStatus_.programId));
            }
            if (criteria.getProgramStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProgramStatus(), ProgramTrackingStatus_.programStatus));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramTrackingStatus_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramTrackingStatus_.modifiedDate));
            }
        }
        return specification;
    }
}
