package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.UserEvent;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.UserEventRepository;
import aduro.basic.programservice.service.dto.UserEventCriteria;
import aduro.basic.programservice.service.dto.UserEventDTO;
import aduro.basic.programservice.service.mapper.UserEventMapper;

/**
 * Service for executing complex queries for {@link UserEvent} entities in the database.
 * The main input is a {@link UserEventCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserEventDTO} or a {@link Page} of {@link UserEventDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserEventQueryService extends QueryService<UserEvent> {

    private final Logger log = LoggerFactory.getLogger(UserEventQueryService.class);

    private final UserEventRepository userEventRepository;

    private final UserEventMapper userEventMapper;

    public UserEventQueryService(UserEventRepository userEventRepository, UserEventMapper userEventMapper) {
        this.userEventRepository = userEventRepository;
        this.userEventMapper = userEventMapper;
    }

    /**
     * Return a {@link List} of {@link UserEventDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserEventDTO> findByCriteria(UserEventCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserEvent> specification = createSpecification(criteria);
        return userEventMapper.toDto(userEventRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserEventDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserEventDTO> findByCriteria(UserEventCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserEvent> specification = createSpecification(criteria);
        return userEventRepository.findAll(specification, page)
            .map(userEventMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserEventCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserEvent> specification = createSpecification(criteria);
        return userEventRepository.count(specification);
    }

    /**
     * Function to convert UserEventCriteria to a {@link Specification}.
     */
    private Specification<UserEvent> createSpecification(UserEventCriteria criteria) {
        Specification<UserEvent> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserEvent_.id));
            }
            if (criteria.getEventCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventCode(), UserEvent_.eventCode));
            }
            if (criteria.getEventId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventId(), UserEvent_.eventId));
            }
            if (criteria.getEventDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEventDate(), UserEvent_.eventDate));
            }
            if (criteria.getEventPoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEventPoint(), UserEvent_.eventPoint));
            }
            if (criteria.getEventCategory() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventCategory(), UserEvent_.eventCategory));
            }
            if (criteria.getProgramUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramUserId(),
                    root -> root.join(UserEvent_.programUser, JoinType.LEFT).get(ProgramUser_.id)));
            }
        }
        return specification;
    }
}
