package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramSubCategoryConfiguration}.
 */
public interface ProgramSubCategoryConfigurationService {

    /**
     * Save a programSubCategoryConfiguration.
     *
     * @param programSubCategoryConfigurationDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramSubCategoryConfigurationDTO save(ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO);

    /**
     * Get all the programSubCategoryConfigurations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramSubCategoryConfigurationDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programSubCategoryConfiguration.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramSubCategoryConfigurationDTO> findOne(Long id);

    /**
     * Delete the "id" programSubCategoryConfiguration.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
