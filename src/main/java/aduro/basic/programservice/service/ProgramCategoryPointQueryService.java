package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramCategoryPoint;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramCategoryPointRepository;
import aduro.basic.programservice.service.dto.ProgramCategoryPointCriteria;
import aduro.basic.programservice.service.dto.ProgramCategoryPointDTO;
import aduro.basic.programservice.service.mapper.ProgramCategoryPointMapper;

/**
 * Service for executing complex queries for {@link ProgramCategoryPoint} entities in the database.
 * The main input is a {@link ProgramCategoryPointCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramCategoryPointDTO} or a {@link Page} of {@link ProgramCategoryPointDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramCategoryPointQueryService extends QueryService<ProgramCategoryPoint> {

    private final Logger log = LoggerFactory.getLogger(ProgramCategoryPointQueryService.class);

    private final ProgramCategoryPointRepository programCategoryPointRepository;

    private final ProgramCategoryPointMapper programCategoryPointMapper;

    public ProgramCategoryPointQueryService(ProgramCategoryPointRepository programCategoryPointRepository, ProgramCategoryPointMapper programCategoryPointMapper) {
        this.programCategoryPointRepository = programCategoryPointRepository;
        this.programCategoryPointMapper = programCategoryPointMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramCategoryPointDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramCategoryPointDTO> findByCriteria(ProgramCategoryPointCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramCategoryPoint> specification = createSpecification(criteria);
        return programCategoryPointMapper.toDto(programCategoryPointRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramCategoryPointDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramCategoryPointDTO> findByCriteria(ProgramCategoryPointCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramCategoryPoint> specification = createSpecification(criteria);
        return programCategoryPointRepository.findAll(specification, page)
            .map(programCategoryPointMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramCategoryPointCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramCategoryPoint> specification = createSpecification(criteria);
        return programCategoryPointRepository.count(specification);
    }

    /**
     * Function to convert ProgramCategoryPointCriteria to a {@link Specification}.
     */
    private Specification<ProgramCategoryPoint> createSpecification(ProgramCategoryPointCriteria criteria) {
        Specification<ProgramCategoryPoint> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramCategoryPoint_.id));
            }
            if (criteria.getCategoryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoryCode(), ProgramCategoryPoint_.categoryCode));
            }
            if (criteria.getCategoryName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCategoryName(), ProgramCategoryPoint_.categoryName));
            }
            if (criteria.getPercentPoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPercentPoint(), ProgramCategoryPoint_.percentPoint));
            }
            if (criteria.getValuePoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValuePoint(), ProgramCategoryPoint_.valuePoint));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramCategoryPoint_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }
}
