package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramCollection;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramCollectionRepository;
import aduro.basic.programservice.service.dto.ProgramCollectionCriteria;
import aduro.basic.programservice.service.dto.ProgramCollectionDTO;
import aduro.basic.programservice.service.mapper.ProgramCollectionMapper;

/**
 * Service for executing complex queries for {@link ProgramCollection} entities in the database.
 * The main input is a {@link ProgramCollectionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramCollectionDTO} or a {@link Page} of {@link ProgramCollectionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramCollectionQueryService extends QueryService<ProgramCollection> {

    private final Logger log = LoggerFactory.getLogger(ProgramCollectionQueryService.class);

    private final ProgramCollectionRepository programCollectionRepository;

    private final ProgramCollectionMapper programCollectionMapper;

    public ProgramCollectionQueryService(ProgramCollectionRepository programCollectionRepository, ProgramCollectionMapper programCollectionMapper) {
        this.programCollectionRepository = programCollectionRepository;
        this.programCollectionMapper = programCollectionMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramCollectionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramCollectionDTO> findByCriteria(ProgramCollectionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramCollection> specification = createSpecification(criteria);
        return programCollectionMapper.toDto(programCollectionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramCollectionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramCollectionDTO> findByCriteria(ProgramCollectionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramCollection> specification = createSpecification(criteria);
        return programCollectionRepository.findAll(specification, page)
            .map(programCollectionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramCollectionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramCollection> specification = createSpecification(criteria);
        return programCollectionRepository.count(specification);
    }

    /**
     * Function to convert ProgramCollectionCriteria to a {@link Specification}.
     */
    private Specification<ProgramCollection> createSpecification(ProgramCollectionCriteria criteria) {
        Specification<ProgramCollection> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramCollection_.id));
            }
            if (criteria.getDisplayName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDisplayName(), ProgramCollection_.displayName));
            }
            if (criteria.getLongDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLongDescription(), ProgramCollection_.longDescription));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramCollection_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramCollection_.modifiedDate));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramCollection_.program, JoinType.LEFT).get(Program_.id)));
            }
            if (criteria.getProgramCollectionContentId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCollectionContentId(),
                    root -> root.join(ProgramCollection_.programCollectionContents, JoinType.LEFT).get(ProgramCollectionContent_.id)));
            }
        }
        return specification;
    }
}
