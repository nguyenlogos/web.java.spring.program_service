package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.UserReward;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.UserRewardRepository;
import aduro.basic.programservice.service.dto.UserRewardCriteria;
import aduro.basic.programservice.service.dto.UserRewardDTO;
import aduro.basic.programservice.service.mapper.UserRewardMapper;

/**
 * Service for executing complex queries for {@link UserReward} entities in the database.
 * The main input is a {@link UserRewardCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserRewardDTO} or a {@link Page} of {@link UserRewardDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserRewardQueryService extends QueryService<UserReward> {

    private final Logger log = LoggerFactory.getLogger(UserRewardQueryService.class);

    private final UserRewardRepository userRewardRepository;

    private final UserRewardMapper userRewardMapper;

    public UserRewardQueryService(UserRewardRepository userRewardRepository, UserRewardMapper userRewardMapper) {
        this.userRewardRepository = userRewardRepository;
        this.userRewardMapper = userRewardMapper;
    }

    /**
     * Return a {@link List} of {@link UserRewardDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserRewardDTO> findByCriteria(UserRewardCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserReward> specification = createSpecification(criteria);
        return userRewardMapper.toDto(userRewardRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserRewardDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserRewardDTO> findByCriteria(UserRewardCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserReward> specification = createSpecification(criteria);
        return userRewardRepository.findAll(specification, page)
            .map(userRewardMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserRewardCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserReward> specification = createSpecification(criteria);
        return userRewardRepository.count(specification);
    }

    /**
     * Function to convert UserRewardCriteria to a {@link Specification}.
     */
    private Specification<UserReward> createSpecification(UserRewardCriteria criteria) {
        Specification<UserReward> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserReward_.id));
            }
            if (criteria.getProgramLevel() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramLevel(), UserReward_.programLevel));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), UserReward_.status));
            }
            if (criteria.getOrderId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getOrderId(), UserReward_.orderId));
            }
            if (criteria.getOrderDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrderDate(), UserReward_.orderDate));
            }
            if (criteria.getGiftId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGiftId(), UserReward_.giftId));
            }
            if (criteria.getLevelCompletedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevelCompletedDate(), UserReward_.levelCompletedDate));
            }
            if (criteria.getParticipantId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParticipantId(), UserReward_.participantId));
            }
            if (criteria.getEmail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEmail(), UserReward_.email));
            }
            if (criteria.getClientId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getClientId(), UserReward_.clientId));
            }
            if (criteria.getRewardType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRewardType(), UserReward_.rewardType));
            }
            if (criteria.getRewardCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRewardCode(), UserReward_.rewardCode));
            }
            if (criteria.getRewardAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRewardAmount(), UserReward_.rewardAmount));
            }
            if (criteria.getCampaignId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCampaignId(), UserReward_.campaignId));
            }
            if (criteria.getProgramUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramUserId(),
                    root -> root.join(UserReward_.programUser, JoinType.LEFT).get(ProgramUser_.id)));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(UserReward_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }
}
