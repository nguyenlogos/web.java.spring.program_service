package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.RewardDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.Reward}.
 */
public interface RewardService {

    /**
     * Save a reward.
     *
     * @param rewardDTO the entity to save.
     * @return the persisted entity.
     */
    RewardDTO save(RewardDTO rewardDTO);

    /**
     * Get all the rewards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RewardDTO> findAll(Pageable pageable);


    /**
     * Get the "id" reward.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RewardDTO> findOne(Long id);

    /**
     * Delete the "id" reward.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
