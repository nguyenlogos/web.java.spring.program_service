package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramIntegrationTracking}.
 */
public interface ProgramIntegrationTrackingService {

    /**
     * Save a programIntegrationTracking.
     *
     * @param programIntegrationTrackingDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramIntegrationTrackingDTO save(ProgramIntegrationTrackingDTO programIntegrationTrackingDTO);

    /**
     * Get all the programIntegrationTrackings.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramIntegrationTrackingDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programIntegrationTracking.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramIntegrationTrackingDTO> findOne(Long id);

    /**
     * Delete the "id" programIntegrationTracking.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
