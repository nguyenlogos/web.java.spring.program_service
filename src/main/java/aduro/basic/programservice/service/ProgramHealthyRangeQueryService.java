package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramHealthyRangeRepository;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeCriteria;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.mapper.ProgramHealthyRangeMapper;

/**
 * Service for executing complex queries for {@link ProgramHealthyRange} entities in the database.
 * The main input is a {@link ProgramHealthyRangeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramHealthyRangeDTO} or a {@link Page} of {@link ProgramHealthyRangeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramHealthyRangeQueryService extends QueryService<ProgramHealthyRange> {

    private final Logger log = LoggerFactory.getLogger(ProgramHealthyRangeQueryService.class);

    private final ProgramHealthyRangeRepository programHealthyRangeRepository;

    private final ProgramHealthyRangeMapper programHealthyRangeMapper;

    public ProgramHealthyRangeQueryService(ProgramHealthyRangeRepository programHealthyRangeRepository, ProgramHealthyRangeMapper programHealthyRangeMapper) {
        this.programHealthyRangeRepository = programHealthyRangeRepository;
        this.programHealthyRangeMapper = programHealthyRangeMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramHealthyRangeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramHealthyRangeDTO> findByCriteria(ProgramHealthyRangeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramHealthyRange> specification = createSpecification(criteria);
        return programHealthyRangeMapper.toDto(programHealthyRangeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramHealthyRangeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramHealthyRangeDTO> findByCriteria(ProgramHealthyRangeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramHealthyRange> specification = createSpecification(criteria);
        return programHealthyRangeRepository.findAll(specification, page)
            .map(programHealthyRangeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramHealthyRangeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramHealthyRange> specification = createSpecification(criteria);
        return programHealthyRangeRepository.count(specification);
    }

    /**
     * Function to convert ProgramHealthyRangeCriteria to a {@link Specification}.
     */
    private Specification<ProgramHealthyRange> createSpecification(ProgramHealthyRangeCriteria criteria) {
        Specification<ProgramHealthyRange> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramHealthyRange_.id));
            }
            if (criteria.getMaleMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaleMin(), ProgramHealthyRange_.maleMin));
            }
            if (criteria.getMaleMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaleMax(), ProgramHealthyRange_.maleMax));
            }
            if (criteria.getFemaleMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFemaleMin(), ProgramHealthyRange_.femaleMin));
            }
            if (criteria.getFemaleMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFemaleMax(), ProgramHealthyRange_.femaleMax));
            }
            if (criteria.getUnidentifiedMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUnidentifiedMin(), ProgramHealthyRange_.unidentifiedMin));
            }
            if (criteria.getUnidentifiedMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUnidentifiedMax(), ProgramHealthyRange_.unidentifiedMax));
            }
            if (criteria.getIsApplyPoint() != null) {
                specification = specification.and(buildSpecification(criteria.getIsApplyPoint(), ProgramHealthyRange_.isApplyPoint));
            }
            if (criteria.getBiometricCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBiometricCode(), ProgramHealthyRange_.biometricCode));
            }
            if (criteria.getSubCategoryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubCategoryCode(), ProgramHealthyRange_.subCategoryCode));
            }
            if (criteria.getSubCategoryId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSubCategoryId(), ProgramHealthyRange_.subCategoryId));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramHealthyRange_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }
}
