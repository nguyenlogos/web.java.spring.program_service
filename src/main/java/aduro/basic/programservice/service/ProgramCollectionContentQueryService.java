package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramCollectionContent;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramCollectionContentRepository;
import aduro.basic.programservice.service.dto.ProgramCollectionContentCriteria;
import aduro.basic.programservice.service.dto.ProgramCollectionContentDTO;
import aduro.basic.programservice.service.mapper.ProgramCollectionContentMapper;

/**
 * Service for executing complex queries for {@link ProgramCollectionContent} entities in the database.
 * The main input is a {@link ProgramCollectionContentCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramCollectionContentDTO} or a {@link Page} of {@link ProgramCollectionContentDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramCollectionContentQueryService extends QueryService<ProgramCollectionContent> {

    private final Logger log = LoggerFactory.getLogger(ProgramCollectionContentQueryService.class);

    private final ProgramCollectionContentRepository programCollectionContentRepository;

    private final ProgramCollectionContentMapper programCollectionContentMapper;

    public ProgramCollectionContentQueryService(ProgramCollectionContentRepository programCollectionContentRepository, ProgramCollectionContentMapper programCollectionContentMapper) {
        this.programCollectionContentRepository = programCollectionContentRepository;
        this.programCollectionContentMapper = programCollectionContentMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramCollectionContentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramCollectionContentDTO> findByCriteria(ProgramCollectionContentCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramCollectionContent> specification = createSpecification(criteria);
        return programCollectionContentMapper.toDto(programCollectionContentRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramCollectionContentDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramCollectionContentDTO> findByCriteria(ProgramCollectionContentCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramCollectionContent> specification = createSpecification(criteria);
        return programCollectionContentRepository.findAll(specification, page)
            .map(programCollectionContentMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramCollectionContentCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramCollectionContent> specification = createSpecification(criteria);
        return programCollectionContentRepository.count(specification);
    }

    /**
     * Function to convert ProgramCollectionContentCriteria to a {@link Specification}.
     */
    private Specification<ProgramCollectionContent> createSpecification(ProgramCollectionContentCriteria criteria) {
        Specification<ProgramCollectionContent> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramCollectionContent_.id));
            }
            if (criteria.getItemName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getItemName(), ProgramCollectionContent_.itemName));
            }
            if (criteria.getItemId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getItemId(), ProgramCollectionContent_.itemId));
            }
            if (criteria.getItemType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getItemType(), ProgramCollectionContent_.itemType));
            }
            if (criteria.getContentType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContentType(), ProgramCollectionContent_.contentType));
            }
            if (criteria.getItemIcon() != null) {
                specification = specification.and(buildStringSpecification(criteria.getItemIcon(), ProgramCollectionContent_.itemIcon));
            }
            if (criteria.getHasRequired() != null) {
                specification = specification.and(buildSpecification(criteria.getHasRequired(), ProgramCollectionContent_.hasRequired));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramCollectionContent_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramCollectionContent_.modifiedDate));
            }
            if (criteria.getProgramCollectionId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCollectionId(),
                    root -> root.join(ProgramCollectionContent_.programCollection, JoinType.LEFT).get(ProgramCollection_.id)));
            }
        }
        return specification;
    }
}
