package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramLevelActivityDTO;
import aduro.basic.programservice.service.dto.ProgramLevelPathDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramLevelPath}.
 */
public interface ProgramLevelPathService {

    /**
     * Save a programLevelPath.
     *
     * @param programLevelPathDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramLevelPathDTO save(ProgramLevelPathDTO programLevelPathDTO);

    /**
     * Get all the programLevelPaths.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramLevelPathDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programLevelPath.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramLevelPathDTO> findOne(Long id);

    /**
     * Delete the "id" programLevelPath.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<ProgramLevelPathDTO> updateLevelPaths(Long programLevelId, List<ProgramLevelPathDTO> programLevelPathDTOs);

    List<ProgramLevelPathDTO> getProgramLevelPathsByClientIdAndParticipantId(String clientId, String participantId, Map<String, String> queryParams);
}
