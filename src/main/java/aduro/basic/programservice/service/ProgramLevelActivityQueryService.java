package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramLevelActivity;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramLevelActivityRepository;
import aduro.basic.programservice.service.dto.ProgramLevelActivityCriteria;
import aduro.basic.programservice.service.dto.ProgramLevelActivityDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelActivityMapper;

/**
 * Service for executing complex queries for {@link ProgramLevelActivity} entities in the database.
 * The main input is a {@link ProgramLevelActivityCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramLevelActivityDTO} or a {@link Page} of {@link ProgramLevelActivityDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramLevelActivityQueryService extends QueryService<ProgramLevelActivity> {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelActivityQueryService.class);

    private final ProgramLevelActivityRepository programLevelActivityRepository;

    private final ProgramLevelActivityMapper programLevelActivityMapper;

    public ProgramLevelActivityQueryService(ProgramLevelActivityRepository programLevelActivityRepository, ProgramLevelActivityMapper programLevelActivityMapper) {
        this.programLevelActivityRepository = programLevelActivityRepository;
        this.programLevelActivityMapper = programLevelActivityMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramLevelActivityDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramLevelActivityDTO> findByCriteria(ProgramLevelActivityCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramLevelActivity> specification = createSpecification(criteria);
        return programLevelActivityMapper.toDto(programLevelActivityRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramLevelActivityDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramLevelActivityDTO> findByCriteria(ProgramLevelActivityCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramLevelActivity> specification = createSpecification(criteria);
        return programLevelActivityRepository.findAll(specification, page)
            .map(programLevelActivityMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramLevelActivityCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramLevelActivity> specification = createSpecification(criteria);
        return programLevelActivityRepository.count(specification);
    }

    /**
     * Function to convert ProgramLevelActivityCriteria to a {@link Specification}.
     */
    private Specification<ProgramLevelActivity> createSpecification(ProgramLevelActivityCriteria criteria) {
        Specification<ProgramLevelActivity> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramLevelActivity_.id));
            }
            if (criteria.getActivityCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivityCode(), ProgramLevelActivity_.activityCode));
            }
            if (criteria.getActivityId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivityId(), ProgramLevelActivity_.activityId));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), ProgramLevelActivity_.subgroupId));
            }
            if (criteria.getSubgroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupName(), ProgramLevelActivity_.subgroupName));
            }
            if (criteria.getProgramLevelId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramLevelId(),
                    root -> root.join(ProgramLevelActivity_.programLevel, JoinType.LEFT).get(ProgramLevel_.id)));
            }
        }
        return specification;
    }
}
