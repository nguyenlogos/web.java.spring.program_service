package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramTrackingStatusDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramTrackingStatus}.
 */
public interface ProgramTrackingStatusService {

    /**
     * Save a programTrackingStatus.
     *
     * @param programTrackingStatusDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramTrackingStatusDTO save(ProgramTrackingStatusDTO programTrackingStatusDTO);

    /**
     * Get all the programTrackingStatuses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramTrackingStatusDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programTrackingStatus.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramTrackingStatusDTO> findOne(Long id);

    /**
     * Delete the "id" programTrackingStatus.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
