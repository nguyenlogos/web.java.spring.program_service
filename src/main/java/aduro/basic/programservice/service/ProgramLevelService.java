package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramLevelDTO;

import aduro.basic.programservice.service.dto.ProgramLevelInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramLevel}.
 */
public interface ProgramLevelService {

    /**
     * Save a programLevel.
     *
     * @param programLevelDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramLevelDTO save(ProgramLevelDTO programLevelDTO);

    /**
     * Get all the programLevels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramLevelDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programLevel.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramLevelDTO> findOne(Long id);

    /**
     * Delete the "id" programLevel.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get the "id" program detail(program info, points, level ... ).
     *
     * @param programId the id of the entity.
     * @return the entity.
     */
    List<ProgramLevelDTO> updateAllLevels(Long programId, List<ProgramLevelDTO> programLevelDTOS);

    ProgramLevelInfo updateProgramLevelInfo(Long programId, ProgramLevelInfo programLevelInfo);
}
