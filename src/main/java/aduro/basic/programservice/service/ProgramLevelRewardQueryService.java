package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramLevelReward;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramLevelRewardRepository;
import aduro.basic.programservice.service.dto.ProgramLevelRewardCriteria;
import aduro.basic.programservice.service.dto.ProgramLevelRewardDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelRewardMapper;

/**
 * Service for executing complex queries for {@link ProgramLevelReward} entities in the database.
 * The main input is a {@link ProgramLevelRewardCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramLevelRewardDTO} or a {@link Page} of {@link ProgramLevelRewardDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramLevelRewardQueryService extends QueryService<ProgramLevelReward> {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelRewardQueryService.class);

    private final ProgramLevelRewardRepository programLevelRewardRepository;

    private final ProgramLevelRewardMapper programLevelRewardMapper;

    public ProgramLevelRewardQueryService(ProgramLevelRewardRepository programLevelRewardRepository, ProgramLevelRewardMapper programLevelRewardMapper) {
        this.programLevelRewardRepository = programLevelRewardRepository;
        this.programLevelRewardMapper = programLevelRewardMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramLevelRewardDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramLevelRewardDTO> findByCriteria(ProgramLevelRewardCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramLevelReward> specification = createSpecification(criteria);
        return programLevelRewardMapper.toDto(programLevelRewardRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramLevelRewardDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramLevelRewardDTO> findByCriteria(ProgramLevelRewardCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramLevelReward> specification = createSpecification(criteria);
        return programLevelRewardRepository.findAll(specification, page)
            .map(programLevelRewardMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramLevelRewardCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramLevelReward> specification = createSpecification(criteria);
        return programLevelRewardRepository.count(specification);
    }

    /**
     * Function to convert ProgramLevelRewardCriteria to a {@link Specification}.
     */
    private Specification<ProgramLevelReward> createSpecification(ProgramLevelRewardCriteria criteria) {
        Specification<ProgramLevelReward> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramLevelReward_.id));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), ProgramLevelReward_.description));
            }
            if (criteria.getQuantity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantity(), ProgramLevelReward_.quantity));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), ProgramLevelReward_.code));
            }
            if (criteria.getRewardAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRewardAmount(), ProgramLevelReward_.rewardAmount));
            }
            if (criteria.getRewardType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRewardType(), ProgramLevelReward_.rewardType));
            }
            if (criteria.getCampaignId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCampaignId(), ProgramLevelReward_.campaignId));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), ProgramLevelReward_.subgroupId));
            }
            if (criteria.getSubgroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupName(), ProgramLevelReward_.subgroupName));
            }
            if (criteria.getProgramLevelId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramLevelId(),
                    root -> root.join(ProgramLevelReward_.programLevel, JoinType.LEFT).get(ProgramLevel_.id)));
            }
        }
        return specification;
    }
}
