package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCollectionDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramCollection}.
 */
public interface ProgramCollectionService {

    /**
     * Save a programCollection.
     *
     * @param programCollectionDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramCollectionDTO save(ProgramCollectionDTO programCollectionDTO);

    /**
     * Get all the programCollections.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramCollectionDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programCollection.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCollectionDTO> findOne(Long id);

    /**
     * Delete the "id" programCollection.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
