package aduro.basic.programservice.service;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.CustomActivityResultDto;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.repository.ProgramActivityRepository;
import aduro.basic.programservice.repository.ProgramLevelActivityRepository;
import aduro.basic.programservice.service.dto.ProgramActivityCriteria;
import aduro.basic.programservice.service.dto.ProgramActivityDTO;
import aduro.basic.programservice.service.mapper.ProgramActivityMapper;
import com.google.common.base.Joiner;
import io.github.jhipster.service.QueryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.JoinType;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service for executing complex queries for {@link ProgramActivity} entities in the database.
 * The main input is a {@link ProgramActivityCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramActivityDTO} or a {@link Page} of {@link ProgramActivityDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramActivityQueryService extends QueryService<ProgramActivity> {

    private final Logger log = LoggerFactory.getLogger(ProgramActivityQueryService.class);

    private final ProgramActivityRepository programActivityRepository;

    @Autowired
    private  ProgramLevelActivityRepository programLevelActivityRepository;

    @Autowired
    private CustomActivityAdapter customActivityAdapter;

    private final ProgramActivityMapper programActivityMapper;

    public ProgramActivityQueryService(ProgramActivityRepository programActivityRepository, ProgramActivityMapper programActivityMapper) {
        this.programActivityRepository = programActivityRepository;
        this.programActivityMapper = programActivityMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramActivityDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramActivityDTO> findByCriteria(ProgramActivityCriteria criteria, MultiValueMap<String, String> queryParams) throws Exception {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramActivity> specification = createSpecification(criteria);
        List<ProgramActivityDTO> rs = programActivityMapper.toDto(programActivityRepository.findAll(specification));

        // fetch custom activities from Platform to be mapping the data into program activity object
        List<String> customActivityIds = rs.stream()
            .map(p -> p.getActivityId()).collect(Collectors.toList());
        List<CustomActivityResultDto> resultCustomActivityList = customActivityAdapter.getCustomActivitiesByIds(customActivityIds, queryParams);
        rs.forEach(programActivityDTO -> {
            CustomActivityResultDto customActivityDTO = resultCustomActivityList.stream()
                .filter(c -> Integer.valueOf(programActivityDTO.getActivityId()) == c.getId()).findFirst().orElse(null);
            if (customActivityDTO != null) {
                programActivityDTO.setTitle(customActivityDTO.getTitle());
                programActivityDTO.setActivityType(customActivityDTO.getType());
                programActivityDTO.setSizeOfActivity(customActivityDTO.getSizeOfActivity());
                programActivityDTO.setCategory(customActivityDTO.getCategory());
                programActivityDTO.setIsCustomized(customActivityDTO.isCustomized());
                programActivityDTO.setProgramName(customActivityDTO.getProgramName());
                programActivityDTO.setTemplateActivityId(customActivityDTO.getTemplateActivityId());
                programActivityDTO.setStartDate(Instant.parse(customActivityDTO.getStartDate()));
                programActivityDTO.setEndDate(Instant.parse(customActivityDTO.getEndDate()));
            }
        });
        return rs;
    }

    /**
     * Return a {@link Page} of {@link ProgramActivityDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramActivityDTO> findByCriteria(ProgramActivityCriteria criteria, Pageable page, MultiValueMap<String, String> queryParams) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        Page<ProgramActivityDTO> rs = null;
        try {
            Long programId = criteria.getProgramId().getEquals();
            Long phaseId = queryParams.get("programPhaseId.equals") != null ? Long.valueOf(queryParams.get("programPhaseId.equals").get(0)) : 0;
            queryParams.set("program_id", String.valueOf(criteria.getProgramId().getEquals()));
            if (phaseId > 0) {
                rs = programActivityRepository.findProgramActivitiesByProgramIdAndProgramPhaseId(programId, phaseId, page)
                    .map(programActivityMapper::toDto);
            } else {
                rs = programActivityRepository.findProgramActivitiesByProgramId(programId, page)
                    .map(programActivityMapper::toDto);
            }
            List<String> customActivityIds = rs.stream()
                .map(c -> String.valueOf(c.getActivityId())).collect(Collectors.toList());
            List<CustomActivityResultDto> resultCustomActivityList = customActivityAdapter.getCustomActivitiesByIds(customActivityIds, queryParams);

            List<ProgramLevelActivity> programLevelActivityList =  programLevelActivityRepository.findProgramLevelActivitiesByProgramLevelProgramId(criteria.getProgramId().getEquals());
            rs.forEach(programActivityDTO-> {
                List<Integer> levels =  programLevelActivityList.stream().filter(la->la.getActivityId().equals(programActivityDTO.getActivityId()))
                    .map(la->la.getProgramLevel().getLevelOrder()).collect(Collectors.toList());
                programActivityDTO.setAssignToLevel(Joiner.on(',').join(levels));

                CustomActivityResultDto customActivityDTO = resultCustomActivityList.stream()
                    .filter(c -> Integer.valueOf(programActivityDTO.getActivityId()) == c.getId()).findFirst().orElse(null);
                if (customActivityDTO != null) {
                    programActivityDTO.setTitle(customActivityDTO.getTitle());
                    programActivityDTO.setActivityType(customActivityDTO.getType());
                    programActivityDTO.setSizeOfActivity(customActivityDTO.getSizeOfActivity());
                    programActivityDTO.setCategory(customActivityDTO.getCategory());
                    programActivityDTO.setIsCustomized(customActivityDTO.isCustomized());
                    programActivityDTO.setProgramName(customActivityDTO.getProgramName());
                    programActivityDTO.setTemplateActivityId(customActivityDTO.getTemplateActivityId());
                    programActivityDTO.setStartDate(Instant.parse(customActivityDTO.getStartDate()));
                    programActivityDTO.setEndDate(Instant.parse(customActivityDTO.getEndDate()));
                    programActivityDTO.setRequired(!levels.isEmpty());
                    programActivityDTO.setFeatured(customActivityDTO.isFeatured());
                }
            });
        } catch (Exception e) {
            log.error("Program activity findByCriteria : {}", e.getMessage());
        }

        return rs;
    }

    @Transactional(readOnly = true)
    public List<Integer> findAllWithProgramActivityIds(Long programId) {
        List<Integer> rs =  programActivityRepository.findProgramActivityIdsByProgramId(programId).stream()
            .filter(p -> !StringUtils.isEmpty(p))
            .map(p -> Integer.valueOf(p))
            .collect(Collectors.toList());
        return rs;
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramActivityCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramActivity> specification = createSpecification(criteria);
        return programActivityRepository.count(specification);
    }

    /**
     * Function to convert ProgramActivityCriteria to a {@link Specification}.
     */
    private Specification<ProgramActivity> createSpecification(ProgramActivityCriteria criteria) {
        Specification<ProgramActivity> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramActivity_.id));
            }
            if (criteria.getActivityId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivityId(), ProgramActivity_.activityId));
            }
            if (criteria.getActivityCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getActivityCode(), ProgramActivity_.activityCode));
            }

            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramActivity_.program, JoinType.LEFT).get(Program_.id)));
            }
            if (criteria.getProgramPhaseId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramPhaseId(),
                    root -> root.join(ProgramActivity_.programPhase, JoinType.LEFT).get(ProgramPhase_.id)));
            }
            if (criteria.getIsCustomized() != null) {
                specification = specification.and(buildSpecification(criteria.getIsCustomized(), ProgramActivity_.isCustomized));
            }


            if (criteria.getMasterId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMasterId(), ProgramActivity_.masterId));
            }

        }
        return specification;
    }

}
