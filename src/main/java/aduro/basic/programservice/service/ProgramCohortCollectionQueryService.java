package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramCohortCollection;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramCohortCollectionRepository;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionCriteria;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortCollectionMapper;

/**
 * Service for executing complex queries for {@link ProgramCohortCollection} entities in the database.
 * The main input is a {@link ProgramCohortCollectionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramCohortCollectionDTO} or a {@link Page} of {@link ProgramCohortCollectionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramCohortCollectionQueryService extends QueryService<ProgramCohortCollection> {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortCollectionQueryService.class);

    private final ProgramCohortCollectionRepository programCohortCollectionRepository;

    private final ProgramCohortCollectionMapper programCohortCollectionMapper;

    public ProgramCohortCollectionQueryService(ProgramCohortCollectionRepository programCohortCollectionRepository, ProgramCohortCollectionMapper programCohortCollectionMapper) {
        this.programCohortCollectionRepository = programCohortCollectionRepository;
        this.programCohortCollectionMapper = programCohortCollectionMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramCohortCollectionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramCohortCollectionDTO> findByCriteria(ProgramCohortCollectionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramCohortCollection> specification = createSpecification(criteria);
        return programCohortCollectionMapper.toDto(programCohortCollectionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramCohortCollectionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramCohortCollectionDTO> findByCriteria(ProgramCohortCollectionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramCohortCollection> specification = createSpecification(criteria);
        return programCohortCollectionRepository.findAll(specification, page)
            .map(programCohortCollectionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramCohortCollectionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramCohortCollection> specification = createSpecification(criteria);
        return programCohortCollectionRepository.count(specification);
    }

    /**
     * Function to convert ProgramCohortCollectionCriteria to a {@link Specification}.
     */
    private Specification<ProgramCohortCollection> createSpecification(ProgramCohortCollectionCriteria criteria) {
        Specification<ProgramCohortCollection> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramCohortCollection_.id));
            }
            if (criteria.getRequiredCompletion() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRequiredCompletion(), ProgramCohortCollection_.requiredCompletion));
            }
            if (criteria.getRequiredLevel() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRequiredLevel(), ProgramCohortCollection_.requiredLevel));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramCohortCollection_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramCohortCollection_.modifiedDate));
            }
            if (criteria.getProgramCollectionId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCollectionId(),
                    root -> root.join(ProgramCohortCollection_.programCollection, JoinType.LEFT).get(ProgramCollection_.id)));
            }
            if (criteria.getProgramCohortId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCohortId(),
                    root -> root.join(ProgramCohortCollection_.programCohort, JoinType.LEFT).get(ProgramCohort_.id)));
            }
        }
        return specification;
    }
}
