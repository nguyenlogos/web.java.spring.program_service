package aduro.basic.programservice.service;

import aduro.basic.programservice.ap.dto.ClientDto;
import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.domain.enumeration.ActivityType;
import aduro.basic.programservice.service.dto.CloneActivityDTO;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.service.dto.ProgramTemplateDTO;
import aduro.basic.programservice.service.dto.UpdatedSubgroupClientPayload;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.Program}.
 */
public interface ProgramService {

    /**
     * Save a program.
     *
     * @param programDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramDTO save(ProgramDTO programDTO);

    /**
     * Get all the programs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramDTO> findAll(Pageable pageable);


    /**
     * Get the "id" program.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramDTO> findOne(Long id);

    /**
     * Delete the "id" program.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * upload logo of Program
     *
     * @param programDTO the id of the entity.
     */
    //ProgramDTO uploadProgramLogo(long id, MultipartFile file);
  //  ProgramDTO uploadProgramLogo(ProgramDTO programDTO);
    /**
     * Get the "id" program detail(program info, points, level ... ).
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    ProgramDTO getProgramDetail(Long id, boolean enableDemoGraphic, boolean showProgramActivity);

    ProgramDTO getProgramDetail(Long id, boolean showProgramActivity);

    /**
     * Get the "id" program detail(program info, points, level ... ).
     *
     * @param clientId the id of the entity.
     * @return the entity.
     */
    ProgramDTO getProgramDetailByClienId(String clientId, String subgroupId, String participantId, boolean showProgramActivity, boolean isQc);

    ProgramDTO exportProgramTemplate(Long programId, ProgramTemplateDTO dto);

    ProgramDTO createProgramFromTemplate(Long templateId, ProgramTemplateDTO dto);

    ProgramDTO cloneProgramActivites(Long templateId, Long programId, List<CloneActivityDTO> cloneActivityDTOS);
    List<CustomActivityDTO> getCustomActivitiesByProgramId(ActivityType type, Long programId) throws Exception;
    UpdatedSubgroupClientPayload updateProgramByNewSubgroupId(UpdatedSubgroupClientPayload updatedSubgroupClientPayload);
    String checkProgramStatusManually() throws Exception;

}
