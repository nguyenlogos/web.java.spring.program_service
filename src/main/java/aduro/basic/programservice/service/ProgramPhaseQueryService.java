package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramPhase;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramPhaseRepository;
import aduro.basic.programservice.service.dto.ProgramPhaseCriteria;
import aduro.basic.programservice.service.dto.ProgramPhaseDTO;
import aduro.basic.programservice.service.mapper.ProgramPhaseMapper;

/**
 * Service for executing complex queries for {@link ProgramPhase} entities in the database.
 * The main input is a {@link ProgramPhaseCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramPhaseDTO} or a {@link Page} of {@link ProgramPhaseDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramPhaseQueryService extends QueryService<ProgramPhase> {

    private final Logger log = LoggerFactory.getLogger(ProgramPhaseQueryService.class);

    private final ProgramPhaseRepository programPhaseRepository;

    private final ProgramPhaseMapper programPhaseMapper;

    public ProgramPhaseQueryService(ProgramPhaseRepository programPhaseRepository, ProgramPhaseMapper programPhaseMapper) {
        this.programPhaseRepository = programPhaseRepository;
        this.programPhaseMapper = programPhaseMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramPhaseDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramPhaseDTO> findByCriteria(ProgramPhaseCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramPhase> specification = createSpecification(criteria);
        return programPhaseMapper.toDto(programPhaseRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramPhaseDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramPhaseDTO> findByCriteria(ProgramPhaseCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramPhase> specification = createSpecification(criteria);
        return programPhaseRepository.findAll(specification, page)
            .map(programPhaseMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramPhaseCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramPhase> specification = createSpecification(criteria);
        return programPhaseRepository.count(specification);
    }

    /**
     * Function to convert ProgramPhaseCriteria to a {@link Specification}.
     */
    private Specification<ProgramPhase> createSpecification(ProgramPhaseCriteria criteria) {
        Specification<ProgramPhase> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramPhase_.id));
            }
            if (criteria.getPhaseOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPhaseOrder(), ProgramPhase_.phaseOrder));
            }
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), ProgramPhase_.startDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), ProgramPhase_.endDate));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramPhase_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }
}
