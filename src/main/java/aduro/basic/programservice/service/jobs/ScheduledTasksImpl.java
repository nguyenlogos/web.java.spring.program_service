package aduro.basic.programservice.service.jobs;

import aduro.basic.programservice.config.Settings;
import aduro.basic.programservice.domain.CronJobLoader;
import aduro.basic.programservice.domain.enumeration.ProcessLevelType;
import aduro.basic.programservice.repository.CronJobLoaderRepository;
import aduro.basic.programservice.service.businessrule.UserLevelService;
import aduro.basic.programservice.service.dto.UpdateLevelRequest;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Component
public class ScheduledTasksImpl implements ScheduledTasks {

    private final Logger log = LoggerFactory.getLogger(ScheduledTasksImpl.class);

    @Autowired
    private CronJobLoaderRepository cronJobLoaderRepository;

    @Autowired
    private UserLevelService userLevelService;

    @Autowired
    private Settings settings;


    /*
    * process in queue level
    * calculate level and reward
    * */
    @Override
    @Scheduled(cron = "0 */6 * * * *")
    @SchedulerLock(name = "processIncentiveParticipant")
    public void processIncentiveParticipant() {
        //job run 5 min
        // process flow validate -> update-level -> update-reward -> done
        if(!settings.isScronJobEnabled()){
            return;
        }
        userLevelService.processCalculateLevelInQueue();
    }



    /*
    * run schedule in midnight for client
    *
    *
    * */

    @Override
    @Scheduled(cron = "0 0 0 * * ?")
    @SchedulerLock(name = "processIncentiveAllParticipantOfClient")
    public void processIncentiveAllParticipantOfClient() {
        if(!settings.isScronJobEnabled()){
            return;
        }
        // run when trigger manual
        // schedule in midnight
        // load all items
        List<CronJobLoader> loaders = cronJobLoaderRepository.findByTypeAndIsForce(ProcessLevelType.ALL, true);
        userLevelService.processParticipantOfClient(loaders);

    }


    /*
    * Scheduled run daily to get triggers
    *
    * if isForce = true  and not completed will be process
    *
    * fetch trigger all
    *
    * fetching trigger individual
    * */

    @Override
    @Scheduled(cron = "0 */35 * * * *")
    @SchedulerLock(name = "handleFindAvailableParticipantMatchingConditionLevel")
    public void handleFindAvailableParticipantMatchingConditionLevel() {
        if(!settings.isScronJobEnabled()){
            return;
        }
        //run in 5 min
        userLevelService.calculateUserArchivedLevel();

    }

    @Override
    @Transactional
    public void insertTriggerLoader(UpdateLevelRequest request) {

        CronJobLoader loader = CronJobLoader.builder()
            .id(request.getId())
            .attemptCount(0)
            .clientId(request.getClientId())
            .participantId(request.getParticipantId())
            .programId(request.getProgramId())
            .createdAt(Instant.now())
            .isCompleted(request.getIsCompleted())
            .isForce(request.getIsForce())
            .updatedAt(Instant.now())
            .type(request.getType())
            .build();

        cronJobLoaderRepository.saveAndFlush(loader);

        userLevelService.calculateUserArchivedLevel();

    }
}
