package aduro.basic.programservice.service.jobs;

import aduro.basic.programservice.service.dto.UpdateLevelRequest;

public interface ScheduledTasks {
    void processIncentiveParticipant();
    void processIncentiveAllParticipantOfClient();
    void handleFindAvailableParticipantMatchingConditionLevel();
    void insertTriggerLoader(UpdateLevelRequest request);
}
