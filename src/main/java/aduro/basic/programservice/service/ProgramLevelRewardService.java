package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramLevelRewardDTO;

import aduro.basic.programservice.service.dto.SubgroupRewardsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramLevelReward}.
 */
public interface ProgramLevelRewardService {

    /**
     * Save a programLevelReward.
     *
     * @param programLevelRewardDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramLevelRewardDTO save(ProgramLevelRewardDTO programLevelRewardDTO);

    /**
     * Get all the programLevelRewards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramLevelRewardDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programLevelReward.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramLevelRewardDTO> findOne(Long id);

    /**
     * Delete the "id" programLevelReward.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<ProgramLevelRewardDTO> updateLevelRewards(Long programLevelId, List<ProgramLevelRewardDTO> programLevelRewardDTOs);

    List<ProgramLevelRewardDTO> updateRewardsForAllLevel(Long programId, SubgroupRewardsDTO programLevelRewardDTO);
}
