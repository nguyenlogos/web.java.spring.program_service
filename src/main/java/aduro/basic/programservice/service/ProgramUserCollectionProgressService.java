package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.ProgramUserCollectionProgress}.
 */
public interface ProgramUserCollectionProgressService {

    /**
     * Save a programUserCollectionProgress.
     *
     * @param programUserCollectionProgressDTO the entity to save.
     * @return the persisted entity.
     */
    ProgramUserCollectionProgressDTO save(ProgramUserCollectionProgressDTO programUserCollectionProgressDTO);

    /**
     * Get all the programUserCollectionProgresses.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProgramUserCollectionProgressDTO> findAll(Pageable pageable);


    /**
     * Get the "id" programUserCollectionProgress.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramUserCollectionProgressDTO> findOne(Long id);

    /**
     * Delete the "id" programUserCollectionProgress.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
