package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.UserEventQueue;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.UserEventQueueRepository;
import aduro.basic.programservice.service.dto.UserEventQueueCriteria;
import aduro.basic.programservice.service.dto.UserEventQueueDTO;
import aduro.basic.programservice.service.mapper.UserEventQueueMapper;

/**
 * Service for executing complex queries for {@link UserEventQueue} entities in the database.
 * The main input is a {@link UserEventQueueCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link UserEventQueueDTO} or a {@link Page} of {@link UserEventQueueDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class UserEventQueueQueryService extends QueryService<UserEventQueue> {

    private final Logger log = LoggerFactory.getLogger(UserEventQueueQueryService.class);

    private final UserEventQueueRepository userEventQueueRepository;

    private final UserEventQueueMapper userEventQueueMapper;

    public UserEventQueueQueryService(UserEventQueueRepository userEventQueueRepository, UserEventQueueMapper userEventQueueMapper) {
        this.userEventQueueRepository = userEventQueueRepository;
        this.userEventQueueMapper = userEventQueueMapper;
    }

    /**
     * Return a {@link List} of {@link UserEventQueueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<UserEventQueueDTO> findByCriteria(UserEventQueueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<UserEventQueue> specification = createSpecification(criteria);
        return userEventQueueMapper.toDto(userEventQueueRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link UserEventQueueDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<UserEventQueueDTO> findByCriteria(UserEventQueueCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<UserEventQueue> specification = createSpecification(criteria);
        return userEventQueueRepository.findAll(specification, page)
            .map(userEventQueueMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(UserEventQueueCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<UserEventQueue> specification = createSpecification(criteria);
        return userEventQueueRepository.count(specification);
    }

    /**
     * Function to convert UserEventQueueCriteria to a {@link Specification}.
     */
    private Specification<UserEventQueue> createSpecification(UserEventQueueCriteria criteria) {
        Specification<UserEventQueue> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), UserEventQueue_.id));
            }
            if (criteria.getEventCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventCode(), UserEventQueue_.eventCode));
            }
            if (criteria.getEventId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventId(), UserEventQueue_.eventId));
            }
            if (criteria.getEventDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEventDate(), UserEventQueue_.eventDate));
            }
            if (criteria.getEventPoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEventPoint(), UserEventQueue_.eventPoint));
            }
            if (criteria.getEventCategory() != null) {
                specification = specification.and(buildStringSpecification(criteria.getEventCategory(), UserEventQueue_.eventCategory));
            }
            if (criteria.getProgramUserId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramUserId(), UserEventQueue_.programUserId));
            }
            if (criteria.getExecuteStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExecuteStatus(), UserEventQueue_.executeStatus));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getProgramId(), UserEventQueue_.programId));
            }
            if (criteria.getReadAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReadAt(), UserEventQueue_.readAt));
            }
            if (criteria.getErrorMessage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getErrorMessage(), UserEventQueue_.errorMessage));
            }
            if (criteria.getCurrentStep() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrentStep(), UserEventQueue_.currentStep));
            }
        }
        return specification;
    }
}
