package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramRequirementItems;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramRequirementItemsRepository;
import aduro.basic.programservice.service.dto.ProgramRequirementItemsCriteria;
import aduro.basic.programservice.service.dto.ProgramRequirementItemsDTO;
import aduro.basic.programservice.service.mapper.ProgramRequirementItemsMapper;

/**
 * Service for executing complex queries for {@link ProgramRequirementItems} entities in the database.
 * The main input is a {@link ProgramRequirementItemsCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramRequirementItemsDTO} or a {@link Page} of {@link ProgramRequirementItemsDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramRequirementItemsQueryService extends QueryService<ProgramRequirementItems> {

    private final Logger log = LoggerFactory.getLogger(ProgramRequirementItemsQueryService.class);

    private final ProgramRequirementItemsRepository programRequirementItemsRepository;

    private final ProgramRequirementItemsMapper programRequirementItemsMapper;

    public ProgramRequirementItemsQueryService(ProgramRequirementItemsRepository programRequirementItemsRepository, ProgramRequirementItemsMapper programRequirementItemsMapper) {
        this.programRequirementItemsRepository = programRequirementItemsRepository;
        this.programRequirementItemsMapper = programRequirementItemsMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramRequirementItemsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramRequirementItemsDTO> findByCriteria(ProgramRequirementItemsCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramRequirementItems> specification = createSpecification(criteria);
        return programRequirementItemsMapper.toDto(programRequirementItemsRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramRequirementItemsDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramRequirementItemsDTO> findByCriteria(ProgramRequirementItemsCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramRequirementItems> specification = createSpecification(criteria);
        return programRequirementItemsRepository.findAll(specification, page)
            .map(programRequirementItemsMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramRequirementItemsCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramRequirementItems> specification = createSpecification(criteria);
        return programRequirementItemsRepository.count(specification);
    }

    /**
     * Function to convert ProgramRequirementItemsCriteria to a {@link Specification}.
     */
    private Specification<ProgramRequirementItems> createSpecification(ProgramRequirementItemsCriteria criteria) {
        Specification<ProgramRequirementItems> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramRequirementItems_.id));
            }
            if (criteria.getItemId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getItemId(), ProgramRequirementItems_.itemId));
            }
            if (criteria.getNameOfItem() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNameOfItem(), ProgramRequirementItems_.nameOfItem));
            }
            if (criteria.getTypeOfItem() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTypeOfItem(), ProgramRequirementItems_.typeOfItem));
            }
            if (criteria.getItemCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getItemCode(), ProgramRequirementItems_.itemCode));
            }
            if (criteria.getSubgroupId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupId(), ProgramRequirementItems_.subgroupId));
            }
            if (criteria.getSubgroupName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubgroupName(), ProgramRequirementItems_.subgroupName));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), ProgramRequirementItems_.createdAt));
            }
            if (criteria.getProgramLevelId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramLevelId(),
                    root -> root.join(ProgramRequirementItems_.programLevel, JoinType.LEFT).get(ProgramLevel_.id)));
            }
        }
        return specification;
    }
}
