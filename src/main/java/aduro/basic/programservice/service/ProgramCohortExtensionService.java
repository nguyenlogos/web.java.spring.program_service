package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.ProgramCohortExtensionDto;

import java.util.List;
import java.util.Optional;

public interface ProgramCohortExtensionService  {

    /**
     * Save a programCohort.
     *
     * @param programCohortDto the entity to save.
     * @return the persisted entity.
     */
    ProgramCohortExtensionDto save(ProgramCohortExtensionDto programCohortDto);


    /**
     * Get the "id" programCohortRule.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProgramCohortExtensionDto> findOne(Long id);

    /**
     * Delete the "id" programCohort.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Delete the "id" programCohort.
     *
     * @param id  the id of the entity.
     *
     */
    void removeCollectionFromCohort(Long id);

    List<ProgramCohortExtensionDto> save(List<ProgramCohortExtensionDto> programCohortDto);

    List<ProgramCohortExtensionDto> getAllProgramCohortByProgramId(Long programId);

}
