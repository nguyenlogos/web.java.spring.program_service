package aduro.basic.programservice.service.extensions;

import aduro.basic.programservice.ap.NotificationAdapter;
import aduro.basic.programservice.ap.dto.NotificationItemRequest;
import aduro.basic.programservice.ap.dto.NotificationItemResponse;
import aduro.basic.programservice.config.Settings;
import aduro.basic.programservice.domain.NotificationCenter;
import aduro.basic.programservice.domain.WellmetricEventQueue;
import aduro.basic.programservice.domain.enumeration.NotificationReceiverName;
import aduro.basic.programservice.repository.NotificationCenterRepository;
import aduro.basic.programservice.repository.extensions.NotificationCenterRepositoryExtension;
import aduro.basic.programservice.repository.extensions.WellmetricEventQueueRepositoryExtension;
import aduro.basic.programservice.service.dto.IncentiveStatusEnum;
import aduro.basic.programservice.service.impl.NotificationCenterServiceImpl;
import aduro.basic.programservice.service.mapper.NotificationCenterMapper;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
@Transactional
@Primary
public class NotificationCenterServiceExtension extends NotificationCenterServiceImpl {

    private final Logger log = LoggerFactory.getLogger(NotificationCenterServiceExtension.class);


    @Autowired
    private NotificationCenterRepositoryExtension notificationCenterRepositoryExtension;

    @Autowired
    private WellmetricEventQueueRepositoryExtension wellmetricEventQueueRepositoryExtension;

    @Autowired
    NotificationAdapter notificationAdapter;

    @Autowired
    private Settings settings;

    public NotificationCenterServiceExtension(NotificationCenterRepository notificationCenterRepository, NotificationCenterMapper notificationCenterMapper) {
        super(notificationCenterRepository, notificationCenterMapper);
    }

    /**
     * sent notification
     **/
    @Scheduled(cron = "0 */1 * * * *")
    @SchedulerLock(name = "processingSendingNotification")
    public void processingSendingNotification()throws Exception {
        if(!settings.isScronJobEnabled()){
            return;
        }
        log.info("Start sent notification at " + Instant.now().toString());

        //fetching all not completed
        List<NotificationCenter> notificationCenters = notificationCenterRepositoryExtension.findAllByExecuteStatus(IncentiveStatusEnum.New.name());

        notificationCenters.forEach(notificationCenter -> {

            notificationCenter.setAttemptCount(notificationCenter.getAttemptCount() + 1);
            notificationCenter.lastAttemptTime(Instant.now());

            String participantId = notificationCenter.getParticipantId();
            log.debug("participantId Notification = ", participantId);

            String eventId = notificationCenter.getEventId();
            log.debug("EventId Notification = ", eventId);

            log.debug("Notification Type = ", notificationCenter.getNotificationType().toString());

            switch (notificationCenter.getNotificationType()) {
                case WELLMETRIC_SCREENING_RESULT:
                    //get item which have not  not incentive yet
                    List<WellmetricEventQueue> wellmetricEventQueues = wellmetricEventQueueRepositoryExtension.findAllByParticipantIdAndEventIdAndIsWaitingPush(participantId, eventId, false);
                    if (wellmetricEventQueues.isEmpty()) {
                        // match condition to sent notification
                        notificationCenter.setIsReady(true);
                        if (notificationCenter.getReceiverName().equals(NotificationReceiverName.MONARCH.name())) {
                            sendIncentiveNotifications(notificationCenter);
                        }
                    }
                case INCENTIVE_RESULT:
                    // match condition to sent notification
                    notificationCenter.setIsReady(true);
                    if (notificationCenter.getReceiverName().equals(NotificationReceiverName.MONARCH.name())) {
                        sendIncentiveNotifications(notificationCenter);
                    }
                    break;

                default:
                    notificationCenter.setIsReady(true);
                    if (notificationCenter.getReceiverName().equals(NotificationReceiverName.MONARCH.name())) {
                        sendIncentiveNotifications(notificationCenter);
                    }
            }
        });
    }

    public void addNotifications(NotificationCenter notificationCenter) {
        //notify
        try {
            NotificationItemRequest notificationItemRequest = new NotificationItemRequest();
            notificationItemRequest.setRewardContent(notificationCenter.getContent());
            notificationItemRequest.setParticipantId(notificationCenter.getParticipantId());
            notificationItemRequest.setTitle(notificationCenter.getTitle());
            notificationItemRequest.setNotificationType(notificationCenter.getNotificationType().name());
            notificationItemRequest.setEventId(notificationCenter.getEventId());
            CompletableFuture<NotificationItemResponse> orderResponseCompletableFuture =  notificationAdapter.AddNotificationItem(notificationItemRequest);
            notificationCenter.setExecuteStatus(IncentiveStatusEnum.Completed.name());
            notificationCenterRepositoryExtension.save(notificationCenter);
        } catch (Exception e) {
            notificationCenter.setExecuteStatus(IncentiveStatusEnum.Error.name());
            notificationCenter.setErrorMessage(e.getMessage());
            notificationCenterRepositoryExtension.save(notificationCenter);
            e.printStackTrace();
        }
    }

    public void sendIncentiveNotifications(NotificationCenter notificationCenter) {
        //notify
        try {
            NotificationItemRequest notificationItemRequest = new NotificationItemRequest();
            notificationItemRequest.setContent(notificationCenter.getContent());
            notificationItemRequest.setParticipantId(notificationCenter.getParticipantId());
            notificationItemRequest.setTitle(notificationCenter.getTitle());
            notificationItemRequest.setNotificationType(notificationCenter.getNotificationType().name());
            notificationItemRequest.setEventId(notificationCenter.getEventId());
            CompletableFuture<NotificationItemResponse> orderResponseCompletableFuture =  notificationAdapter.sendIncentiveNotificationItem(notificationItemRequest);
            orderResponseCompletableFuture.thenAcceptAsync(notificationItemResponse -> {
                if (notificationItemResponse != null) {
                    notificationCenter.setExecuteStatus(IncentiveStatusEnum.Completed.name());
                    notificationCenterRepositoryExtension.save(notificationCenter);
                }
            }).exceptionally(throwable -> {
                notificationCenter.setExecuteStatus(IncentiveStatusEnum.Error.name());
                notificationCenter.setErrorMessage(throwable.getMessage());
                notificationCenterRepositoryExtension.save(notificationCenter);
                return null;
            });

        } catch (Exception e) {
            notificationCenter.setExecuteStatus(IncentiveStatusEnum.Error.name());
            notificationCenter.setErrorMessage(e.getMessage());
            notificationCenterRepositoryExtension.save(notificationCenter);
            e.printStackTrace();
        }
    }
}
