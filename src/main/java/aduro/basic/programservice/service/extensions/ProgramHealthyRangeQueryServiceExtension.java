package aduro.basic.programservice.service.extensions;

import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.domain.ProgramHealthyRange_;
import aduro.basic.programservice.domain.Program_;
import aduro.basic.programservice.repository.ProgramHealthyRangeRepository;
import aduro.basic.programservice.repository.extensions.ProgramHealthyRangeRepositoryExtension;
import aduro.basic.programservice.service.HealthyRangePayloadMapper;
import aduro.basic.programservice.service.ProgramHealthyRangeQueryService;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeCriteria;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.dto.ProgramHealthyRangePayLoad;
import aduro.basic.programservice.service.mapper.ProgramHealthyRangeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.JoinType;

@Service
@Primary
public class ProgramHealthyRangeQueryServiceExtension extends ProgramHealthyRangeQueryService {

    private final Logger log = LoggerFactory.getLogger(ProgramHealthyRangeQueryServiceExtension.class);

    private final ProgramHealthyRangeRepositoryExtension programHealthyRangeRepository;

    private final ProgramHealthyRangeMapper programHealthyRangeMapper;

    private final HealthyRangePayloadMapper healthyRangePayloadMapper;

    public ProgramHealthyRangeQueryServiceExtension(ProgramHealthyRangeRepository programHealthyRangeRepository, ProgramHealthyRangeMapper programHealthyRangeMapper, ProgramHealthyRangeRepositoryExtension programHealthyRangeRepository1, ProgramHealthyRangeMapper programHealthyRangeMapper1, HealthyRangePayloadMapper healthyRangePayloadMapper) {
        super(programHealthyRangeRepository, programHealthyRangeMapper);
        this.programHealthyRangeRepository = programHealthyRangeRepository1;
        this.programHealthyRangeMapper = programHealthyRangeMapper1;
        this.healthyRangePayloadMapper = healthyRangePayloadMapper;
    }

    @Transactional(readOnly = true)
    public Page<ProgramHealthyRangePayLoad> getByCriteria(ProgramHealthyRangeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramHealthyRange> specification = createSpecification(criteria);
        Page<ProgramHealthyRangeDTO> listHealthyDto = programHealthyRangeRepository.findAll(specification, page)
            .map(programHealthyRangeMapper::toDto);
        Page<ProgramHealthyRangePayLoad> map = listHealthyDto.map(dto -> healthyRangePayloadMapper.toPayload(dto));
        return map;
    }

    /*can not modified ProgramHealthyRangeQueryService because it generated*/
    private Specification<ProgramHealthyRange> createSpecification(ProgramHealthyRangeCriteria criteria) {
        Specification<ProgramHealthyRange> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramHealthyRange_.id));
            }
            if (criteria.getMaleMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaleMin(), ProgramHealthyRange_.maleMin));
            }
            if (criteria.getMaleMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaleMax(), ProgramHealthyRange_.maleMax));
            }
            if (criteria.getFemaleMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFemaleMin(), ProgramHealthyRange_.femaleMin));
            }
            if (criteria.getFemaleMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFemaleMax(), ProgramHealthyRange_.femaleMax));
            }
            if (criteria.getUnidentifiedMin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUnidentifiedMin(), ProgramHealthyRange_.unidentifiedMin));
            }
            if (criteria.getUnidentifiedMax() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUnidentifiedMax(), ProgramHealthyRange_.unidentifiedMax));
            }
            if (criteria.getIsApplyPoint() != null) {
                specification = specification.and(buildSpecification(criteria.getIsApplyPoint(), ProgramHealthyRange_.isApplyPoint));
            }
            if (criteria.getBiometricCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBiometricCode(), ProgramHealthyRange_.biometricCode));
            }
            if (criteria.getSubCategoryCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubCategoryCode(), ProgramHealthyRange_.subCategoryCode));
            }
            if (criteria.getSubCategoryId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSubCategoryId(), ProgramHealthyRange_.subCategoryId));
            }
            if (criteria.getProgramId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramId(),
                    root -> root.join(ProgramHealthyRange_.program, JoinType.LEFT).get(Program_.id)));
            }
        }
        return specification;
    }
}
