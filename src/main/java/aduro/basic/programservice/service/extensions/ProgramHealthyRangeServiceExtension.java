package aduro.basic.programservice.service.extensions;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.ProgramCategoryPoint;
import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import aduro.basic.programservice.domain.ProgramSubCategoryPoint;
import aduro.basic.programservice.domain.enumeration.BiometricCode;
import aduro.basic.programservice.repository.ProgramCategoryPointRepository;
import aduro.basic.programservice.repository.ProgramHealthyRangeRepository;
import aduro.basic.programservice.repository.ProgramSubCategoryPointRepository;
import aduro.basic.programservice.repository.extensions.ProgramHealthyRangeRepositoryExtension;
import aduro.basic.programservice.repository.extensions.ProgramSubCategoryConfigurationRepositoryExtension;
import aduro.basic.programservice.service.HealthyRangePayloadMapper;
import aduro.basic.programservice.service.businessrule.ProgramCategoryPointBusinessRule;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.dto.ProgramHealthyRangePayLoad;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationDTO;
import aduro.basic.programservice.service.impl.ProgramHealthyRangeServiceImpl;
import aduro.basic.programservice.service.mapper.ProgramHealthyRangeMapper;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryConfigurationMapper;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static aduro.basic.programservice.config.Constants.WELLMETRIC_BLOOD_PRESSURE;
import static aduro.basic.programservice.config.Constants.WELLMETRIC_BODY_COMPOSITION;

@Service
@Transactional
@Primary
public class ProgramHealthyRangeServiceExtension extends ProgramHealthyRangeServiceImpl {

    private final ProgramHealthyRangeRepositoryExtension programHealthyRangeRepositoryExtension;

    private final ProgramHealthyRangeMapper programHealthyRangeMapper;

    private final ProgramSubCategoryConfigurationMapper programSubCategoryConfigurationMapper;

    private final ProgramSubCategoryConfigurationRepositoryExtension programSubCategoryConfigurationRepository;

    private final HealthyRangePayloadMapper healthyRangePayloadMapper;

    private final ProgramSubCategoryPointRepository programSubCategoryPointRepository;

    private final ProgramCategoryPointRepository programCategoryPointRepository;

    private final ProgramCategoryPointBusinessRule programCategoryPointBusinessRule;

    public ProgramHealthyRangeServiceExtension(ProgramHealthyRangeRepository programHealthyRangeRepository, ProgramHealthyRangeMapper programHealthyRangeMapper, ProgramHealthyRangeRepositoryExtension programHealthyRangeRepositoryExtension, ProgramHealthyRangeMapper programHealthyRangeMapper1, ProgramSubCategoryConfigurationMapper programSubCategoryConfigurationMapper, ProgramSubCategoryConfigurationRepositoryExtension programSubCategoryConfigurationRepository, HealthyRangePayloadMapper healthyRangePayloadMapper, ProgramSubCategoryPointRepository programSubCategoryPointRepository, ProgramCategoryPointRepository programCategoryPointRepository, ProgramCategoryPointBusinessRule programCategoryPointBusinessRule) {
        super(programHealthyRangeRepository, programHealthyRangeMapper);
        this.programHealthyRangeRepositoryExtension = programHealthyRangeRepositoryExtension;
        this.programHealthyRangeMapper = programHealthyRangeMapper1;
        this.programSubCategoryConfigurationMapper = programSubCategoryConfigurationMapper;
        this.programSubCategoryConfigurationRepository = programSubCategoryConfigurationRepository;
        this.healthyRangePayloadMapper = healthyRangePayloadMapper;
        this.programSubCategoryPointRepository = programSubCategoryPointRepository;
        this.programCategoryPointRepository = programCategoryPointRepository;
        this.programCategoryPointBusinessRule = programCategoryPointBusinessRule;
    }

    public List<ProgramHealthyRangePayLoad> save(List<ProgramHealthyRangePayLoad> programHealthyRangeDTOList) {

        if (CollectionUtils.isEmpty(programHealthyRangeDTOList)) {
            return new ArrayList<>();
        }

        Long programId = programHealthyRangeDTOList.get(0).getProgramId();

        List<ProgramHealthyRangePayLoad> savingRanges = programHealthyRangeDTOList.stream()
            .filter(ProgramHealthyRangeDTO::isIsApplyPoint)
            .collect(Collectors.toList());

        List<Long> deleteRanges = programHealthyRangeDTOList.stream()
            .filter( p -> !p.isIsApplyPoint())
            .map(ProgramHealthyRangeDTO::getId)
            .collect(Collectors.toList());

        List<String> specialThings = Arrays.asList(
            BiometricCode.BP_Systolic__c.name(),
            BiometricCode.BP_Diastolic__c.name(), BiometricCode.BMI__c.name(), BiometricCode.Waist__c.name());

        List<String> biometricCodeDeletes = programHealthyRangeDTOList.stream()
            .filter(p -> !p.isIsApplyPoint())
            .map(ProgramHealthyRangeDTO::getBiometricCode)
            .filter(c -> !specialThings.contains(c))
            .collect(Collectors.toList());

        if (!biometricCodeDeletes.isEmpty()) {
            List<ProgramSubCategoryPoint> deletesCategoriesPoints = programSubCategoryPointRepository.findProgramSubCategoryPointWithCodeIn(Constants.WELLMETRICS, programId, biometricCodeDeletes);
            programSubCategoryPointRepository.deleteAll(deletesCategoriesPoints);
        }

        programHealthyRangeRepositoryExtension.deleteAllByIdIn(deleteRanges);

        List<ProgramHealthyRangePayLoad> healthyRangePayLoads = new ArrayList<>();

        for (ProgramHealthyRangePayLoad savingRange : savingRanges) {
            ProgramHealthyRangePayLoad save = save(savingRange);
            healthyRangePayLoads.add(save);
        }

        List<String> bloodPressureFields = Arrays.asList(BiometricCode.BP_Systolic__c.name(), BiometricCode.BP_Diastolic__c.name());

        //get BP
        List<ProgramHealthyRange> dbRangeBPs = programHealthyRangeRepositoryExtension.findByProgramIdAndBiometricCodeIn(programId, bloodPressureFields);

        List<ProgramSubCategoryPoint> programSubCategoryPoints = null;
        if (!dbRangeBPs.isEmpty()) {

            Optional<ProgramSubCategoryConfiguration> configurationOptional = programSubCategoryConfigurationRepository.findByProgramIdAndSubCategoryCode(programId, WELLMETRIC_BLOOD_PRESSURE);
            if (configurationOptional.isPresent()) {
                ProgramSubCategoryConfiguration programSubCategoryConfiguration = configurationOptional.get();

                if (programSubCategoryConfiguration.isIsBloodPressureSingleTest()) {
                    programSubCategoryPoints = programSubCategoryPointRepository.findProgramSubCategoryPointWithCodeIn(Constants.WELLMETRICS, programId, Arrays.asList(BiometricCode.BP_Systolic__c.name(), BiometricCode.BP_Diastolic__c.name()));
                }

                if (programSubCategoryConfiguration.isIsBloodPressureIndividualTest()) {
                    List<String> deleteSingleRange = bloodPressureFields.stream()
                        .filter(r -> !dbRangeBPs.stream().map(ProgramHealthyRange::getBiometricCode)
                            .collect(Collectors.toList()).contains(r)).collect(Collectors.toList());
                    List<String> deleteSingles = new ArrayList<>();
                    deleteSingles.addAll(deleteSingleRange);
                    deleteSingles.add(WELLMETRIC_BLOOD_PRESSURE);
                    programSubCategoryPoints = programSubCategoryPointRepository.findProgramSubCategoryPointWithCodeIn(Constants.WELLMETRICS, programId, deleteSingles);
                }

            }
        } else {
            programSubCategoryPoints = programSubCategoryPointRepository.findProgramSubCategoryPointWithCodeIn(Constants.WELLMETRICS, programId, Arrays.asList(BiometricCode.BP_Systolic__c.name(), BiometricCode.BP_Diastolic__c.name(), WELLMETRIC_BLOOD_PRESSURE));
        }

        if (!CollectionUtils.isEmpty(programSubCategoryPoints)) {
            programSubCategoryPointRepository.deleteAll(programSubCategoryPoints);
        }

        List<String> bcBiometrics = Arrays.asList(BiometricCode.Waist__c.name(), BiometricCode.BMI__c.name());

        List<ProgramHealthyRange> dbRangeBCs = programHealthyRangeRepositoryExtension.findByProgramIdAndBiometricCodeIn(programId, bcBiometrics);

        List<ProgramSubCategoryPoint> cbSubCategories = new ArrayList<>();
        switch (dbRangeBCs.size()) {
            case 0:
                List<String> cbLists = new ArrayList<>();
                cbLists.add(WELLMETRIC_BODY_COMPOSITION);
                cbLists.addAll(bcBiometrics);

                cbSubCategories = programSubCategoryPointRepository.findProgramSubCategoryPointWithCodeIn (Constants.WELLMETRICS, programId, cbLists);
                break;
            case 1: // waist or BMI
                List<String> totalCategoryBCs = Arrays.asList(BiometricCode.Waist__c.name(), BiometricCode.BMI__c.name(), WELLMETRIC_BODY_COMPOSITION);

                List<String> deleteCategoryCodes = totalCategoryBCs.stream().filter(r -> !dbRangeBCs.stream().map(ProgramHealthyRange::getBiometricCode).collect(Collectors.toList()).contains(r)).collect(Collectors.toList());

                cbSubCategories = programSubCategoryPointRepository.findProgramSubCategoryPointWithCodeIn(Constants.WELLMETRICS, programId, deleteCategoryCodes);
                break;
            case 2:
                List<String> rangesDeletes = Arrays.asList(BiometricCode.Waist__c.name(), BiometricCode.BMI__c.name());
                cbSubCategories = programSubCategoryPointRepository.findProgramSubCategoryPointWithCodeIn (Constants.WELLMETRICS, programId, rangesDeletes);
                break;

        }

        if (!cbSubCategories.isEmpty()) {
            programSubCategoryPointRepository.deleteAll(cbSubCategories);
        }
        return healthyRangePayLoads;
    }

    private List<ProgramHealthyRangePayLoad> saveHealthyRangeWithSubCategoryId(ProgramSubCategoryConfiguration configuration, List<ProgramHealthyRangePayLoad> programHealthyRangePayLoads) {
        // create configuration then save healthy ranges
        List<ProgramHealthyRangeDTO> result = new ArrayList<>();
        programHealthyRangePayLoads.stream().findFirst()
            .flatMap(programHealthyRangePayLoad -> {
                //save sub-category configuration
                ProgramSubCategoryConfiguration programSubCategoryConfiguration = programSubCategoryConfigurationMapper
                    .toEntity(programHealthyRangePayLoad.getProgramSubCategoryConfigurationDTO());
                programSubCategoryConfiguration.setProgramId(programHealthyRangePayLoad.getId());
                ProgramSubCategoryConfiguration saved = programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);
                return Optional.of(saved);
            }).ifPresent(configuration1 -> {
                //save theirs sub-category
            programHealthyRangePayLoads.stream()
                .map(payLoad -> healthyRangePayloadMapper.toProgramHealthyRangeDTO(payLoad))
                .forEach(programHealthyRangeDTO -> {
                    programHealthyRangeDTO.setProgramId(configuration1.getId());
                    programHealthyRangeDTO.setSubCategoryId(configuration1.getId());
                    programHealthyRangeDTO.setSubCategoryCode(configuration1.getSubCategoryCode());
                    ProgramHealthyRange range = programHealthyRangeMapper.toEntity(programHealthyRangeDTO);
                    ProgramHealthyRange saved = programHealthyRangeRepositoryExtension.saveAndFlush(range);
                    result.add(programHealthyRangeMapper.toDto(saved));
                });
        });

        return healthyRangePayloadMapper.toHealthyPayloadList(result);
    }

    private ProgramSubCategoryConfiguration getConditionGroup(ProgramHealthyRangePayLoad payload) {
        return healthyRangePayloadMapper.getProgramSubCategoryConfiguration(payload);
    }

    public ProgramHealthyRangePayLoad   save(ProgramHealthyRangePayLoad programHealthyRangePayLoad) {

        // create bio configuration
        ProgramSubCategoryConfigurationDTO dto = programHealthyRangePayLoad.getProgramSubCategoryConfigurationDTO();
        Optional<ProgramSubCategoryConfiguration> byProgramIdAndSubCategoryCode = programSubCategoryConfigurationRepository.findByProgramIdAndSubCategoryCode(dto.getProgramId(), dto.getSubCategoryCode());


        ProgramSubCategoryConfiguration configuration;
        if (!byProgramIdAndSubCategoryCode.isPresent()) {
            configuration = programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfigurationMapper.toEntity(dto));
        } else {
            configuration = byProgramIdAndSubCategoryCode.get();
            configuration.setProgramId(dto.getProgramId());
            configuration.setIsGlucoseAwardedInRange(dto.isIsGlucoseAwardedInRange());
            configuration.setIsBmiAwardedInRange(dto.isIsBmiAwardedInRange());
            configuration.setIsBmiAwardedNonFasting(dto.isIsBmiAwardedNonFasting());
            configuration.setIsBmiAwardedFasting(dto.isIsBmiAwardedFasting());
            configuration.setIsBloodPressureSingleTest(dto.isIsBloodPressureSingleTest());
            configuration.setIsBloodPressureIndividualTest(dto.isIsBloodPressureIndividualTest());
            configuration.setSubCategoryCode(dto.getSubCategoryCode());
            configuration = programSubCategoryConfigurationRepository.saveAndFlush(configuration);
        }

        // create program healthy range
        ProgramHealthyRangeDTO programHealthyRangeDTO = healthyRangePayloadMapper.toProgramHealthyRangeDTO(programHealthyRangePayLoad);
        programHealthyRangeDTO.setSubCategoryId(configuration.getId());
        programHealthyRangeDTO.setSubCategoryCode(configuration.getSubCategoryCode());
        ProgramHealthyRange range = programHealthyRangeRepositoryExtension.saveAndFlush(programHealthyRangeMapper.toEntity(programHealthyRangeDTO));
        return healthyRangePayloadMapper.toPayload(programHealthyRangeMapper.toDto(range));
    }

}
