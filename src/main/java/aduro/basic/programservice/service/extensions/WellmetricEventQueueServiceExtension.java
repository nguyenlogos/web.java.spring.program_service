package aduro.basic.programservice.service.extensions;
import aduro.basic.programservice.config.ApplicationProperties;
import aduro.basic.programservice.config.Settings;
import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.*;
import aduro.basic.programservice.helpers.DateHelpers;
import aduro.basic.programservice.repository.*;
import aduro.basic.programservice.repository.extensions.UserEventQueueRepositoryExtension;
import aduro.basic.programservice.repository.extensions.WellmetricEventQueueRepositoryExtension;
import aduro.basic.programservice.service.WellmetricEventQueueQueryService;
import aduro.basic.programservice.service.businessrule.OutcomeLiteRule;
import aduro.basic.programservice.service.businessrule.UserIncentiveBusinessRule;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.impl.WellmetricEventQueueServiceImpl;
import aduro.basic.programservice.service.mapper.UserEventMapper;
import aduro.basic.programservice.service.mapper.WellmetricEventQueueMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;

@Service
@Transactional
@Primary
public class WellmetricEventQueueServiceExtension extends WellmetricEventQueueServiceImpl {

    private final Logger log = LoggerFactory.getLogger(WellmetricEventQueueServiceExtension.class);

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    @Autowired
    private WellmetricEventQueueRepositoryExtension wellmetricEventQueueRepositoryExtension;

    @Autowired
    private ProgramUserRepository programUserRepository;

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private UserIncentiveBusinessRule userIncentiveBusinessRule;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private WellmetricEventQueueMapper wellmetricEventQueueMapper;

    @Autowired
    private WellmetricEventQueueQueryService wellmetricEventQueueQueryService;

    @Autowired
    private OutcomeLiteRule outcomeLiteRule;

    @Autowired
    private UserEventMapper userEventMapper;

    @Autowired
    private Settings settings;

    @Autowired
    private UserEventQueueRepositoryExtension userEventQueueRepositoryExtension;

    public WellmetricEventQueueServiceExtension(WellmetricEventQueueRepository wellmetricEventQueueRepository, WellmetricEventQueueMapper wellmetricEventQueueMapper) {
        super(wellmetricEventQueueRepository, wellmetricEventQueueMapper);

    }

    /**
     * Update status program base on to date
     *
     * This is scheduled to get fired every 10 minutes
     */
    @Scheduled(cron = "0 */10 * * * *")
    @SchedulerLock(name = "processBiometricEvent")
    public void processBiometricEvent()throws Exception {
        if(!settings.isScronJobEnabled()){
            return;
        }
        log.info("Start process biometric" + Instant.now().toString());

        //get all events queue
        outcomeLiteRule.processingIncentive();
    }



    @Transactional(readOnly = true)
    public List<WellmetricResultDTO> findScreeningResultByCriteria (WellmetricEventQueueCriteria criteria) {

        log.debug("find by criteria : {}", criteria);

        List<WellmetricResultDTO> rs = new ArrayList<>();

        List<WellmetricEventQueue> wellmetricEventQueues;

        List<WellmetricEventQueueDTO> wellmetricEventQueueDTOS;


        if (criteria.getClientId() == null || criteria.getClientId().getEquals().equals("")) {
            throw new BadRequestAlertException("clientId can not be null or empty.", "WellmetricCriteria", "ClientId");
        }
        String clientId = criteria.getClientId().getEquals();

        if (criteria.getParticipantId() == null|| criteria.getParticipantId().getEquals().equals("")) {
            throw new BadRequestAlertException("PaticipantId can not be null or empty.", "ProgramUserId", "ParticipantID");
        }

        String participantId = criteria.getParticipantId().getEquals();


        if (criteria.getAttemptedAt() == null || criteria.getAttemptedAt().equals("")) {
            wellmetricEventQueueDTOS = wellmetricEventQueueQueryService.findByCriteria(criteria);

        } else {
            Instant attemptedAt = criteria.getAttemptedAt().getEquals();
            Instant startOfDay = DateHelpers.getStarOfDay(Date.from(attemptedAt)).toInstant();
            Instant endOfDay = DateHelpers.getEndOfDay(Date.from(attemptedAt)).toInstant();
            wellmetricEventQueues =
                wellmetricEventQueueRepositoryExtension.findAllByParticipantIdAndClientIdAndAttemptedAtAfterAndAttemptedAtBefore
                    (participantId, clientId, startOfDay, endOfDay);
            wellmetricEventQueueDTOS = wellmetricEventQueueMapper.toDto(wellmetricEventQueues);

        }


        if (wellmetricEventQueueDTOS.isEmpty()){
            return rs;
        }

        WellmetricEventQueueDTO first = wellmetricEventQueueDTOS.get(0);

        Optional<ProgramUser> programUser = programUserRepository.getCurrentUserProgram(participantId, clientId, first.getProgramId());
        wellmetricEventQueueDTOS.stream().forEach(wellmetricEventQueueDTO -> {

            if (!wellmetricEventQueueDTO.getExecuteStatus().equals(IncentiveStatusEnum.Completed.name())) {
                rs.add(new WellmetricResultDTO(wellmetricEventQueueDTO));
            } else {
                if (programUser.isPresent()) {
                    Optional<UserEvent> userEvent = userEventRepository.getUserEventByProgramUserAndEventIdAndEventCode(programUser.get(), wellmetricEventQueueDTO.getId().toString(), wellmetricEventQueueDTO.getEventCode());
                    if (userEvent.isPresent()) {
                        rs.add(new WellmetricResultDTO(wellmetricEventQueueDTO, userEvent.get()));
                    } else {
                        rs.add(new WellmetricResultDTO(wellmetricEventQueueDTO));
                    }
                } else {
                    rs.add(new WellmetricResultDTO(wellmetricEventQueueDTO));
                }
            }
        });

        return rs;
    }


    @Transactional(readOnly = true)
    public ScreenIncentiveResultPayload findWellmetricResultByCriteria (WellmetricEventQueueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);

        ScreenIncentiveResultPayload payload = new ScreenIncentiveResultPayload();
        List<WellmetricEventQueue> wellmetricEventQueues;

        List<WellmetricEventQueueDTO> wellmetricEventQueueDTOS;

        if (criteria.getClientId() == null || criteria.getClientId().getEquals().equals("")) {
            throw new BadRequestAlertException("clientId can not be null or empty.", "WellmetricCriteria", "ClientId");
        }
        String clientId = criteria.getClientId().getEquals();

        if (criteria.getParticipantId() == null|| criteria.getParticipantId().getEquals().equals("")) {
            throw new BadRequestAlertException("PaticipantId can not be null or empty.", "ProgramUserId", "ParticipantID");
        }

        String participantId = criteria.getParticipantId().getEquals();


        if (criteria.getAttemptedAt() == null || criteria.getAttemptedAt().equals("")) {
            wellmetricEventQueueDTOS = wellmetricEventQueueQueryService.findByCriteria(criteria);

        } else {
            Instant attemptedAt = criteria.getAttemptedAt().getEquals();
            Instant startOfDay = DateHelpers.getStarOfDay(Date.from(attemptedAt)).toInstant();
            Instant endOfDay = DateHelpers.getEndOfDay(Date.from(attemptedAt)).toInstant();
            wellmetricEventQueues =
                wellmetricEventQueueRepositoryExtension.findAllByParticipantIdAndClientIdAndAttemptedAtAfterAndAttemptedAtBefore
                    (participantId, clientId,startOfDay, endOfDay);
            wellmetricEventQueueDTOS = wellmetricEventQueueMapper.toDto(wellmetricEventQueues);

        }

        if (wellmetricEventQueueDTOS.isEmpty()){
            return payload;
        }

        // get screening result with only biometrics
        List<WellmetricResultDTO> resultDTOS = wellmetricEventQueueDTOS.stream().map(WellmetricResultDTO::new)
            .filter(a -> !isNullOrEmpty(a.getEventCode()) && !a.getEventCode().equalsIgnoreCase(Constants.WELLMETRIC_REGISTRATION))
            .filter(b -> !isNullOrEmpty(b.getEventCode()) &&  !b.getEventCode().equalsIgnoreCase(Constants.WELLMETRIC_SCREENING))
            .collect(Collectors.toList());
        payload.setScreenResult(resultDTOS);


        List<ProgramUser> programUsers = programUserRepository.findProgramUserByParticipantIdAndClientId(participantId, clientId).collect(Collectors.toList());

        List<UserEventDTO> allUserEvents = new ArrayList<>();

        // get user events with only biometrics
        if (!programUsers.isEmpty()) {

            programUsers.stream().forEach(user -> {
                List<String> eventIdByProgramIds = userEventQueueRepositoryExtension.findByProgramUserIdAndProgramIdAndEventCategory(user.getId(), user.getProgramId(), Constants.WELLMETRICS)
                    .filter(a ->!isNullOrEmpty(a.getEventCode()) &&  !a.getEventCode().equalsIgnoreCase(Constants.WELLMETRIC_REGISTRATION))
                    .filter(b -> !isNullOrEmpty(b.getEventCode()) &&  !b.getEventCode().equalsIgnoreCase(Constants.WELLMETRIC_SCREENING))
                    .filter(c -> !isNullOrEmpty(c.getEventCode()) &&  c.getExecuteStatus().equalsIgnoreCase(IncentiveStatusEnum.Completed.name()))
                    .map(q -> q.getEventId()).collect(Collectors.toList());

                List<UserEvent> userEvent = userEventRepository.findUserEventsByProgramUserIdAndEventCategoryAndEventIdIn(user.getId(), Constants.WELLMETRICS, eventIdByProgramIds);
                if (!userEvent.isEmpty()) {
                    List<UserEventDTO> userEventDTOS = userEventMapper.toDto(userEvent);
                    userEventDTOS.forEach( e -> e.setProgramId(user.getProgramId()));
                    allUserEvents.addAll(userEventDTOS);
                }
            });

            payload.setUserEvents(allUserEvents);
        }

        return payload;
    }

    @Transactional(readOnly = true)
    public ScreenIncentiveResultPayload findWellmetricResult (WellmetricEventQueueCriteria criteria) {
        log.debug("find by criteria : {}", criteria);

        ScreenIncentiveResultPayload payload = new ScreenIncentiveResultPayload();
        List<WellmetricEventQueue> wellmetricEventQueues;
        List<WellmetricEventQueueDTO> wellmetricEventQueueDTOS;
        if (criteria.getClientId() == null || criteria.getClientId().getEquals().equals("")) {
            throw new BadRequestAlertException("clientId can not be null or empty.", "WellmetricCriteria", "ClientId");
        }

        String clientId = criteria.getClientId().getEquals();
        if (criteria.getParticipantId() == null|| criteria.getParticipantId().getEquals().equals("")) {
            throw new BadRequestAlertException("PaticipantId can not be null or empty.", "ProgramUserId", "ParticipantID");
        }

        String participantId = criteria.getParticipantId().getEquals();
        if (criteria.getAttemptedAt() == null || criteria.getAttemptedAt().equals("")) {
            wellmetricEventQueueDTOS = wellmetricEventQueueQueryService.findByCriteria(criteria);
        } else {
            Instant attemptedAt = criteria.getAttemptedAt().getEquals();
            Instant startOfDay = DateHelpers.getStarOfDay(Date.from(attemptedAt)).toInstant();
            Instant endOfDay = DateHelpers.getEndOfDay(Date.from(attemptedAt)).toInstant();
            wellmetricEventQueues =
                wellmetricEventQueueRepositoryExtension.findAllByParticipantIdAndClientIdAndAttemptedAtAfterAndAttemptedAtBefore
                    (participantId, clientId,startOfDay, endOfDay);
            wellmetricEventQueueDTOS = wellmetricEventQueueMapper.toDto(wellmetricEventQueues);
        }

        if (wellmetricEventQueueDTOS.isEmpty()){
            return payload;
        }

        // Get screening result
        List<WellmetricResultDTO> resultDTOS = wellmetricEventQueueDTOS.stream().map(WellmetricResultDTO::new)
            .collect(Collectors.toList());
        payload.setScreenResult(resultDTOS);

        List<ProgramUser> programUsers = programUserRepository.findProgramUserByParticipantIdAndClientId(participantId, clientId).collect(Collectors.toList());

        List<UserEventDTO> allUserEvents = new ArrayList<>();

        // Get user events with
        if (!programUsers.isEmpty()) {
            programUsers.stream().forEach(user -> {
                List<String> eventIdByProgramIds = userEventQueueRepositoryExtension
                    .findByProgramUserIdAndProgramIdAndEventCategory(user.getId(), user.getProgramId(), Constants.WELLMETRICS)
                    .filter(c -> !isNullOrEmpty(c.getEventCode()) &&  c.getExecuteStatus().equalsIgnoreCase(IncentiveStatusEnum.Completed.name()))
                    .map(q -> q.getEventId()).collect(Collectors.toList());

                List<UserEvent> userEvent = userEventRepository
                    .findUserEventsByProgramUserIdAndEventCategoryAndEventIdIn(user.getId(), Constants.WELLMETRICS, eventIdByProgramIds);
                if (!userEvent.isEmpty()) {
                    List<UserEventDTO> userEventDTOS = userEventMapper.toDto(userEvent);
                    userEventDTOS.forEach( e -> e.setProgramId(user.getProgramId()));
                    allUserEvents.addAll(userEventDTOS);
                }
            });

            payload.setUserEvents(allUserEvents);
        }

        return payload;
    }
}
