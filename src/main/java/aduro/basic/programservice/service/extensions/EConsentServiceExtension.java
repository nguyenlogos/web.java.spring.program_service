package aduro.basic.programservice.service.extensions;

import aduro.basic.programservice.ap.CustomActivityAdapter;
import aduro.basic.programservice.ap.dto.UserDto;
import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.EConsent;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.TermAndCondition;
import aduro.basic.programservice.repository.ClientProgramRepository;
import aduro.basic.programservice.repository.EConsentRepository;
import aduro.basic.programservice.repository.TermAndConditionRepository;
import aduro.basic.programservice.service.dto.EConsentDTO;
import aduro.basic.programservice.service.dto.ProgramStatus;
import aduro.basic.programservice.service.impl.EConsentServiceImpl;
import aduro.basic.programservice.service.mapper.EConsentMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Primary
public class EConsentServiceExtension extends EConsentServiceImpl {

    private final EConsentMapper eConsentMapper;

    private final ClientProgramRepository clientProgramRepository;

    @Autowired
    private EConsentRepository eConsentRepository;

    @Autowired
    private TermAndConditionRepository termAndConditionRepository;


    public EConsentServiceExtension(EConsentRepository eConsentRepository, EConsentMapper eConsentMapper, EConsentMapper eConsentMapper1, ClientProgramRepository clientProgramRepository) {
        super(eConsentRepository, eConsentMapper);
        this.eConsentMapper = eConsentMapper1;
        this.clientProgramRepository = clientProgramRepository;
    }

    public EConsentDTO saveAdvance(EConsentDTO eConsentDTO, String clientId, String subgroupId) {

        if (clientId == null || clientId.equalsIgnoreCase("")) {
            throw new BadRequestAlertException("Client Id Not Null Or Empty.", "EConsent", "clientId");
        }

        if (eConsentDTO.getCreatedAt() == null) {
            eConsentDTO.setCreatedAt(Instant.now());
        }

        Optional<ClientProgram> clientProgramOptional = clientProgramRepository.findByClientIdAndProgramStatusAndProgramQaVerify(clientId, ProgramStatus.Active.name(), eConsentDTO.isAsProgramTester());
        if (!clientProgramOptional.isPresent()) {
            throw new BadRequestAlertException("Client Not Existed", "EConsent", "clientId");
        }

        Program program = clientProgramOptional.get().getProgram();
        eConsentDTO.setProgramId(program.getId());

        if (eConsentDTO.isHasConfirmed() == null) {
            eConsentDTO.setHasConfirmed(true);
        }

        List<TermAndCondition> termAndConditionList = termAndConditionRepository.findTermAndConditionsByClientId(clientId);

        Optional<TermAndCondition> optionalTermAndConditionBySubgroup = termAndConditionList.stream().filter(t-> t.getSubgroupId() != null && t.getSubgroupId().equals(subgroupId)).findFirst();
        if (optionalTermAndConditionBySubgroup.isPresent()) {
            TermAndCondition termAndCondition = optionalTermAndConditionBySubgroup.get();
            eConsentDTO.setTermAndConditionId(termAndCondition.getId());
        } else {
            Optional<TermAndCondition> nonSubgroupCondition = termAndConditionList.stream().filter(t -> t.getSubgroupId() == null || t.getSubgroupId().equals("undefined") || t.getSubgroupId().equals("0")).findFirst();
            if (nonSubgroupCondition.isPresent()) {
                eConsentDTO.setTermAndConditionId(nonSubgroupCondition.get().getId());
            } else {
                eConsentDTO.setTermAndConditionId(0L);
            }
        }

        List<EConsent> existed =  eConsentRepository.findByProgramIdAndParticipantId(program.getId(), eConsentDTO.getParticipantId());

        if(existed == null || existed.isEmpty() ) {
            EConsent eConsent = eConsentRepository.saveAndFlush(eConsentMapper.toEntity(eConsentDTO));
            return  eConsentMapper.toDto(eConsent);
        }else{
            return  eConsentMapper.toDto(existed.get(0));
        }
    }
}
