package aduro.basic.programservice.service;

import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import aduro.basic.programservice.repository.ProgramSubCategoryConfigurationRepository;
import aduro.basic.programservice.repository.extensions.ProgramSubCategoryConfigurationRepositoryExtension;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.dto.ProgramHealthyRangePayLoad;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationDTO;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryConfigurationMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class HealthyRangePayloadMapperImpl implements HealthyRangePayloadMapper {

    private final ProgramSubCategoryConfigurationRepositoryExtension repository;

    private final ProgramSubCategoryConfigurationMapper mapper;

    public HealthyRangePayloadMapperImpl(ProgramSubCategoryConfigurationRepositoryExtension repository, ProgramSubCategoryConfigurationMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public ProgramHealthyRangeDTO toProgramHealthyRangeDTO(ProgramHealthyRangePayLoad payLoad) {

        ProgramHealthyRangeDTO dto = new ProgramHealthyRangeDTO();
        dto.setId(payLoad.getId());
        dto.setBiometricCode(payLoad.getBiometricCode());
        dto.setFemaleMax(payLoad.getFemaleMax());
        dto.setFemaleMin(payLoad.getFemaleMin());
        dto.setIsApplyPoint(payLoad.isIsApplyPoint());
        dto.setMaleMax(payLoad.getMaleMax());
        dto.setMaleMin(payLoad.getMaleMin());
        dto.setProgramId(payLoad.getProgramId());
        dto.setUnidentifiedMax(payLoad.getUnidentifiedMax());
        dto.setUnidentifiedMin(payLoad.getUnidentifiedMin());
        dto.setSubCategoryCode(payLoad.getSubCategoryCode());
        dto.setSubCategoryId(payLoad.getSubCategoryId());
        return dto;
    }

    @Override
    public ProgramHealthyRangePayLoad toPayload(ProgramHealthyRangeDTO dto) {
        ProgramHealthyRangePayLoad programHealthyRangePayLoad = new ProgramHealthyRangePayLoad();
        programHealthyRangePayLoad.setId(dto.getId());
        programHealthyRangePayLoad.setBiometricCode(dto.getBiometricCode());
        programHealthyRangePayLoad.setFemaleMax(dto.getFemaleMax());
        programHealthyRangePayLoad.setFemaleMin(dto.getFemaleMin());
        programHealthyRangePayLoad.setIsApplyPoint(dto.isIsApplyPoint());
        programHealthyRangePayLoad.setMaleMax(dto.getMaleMax());
        programHealthyRangePayLoad.setMaleMin(dto.getMaleMin());
        programHealthyRangePayLoad.setProgramId(dto.getProgramId());
        programHealthyRangePayLoad.setUnidentifiedMax(dto.getUnidentifiedMax());
        programHealthyRangePayLoad.setUnidentifiedMin(dto.getUnidentifiedMin());
        programHealthyRangePayLoad.setProgramId(dto.getProgramId());
        programHealthyRangePayLoad.setSubCategoryCode(dto.getSubCategoryCode());
        programHealthyRangePayLoad.setSubCategoryId(dto.getSubCategoryId());
        if (dto.getSubCategoryId() != null) {
            repository.findById(dto.getSubCategoryId()).ifPresent(configuration ->  programHealthyRangePayLoad.setProgramSubCategoryConfigurationDTO(mapper.toDto(configuration)));
        }
        return programHealthyRangePayLoad;
    }

    @Override
    public ProgramSubCategoryConfiguration getProgramSubCategoryConfiguration(ProgramHealthyRangePayLoad payload) {
        ProgramSubCategoryConfigurationDTO subCategoryConfigurationDTO = payload.getProgramSubCategoryConfigurationDTO();
        ProgramSubCategoryConfiguration configuration = new ProgramSubCategoryConfiguration();
        configuration.setId(subCategoryConfigurationDTO.getId());
        configuration.setSubCategoryCode(subCategoryConfigurationDTO.getSubCategoryCode());
        configuration.setIsBloodPressureIndividualTest(subCategoryConfigurationDTO.isIsBloodPressureIndividualTest());
        configuration.setIsBloodPressureSingleTest(subCategoryConfigurationDTO.isIsBloodPressureSingleTest());
        configuration.setIsBmiAwardedFasting(subCategoryConfigurationDTO.isIsBmiAwardedFasting());
        configuration.setIsBmiAwardedNonFasting(subCategoryConfigurationDTO.isIsBmiAwardedNonFasting());
        configuration.setIsBmiAwardedInRange(subCategoryConfigurationDTO.isIsBmiAwardedInRange());
        configuration.setIsGlucoseAwardedInRange(subCategoryConfigurationDTO.isIsGlucoseAwardedInRange());
        configuration.setProgramId(subCategoryConfigurationDTO.getProgramId());
        return configuration;
    }

    @Override
    public List<ProgramHealthyRangePayLoad> toHealthyPayloadList(List<ProgramHealthyRangeDTO> rangeSet) {
        return  rangeSet.stream()
            .map(programHealthyRange -> {
                ProgramHealthyRangePayLoad payLoad = this.toPayload(programHealthyRange);
                repository.findByProgramIdAndSubCategoryCode(programHealthyRange.getProgramId(), programHealthyRange.getSubCategoryCode())
                    .ifPresent(configuration -> payLoad.setProgramSubCategoryConfigurationDTO(mapper.toDto(configuration)));
                return  payLoad;
            }).collect(Collectors.toList());
    }
}
