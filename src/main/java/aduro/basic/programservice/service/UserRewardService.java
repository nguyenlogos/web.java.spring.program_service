package aduro.basic.programservice.service;

import aduro.basic.programservice.service.dto.UserRewardDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link aduro.basic.programservice.domain.UserReward}.
 */
public interface UserRewardService {

    /**
     * Save a userReward.
     *
     * @param userRewardDTO the entity to save.
     * @return the persisted entity.
     */
    UserRewardDTO save(UserRewardDTO userRewardDTO);

    /**
     * Get all the userRewards.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserRewardDTO> findAll(Pageable pageable);


    /**
     * Get the "id" userReward.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserRewardDTO> findOne(Long id);

    /**
     * Delete the "id" userReward.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    void triggerUserRewards();
}
