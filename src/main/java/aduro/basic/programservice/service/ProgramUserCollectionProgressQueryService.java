package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.ProgramUserCollectionProgress;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramUserCollectionProgressRepository;
import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressCriteria;
import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressDTO;
import aduro.basic.programservice.service.mapper.ProgramUserCollectionProgressMapper;

/**
 * Service for executing complex queries for {@link ProgramUserCollectionProgress} entities in the database.
 * The main input is a {@link ProgramUserCollectionProgressCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramUserCollectionProgressDTO} or a {@link Page} of {@link ProgramUserCollectionProgressDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramUserCollectionProgressQueryService extends QueryService<ProgramUserCollectionProgress> {

    private final Logger log = LoggerFactory.getLogger(ProgramUserCollectionProgressQueryService.class);

    private final ProgramUserCollectionProgressRepository programUserCollectionProgressRepository;

    private final ProgramUserCollectionProgressMapper programUserCollectionProgressMapper;

    public ProgramUserCollectionProgressQueryService(ProgramUserCollectionProgressRepository programUserCollectionProgressRepository, ProgramUserCollectionProgressMapper programUserCollectionProgressMapper) {
        this.programUserCollectionProgressRepository = programUserCollectionProgressRepository;
        this.programUserCollectionProgressMapper = programUserCollectionProgressMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramUserCollectionProgressDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramUserCollectionProgressDTO> findByCriteria(ProgramUserCollectionProgressCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProgramUserCollectionProgress> specification = createSpecification(criteria);
        return programUserCollectionProgressMapper.toDto(programUserCollectionProgressRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramUserCollectionProgressDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramUserCollectionProgressDTO> findByCriteria(ProgramUserCollectionProgressCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProgramUserCollectionProgress> specification = createSpecification(criteria);
        return programUserCollectionProgressRepository.findAll(specification, page)
            .map(programUserCollectionProgressMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramUserCollectionProgressCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProgramUserCollectionProgress> specification = createSpecification(criteria);
        return programUserCollectionProgressRepository.count(specification);
    }

    /**
     * Function to convert ProgramUserCollectionProgressCriteria to a {@link Specification}.
     */
    private Specification<ProgramUserCollectionProgress> createSpecification(ProgramUserCollectionProgressCriteria criteria) {
        Specification<ProgramUserCollectionProgress> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ProgramUserCollectionProgress_.id));
            }
            if (criteria.getCollectionId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCollectionId(), ProgramUserCollectionProgress_.collectionId));
            }
            if (criteria.getTotalRequiredItems() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalRequiredItems(), ProgramUserCollectionProgress_.totalRequiredItems));
            }
            if (criteria.getCurrentProgress() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCurrentProgress(), ProgramUserCollectionProgress_.currentProgress));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), ProgramUserCollectionProgress_.status));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ProgramUserCollectionProgress_.createdDate));
            }
            if (criteria.getModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getModifiedDate(), ProgramUserCollectionProgress_.modifiedDate));
            }
            if (criteria.getProgramUserCohortId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramUserCohortId(),
                    root -> root.join(ProgramUserCollectionProgress_.programUserCohort, JoinType.LEFT).get(ProgramUserCohort_.id)));
            }
        }
        return specification;
    }
}
