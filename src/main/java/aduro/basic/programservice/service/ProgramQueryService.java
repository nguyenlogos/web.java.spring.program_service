package aduro.basic.programservice.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import aduro.basic.programservice.service.mapper.ProgramMapperForSearchImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.*; // for static metamodels
import aduro.basic.programservice.repository.ProgramRepository;
import aduro.basic.programservice.service.dto.ProgramCriteria;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.service.mapper.ProgramMapper;

/**
 * Service for executing complex queries for {@link Program} entities in the database.
 * The main input is a {@link ProgramCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProgramDTO} or a {@link Page} of {@link ProgramDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProgramQueryService extends QueryService<Program> {

    private final Logger log = LoggerFactory.getLogger(ProgramQueryService.class);

    private final ProgramRepository programRepository;

    private final ProgramMapper programMapper;

    @Autowired
    private  ProgramMapperForSearchImpl programMapperForSearch;

    public ProgramQueryService(ProgramRepository programRepository, ProgramMapper programMapper) {
        this.programRepository = programRepository;
        this.programMapper = programMapper;
    }

    /**
     * Return a {@link List} of {@link ProgramDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProgramDTO> findByCriteria(ProgramCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Program> specification = createSpecification(criteria);
        return programMapper.toDto(programRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProgramDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProgramDTO> findByCriteria(ProgramCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Program> specification = createSpecification(criteria);
        return programRepository.findAll(specification, page)
            .map(programMapperForSearch::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProgramCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Program> specification = createSpecification(criteria);
        return programRepository.count(specification);
    }

    /**
     * Function to convert ProgramCriteria to a {@link Specification}.
     */
    private Specification<Program> createSpecification(ProgramCriteria criteria) {
        Specification<Program> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Program_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Program_.name));
            }
            if (criteria.getStartDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartDate(), Program_.startDate));
            }
            if (criteria.getResetDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getResetDate(), Program_.resetDate));
            }
            if (criteria.getLastSent() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastSent(), Program_.lastSent));
            }
            if (criteria.getIsRetriggerEmail() != null) {
                specification = specification.and(buildSpecification(criteria.getIsRetriggerEmail(), Program_.isRetriggerEmail));
            }
            if (criteria.getIsEligible() != null) {
                specification = specification.and(buildSpecification(criteria.getIsEligible(), Program_.isEligible));
            }
            if (criteria.getIsSentRegistrationEmail() != null) {
                specification = specification.and(buildSpecification(criteria.getIsSentRegistrationEmail(), Program_.isSentRegistrationEmail));
            }
            if (criteria.getIsRegisteredForPlatform() != null) {
                specification = specification.and(buildSpecification(criteria.getIsRegisteredForPlatform(), Program_.isRegisteredForPlatform));
            }
            if (criteria.getIsScheduledScreening() != null) {
                specification = specification.and(buildSpecification(criteria.getIsScheduledScreening(), Program_.isScheduledScreening));
            }
            if (criteria.getIsFunctionally() != null) {
                specification = specification.and(buildSpecification(criteria.getIsFunctionally(), Program_.isFunctionally));
            }
            if (criteria.getLogoUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLogoUrl(), Program_.logoUrl));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Program_.description));
            }
            if (criteria.getUserPoint() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUserPoint(), Program_.userPoint));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), Program_.lastModifiedDate));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), Program_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), Program_.lastModifiedBy));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Program_.createdBy));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), Program_.status));
            }
            if (criteria.getIsUsePoint() != null) {
                specification = specification.and(buildSpecification(criteria.getIsUsePoint(), Program_.isUsePoint));
            }
            if (criteria.getIsScreen() != null) {
                specification = specification.and(buildSpecification(criteria.getIsScreen(), Program_.isScreen));
            }

            if (criteria.getIsCoaching() != null) {
                specification = specification.and(buildSpecification(criteria.getIsCoaching(), Program_.isCoaching));
            }
            if (criteria.getIsWellMatric() != null) {
                specification = specification.and(buildSpecification(criteria.getIsWellMatric(), Program_.isWellMatric));
            }
            if (criteria.getIsHP() != null) {
                specification = specification.and(buildSpecification(criteria.getIsHP(), Program_.isHp));
            }
            if (criteria.getIsUseLevel() != null) {
                specification = specification.and(buildSpecification(criteria.getIsUseLevel(), Program_.isUseLevel));
            }

            if (criteria.getIsTemplate() != null) {
                specification = specification.and(buildSpecification(criteria.getIsTemplate(), Program_.isTemplate));
            }

            if (criteria.getIsHPSF() != null) {
                specification = specification.and(buildSpecification(criteria.getIsHPSF(), Program_.isHPSF));
            }
            if (criteria.getIsHTK() != null) {
                specification = specification.and(buildSpecification(criteria.getIsHTK(), Program_.isHTK));
            }
            if (criteria.getIsLabcorp() != null) {
                specification = specification.and(buildSpecification(criteria.getIsLabcorp(), Program_.isLabcorp));
            }
            if (criteria.getLabcorpAccountNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabcorpAccountNumber(), Program_.labcorpAccountNumber));
            }
            if (criteria.getLabcorpFileUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLabcorpFileUrl(), Program_.labcorpFileUrl));
            }

            if (criteria.getQaVerify() != null) {
                specification = specification.and(buildSpecification(criteria.getQaVerify(), Program_.qaVerify));
            }

            if (criteria.getProgramCategoryPointId() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramCategoryPointId(),
                    root -> root.join(Program_.programCategoryPoints, JoinType.LEFT).get(ProgramCategoryPoint_.id)));
            }
            if(criteria.getClientId() != null) {
                specification = specification.and(buildSpecification(criteria.getClientId(),
                    root -> root.join(Program_.clientPrograms, JoinType.LEFT).get(ClientProgram_.clientId)));
            }

            if (criteria.getProgramType() != null) {
                specification = specification.and(buildSpecification(criteria.getProgramType(), Program_.programType));
            }

        }
        return specification;
    }
}
