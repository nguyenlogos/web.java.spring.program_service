package aduro.basic.programservice.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String DEFAULT_LANGUAGE = "en";
    public static final String ANONYMOUS_USER = "anonymoususer";


    public static final String ACTIVITIES = "ACTIVITIES";

    // Activity Wellmetric type
    public static final String ACTIVITY_WELLMETRICS_SCREENING = "WELLMETRICS_SCREENING";


    public static final String HPCONTENT = "HPCONTENT";

    public static final String PATHS = "PATHS";
    public static final String PATH = "PATH";

    public static final String WELLMETRICS = "WELLMETRICS";

    public static final String WELLMETRIC_SCREENING = "WELLMETRIC_SCREENNING";

    public static final String PREVENTIVE_SCREENINGS = "PREVENTIVE_SCREENINGS";
    public static final String PREVENTIVE_CUSTOM_ACTIVITY_TYPE = "PREVENTIVE_FORM";

    public static final String PREVENTIVE_SCREENINGS_EVENT_CODE = "PREVENTIVE_SCREENING";
    public static final String APPEAL_SCREENINGS_EVENT_CODE = "APPEAL_SCREENING";

    public static final String WELLMETRIC_REGISTRATION = "WELLMETRIC_REGISTRATION";

    public static final  String HUMAN_PERFORMANCE_ASSESSMENT = "HUMAN_PERFORMANCE_ASSESSMENT";
    public static final  String FLOURING_INDEX = "FLOURING_INDEX";
    public static final  String OTHER_ASSESSMENT = "OTHER_ASSESSMENT";
    public static final  String ACTIVITY_TOBACCO_ATTESTATION = "WELLMETRIC_TOBACCO_ATTESTATION";
    public static final  String WELLMETRIC_TOBACCO_RAS = "WELLMETRIC_TOBACCO_RAS";
    public static final  String COACHING = "COACH";
    public static final  String HPINTERACTIONS = "HPINTERACTIONS";
    public static final  String ASSESSMENTS = "ASSESSMENTS";

    public static final String YYYY_MM_DD =  "yyyy-MM-dd";

    public static final String CLIENT_CENTER = "ClientCenter";

    public static final String EMPLOYEE_SERVICE = "EMPLOYEE_SERVICE";

    public static final String MONARCH = "MONARCH";
    public static final String ERR_USER_NOT_FOUND = "ERR_USER_NOT_FOUND_OR_NO_ACTIVE_PROGRAM";

    public static final String BONUS = "BONUS";
    public static final String BONUS_ACTIVITY = "BONUS_ACTIVITY";

    public static final String DATE_FULL_BIO_SERVICE_RECEIVE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";
    public static final String DATE_MONARCH_RECEIVE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    public static final String USER_TYPE = "User";

    public static final String JOB_NAME_INCENTIVE = "RUN_INCENTIVE_USER";

    public static final String EMPLOYER_VERIFIED = "EMPLOYER_VERIFIED";
    public static final String PROGRAM_NOT_FOUND = "PROGRAM_NOT_FOUND";
    public static final String ID_NOT_FOUND = "ID_NOT_FOUND";
    public static final String COLLECTION_NOT_FOUND = "COLLECTION_NOT_FOUND";
    public static final String PROGRAM_TYPE_ERR = "PROGRAM_TYPE_ERR_NOT_CHANGED";
    public static final String COLLECTION_HAS_EXISTED = "COLLECTION_HAS_EXISTED";
    public static final String COLLECTION_REQUIRED_COMPLETION = "COLLECTION_REQUIRED_COMPLETION";
    public static final String COLLECTION_REQUIRED_LEVEL = "COLLECTION_REQUIRED_LEVEL";
    public static final String COHORT_DATA_INPUT_NOT_SUPPORTED = "COHORT_DATA_INPUT_NOT_SUPPORTED";
    public static final String COHORT_NOT_FOUND = "COHORT_NOT_FOUND";
    public static final String COHORT_ERROR_DEFAULT = "COHORT_ERROR_DEFAULT";
    public static final String PROGRAM_HAS_ACTIVATED ="PROGRAM_HAS_ACTIVATED";
    public static final String FULL_OUTCOMES_ERR = "FULL_OUTCOMES_ERR";
    public static final String CATEGORIZE_BIO_USER = "CATEGORIZE_BIO_USER";
    public static final String CLIENT_PROGRAM_ERR = "CLIENT_PROGRAM_ERR";
    public static final String ACTIVITY = "ACTIVITY";
    public static final String COHORT_NOT_CHANGED = "COHORT_NOT_CHANGED";
    public static final String COHORT_NAME_EXISTED = "COHORT_NAME_EXISTED";
    public static final String WELLMETRIC_BLOOD_PRESSURE = "WELLMETRIC_BLOOD_PRESSURE";
    public static final String WELLMETRIC_BODY_COMPOSITION = "WELLMETRIC_BODY_COMPOSITION";
    public static final String MEMBER_ID = "Member_ID";
    public static final int BATCH_SIZE = 10;

    private Constants() {
    }



}
