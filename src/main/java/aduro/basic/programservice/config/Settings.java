package aduro.basic.programservice.config;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
@Setter
public class Settings implements InitializingBean {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    // default values
    // base url
    @Value("${job.cronjob-enabled:true}")
    private boolean scronJobEnabled;

    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
