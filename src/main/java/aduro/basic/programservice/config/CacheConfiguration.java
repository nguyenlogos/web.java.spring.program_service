package aduro.basic.programservice.config;

import java.time.Duration;

import aduro.basic.programservice.domain.ActivityEventQueue;
import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, aduro.basic.programservice.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, aduro.basic.programservice.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, aduro.basic.programservice.domain.User.class.getName());
            createCache(cm, aduro.basic.programservice.domain.Authority.class.getName());
            createCache(cm, aduro.basic.programservice.domain.User.class.getName() + ".authorities");
            createCache(cm, aduro.basic.programservice.domain.Category.class.getName());
            createCache(cm, aduro.basic.programservice.domain.Program.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCategoryPoint.class.getName());
            createCache(cm, aduro.basic.programservice.domain.Program.class.getName() + ".programCategoryPoints");
            createCache(cm, aduro.basic.programservice.domain.ClientProgram.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramLevel.class.getName());
            createCache(cm, aduro.basic.programservice.domain.Program.class.getName() + ".programLevels");
            createCache(cm, aduro.basic.programservice.domain.ProgramLevelActivity.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramLevel.class.getName() + ".programLevelActivities");
            createCache(cm, aduro.basic.programservice.domain.Reward.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramActivity.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramLevelReward.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramLevelReward.class.getName() + ".rewards");
            createCache(cm, aduro.basic.programservice.domain.SubCategory.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramSubCategoryPoint.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramUser.class.getName());
            createCache(cm, aduro.basic.programservice.domain.UserEvent.class.getName());
            createCache(cm, aduro.basic.programservice.domain.UserReward.class.getName());
            createCache(cm, aduro.basic.programservice.domain.OrderTransaction.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ClientProgramRewardSetting.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramPhase.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ClientSetting.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramSubgroup.class.getName());
            createCache(cm, ActivityEventQueue.class.getName());
            createCache(cm, aduro.basic.programservice.domain.TermAndCondition.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ActivityEventQueue.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramLevelPath.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramLevelPractice.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ClientBrandSetting.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramBiometricData.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramHealthyRange.class.getName());
            createCache(cm, aduro.basic.programservice.domain.Program.class.getName() + ".programHealthyRanges");
            createCache(cm, aduro.basic.programservice.domain.WellmetricEventQueue.class.getName());
            createCache(cm, aduro.basic.programservice.domain.NotificationCenter.class.getName());
            createCache(cm, aduro.basic.programservice.domain.WellmetricEventQueue.class.getName() + ".userRewards");
            createCache(cm, aduro.basic.programservice.domain.ProgramSubCategoryConfiguration.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramSubCategoryConfiguration.class.getName() + ".programHealthyRanges");
            createCache(cm, aduro.basic.programservice.domain.UserEventQueue.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramRequirementItems.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramLevel.class.getName() + ".programRequirementItems");
            createCache(cm, aduro.basic.programservice.domain.EConsent.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramIntegrationTracking.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ClientSubDomain.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramUserLevelProgress.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCollection.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCollectionContent.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCollection.class.getName() + ".programCollectionContents");
            createCache(cm, aduro.basic.programservice.domain.ProgramTrackingStatus.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCohort.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCohortDataInput.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCohortRule.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCohortCollection.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramCohort.class.getName() + ".programCohortDataInputs");
            createCache(cm, aduro.basic.programservice.domain.ProgramCohort.class.getName() + ".programCohortCollections");
            createCache(cm, aduro.basic.programservice.domain.ProgramCohortDataInput.class.getName() + ".programCohortRules");
            createCache(cm, aduro.basic.programservice.domain.ProgramCohort.class.getName() + ".programCohortRules");
            createCache(cm, aduro.basic.programservice.domain.ProgramUserCohort.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramUserLogs.class.getName());
            createCache(cm, aduro.basic.programservice.domain.ProgramUserCollectionProgress.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, jcacheConfiguration);
    }
}
