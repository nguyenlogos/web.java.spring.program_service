package aduro.basic.programservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.math.BigDecimal;

/**
 * Properties specific to Programservice.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    public BigDecimal getTotalPercentEconomy() {
        return totalPercentEconomy;
    }

    public void setTotalPercentEconomy(BigDecimal totalPercentEconomy) {
        this.totalPercentEconomy = totalPercentEconomy;
    }

    private  BigDecimal  totalPercentEconomy;
    private Boolean processAddUserPointEnabled;

    public Boolean getProcessAddUserPointEnabled() {
        return processAddUserPointEnabled;
    }

    public void setProcessAddUserPointEnabled(Boolean processAddUserPointEnabled) {
        this.processAddUserPointEnabled = processAddUserPointEnabled;
    }
}
