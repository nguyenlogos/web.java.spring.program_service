package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.domain.ProgramLevelReward;
import aduro.basic.programservice.service.ProgramLevelRewardService;
import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import aduro.basic.programservice.service.dto.SubgroupRewardsDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramLevelRewardDTO;
import aduro.basic.programservice.service.dto.ProgramLevelRewardCriteria;
import aduro.basic.programservice.service.ProgramLevelRewardQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramLevelReward}.
 */
@RestController
@RequestMapping("/api")
public class ProgramLevelRewardResource {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelRewardResource.class);

    private static final String ENTITY_NAME = "programLevelReward";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramLevelRewardService programLevelRewardService;

    private final ProgramLevelRewardQueryService programLevelRewardQueryService;

    public ProgramLevelRewardResource(ProgramLevelRewardService programLevelRewardService, ProgramLevelRewardQueryService programLevelRewardQueryService) {
        this.programLevelRewardService = programLevelRewardService;
        this.programLevelRewardQueryService = programLevelRewardQueryService;
    }

    /**
     * {@code POST  /program-level-rewards} : Create a new programLevelReward.
     *
     * @param programLevelRewardDTO the programLevelRewardDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programLevelRewardDTO, or with status {@code 400 (Bad Request)} if the programLevelReward has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-level-rewards")
    public ResponseEntity<ProgramLevelRewardDTO> createProgramLevelReward(@Valid @RequestBody ProgramLevelRewardDTO programLevelRewardDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramLevelReward : {}", programLevelRewardDTO);
        if (programLevelRewardDTO.getId() != null) {
            throw new BadRequestAlertException("A new programLevelReward cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramLevelRewardDTO result = programLevelRewardService.save(programLevelRewardDTO);
        return ResponseEntity.created(new URI("/api/program-level-rewards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-level-rewards} : Updates an existing programLevelReward.
     *
     * @param programLevelRewardDTO the programLevelRewardDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programLevelRewardDTO,
     * or with status {@code 400 (Bad Request)} if the programLevelRewardDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programLevelRewardDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-level-rewards")
    public ResponseEntity<ProgramLevelRewardDTO> updateProgramLevelReward(@Valid @RequestBody ProgramLevelRewardDTO programLevelRewardDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramLevelReward : {}", programLevelRewardDTO);
        if (programLevelRewardDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramLevelRewardDTO result = programLevelRewardService.save(programLevelRewardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programLevelRewardDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-level-rewards} : get all the programLevelRewards.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programLevelRewards in body.
     */
    @GetMapping("/program-level-rewards")
    public ResponseEntity<List<ProgramLevelRewardDTO>> getAllProgramLevelRewards(ProgramLevelRewardCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramLevelRewards by criteria: {}", criteria);
        Page<ProgramLevelRewardDTO> page = programLevelRewardQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-level-rewards/count} : count all the programLevelRewards.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-level-rewards/count")
    public ResponseEntity<Long> countProgramLevelRewards(ProgramLevelRewardCriteria criteria) {
        log.debug("REST request to count ProgramLevelRewards by criteria: {}", criteria);
        return ResponseEntity.ok().body(programLevelRewardQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-level-rewards/:id} : get the "id" programLevelReward.
     *
     * @param id the id of the programLevelRewardDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programLevelRewardDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-level-rewards/{id}")
    public ResponseEntity<ProgramLevelRewardDTO> getProgramLevelReward(@PathVariable Long id) {
        log.debug("REST request to get ProgramLevelReward : {}", id);
        Optional<ProgramLevelRewardDTO> programLevelRewardDTO = programLevelRewardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programLevelRewardDTO);
    }

    /**
     * {@code DELETE  /program-level-rewards/:id} : delete the "id" programLevelReward.
     *
     * @param id the id of the programLevelRewardDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-level-rewards/{id}")
    public ResponseEntity<Void> deleteProgramLevelReward(@PathVariable Long id) {
        log.debug("REST request to delete ProgramLevelReward : {}", id);
        programLevelRewardService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


    @PostMapping("/program-level-rewards/{programLevelId}/update-level-rewards")
    public ResponseEntity<List<ProgramLevelRewardDTO>> updateLevelRewards(@PathVariable Long programLevelId, @RequestBody List<ProgramLevelRewardDTO> programLevelRewardDTOS) throws URISyntaxException {
        log.debug("REST request to save ProgramLevelRewards : {}", programLevelRewardDTOS);

        List<ProgramLevelRewardDTO> result = programLevelRewardService.updateLevelRewards(programLevelId, programLevelRewardDTOS);

        return ResponseEntity.created(new URI("/api/program-level-rewards/" + programLevelId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programLevelId.toString()))
            .body(result);
    }

    @PostMapping("/program-level-rewards/{programId}/update-rewards")
    public ResponseEntity<List<ProgramLevelRewardDTO>> updateRewardsForAllLevel(@PathVariable Long programId, @RequestBody SubgroupRewardsDTO subgroupRewardsDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramLevelReward for all level : {}", subgroupRewardsDTO);

        List<ProgramLevelRewardDTO> result = programLevelRewardService.updateRewardsForAllLevel(programId, subgroupRewardsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ""))
            .body(result);
    }
}
