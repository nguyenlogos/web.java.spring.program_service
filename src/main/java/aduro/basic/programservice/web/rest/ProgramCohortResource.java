package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.repository.ProgramCohortCollectionRepository;
import aduro.basic.programservice.service.ProgramCohortQueryService;
import aduro.basic.programservice.service.ProgramCohortService;
import aduro.basic.programservice.service.dto.ProgramCohortCriteria;
import aduro.basic.programservice.service.dto.ProgramCohortDTO;
import aduro.basic.programservice.service.dto.ProgramCohortWithCollectionDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortMapper;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCohort}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCohortResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortResource.class);

    private static final String ENTITY_NAME = "programCohort";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCohortService programCohortService;

    private final ProgramCohortQueryService programCohortQueryService;

    public ProgramCohortResource(ProgramCohortService programCohortService, ProgramCohortQueryService programCohortQueryService) {
        this.programCohortService = programCohortService;
        this.programCohortQueryService = programCohortQueryService;
    }

    /**
     * {@code POST  /program-cohorts} : Create a new programCohort.
     *
     * @param programCohortDTO the programCohortDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCohortDTO, or with status {@code 400 (Bad Request)} if the programCohort has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-cohorts")
    public ResponseEntity<ProgramCohortDTO> createProgramCohort(@Valid @RequestBody ProgramCohortDTO programCohortDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramCohort : {}", programCohortDTO);
        if (programCohortDTO.getId() != null) {
            throw new BadRequestAlertException("A new programCohort cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramCohortDTO result = programCohortService.save(programCohortDTO);
        return ResponseEntity.created(new URI("/api/program-cohorts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-cohorts} : Updates an existing programCohort.
     *
     * @param programCohortDTO the programCohortDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programCohortDTO,
     * or with status {@code 400 (Bad Request)} if the programCohortDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programCohortDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-cohorts")
    public ResponseEntity<ProgramCohortDTO> updateProgramCohort(@Valid @RequestBody ProgramCohortDTO programCohortDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramCohort : {}", programCohortDTO);
        if (programCohortDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramCohortDTO result = programCohortService.save(programCohortDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programCohortDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-cohorts} : get all the programCohorts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCohorts in body.
     */
    @GetMapping("/program-cohorts")
    public ResponseEntity<List<ProgramCohortDTO>> getAllProgramCohorts(ProgramCohortCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramCohorts by criteria: {}", criteria);
        Page<ProgramCohortDTO> page = programCohortQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /program-cohorts-with-collections} : get all the programCohorts including collections.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCohorts in body.
     */
    @GetMapping("/program-cohorts-with-collections")
    public ResponseEntity<List<ProgramCohortWithCollectionDTO>> getAllProgramCohortsWithCollections(ProgramCohortCriteria criteria) {
        log.debug("REST request to get ProgramCohorts by criteria: {}", criteria);
        List<ProgramCohortWithCollectionDTO> cohortDTOs = programCohortQueryService.findByCriteriaWithCollections(criteria);
        return ResponseEntity.ok(cohortDTOs);
    }

    /**
    * {@code GET  /program-cohorts/count} : count all the programCohorts.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-cohorts/count")
    public ResponseEntity<Long> countProgramCohorts(ProgramCohortCriteria criteria) {
        log.debug("REST request to count ProgramCohorts by criteria: {}", criteria);
        return ResponseEntity.ok().body(programCohortQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-cohorts/:id} : get the "id" programCohort.
     *
     * @param id the id of the programCohortDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCohortDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-cohorts/{id}")
    public ResponseEntity<ProgramCohortDTO> getProgramCohort(@PathVariable Long id) {
        log.debug("REST request to get ProgramCohort : {}", id);
        Optional<ProgramCohortDTO> programCohortDTO = programCohortService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCohortDTO);
    }

    /**
     * {@code DELETE  /program-cohorts/:id} : delete the "id" programCohort.
     *
     * @param id the id of the programCohortDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-cohorts/{id}")
    public ResponseEntity<Void> deleteProgramCohort(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCohort : {}", id);
        programCohortService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
