package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramSubgroupService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramSubgroupDTO;
import aduro.basic.programservice.service.dto.ProgramSubgroupCriteria;
import aduro.basic.programservice.service.ProgramSubgroupQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramSubgroup}.
 */
@RestController
@RequestMapping("/api")
public class ProgramSubgroupResource {

    private final Logger log = LoggerFactory.getLogger(ProgramSubgroupResource.class);

    private static final String ENTITY_NAME = "programSubgroup";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramSubgroupService programSubgroupService;

    private final ProgramSubgroupQueryService programSubgroupQueryService;

    public ProgramSubgroupResource(ProgramSubgroupService programSubgroupService, ProgramSubgroupQueryService programSubgroupQueryService) {
        this.programSubgroupService = programSubgroupService;
        this.programSubgroupQueryService = programSubgroupQueryService;
    }

    /**
     * {@code POST  /program-subgroups} : Create a new programSubgroup.
     *
     * @param programSubgroupDTO the programSubgroupDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programSubgroupDTO, or with status {@code 400 (Bad Request)} if the programSubgroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-subgroups")
    public ResponseEntity<ProgramSubgroupDTO> createProgramSubgroup(@Valid @RequestBody ProgramSubgroupDTO programSubgroupDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramSubgroup : {}", programSubgroupDTO);
        if (programSubgroupDTO.getId() != null) {
            throw new BadRequestAlertException("A new programSubgroup cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramSubgroupDTO result = programSubgroupService.save(programSubgroupDTO);
        return ResponseEntity.created(new URI("/api/program-subgroups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-subgroups} : Updates an existing programSubgroup.
     *
     * @param programSubgroupDTO the programSubgroupDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programSubgroupDTO,
     * or with status {@code 400 (Bad Request)} if the programSubgroupDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programSubgroupDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-subgroups")
    public ResponseEntity<ProgramSubgroupDTO> updateProgramSubgroup(@Valid @RequestBody ProgramSubgroupDTO programSubgroupDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramSubgroup : {}", programSubgroupDTO);
        if (programSubgroupDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramSubgroupDTO result = programSubgroupService.save(programSubgroupDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programSubgroupDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-subgroups} : get all the programSubgroups.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programSubgroups in body.
     */
    @GetMapping("/program-subgroups")
    public ResponseEntity<List<ProgramSubgroupDTO>> getAllProgramSubgroups(ProgramSubgroupCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramSubgroups by criteria: {}", criteria);
        Page<ProgramSubgroupDTO> page = programSubgroupQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-subgroups/count} : count all the programSubgroups.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-subgroups/count")
    public ResponseEntity<Long> countProgramSubgroups(ProgramSubgroupCriteria criteria) {
        log.debug("REST request to count ProgramSubgroups by criteria: {}", criteria);
        return ResponseEntity.ok().body(programSubgroupQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-subgroups/:id} : get the "id" programSubgroup.
     *
     * @param id the id of the programSubgroupDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programSubgroupDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-subgroups/{id}")
    public ResponseEntity<ProgramSubgroupDTO> getProgramSubgroup(@PathVariable Long id) {
        log.debug("REST request to get ProgramSubgroup : {}", id);
        Optional<ProgramSubgroupDTO> programSubgroupDTO = programSubgroupService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programSubgroupDTO);
    }

    /**
     * {@code DELETE  /program-subgroups/:id} : delete the "id" programSubgroup.
     *
     * @param id the id of the programSubgroupDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-subgroups/{id}")
    public ResponseEntity<Void> deleteProgramSubgroup(@PathVariable Long id) {
        log.debug("REST request to delete ProgramSubgroup : {}", id);
        programSubgroupService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
