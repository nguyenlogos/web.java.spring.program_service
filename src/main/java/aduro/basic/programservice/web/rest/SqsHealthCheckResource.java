package aduro.basic.programservice.web.rest;


import aduro.basic.programservice.service.dto.EmployeeDto;
import aduro.basic.programservice.sqs.ProgramProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

/**
 * REST controller for managing the current user's account.
 */
@ApiIgnore
@RestController
@RequestMapping("/api")
public class SqsHealthCheckResource {

    @Autowired
    private ProgramProducer programProducer;

    @Value("${aws.queues.switching-subgroup}")
    private String mainQueueSubgroupChange;

    @PostMapping("/sqs/subgroupChange")
    public ResponseEntity<EmployeeDto> sendSubgroupChangeMessage(@Valid @RequestBody EmployeeDto dto)  {

        programProducer.send(mainQueueSubgroupChange, dto);
        return ResponseEntity.ok(dto);
    }
}
