package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.UserEventQueueService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.UserEventQueueDTO;
import aduro.basic.programservice.service.dto.UserEventQueueCriteria;
import aduro.basic.programservice.service.UserEventQueueQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.UserEventQueue}.
 */
@RestController
@RequestMapping("/api")
public class UserEventQueueResource {

    private final Logger log = LoggerFactory.getLogger(UserEventQueueResource.class);

    private static final String ENTITY_NAME = "userEventQueue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserEventQueueService userEventQueueService;

    private final UserEventQueueQueryService userEventQueueQueryService;

    public UserEventQueueResource(UserEventQueueService userEventQueueService, UserEventQueueQueryService userEventQueueQueryService) {
        this.userEventQueueService = userEventQueueService;
        this.userEventQueueQueryService = userEventQueueQueryService;
    }

    /**
     * {@code POST  /user-event-queues} : Create a new userEventQueue.
     *
     * @param userEventQueueDTO the userEventQueueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userEventQueueDTO, or with status {@code 400 (Bad Request)} if the userEventQueue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-event-queues")
    public ResponseEntity<UserEventQueueDTO> createUserEventQueue(@Valid @RequestBody UserEventQueueDTO userEventQueueDTO) throws URISyntaxException {
        log.debug("REST request to save UserEventQueue : {}", userEventQueueDTO);
        if (userEventQueueDTO.getId() != null) {
            throw new BadRequestAlertException("A new userEventQueue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserEventQueueDTO result = userEventQueueService.save(userEventQueueDTO);
        return ResponseEntity.created(new URI("/api/user-event-queues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-event-queues} : Updates an existing userEventQueue.
     *
     * @param userEventQueueDTO the userEventQueueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userEventQueueDTO,
     * or with status {@code 400 (Bad Request)} if the userEventQueueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userEventQueueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-event-queues")
    public ResponseEntity<UserEventQueueDTO> updateUserEventQueue(@Valid @RequestBody UserEventQueueDTO userEventQueueDTO) throws URISyntaxException {
        log.debug("REST request to update UserEventQueue : {}", userEventQueueDTO);
        if (userEventQueueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserEventQueueDTO result = userEventQueueService.save(userEventQueueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userEventQueueDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-event-queues} : get all the userEventQueues.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userEventQueues in body.
     */
    @GetMapping("/user-event-queues")
    public ResponseEntity<List<UserEventQueueDTO>> getAllUserEventQueues(UserEventQueueCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get UserEventQueues by criteria: {}", criteria);
        Page<UserEventQueueDTO> page = userEventQueueQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /user-event-queues/count} : count all the userEventQueues.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/user-event-queues/count")
    public ResponseEntity<Long> countUserEventQueues(UserEventQueueCriteria criteria) {
        log.debug("REST request to count UserEventQueues by criteria: {}", criteria);
        return ResponseEntity.ok().body(userEventQueueQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-event-queues/:id} : get the "id" userEventQueue.
     *
     * @param id the id of the userEventQueueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userEventQueueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-event-queues/{id}")
    public ResponseEntity<UserEventQueueDTO> getUserEventQueue(@PathVariable Long id) {
        log.debug("REST request to get UserEventQueue : {}", id);
        Optional<UserEventQueueDTO> userEventQueueDTO = userEventQueueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userEventQueueDTO);
    }

    /**
     * {@code DELETE  /user-event-queues/:id} : delete the "id" userEventQueue.
     *
     * @param id the id of the userEventQueueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-event-queues/{id}")
    public ResponseEntity<Void> deleteUserEventQueue(@PathVariable Long id) {
        log.debug("REST request to delete UserEventQueue : {}", id);
        userEventQueueService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
