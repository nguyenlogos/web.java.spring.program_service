package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ClientProgramRewardSettingService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ClientProgramRewardSettingDTO;
import aduro.basic.programservice.service.dto.ClientProgramRewardSettingCriteria;
import aduro.basic.programservice.service.ClientProgramRewardSettingQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ClientProgramRewardSetting}.
 */
@RestController
@RequestMapping("/api")
public class ClientProgramRewardSettingResource {

    private final Logger log = LoggerFactory.getLogger(ClientProgramRewardSettingResource.class);

    private static final String ENTITY_NAME = "clientProgramRewardSetting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClientProgramRewardSettingService clientProgramRewardSettingService;

    private final ClientProgramRewardSettingQueryService clientProgramRewardSettingQueryService;

    public ClientProgramRewardSettingResource(ClientProgramRewardSettingService clientProgramRewardSettingService, ClientProgramRewardSettingQueryService clientProgramRewardSettingQueryService) {
        this.clientProgramRewardSettingService = clientProgramRewardSettingService;
        this.clientProgramRewardSettingQueryService = clientProgramRewardSettingQueryService;
    }

    /**
     * {@code POST  /client-program-reward-settings} : Create a new clientProgramRewardSetting.
     *
     * @param clientProgramRewardSettingDTO the clientProgramRewardSettingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientProgramRewardSettingDTO, or with status {@code 400 (Bad Request)} if the clientProgramRewardSetting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-program-reward-settings")
    public ResponseEntity<ClientProgramRewardSettingDTO> createClientProgramRewardSetting(@Valid @RequestBody ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws URISyntaxException {
        log.debug("REST request to save ClientProgramRewardSetting : {}", clientProgramRewardSettingDTO);
        if (clientProgramRewardSettingDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientProgramRewardSetting cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientProgramRewardSettingDTO result = clientProgramRewardSettingService.save(clientProgramRewardSettingDTO);
        return ResponseEntity.created(new URI("/api/client-program-reward-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /client-program-reward-settings} : Updates an existing clientProgramRewardSetting.
     *
     * @param clientProgramRewardSettingDTO the clientProgramRewardSettingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientProgramRewardSettingDTO,
     * or with status {@code 400 (Bad Request)} if the clientProgramRewardSettingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clientProgramRewardSettingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/client-program-reward-settings")
    public ResponseEntity<ClientProgramRewardSettingDTO> updateClientProgramRewardSetting(@Valid @RequestBody ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws URISyntaxException {
        log.debug("REST request to update ClientProgramRewardSetting : {}", clientProgramRewardSettingDTO);
        if (clientProgramRewardSettingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClientProgramRewardSettingDTO result = clientProgramRewardSettingService.save(clientProgramRewardSettingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clientProgramRewardSettingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /client-program-reward-settings} : get all the clientProgramRewardSettings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clientProgramRewardSettings in body.
     */
    @GetMapping("/client-program-reward-settings")
    public ResponseEntity<List<ClientProgramRewardSettingDTO>> getAllClientProgramRewardSettings(ClientProgramRewardSettingCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ClientProgramRewardSettings by criteria: {}", criteria);
        Page<ClientProgramRewardSettingDTO> page = clientProgramRewardSettingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /client-program-reward-settings/count} : count all the clientProgramRewardSettings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/client-program-reward-settings/count")
    public ResponseEntity<Long> countClientProgramRewardSettings(ClientProgramRewardSettingCriteria criteria) {
        log.debug("REST request to count ClientProgramRewardSettings by criteria: {}", criteria);
        return ResponseEntity.ok().body(clientProgramRewardSettingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /client-program-reward-settings/:id} : get the "id" clientProgramRewardSetting.
     *
     * @param id the id of the clientProgramRewardSettingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientProgramRewardSettingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-program-reward-settings/{id}")
    public ResponseEntity<ClientProgramRewardSettingDTO> getClientProgramRewardSetting(@PathVariable Long id) throws Exception {
        log.debug("REST request to get ClientProgramRewardSetting : {}", id);
        Optional<ClientProgramRewardSettingDTO> clientProgramRewardSettingDTO = clientProgramRewardSettingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clientProgramRewardSettingDTO);
    }

    /**
     * {@code DELETE  /client-program-reward-settings/:id} : delete the "id" clientProgramRewardSetting.
     *
     * @param id the id of the clientProgramRewardSettingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/client-program-reward-settings/{id}")
    public ResponseEntity<Void> deleteClientProgramRewardSetting(@PathVariable Long id) {
        log.debug("REST request to delete ClientProgramRewardSetting : {}", id);
        clientProgramRewardSettingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


    /**
     * {@code POST  /client-program-reward-settings} : Create a new clientProgramRewardSetting.
     *
     * @param clientProgramRewardSettingDTO the clientProgramRewardSettingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientProgramRewardSettingDTO, or with status {@code 400 (Bad Request)} if the clientProgramRewardSetting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-program-reward-settings/generate-access-token")
    public ResponseEntity<ClientProgramRewardSettingDTO> generateAccessToken(@Valid @RequestBody ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception {
        log.debug("REST request to generateAccessToken : {}", clientProgramRewardSettingDTO);
        if (clientProgramRewardSettingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClientProgramRewardSettingDTO result = clientProgramRewardSettingService.generateAccessToken(clientProgramRewardSettingDTO);
        return ResponseEntity.created(new URI("/api/client-program-reward-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /client-program-reward-settings} : Create a new clientProgramRewardSetting.
     *
     * @param clientProgramRewardSettingDTO the clientProgramRewardSettingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientProgramRewardSettingDTO, or with status {@code 400 (Bad Request)} if the clientProgramRewardSetting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-program-reward-settings/create-tremendous-team")
    public ResponseEntity<ClientProgramRewardSettingDTO> createTremendousTeam(@RequestBody ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception {
        log.debug("REST request to createTremendousTeam : {}", clientProgramRewardSettingDTO);
        ClientProgramRewardSettingDTO result = clientProgramRewardSettingService.createTremendousTeam(clientProgramRewardSettingDTO);
        return ResponseEntity.created(new URI("/api/client-program-reward-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /*@GetMapping("/client-program-reward-settings/get-team-detail/{id}")
    public ResponseEntity<ClientProgramRewardSettingDTO> getTeamDetail(@PathVariable Long id) throws Exception {
        log.debug("REST request to get ClientProgramRewardSetting : {}", id);
        clientProgramRewardSettingService.getTremendousTeam(new ClientProgramRewardSettingDTO());
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }*/


    /**
     * {@code POST  /client-program-reward-settings} : Create a new clientProgramRewardSetting.
     *
     * @param clientProgramRewardSettingDTO the clientProgramRewardSettingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientProgramRewardSettingDTO, or with status {@code 400 (Bad Request)} if the clientProgramRewardSetting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-program-reward-settings/create-tango-account")
    public ResponseEntity<ClientProgramRewardSettingDTO> createTangoAccount(@RequestBody ClientProgramRewardSettingDTO clientProgramRewardSettingDTO) throws Exception {
        log.debug("REST request to tango account : {}", clientProgramRewardSettingDTO);
        ClientProgramRewardSettingDTO result = clientProgramRewardSettingService.createTangoAccount(clientProgramRewardSettingDTO);
        return ResponseEntity.created(new URI("/api/client-program-reward-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }







}
