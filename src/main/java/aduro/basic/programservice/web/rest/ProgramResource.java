package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.domain.enumeration.ActivityType;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.service.ProgramQueryService;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.ProgramCriteria;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.service.dto.ProgramStatus;
import aduro.basic.programservice.service.dto.UpdatedSubgroupClientPayload;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.Program}.
 */
@RestController
@RequestMapping("/api")
public class ProgramResource {

    private final Logger log = LoggerFactory.getLogger(ProgramResource.class);
    private final String PROGRAM_ID = "programId";
    private final String ACTIVITY_TYPE = "type";

    private static final String ENTITY_NAME = "program";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramService programService;

    private final ProgramQueryService programQueryService;

    public ProgramResource(ProgramService programService, ProgramQueryService programQueryService) {
        this.programService = programService;
        this.programQueryService = programQueryService;
    }

    /**
     * {@code POST  /programs} : Create a new program.
     *
     * @param programDTO the programDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programDTO, or with status {@code 400 (Bad Request)} if the program has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/programs")
    public ResponseEntity<ProgramDTO> createProgram(@Valid @RequestBody ProgramDTO programDTO) throws URISyntaxException {
        log.debug("REST request to save Program : {}", programDTO);
        if (programDTO.getId() != null &&  programDTO.getId() != 0) {
            throw new BadRequestAlertException("A new program cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramDTO result = programService.save(programDTO);
        return ResponseEntity.created(new URI("/api/programs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /programs} : Updates an existing program.
     *
     * @param programDTO the programDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programDTO,
     * or with status {@code 400 (Bad Request)} if the programDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/programs")
    public ResponseEntity<ProgramDTO> updateProgram(@Valid @RequestBody ProgramDTO programDTO) throws URISyntaxException {
        log.debug("REST request to update Program : {}", programDTO);
        if (programDTO.getId() == null || programDTO.getId() == 0) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        ProgramDTO result = programService.save(programDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /programs} : get all the programs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programs in body.
     */
    @GetMapping("/programs")
    public ResponseEntity<List<ProgramDTO>> getAllPrograms(ProgramCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Programs by criteria: {}", criteria);

        BooleanFilter booleanFilter = new BooleanFilter();
        booleanFilter.setEquals(false);
        criteria.setIsTemplate(booleanFilter);

        if(criteria.getStatus() != null){

            boolean isFilterQaVerify = false;
            if((
                (criteria.getStatus().getEquals() != null && criteria.getStatus().getEquals().equals(ProgramStatus.QA_Verify.toString()))
                    || (criteria.getStatus().getContains() != null && criteria.getStatus().getContains().equals(ProgramStatus.QA_Verify.toString())))){
                isFilterQaVerify = true;
            }
            if(criteria.getStatus().getEquals() != null && isFilterQaVerify) {
                criteria.getStatus().setEquals(ProgramStatus.Active.name());
            }

            if(criteria.getStatus().getContains() != null && isFilterQaVerify) {
                criteria.getStatus().setContains(ProgramStatus.Active.name());
            }
            BooleanFilter booleanFilterQA = new BooleanFilter();
            booleanFilterQA.setEquals(isFilterQaVerify);
            criteria.setQaVerify(booleanFilterQA);
        }

        Page<ProgramDTO> page = programQueryService.findByCriteria(criteria, pageable);

        // Set status to `QA Verify` if programs are used for QA Verify
        for (ProgramDTO dto: page.getContent()) {
            dto.setStatusBeforeSendingIfProgramIsQAVerify();
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /programs/count} : count all the programs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/programs/count")
    public ResponseEntity<Long> countPrograms(ProgramCriteria criteria) {
        log.debug("REST request to count Programs by criteria: {}", criteria);
        return ResponseEntity.ok().body(programQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /programs/:id} : get the "id" program.
     *
     * @param id the id of the programDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/programs/{id}")
    public ResponseEntity<ProgramDTO> getProgram(@PathVariable Long id, @RequestParam(value = "enableDemoGraphic", required = false, defaultValue = "false") boolean enableDemoGraphic, @RequestParam(value = "showProgramActivity", defaultValue = "true", required = false) boolean showProgramActivity) {
        log.debug("REST request to get Program : {}", id);
        ProgramDTO programDTO;
        programDTO = programService.getProgramDetail(id, enableDemoGraphic, showProgramActivity);
        Optional<ProgramDTO> rs = Optional.of(programDTO);
        return ResponseUtil.wrapOrNotFound(rs);
    }

    /**
     * {@code DELETE  /programs/:id} : delete the "id" program.
     *
     * @param id the id of the programDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/programs/{id}")
    public ResponseEntity<Void> deleteProgram(@PathVariable Long id) {
        log.debug("REST request to delete Program : {}", id);
        programService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

  /*  @PostMapping("/uploadProgramLogo")
    public ResponseEntity uploadProgramLogo(@RequestParam("programId") long programId, @RequestParam(value = "file", required = false) MultipartFile file) throws URISyntaxException {

        log.debug("REST request to update logo of Program : {}", programId);
        programService.uploadProgramLogo(programId, file);

        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, "")).build();
    }*/


   /* @PostMapping("/uploadProgramLogoUrl")
    public ResponseEntity uploadProgramLogo(@RequestBody ProgramDTO programDTO){
        log.debug("REST request to update logo of Program : {}", programDTO.getId());
        programService.uploadProgramLogo(programDTO);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, "")).build();
    }*/

    /**
     * {@code GET  /programs/:id} : get the "id" program.
     *
     * @param id the id of the programDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/programs/{id}/detail")
    public ResponseEntity<ProgramDTO> getProgramDetail(@PathVariable Long id, @RequestParam(value = "showProgramActivity", defaultValue = "true", required = false) boolean showProgramActivity) {
        log.debug("REST request to get Program : {}", id);
        ProgramDTO programDTO = programService.getProgramDetail(id, showProgramActivity);
        Optional<ProgramDTO> rs = Optional.of(programDTO);
        return ResponseUtil.wrapOrNotFound(rs);
    }



    /**
     * {@code GET  /programs/:id} : get employer verified activity by "id" program.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the list of activity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/programs/custom-activities")
    public ResponseEntity<List<CustomActivityDTO>> getCustomActivitiesByTypeAndProgramId(@RequestParam Map<String,String> allParams) throws Exception {
        log.debug("REST add list activity to program");

        Long programId = Long.valueOf(0);
        String type = "";

        if (allParams.containsKey(PROGRAM_ID)) {
            programId = Long.parseLong(allParams.get(PROGRAM_ID));
        }

        if (allParams.containsKey(ACTIVITY_TYPE)) {
            type = String.valueOf(allParams.get(ACTIVITY_TYPE));
        }

        if ( programId == 0) {
            throw new BadRequestAlertException("ProgramId can not be null or 0", "CustomActivityDTO", Instant.now().toString());
        }

        if (type == null || type.equals("")) {
            throw new BadRequestAlertException("Activity Type can not be empty.", "CustomActivityDTO", Instant.now().toString());
        }
        ActivityType activityType = CommonUtils.getIfPresent(type);

        if (activityType == null) {
            throw new BadRequestAlertException("Activity Type can not be empty.", "CustomActivityDTO", Instant.now().toString());
        }

        List<CustomActivityDTO> rs = programService.getCustomActivitiesByProgramId(activityType, programId);
        return ResponseEntity.ok(rs);
    }

    @PostMapping("/programs/update-by-subgroup")
    public  ResponseEntity<UpdatedSubgroupClientPayload> updateProgramByNewSubgroupId(@RequestBody UpdatedSubgroupClientPayload updatedSubgroupClientPayload) {

        UpdatedSubgroupClientPayload result = programService.updateProgramByNewSubgroupId(updatedSubgroupClientPayload);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getClientId().toString()))
            .body(result);
    }

    @GetMapping("/programs/check-program-status")
    public ResponseEntity<String> checkProgramStatus() throws Exception {
        log.debug("REST check program status manually.");
        return ResponseEntity.ok(programService.checkProgramStatusManually());
    }
}
