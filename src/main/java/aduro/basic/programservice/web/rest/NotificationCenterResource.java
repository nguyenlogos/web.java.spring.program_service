package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.NotificationCenterService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.NotificationCenterDTO;
import aduro.basic.programservice.service.dto.NotificationCenterCriteria;
import aduro.basic.programservice.service.NotificationCenterQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.NotificationCenter}.
 */
@RestController
@RequestMapping("/api")
public class NotificationCenterResource {

    private final Logger log = LoggerFactory.getLogger(NotificationCenterResource.class);

    private static final String ENTITY_NAME = "notificationCenter";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NotificationCenterService notificationCenterService;

    private final NotificationCenterQueryService notificationCenterQueryService;

    public NotificationCenterResource(NotificationCenterService notificationCenterService, NotificationCenterQueryService notificationCenterQueryService) {
        this.notificationCenterService = notificationCenterService;
        this.notificationCenterQueryService = notificationCenterQueryService;
    }

    /**
     * {@code POST  /notification-centers} : Create a new notificationCenter.
     *
     * @param notificationCenterDTO the notificationCenterDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notificationCenterDTO, or with status {@code 400 (Bad Request)} if the notificationCenter has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/notification-centers")
    public ResponseEntity<NotificationCenterDTO> createNotificationCenter(@Valid @RequestBody NotificationCenterDTO notificationCenterDTO) throws URISyntaxException {
        log.debug("REST request to save NotificationCenter : {}", notificationCenterDTO);
        if (notificationCenterDTO.getId() != null) {
            throw new BadRequestAlertException("A new notificationCenter cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NotificationCenterDTO result = notificationCenterService.save(notificationCenterDTO);
        return ResponseEntity.created(new URI("/api/notification-centers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notification-centers} : Updates an existing notificationCenter.
     *
     * @param notificationCenterDTO the notificationCenterDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notificationCenterDTO,
     * or with status {@code 400 (Bad Request)} if the notificationCenterDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the notificationCenterDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/notification-centers")
    public ResponseEntity<NotificationCenterDTO> updateNotificationCenter(@Valid @RequestBody NotificationCenterDTO notificationCenterDTO) throws URISyntaxException {
        log.debug("REST request to update NotificationCenter : {}", notificationCenterDTO);
        if (notificationCenterDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NotificationCenterDTO result = notificationCenterService.save(notificationCenterDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, notificationCenterDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /notification-centers} : get all the notificationCenters.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of notificationCenters in body.
     */
    @GetMapping("/notification-centers")
    public ResponseEntity<List<NotificationCenterDTO>> getAllNotificationCenters(NotificationCenterCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get NotificationCenters by criteria: {}", criteria);
        Page<NotificationCenterDTO> page = notificationCenterQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /notification-centers/count} : count all the notificationCenters.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/notification-centers/count")
    public ResponseEntity<Long> countNotificationCenters(NotificationCenterCriteria criteria) {
        log.debug("REST request to count NotificationCenters by criteria: {}", criteria);
        return ResponseEntity.ok().body(notificationCenterQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /notification-centers/:id} : get the "id" notificationCenter.
     *
     * @param id the id of the notificationCenterDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notificationCenterDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/notification-centers/{id}")
    public ResponseEntity<NotificationCenterDTO> getNotificationCenter(@PathVariable Long id) {
        log.debug("REST request to get NotificationCenter : {}", id);
        Optional<NotificationCenterDTO> notificationCenterDTO = notificationCenterService.findOne(id);
        return ResponseUtil.wrapOrNotFound(notificationCenterDTO);
    }

    /**
     * {@code DELETE  /notification-centers/:id} : delete the "id" notificationCenter.
     *
     * @param id the id of the notificationCenterDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/notification-centers/{id}")
    public ResponseEntity<Void> deleteNotificationCenter(@PathVariable Long id) {
        log.debug("REST request to delete NotificationCenter : {}", id);
        notificationCenterService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
