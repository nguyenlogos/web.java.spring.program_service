package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramHealthyRangeService;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeCriteria;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.dto.ProgramHealthyRangePayLoad;
import aduro.basic.programservice.service.extensions.ProgramHealthyRangeQueryServiceExtension;
import aduro.basic.programservice.service.extensions.ProgramHealthyRangeServiceExtension;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProgramHealthyRangeResourceExtension {


    private final Logger log = LoggerFactory.getLogger(ProgramHealthyRangeResource.class);

    private static final String ENTITY_NAME = "programHealthyRange";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    private ProgramHealthyRangeServiceExtension programHealthyRangeServiceExtension;

    @Autowired
    private ProgramHealthyRangeQueryServiceExtension programHealthyRangeQueryServiceExtension;

    @PostMapping("/extension/program-healthy-ranges/save-list")
    public ResponseEntity<List<ProgramHealthyRangePayLoad>> saveProgramHealthyRanges(@Valid @RequestBody List<ProgramHealthyRangePayLoad> programHealthyRangeDTOs) throws URISyntaxException {
        log.debug("REST request to save ProgramHealthyRange : {}", programHealthyRangeDTOs.toArray());
        if (programHealthyRangeDTOs.isEmpty()) {
            throw new BadRequestAlertException("List programHealthyRange cannot empty", ENTITY_NAME, "NULL");
        }
        List<ProgramHealthyRangePayLoad> res = programHealthyRangeServiceExtension.save(programHealthyRangeDTOs);

        return ResponseEntity.ok(res);
    }

    @PutMapping("/extension/program-healthy-ranges")
    public ResponseEntity<ProgramHealthyRangePayLoad> updateProgramHealthyRange(@Valid @RequestBody ProgramHealthyRangePayLoad programHealthyRangeDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramHealthyRange : {}", programHealthyRangeDTO);
        if (programHealthyRangeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramHealthyRangePayLoad result = programHealthyRangeServiceExtension.save(programHealthyRangeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programHealthyRangeDTO.getId().toString()))
            .body(result);
    }

    @GetMapping("/extension/program-healthy-ranges")
    public ResponseEntity<List<ProgramHealthyRangePayLoad>> getAllProgramHealthyRanges(ProgramHealthyRangeCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramHealthyRanges by criteria: {}", criteria);
        Page<ProgramHealthyRangePayLoad> page = programHealthyRangeQueryServiceExtension.getByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
