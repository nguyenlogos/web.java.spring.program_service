package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.dto.EConsentDTO;
import aduro.basic.programservice.service.extensions.EConsentServiceExtension;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class EConsentResourceExtension {

    private final Logger log = LoggerFactory.getLogger(EConsentResourceExtension.class);

    private static final String ENTITY_NAME = "eConsent";

    private final EConsentServiceExtension eConsentServiceExtension;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public EConsentResourceExtension(EConsentServiceExtension eConsentServiceExtension) {
        this.eConsentServiceExtension = eConsentServiceExtension;
    }


    @PostMapping("/e-consents/create/{clientId}/{subgroupId}")
    public ResponseEntity<EConsentDTO> createEConsent(@Valid @RequestBody EConsentDTO eConsentDTO, @PathVariable String clientId, @PathVariable String subgroupId) throws URISyntaxException {
        log.debug("REST request to save EConsent : {}", eConsentDTO);
        if (eConsentDTO.getId() != null) {
            throw new BadRequestAlertException("A new eConsent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EConsentDTO result = eConsentServiceExtension.saveAdvance(eConsentDTO, clientId, subgroupId);
        return ResponseEntity.created(new URI("/api/e-consents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

}
