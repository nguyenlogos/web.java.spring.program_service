package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.adp.service.AmpUserActivityProgressService;
import aduro.basic.programservice.domain.ActivityEventQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST controller for managing {@link ActivityEventQueue}.
 */
@RestController
@RequestMapping("/api")
public class AmpUserActivityProgressResource {

    private final Logger log = LoggerFactory.getLogger(AmpUserActivityProgressResource.class);

    private static final String ENTITY_NAME = "AmpUserActivityProgress";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AmpUserActivityProgressService ampUserActivityProgressService;


    public AmpUserActivityProgressResource(AmpUserActivityProgressService ampUserActivityProgressService) {
        this.ampUserActivityProgressService = ampUserActivityProgressService;
    }


    @GetMapping("/amp-user-activity-progress/count")
    public ResponseEntity<Long> countOrderTransactions( ) {
        log.debug("REST request to count OrderTransactions by criteria: {}");
        return ResponseEntity.ok().body(ampUserActivityProgressService.countByCriteria());
    }










}
