package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramLevelPracticeService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramLevelPracticeDTO;
import aduro.basic.programservice.service.dto.ProgramLevelPracticeCriteria;
import aduro.basic.programservice.service.ProgramLevelPracticeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramLevelPractice}.
 */
@RestController
@RequestMapping("/api")
public class ProgramLevelPracticeResource {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelPracticeResource.class);

    private static final String ENTITY_NAME = "programLevelPractice";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramLevelPracticeService programLevelPracticeService;

    private final ProgramLevelPracticeQueryService programLevelPracticeQueryService;

    public ProgramLevelPracticeResource(ProgramLevelPracticeService programLevelPracticeService, ProgramLevelPracticeQueryService programLevelPracticeQueryService) {
        this.programLevelPracticeService = programLevelPracticeService;
        this.programLevelPracticeQueryService = programLevelPracticeQueryService;
    }

    /**
     * {@code POST  /program-level-practices} : Create a new programLevelPractice.
     *
     * @param programLevelPracticeDTO the programLevelPracticeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programLevelPracticeDTO, or with status {@code 400 (Bad Request)} if the programLevelPractice has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-level-practices")
    public ResponseEntity<ProgramLevelPracticeDTO> createProgramLevelPractice(@Valid @RequestBody ProgramLevelPracticeDTO programLevelPracticeDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramLevelPractice : {}", programLevelPracticeDTO);
        if (programLevelPracticeDTO.getId() != null) {
            throw new BadRequestAlertException("A new programLevelPractice cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramLevelPracticeDTO result = programLevelPracticeService.save(programLevelPracticeDTO);
        return ResponseEntity.created(new URI("/api/program-level-practices/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-level-practices} : Updates an existing programLevelPractice.
     *
     * @param programLevelPracticeDTO the programLevelPracticeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programLevelPracticeDTO,
     * or with status {@code 400 (Bad Request)} if the programLevelPracticeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programLevelPracticeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-level-practices")
    public ResponseEntity<ProgramLevelPracticeDTO> updateProgramLevelPractice(@Valid @RequestBody ProgramLevelPracticeDTO programLevelPracticeDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramLevelPractice : {}", programLevelPracticeDTO);
        if (programLevelPracticeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramLevelPracticeDTO result = programLevelPracticeService.save(programLevelPracticeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programLevelPracticeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-level-practices} : get all the programLevelPractices.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programLevelPractices in body.
     */
    @GetMapping("/program-level-practices")
    public ResponseEntity<List<ProgramLevelPracticeDTO>> getAllProgramLevelPractices(ProgramLevelPracticeCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramLevelPractices by criteria: {}", criteria);
        Page<ProgramLevelPracticeDTO> page = programLevelPracticeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-level-practices/count} : count all the programLevelPractices.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-level-practices/count")
    public ResponseEntity<Long> countProgramLevelPractices(ProgramLevelPracticeCriteria criteria) {
        log.debug("REST request to count ProgramLevelPractices by criteria: {}", criteria);
        return ResponseEntity.ok().body(programLevelPracticeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-level-practices/:id} : get the "id" programLevelPractice.
     *
     * @param id the id of the programLevelPracticeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programLevelPracticeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-level-practices/{id}")
    public ResponseEntity<ProgramLevelPracticeDTO> getProgramLevelPractice(@PathVariable Long id) {
        log.debug("REST request to get ProgramLevelPractice : {}", id);
        Optional<ProgramLevelPracticeDTO> programLevelPracticeDTO = programLevelPracticeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programLevelPracticeDTO);
    }

    /**
     * {@code DELETE  /program-level-practices/:id} : delete the "id" programLevelPractice.
     *
     * @param id the id of the programLevelPracticeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-level-practices/{id}")
    public ResponseEntity<Void> deleteProgramLevelPractice(@PathVariable Long id) {
        log.debug("REST request to delete ProgramLevelPractice : {}", id);
        programLevelPracticeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
