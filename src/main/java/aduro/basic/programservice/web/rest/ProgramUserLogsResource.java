package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.domain.ProgramUserLogs;
import aduro.basic.programservice.repository.ProgramUserLogsRepository;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramUserLogs}.
 */
@RestController
@RequestMapping("/api")
public class ProgramUserLogsResource {

    private final Logger log = LoggerFactory.getLogger(ProgramUserLogsResource.class);

    private static final String ENTITY_NAME = "programUserLogs";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramUserLogsRepository programUserLogsRepository;

    public ProgramUserLogsResource(ProgramUserLogsRepository programUserLogsRepository) {
        this.programUserLogsRepository = programUserLogsRepository;
    }

    /**
     * {@code POST  /program-user-logs} : Create a new programUserLogs.
     *
     * @param programUserLogs the programUserLogs to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programUserLogs, or with status {@code 400 (Bad Request)} if the programUserLogs has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-user-logs")
    public ResponseEntity<ProgramUserLogs> createProgramUserLogs(@Valid @RequestBody ProgramUserLogs programUserLogs) throws URISyntaxException {
        log.debug("REST request to save ProgramUserLogs : {}", programUserLogs);
        if (programUserLogs.getId() != null) {
            throw new BadRequestAlertException("A new programUserLogs cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramUserLogs result = programUserLogsRepository.save(programUserLogs);
        return ResponseEntity.created(new URI("/api/program-user-logs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-user-logs} : Updates an existing programUserLogs.
     *
     * @param programUserLogs the programUserLogs to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programUserLogs,
     * or with status {@code 400 (Bad Request)} if the programUserLogs is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programUserLogs couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-user-logs")
    public ResponseEntity<ProgramUserLogs> updateProgramUserLogs(@Valid @RequestBody ProgramUserLogs programUserLogs) throws URISyntaxException {
        log.debug("REST request to update ProgramUserLogs : {}", programUserLogs);
        if (programUserLogs.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramUserLogs result = programUserLogsRepository.save(programUserLogs);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programUserLogs.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-user-logs} : get all the programUserLogs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programUserLogs in body.
     */
    @GetMapping("/program-user-logs")
    public ResponseEntity<List<ProgramUserLogs>> getAllProgramUserLogs(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of ProgramUserLogs");
        Page<ProgramUserLogs> page = programUserLogsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /program-user-logs/:id} : get the "id" programUserLogs.
     *
     * @param id the id of the programUserLogs to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programUserLogs, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-user-logs/{id}")
    public ResponseEntity<ProgramUserLogs> getProgramUserLogs(@PathVariable Long id) {
        log.debug("REST request to get ProgramUserLogs : {}", id);
        Optional<ProgramUserLogs> programUserLogs = programUserLogsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(programUserLogs);
    }

    /**
     * {@code DELETE  /program-user-logs/:id} : delete the "id" programUserLogs.
     *
     * @param id the id of the programUserLogs to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-user-logs/{id}")
    public ResponseEntity<Void> deleteProgramUserLogs(@PathVariable Long id) {
        log.debug("REST request to delete ProgramUserLogs : {}", id);
        programUserLogsRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
