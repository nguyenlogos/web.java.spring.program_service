package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.service.ProgramActivityQueryService;
import aduro.basic.programservice.service.ProgramActivityService;
import aduro.basic.programservice.service.dto.ProgramActivityCriteria;
import aduro.basic.programservice.service.dto.ProgramActivityDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramActivity}.
 */
@RestController
@RequestMapping("/api")
public class ProgramActivityResource {

    private final Logger log = LoggerFactory.getLogger(ProgramActivityResource.class);

    private static final String ENTITY_NAME = "programActivity";
    private static final String ERROR_INVALID_DELETE_ACTIVITY = "Activity cannot be removed because there are users currently enrolled this activity.";
    private static final String ERROR_INVALID_DELETE_ACTIVITY_KEY = "enrolledCustomActivity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramActivityService programActivityService;

    private final ProgramActivityQueryService programActivityQueryService;

    public ProgramActivityResource(ProgramActivityService programActivityService, ProgramActivityQueryService programActivityQueryService) {
        this.programActivityService = programActivityService;
        this.programActivityQueryService = programActivityQueryService;
    }

    /**
     * {@code POST  /program-activities} : Create a new programActivity.
     *
     * @param programActivityDTO the programActivityDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programActivityDTO, or with status {@code 400 (Bad Request)} if the programActivity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-activities")
    public ResponseEntity<ProgramActivityDTO> createProgramActivity(@Valid @RequestBody ProgramActivityDTO programActivityDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramActivity : {}", programActivityDTO);
        if (programActivityDTO.getId() != null  &&  programActivityDTO.getId() != 0) {
            throw new BadRequestAlertException("A new programActivity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramActivityDTO result = programActivityService.save(programActivityDTO);
        return ResponseEntity.created(new URI("/api/program-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /program-activities/{programId}/bulk} : Create a new programActivity.
     *
     * @param programActivityList the programActivityList to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programActivityList, or with status {@code 400 (Bad Request)} if the programActivity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-activities/{programId}/bulk")
    public ResponseEntity<List<ProgramActivityDTO>> addBulkActivity(@PathVariable Long programId, @Valid @RequestBody List<ProgramActivityDTO>  programActivityList) throws URISyntaxException {
        log.debug("REST request to save ProgramActivity  : {}", programActivityList);

        List<ProgramActivityDTO> result = programActivityService.addBulkActivity(programId, programActivityList);

        return ResponseEntity.created(new URI("/api/program-activities/" + programId + "bulk"))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programId.toString()))
            .body(result);
    }



    /**
     * {@code PUT  /program-activities} : Updates an existing programActivity.
     *
     * @param programActivityDTO the programActivityDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programActivityDTO,
     * or with status {@code 400 (Bad Request)} if the programActivityDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programActivityDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-activities")
    public ResponseEntity<ProgramActivityDTO> updateProgramActivity(@Valid @RequestBody ProgramActivityDTO programActivityDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramActivity : {}", programActivityDTO);
        if (programActivityDTO.getId() == null ||  programActivityDTO.getId() == 0) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Boolean checkScreenType = programActivityService.checkExitScreeningTypeInPhase(programActivityDTO,null);
        if(!checkScreenType){
            throw new BadRequestAlertException("Screening type is already exist in Phase", ENTITY_NAME, "screeningtypeexist");
        }
        ProgramActivityDTO result = programActivityService.save(programActivityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programActivityDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-activities} : get all the programActivities.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programActivities in body.
     */
    @GetMapping("/program-activities")
    public ResponseEntity<List<ProgramActivityDTO>> getAllProgramActivities(ProgramActivityCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramActivities by criteria: {}", criteria);
        Page<ProgramActivityDTO> page = programActivityQueryService.findByCriteria(criteria, pageable, queryParams);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /program-activities/{programId}/ids} : get all the programActivities with activity ids only.
     *
     * @param programId the program id.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programActivities in body.
     */
    @GetMapping("/program-activities/{programId}/ids")
    public ResponseEntity<List<Integer>> getAllProgramActivityIds(@PathVariable Long programId) {
        log.debug("REST request to get getAllProgramActivityIds: {}", programId);
        List<Integer> programActivityIds = programActivityQueryService.findAllWithProgramActivityIds(programId);
        return ResponseEntity.ok().body(programActivityIds);
    }

    /**
    * {@code GET  /program-activities/count} : count all the programActivities.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-activities/count")
    public ResponseEntity<Long> countProgramActivities(ProgramActivityCriteria criteria) {
        log.debug("REST request to count ProgramActivities by criteria: {}", criteria);
        return ResponseEntity.ok().body(programActivityQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-activities/:id} : get the "id" programActivity.
     *
     * @param id the id of the programActivityDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programActivityDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-activities/{id}")
    public ResponseEntity<ProgramActivityDTO> getProgramActivity(@PathVariable Long id) {
        log.debug("REST request to get ProgramActivity : {}", id);
        Optional<ProgramActivityDTO> programActivityDTO = programActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programActivityDTO);
    }

    /**
     * {@code DELETE  /program-activities/:id} : delete the "id" programActivity.
     *
     * @param id the id of the programActivityDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-activities/{id}")
    public ResponseEntity<Void> deleteProgramActivity(@PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to delete ProgramActivity : {}", id);
        boolean isValidToDeleteCustomActivity = programActivityService.delete(id);
        if (!isValidToDeleteCustomActivity) {
            throw new BadRequestAlertException(ERROR_INVALID_DELETE_ACTIVITY, ENTITY_NAME, ERROR_INVALID_DELETE_ACTIVITY_KEY);
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @DeleteMapping("/program-activities/{programId}/delete/{activityId}")
    public ResponseEntity<Void> deleteProgramActivity(@PathVariable Long programId, @PathVariable String activityId) throws URISyntaxException {
        log.debug("REST request to delete ProgramActivityId : {}", activityId);
        boolean isValidToDeleteCustomActivity = programActivityService.deleteProgramActivityByActivityIdAndProgramId(activityId, programId);
        if (!isValidToDeleteCustomActivity) {
            throw new BadRequestAlertException(ERROR_INVALID_DELETE_ACTIVITY, ENTITY_NAME, ERROR_INVALID_DELETE_ACTIVITY_KEY);
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, activityId.toString())).build();
    }

    @PostMapping("/program-activities/{programId}/delete-bulk")
    public ResponseEntity<Void> deleteBulkProgramActivities(@PathVariable Long programId, @RequestBody List<Long> Ids) throws URISyntaxException {
        log.debug("REST request to delete ProgramActivityId : {}", Ids);
        boolean isValidToDeleteCustomActivity = programActivityService.deleteBulkProgramActivities(Ids, programId);
        if (!isValidToDeleteCustomActivity) {
            throw new BadRequestAlertException(ERROR_INVALID_DELETE_ACTIVITY, ENTITY_NAME, ERROR_INVALID_DELETE_ACTIVITY_KEY);
        }

        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, "")).build();
    }

    /**
     * {@code POST  /program/custom-activities} : Create a new programActivity.
     *
     * @param customActivityDTO the customActivityDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programActivityDTO, or with status {@code 400 (Bad Request)} if the programActivity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program/custom-activities")
    public ResponseEntity<ProgramActivityDTO> addCustomActivity(@Valid @RequestBody CustomActivityDTO customActivityDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramActivity : {}", customActivityDTO);
        Boolean checkScreenType = programActivityService.checkExitScreeningTypeInPhase(null,customActivityDTO);
        if(!checkScreenType){
            throw new BadRequestAlertException("Screening type is already exist in Phase", ENTITY_NAME, "screeningtypeexist");
        }
        ProgramActivityDTO result = programActivityService.saveCustomActivity(customActivityDTO);
        return ResponseEntity.created(new URI("/api/program/custom-activities" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/custom-activities")
    public ResponseEntity<List<CustomActivityDTO>> getCustomActivitiesByProgramId(@RequestParam(value = "programId") Long id, @RequestParam(value = "type") String type) throws Exception {
        log.debug("REST request to get ProgramActivity : {}", id);
        List<CustomActivityDTO> res = programActivityService.getCustomActivitiesByProgram(type, id);
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }

    @PostMapping("/program/check-screening-type-in-phase")
    public ResponseEntity<Boolean> checkExitScreeningTypeInPhase(@Valid @RequestBody CustomActivityDTO customActivityDTO) throws URISyntaxException {
        Boolean checkScreenType = programActivityService.checkExitScreeningTypeInPhase(null,customActivityDTO);
        if(!checkScreenType){
            throw new BadRequestAlertException("Screening type is already exist in Phase", ENTITY_NAME, "screeningtypeexist");
        }
        return ResponseEntity.status(HttpStatus.OK).body(true);
    }
}
