package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.domain.enumeration.ActivityType;
import aduro.basic.programservice.service.ClientProgramService;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.CustomClientBrandDTO;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ClientProgramDTO;
import aduro.basic.programservice.service.dto.ClientProgramCriteria;
import aduro.basic.programservice.service.ClientProgramQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.*;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ClientProgram}.
 */
@RestController
@RequestMapping("/api")
public class ClientProgramResource {

    private final Logger log = LoggerFactory.getLogger(ClientProgramResource.class);

    private static final String ENTITY_NAME = "clientProgram";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClientProgramService clientProgramService;

    private  final ProgramService programService;

    private final ClientProgramQueryService clientProgramQueryService;

    public ClientProgramResource(ClientProgramService clientProgramService, ClientProgramQueryService clientProgramQueryService, ProgramService programService) {
        this.clientProgramService = clientProgramService;
        this.clientProgramQueryService = clientProgramQueryService;
        this.programService = programService;
    }

    /**
     * {@code POST  /client-programs} : Create a new clientProgram.
     *
     * @param clientProgramDTO the clientProgramDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientProgramDTO, or with status {@code 400 (Bad Request)} if the clientProgram has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-programs")
    public ResponseEntity<ClientProgramDTO> createClientProgram(@Valid @RequestBody ClientProgramDTO clientProgramDTO) throws URISyntaxException {
        log.debug("REST request to save ClientProgram : {}", clientProgramDTO);
        if (clientProgramDTO.getId() != null &&  clientProgramDTO.getId() != 0) {
            throw new BadRequestAlertException("A new clientProgram cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientProgramDTO result = clientProgramService.save(clientProgramDTO);
        return ResponseEntity.created(new URI("/api/client-programs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /client-programs} : Updates an existing clientProgram.
     *
     * @param clientProgramDTO the clientProgramDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientProgramDTO,
     * or with status {@code 400 (Bad Request)} if the clientProgramDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clientProgramDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/client-programs")
    public ResponseEntity<ClientProgramDTO> updateClientProgram(@Valid @RequestBody ClientProgramDTO clientProgramDTO) throws URISyntaxException {
        log.debug("REST request to update ClientProgram : {}", clientProgramDTO);
        if (clientProgramDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClientProgramDTO result = clientProgramService.save(clientProgramDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clientProgramDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /client-programs} : get all the clientPrograms.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clientPrograms in body.
     */
    @GetMapping("/client-programs")
    public ResponseEntity<List<ClientProgramDTO>> getAllClientPrograms(ClientProgramCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ClientPrograms by criteria: {}", criteria);
        Page<ClientProgramDTO> page = clientProgramQueryService.findByCriteria(criteria, pageable, queryParams);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /client-programs/count} : count all the clientPrograms.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/client-programs/count")
    public ResponseEntity<Long> countClientPrograms(ClientProgramCriteria criteria) {
        log.debug("REST request to count ClientPrograms by criteria: {}", criteria);
        return ResponseEntity.ok().body(clientProgramQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /client-programs/:id} : get the "id" clientProgram.
     *
     * @param id the id of the clientProgramDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientProgramDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-programs/{id}")
    public ResponseEntity<ClientProgramDTO> getClientProgram(@PathVariable Long id) {
        log.debug("REST request to get ClientProgram : {}", id);
        Optional<ClientProgramDTO> clientProgramDTO = clientProgramService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clientProgramDTO);
    }

    /**
     * {@code DELETE  /client-programs/:id} : delete the "id" clientProgram.
     *
     * @param id the id of the clientProgramDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/client-programs/{id}")
    public ResponseEntity<Void> deleteClientProgram(@PathVariable Long id) {
        log.debug("REST request to delete ClientProgram : {}", id);
        clientProgramService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /programs/:id} : get the "id" program.
     *
     * @param clientId the id of the programDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-programs/{clientId}/current")
    public ResponseEntity<ProgramDTO> getProgramDetail(@PathVariable String clientId, @RequestParam(value = "showProgramActivity", defaultValue = "true", required = false) boolean showProgramActivity, @RequestParam(required = false) Boolean isQcTesting) {
        log.debug("REST request to get Program with client : {}", clientId);

        ProgramDTO programDTO = programService.getProgramDetailByClienId(clientId, "","", showProgramActivity, isQcTesting != null ? isQcTesting: false);

        Optional<ProgramDTO> rs = Optional.of(programDTO);
        return ResponseUtil.wrapOrNotFound(rs);
    }

    @GetMapping("/client-programs/{clientId}/current/{subgroupId}")
    public ResponseEntity<ProgramDTO> getProgramDetailForSubgroup(@PathVariable String clientId, @PathVariable String subgroupId, @RequestParam(value = "showProgramActivity", defaultValue = "true", required = false) boolean showProgramActivity, @RequestParam(required = false) Boolean isQcTesting) {
        log.debug("REST request to get Program with client : {}", clientId);

        if(subgroupId != null && (subgroupId.equals("undefined") || subgroupId.equals("0"))) {
            subgroupId = "";
        }

        ProgramDTO programDTO = programService.getProgramDetailByClienId(clientId, subgroupId,"", showProgramActivity, isQcTesting != null ? isQcTesting: false);

        Optional<ProgramDTO> rs = Optional.of(programDTO);
        return ResponseUtil.wrapOrNotFound(rs);
    }

    @GetMapping("/client-programs/{clientId}/current/{subgroupId}/{participantId}")
    public ResponseEntity<ProgramDTO> getProgramDetailByParticipant(@PathVariable String clientId, @PathVariable String subgroupId, @PathVariable String participantId, @RequestParam(value = "showProgramActivity", defaultValue = "true", required = false) boolean showProgramActivity, @RequestParam(required = false) Boolean isQcTesting) {
        log.debug("REST request to get Program with client : {}", clientId);

        if(subgroupId != null && (subgroupId.equals("undefined") || subgroupId.equals("0")))
            subgroupId = "";

        if(participantId != null && (participantId.equals("undefined") || participantId.equals("0")))
            participantId = "";

        ProgramDTO programDTO = programService.getProgramDetailByClienId(clientId, subgroupId, participantId, showProgramActivity, isQcTesting != null ? isQcTesting: false);

        Optional<ProgramDTO> rs = Optional.of(programDTO);
        return ResponseUtil.wrapOrNotFound(rs);
    }


    /**
     * {@code GET  /programs/:id} : get the "id" program.
     *
     * @param clientProgramDTOList the id of the programDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programDTO, or with status {@code 404 (Not Found)}.
     */
    @PostMapping("/client-programs/{programId}/assign-clients-program")
    public ResponseEntity<List<ClientProgramDTO>> assignClientsToProgram(@PathVariable Long programId, @Valid @RequestBody List<ClientProgramDTO> clientProgramDTOList) throws URISyntaxException {
        log.debug("REST add list clients to program");
        ClientProgramDTO rs = clientProgramService.assignClientsToProgram(programId, clientProgramDTOList);
        List<ClientProgramDTO> clientPrograms = new ArrayList<>();
        if(rs != null){
            clientPrograms.add(rs);
        }
        return ResponseEntity.created(new URI("/api/client-programs/assign-clients-program" ))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ""))
            .body(clientPrograms);
    }

    /*
    * This api will get Program Brand by List clients and Program Status
    *
    * */
    @GetMapping("/client-programs/filter")
    public ResponseEntity<List<CustomClientBrandDTO>> searchClientProgram(@RequestParam List<String> clients, @RequestParam(name = "programActive") String programStatus) {

        List<CustomClientBrandDTO> customClientBrandDTOList = clientProgramQueryService.filterProgramByClient(clients, programStatus);
        return ResponseEntity.ok().body(customClientBrandDTOList);
    }

    @GetMapping("/client-programs/program-for-qc-email")
    public ResponseEntity<List<ProgramDTO>> getProgramForSendEmailToQC(@RequestParam String clientIds) {

        List<ProgramDTO> programDTOs = clientProgramQueryService.getProgramForSendEmailToQC(Arrays.asList(clientIds.split(",")));
        return ResponseEntity.ok().body(programDTOs);
    }


}
