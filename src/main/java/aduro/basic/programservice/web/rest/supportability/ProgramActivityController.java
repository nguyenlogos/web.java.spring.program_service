package aduro.basic.programservice.web.rest.supportability;

import aduro.basic.programservice.service.dto.APIResponse;
import aduro.basic.programservice.service.dto.ActivityUpdatePointResponseDto;
import aduro.basic.programservice.service.dto.ProcessIncentiveActivityDto;
import aduro.basic.programservice.service.dto.ProcessIncentiveActivityResponse;
import aduro.basic.programservice.service.supportability.SProgramService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/program-activities")
public class ProgramActivityController {

    private final Logger logger = LoggerFactory.getLogger(ProgramActivityController.class);


    @Autowired
    private SProgramService programService;

    @GetMapping(value = "/user-events")
    public ResponseEntity<APIResponse<ActivityUpdatePointResponseDto>> getUserEventByActivityId(
        @RequestParam("participantId") String participantId,
        @RequestParam("clientId") String clientId,
        @RequestParam("activityId") Integer activityId,
        @RequestParam("programId") Integer programId
    ) throws Exception {
        logger.debug("REST request to get User Event By ParticipantId = {}, clientId = {},  activityId {}, programId = {}", participantId, clientId, activityId, programId);
        APIResponse<ActivityUpdatePointResponseDto> dto = programService.getUserEventByEventId(participantId, clientId, activityId, programId);
        return ResponseEntity.status(dto.getHttpStatus()).body(dto);
    }


    @PostMapping(value = "/process-incentive")
    public ResponseEntity<ProcessIncentiveActivityResponse> processIncentiveActivity(@RequestBody ProcessIncentiveActivityDto dto){
        logger.debug("REST request to process incentive with body = {} ", dto.toString());

        APIResponse<ProcessIncentiveActivityResponse> result = new APIResponse<>();
        ProcessIncentiveActivityResponse res = ProcessIncentiveActivityResponse.builder().build();

        try {
            result = programService.processIncentiveActivity(dto);
            if (result.getResult() != null) {
                res = result.getResult();
            }
            res.setHttpStatus(result.getHttpStatus());
            res.setErrors(result.getErrors());
            res.setMessage(result.getMessage());
        } catch (Exception e) {
            logger.info("Program Process Point Error: {}", e.getMessage());
            res.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            res.setMessage(e.getMessage());
        }
        return ResponseEntity.status(result.getHttpStatus()).body(res);
    }


}
