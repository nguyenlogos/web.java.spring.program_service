package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.service.UserEventTotalPointsDto;
import aduro.basic.programservice.service.UserEventService;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.UserEventQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.UserEvent}.
 */
@RestController
@RequestMapping("/api")
public class UserEventResource {

    private final Logger log = LoggerFactory.getLogger(UserEventResource.class);

    private static final String ENTITY_NAME = "userEvent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserEventService userEventService;

    private final UserEventQueryService userEventQueryService;

   // @Autowired
   // private OrderAdapter orderAdapter;

    public UserEventResource(UserEventService userEventService, UserEventQueryService userEventQueryService) {
        this.userEventService = userEventService;
        this.userEventQueryService = userEventQueryService;
    }

    /**
     * {@code POST  /user-events} : Create a new userEvent.
     *
     * @param userEventDTO the userEventDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userEventDTO, or with status {@code 400 (Bad Request)} if the userEvent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-events")
    public ResponseEntity<UserEventDTO> createUserEvent(@Valid @RequestBody UserEventDTO userEventDTO) throws URISyntaxException {
        log.debug("REST request to save UserEvent : {}", userEventDTO);
        if (userEventDTO.getId() != null) {
            throw new BadRequestAlertException("A new userEvent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserEventDTO result = userEventService.save(userEventDTO);
        return ResponseEntity.created(new URI("/api/user-events/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-events} : Updates an existing userEvent.
     *
     * @param userEventDTO the userEventDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userEventDTO,
     * or with status {@code 400 (Bad Request)} if the userEventDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userEventDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-events")
    public ResponseEntity<UserEventDTO> updateUserEvent(@Valid @RequestBody UserEventDTO userEventDTO) throws URISyntaxException {
        log.debug("REST request to update UserEvent : {}", userEventDTO);
        if (userEventDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserEventDTO result = userEventService.save(userEventDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userEventDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-events} : get all the userEvents.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userEvents in body.
     */
    @GetMapping("/user-events")
    public ResponseEntity<List<UserEventDTO>> getAllUserEvents(UserEventCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get UserEvents by criteria: {}", criteria);
        Page<UserEventDTO> page = userEventQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /user-events/count} : count all the userEvents.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/user-events/count")
    public ResponseEntity<Long> countUserEvents(UserEventCriteria criteria) {
        log.debug("REST request to count UserEvents by criteria: {}", criteria);
        return ResponseEntity.ok().body(userEventQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-events/:id} : get the "id" userEvent.
     *
     * @param id the id of the userEventDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userEventDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-events/{id}")
    public ResponseEntity<UserEventDTO> getUserEvent(@PathVariable Long id) {
        log.debug("REST request to get UserEvent : {}", id);
        Optional<UserEventDTO> userEventDTO = userEventService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userEventDTO);
    }

    /**
     * {@code DELETE  /user-events/:id} : delete the "id" userEvent.
     *
     * @param id the id of the userEventDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-events/{id}")
    public ResponseEntity<Void> deleteUserEvent(@PathVariable Long id) {
        log.debug("REST request to delete UserEvent : {}", id);
        userEventService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


    //activities
    @PostMapping("/user-events/incentives")
    public ResponseEntity<IncentiveEventPointDTO> handleActivityIncentiveEvent(@Valid @RequestBody ActivityEventDTO activityEventDTO) throws URISyntaxException, ExecutionException, InterruptedException {
        log.debug("REST request to save UserEvent : {}", activityEventDTO);
        // Process user incentive
        IncentiveEventPointDTO dto = userEventService.handleActivityIncentiveEvent(activityEventDTO);

        return ResponseEntity.ok().body(dto);
    }


    /**
     Incentive for wellmetric
     */
    @PostMapping("/user-events/incentive")
    public ResponseEntity<IncentiveEventPointDTO> handleIncentiveEvent(@Valid @RequestBody IncentiveEventDTO incentiveEventDTO) throws URISyntaxException, ExecutionException, InterruptedException {
        log.debug("REST request to save UserEvent for wellmetric : {}", incentiveEventDTO);
        // Process user incentive
        if (incentiveEventDTO.getEventDate() == null) {
            incentiveEventDTO.setEventDate(Instant.now());
        }
        IncentiveEventPointDTO dto = userEventService.handleIncentiveEventForWellmetric(incentiveEventDTO);
        return ResponseEntity.ok().body(dto);
    }

    /**
     Incentive for HPCONTENT
     */
    @PostMapping("/user-events/hp-content/incentive")
    public ResponseEntity<IncentiveEventPointDTO> handleIncentiveEventForHPContent(@Valid @RequestBody IncentiveEventDTO incentiveEventDTO) throws URISyntaxException, ExecutionException, InterruptedException {
        log.debug("REST request to save UserEvent for hp content : {}", incentiveEventDTO);
        // Process user incentive
        incentiveEventDTO.setEventDate(Instant.now());
        incentiveEventDTO.setEventCategory(Constants.HPCONTENT);
        IncentiveEventPointDTO dto = userEventService.processingIncentiveEventByEventCategory(incentiveEventDTO);
        return ResponseEntity.ok().body(dto);
    }

    /**
     Incentive for HPInteract
     */
    @PostMapping("/user-events/hp-interactions/incentive")
    public ResponseEntity<IncentiveEventPointDTO> handleIncentiveEventForHPInteract(@Valid @RequestBody IncentiveEventDTO incentiveEventDTO) throws URISyntaxException, ExecutionException, InterruptedException {
        log.debug("REST request to save UserEvent for hp content : {}", incentiveEventDTO);
        // Process user incentive
        incentiveEventDTO.setEventDate(Instant.now());
        incentiveEventDTO.setEventCategory(Constants.HPINTERACTIONS);
        IncentiveEventPointDTO dto = userEventService.processingIncentiveEventByEventCategory(incentiveEventDTO);
        return ResponseEntity.ok().body(dto);
    }

    /**
     Incentive for 1-1
     */
    @PostMapping("/user-events/one-one-coaching/incentive")
    public ResponseEntity<IncentiveEventPointDTO> handleIncentiveEventForOneOneCoaching(@Valid @RequestBody IncentiveEventDTO incentiveEventDTO) throws URISyntaxException, ExecutionException, InterruptedException {
        log.debug("REST request to save UserEvent for hp content : {}", incentiveEventDTO);
        // Process user incentive
        incentiveEventDTO.setEventDate(Instant.now());
        incentiveEventDTO.setEventCategory(Constants.COACHING);
        IncentiveEventPointDTO dto = userEventService.processingIncentiveEventByEventCategory(incentiveEventDTO);
        return ResponseEntity.ok().body(dto);
    }


    @PostMapping("/user-events/incentive/levelUp")
    public ResponseEntity<IncentiveResponseDTO> getLevelUpAndReward(@Valid @RequestBody IncentiveRequestBody incentiveEventDTO) throws URISyntaxException, ExecutionException, InterruptedException {
        log.debug("REST request to save UserEvent for hp content : {}", incentiveEventDTO);
        // Process user incentive
        IncentiveResponseDTO dto = userEventService.findIncentiveResultByParticipantIdAndClientIdAndEventId(incentiveEventDTO.getParticipantId(), incentiveEventDTO.getClientId(), incentiveEventDTO.getEventId(), incentiveEventDTO.isAsProgramTester());
        return ResponseEntity.ok().body(dto);
    }


    @GetMapping("/user-events/{participantId}/{clientId}/{customActivityId}/histories")
    public ResponseEntity<List<UserEventDTO>> getAllUserEventByEventId(@PathVariable String participantId, @PathVariable String clientId, @PathVariable String customActivityId, @RequestParam Map<String, String> queryParams) {
        log.debug("REST request to count UserEvents by EventId: {}", customActivityId);
        return ResponseEntity.ok().body(userEventService.getAllUserEventByParticipantIdAndClientIdAndEventId(participantId, clientId, customActivityId, queryParams));
    }

    @GetMapping("/user-events/{participantId}/{clientId}/{customActivityId}/{eventCategory}/total-points")
    public ResponseEntity<UserEventTotalPointsDto> getTotalBonusPointsByCustomActivityId(@PathVariable String participantId, @PathVariable String clientId, @PathVariable String customActivityId, @PathVariable String eventCategory, @RequestParam Map<String, String> queryParams) {
        log.debug("REST request to count UserEvents by customActivityId: {}", customActivityId);
        return ResponseEntity.ok().body(userEventService.getTotalBonusPointsByCustomActivityId(participantId, clientId, customActivityId, eventCategory, queryParams));
    }

}
