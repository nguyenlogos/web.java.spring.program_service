package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramCohortRuleService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramCohortRuleDTO;
import aduro.basic.programservice.service.dto.ProgramCohortRuleCriteria;
import aduro.basic.programservice.service.ProgramCohortRuleQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCohortRule}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCohortRuleResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortRuleResource.class);

    private static final String ENTITY_NAME = "programCohortRule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCohortRuleService programCohortRuleService;

    private final ProgramCohortRuleQueryService programCohortRuleQueryService;

    public ProgramCohortRuleResource(ProgramCohortRuleService programCohortRuleService, ProgramCohortRuleQueryService programCohortRuleQueryService) {
        this.programCohortRuleService = programCohortRuleService;
        this.programCohortRuleQueryService = programCohortRuleQueryService;
    }

    /**
     * {@code POST  /program-cohort-rules} : Create a new programCohortRule.
     *
     * @param programCohortRuleDTO the programCohortRuleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCohortRuleDTO, or with status {@code 400 (Bad Request)} if the programCohortRule has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-cohort-rules")
    public ResponseEntity<ProgramCohortRuleDTO> createProgramCohortRule(@Valid @RequestBody ProgramCohortRuleDTO programCohortRuleDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramCohortRule : {}", programCohortRuleDTO);
        if (programCohortRuleDTO.getId() != null) {
            throw new BadRequestAlertException("A new programCohortRule cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramCohortRuleDTO result = programCohortRuleService.save(programCohortRuleDTO);
        return ResponseEntity.created(new URI("/api/program-cohort-rules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-cohort-rules} : Updates an existing programCohortRule.
     *
     * @param programCohortRuleDTO the programCohortRuleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programCohortRuleDTO,
     * or with status {@code 400 (Bad Request)} if the programCohortRuleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programCohortRuleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-cohort-rules")
    public ResponseEntity<ProgramCohortRuleDTO> updateProgramCohortRule(@Valid @RequestBody ProgramCohortRuleDTO programCohortRuleDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramCohortRule : {}", programCohortRuleDTO);
        if (programCohortRuleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramCohortRuleDTO result = programCohortRuleService.save(programCohortRuleDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programCohortRuleDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-cohort-rules} : get all the programCohortRules.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCohortRules in body.
     */
    @GetMapping("/program-cohort-rules")
    public ResponseEntity<List<ProgramCohortRuleDTO>> getAllProgramCohortRules(ProgramCohortRuleCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramCohortRules by criteria: {}", criteria);
        Page<ProgramCohortRuleDTO> page = programCohortRuleQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-cohort-rules/count} : count all the programCohortRules.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-cohort-rules/count")
    public ResponseEntity<Long> countProgramCohortRules(ProgramCohortRuleCriteria criteria) {
        log.debug("REST request to count ProgramCohortRules by criteria: {}", criteria);
        return ResponseEntity.ok().body(programCohortRuleQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-cohort-rules/:id} : get the "id" programCohortRule.
     *
     * @param id the id of the programCohortRuleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCohortRuleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-cohort-rules/{id}")
    public ResponseEntity<ProgramCohortRuleDTO> getProgramCohortRule(@PathVariable Long id) {
        log.debug("REST request to get ProgramCohortRule : {}", id);
        Optional<ProgramCohortRuleDTO> programCohortRuleDTO = programCohortRuleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCohortRuleDTO);
    }

    /**
     * {@code DELETE  /program-cohort-rules/:id} : delete the "id" programCohortRule.
     *
     * @param id the id of the programCohortRuleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-cohort-rules/{id}")
    public ResponseEntity<Void> deleteProgramCohortRule(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCohortRule : {}", id);
        programCohortRuleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
