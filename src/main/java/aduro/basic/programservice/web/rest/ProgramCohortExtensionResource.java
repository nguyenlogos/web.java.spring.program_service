package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramCohortExtensionService;
import aduro.basic.programservice.service.ProgramCohortQueryService;
import aduro.basic.programservice.service.ProgramCohortService;
import aduro.basic.programservice.service.dto.ProgramCohortCriteria;
import aduro.basic.programservice.service.dto.ProgramCohortDTO;
import aduro.basic.programservice.service.dto.ProgramCohortExtensionDto;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCohort}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCohortExtensionResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortExtensionResource.class);

    private static final String ENTITY_NAME = "programCohort";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCohortExtensionService programCohortService;


    public ProgramCohortExtensionResource(ProgramCohortExtensionService programCohortService) {
        this.programCohortService = programCohortService;
    }

    /**
     * {@code POST  /program-cohorts} : Create a new programCohort.
     *
     * @param programCohortDto the programCohortDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCohortDTO, or with status {@code 400 (Bad Request)} if the programCohort has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-cohorts-extension")
    public ResponseEntity<ProgramCohortExtensionDto> createProgramCohort(@Valid @RequestBody ProgramCohortExtensionDto programCohortDto) throws URISyntaxException {
        log.debug("REST request to save ProgramCohort : {}", programCohortDto);

        ProgramCohortExtensionDto result = programCohortService.save(programCohortDto);
        return ResponseEntity.ok(result);
    }

    /**
     * {@code GET  /program-cohorts/:id} : get the "id" programCohort.
     *
     * @param id the id of the programCohortDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCohortDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-cohorts-extension/{id}")
    public ResponseEntity<ProgramCohortExtensionDto> getProgramCohort(@PathVariable Long id) {
        log.debug("REST request to get ProgramCohort : {}", id);
        Optional<ProgramCohortExtensionDto> programCohortDTO = programCohortService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCohortDTO);
    }

    /**
     * {@code DELETE  /program-cohorts/:id} : delete the "id" programCohort.
     *
     * @param id the id of the programCohortDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-cohorts-extension/{id}")
    public ResponseEntity<Void> deleteProgramCohort(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCohort : {}", id);
        programCohortService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


    @DeleteMapping("/program-cohorts-extension/remove-collection/{id}")
    public ResponseEntity<Void> deleteCollectionFromCohort(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCohort : {}", id);
        programCohortService.removeCollectionFromCohort(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/program-cohorts-extension/add")
    public ResponseEntity<List<ProgramCohortExtensionDto>> createProgramCohorts(@Valid @RequestBody List<ProgramCohortExtensionDto> programCohortList) {
        log.debug("REST request to save ProgramCohort : {}", programCohortList);

        List<ProgramCohortExtensionDto> result = programCohortService.save(programCohortList);
        return ResponseEntity.ok(result);
    }


    @GetMapping("/program-cohorts-extension")
    public ResponseEntity<List<ProgramCohortExtensionDto>> getAllProgramCohortByProgramId(@RequestParam(name = "programId") Long programId) {
        log.debug("REST request to get ProgramCohorts by programId: {}", programId);
        List<ProgramCohortExtensionDto> result = programCohortService.getAllProgramCohortByProgramId(programId);
        return ResponseEntity.ok().body(result);
    }

}
