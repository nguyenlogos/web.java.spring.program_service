package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ClientSettingService;
import aduro.basic.programservice.tangocard.CreditCardAdapter;
import aduro.basic.programservice.tangocard.dto.CreditCardDepositsRequest;
import aduro.basic.programservice.tangocard.dto.CreditCardDepositsResponse;
import aduro.basic.programservice.tangocard.dto.CreditCardRequest;
import aduro.basic.programservice.tangocard.dto.CreditCardResponse;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ClientSettingDTO;
import aduro.basic.programservice.service.dto.ClientSettingCriteria;
import aduro.basic.programservice.service.ClientSettingQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ClientSetting}.
 */
@RestController
@RequestMapping("/api")
public class ClientSettingResource {

    private final Logger log = LoggerFactory.getLogger(ClientSettingResource.class);

    private static final String ENTITY_NAME = "clientSetting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClientSettingService clientSettingService;

    private final ClientSettingQueryService clientSettingQueryService;

    @Autowired
    private CreditCardAdapter creditCardAdapter;

    public ClientSettingResource(ClientSettingService clientSettingService, ClientSettingQueryService clientSettingQueryService) {
        this.clientSettingService = clientSettingService;
        this.clientSettingQueryService = clientSettingQueryService;
    }

    /**
     * {@code POST  /client-settings} : Create a new clientSetting.
     *
     * @param clientSettingDTO the clientSettingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientSettingDTO, or with status {@code 400 (Bad Request)} if the clientSetting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-settings")
    public ResponseEntity<ClientSettingDTO> createClientSetting(@Valid @RequestBody ClientSettingDTO clientSettingDTO) throws Exception {
        log.debug("REST request to save ClientSetting : {}", clientSettingDTO);
        if (clientSettingDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientSetting cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientSettingDTO result = clientSettingService.save(clientSettingDTO);
        return ResponseEntity.created(new URI("/api/client-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
/*
    *//**
     * {@code PUT  /client-settings} : Updates an existing clientSetting.
     *
     * @param clientSettingDTO the clientSettingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientSettingDTO,
     * or with status {@code 400 (Bad Request)} if the clientSettingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clientSettingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     *//*
    @PutMapping("/client-settings")
    public ResponseEntity<ClientSettingDTO> updateClientSetting(@Valid @RequestBody ClientSettingDTO clientSettingDTO) throws Exception {
        log.debug("REST request to update ClientSetting : {}", clientSettingDTO);
        if (clientSettingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClientSettingDTO result = clientSettingService.save(clientSettingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clientSettingDTO.getId().toString()))
            .body(result);
    }*/

    /**
     * {@code GET  /client-settings} : get all the clientSettings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clientSettings in body.
     */
    @GetMapping("/client-settings")
    public ResponseEntity<List<ClientSettingDTO>> getAllClientSettings(ClientSettingCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ClientSettings by criteria: {}", criteria);
        Page<ClientSettingDTO> page = clientSettingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /client-settings/count} : count all the clientSettings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/client-settings/count")
    public ResponseEntity<Long> countClientSettings(ClientSettingCriteria criteria) {
        log.debug("REST request to count ClientSettings by criteria: {}", criteria);
        return ResponseEntity.ok().body(clientSettingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /client-settings/:id} : get the "id" clientSetting.
     *
     * @param id the id of the clientSettingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientSettingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-settings/{id}")
    public ResponseEntity<ClientSettingDTO> getClientSetting(@PathVariable Long id) {
        log.debug("REST request to get ClientSetting : {}", id);
        Optional<ClientSettingDTO> clientSettingDTO = clientSettingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clientSettingDTO);
    }

    /**
     * {@code DELETE  /client-settings/:id} : delete the "id" clientSetting.
     *
     * @param id the id of the clientSettingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/client-settings/{id}")
    public ResponseEntity<Void> deleteClientSetting(@PathVariable Long id) {
        log.debug("REST request to delete ClientSetting : {}", id);
        clientSettingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /client-settings/:id} : get the "id" clientSetting.
     *
     * @param clientId the id of the clientSettingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientSettingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-settings/get-by-client-id/{clientId}")
    public ResponseEntity<ClientSettingDTO> getClientSettingByClientId(@PathVariable String clientId) throws Exception {
        log.debug("REST request to get ClientSetting : {}", clientId);
        ClientSettingDTO clientSettingDTO = clientSettingService.getByClientId(clientId);
        return ResponseEntity.ok().body(clientSettingDTO);
    }


    /**
     * {@code GET  /client-settings/:id} : get the "id" clientSetting.
     *
     * @param accountIdentifier the id of the clientSettingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientSettingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-settings/credit-cards/{customerIdentifier}/{accountIdentifier}")
    public ResponseEntity<List<CreditCardResponse>> getClientSettingByClientId(@PathVariable String customerIdentifier, @PathVariable String accountIdentifier) throws Exception {
        log.debug("REST request to get ClientSetting : {}", accountIdentifier);
        List<CreditCardResponse> rs =  clientSettingService.getCreditCardsByAccount(customerIdentifier, accountIdentifier);
        return ResponseEntity.ok().body(rs);
    }

    /**
     * {@code POST  /client-settings} : Create a new clientSetting.
     *
     * @param   creditCardDepositsRequest to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientSettingDTO, or with status {@code 400 (Bad Request)} if the clientSetting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-settings/credit-cards/create-deposit")
    public ResponseEntity<CreditCardDepositsResponse> createCreditCardDeposit(@Valid @RequestBody CreditCardDepositsRequest creditCardDepositsRequest) throws Exception {
        CompletableFuture<CreditCardDepositsResponse> creditCardDepositsResponseCompletableFuture = creditCardAdapter.createCreditCardDepositsAsync(creditCardDepositsRequest);
        CreditCardDepositsResponse creditCardDepositsResponse = creditCardDepositsResponseCompletableFuture.get();
        return ResponseEntity.ok().body(creditCardDepositsResponse);

    }

    @PostMapping("/client-settings/update-threshold")
    public ResponseEntity<ClientSettingDTO> updateThresholdBalance(@Valid @RequestBody ClientSettingDTO clientSettingDTO) throws Exception {

        log.debug("REST request to update ClientSetting : {}", clientSettingDTO);

        if (clientSettingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        ClientSettingDTO result = clientSettingService.updateThresholdBalance(clientSettingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clientSettingDTO.getId().toString()))
            .body(result);

    }




}
