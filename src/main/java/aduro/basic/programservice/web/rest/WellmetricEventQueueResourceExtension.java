package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.dto.ScreenIncentiveResultPayload;
import aduro.basic.programservice.service.dto.WellmetricResultDTO;
import aduro.basic.programservice.service.extensions.WellmetricEventQueueServiceExtension;
import aduro.basic.programservice.service.dto.WellmetricEventQueueDTO;
import aduro.basic.programservice.service.dto.WellmetricEventQueueCriteria;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.WellmetricEventQueue}.
 */
@RestController
@RequestMapping("/api")
public class WellmetricEventQueueResourceExtension {

    private final Logger log = LoggerFactory.getLogger(WellmetricEventQueueResourceExtension.class);

    private static final String ENTITY_NAME = "wellmetricEventQueue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WellmetricEventQueueServiceExtension wellmetricEventQueueService;

    @Autowired
    private WellmetricEventQueueServiceExtension eventQueueService;

    public WellmetricEventQueueResourceExtension(WellmetricEventQueueServiceExtension wellmetricEventQueueService) {
        this.wellmetricEventQueueService = wellmetricEventQueueService;
    }


    /**
     * {@code GET  /wellmetric-event-queues} : get all the wellmetricEventQueues.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of wellmetricEventQueues in body.
     */
    @GetMapping("/wellmetric-event-queues/incentive")
    public ResponseEntity<List<WellmetricResultDTO>> getAllWellmetricIncentiveByParticipantId(WellmetricEventQueueCriteria criterial) {
        log.debug("REST request to get wellmetric incentive by params: {}", criterial);

        List<WellmetricResultDTO> list = eventQueueService.findScreeningResultByCriteria(criterial);

        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/wellmetric-event-queues/screening-result")
    public ResponseEntity<ScreenIncentiveResultPayload> getScreeningResult(WellmetricEventQueueCriteria criterial) {
        log.debug("REST request to get wellmetric incentive by params: {}", criterial);

        ScreenIncentiveResultPayload res = eventQueueService.findWellmetricResultByCriteria(criterial);

        return ResponseEntity.ok().body(res);
    }

    /**
    * {@code GET  /wellmetric-event-queues} : get all the wellmetricEventQueues and userEvent
    *
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of wellmetricEventQueues in body.
    */
    @GetMapping("/wellmetric-event-queues/screening-user")
    public ResponseEntity<ScreenIncentiveResultPayload> getScreeningResultUserEvent(WellmetricEventQueueCriteria criterial) {
        log.debug("REST request to get wellmetric incentive by params: {}", criterial);

        ScreenIncentiveResultPayload res = eventQueueService.findWellmetricResult(criterial);

        return ResponseEntity.ok().body(res);
    }
}
