package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ClientBrandSettingService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ClientBrandSettingDTO;
import aduro.basic.programservice.service.dto.ClientBrandSettingCriteria;
import aduro.basic.programservice.service.ClientBrandSettingQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ClientBrandSetting}.
 */
@RestController
@RequestMapping("/api")
public class ClientBrandSettingResource {

    private final Logger log = LoggerFactory.getLogger(ClientBrandSettingResource.class);

    private static final String ENTITY_NAME = "clientBrandSetting";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClientBrandSettingService clientBrandSettingService;

    private final ClientBrandSettingQueryService clientBrandSettingQueryService;

    public ClientBrandSettingResource(ClientBrandSettingService clientBrandSettingService, ClientBrandSettingQueryService clientBrandSettingQueryService) {
        this.clientBrandSettingService = clientBrandSettingService;
        this.clientBrandSettingQueryService = clientBrandSettingQueryService;
    }

    /**
     * {@code POST  /client-brand-settings} : Create a new clientBrandSetting.
     *
     * @param clientBrandSettingDTO the clientBrandSettingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientBrandSettingDTO, or with status {@code 400 (Bad Request)} if the clientBrandSetting has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-brand-settings")
    public ResponseEntity<ClientBrandSettingDTO> createClientBrandSetting(@Valid @RequestBody ClientBrandSettingDTO clientBrandSettingDTO) throws URISyntaxException {
        log.debug("REST request to save ClientBrandSetting : {}", clientBrandSettingDTO);
        if (clientBrandSettingDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientBrandSetting cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientBrandSettingDTO result = clientBrandSettingService.saveClientBrandSetting(clientBrandSettingDTO);
        return ResponseEntity.created(new URI("/api/client-brand-settings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /client-brand-settings} : Updates an existing clientBrandSetting.
     *
     * @param clientBrandSettingDTO the clientBrandSettingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientBrandSettingDTO,
     * or with status {@code 400 (Bad Request)} if the clientBrandSettingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clientBrandSettingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/client-brand-settings")
    public ResponseEntity<ClientBrandSettingDTO> updateClientBrandSetting(@Valid @RequestBody ClientBrandSettingDTO clientBrandSettingDTO) throws URISyntaxException {
        log.debug("REST request to update ClientBrandSetting : {}", clientBrandSettingDTO);
        if (clientBrandSettingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClientBrandSettingDTO result = clientBrandSettingService.saveClientBrandSetting(clientBrandSettingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clientBrandSettingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /client-brand-settings} : get all the clientBrandSettings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clientBrandSettings in body.
     */
    @GetMapping("/client-brand-settings")
    public ResponseEntity<List<ClientBrandSettingDTO>> getAllClientBrandSettings(ClientBrandSettingCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ClientBrandSettings by criteria: {}", criteria);
        Page<ClientBrandSettingDTO> page = clientBrandSettingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /client-brand-settings/count} : count all the clientBrandSettings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/client-brand-settings/count")
    public ResponseEntity<Long> countClientBrandSettings(ClientBrandSettingCriteria criteria) {
        log.debug("REST request to count ClientBrandSettings by criteria: {}", criteria);
        return ResponseEntity.ok().body(clientBrandSettingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /client-brand-settings/:id} : get the "id" clientBrandSetting.
     *
     * @param id the id of the clientBrandSettingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientBrandSettingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-brand-settings/{id}")
    public ResponseEntity<ClientBrandSettingDTO> getClientBrandSetting(@PathVariable Long id) {
        log.debug("REST request to get ClientBrandSetting : {}", id);
        Optional<ClientBrandSettingDTO> clientBrandSettingDTO = clientBrandSettingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clientBrandSettingDTO);
    }

    /**
     * {@code DELETE  /client-brand-settings/:id} : delete the "id" clientBrandSetting.
     *
     * @param id the id of the clientBrandSettingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/client-brand-settings/{id}")
    public ResponseEntity<Void> deleteClientBrandSetting(@PathVariable Long id) {
        log.debug("REST request to delete ClientBrandSetting : {}", id);
        clientBrandSettingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /client-brand-settings/:id} : get the "id" clientBrandSetting.
     *
     * @param clientId the id of the clientBrandSettingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientBrandSettingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-brand-settings/get-by-client-id/{clientId}")
    public ResponseEntity<ClientBrandSettingDTO> getClientBrandSettingByClientId(@PathVariable String clientId) {
        log.debug("REST request to get ClientBrandSetting : {}", clientId);
        Optional<ClientBrandSettingDTO> clientBrandSettingDTO = clientBrandSettingService.getClientBrandSettingByClientId(clientId);
        if(clientBrandSettingDTO.isPresent())
            return ResponseEntity.ok().body(clientBrandSettingDTO.get());

        return  ResponseEntity.ok().body(new ClientBrandSettingDTO());
    }

    /**
     * {@code GET  /client-brand-settings/:id} : get the "id" clientBrandSetting.
     *
     * @param subPathUrl the id of the clientBrandSettingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientBrandSettingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-brand-settings/get-by-sub-path-url/{subPathUrl}")
    public ResponseEntity<ClientBrandSettingDTO> getClientBrandSettingBySubPathUrl(@PathVariable String subPathUrl) {
        log.debug("REST request to get ClientBrandSetting : {}", subPathUrl);
        ClientBrandSettingDTO clientBrandSettingDTO = clientBrandSettingService.getClientBrandSettingBySubPathUrl(subPathUrl);

        return  ResponseEntity.ok().body(clientBrandSettingDTO);
    }
}
