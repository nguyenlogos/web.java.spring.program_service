package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ClientSubDomainService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ClientSubDomainDTO;
import aduro.basic.programservice.service.dto.ClientSubDomainCriteria;
import aduro.basic.programservice.service.ClientSubDomainQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ClientSubDomain}.
 */
@RestController
@RequestMapping("/api")
public class ClientSubDomainResource {

    private final Logger log = LoggerFactory.getLogger(ClientSubDomainResource.class);

    private static final String ENTITY_NAME = "clientSubDomain";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ClientSubDomainService clientSubDomainService;

    private final ClientSubDomainQueryService clientSubDomainQueryService;

    public ClientSubDomainResource(ClientSubDomainService clientSubDomainService, ClientSubDomainQueryService clientSubDomainQueryService) {
        this.clientSubDomainService = clientSubDomainService;
        this.clientSubDomainQueryService = clientSubDomainQueryService;
    }

    /**
     * {@code POST  /client-sub-domains} : Create a new clientSubDomain.
     *
     * @param clientSubDomainDTO the clientSubDomainDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new clientSubDomainDTO, or with status {@code 400 (Bad Request)} if the clientSubDomain has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/client-sub-domains")
    public ResponseEntity<ClientSubDomainDTO> createClientSubDomain(@Valid @RequestBody ClientSubDomainDTO clientSubDomainDTO) throws URISyntaxException {
        log.debug("REST request to save ClientSubDomain : {}", clientSubDomainDTO);
        if (clientSubDomainDTO.getId() != null) {
            throw new BadRequestAlertException("A new clientSubDomain cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ClientSubDomainDTO result = clientSubDomainService.save(clientSubDomainDTO);
        return ResponseEntity.created(new URI("/api/client-sub-domains/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /client-sub-domains} : Updates an existing clientSubDomain.
     *
     * @param clientSubDomainDTO the clientSubDomainDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated clientSubDomainDTO,
     * or with status {@code 400 (Bad Request)} if the clientSubDomainDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the clientSubDomainDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/client-sub-domains")
    public ResponseEntity<ClientSubDomainDTO> updateClientSubDomain(@Valid @RequestBody ClientSubDomainDTO clientSubDomainDTO) throws URISyntaxException {
        log.debug("REST request to update ClientSubDomain : {}", clientSubDomainDTO);
        if (clientSubDomainDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ClientSubDomainDTO result = clientSubDomainService.save(clientSubDomainDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, clientSubDomainDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /client-sub-domains} : get all the clientSubDomains.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of clientSubDomains in body.
     */
    @GetMapping("/client-sub-domains")
    public ResponseEntity<List<ClientSubDomainDTO>> getAllClientSubDomains(ClientSubDomainCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ClientSubDomains by criteria: {}", criteria);
        Page<ClientSubDomainDTO> page = clientSubDomainQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /client-sub-domains/count} : count all the clientSubDomains.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/client-sub-domains/count")
    public ResponseEntity<Long> countClientSubDomains(ClientSubDomainCriteria criteria) {
        log.debug("REST request to count ClientSubDomains by criteria: {}", criteria);
        return ResponseEntity.ok().body(clientSubDomainQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /client-sub-domains/:id} : get the "id" clientSubDomain.
     *
     * @param id the id of the clientSubDomainDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the clientSubDomainDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/client-sub-domains/{id}")
    public ResponseEntity<ClientSubDomainDTO> getClientSubDomain(@PathVariable Long id) {
        log.debug("REST request to get ClientSubDomain : {}", id);
        Optional<ClientSubDomainDTO> clientSubDomainDTO = clientSubDomainService.findOne(id);
        return ResponseUtil.wrapOrNotFound(clientSubDomainDTO);
    }

    /**
     * {@code DELETE  /client-sub-domains/:id} : delete the "id" clientSubDomain.
     *
     * @param id the id of the clientSubDomainDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/client-sub-domains/{id}")
    public ResponseEntity<Void> deleteClientSubDomain(@PathVariable Long id) {
        log.debug("REST request to delete ClientSubDomain : {}", id);
        clientSubDomainService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
