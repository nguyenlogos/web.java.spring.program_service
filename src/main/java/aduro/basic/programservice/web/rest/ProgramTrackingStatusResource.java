package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramTrackingStatusService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramTrackingStatusDTO;
import aduro.basic.programservice.service.dto.ProgramTrackingStatusCriteria;
import aduro.basic.programservice.service.ProgramTrackingStatusQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramTrackingStatus}.
 */
@RestController
@RequestMapping("/api")
public class ProgramTrackingStatusResource {

    private final Logger log = LoggerFactory.getLogger(ProgramTrackingStatusResource.class);

    private static final String ENTITY_NAME = "programTrackingStatus";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramTrackingStatusService programTrackingStatusService;

    private final ProgramTrackingStatusQueryService programTrackingStatusQueryService;

    public ProgramTrackingStatusResource(ProgramTrackingStatusService programTrackingStatusService, ProgramTrackingStatusQueryService programTrackingStatusQueryService) {
        this.programTrackingStatusService = programTrackingStatusService;
        this.programTrackingStatusQueryService = programTrackingStatusQueryService;
    }

    /**
     * {@code POST  /program-tracking-statuses} : Create a new programTrackingStatus.
     *
     * @param programTrackingStatusDTO the programTrackingStatusDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programTrackingStatusDTO, or with status {@code 400 (Bad Request)} if the programTrackingStatus has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-tracking-statuses")
    public ResponseEntity<ProgramTrackingStatusDTO> createProgramTrackingStatus(@Valid @RequestBody ProgramTrackingStatusDTO programTrackingStatusDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramTrackingStatus : {}", programTrackingStatusDTO);
        if (programTrackingStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new programTrackingStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramTrackingStatusDTO result = programTrackingStatusService.save(programTrackingStatusDTO);
        return ResponseEntity.created(new URI("/api/program-tracking-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-tracking-statuses} : Updates an existing programTrackingStatus.
     *
     * @param programTrackingStatusDTO the programTrackingStatusDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programTrackingStatusDTO,
     * or with status {@code 400 (Bad Request)} if the programTrackingStatusDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programTrackingStatusDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-tracking-statuses")
    public ResponseEntity<ProgramTrackingStatusDTO> updateProgramTrackingStatus(@Valid @RequestBody ProgramTrackingStatusDTO programTrackingStatusDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramTrackingStatus : {}", programTrackingStatusDTO);
        if (programTrackingStatusDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramTrackingStatusDTO result = programTrackingStatusService.save(programTrackingStatusDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programTrackingStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-tracking-statuses} : get all the programTrackingStatuses.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programTrackingStatuses in body.
     */
    @GetMapping("/program-tracking-statuses")
    public ResponseEntity<List<ProgramTrackingStatusDTO>> getAllProgramTrackingStatuses(ProgramTrackingStatusCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramTrackingStatuses by criteria: {}", criteria);
        Page<ProgramTrackingStatusDTO> page = programTrackingStatusQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-tracking-statuses/count} : count all the programTrackingStatuses.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-tracking-statuses/count")
    public ResponseEntity<Long> countProgramTrackingStatuses(ProgramTrackingStatusCriteria criteria) {
        log.debug("REST request to count ProgramTrackingStatuses by criteria: {}", criteria);
        return ResponseEntity.ok().body(programTrackingStatusQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-tracking-statuses/:id} : get the "id" programTrackingStatus.
     *
     * @param id the id of the programTrackingStatusDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programTrackingStatusDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-tracking-statuses/{id}")
    public ResponseEntity<ProgramTrackingStatusDTO> getProgramTrackingStatus(@PathVariable Long id) {
        log.debug("REST request to get ProgramTrackingStatus : {}", id);
        Optional<ProgramTrackingStatusDTO> programTrackingStatusDTO = programTrackingStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programTrackingStatusDTO);
    }

    /**
     * {@code DELETE  /program-tracking-statuses/:id} : delete the "id" programTrackingStatus.
     *
     * @param id the id of the programTrackingStatusDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-tracking-statuses/{id}")
    public ResponseEntity<Void> deleteProgramTrackingStatus(@PathVariable Long id) {
        log.debug("REST request to delete ProgramTrackingStatus : {}", id);
        programTrackingStatusService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
