package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramRequirementItemsService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramRequirementItemsDTO;
import aduro.basic.programservice.service.dto.ProgramRequirementItemsCriteria;
import aduro.basic.programservice.service.ProgramRequirementItemsQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramRequirementItems}.
 */
@RestController
@RequestMapping("/api")
public class ProgramRequirementItemsResource {

    private final Logger log = LoggerFactory.getLogger(ProgramRequirementItemsResource.class);

    private static final String ENTITY_NAME = "programRequirementItems";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramRequirementItemsService programRequirementItemsService;

    private final ProgramRequirementItemsQueryService programRequirementItemsQueryService;

    public ProgramRequirementItemsResource(ProgramRequirementItemsService programRequirementItemsService, ProgramRequirementItemsQueryService programRequirementItemsQueryService) {
        this.programRequirementItemsService = programRequirementItemsService;
        this.programRequirementItemsQueryService = programRequirementItemsQueryService;
    }

    /**
     * {@code POST  /program-requirement-items} : Create a new programRequirementItems.
     *
     * @param programRequirementItemsDTO the programRequirementItemsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programRequirementItemsDTO, or with status {@code 400 (Bad Request)} if the programRequirementItems has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-requirement-items")
    public ResponseEntity<ProgramRequirementItemsDTO> createProgramRequirementItems(@Valid @RequestBody ProgramRequirementItemsDTO programRequirementItemsDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramRequirementItems : {}", programRequirementItemsDTO);
        if (programRequirementItemsDTO.getId() != null) {
            throw new BadRequestAlertException("A new programRequirementItems cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramRequirementItemsDTO result = programRequirementItemsService.save(programRequirementItemsDTO);
        return ResponseEntity.created(new URI("/api/program-requirement-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-requirement-items} : Updates an existing programRequirementItems.
     *
     * @param programRequirementItemsDTO the programRequirementItemsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programRequirementItemsDTO,
     * or with status {@code 400 (Bad Request)} if the programRequirementItemsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programRequirementItemsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-requirement-items")
    public ResponseEntity<ProgramRequirementItemsDTO> updateProgramRequirementItems(@Valid @RequestBody ProgramRequirementItemsDTO programRequirementItemsDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramRequirementItems : {}", programRequirementItemsDTO);
        if (programRequirementItemsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramRequirementItemsDTO result = programRequirementItemsService.save(programRequirementItemsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programRequirementItemsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-requirement-items} : get all the programRequirementItems.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programRequirementItems in body.
     */
    @GetMapping("/program-requirement-items")
    public ResponseEntity<List<ProgramRequirementItemsDTO>> getAllProgramRequirementItems(ProgramRequirementItemsCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramRequirementItems by criteria: {}", criteria);
        Page<ProgramRequirementItemsDTO> page = programRequirementItemsQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-requirement-items/count} : count all the programRequirementItems.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-requirement-items/count")
    public ResponseEntity<Long> countProgramRequirementItems(ProgramRequirementItemsCriteria criteria) {
        log.debug("REST request to count ProgramRequirementItems by criteria: {}", criteria);
        return ResponseEntity.ok().body(programRequirementItemsQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-requirement-items/:id} : get the "id" programRequirementItems.
     *
     * @param id the id of the programRequirementItemsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programRequirementItemsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-requirement-items/{id}")
    public ResponseEntity<ProgramRequirementItemsDTO> getProgramRequirementItems(@PathVariable Long id) {
        log.debug("REST request to get ProgramRequirementItems : {}", id);
        Optional<ProgramRequirementItemsDTO> programRequirementItemsDTO = programRequirementItemsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programRequirementItemsDTO);
    }

    /**
     * {@code DELETE  /program-requirement-items/:id} : delete the "id" programRequirementItems.
     *
     * @param id the id of the programRequirementItemsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-requirement-items/{id}")
    public ResponseEntity<Void> deleteProgramRequirementItems(@PathVariable Long id) {
        log.debug("REST request to delete ProgramRequirementItems : {}", id);
        programRequirementItemsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
