package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramUserCohortService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramUserCohortDTO;
import aduro.basic.programservice.service.dto.ProgramUserCohortCriteria;
import aduro.basic.programservice.service.ProgramUserCohortQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramUserCohort}.
 */
@RestController
@RequestMapping("/api")
public class ProgramUserCohortResource {

    private final Logger log = LoggerFactory.getLogger(ProgramUserCohortResource.class);

    private static final String ENTITY_NAME = "programUserCohort";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramUserCohortService programUserCohortService;

    private final ProgramUserCohortQueryService programUserCohortQueryService;

    public ProgramUserCohortResource(ProgramUserCohortService programUserCohortService, ProgramUserCohortQueryService programUserCohortQueryService) {
        this.programUserCohortService = programUserCohortService;
        this.programUserCohortQueryService = programUserCohortQueryService;
    }

    /**
     * {@code POST  /program-user-cohorts} : Create a new programUserCohort.
     *
     * @param programUserCohortDTO the programUserCohortDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programUserCohortDTO, or with status {@code 400 (Bad Request)} if the programUserCohort has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-user-cohorts")
    public ResponseEntity<ProgramUserCohortDTO> createProgramUserCohort(@Valid @RequestBody ProgramUserCohortDTO programUserCohortDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramUserCohort : {}", programUserCohortDTO);
        if (programUserCohortDTO.getId() != null) {
            throw new BadRequestAlertException("A new programUserCohort cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramUserCohortDTO result = programUserCohortService.save(programUserCohortDTO);
        return ResponseEntity.created(new URI("/api/program-user-cohorts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-user-cohorts} : Updates an existing programUserCohort.
     *
     * @param programUserCohortDTO the programUserCohortDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programUserCohortDTO,
     * or with status {@code 400 (Bad Request)} if the programUserCohortDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programUserCohortDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-user-cohorts")
    public ResponseEntity<ProgramUserCohortDTO> updateProgramUserCohort(@Valid @RequestBody ProgramUserCohortDTO programUserCohortDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramUserCohort : {}", programUserCohortDTO);
        if (programUserCohortDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramUserCohortDTO result = programUserCohortService.save(programUserCohortDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programUserCohortDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-user-cohorts} : get all the programUserCohorts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programUserCohorts in body.
     */
    @GetMapping("/program-user-cohorts")
    public ResponseEntity<List<ProgramUserCohortDTO>> getAllProgramUserCohorts(ProgramUserCohortCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramUserCohorts by criteria: {}", criteria);
        Page<ProgramUserCohortDTO> page = programUserCohortQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-user-cohorts/count} : count all the programUserCohorts.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-user-cohorts/count")
    public ResponseEntity<Long> countProgramUserCohorts(ProgramUserCohortCriteria criteria) {
        log.debug("REST request to count ProgramUserCohorts by criteria: {}", criteria);
        return ResponseEntity.ok().body(programUserCohortQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-user-cohorts/:id} : get the "id" programUserCohort.
     *
     * @param id the id of the programUserCohortDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programUserCohortDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-user-cohorts/{id}")
    public ResponseEntity<ProgramUserCohortDTO> getProgramUserCohort(@PathVariable Long id) {
        log.debug("REST request to get ProgramUserCohort : {}", id);
        Optional<ProgramUserCohortDTO> programUserCohortDTO = programUserCohortService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programUserCohortDTO);
    }

    /**
     * {@code DELETE  /program-user-cohorts/:id} : delete the "id" programUserCohort.
     *
     * @param id the id of the programUserCohortDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-user-cohorts/{id}")
    public ResponseEntity<Void> deleteProgramUserCohort(@PathVariable Long id) {
        log.debug("REST request to delete ProgramUserCohort : {}", id);
        programUserCohortService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
