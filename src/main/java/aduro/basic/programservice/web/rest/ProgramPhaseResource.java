package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramPhaseQueryService;
import aduro.basic.programservice.service.ProgramPhaseService;
import aduro.basic.programservice.service.dto.ProgramPhaseCriteria;
import aduro.basic.programservice.service.dto.ProgramPhaseDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramPhase}.
 */
@RestController
@RequestMapping("/api")
public class ProgramPhaseResource {

    private final Logger log = LoggerFactory.getLogger(ProgramPhaseResource.class);

    private static final String ENTITY_NAME = "programPhase";
    private static final String ERROR_INVALID_DELETE_ACTIVITY_KEY = "enrolledCustomActivity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramPhaseService programPhaseService;

    private final ProgramPhaseQueryService programPhaseQueryService;

    public ProgramPhaseResource(ProgramPhaseService programPhaseService, ProgramPhaseQueryService programPhaseQueryService) {
        this.programPhaseService = programPhaseService;
        this.programPhaseQueryService = programPhaseQueryService;
    }

    /**
     * {@code POST  /program-phases} : Create a new programPhase.
     *
     * @param programPhaseDTO the programPhaseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programPhaseDTO, or with status {@code 400 (Bad Request)} if the programPhase has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-phases")
    public ResponseEntity<ProgramPhaseDTO> createProgramPhase(@Valid @RequestBody ProgramPhaseDTO programPhaseDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramPhase : {}", programPhaseDTO);
        if (programPhaseDTO.getId() != null) {
            throw new BadRequestAlertException("A new programPhase cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramPhaseDTO result = programPhaseService.save(programPhaseDTO);
        return ResponseEntity.created(new URI("/api/program-phases/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-phases} : Updates an existing programPhase.
     *
     * @param programPhaseDTO the programPhaseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programPhaseDTO,
     * or with status {@code 400 (Bad Request)} if the programPhaseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programPhaseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-phases")
    public ResponseEntity<ProgramPhaseDTO> updateProgramPhase(@Valid @RequestBody ProgramPhaseDTO programPhaseDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramPhase : {}", programPhaseDTO);
        if (programPhaseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramPhaseDTO result = programPhaseService.save(programPhaseDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programPhaseDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-phases} : get all the programPhases.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programPhases in body.
     */
    @GetMapping("/program-phases")
    public ResponseEntity<List<ProgramPhaseDTO>> getAllProgramPhases(ProgramPhaseCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramPhases by criteria: {}", criteria);
        Page<ProgramPhaseDTO> page = programPhaseQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-phases/count} : count all the programPhases.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-phases/count")
    public ResponseEntity<Long> countProgramPhases(ProgramPhaseCriteria criteria) {
        log.debug("REST request to count ProgramPhases by criteria: {}", criteria);
        return ResponseEntity.ok().body(programPhaseQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-phases/:id} : get the "id" programPhase.
     *
     * @param id the id of the programPhaseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programPhaseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-phases/{id}")
    public ResponseEntity<ProgramPhaseDTO> getProgramPhase(@PathVariable Long id) {
        log.debug("REST request to get ProgramPhase : {}", id);
        Optional<ProgramPhaseDTO> programPhaseDTO = programPhaseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programPhaseDTO);
    }

    /**
     * {@code DELETE  /program-phases/:id} : delete the "id" programPhase.
     *
     * @param id the id of the programPhaseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-phases/{id}")
    public ResponseEntity<Void> deleteProgramPhase(@PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to delete ProgramPhase : {}", id);

        //delete all program activity in phase.
        String resultDeletePhase = programPhaseService.delete(id);
        if (!StringUtils.isEmpty(resultDeletePhase)) {
            throw new BadRequestAlertException(resultDeletePhase, ENTITY_NAME, ERROR_INVALID_DELETE_ACTIVITY_KEY);
        }

        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
