package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.EConsentService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.EConsentDTO;
import aduro.basic.programservice.service.dto.EConsentCriteria;
import aduro.basic.programservice.service.EConsentQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.EConsent}.
 */
@RestController
@RequestMapping("/api")
public class EConsentResource {

    private final Logger log = LoggerFactory.getLogger(EConsentResource.class);

    private static final String ENTITY_NAME = "eConsent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EConsentService eConsentService;

    private final EConsentQueryService eConsentQueryService;

    public EConsentResource(EConsentService eConsentService, EConsentQueryService eConsentQueryService) {
        this.eConsentService = eConsentService;
        this.eConsentQueryService = eConsentQueryService;
    }

    /**
     * {@code POST  /e-consents} : Create a new eConsent.
     *
     * @param eConsentDTO the eConsentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new eConsentDTO, or with status {@code 400 (Bad Request)} if the eConsent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/e-consents")
    public ResponseEntity<EConsentDTO> createEConsent(@Valid @RequestBody EConsentDTO eConsentDTO) throws URISyntaxException {
        log.debug("REST request to save EConsent : {}", eConsentDTO);
        if (eConsentDTO.getId() != null) {
            throw new BadRequestAlertException("A new eConsent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EConsentDTO result = eConsentService.save(eConsentDTO);
        return ResponseEntity.created(new URI("/api/e-consents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /e-consents} : Updates an existing eConsent.
     *
     * @param eConsentDTO the eConsentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated eConsentDTO,
     * or with status {@code 400 (Bad Request)} if the eConsentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the eConsentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/e-consents")
    public ResponseEntity<EConsentDTO> updateEConsent(@Valid @RequestBody EConsentDTO eConsentDTO) throws URISyntaxException {
        log.debug("REST request to update EConsent : {}", eConsentDTO);
        if (eConsentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        EConsentDTO result = eConsentService.save(eConsentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, eConsentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /e-consents} : get all the eConsents.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of eConsents in body.
     */
    @GetMapping("/e-consents")
    public ResponseEntity<List<EConsentDTO>> getAllEConsents(EConsentCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get EConsents by criteria: {}", criteria);
        Page<EConsentDTO> page = eConsentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /e-consents/count} : count all the eConsents.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/e-consents/count")
    public ResponseEntity<Long> countEConsents(EConsentCriteria criteria) {
        log.debug("REST request to count EConsents by criteria: {}", criteria);
        return ResponseEntity.ok().body(eConsentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /e-consents/:id} : get the "id" eConsent.
     *
     * @param id the id of the eConsentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the eConsentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/e-consents/{id}")
    public ResponseEntity<EConsentDTO> getEConsent(@PathVariable Long id) {
        log.debug("REST request to get EConsent : {}", id);
        Optional<EConsentDTO> eConsentDTO = eConsentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(eConsentDTO);
    }

    /**
     * {@code DELETE  /e-consents/:id} : delete the "id" eConsent.
     *
     * @param id the id of the eConsentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/e-consents/{id}")
    public ResponseEntity<Void> deleteEConsent(@PathVariable Long id) {
        log.debug("REST request to delete EConsent : {}", id);
        eConsentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
