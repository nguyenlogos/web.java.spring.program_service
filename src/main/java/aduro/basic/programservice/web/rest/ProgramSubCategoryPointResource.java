package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramSubCategoryPointService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointCriteria;
import aduro.basic.programservice.service.ProgramSubCategoryPointQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramSubCategoryPoint}.
 */
@RestController
@RequestMapping("/api")
public class ProgramSubCategoryPointResource {

    private final Logger log = LoggerFactory.getLogger(ProgramSubCategoryPointResource.class);

    private static final String ENTITY_NAME = "programSubCategoryPoint";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramSubCategoryPointService programSubCategoryPointService;

    private final ProgramSubCategoryPointQueryService programSubCategoryPointQueryService;

    public ProgramSubCategoryPointResource(ProgramSubCategoryPointService programSubCategoryPointService, ProgramSubCategoryPointQueryService programSubCategoryPointQueryService) {
        this.programSubCategoryPointService = programSubCategoryPointService;
        this.programSubCategoryPointQueryService = programSubCategoryPointQueryService;
    }

    /**
     * {@code POST  /program-sub-category-points} : Create a new programSubCategoryPoint.
     *
     * @param programSubCategoryPointDTO the programSubCategoryPointDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programSubCategoryPointDTO, or with status {@code 400 (Bad Request)} if the programSubCategoryPoint has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-sub-category-points")
    public ResponseEntity<ProgramSubCategoryPointDTO> createProgramSubCategoryPoint(@Valid @RequestBody ProgramSubCategoryPointDTO programSubCategoryPointDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramSubCategoryPoint : {}", programSubCategoryPointDTO);
        if (programSubCategoryPointDTO.getId() != null && programSubCategoryPointDTO.getId() != 0) {
            throw new BadRequestAlertException("A new programSubCategoryPoint cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramSubCategoryPointDTO result = programSubCategoryPointService.save(programSubCategoryPointDTO);
        return ResponseEntity.created(new URI("/api/program-sub-category-points/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-sub-category-points} : Updates an existing programSubCategoryPoint.
     *
     * @param programSubCategoryPointDTO the programSubCategoryPointDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programSubCategoryPointDTO,
     * or with status {@code 400 (Bad Request)} if the programSubCategoryPointDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programSubCategoryPointDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-sub-category-points")
    public ResponseEntity<ProgramSubCategoryPointDTO> updateProgramSubCategoryPoint(@Valid @RequestBody ProgramSubCategoryPointDTO programSubCategoryPointDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramSubCategoryPoint : {}", programSubCategoryPointDTO);
        if (programSubCategoryPointDTO.getId() == null ||  programSubCategoryPointDTO.getId() == 0) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramSubCategoryPointDTO result = programSubCategoryPointService.save(programSubCategoryPointDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-sub-category-points} : get all the programSubCategoryPoints.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programSubCategoryPoints in body.
     */
    @GetMapping("/program-sub-category-points")
    public ResponseEntity<List<ProgramSubCategoryPointDTO>> getAllProgramSubCategoryPoints(ProgramSubCategoryPointCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramSubCategoryPoints by criteria: {}", criteria);
        Page<ProgramSubCategoryPointDTO> page = programSubCategoryPointQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-sub-category-points/count} : count all the programSubCategoryPoints.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-sub-category-points/count")
    public ResponseEntity<Long> countProgramSubCategoryPoints(ProgramSubCategoryPointCriteria criteria) {
        log.debug("REST request to count ProgramSubCategoryPoints by criteria: {}", criteria);
        return ResponseEntity.ok().body(programSubCategoryPointQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-sub-category-points/:id} : get the "id" programSubCategoryPoint.
     *
     * @param id the id of the programSubCategoryPointDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programSubCategoryPointDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-sub-category-points/{id}")
    public ResponseEntity<ProgramSubCategoryPointDTO> getProgramSubCategoryPoint(@PathVariable Long id) {
        log.debug("REST request to get ProgramSubCategoryPoint : {}", id);
        Optional<ProgramSubCategoryPointDTO> programSubCategoryPointDTO = programSubCategoryPointService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programSubCategoryPointDTO);
    }

    /**
     * {@code DELETE  /program-sub-category-points/:id} : delete the "id" programSubCategoryPoint.
     *
     * @param id the id of the programSubCategoryPointDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-sub-category-points/{id}")
    public ResponseEntity<Void> deleteProgramSubCategoryPoint(@PathVariable Long id) {
        log.debug("REST request to delete ProgramSubCategoryPoint : {}", id);
        programSubCategoryPointService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


    @PostMapping("/program-category-points/{programCategoryPointId}/add-sub-categories")
    public ResponseEntity<List<ProgramSubCategoryPointDTO>> AddProgramCategoryPoints(@PathVariable Long programCategoryPointId, @RequestBody List<ProgramSubCategoryPointDTO> programSubCategoryPointDTOs) throws URISyntaxException {
        log.debug("REST request to save ProgramCategoryPoint : {}", programSubCategoryPointDTOs);

        List<ProgramSubCategoryPointDTO> result = programSubCategoryPointService.addAndRemoveProgramSubCategoryPoints(programCategoryPointId, programSubCategoryPointDTOs);

        return ResponseEntity.created(new URI("/api/program-category-points/" + programCategoryPointId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programCategoryPointId.toString()))
            .body(result);
    }

    @GetMapping("/program-sub-category-points/get-by-user/{clientId}/{participantId}/{subCategoryCode}")
    public ResponseEntity<ProgramSubCategoryPointDTO> getProgramSubCategoryPointByUser(@PathVariable String clientId,@PathVariable String participantId, @PathVariable String subCategoryCode) {
        log.debug("REST request to get ProgramSubCategoryPoint : {}", clientId + participantId + subCategoryCode);
        ProgramSubCategoryPointDTO programCategoryPointDTO = programSubCategoryPointService.getProgramSubCategoryPointByUser(clientId, participantId, subCategoryCode);
        return ResponseEntity.ok().body(programCategoryPointDTO);
    }


}
