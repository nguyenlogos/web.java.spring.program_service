/**
 * View Models used by Spring MVC REST controllers.
 */
package aduro.basic.programservice.web.rest.vm;
