package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramCollectionExtensionService;
import aduro.basic.programservice.service.ProgramCollectionQueryService;
import aduro.basic.programservice.service.dto.DeleteCollectionRequestDto;
import aduro.basic.programservice.service.dto.ProgramCollectionExtensionDto;
import aduro.basic.programservice.service.dto.ProgramCollectionExtensionWithCompletionDto;
import aduro.basic.programservice.service.dto.RemovableCollectionContentDto;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCollection}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCollectionExtensionResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCollectionExtensionResource.class);

    private static final String ENTITY_NAME = "programCollection";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCollectionExtensionService programCollectionService;

    private final ProgramCollectionQueryService programCollectionQueryService;

    public ProgramCollectionExtensionResource(ProgramCollectionExtensionService programCollectionService, ProgramCollectionQueryService programCollectionQueryService) {
        this.programCollectionService = programCollectionService;
        this.programCollectionQueryService = programCollectionQueryService;
    }

    @PostMapping("/program-collections-extension")
    public ResponseEntity<ProgramCollectionExtensionDto> createProgramCollection(@Valid @RequestBody ProgramCollectionExtensionDto dto) throws URISyntaxException {
        log.debug("REST request to save ProgramCollection : {}", dto);
        ProgramCollectionExtensionDto result = programCollectionService.save(dto);
        return ResponseEntity.ok(result);
    }

    /**
     * {@code GET  /program-collections-extension/:id} : get the "id" programCollection.
     *
     * @param id the id of the programCollectionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCollectionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-collections-extension/{id}")
    public ResponseEntity<ProgramCollectionExtensionDto> getProgramCollection(@PathVariable Long id) {
        log.debug("REST request to get ProgramCollection : {}", id);
        Optional<ProgramCollectionExtensionDto> programCollectionDTO = programCollectionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCollectionDTO);
    }

    /**
     * {@code GET  /program-collections-extension/:id/:participantId/:clientId} : get the "id" programCollection by "participantId" and "clientId".
     *
     * @param id the id of the programCollectionDTO to retrieve.
     * @param participantId the current participant's id which belongs the programCollection to retrieve.
     * @param clientId the current client
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCollectionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-collections-extension/{id}/{participantId}/{clientId}")
    public ResponseEntity<ProgramCollectionExtensionWithCompletionDto> getProgramCollectionWithCompletion(@PathVariable Long id, @PathVariable String participantId, @PathVariable String clientId) {
        log.debug("REST request to get ProgramCollection : {}", id);
        Optional<ProgramCollectionExtensionWithCompletionDto> programCollectionDTO = programCollectionService.findOneWithCompletions(id, participantId, clientId);
        return ResponseUtil.wrapOrNotFound(programCollectionDTO);
    }

    /**
     * {@code DELETE /program-collections-extension/:id} : delete the "id" programCollection.
     *
     * @param id the id of the programCollectionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @PostMapping("/program-collections-extension/remove")
    public ResponseEntity<DeleteCollectionRequestDto> deleteProgramCollection(@Valid @RequestBody DeleteCollectionRequestDto dto) {
        log.debug("REST request to delete ProgramCollection : {}", dto.getId());
        DeleteCollectionRequestDto res = programCollectionService.delete(dto);
        return ResponseEntity.ok().body(res);
    }

    @GetMapping("/program-collections-extension")
    public ResponseEntity<List<ProgramCollectionExtensionDto>> getAllProgramCollections(@RequestParam(name = "programId") Long programId) throws URISyntaxException {
        log.debug("REST request to get ProgramCollection : {}", programId);
        List<ProgramCollectionExtensionDto> result = programCollectionService.getByProgramId(programId);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/program-collections-extension/save-list")
    public ResponseEntity<List<ProgramCollectionExtensionDto>> createProgramCollection(@Valid @RequestBody List<ProgramCollectionExtensionDto> collections) throws URISyntaxException {
        log.debug("REST request to save ProgramCollection : {}", collections);
        List<ProgramCollectionExtensionDto> result = programCollectionService.saveList(collections);
        return ResponseEntity.ok(result);
    }


        @PostMapping("/program-collections-extension/check-delete-item")
    public ResponseEntity<RemovableCollectionContentDto> checkDeleteItem(@Valid @RequestBody RemovableCollectionContentDto dto) throws URISyntaxException {
        log.debug("REST request to check delete item = : {}", dto);
        RemovableCollectionContentDto result = programCollectionService.checkDeleteItemIdInCollection(dto);
        return ResponseEntity.ok(result);
    }

}
