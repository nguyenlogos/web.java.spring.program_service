package aduro.basic.programservice.web.rest.supportability;

import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.jobs.ScheduledTasks;
import aduro.basic.programservice.service.supportability.SProgramService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class ProgramUserController {

    private final Logger log = LoggerFactory.getLogger(ProgramUserController.class);


    private final SProgramService programService;

    @Autowired
    private ScheduledTasks scheduledTasks;

    private static final String ENTITY_NAME = "program-users";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public ProgramUserController(SProgramService programService) {
        this.programService = programService;
    }


    @GetMapping(value = "/program-user-detail/{clientId}/{participantId}")
    public ResponseEntity<ProgramUserInfoDto> getActiveProgramDetail(
        @PathVariable String clientId,
        @PathVariable String participantId
    ) {
        log.debug("REST request to get Program  clientId: {}, participantId {}", clientId, participantId);
        ProgramUserInfoDto programDTO = programService.getActiveProgramUserByClientIdAndParticipantId(clientId, participantId);
        return ResponseEntity.ok(programDTO);
    }


    @GetMapping(value = "/program-activities")
    public ResponseEntity<ListCustomActivityResponseDto> getActivitiesByActiveProgram(
        @RequestParam("clientId") String clientId,
        @RequestParam("participantId") String participantId,
        @RequestParam("limit") long limit,
        @RequestParam("offset") long offset,
        @RequestParam(value = "searchTitle", required = false) String searchTitle
    ) throws Exception {
        log.debug("REST request to get Program  clientId: {}, participantId {}", clientId, participantId);
        APIResponse<ListCustomActivityResponseDto> dto = programService.getCustomActivityByActiveProgramId(clientId, participantId, limit, offset, searchTitle);
        return ResponseEntity.status(dto.getHttpStatus()).body(dto.getResult());
    }

    @PostMapping(value = "/level-trigger")
    public ResponseEntity<Boolean> triggerUpdateLevel(@RequestBody UpdateLevelRequest request) {
        log.debug("REST request to trigger update level  clientId: {}, participantId {}", request.getClientId(), request.getParticipantId());

        new Thread(() -> {
            log.info("->>>> REST request to trigger level");
            scheduledTasks.insertTriggerLoader(request);
        }).start();

        return ResponseEntity.status(HttpStatus.OK).body(true);
    }

    @PostMapping(value = "/trigger-process-level-queue")
    public ResponseEntity<Boolean> triggerProcessLevelQueue() {
        log.debug("REST request to trigger check process level queue");

        new Thread(() -> {
            log.info("->>>> Start request to process level in queue");
            scheduledTasks.processIncentiveParticipant();
            log.info("->>>> End request to trigger level queue");
        }).start();

        return ResponseEntity.status(HttpStatus.OK).body(true);
    }
    @GetMapping("/level-user-info")
    public ResponseEntity<APIResponse<List<ScProgramLevelDataDto>>>getLevelInfo(@RequestParam("participantId")  String participantId,
                                                                   @RequestParam("clientId") String clientId,
                                                                   @RequestParam("programId") Long programId) throws Exception {
        log.debug("REST request to get program levels : {} {}", clientId, participantId);
        APIResponse<List<ScProgramLevelDataDto>> res = programService.getLevelInfoByParticipantIdAndClientIdAndProgramId(clientId, participantId, programId);
        return ResponseEntity.ok().body(res);
    }

    @GetMapping(value = "/program-paths")
    public ResponseEntity<ListProgramPathsResponseDto> getPathsByClientIdAndParticipantId(
        @RequestParam("clientId") String clientId,
        @RequestParam("participantId") String participantId
    ) throws Exception {
        log.debug("REST request to get all Path by clientId: {}, participantId {}", clientId, participantId);
        APIResponse<ListProgramPathsResponseDto> dto = programService.getPathsByClientIdAndParticipantId(clientId, participantId);
        return ResponseEntity.status(dto.getHttpStatus()).body(dto.getResult());
    }

}
