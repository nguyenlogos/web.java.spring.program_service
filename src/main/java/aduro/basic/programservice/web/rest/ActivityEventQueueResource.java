package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.domain.ActivityEventQueue;
import aduro.basic.programservice.service.ActivityEventQueueService;
import aduro.basic.programservice.service.ActivityEventQueueQueryService;
import aduro.basic.programservice.service.ActivityEventQueueService;
import aduro.basic.programservice.service.dto.CheckEmployerVerifiedDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ActivityEventQueueDTO;
import aduro.basic.programservice.service.dto.ActivityEventQueueCriteria;
import aduro.basic.programservice.service.ActivityEventQueueQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link ActivityEventQueue}.
 */
@RestController
@RequestMapping("/api")
public class ActivityEventQueueResource {

    private final Logger log = LoggerFactory.getLogger(ActivityEventQueueResource.class);

    private static final String ENTITY_NAME = "activityEventQueue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ActivityEventQueueService activityEventQueueService;

    private final ActivityEventQueueQueryService activityEventQueueQueryService;

    public ActivityEventQueueResource(ActivityEventQueueService activityEventQueueService, ActivityEventQueueQueryService activityEventQueueQueryService) {
        this.activityEventQueueService = activityEventQueueService;
        this.activityEventQueueQueryService = activityEventQueueQueryService;
    }

    /**
     * {@code POST  /activity-event-queues} : Create a new activityEventQueue.
     *
     * @param activityEventQueueDTO the activityEventQueueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new activityEventQueueDTO, or with status {@code 400 (Bad Request)} if the activityEventQueue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/activity-event-queues")
    public ResponseEntity<ActivityEventQueueDTO> createActivityEventQueue(@Valid @RequestBody ActivityEventQueueDTO activityEventQueueDTO) throws URISyntaxException {
        log.debug("REST request to save ActivityEventQueue : {}", activityEventQueueDTO);
        ActivityEventQueueDTO result = activityEventQueueService.save(activityEventQueueDTO);
        return ResponseEntity.created(new URI("/api/activity-event-queues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }


    @PostMapping("/activity-event-queues/bulk-create")
    public void createActivityEventQueues(@Valid @RequestBody List<ActivityEventQueueDTO> activityEventQueueDTOs) throws URISyntaxException {
        log.debug("REST request to save ActivityEventQueue : {}", activityEventQueueDTOs);
        activityEventQueueService.saveList(activityEventQueueDTOs);
    }

    /**
     * {@code PUT  /activity-event-queues} : Updates an existing activityEventQueue.
     *
     * @param activityEventQueueDTO the activityEventQueueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated activityEventQueueDTO,
     * or with status {@code 400 (Bad Request)} if the activityEventQueueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the activityEventQueueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/activity-event-queues")
    public ResponseEntity<ActivityEventQueueDTO> updateActivityEventQueue(@Valid @RequestBody ActivityEventQueueDTO activityEventQueueDTO) throws URISyntaxException {
        log.debug("REST request to update ActivityEventQueue : {}", activityEventQueueDTO);
        if (activityEventQueueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ActivityEventQueueDTO result = activityEventQueueService.save(activityEventQueueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, activityEventQueueDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /activity-event-queues} : get all the activityEventQueues.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of activityEventQueues in body.
     */
    @GetMapping("/activity-event-queues")
    public ResponseEntity<List<ActivityEventQueueDTO>> getAllActivityEventQueues(ActivityEventQueueCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ActivityEventQueues by criteria: {}", criteria);
        Page<ActivityEventQueueDTO> page = activityEventQueueQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /activity-event-queues/count} : count all the activityEventQueues.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/activity-event-queues/count")
    public ResponseEntity<Long> countActivityEventQueues(ActivityEventQueueCriteria criteria) {
        log.debug("REST request to count ActivityEventQueues by criteria: {}", criteria);
        return ResponseEntity.ok().body(activityEventQueueQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /activity-event-queues/:id} : get the "id" activityEventQueue.
     *
     * @param id the id of the activityEventQueueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the activityEventQueueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/activity-event-queues/{id}")
    public ResponseEntity<ActivityEventQueueDTO> getActivityEventQueue(@PathVariable Long id) {
        log.debug("REST request to get ActivityEventQueue : {}", id);
        Optional<ActivityEventQueueDTO> activityEventQueueDTO = activityEventQueueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(activityEventQueueDTO);
    }

    /**
     * {@code DELETE  /activity-event-queues/:id} : delete the "id" activityEventQueue.
     *
     * @param id the id of the activityEventQueueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/activity-event-queues/{id}")
    public ResponseEntity<Void> deleteActivityEventQueue(@PathVariable Long id) {
        log.debug("REST request to delete ActivityEventQueue : {}", id);
        activityEventQueueService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/activity-event-queues/validate-employer-verified")
    public ResponseEntity<CheckEmployerVerifiedDTO> validateEmployerVierified(@Valid @RequestBody CheckEmployerVerifiedDTO checkEmployerVerifiedDTO) throws Exception {
        log.debug("REST request to validate ActivityEventQueue : {}", checkEmployerVerifiedDTO);

        CheckEmployerVerifiedDTO result = activityEventQueueService.validateEmployerVierified(checkEmployerVerifiedDTO);
        return ResponseEntity.created(new URI("/api/activity-event-queues/validate-employer-verified" + result.getClientId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getClientId()))
            .body(result);
    }

    @PostMapping("/activity-event-queues/trigger")
    public  ResponseEntity<String> triggerProcessEmployerVerified() throws Exception {
        log.debug("REST request to trigger ActivityEventQueue");

        new Thread(() -> {
            try {
                activityEventQueueService.processEmployerVierifiedActivity();
            } catch (Exception e) {
                e.printStackTrace();
                log.debug("Process Employer Verified Error: {}", e.getMessage());
            }
        }).start();

        return ResponseEntity.status(HttpStatus.OK).body("processing");
    }
}
