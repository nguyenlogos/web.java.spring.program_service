package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramCollectionService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramCollectionDTO;
import aduro.basic.programservice.service.dto.ProgramCollectionCriteria;
import aduro.basic.programservice.service.ProgramCollectionQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCollection}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCollectionResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCollectionResource.class);

    private static final String ENTITY_NAME = "programCollection";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCollectionService programCollectionService;

    private final ProgramCollectionQueryService programCollectionQueryService;

    public ProgramCollectionResource(ProgramCollectionService programCollectionService, ProgramCollectionQueryService programCollectionQueryService) {
        this.programCollectionService = programCollectionService;
        this.programCollectionQueryService = programCollectionQueryService;
    }

    /**
     * {@code POST  /program-collections} : Create a new programCollection.
     *
     * @param programCollectionDTO the programCollectionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCollectionDTO, or with status {@code 400 (Bad Request)} if the programCollection has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-collections")
    public ResponseEntity<ProgramCollectionDTO> createProgramCollection(@Valid @RequestBody ProgramCollectionDTO programCollectionDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramCollection : {}", programCollectionDTO);
        if (programCollectionDTO.getId() != null) {
            throw new BadRequestAlertException("A new programCollection cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramCollectionDTO result = programCollectionService.save(programCollectionDTO);
        return ResponseEntity.created(new URI("/api/program-collections/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-collections} : Updates an existing programCollection.
     *
     * @param programCollectionDTO the programCollectionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programCollectionDTO,
     * or with status {@code 400 (Bad Request)} if the programCollectionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programCollectionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-collections")
    public ResponseEntity<ProgramCollectionDTO> updateProgramCollection(@Valid @RequestBody ProgramCollectionDTO programCollectionDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramCollection : {}", programCollectionDTO);
        if (programCollectionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramCollectionDTO result = programCollectionService.save(programCollectionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programCollectionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-collections} : get all the programCollections.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCollections in body.
     */
    @GetMapping("/program-collections")
    public ResponseEntity<List<ProgramCollectionDTO>> getAllProgramCollections(ProgramCollectionCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramCollections by criteria: {}", criteria);
        Page<ProgramCollectionDTO> page = programCollectionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-collections/count} : count all the programCollections.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-collections/count")
    public ResponseEntity<Long> countProgramCollections(ProgramCollectionCriteria criteria) {
        log.debug("REST request to count ProgramCollections by criteria: {}", criteria);
        return ResponseEntity.ok().body(programCollectionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-collections/:id} : get the "id" programCollection.
     *
     * @param id the id of the programCollectionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCollectionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-collections/{id}")
    public ResponseEntity<ProgramCollectionDTO> getProgramCollection(@PathVariable Long id) {
        log.debug("REST request to get ProgramCollection : {}", id);
        Optional<ProgramCollectionDTO> programCollectionDTO = programCollectionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCollectionDTO);
    }

    /**
     * {@code DELETE  /program-collections/:id} : delete the "id" programCollection.
     *
     * @param id the id of the programCollectionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-collections/{id}")
    public ResponseEntity<Void> deleteProgramCollection(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCollection : {}", id);
        programCollectionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


}
