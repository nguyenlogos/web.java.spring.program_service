package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramSubCategoryConfigurationService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationDTO;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationCriteria;
import aduro.basic.programservice.service.ProgramSubCategoryConfigurationQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramSubCategoryConfiguration}.
 */
@RestController
@RequestMapping("/api")
public class ProgramSubCategoryConfigurationResource {

    private final Logger log = LoggerFactory.getLogger(ProgramSubCategoryConfigurationResource.class);

    private static final String ENTITY_NAME = "programSubCategoryConfiguration";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramSubCategoryConfigurationService programSubCategoryConfigurationService;

    private final ProgramSubCategoryConfigurationQueryService programSubCategoryConfigurationQueryService;

    public ProgramSubCategoryConfigurationResource(ProgramSubCategoryConfigurationService programSubCategoryConfigurationService, ProgramSubCategoryConfigurationQueryService programSubCategoryConfigurationQueryService) {
        this.programSubCategoryConfigurationService = programSubCategoryConfigurationService;
        this.programSubCategoryConfigurationQueryService = programSubCategoryConfigurationQueryService;
    }

    /**
     * {@code POST  /program-sub-category-configurations} : Create a new programSubCategoryConfiguration.
     *
     * @param programSubCategoryConfigurationDTO the programSubCategoryConfigurationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programSubCategoryConfigurationDTO, or with status {@code 400 (Bad Request)} if the programSubCategoryConfiguration has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-sub-category-configurations")
    public ResponseEntity<ProgramSubCategoryConfigurationDTO> createProgramSubCategoryConfiguration(@Valid @RequestBody ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramSubCategoryConfiguration : {}", programSubCategoryConfigurationDTO);
        if (programSubCategoryConfigurationDTO.getId() != null) {
            throw new BadRequestAlertException("A new programSubCategoryConfiguration cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramSubCategoryConfigurationDTO result = programSubCategoryConfigurationService.save(programSubCategoryConfigurationDTO);
        return ResponseEntity.created(new URI("/api/program-sub-category-configurations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-sub-category-configurations} : Updates an existing programSubCategoryConfiguration.
     *
     * @param programSubCategoryConfigurationDTO the programSubCategoryConfigurationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programSubCategoryConfigurationDTO,
     * or with status {@code 400 (Bad Request)} if the programSubCategoryConfigurationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programSubCategoryConfigurationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-sub-category-configurations")
    public ResponseEntity<ProgramSubCategoryConfigurationDTO> updateProgramSubCategoryConfiguration(@Valid @RequestBody ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramSubCategoryConfiguration : {}", programSubCategoryConfigurationDTO);
        if (programSubCategoryConfigurationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramSubCategoryConfigurationDTO result = programSubCategoryConfigurationService.save(programSubCategoryConfigurationDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programSubCategoryConfigurationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-sub-category-configurations} : get all the programSubCategoryConfigurations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programSubCategoryConfigurations in body.
     */
    @GetMapping("/program-sub-category-configurations")
    public ResponseEntity<List<ProgramSubCategoryConfigurationDTO>> getAllProgramSubCategoryConfigurations(ProgramSubCategoryConfigurationCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramSubCategoryConfigurations by criteria: {}", criteria);
        Page<ProgramSubCategoryConfigurationDTO> page = programSubCategoryConfigurationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-sub-category-configurations/count} : count all the programSubCategoryConfigurations.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-sub-category-configurations/count")
    public ResponseEntity<Long> countProgramSubCategoryConfigurations(ProgramSubCategoryConfigurationCriteria criteria) {
        log.debug("REST request to count ProgramSubCategoryConfigurations by criteria: {}", criteria);
        return ResponseEntity.ok().body(programSubCategoryConfigurationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-sub-category-configurations/:id} : get the "id" programSubCategoryConfiguration.
     *
     * @param id the id of the programSubCategoryConfigurationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programSubCategoryConfigurationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-sub-category-configurations/{id}")
    public ResponseEntity<ProgramSubCategoryConfigurationDTO> getProgramSubCategoryConfiguration(@PathVariable Long id) {
        log.debug("REST request to get ProgramSubCategoryConfiguration : {}", id);
        Optional<ProgramSubCategoryConfigurationDTO> programSubCategoryConfigurationDTO = programSubCategoryConfigurationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programSubCategoryConfigurationDTO);
    }

    /**
     * {@code DELETE  /program-sub-category-configurations/:id} : delete the "id" programSubCategoryConfiguration.
     *
     * @param id the id of the programSubCategoryConfigurationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-sub-category-configurations/{id}")
    public ResponseEntity<Void> deleteProgramSubCategoryConfiguration(@PathVariable Long id) {
        log.debug("REST request to delete ProgramSubCategoryConfiguration : {}", id);
        programSubCategoryConfigurationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
