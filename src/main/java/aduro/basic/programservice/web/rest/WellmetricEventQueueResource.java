package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.WellmetricEventQueueService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.WellmetricEventQueueDTO;
import aduro.basic.programservice.service.dto.WellmetricEventQueueCriteria;
import aduro.basic.programservice.service.WellmetricEventQueueQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.WellmetricEventQueue}.
 */
@RestController
@RequestMapping("/api")
public class WellmetricEventQueueResource {

    private final Logger log = LoggerFactory.getLogger(WellmetricEventQueueResource.class);

    private static final String ENTITY_NAME = "wellmetricEventQueue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WellmetricEventQueueService wellmetricEventQueueService;

    private final WellmetricEventQueueQueryService wellmetricEventQueueQueryService;

    public WellmetricEventQueueResource(WellmetricEventQueueService wellmetricEventQueueService, WellmetricEventQueueQueryService wellmetricEventQueueQueryService) {
        this.wellmetricEventQueueService = wellmetricEventQueueService;
        this.wellmetricEventQueueQueryService = wellmetricEventQueueQueryService;
    }

    /**
     * {@code POST  /wellmetric-event-queues} : Create a new wellmetricEventQueue.
     *
     * @param wellmetricEventQueueDTO the wellmetricEventQueueDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new wellmetricEventQueueDTO, or with status {@code 400 (Bad Request)} if the wellmetricEventQueue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/wellmetric-event-queues")
    public ResponseEntity<WellmetricEventQueueDTO> createWellmetricEventQueue(@Valid @RequestBody WellmetricEventQueueDTO wellmetricEventQueueDTO) throws URISyntaxException {
        log.debug("REST request to save WellmetricEventQueue : {}", wellmetricEventQueueDTO);
        if (wellmetricEventQueueDTO.getId() != null) {
            throw new BadRequestAlertException("A new wellmetricEventQueue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WellmetricEventQueueDTO result = wellmetricEventQueueService.save(wellmetricEventQueueDTO);
        return ResponseEntity.created(new URI("/api/wellmetric-event-queues/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /wellmetric-event-queues} : Updates an existing wellmetricEventQueue.
     *
     * @param wellmetricEventQueueDTO the wellmetricEventQueueDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated wellmetricEventQueueDTO,
     * or with status {@code 400 (Bad Request)} if the wellmetricEventQueueDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the wellmetricEventQueueDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/wellmetric-event-queues")
    public ResponseEntity<WellmetricEventQueueDTO> updateWellmetricEventQueue(@Valid @RequestBody WellmetricEventQueueDTO wellmetricEventQueueDTO) throws URISyntaxException {
        log.debug("REST request to update WellmetricEventQueue : {}", wellmetricEventQueueDTO);
        if (wellmetricEventQueueDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        WellmetricEventQueueDTO result = wellmetricEventQueueService.save(wellmetricEventQueueDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, wellmetricEventQueueDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /wellmetric-event-queues} : get all the wellmetricEventQueues.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of wellmetricEventQueues in body.
     */
    @GetMapping("/wellmetric-event-queues")
    public ResponseEntity<List<WellmetricEventQueueDTO>> getAllWellmetricEventQueues(WellmetricEventQueueCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get WellmetricEventQueues by criteria: {}", criteria);
        Page<WellmetricEventQueueDTO> page = wellmetricEventQueueQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /wellmetric-event-queues/count} : count all the wellmetricEventQueues.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/wellmetric-event-queues/count")
    public ResponseEntity<Long> countWellmetricEventQueues(WellmetricEventQueueCriteria criteria) {
        log.debug("REST request to count WellmetricEventQueues by criteria: {}", criteria);
        return ResponseEntity.ok().body(wellmetricEventQueueQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /wellmetric-event-queues/:id} : get the "id" wellmetricEventQueue.
     *
     * @param id the id of the wellmetricEventQueueDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the wellmetricEventQueueDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/wellmetric-event-queues/{id}")
    public ResponseEntity<WellmetricEventQueueDTO> getWellmetricEventQueue(@PathVariable Long id) {
        log.debug("REST request to get WellmetricEventQueue : {}", id);
        Optional<WellmetricEventQueueDTO> wellmetricEventQueueDTO = wellmetricEventQueueService.findOne(id);
        return ResponseUtil.wrapOrNotFound(wellmetricEventQueueDTO);
    }

    /**
     * {@code DELETE  /wellmetric-event-queues/:id} : delete the "id" wellmetricEventQueue.
     *
     * @param id the id of the wellmetricEventQueueDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/wellmetric-event-queues/{id}")
    public ResponseEntity<Void> deleteWellmetricEventQueue(@PathVariable Long id) {
        log.debug("REST request to delete WellmetricEventQueue : {}", id);
        wellmetricEventQueueService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
