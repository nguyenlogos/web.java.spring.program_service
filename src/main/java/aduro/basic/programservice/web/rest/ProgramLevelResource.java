package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramLevelService;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.ProgramLevelInfo;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import aduro.basic.programservice.service.dto.ProgramLevelCriteria;
import aduro.basic.programservice.service.ProgramLevelQueryService;

import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.*;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramLevel}.
 */
@RestController
@RequestMapping("/api")
public class ProgramLevelResource {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelResource.class);

    private static final String ENTITY_NAME = "programLevel";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramLevelService programLevelService;

    private final ProgramLevelQueryService programLevelQueryService;

    private final ProgramService programService;

    public ProgramLevelResource(ProgramService programService, ProgramLevelService programLevelService, ProgramLevelQueryService programLevelQueryService) {
        this.programService = programService;
        this.programLevelService = programLevelService;
        this.programLevelQueryService = programLevelQueryService;
    }

    /**
     * {@code POST  /program-levels} : Create a new programLevel.
     *
     * @param programLevelDTO the programLevelDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programLevelDTO, or with status {@code 400 (Bad Request)} if the programLevel has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-levels")
    public ResponseEntity<ProgramLevelDTO> createProgramLevel(@RequestBody ProgramLevelDTO programLevelDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramLevel : {}", programLevelDTO);
        if (programLevelDTO.getId() != null) {
            throw new BadRequestAlertException("A new level cannot already have an ID", ENTITY_NAME, "idexists");
        }

        // Validate business rules
      //  ProgramLevelBusinessRule businessValidator = new ProgramLevelBusinessRule(this.programService, this.programLevelService, this.programLevelQueryService);
      //  programLevelDTO = businessValidator.validateOnCreate(programLevelDTO);

        ProgramLevelDTO result = programLevelService.save(programLevelDTO);
        return ResponseEntity.created(new URI("/api/program-levels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

   /* @PostMapping("/program-levels/{programId}/update-levels")
    public ResponseEntity<List<ProgramLevelDTO>> updateAllLevels(@PathVariable Long programId,@RequestBody List<ProgramLevelDTO> programLevelDTOS) throws URISyntaxException {
        log.debug("REST request to save ProgramLevel : {}", programLevelDTOS);

        List<ProgramLevelDTO> result = programLevelService.updateAllLevels(programId, programLevelDTOS);

        return ResponseEntity.created(new URI("/api/program-levels/" + programId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programId.toString()))
            .body(result);
    }*/



    /**
     * {@code PUT  /program-levels} : Updates an existing programLevel.
     *
     * @param programLevelDTO the programLevelDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programLevelDTO,
     * or with status {@code 400 (Bad Request)} if the programLevelDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programLevelDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-levels")
    public ResponseEntity<ProgramLevelDTO> updateProgramLevel(@RequestBody ProgramLevelDTO programLevelDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramLevel : {}", programLevelDTO);
        if (programLevelDTO.getId() == null) {
            throw new BadRequestAlertException("Null is invalid id", ENTITY_NAME, "idnull");
        }

        ProgramLevelDTO result = programLevelService.save(programLevelDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programLevelDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-levels} : get all the programLevels
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programLevels in body.
     */
    @GetMapping("/program-levels")
    public ResponseEntity<List<ProgramLevelDTO>> getAllProgramLevels(ProgramLevelCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramLevels by criteria: {}", criteria);
        Page<ProgramLevelDTO> page = programLevelQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /program-levels/count} : count all the programLevels.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/program-levels/count")
    public ResponseEntity<Long> countProgramLevels(ProgramLevelCriteria criteria) {
        log.debug("REST request to count ProgramLevels by criteria: {}", criteria);
        return ResponseEntity.ok().body(programLevelQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-levels/:id} : get the "id" programLevel.
     *
     * @param id the id of the programLevelDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programLevelDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-levels/{id}")
    public ResponseEntity<ProgramLevelDTO> getProgramLevel(@PathVariable Long id) {
        log.debug("REST request to get ProgramLevel : {}", id);
        Optional<ProgramLevelDTO> programLevelDTO = programLevelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programLevelDTO);
    }

    /**
     * {@code DELETE  /program-levels/:id} : delete the "id" programLevel.
     *
     * @param id the id of the programLevelDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-levels/{id}")
    public ResponseEntity<Void> deleteProgramLevel(@PathVariable Long id) {
        log.debug("REST request to delete ProgramLevel : {}", id);
        programLevelService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /programs/{progId}/program-levels} : get all the programLevels of program.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programLevels in body.
     */
    @GetMapping("/programs/{progId}/program-levels")
    public ResponseEntity<List<ProgramLevelDTO>> getProgramLevels(@PathVariable Long progId, ProgramLevelCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramLevels by criteria: {}", criteria);
        // Override program id filter to only query based on a specific program id
        LongFilter progIdFilter = new LongFilter();
        progIdFilter.setEquals(progId);
        criteria.setProgramId(progIdFilter);
        Page<ProgramLevelDTO> page = programLevelQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping("/program-levels/{programId}/update-levels")
    public ResponseEntity<ProgramLevelInfo> updateProgramLevelInfo(@PathVariable Long programId, @RequestBody ProgramLevelInfo programLevelInfo) throws URISyntaxException {
        log.debug("REST request to save ProgramLevel : {}", programLevelInfo);

        ProgramLevelInfo rs = programLevelService.updateProgramLevelInfo(programId, programLevelInfo);

        return ResponseEntity.created(new URI("/api/program-levels/" + programId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programId.toString()))
            .body(rs);
    }
}
