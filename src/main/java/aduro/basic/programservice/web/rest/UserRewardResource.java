package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.UserRewardService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.UserRewardDTO;
import aduro.basic.programservice.service.dto.UserRewardCriteria;
import aduro.basic.programservice.service.UserRewardQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.UserReward}.
 */
@RestController
@RequestMapping("/api")
public class UserRewardResource {

    private final Logger log = LoggerFactory.getLogger(UserRewardResource.class);

    private static final String ENTITY_NAME = "userReward";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserRewardService userRewardService;

    private final UserRewardQueryService userRewardQueryService;

    public UserRewardResource(UserRewardService userRewardService, UserRewardQueryService userRewardQueryService) {
        this.userRewardService = userRewardService;
        this.userRewardQueryService = userRewardQueryService;
    }

    /**
     * {@code POST  /user-rewards} : Create a new userReward.
     *
     * @param userRewardDTO the userRewardDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userRewardDTO, or with status {@code 400 (Bad Request)} if the userReward has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-rewards")
    public ResponseEntity<UserRewardDTO> createUserReward(@Valid @RequestBody UserRewardDTO userRewardDTO) throws URISyntaxException {
        log.debug("REST request to save UserReward : {}", userRewardDTO);
        if (userRewardDTO.getId() != null) {
            throw new BadRequestAlertException("A new userReward cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserRewardDTO result = userRewardService.save(userRewardDTO);
        return ResponseEntity.created(new URI("/api/user-rewards/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-rewards} : Updates an existing userReward.
     *
     * @param userRewardDTO the userRewardDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userRewardDTO,
     * or with status {@code 400 (Bad Request)} if the userRewardDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userRewardDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-rewards")
    public ResponseEntity<UserRewardDTO> updateUserReward(@Valid @RequestBody UserRewardDTO userRewardDTO) throws URISyntaxException {
        log.debug("REST request to update UserReward : {}", userRewardDTO);
        if (userRewardDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        UserRewardDTO result = userRewardService.save(userRewardDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userRewardDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /user-rewards} : get all the userRewards.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userRewards in body.
     */
    @GetMapping("/user-rewards")
    public ResponseEntity<List<UserRewardDTO>> getAllUserRewards(UserRewardCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get UserRewards by criteria: {}", criteria);
        Page<UserRewardDTO> page = userRewardQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /user-rewards/count} : count all the userRewards.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/user-rewards/count")
    public ResponseEntity<Long> countUserRewards(UserRewardCriteria criteria) {
        log.debug("REST request to count UserRewards by criteria: {}", criteria);
        return ResponseEntity.ok().body(userRewardQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /user-rewards/:id} : get the "id" userReward.
     *
     * @param id the id of the userRewardDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userRewardDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-rewards/{id}")
    public ResponseEntity<UserRewardDTO> getUserReward(@PathVariable Long id) {
        log.debug("REST request to get UserReward : {}", id);
        Optional<UserRewardDTO> userRewardDTO = userRewardService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userRewardDTO);
    }

    /**
     * {@code DELETE  /user-rewards/:id} : delete the "id" userReward.
     *
     * @param id the id of the userRewardDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-rewards/{id}")
    public ResponseEntity<Void> deleteUserReward(@PathVariable Long id) {
        log.debug("REST request to delete UserReward : {}", id);
        userRewardService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


}
