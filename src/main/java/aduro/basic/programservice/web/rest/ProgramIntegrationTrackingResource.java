package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramIntegrationTrackingService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingDTO;
import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingCriteria;
import aduro.basic.programservice.service.ProgramIntegrationTrackingQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramIntegrationTracking}.
 */
@RestController
@RequestMapping("/api")
public class ProgramIntegrationTrackingResource {

    private final Logger log = LoggerFactory.getLogger(ProgramIntegrationTrackingResource.class);

    private static final String ENTITY_NAME = "programIntegrationTracking";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramIntegrationTrackingService programIntegrationTrackingService;

    private final ProgramIntegrationTrackingQueryService programIntegrationTrackingQueryService;

    public ProgramIntegrationTrackingResource(ProgramIntegrationTrackingService programIntegrationTrackingService, ProgramIntegrationTrackingQueryService programIntegrationTrackingQueryService) {
        this.programIntegrationTrackingService = programIntegrationTrackingService;
        this.programIntegrationTrackingQueryService = programIntegrationTrackingQueryService;
    }

    /**
     * {@code POST  /program-integration-trackings} : Create a new programIntegrationTracking.
     *
     * @param programIntegrationTrackingDTO the programIntegrationTrackingDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programIntegrationTrackingDTO, or with status {@code 400 (Bad Request)} if the programIntegrationTracking has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-integration-trackings")
    public ResponseEntity<ProgramIntegrationTrackingDTO> createProgramIntegrationTracking(@RequestBody ProgramIntegrationTrackingDTO programIntegrationTrackingDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramIntegrationTracking : {}", programIntegrationTrackingDTO);
        if (programIntegrationTrackingDTO.getId() != null) {
            throw new BadRequestAlertException("A new programIntegrationTracking cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramIntegrationTrackingDTO result = programIntegrationTrackingService.save(programIntegrationTrackingDTO);
        return ResponseEntity.created(new URI("/api/program-integration-trackings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-integration-trackings} : Updates an existing programIntegrationTracking.
     *
     * @param programIntegrationTrackingDTO the programIntegrationTrackingDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programIntegrationTrackingDTO,
     * or with status {@code 400 (Bad Request)} if the programIntegrationTrackingDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programIntegrationTrackingDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-integration-trackings")
    public ResponseEntity<ProgramIntegrationTrackingDTO> updateProgramIntegrationTracking(@RequestBody ProgramIntegrationTrackingDTO programIntegrationTrackingDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramIntegrationTracking : {}", programIntegrationTrackingDTO);
        if (programIntegrationTrackingDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramIntegrationTrackingDTO result = programIntegrationTrackingService.save(programIntegrationTrackingDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programIntegrationTrackingDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-integration-trackings} : get all the programIntegrationTrackings.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programIntegrationTrackings in body.
     */
    @GetMapping("/program-integration-trackings")
    public ResponseEntity<List<ProgramIntegrationTrackingDTO>> getAllProgramIntegrationTrackings(ProgramIntegrationTrackingCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramIntegrationTrackings by criteria: {}", criteria);
        Page<ProgramIntegrationTrackingDTO> page = programIntegrationTrackingQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-integration-trackings/count} : count all the programIntegrationTrackings.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-integration-trackings/count")
    public ResponseEntity<Long> countProgramIntegrationTrackings(ProgramIntegrationTrackingCriteria criteria) {
        log.debug("REST request to count ProgramIntegrationTrackings by criteria: {}", criteria);
        return ResponseEntity.ok().body(programIntegrationTrackingQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-integration-trackings/:id} : get the "id" programIntegrationTracking.
     *
     * @param id the id of the programIntegrationTrackingDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programIntegrationTrackingDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-integration-trackings/{id}")
    public ResponseEntity<ProgramIntegrationTrackingDTO> getProgramIntegrationTracking(@PathVariable Long id) {
        log.debug("REST request to get ProgramIntegrationTracking : {}", id);
        Optional<ProgramIntegrationTrackingDTO> programIntegrationTrackingDTO = programIntegrationTrackingService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programIntegrationTrackingDTO);
    }

    /**
     * {@code DELETE  /program-integration-trackings/:id} : delete the "id" programIntegrationTracking.
     *
     * @param id the id of the programIntegrationTrackingDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-integration-trackings/{id}")
    public ResponseEntity<Void> deleteProgramIntegrationTracking(@PathVariable Long id) {
        log.debug("REST request to delete ProgramIntegrationTracking : {}", id);
        programIntegrationTrackingService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
