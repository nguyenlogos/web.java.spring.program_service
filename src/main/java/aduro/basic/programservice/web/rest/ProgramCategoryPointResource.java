package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramCategoryPointService;
import aduro.basic.programservice.service.dto.ProgramLevelActivityDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramCategoryPointDTO;
import aduro.basic.programservice.service.dto.ProgramCategoryPointCriteria;
import aduro.basic.programservice.service.ProgramCategoryPointQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCategoryPoint}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCategoryPointResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCategoryPointResource.class);

    private static final String ENTITY_NAME = "programCategoryPoint";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCategoryPointService programCategoryPointService;

    private final ProgramCategoryPointQueryService programCategoryPointQueryService;

    public ProgramCategoryPointResource(ProgramCategoryPointService programCategoryPointService, ProgramCategoryPointQueryService programCategoryPointQueryService) {
        this.programCategoryPointService = programCategoryPointService;
        this.programCategoryPointQueryService = programCategoryPointQueryService;
    }

    /**
     * {@code POST  /program-category-points} : Create a new programCategoryPoint.
     *
     * @param programCategoryPointDTO the programCategoryPointDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCategoryPointDTO, or with status {@code 400 (Bad Request)} if the programCategoryPoint has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-category-points")
    public ResponseEntity<ProgramCategoryPointDTO> createProgramCategoryPoint(@Valid @RequestBody ProgramCategoryPointDTO programCategoryPointDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramCategoryPoint : {}", programCategoryPointDTO);
        if (programCategoryPointDTO.getId() != null &&   programCategoryPointDTO.getId() != 0) {
            throw new BadRequestAlertException("A new programCategoryPoint cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramCategoryPointDTO result = programCategoryPointService.save(programCategoryPointDTO);
        return ResponseEntity.created(new URI("/api/program-category-points/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-category-points} : Updates an existing programCategoryPoint.
     *
     * @param programCategoryPointDTO the programCategoryPointDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programCategoryPointDTO,
     * or with status {@code 400 (Bad Request)} if the programCategoryPointDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programCategoryPointDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-category-points")
    public ResponseEntity<ProgramCategoryPointDTO> updateProgramCategoryPoint(@Valid @RequestBody ProgramCategoryPointDTO programCategoryPointDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramCategoryPoint : {}", programCategoryPointDTO);
        if (programCategoryPointDTO.getId() == null || programCategoryPointDTO.getId() == 0) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramCategoryPointDTO result = programCategoryPointService.save(programCategoryPointDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programCategoryPointDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-category-points} : get all the programCategoryPoints.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCategoryPoints in body.
     */
    @GetMapping("/program-category-points")
    public ResponseEntity<List<ProgramCategoryPointDTO>> getAllProgramCategoryPoints(ProgramCategoryPointCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramCategoryPoints by criteria: {}", criteria);
        Page<ProgramCategoryPointDTO> page = programCategoryPointQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-category-points/count} : count all the programCategoryPoints.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-category-points/count")
    public ResponseEntity<Long> countProgramCategoryPoints(ProgramCategoryPointCriteria criteria) {
        log.debug("REST request to count ProgramCategoryPoints by criteria: {}", criteria);
        return ResponseEntity.ok().body(programCategoryPointQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-category-points/:id} : get the "id" programCategoryPoint.
     *
     * @param id the id of the programCategoryPointDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCategoryPointDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-category-points/{id}")
    public ResponseEntity<ProgramCategoryPointDTO> getProgramCategoryPoint(@PathVariable Long id) {
        log.debug("REST request to get ProgramCategoryPoint : {}", id);
        Optional<ProgramCategoryPointDTO> programCategoryPointDTO = programCategoryPointService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCategoryPointDTO);
    }

    @GetMapping("/program-category-points/{programId}/{categoryCode}")
    public ResponseEntity<ProgramCategoryPointDTO> getProgramCategoryPoint(@PathVariable Long programId, @PathVariable String categoryCode) {
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointService.getProgramCategoryPointByProgramId(programId, categoryCode);
        return ResponseEntity.ok().body(programCategoryPointDTO);
    }

    /**
     * {@code DELETE  /program-category-points/:id} : delete the "id" programCategoryPoint.
     *
     * @param id the id of the programCategoryPointDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-category-points/{id}")
    public ResponseEntity<Void> deleteProgramCategoryPoint(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCategoryPoint : {}", id);
        programCategoryPointService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code POST  /program-category-points} : Create a new programCategoryPoint.
     *
     * @param programCategoryPointDTOs the programCategoryPointDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCategoryPointDTO, or with status {@code 400 (Bad Request)} if the programCategoryPoint has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-category-points/{programId}/update-categories")
    public ResponseEntity<List<ProgramCategoryPointDTO>> UpdateProgramCategoryPoint(@PathVariable Long programId, @RequestBody List<ProgramCategoryPointDTO> programCategoryPointDTOs) throws URISyntaxException {
        log.debug("REST request to save ProgramCategoryPoint : {}", programCategoryPointDTOs);

        List<ProgramCategoryPointDTO> result = programCategoryPointService.updateProgramCategoryPoints(programId, programCategoryPointDTOs);

        return ResponseEntity.created(new URI("/api/program-category-points/" + programId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programId.toString()))
            .body(result);
    }

    /**
     * {@code POST  /program-category-points} : Create a new programCategoryPoint.
     *
     * @param programCategoryPointDTOs the programCategoryPointDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCategoryPointDTO, or with status {@code 400 (Bad Request)} if the programCategoryPoint has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-category-points/{programId}/add-categories")
    public ResponseEntity<List<ProgramCategoryPointDTO>> AddProgramCategoryPoints(@PathVariable Long programId, @RequestBody List<ProgramCategoryPointDTO> programCategoryPointDTOs) throws URISyntaxException {
        log.debug("REST request to save ProgramCategoryPoint : {}", programCategoryPointDTOs);

        List<ProgramCategoryPointDTO> result = programCategoryPointService.addAndRemoveProgramCategoryPoints(programId, programCategoryPointDTOs);

        return ResponseEntity.created(new URI("/api/program-category-points/" + programId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programId.toString()))
            .body(result);
    }

    @GetMapping("/program-category-points/get-by-client-id/{clientId}/{categoryCode}")
    public ResponseEntity<ProgramCategoryPointDTO> getProgramCategoryPointByClientIdAnCategory(@PathVariable String clientId,@PathVariable String categoryCode) {
        log.debug("REST request to get ProgramCategoryPoint : {}", clientId + categoryCode);
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointService.getProgramCategoryPointByClientIdAnCategory(clientId, categoryCode);
        return ResponseEntity.ok().body(programCategoryPointDTO);
    }

    @GetMapping("/program-category-points/get-by-user/{clientId}/{participantId}/{categoryCode}")
    public ResponseEntity<ProgramCategoryPointDTO> getProgramCategoryPointByUser(@PathVariable String clientId,@PathVariable String participantId, @PathVariable String categoryCode, @RequestParam Map<String, String> queryParams) {
        log.debug("REST request to get ProgramCategoryPoint : {}", clientId + categoryCode);
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointService.getProgramCategoryPointByUser(clientId, participantId, categoryCode, queryParams);
        return ResponseEntity.ok().body(programCategoryPointDTO);
    }


}
