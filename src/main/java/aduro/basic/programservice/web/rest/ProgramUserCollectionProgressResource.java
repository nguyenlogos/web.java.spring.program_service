package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramUserCollectionProgressService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressDTO;
import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressCriteria;
import aduro.basic.programservice.service.ProgramUserCollectionProgressQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramUserCollectionProgress}.
 */
@RestController
@RequestMapping("/api")
public class ProgramUserCollectionProgressResource {

    private final Logger log = LoggerFactory.getLogger(ProgramUserCollectionProgressResource.class);

    private static final String ENTITY_NAME = "programUserCollectionProgress";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramUserCollectionProgressService programUserCollectionProgressService;

    private final ProgramUserCollectionProgressQueryService programUserCollectionProgressQueryService;

    public ProgramUserCollectionProgressResource(ProgramUserCollectionProgressService programUserCollectionProgressService, ProgramUserCollectionProgressQueryService programUserCollectionProgressQueryService) {
        this.programUserCollectionProgressService = programUserCollectionProgressService;
        this.programUserCollectionProgressQueryService = programUserCollectionProgressQueryService;
    }

    /**
     * {@code POST  /program-user-collection-progresses} : Create a new programUserCollectionProgress.
     *
     * @param programUserCollectionProgressDTO the programUserCollectionProgressDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programUserCollectionProgressDTO, or with status {@code 400 (Bad Request)} if the programUserCollectionProgress has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-user-collection-progresses")
    public ResponseEntity<ProgramUserCollectionProgressDTO> createProgramUserCollectionProgress(@Valid @RequestBody ProgramUserCollectionProgressDTO programUserCollectionProgressDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramUserCollectionProgress : {}", programUserCollectionProgressDTO);
        if (programUserCollectionProgressDTO.getId() != null) {
            throw new BadRequestAlertException("A new programUserCollectionProgress cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramUserCollectionProgressDTO result = programUserCollectionProgressService.save(programUserCollectionProgressDTO);
        return ResponseEntity.created(new URI("/api/program-user-collection-progresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-user-collection-progresses} : Updates an existing programUserCollectionProgress.
     *
     * @param programUserCollectionProgressDTO the programUserCollectionProgressDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programUserCollectionProgressDTO,
     * or with status {@code 400 (Bad Request)} if the programUserCollectionProgressDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programUserCollectionProgressDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-user-collection-progresses")
    public ResponseEntity<ProgramUserCollectionProgressDTO> updateProgramUserCollectionProgress(@Valid @RequestBody ProgramUserCollectionProgressDTO programUserCollectionProgressDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramUserCollectionProgress : {}", programUserCollectionProgressDTO);
        if (programUserCollectionProgressDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramUserCollectionProgressDTO result = programUserCollectionProgressService.save(programUserCollectionProgressDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programUserCollectionProgressDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-user-collection-progresses} : get all the programUserCollectionProgresses.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programUserCollectionProgresses in body.
     */
    @GetMapping("/program-user-collection-progresses")
    public ResponseEntity<List<ProgramUserCollectionProgressDTO>> getAllProgramUserCollectionProgresses(ProgramUserCollectionProgressCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramUserCollectionProgresses by criteria: {}", criteria);
        Page<ProgramUserCollectionProgressDTO> page = programUserCollectionProgressQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-user-collection-progresses/count} : count all the programUserCollectionProgresses.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-user-collection-progresses/count")
    public ResponseEntity<Long> countProgramUserCollectionProgresses(ProgramUserCollectionProgressCriteria criteria) {
        log.debug("REST request to count ProgramUserCollectionProgresses by criteria: {}", criteria);
        return ResponseEntity.ok().body(programUserCollectionProgressQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-user-collection-progresses/:id} : get the "id" programUserCollectionProgress.
     *
     * @param id the id of the programUserCollectionProgressDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programUserCollectionProgressDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-user-collection-progresses/{id}")
    public ResponseEntity<ProgramUserCollectionProgressDTO> getProgramUserCollectionProgress(@PathVariable Long id) {
        log.debug("REST request to get ProgramUserCollectionProgress : {}", id);
        Optional<ProgramUserCollectionProgressDTO> programUserCollectionProgressDTO = programUserCollectionProgressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programUserCollectionProgressDTO);
    }

    /**
     * {@code DELETE  /program-user-collection-progresses/:id} : delete the "id" programUserCollectionProgress.
     *
     * @param id the id of the programUserCollectionProgressDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-user-collection-progresses/{id}")
    public ResponseEntity<Void> deleteProgramUserCollectionProgress(@PathVariable Long id) {
        log.debug("REST request to delete ProgramUserCollectionProgress : {}", id);
        programUserCollectionProgressService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
