package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.TermAndConditionService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.TermAndConditionDTO;
import aduro.basic.programservice.service.dto.TermAndConditionRequest;
import aduro.basic.programservice.service.dto.TermAndConditionCriteria;
import aduro.basic.programservice.service.TermAndConditionQueryService;
import aduro.basic.programservice.service.ProgramMemberConsentDetailService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static aduro.basic.programservice.service.util.RandomUtil.isNullOrEmpty;
import static org.hibernate.id.IdentifierGenerator.ENTITY_NAME;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.TermAndCondition}.
 */
@RestController
@RequestMapping("/api")
public class TermAndConditionResource {

    private final Logger log = LoggerFactory.getLogger(TermAndConditionResource.class);

    private static final String ENTITY_NAME = "termAndCondition";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TermAndConditionService termAndConditionService;

    private final TermAndConditionQueryService termAndConditionQueryService;

    public TermAndConditionResource(TermAndConditionService termAndConditionService, TermAndConditionQueryService termAndConditionQueryService) {
        this.termAndConditionService = termAndConditionService;
        this.termAndConditionQueryService = termAndConditionQueryService;
    }

    /**
     * {@code POST  /term-and-conditions} : Create a new termAndCondition.
     *
     * @param termAndConditionDTO the termAndConditionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new termAndConditionDTO, or with status {@code 400 (Bad Request)} if the termAndCondition has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/term-and-conditions")
    public ResponseEntity<TermAndConditionDTO> createTermAndCondition(@Valid @RequestBody TermAndConditionDTO termAndConditionDTO) throws URISyntaxException {
        log.debug("REST request to save TermAndCondition : {}", termAndConditionDTO);
        if (termAndConditionDTO.getId() != null) {
            throw new BadRequestAlertException("A new termAndCondition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TermAndConditionDTO result = termAndConditionService.save(termAndConditionDTO);
        return ResponseEntity.created(new URI("/api/term-and-conditions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /term-and-conditions} : Updates an existing termAndCondition.
     *
     * @param termAndConditionDTO the termAndConditionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated termAndConditionDTO,
     * or with status {@code 400 (Bad Request)} if the termAndConditionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the termAndConditionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/term-and-conditions")
    public ResponseEntity<TermAndConditionDTO> updateTermAndCondition(@Valid @RequestBody TermAndConditionDTO termAndConditionDTO) throws URISyntaxException {
        log.debug("REST request to update TermAndCondition : {}", termAndConditionDTO);
        if (termAndConditionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        TermAndConditionDTO result = termAndConditionService.save(termAndConditionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, termAndConditionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /term-and-conditions} : get all the termAndConditions.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of termAndConditions in body.
     */
    @GetMapping("/term-and-conditions")
    public ResponseEntity<List<TermAndConditionDTO>> getAllTermAndConditions(TermAndConditionCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get TermAndConditions by criteria: {}", criteria);
        Page<TermAndConditionDTO> page = termAndConditionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    /**
    * {@code GET  /term-and-conditions/count} : count all the termAndConditions.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/term-and-conditions/count")
    public ResponseEntity<Long> countTermAndConditions(TermAndConditionCriteria criteria) {
        log.debug("REST request to count TermAndConditions by criteria: {}", criteria);
        return ResponseEntity.ok().body(termAndConditionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /term-and-conditions/:id} : get the "id" termAndCondition.
     *
     * @param id the id of the termAndConditionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the termAndConditionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/term-and-conditions/{id}")
    public ResponseEntity<TermAndConditionDTO> getTermAndCondition(@PathVariable Long id) {
        log.debug("REST request to get TermAndCondition : {}", id);
        Optional<TermAndConditionDTO> termAndConditionDTO = termAndConditionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(termAndConditionDTO);
    }

    /**
     * {@code DELETE  /term-and-conditions/:id} : delete the "id" termAndCondition.
     *
     * @param id the id of the termAndConditionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/term-and-conditions/{id}")
    public ResponseEntity<Void> deleteTermAndCondition(@PathVariable Long id) {
        log.debug("REST request to delete TermAndCondition : {}", id);
        termAndConditionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/term-and-conditions/update-list")
    public ResponseEntity<List<TermAndConditionDTO>> updateTermAndConditions(@Valid @RequestBody List<TermAndConditionDTO> termAndConditionDTOs) throws URISyntaxException {
        log.debug("REST request to save TermAndConditions : {}" );

        List<TermAndConditionDTO> rs = termAndConditionService.updateTermAndConditions(termAndConditionDTOs);

        return ResponseEntity.created(new URI("/api/term-and-conditions/" ))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ""))
            .body(rs);
    }

    @GetMapping("/term-and-conditions/{clientId}/{subgroupId}")
    public ResponseEntity<TermAndConditionDTO> getTermAndCondition(@PathVariable String clientId, @PathVariable String subgroupId) {
        log.debug("REST request to get TermAndCondition by client: {}", clientId);
        TermAndConditionDTO termAndConditionDTO = termAndConditionService.getTermAndConditionByClient(clientId, subgroupId);
        return ResponseEntity.ok().body(termAndConditionDTO);
    }

    @GetMapping("/consent/{clientId}")
    public ResponseEntity<List<TermAndConditionDTO>> getConsentByClient(@PathVariable String clientId) {
        log.debug("REST request to get TermAndConditions by criteria: {}", clientId);
        List<TermAndConditionDTO> listConsent = termAndConditionService.getConsentByClient(clientId);
        int indexItem = 0;
        for (TermAndConditionDTO item: listConsent) {
            item.setConsentOrder(indexItem);
            indexItem++;
        }
        termAndConditionService.updateTermAndConditions(listConsent);
        return ResponseEntity.ok().body(listConsent);
    }
    @DeleteMapping("/consent/{id}")
    public ResponseEntity<Void> deleteConsent(@PathVariable Long id) {
        log.debug("REST request to delete TermAndCondition : {}", id);
        TermAndConditionDTO deleteItem = new TermAndConditionDTO();
        Optional<TermAndConditionDTO> termAndConditionItem = termAndConditionService.findOne(id);
        deleteItem = termAndConditionItem.get();
        deleteItem.setIsTerminated(true);
        TermAndConditionDTO result = termAndConditionService.save(deleteItem);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @PutMapping("/consent")
    public ResponseEntity<List<TermAndConditionDTO>> updateConsents(@Valid @RequestBody List<TermAndConditionRequest> termAndConditionRequests) throws URISyntaxException {
        log.debug("REST request to save TermAndConditions : {}" );
        ArrayList<TermAndConditionDTO> newList = new ArrayList<TermAndConditionDTO>();
        for (TermAndConditionRequest dto: termAndConditionRequests) {
            Optional<TermAndConditionDTO> updateItemBefore = termAndConditionService.findOne(dto.getId());
            TermAndConditionDTO updateItem = updateItemBefore.get();
            if (dto.getId() != null) { updateItem.setId(dto.getId()); }
            if (dto.getClientId() != null) { updateItem.setClientId(dto.getClientId()); }
            if (dto.getSubgroupId() != null) { updateItem.setSubgroupId(Arrays.toString(dto.getSubgroupId())); }
            if (dto.getTitle() != null) { updateItem.setTitle(dto.getTitle()); }
            if (dto.getContent() != null) { updateItem.setContent(dto.getContent()); }
            if (dto.getIsTargetSubgroup() != null) { updateItem.setIsTargetSubgroup(dto.getIsTargetSubgroup()); }
            if (dto.getSubgroupName() != null) { updateItem.setSubgroupName(dto.getSubgroupName()); }
            if (dto.getIsRequiredAnnually() != null) { updateItem.setIsRequiredAnnually(dto.getIsRequiredAnnually()); }
            if (dto.getConsentOrder() != null) { updateItem.setConsentOrder(dto.getConsentOrder()); }
            if (dto.getIsTerminated() != null) { updateItem.setIsTerminated(dto.getIsTerminated()); }
            newList.add(updateItem);
        }
        List<TermAndConditionDTO> rs = termAndConditionService.updateTermAndConditions(newList);
        return ResponseEntity.ok().body(rs);
    }
    @PostMapping("/consent")
    public ResponseEntity<TermAndConditionDTO> createConsent(@Valid @RequestBody TermAndConditionRequest termAndConditionRequest) throws URISyntaxException {
        log.debug("REST request to save TermAndCondition : {}", termAndConditionRequest);
        if (termAndConditionRequest.getId() != null) {
            throw new BadRequestAlertException("A new termAndCondition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TermAndConditionDTO inputItem = new TermAndConditionDTO();
        int numberConsentOrder = 0;
        try {
            List<TermAndConditionDTO> listConsent = termAndConditionService.getConsentByClient(termAndConditionRequest.getClientId());
            numberConsentOrder = listConsent.get(listConsent.size() - 1).getConsentOrder();

        } catch (Exception e) {
            //
        }
        inputItem.setClientId(termAndConditionRequest.getClientId());
        inputItem.setSubgroupId(Arrays.toString(termAndConditionRequest.getSubgroupId()));
        inputItem.setTitle(termAndConditionRequest.getTitle());
        inputItem.setContent(termAndConditionRequest.getContent());
        inputItem.setIsTargetSubgroup(termAndConditionRequest.getIsTargetSubgroup());
        inputItem.setSubgroupName(termAndConditionRequest.getSubgroupName());
        inputItem.setIsRequiredAnnually(termAndConditionRequest.getIsRequiredAnnually());
        inputItem.setConsentOrder(numberConsentOrder + 1);
        inputItem.setIsTerminated(termAndConditionRequest.getIsTerminated());

        TermAndConditionDTO result = termAndConditionService.save(inputItem);
        return ResponseEntity.created(new URI("/api/consent/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @GetMapping("/consent-by-client-subgroup")
    public ResponseEntity<List<TermAndConditionDTO>> getConsentByClientSubgroup(@RequestParam(name = "clientId") String clientId, @RequestParam(name = "subgroupId") String subgroupId) {
        log.debug("REST request to get TermAndConditions by criteria: {}", clientId);
        ArrayList<TermAndConditionDTO> resultList = new ArrayList<TermAndConditionDTO>();
        List<TermAndConditionDTO> listConsentIsNull = termAndConditionService.getTermAndConditionsByClientIdAndSubgroupIdIsNull(clientId);
        List<TermAndConditionDTO> listConsentEmpty = termAndConditionService.getConsentByClientIdAndSubgroupIdAndIsTerminated(clientId, "[]", false);
        List<TermAndConditionDTO> listConsent = termAndConditionService.getConsentByClientIdAndSubgroupIdAndIsTerminated(clientId, subgroupId, false);
        for (TermAndConditionDTO dto : listConsentIsNull) {
            resultList.add(dto);
        }
        for (TermAndConditionDTO dto : listConsentEmpty) {
            resultList.add(dto);
        }
        if (!subgroupId.equals("All")) {
            for (TermAndConditionDTO dto: listConsent) {
                String arrSubgroup = dto.getSubgroupId().replaceAll("^\\[|]$", "");
                List<String> arrList = new ArrayList<String>(Arrays.asList(arrSubgroup.split(",")));
                Boolean checkAdd = false;
                for (String item: arrList) {
                    if (item.trim().equals(subgroupId.trim())) { checkAdd = true; }
                }
                if (checkAdd) { resultList.add(dto); }
            }
        }
        return ResponseEntity.ok().body(resultList);
    }
}
