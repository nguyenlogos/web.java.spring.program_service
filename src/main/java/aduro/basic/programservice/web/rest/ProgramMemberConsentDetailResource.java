package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.domain.ProgramMemberConsentDetail;
import aduro.basic.programservice.service.ProgramMemberConsentDetailService;
import aduro.basic.programservice.service.dto.ProgramMemberConsentDetailDTO;
import aduro.basic.programservice.service.ProgramUserService;
import aduro.basic.programservice.service.TermAndConditionService;
import aduro.basic.programservice.service.dto.ProgramUserDTO;
import aduro.basic.programservice.service.dto.TermAndConditionDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.*;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramMemberConsentDetail}.
 */
@RestController
@RequestMapping("/api")
public class ProgramMemberConsentDetailResource {

    private final Logger log = LoggerFactory.getLogger(ProgramMemberConsentDetailResource.class);

    private static final String ENTITY_NAME = "ProgramMemberConsentDetail";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramMemberConsentDetailService programMemberConsentDetailService;
    private final ProgramUserService programUserService;
    private final TermAndConditionService termAndConditionService;

    public ProgramMemberConsentDetailResource(ProgramMemberConsentDetailService programMemberConsentDetailService,
                                              ProgramUserService programUserService, TermAndConditionService termAndConditionService) {
        this.programMemberConsentDetailService = programMemberConsentDetailService;
        this.programUserService = programUserService;
        this.termAndConditionService = termAndConditionService;
    }

    @GetMapping("/program-member-consent-detail")
    public ResponseEntity<List<ProgramMemberConsentDetail>> getConsentAcceptByDuring(@RequestParam(name = "consentId") Long consentId, @RequestParam(name = "programId") Long programId, @RequestParam(name = "fromDate") Date fromDate, @RequestParam(name = "toDate") Date toDate) {
        log.debug("REST request to get ProgramMemberConsentDetail by criteria: {}", consentId, programId, fromDate, toDate);
        List<ProgramMemberConsentDetail> listConsent = programMemberConsentDetailService.getConsentAcceptByDuring(consentId, programId);
        ArrayList<ProgramMemberConsentDetail> listConsentDuring = new ArrayList<ProgramMemberConsentDetail>();
        for (ProgramMemberConsentDetail item : listConsent) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(item.getConsentDate());
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Date itemCheck = cal.getTime();
            if ((itemCheck.after(fromDate) || itemCheck.equals(fromDate)) && (itemCheck.before(toDate) || itemCheck.equals(toDate)))
            {
                listConsentDuring.add(item);
            }
        }
        return ResponseEntity.ok().body(listConsentDuring);
    }

    @GetMapping("/consent-acceptance-by-participant")
    public ResponseEntity<List<TermAndConditionDTO>> getConsentAcceptanceByParticipant(@RequestParam(name = "clientId") String clientId, @RequestParam(name = "subgroupId") String subgroupId, @RequestParam(name = "participantId") String participantId, @RequestParam(name = "programId") Long programId) {
        log.debug("REST request to get ProgramMemberConsentDetail by criteria: {}", clientId);
        List<TermAndConditionDTO> resultTermAndConditionDTO = new ArrayList<>();
        try {
            ArrayList<TermAndConditionDTO> resultList = new ArrayList<TermAndConditionDTO>();
            List<TermAndConditionDTO> listConsentIsNull = termAndConditionService.getTermAndConditionsByClientIdAndSubgroupIdIsNull(clientId);
            List<TermAndConditionDTO> listConsentEmpty = termAndConditionService.getConsentByClientIdAndSubgroupIdAndIsTerminated(clientId, "[]", false);
            List<TermAndConditionDTO> listConsent = termAndConditionService.getConsentByClientIdAndSubgroupIdAndIsTerminated(clientId, subgroupId, false);
            for (TermAndConditionDTO dto : listConsentIsNull) {
                resultList.add(dto);
            }
            for (TermAndConditionDTO dto : listConsentEmpty) {
                resultList.add(dto);
            }
            if (!subgroupId.equals("All")) {
                for (TermAndConditionDTO dto: listConsent) {
                    String arrSubgroup = dto.getSubgroupId().replaceAll("^\\[|]$", "");
                    List<String> arrList = new ArrayList<String>(Arrays.asList(arrSubgroup.split(",")));
                    Boolean checkAdd = false;
                    for (String item: arrList) {
                        if (Long.valueOf(item.trim()).equals(Long.valueOf(subgroupId.trim()))) { checkAdd = true; }
                    }
                    if (checkAdd) { resultList.add(dto); }
                }
            }
            for (TermAndConditionDTO item : resultList) {
                List<ProgramMemberConsentDetailDTO> itemStatusConsent = new ArrayList<>();
                if (!item.getIsRequiredAnnually()) {
                    itemStatusConsent = programMemberConsentDetailService.getConsentByParticipantIdAndClientIdAndConsentIdAndProgramId(participantId, clientId, item.getId(), programId);
                } else {
                    itemStatusConsent = programMemberConsentDetailService.getConsentByParticipantIdAndClientIdAndConsentId(participantId, clientId, item.getId());
                }
                if (!itemStatusConsent.isEmpty()) {
                    item.setStatusConsent(itemStatusConsent.get(0).getStatusConsent());
                    resultTermAndConditionDTO.add(item);
                }
            }
        } catch (Exception e) {
            //
        }
        return ResponseEntity.ok().body(resultTermAndConditionDTO);
    }

    @PostMapping("/program-member-consent-detail")
    public ResponseEntity<List<ProgramMemberConsentDetailDTO>> createConsentAccept(@Valid @RequestBody List<ProgramMemberConsentDetailDTO> programMemberConsentDetailDTOs) throws URISyntaxException {
        log.debug("REST request to post ProgramMemberConsentDetail by criteria: {}");
        List<ProgramMemberConsentDetailDTO> resultList = new ArrayList<>();
        for (ProgramMemberConsentDetailDTO item : programMemberConsentDetailDTOs) {
            if (item.getId() != null) {
                throw new BadRequestAlertException("A new programMemberConsentDetail cannot already have an ID", ENTITY_NAME, "idexists");
            }
            try {
                ProgramUserDTO programUser = programUserService.getProgramUserByParticipantIdAndClientId(item.getParticipantId(), item.getClientId());
                item.setProgramUserId(programUser.getId());
                if (item.getConsentDate() == null) { item.setConsentDate(new Date());};
                if (item.getCreatedDate() == null) { item.setCreatedDate(Instant.now());};
                if (item.getModifiedDate() == null) { item.setModifiedDate(Instant.now());};
                ProgramMemberConsentDetailDTO itemProgramMemberConsentDetailDTO = programMemberConsentDetailService.createConsentAccept(item);
                resultList.add(itemProgramMemberConsentDetailDTO);
            } catch (Exception e) {
                //
            }
        }
        return ResponseEntity.ok().body(resultList);
    }

}
