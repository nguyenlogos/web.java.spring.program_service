package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramCollectionContentService;
import aduro.basic.programservice.service.dto.ProgramCollectionContentResponseDto;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramCollectionContentDTO;
import aduro.basic.programservice.service.dto.ProgramCollectionContentCriteria;
import aduro.basic.programservice.service.ProgramCollectionContentQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCollectionContent}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCollectionContentResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCollectionContentResource.class);

    private static final String ENTITY_NAME = "programCollectionContent";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCollectionContentService programCollectionContentService;

    private final ProgramCollectionContentQueryService programCollectionContentQueryService;

    public ProgramCollectionContentResource(ProgramCollectionContentService programCollectionContentService, ProgramCollectionContentQueryService programCollectionContentQueryService) {
        this.programCollectionContentService = programCollectionContentService;
        this.programCollectionContentQueryService = programCollectionContentQueryService;
    }

    /**
     * {@code POST  /program-collection-contents} : Create a new programCollectionContent.
     *
     * @param programCollectionContentDTO the programCollectionContentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCollectionContentDTO, or with status {@code 400 (Bad Request)} if the programCollectionContent has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-collection-contents")
    public ResponseEntity<ProgramCollectionContentDTO> createProgramCollectionContent(@Valid @RequestBody ProgramCollectionContentDTO programCollectionContentDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramCollectionContent : {}", programCollectionContentDTO);
        if (programCollectionContentDTO.getId() != null) {
            throw new BadRequestAlertException("A new programCollectionContent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramCollectionContentDTO result = programCollectionContentService.save(programCollectionContentDTO);
        return ResponseEntity.created(new URI("/api/program-collection-contents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-collection-contents} : Updates an existing programCollectionContent.
     *
     * @param programCollectionContentDTO the programCollectionContentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programCollectionContentDTO,
     * or with status {@code 400 (Bad Request)} if the programCollectionContentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programCollectionContentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-collection-contents")
    public ResponseEntity<ProgramCollectionContentDTO> updateProgramCollectionContent(@Valid @RequestBody ProgramCollectionContentDTO programCollectionContentDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramCollectionContent : {}", programCollectionContentDTO);
        if (programCollectionContentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramCollectionContentDTO result = programCollectionContentService.save(programCollectionContentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programCollectionContentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-collection-contents} : get all the programCollectionContents.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCollectionContents in body.
     */
    @GetMapping("/program-collection-contents")
    public ResponseEntity<ProgramCollectionContentResponseDto> getAllProgramCollectionContents(ProgramCollectionContentCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramCollectionContents by criteria: {}", criteria);
        Page<ProgramCollectionContentDTO> page = programCollectionContentQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);

        ProgramCollectionContentResponseDto res = ProgramCollectionContentResponseDto.builder()
            .totalPages(page.getTotalPages())
            .currentPage(page.getNumber() + 1)// default = 0
            .items(page.getContent())
            .build();

        return ResponseEntity.ok().headers(headers).body(res);
    }

    /**
    * {@code GET  /program-collection-contents/count} : count all the programCollectionContents.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-collection-contents/count")
    public ResponseEntity<Long> countProgramCollectionContents(ProgramCollectionContentCriteria criteria) {
        log.debug("REST request to count ProgramCollectionContents by criteria: {}", criteria);
        return ResponseEntity.ok().body(programCollectionContentQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-collection-contents/:id} : get the "id" programCollectionContent.
     *
     * @param id the id of the programCollectionContentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCollectionContentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-collection-contents/{id}")
    public ResponseEntity<ProgramCollectionContentDTO> getProgramCollectionContent(@PathVariable Long id) {
        log.debug("REST request to get ProgramCollectionContent : {}", id);
        Optional<ProgramCollectionContentDTO> programCollectionContentDTO = programCollectionContentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCollectionContentDTO);
    }

    /**
     * {@code DELETE  /program-collection-contents/:id} : delete the "id" programCollectionContent.
     *
     * @param id the id of the programCollectionContentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-collection-contents/{id}")
    public ResponseEntity<Void> deleteProgramCollectionContent(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCollectionContent : {}", id);
        programCollectionContentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }


    /**
     * {@code GET  /program-collection-contents} : get all the programCollectionContents.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCollectionContents in body.
     */
    @GetMapping("/program-collection-items")
    public ResponseEntity<List<ProgramCollectionContentDTO>> fetchingCollectionItems(ProgramCollectionContentCriteria criteria, Pageable pageable, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get items by criteria: {}", criteria);
        Page<ProgramCollectionContentDTO> page = programCollectionContentQueryService.findByCriteria(criteria, pageable);
        MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
