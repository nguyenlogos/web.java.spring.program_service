package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ap.dto.ClientDto;
import aduro.basic.programservice.ap.dto.CustomActivityDTO;
import aduro.basic.programservice.domain.ProgramTrackingStatus;
import aduro.basic.programservice.domain.enumeration.ActivityType;
import aduro.basic.programservice.domain.enumeration.ProgramSNSEvent;
import aduro.basic.programservice.helpers.CommonUtils;
import aduro.basic.programservice.service.ProgramQueryService;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.sns.PublisherService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.Program}.
 */
@RestController
@RequestMapping("/api")
public class MessageResource {

    private final Logger log = LoggerFactory.getLogger(MessageResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    @Autowired
    private PublisherService publisherService;


    /**
     * {@code POST  /programs} : Create a new program.
     *
     * @param programDTO the programDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programDTO, or with status {@code 400 (Bad Request)} if the program has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sns/publish")
    public ResponseEntity<Void> publishMessage(@Valid @RequestBody ProgramStatusChangeMessage dto) throws URISyntaxException {
        publisherService.publishWithEvent(ProgramSNSEvent.PROGRAM_TEXT_SNS, dto);
        return ResponseEntity.ok().build();
    }

}
