package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramHealthyRangeService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeCriteria;
import aduro.basic.programservice.service.ProgramHealthyRangeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramHealthyRange}.
 */
@RestController
@RequestMapping("/api")
public class ProgramHealthyRangeResource {

    private final Logger log = LoggerFactory.getLogger(ProgramHealthyRangeResource.class);

    private static final String ENTITY_NAME = "programHealthyRange";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramHealthyRangeService programHealthyRangeService;

    private final ProgramHealthyRangeQueryService programHealthyRangeQueryService;

    public ProgramHealthyRangeResource(ProgramHealthyRangeService programHealthyRangeService, ProgramHealthyRangeQueryService programHealthyRangeQueryService) {
        this.programHealthyRangeService = programHealthyRangeService;
        this.programHealthyRangeQueryService = programHealthyRangeQueryService;
    }

    /**
     * {@code POST  /program-healthy-ranges} : Create a new programHealthyRange.
     *
     * @param programHealthyRangeDTO the programHealthyRangeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programHealthyRangeDTO, or with status {@code 400 (Bad Request)} if the programHealthyRange has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-healthy-ranges")
    public ResponseEntity<ProgramHealthyRangeDTO> createProgramHealthyRange(@Valid @RequestBody ProgramHealthyRangeDTO programHealthyRangeDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramHealthyRange : {}", programHealthyRangeDTO);
        if (programHealthyRangeDTO.getId() != null) {
            throw new BadRequestAlertException("A new programHealthyRange cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramHealthyRangeDTO result = programHealthyRangeService.save(programHealthyRangeDTO);
        return ResponseEntity.created(new URI("/api/program-healthy-ranges/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-healthy-ranges} : Updates an existing programHealthyRange.
     *
     * @param programHealthyRangeDTO the programHealthyRangeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programHealthyRangeDTO,
     * or with status {@code 400 (Bad Request)} if the programHealthyRangeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programHealthyRangeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-healthy-ranges")
    public ResponseEntity<ProgramHealthyRangeDTO> updateProgramHealthyRange(@Valid @RequestBody ProgramHealthyRangeDTO programHealthyRangeDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramHealthyRange : {}", programHealthyRangeDTO);
        if (programHealthyRangeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramHealthyRangeDTO result = programHealthyRangeService.save(programHealthyRangeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programHealthyRangeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-healthy-ranges} : get all the programHealthyRanges.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programHealthyRanges in body.
     */
    @GetMapping("/program-healthy-ranges")
    public ResponseEntity<List<ProgramHealthyRangeDTO>> getAllProgramHealthyRanges(ProgramHealthyRangeCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramHealthyRanges by criteria: {}", criteria);
        Page<ProgramHealthyRangeDTO> page = programHealthyRangeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-healthy-ranges/count} : count all the programHealthyRanges.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-healthy-ranges/count")
    public ResponseEntity<Long> countProgramHealthyRanges(ProgramHealthyRangeCriteria criteria) {
        log.debug("REST request to count ProgramHealthyRanges by criteria: {}", criteria);
        return ResponseEntity.ok().body(programHealthyRangeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-healthy-ranges/:id} : get the "id" programHealthyRange.
     *
     * @param id the id of the programHealthyRangeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programHealthyRangeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-healthy-ranges/{id}")
    public ResponseEntity<ProgramHealthyRangeDTO> getProgramHealthyRange(@PathVariable Long id) {
        log.debug("REST request to get ProgramHealthyRange : {}", id);
        Optional<ProgramHealthyRangeDTO> programHealthyRangeDTO = programHealthyRangeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programHealthyRangeDTO);
    }

    /**
     * {@code DELETE  /program-healthy-ranges/:id} : delete the "id" programHealthyRange.
     *
     * @param id the id of the programHealthyRangeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-healthy-ranges/{id}")
    public ResponseEntity<Void> deleteProgramHealthyRange(@PathVariable Long id) {
        log.debug("REST request to delete ProgramHealthyRange : {}", id);
        programHealthyRangeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
