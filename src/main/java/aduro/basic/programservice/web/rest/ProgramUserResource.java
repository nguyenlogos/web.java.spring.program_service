package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.domain.EarnedUserLevel;
import aduro.basic.programservice.service.ProgramUserService;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.ProgramUserQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramUser}.
 */
@RestController
@RequestMapping("/api")
public class ProgramUserResource {

    private final Logger log = LoggerFactory.getLogger(ProgramUserResource.class);

    private static final String ENTITY_NAME = "programUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramUserService programUserService;

    private final ProgramUserQueryService programUserQueryService;

    public ProgramUserResource(ProgramUserService programUserService, ProgramUserQueryService programUserQueryService) {
        this.programUserService = programUserService;
        this.programUserQueryService = programUserQueryService;
    }

    /**
     * {@code POST  /program-users} : Create a new programUser.
     *
     * @param programUserDTO the programUserDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programUserDTO, or with status {@code 400 (Bad Request)} if the programUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-users")
    public ResponseEntity<ProgramUserDTO> createProgramUser(@Valid @RequestBody ProgramUserDTO programUserDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramUser : {}", programUserDTO);
        if (programUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new programUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramUserDTO result = programUserService.save(programUserDTO);
        return ResponseEntity.created(new URI("/api/program-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-users} : Updates an existing programUser.
     *
     * @param programUserDTO the programUserDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programUserDTO,
     * or with status {@code 400 (Bad Request)} if the programUserDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programUserDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-users")
    public ResponseEntity<ProgramUserDTO> updateProgramUser(@Valid @RequestBody ProgramUserDTO programUserDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramUser : {}", programUserDTO);
        if (programUserDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramUserDTO result = programUserService.save(programUserDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-users} : get all the programUsers.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programUsers in body.
     */
    @GetMapping("/program-users")
    public ResponseEntity<List<ProgramUserDTO>> getAllProgramUsers(ProgramUserCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramUsers by criteria: {}", criteria);
        Page<ProgramUserDTO> page = programUserQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /*
    * This api may be have performance issue, because it will fetching all program user of client.
    *
    * */
    @GetMapping("/program-users/fetching-all/{clientId}/{programId}")
    public ResponseEntity<List<ProgramUserDTO>> fetchingAllProgramUsers(@PathVariable String clientId, @PathVariable Long programId) {
        log.debug("REST request to get ProgramUsers by clientId : {} and programId = {}", clientId, programId);
        List<ProgramUserDTO> listUsers = programUserService.getAllProgramsByClientIdAndProgramId(clientId, programId);
        return ResponseEntity.ok().body(listUsers);
    }



    @GetMapping("/program-users/tobacco/{clientId}/{programId}")
    public ResponseEntity<List<TobaccoUserProgramDTO>> getAllProgramUsersTobacco(@PathVariable String clientId, @PathVariable Long programId) {
        List<TobaccoUserProgramDTO> page = programUserQueryService.findProgramUserByClientIdAndProgramId(clientId, programId);
        return ResponseEntity.ok().body(page);
    }

    /**
     * {@code GET  /program-users/count} : count all the programUsers.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/program-users/count")
    public ResponseEntity<Long> countProgramUsers(ProgramUserCriteria criteria) {
        log.debug("REST request to count ProgramUsers by criteria: {}", criteria);
        return ResponseEntity.ok().body(programUserQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-users/:id} : get the "id" programUser.
     *
     * @param id the id of the programUserDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programUserDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-users/{id}")
    public ResponseEntity<ProgramUserDTO> getProgramUser(@PathVariable Long id) {
        log.debug("REST request to get ProgramUser : {}", id);
        Optional<ProgramUserDTO> programUserDTO = programUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programUserDTO);
    }

    /**
     * {@code DELETE  /program-users/:id} : delete the "id" programUser.
     *
     * @param id the id of the programUserDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-users/{id}")
    public ResponseEntity<Void> deleteProgramUser(@PathVariable Long id) {
        log.debug("REST request to delete ProgramUser : {}", id);
        programUserService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/program-users/{participantId}/{clientId}/current-info")
    public ResponseEntity<ProgramUserDTO> getProgramUser(@PathVariable String participantId, @PathVariable String clientId, @RequestParam Map<String, String> queryParams) {
        log.debug("REST request to get participantId : {}", participantId);
        ProgramUserDTO programUserDTO = programUserService.getCurrentUserProgramInfo(participantId, clientId, "", queryParams);
        return ResponseEntity.ok().body(programUserDTO);
    }

    @GetMapping("/program-users/{participantId}/{clientId}/current-info/{subgroupId}")
    public ResponseEntity<ProgramUserDTO> getProgramUserWithSubgroup(@PathVariable String participantId, @PathVariable String clientId , @PathVariable String subgroupId, @RequestParam Map<String, String> queryParams) {
        log.debug("REST request to get participantId : {}", participantId);
        if(subgroupId != null && (subgroupId.equals("undefined") || subgroupId.equals("0")))
            subgroupId = "";

        ProgramUserDTO programUserDTO = programUserService.getCurrentUserProgramInfo(participantId, clientId, subgroupId, queryParams);
        return ResponseEntity.ok().body(programUserDTO);
    }

    @GetMapping("/program-users/{programId}/{clientId}/activity/{customActivityId}")
    public ResponseEntity<List<ProgramUserWithActivityStatusDTO>> getProgramUserWithActivityStatus(@PathVariable long programId, @PathVariable String clientId , @PathVariable String customActivityId){

        List<ProgramUserWithActivityStatusDTO> rs= programUserService.getProgramUserWithActivityStatus(programId, clientId, customActivityId);
        return ResponseEntity.ok().body(rs);

    }

    @PostMapping("/program-users/tobacco-user")
    public ResponseEntity<ProgramUserDTO> getLevelUpAndReward(@Valid @RequestBody TobaccoUserPayload tobaccoUserPayload) throws URISyntaxException, ExecutionException, InterruptedException {
        log.debug("REST request to save TobaccoUser : {}", tobaccoUserPayload.toString());
        // Process user incentive
        ProgramUserDTO updated = programUserService.updateTobaccoUser(tobaccoUserPayload);
        return ResponseEntity.ok().body(updated);
    }

    /*This API will invoke when get all program of participant*/
    @GetMapping("/program-users/get-programs/{participantId}/{clientId}")
    public ResponseEntity<List<ProgramPayload>> getProgramUserWithSubgroup(@PathVariable String participantId, @PathVariable String clientId) {
        log.debug("REST request to get all program by participantId And ClientId : {}", participantId);
        List<ProgramPayload> programs = programUserService.getAllProgramByParticipantIdAndClientId(participantId, clientId);
        return ResponseEntity.ok().body(programs);
    }

    /*This API will invoke when client want to remove subgroup
    * API return true : allow remove subgroup
    *  false: don't allow remove subgroup
    * */
    @GetMapping("/program-users/verify-subgroup-changed/{clientId}/{subgroupId}")
    public ResponseEntity<VerifiedSubgroupResponse>verifySubgroupIdByClientId(@PathVariable String clientId, @PathVariable String subgroupId) {
        log.debug("REST request to verify program user And subgroupId : {} {}", clientId, subgroupId);
        Boolean isAllowed = programUserService.verifySubgroupClient(subgroupId, clientId);
        return ResponseEntity.ok().body(new VerifiedSubgroupResponse(isAllowed));
    }

    @PostMapping("/program-users/update-subgroups")
    public ResponseEntity<List<ProgramUserDTO>> updateProgramUserBySubgroups(@Valid @RequestBody UpdatedSubgroupParticipantPayload updatedSubgroupParticipantPayload) throws URISyntaxException, ExecutionException, InterruptedException {
        log.debug("REST request to save Program User with Subgroups : {}", updatedSubgroupParticipantPayload.toString());
        // Process user incentive
        List<ProgramUserDTO> updated = programUserService.updateSubgroupsForParticipant(updatedSubgroupParticipantPayload);
        return ResponseEntity.ok().body(updated);
    }

    // impact to all users of programs, may be lead to performance or program service.
    @GetMapping("/program-users/extract-earned-level")
    public ResponseEntity<List<EarnedUserLevel>> extractEarnedLevel(@RequestParam("clientId") String clientId, @RequestParam("programId") long programId) {
        log.debug("REST Extract Data Earned Level clientId And ProgramId : {} {}", clientId, programId);
        List<EarnedUserLevel> earnedLists = programUserService.extractEarnedLevel(clientId, programId);
        return ResponseEntity.ok().body(earnedLists);
    }

    @PostMapping("program-users/subscribe-program")
    public ResponseEntity<APIResponse<MemberProgramDto>> getFullProgram(@Valid @RequestBody JoinProgramRequest programRequest) throws URISyntaxException {
        log.debug("REST request to get FullOutComeProgram : {}", programRequest);

        APIResponse<MemberProgramDto> result = programUserService.subscribeProgram(programRequest);

        return ResponseEntity
            .status(HttpStatus.OK)
            .body(result);
    }

    @GetMapping("/program-users/get-screening-info")
    public ResponseEntity<APIResponse<ItemCollectionResponseDto>> getAllContentProgramCollections(@RequestParam(name = "participantId") String participantId, @RequestParam(name = "clientId") String clientId) throws URISyntaxException {
        log.debug("REST request to get ProgramUser : {}", participantId);
        APIResponse<ItemCollectionResponseDto> result = programUserService.getInformationAfterScreening(participantId, clientId);
        return ResponseEntity.ok(result);
    }
    @PostMapping("/program-users/join-program")
    public ResponseEntity<JoinRequestResponseDto> joinProgramWithPayload(@Valid @RequestBody JoinProgramRequest programRequest) throws URISyntaxException {
        log.debug("REST request to save ProgramUser : {}", programRequest);


        JoinRequestResponseDto result = programUserService.joinProgramWithRequest(programRequest);

        return ResponseEntity
            .status(HttpStatus.OK)
            .body(result);
    }

}
