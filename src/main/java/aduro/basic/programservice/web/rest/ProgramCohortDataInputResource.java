package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramCohortDataInputService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramCohortDataInputDTO;
import aduro.basic.programservice.service.dto.ProgramCohortDataInputCriteria;
import aduro.basic.programservice.service.ProgramCohortDataInputQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCohortDataInput}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCohortDataInputResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortDataInputResource.class);

    private static final String ENTITY_NAME = "programCohortDataInput";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCohortDataInputService programCohortDataInputService;

    private final ProgramCohortDataInputQueryService programCohortDataInputQueryService;

    public ProgramCohortDataInputResource(ProgramCohortDataInputService programCohortDataInputService, ProgramCohortDataInputQueryService programCohortDataInputQueryService) {
        this.programCohortDataInputService = programCohortDataInputService;
        this.programCohortDataInputQueryService = programCohortDataInputQueryService;
    }

    /**
     * {@code POST  /program-cohort-data-inputs} : Create a new programCohortDataInput.
     *
     * @param programCohortDataInputDTO the programCohortDataInputDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCohortDataInputDTO, or with status {@code 400 (Bad Request)} if the programCohortDataInput has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-cohort-data-inputs")
    public ResponseEntity<ProgramCohortDataInputDTO> createProgramCohortDataInput(@Valid @RequestBody ProgramCohortDataInputDTO programCohortDataInputDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramCohortDataInput : {}", programCohortDataInputDTO);
        if (programCohortDataInputDTO.getId() != null) {
            throw new BadRequestAlertException("A new programCohortDataInput cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramCohortDataInputDTO result = programCohortDataInputService.save(programCohortDataInputDTO);
        return ResponseEntity.created(new URI("/api/program-cohort-data-inputs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-cohort-data-inputs} : Updates an existing programCohortDataInput.
     *
     * @param programCohortDataInputDTO the programCohortDataInputDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programCohortDataInputDTO,
     * or with status {@code 400 (Bad Request)} if the programCohortDataInputDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programCohortDataInputDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-cohort-data-inputs")
    public ResponseEntity<ProgramCohortDataInputDTO> updateProgramCohortDataInput(@Valid @RequestBody ProgramCohortDataInputDTO programCohortDataInputDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramCohortDataInput : {}", programCohortDataInputDTO);
        if (programCohortDataInputDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramCohortDataInputDTO result = programCohortDataInputService.save(programCohortDataInputDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programCohortDataInputDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-cohort-data-inputs} : get all the programCohortDataInputs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCohortDataInputs in body.
     */
    @GetMapping("/program-cohort-data-inputs")
    public ResponseEntity<List<ProgramCohortDataInputDTO>> getAllProgramCohortDataInputs(ProgramCohortDataInputCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramCohortDataInputs by criteria: {}", criteria);
        Page<ProgramCohortDataInputDTO> page = programCohortDataInputQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-cohort-data-inputs/count} : count all the programCohortDataInputs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-cohort-data-inputs/count")
    public ResponseEntity<Long> countProgramCohortDataInputs(ProgramCohortDataInputCriteria criteria) {
        log.debug("REST request to count ProgramCohortDataInputs by criteria: {}", criteria);
        return ResponseEntity.ok().body(programCohortDataInputQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-cohort-data-inputs/:id} : get the "id" programCohortDataInput.
     *
     * @param id the id of the programCohortDataInputDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCohortDataInputDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-cohort-data-inputs/{id}")
    public ResponseEntity<ProgramCohortDataInputDTO> getProgramCohortDataInput(@PathVariable Long id) {
        log.debug("REST request to get ProgramCohortDataInput : {}", id);
        Optional<ProgramCohortDataInputDTO> programCohortDataInputDTO = programCohortDataInputService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCohortDataInputDTO);
    }

    /**
     * {@code DELETE  /program-cohort-data-inputs/:id} : delete the "id" programCohortDataInput.
     *
     * @param id the id of the programCohortDataInputDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-cohort-data-inputs/{id}")
    public ResponseEntity<Void> deleteProgramCohortDataInput(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCohortDataInput : {}", id);
        programCohortDataInputService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
