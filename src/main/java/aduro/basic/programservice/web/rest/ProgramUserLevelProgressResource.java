package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramUserLevelProgressService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramUserLevelProgressDTO;
import aduro.basic.programservice.service.dto.ProgramUserLevelProgressCriteria;
import aduro.basic.programservice.service.ProgramUserLevelProgressQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramUserLevelProgress}.
 */
@RestController
@RequestMapping("/api")
public class ProgramUserLevelProgressResource {

    private final Logger log = LoggerFactory.getLogger(ProgramUserLevelProgressResource.class);

    private static final String ENTITY_NAME = "programUserLevelProgress";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramUserLevelProgressService programUserLevelProgressService;

    private final ProgramUserLevelProgressQueryService programUserLevelProgressQueryService;

    public ProgramUserLevelProgressResource(ProgramUserLevelProgressService programUserLevelProgressService, ProgramUserLevelProgressQueryService programUserLevelProgressQueryService) {
        this.programUserLevelProgressService = programUserLevelProgressService;
        this.programUserLevelProgressQueryService = programUserLevelProgressQueryService;
    }

    /**
     * {@code POST  /program-user-level-progresses} : Create a new programUserLevelProgress.
     *
     * @param programUserLevelProgressDTO the programUserLevelProgressDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programUserLevelProgressDTO, or with status {@code 400 (Bad Request)} if the programUserLevelProgress has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-user-level-progresses")
    public ResponseEntity<ProgramUserLevelProgressDTO> createProgramUserLevelProgress(@Valid @RequestBody ProgramUserLevelProgressDTO programUserLevelProgressDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramUserLevelProgress : {}", programUserLevelProgressDTO);
        if (programUserLevelProgressDTO.getId() != null) {
            throw new BadRequestAlertException("A new programUserLevelProgress cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramUserLevelProgressDTO result = programUserLevelProgressService.save(programUserLevelProgressDTO);
        return ResponseEntity.created(new URI("/api/program-user-level-progresses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-user-level-progresses} : Updates an existing programUserLevelProgress.
     *
     * @param programUserLevelProgressDTO the programUserLevelProgressDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programUserLevelProgressDTO,
     * or with status {@code 400 (Bad Request)} if the programUserLevelProgressDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programUserLevelProgressDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-user-level-progresses")
    public ResponseEntity<ProgramUserLevelProgressDTO> updateProgramUserLevelProgress(@Valid @RequestBody ProgramUserLevelProgressDTO programUserLevelProgressDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramUserLevelProgress : {}", programUserLevelProgressDTO);
        if (programUserLevelProgressDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramUserLevelProgressDTO result = programUserLevelProgressService.save(programUserLevelProgressDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programUserLevelProgressDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-user-level-progresses} : get all the programUserLevelProgresses.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programUserLevelProgresses in body.
     */
    @GetMapping("/program-user-level-progresses")
    public ResponseEntity<List<ProgramUserLevelProgressDTO>> getAllProgramUserLevelProgresses(ProgramUserLevelProgressCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramUserLevelProgresses by criteria: {}", criteria);
        Page<ProgramUserLevelProgressDTO> page = programUserLevelProgressQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-user-level-progresses/count} : count all the programUserLevelProgresses.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-user-level-progresses/count")
    public ResponseEntity<Long> countProgramUserLevelProgresses(ProgramUserLevelProgressCriteria criteria) {
        log.debug("REST request to count ProgramUserLevelProgresses by criteria: {}", criteria);
        return ResponseEntity.ok().body(programUserLevelProgressQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-user-level-progresses/:id} : get the "id" programUserLevelProgress.
     *
     * @param id the id of the programUserLevelProgressDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programUserLevelProgressDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-user-level-progresses/{id}")
    public ResponseEntity<ProgramUserLevelProgressDTO> getProgramUserLevelProgress(@PathVariable Long id) {
        log.debug("REST request to get ProgramUserLevelProgress : {}", id);
        Optional<ProgramUserLevelProgressDTO> programUserLevelProgressDTO = programUserLevelProgressService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programUserLevelProgressDTO);
    }

    /**
     * {@code DELETE  /program-user-level-progresses/:id} : delete the "id" programUserLevelProgress.
     *
     * @param id the id of the programUserLevelProgressDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-user-level-progresses/{id}")
    public ResponseEntity<Void> deleteProgramUserLevelProgress(@PathVariable Long id) {
        log.debug("REST request to delete ProgramUserLevelProgress : {}", id);
        programUserLevelProgressService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
