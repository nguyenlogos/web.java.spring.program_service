package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramCohortCollectionService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionDTO;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionCriteria;
import aduro.basic.programservice.service.ProgramCohortCollectionQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramCohortCollection}.
 */
@RestController
@RequestMapping("/api")
public class ProgramCohortCollectionResource {

    private final Logger log = LoggerFactory.getLogger(ProgramCohortCollectionResource.class);

    private static final String ENTITY_NAME = "programCohortCollection";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramCohortCollectionService programCohortCollectionService;

    private final ProgramCohortCollectionQueryService programCohortCollectionQueryService;

    public ProgramCohortCollectionResource(ProgramCohortCollectionService programCohortCollectionService, ProgramCohortCollectionQueryService programCohortCollectionQueryService) {
        this.programCohortCollectionService = programCohortCollectionService;
        this.programCohortCollectionQueryService = programCohortCollectionQueryService;
    }

    /**
     * {@code POST  /program-cohort-collections} : Create a new programCohortCollection.
     *
     * @param programCohortCollectionDTO the programCohortCollectionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programCohortCollectionDTO, or with status {@code 400 (Bad Request)} if the programCohortCollection has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-cohort-collections")
    public ResponseEntity<ProgramCohortCollectionDTO> createProgramCohortCollection(@RequestBody ProgramCohortCollectionDTO programCohortCollectionDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramCohortCollection : {}", programCohortCollectionDTO);
        if (programCohortCollectionDTO.getId() != null) {
            throw new BadRequestAlertException("A new programCohortCollection cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramCohortCollectionDTO result = programCohortCollectionService.save(programCohortCollectionDTO);
        return ResponseEntity.created(new URI("/api/program-cohort-collections/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-cohort-collections} : Updates an existing programCohortCollection.
     *
     * @param programCohortCollectionDTO the programCohortCollectionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programCohortCollectionDTO,
     * or with status {@code 400 (Bad Request)} if the programCohortCollectionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programCohortCollectionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-cohort-collections")
    public ResponseEntity<ProgramCohortCollectionDTO> updateProgramCohortCollection(@RequestBody ProgramCohortCollectionDTO programCohortCollectionDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramCohortCollection : {}", programCohortCollectionDTO);
        if (programCohortCollectionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramCohortCollectionDTO result = programCohortCollectionService.save(programCohortCollectionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programCohortCollectionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-cohort-collections} : get all the programCohortCollections.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programCohortCollections in body.
     */
    @GetMapping("/program-cohort-collections")
    public ResponseEntity<List<ProgramCohortCollectionDTO>> getAllProgramCohortCollections(ProgramCohortCollectionCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramCohortCollections by criteria: {}", criteria);
        Page<ProgramCohortCollectionDTO> page = programCohortCollectionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-cohort-collections/count} : count all the programCohortCollections.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-cohort-collections/count")
    public ResponseEntity<Long> countProgramCohortCollections(ProgramCohortCollectionCriteria criteria) {
        log.debug("REST request to count ProgramCohortCollections by criteria: {}", criteria);
        return ResponseEntity.ok().body(programCohortCollectionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-cohort-collections/:id} : get the "id" programCohortCollection.
     *
     * @param id the id of the programCohortCollectionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programCohortCollectionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-cohort-collections/{id}")
    public ResponseEntity<ProgramCohortCollectionDTO> getProgramCohortCollection(@PathVariable Long id) {
        log.debug("REST request to get ProgramCohortCollection : {}", id);
        Optional<ProgramCohortCollectionDTO> programCohortCollectionDTO = programCohortCollectionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programCohortCollectionDTO);
    }

    /**
     * {@code DELETE  /program-cohort-collections/:id} : delete the "id" programCohortCollection.
     *
     * @param id the id of the programCohortCollectionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-cohort-collections/{id}")
    public ResponseEntity<Void> deleteProgramCohortCollection(@PathVariable Long id) {
        log.debug("REST request to delete ProgramCohortCollection : {}", id);
        programCohortCollectionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
