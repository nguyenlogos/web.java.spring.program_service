package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramLevelPathService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramLevelPathDTO;
import aduro.basic.programservice.service.dto.ProgramLevelPathCriteria;
import aduro.basic.programservice.service.ProgramLevelPathQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramLevelPath}.
 */
@RestController
@RequestMapping("/api")
public class ProgramLevelPathResource {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelPathResource.class);

    private static final String ENTITY_NAME = "programLevelPath";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramLevelPathService programLevelPathService;

    private final ProgramLevelPathQueryService programLevelPathQueryService;

    public ProgramLevelPathResource(ProgramLevelPathService programLevelPathService, ProgramLevelPathQueryService programLevelPathQueryService) {
        this.programLevelPathService = programLevelPathService;
        this.programLevelPathQueryService = programLevelPathQueryService;
    }

    /**
     * {@code POST  /program-level-paths} : Create a new programLevelPath.
     *
     * @param programLevelPathDTO the programLevelPathDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programLevelPathDTO, or with status {@code 400 (Bad Request)} if the programLevelPath has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-level-paths")
    public ResponseEntity<ProgramLevelPathDTO> createProgramLevelPath(@Valid @RequestBody ProgramLevelPathDTO programLevelPathDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramLevelPath : {}", programLevelPathDTO);
        if (programLevelPathDTO.getId() != null) {
            throw new BadRequestAlertException("A new programLevelPath cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramLevelPathDTO result = programLevelPathService.save(programLevelPathDTO);
        return ResponseEntity.created(new URI("/api/program-level-paths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-level-paths} : Updates an existing programLevelPath.
     *
     * @param programLevelPathDTO the programLevelPathDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programLevelPathDTO,
     * or with status {@code 400 (Bad Request)} if the programLevelPathDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programLevelPathDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-level-paths")
    public ResponseEntity<ProgramLevelPathDTO> updateProgramLevelPath(@Valid @RequestBody ProgramLevelPathDTO programLevelPathDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramLevelPath : {}", programLevelPathDTO);
        if (programLevelPathDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramLevelPathDTO result = programLevelPathService.save(programLevelPathDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programLevelPathDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-level-paths} : get all the programLevelPaths.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programLevelPaths in body.
     */
    @GetMapping("/program-level-paths")
    public ResponseEntity<List<ProgramLevelPathDTO>> getAllProgramLevelPaths(ProgramLevelPathCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramLevelPaths by criteria: {}", criteria);
        Page<ProgramLevelPathDTO> page = programLevelPathQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-level-paths/count} : count all the programLevelPaths.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-level-paths/count")
    public ResponseEntity<Long> countProgramLevelPaths(ProgramLevelPathCriteria criteria) {
        log.debug("REST request to count ProgramLevelPaths by criteria: {}", criteria);
        return ResponseEntity.ok().body(programLevelPathQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-level-paths/:id} : get the "id" programLevelPath.
     *
     * @param id the id of the programLevelPathDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programLevelPathDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-level-paths/{id}")
    public ResponseEntity<ProgramLevelPathDTO> getProgramLevelPath(@PathVariable Long id) {
        log.debug("REST request to get ProgramLevelPath : {}", id);
        Optional<ProgramLevelPathDTO> programLevelPathDTO = programLevelPathService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programLevelPathDTO);
    }

    /**
     * {@code DELETE  /program-level-paths/:id} : delete the "id" programLevelPath.
     *
     * @param id the id of the programLevelPathDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-level-paths/{id}")
    public ResponseEntity<Void> deleteProgramLevelPath(@PathVariable Long id) {
        log.debug("REST request to delete ProgramLevelPath : {}", id);
        programLevelPathService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/program-level-paths/{programLevelId}/update-paths")
    public ResponseEntity<List<ProgramLevelPathDTO>> updateLevelPaths(@PathVariable Long programLevelId, @RequestBody List<ProgramLevelPathDTO> programLevelPathDTOs) throws URISyntaxException {
        log.debug("REST request to save ProgramLevelRewards : {}", programLevelPathDTOs);

        List<ProgramLevelPathDTO> result = programLevelPathService.updateLevelPaths(programLevelId, programLevelPathDTOs);

        return ResponseEntity.created(new URI("/api/program-level-paths/" + programLevelId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programLevelId.toString()))
            .body(result);
    }

    @GetMapping("/program-level-paths/get-by-client/{clientId}/{participantId}")
    public ResponseEntity<List<ProgramLevelPathDTO>> getProgramLevelPathsByClient(@PathVariable String clientId, @PathVariable String participantId, @RequestParam Map<String, String> queryParams) {
        log.debug("REST request to get ProgramSubCategoryPoint : {}", clientId);
        List<ProgramLevelPathDTO> programLevelPathDTOList = programLevelPathService.getProgramLevelPathsByClientIdAndParticipantId(clientId, participantId, queryParams);
        return ResponseEntity.ok().body(programLevelPathDTOList);
    }
}
