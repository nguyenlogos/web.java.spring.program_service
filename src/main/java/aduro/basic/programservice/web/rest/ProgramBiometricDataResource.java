package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramBiometricDataService;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramBiometricDataDTO;
import aduro.basic.programservice.service.dto.ProgramBiometricDataCriteria;
import aduro.basic.programservice.service.ProgramBiometricDataQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramBiometricData}.
 */
@RestController
@RequestMapping("/api")
public class ProgramBiometricDataResource {

    private final Logger log = LoggerFactory.getLogger(ProgramBiometricDataResource.class);

    private static final String ENTITY_NAME = "programBiometricData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramBiometricDataService programBiometricDataService;

    private final ProgramBiometricDataQueryService programBiometricDataQueryService;

    public ProgramBiometricDataResource(ProgramBiometricDataService programBiometricDataService, ProgramBiometricDataQueryService programBiometricDataQueryService) {
        this.programBiometricDataService = programBiometricDataService;
        this.programBiometricDataQueryService = programBiometricDataQueryService;
    }

    /**
     * {@code POST  /program-biometric-data} : Create a new programBiometricData.
     *
     * @param programBiometricDataDTO the programBiometricDataDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programBiometricDataDTO, or with status {@code 400 (Bad Request)} if the programBiometricData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-biometric-data")
    public ResponseEntity<ProgramBiometricDataDTO> createProgramBiometricData(@RequestBody ProgramBiometricDataDTO programBiometricDataDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramBiometricData : {}", programBiometricDataDTO);
        if (programBiometricDataDTO.getId() != null) {
            throw new BadRequestAlertException("A new programBiometricData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramBiometricDataDTO result = programBiometricDataService.save(programBiometricDataDTO);
        return ResponseEntity.created(new URI("/api/program-biometric-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /program-biometric-data} : Updates an existing programBiometricData.
     *
     * @param programBiometricDataDTO the programBiometricDataDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programBiometricDataDTO,
     * or with status {@code 400 (Bad Request)} if the programBiometricDataDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programBiometricDataDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-biometric-data")
    public ResponseEntity<ProgramBiometricDataDTO> updateProgramBiometricData(@RequestBody ProgramBiometricDataDTO programBiometricDataDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramBiometricData : {}", programBiometricDataDTO);
        if (programBiometricDataDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramBiometricDataDTO result = programBiometricDataService.save(programBiometricDataDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programBiometricDataDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-biometric-data} : get all the programBiometricData.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programBiometricData in body.
     */
    @GetMapping("/program-biometric-data")
    public ResponseEntity<List<ProgramBiometricDataDTO>> getAllProgramBiometricData(ProgramBiometricDataCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramBiometricData by criteria: {}", criteria);
        Page<ProgramBiometricDataDTO> page = programBiometricDataQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-biometric-data/count} : count all the programBiometricData.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-biometric-data/count")
    public ResponseEntity<Long> countProgramBiometricData(ProgramBiometricDataCriteria criteria) {
        log.debug("REST request to count ProgramBiometricData by criteria: {}", criteria);
        return ResponseEntity.ok().body(programBiometricDataQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-biometric-data/:id} : get the "id" programBiometricData.
     *
     * @param id the id of the programBiometricDataDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programBiometricDataDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-biometric-data/{id}")
    public ResponseEntity<ProgramBiometricDataDTO> getProgramBiometricData(@PathVariable Long id) {
        log.debug("REST request to get ProgramBiometricData : {}", id);
        Optional<ProgramBiometricDataDTO> programBiometricDataDTO = programBiometricDataService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programBiometricDataDTO);
    }

    /**
     * {@code DELETE  /program-biometric-data/:id} : delete the "id" programBiometricData.
     *
     * @param id the id of the programBiometricDataDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-biometric-data/{id}")
    public ResponseEntity<Void> deleteProgramBiometricData(@PathVariable Long id) {
        log.debug("REST request to delete ProgramBiometricData : {}", id);
        programBiometricDataService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
