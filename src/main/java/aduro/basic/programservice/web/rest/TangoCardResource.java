package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.config.Constants;
import aduro.basic.programservice.domain.User;
import aduro.basic.programservice.repository.UserRepository;
import aduro.basic.programservice.security.AuthoritiesConstants;
import aduro.basic.programservice.service.MailService;
import aduro.basic.programservice.service.UserService;
import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import aduro.basic.programservice.service.dto.UserDTO;
import aduro.basic.programservice.tangocard.AccountAdapter;
import aduro.basic.programservice.tangocard.CatalogAdapter;
import aduro.basic.programservice.tangocard.CreditCardAdapter;
import aduro.basic.programservice.tangocard.CustomerAdapter;
import aduro.basic.programservice.tangocard.dto.*;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.web.rest.errors.EmailAlreadyUsedException;
import aduro.basic.programservice.web.rest.errors.LoginAlreadyUsedException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/api")
public class TangoCardResource {

    private final Logger log = LoggerFactory.getLogger(TangoCardResource.class);


    @Autowired
    CatalogAdapter catalogAdapter;

    @Autowired
    CustomerAdapter customerAdapter;

    @Autowired
    AccountAdapter accountAdapter;

    @Autowired
    CreditCardAdapter creditCardAdapter;

    public TangoCardResource() {

    }

    @GetMapping("/tango-card/catalogs")
    public ResponseEntity<List<BrandItem>> getCatalogs() throws Exception {
        log.debug("REST request to get catalog : {}");
        CompletableFuture<CatalogResponse> rs =  catalogAdapter.getCatalogAsync();

        CatalogResponse rsp = rs.get();

        List<BrandItem> brandItems = new ArrayList<>();
        rsp.getBrands().forEach(b->b.getItems().forEach(i->brandItems.add(i)));

        return ResponseEntity.ok().body(brandItems);
    }

    @GetMapping("/tango-card/customers")
    public ResponseEntity<List<CustomerResponse>> getCustomers() throws Exception {
        log.debug("REST request to get catalog : {}");
        CompletableFuture<List<CustomerResponse>> rs =  customerAdapter.GetCustomersAsync();
        return ResponseEntity.ok().body(rs.get());
    }

    @GetMapping("/tango-card/customers/{customerIdentifier}")
    public ResponseEntity<CustomerResponse> getCustomerDetail(@PathVariable String customerIdentifier) throws Exception {
        log.debug("REST request to get catalog : {}");
        CompletableFuture<CustomerResponse> rs =  customerAdapter.GetCustomerDetailAsync(customerIdentifier);
        return ResponseEntity.ok().body(rs.get());
    }

    @GetMapping("/tango-card/accounts/{accountIdentifier}")
    public ResponseEntity<AccountResponse> getAccountDetail(@PathVariable String accountIdentifier) throws Exception {
        log.debug("REST request to get catalog : {}");
        CompletableFuture<AccountResponse> rs =  accountAdapter.GetAccountDetailAsync(accountIdentifier);
        return ResponseEntity.ok().body(rs.get());
    }

    @GetMapping("/tango-card/customers/{customerIdentifier}/accounts")
    public ResponseEntity<List<AccountResponse>> getAccountsByCustomer(@PathVariable String customerIdentifier) throws Exception {
        log.debug("REST request to create account : {}");
        CompletableFuture<List<AccountResponse>> rs =  customerAdapter.getAccountsByCustomer(customerIdentifier);
        return ResponseEntity.ok().body(rs.get());
    }



    @PostMapping("/tango-card/customers")
    public ResponseEntity<CustomerResponse> createCustomer(@RequestBody CustomerRequest customerRequest) throws Exception {
        log.debug("REST request to create customer : {}");
        CompletableFuture<CustomerResponse> rs =  customerAdapter.CreateCustomerAysnc(customerRequest);
        return ResponseEntity.ok().body(rs.get());
    }

    @PostMapping("/tango-card/customers/{customerIdentifier}/accounts")
    public ResponseEntity<AccountResponse> createAccount(@PathVariable String customerIdentifier, @RequestBody AccountRequest accountRequest) throws Exception {
        log.debug("REST request to create account : {}");
        CompletableFuture<AccountResponse> rs =  customerAdapter.CreateAccountAysnc(customerIdentifier, accountRequest);
        return ResponseEntity.ok().body(rs.get());
    }


    @PostMapping("/tango-card/credit-cards/register")
    public ResponseEntity<CreditCardResponse> registerCreditCard(@RequestBody CreditCardRequest creditCardRequest) throws Exception {
        log.debug("REST request to create credit card : {}");
        CompletableFuture<CreditCardResponse> rs =  creditCardAdapter.registerCreditCardAysnc(creditCardRequest);
        return ResponseEntity.ok().body(rs.get());
    }

    @PostMapping("/tango-card/credit-cards/deposits")
    public ResponseEntity<CreditCardDepositsResponse> creditCardDeposits(@RequestBody CreditCardDepositsRequest creditCardDepositsRequest) throws Exception {
        log.debug("REST request to deposit : {}");
        CompletableFuture<CreditCardDepositsResponse> rs =  creditCardAdapter.createCreditCardDepositsAsync(creditCardDepositsRequest);
        return ResponseEntity.ok().body(rs.get());
    }


   @GetMapping("/tango-card/credit-cards")
    public ResponseEntity<List<CreditCardResponse>> getCreditCardsByAccount() throws Exception {
        log.debug("REST request to deposit : {}");
        CompletableFuture<List<CreditCardResponse>> rs =  creditCardAdapter.getCreditCards();
        return ResponseEntity.ok().body(rs.get());
    }




}
