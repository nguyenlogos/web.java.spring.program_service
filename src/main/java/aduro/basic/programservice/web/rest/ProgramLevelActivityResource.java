package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramLevelActivityService;
import aduro.basic.programservice.service.dto.APIResponse;
import aduro.basic.programservice.service.dto.ProgramLevelRewardDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import aduro.basic.programservice.service.dto.ProgramLevelActivityDTO;
import aduro.basic.programservice.service.dto.ProgramLevelActivityCriteria;
import aduro.basic.programservice.service.ProgramLevelActivityQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.ProgramLevelActivity}.
 */
@RestController
@RequestMapping("/api")
public class ProgramLevelActivityResource {

    private final Logger log = LoggerFactory.getLogger(ProgramLevelActivityResource.class);

    private static final String ENTITY_NAME = "programLevelActivity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramLevelActivityService programLevelActivityService;

    private final ProgramLevelActivityQueryService programLevelActivityQueryService;

    public ProgramLevelActivityResource(ProgramLevelActivityService programLevelActivityService, ProgramLevelActivityQueryService programLevelActivityQueryService) {
        this.programLevelActivityService = programLevelActivityService;
        this.programLevelActivityQueryService = programLevelActivityQueryService;
    }

    /**
     * {@code POST  /program-level-activities} : Create a new programLevelActivity.
     *
     * @param programLevelActivityDTO the programLevelActivityDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programLevelActivityDTO, or with status {@code 400 (Bad Request)} if the programLevelActivity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-level-activities")
    public ResponseEntity<ProgramLevelActivityDTO> createProgramLevelActivity(@Valid @RequestBody ProgramLevelActivityDTO programLevelActivityDTO) throws URISyntaxException {
        log.debug("REST request to save ProgramLevelActivity : {}", programLevelActivityDTO);
        if (programLevelActivityDTO.getId() != null) {
            throw new BadRequestAlertException("A new programLevelActivity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProgramLevelActivityDTO result = programLevelActivityService.save(programLevelActivityDTO);
        return ResponseEntity.created(new URI("/api/program-level-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/program-level-activities/save")
    public ResponseEntity<APIResponse<Boolean>> saveListActivityRequired(@Valid @RequestBody List<ProgramLevelActivityDTO> programLevelActivities) throws URISyntaxException {
        log.debug("REST request to save ProgramLevelActivity : {}", programLevelActivities);
        APIResponse<Boolean> response = programLevelActivityService.saveWithList(programLevelActivities);
        return ResponseEntity.ok(response);
    }

    /**
     * {@code PUT  /program-level-activities} : Updates an existing programLevelActivity.
     *
     * @param programLevelActivityDTO the programLevelActivityDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated programLevelActivityDTO,
     * or with status {@code 400 (Bad Request)} if the programLevelActivityDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the programLevelActivityDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/program-level-activities")
    public ResponseEntity<ProgramLevelActivityDTO> updateProgramLevelActivity(@Valid @RequestBody ProgramLevelActivityDTO programLevelActivityDTO) throws URISyntaxException {
        log.debug("REST request to update ProgramLevelActivity : {}", programLevelActivityDTO);
        if (programLevelActivityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProgramLevelActivityDTO result = programLevelActivityService.save(programLevelActivityDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, programLevelActivityDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /program-level-activities} : get all the programLevelActivities.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programLevelActivities in body.
     */
    @GetMapping("/program-level-activities")
    public ResponseEntity<List<ProgramLevelActivityDTO>> getAllProgramLevelActivities(ProgramLevelActivityCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ProgramLevelActivities by criteria: {}", criteria);
        Page<ProgramLevelActivityDTO> page = programLevelActivityQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /program-level-activities/count} : count all the programLevelActivities.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/program-level-activities/count")
    public ResponseEntity<Long> countProgramLevelActivities(ProgramLevelActivityCriteria criteria) {
        log.debug("REST request to count ProgramLevelActivities by criteria: {}", criteria);
        return ResponseEntity.ok().body(programLevelActivityQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /program-level-activities/:id} : get the "id" programLevelActivity.
     *
     * @param id the id of the programLevelActivityDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the programLevelActivityDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/program-level-activities/{id}")
    public ResponseEntity<ProgramLevelActivityDTO> getProgramLevelActivity(@PathVariable Long id) {
        log.debug("REST request to get ProgramLevelActivity : {}", id);
        Optional<ProgramLevelActivityDTO> programLevelActivityDTO = programLevelActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(programLevelActivityDTO);
    }

    /**
     * {@code DELETE  /program-level-activities/:id} : delete the "id" programLevelActivity.
     *
     * @param id the id of the programLevelActivityDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/program-level-activities/{id}")
    public ResponseEntity<Void> deleteProgramLevelActivity(@PathVariable Long id) {
        log.debug("REST request to delete ProgramLevelActivity : {}", id);
        programLevelActivityService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/program-level-activities/{programLevelId}/update-activities")
    public ResponseEntity<List<ProgramLevelActivityDTO>> updateLevelActivities(@PathVariable Long programLevelId, @RequestBody List<ProgramLevelActivityDTO> programLevelActivityDTOS) throws URISyntaxException {
        log.debug("REST request to save ProgramLevelRewards : {}", programLevelActivityDTOS);

        List<ProgramLevelActivityDTO> result = programLevelActivityService.updateLevelActivities(programLevelId, programLevelActivityDTOS);

        return ResponseEntity.created(new URI("/api/program-level-rewards/" + programLevelId))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, programLevelId.toString()))
            .body(result);
    }
}
