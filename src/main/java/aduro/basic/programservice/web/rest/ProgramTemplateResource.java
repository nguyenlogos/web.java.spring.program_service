package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.service.ProgramQueryService;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.CloneActivityDTO;
import aduro.basic.programservice.service.dto.ProgramCriteria;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.service.dto.ProgramTemplateDTO;
import aduro.basic.programservice.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link aduro.basic.programservice.domain.Program}.
 */
@RestController
@RequestMapping("/api")
public class ProgramTemplateResource {

    private final Logger log = LoggerFactory.getLogger(ProgramTemplateResource.class);

    private static final String ENTITY_NAME = "program";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProgramService programService;

    private final ProgramQueryService programQueryService;

    public ProgramTemplateResource(ProgramService programService, ProgramQueryService programQueryService) {
        this.programService = programService;
        this.programQueryService = programQueryService;
    }


    /**
     * {@code GET  /programs} : get all the programs.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of programs in body.
     */
    @GetMapping("/program-templates")
    public ResponseEntity<List<ProgramDTO>> getAllProgramTemplates(ProgramCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Programs by criteria: {}", criteria);

        BooleanFilter booleanFilter = new BooleanFilter();
        booleanFilter.setEquals(true);
        criteria.setIsTemplate(booleanFilter);

        Page<ProgramDTO> page = programQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    /**
     * {@code POST  /programs} : Create a new program.
     *
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programDTO, or with status {@code 400 (Bad Request)} if the program has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-templates/{programId}/export-template")
    public ResponseEntity<ProgramDTO> exportProgramTemplate(@PathVariable Long programId, @RequestBody ProgramTemplateDTO dto) throws URISyntaxException {
        ProgramDTO result = programService.exportProgramTemplate(programId, dto);
        return ResponseEntity.created(new URI("/api/programs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ""))
            .body(result);
    }

    /**
     * {@code POST  /programs} : Create a new program.
     *
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programDTO, or with status {@code 400 (Bad Request)} if the program has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-templates/{templateId}/create-program")
    public ResponseEntity<ProgramDTO> createProgramFromTemplate(@PathVariable Long templateId,@RequestBody ProgramTemplateDTO dto) throws URISyntaxException {
        ProgramDTO result =  programService.createProgramFromTemplate(templateId, dto);
        return ResponseEntity.created(new URI("/api/programs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ""))
            .body(result);
    }



    /**
     * {@code POST  /programs} : Create a new program.
     *
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new programDTO, or with status {@code 400 (Bad Request)} if the program has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/program-templates/{templateId}/{programId}/clone-activities")
    public ResponseEntity<ProgramDTO> cloneProgramActivites(@PathVariable Long templateId, @PathVariable Long programId,@RequestBody List<CloneActivityDTO> cloneActivityDTOS) throws URISyntaxException {
        ProgramDTO result =  programService.cloneProgramActivites(templateId, programId, cloneActivityDTOS);
        return ResponseEntity.created(new URI("/api/programs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, ""))
            .body(result);
    }


}
