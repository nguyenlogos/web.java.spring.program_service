package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import aduro.basic.programservice.domain.enumeration.CollectionStatus;

/**
 * A ProgramUserCollectionProgress.
 */
@Entity
@Table(name = "program_user_collection_progress")
public class ProgramUserCollectionProgress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "collection_id", nullable = false)
    private Long collectionId;

    @NotNull
    @Column(name = "total_required_items", nullable = false)
    private Integer totalRequiredItems;

    @Column(name = "current_progress")
    private Long currentProgress;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CollectionStatus status;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @NotNull
    @Column(name = "modified_date", nullable = false)
    private Instant modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "program_user_cohort_id")
    @NotNull
    @JsonIgnoreProperties("programUserCollectionProgresses")
    private ProgramUserCohort programUserCohort;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCollectionId() {
        return collectionId;
    }

    public ProgramUserCollectionProgress collectionId(Long collectionId) {
        this.collectionId = collectionId;
        return this;
    }

    public void setCollectionId(Long collectionId) {
        this.collectionId = collectionId;
    }

    public Integer getTotalRequiredItems() {
        return totalRequiredItems;
    }

    public ProgramUserCollectionProgress totalRequiredItems(Integer totalRequiredItems) {
        this.totalRequiredItems = totalRequiredItems;
        return this;
    }

    public void setTotalRequiredItems(Integer totalRequiredItems) {
        this.totalRequiredItems = totalRequiredItems;
    }

    public Long getCurrentProgress() {
        return currentProgress;
    }

    public ProgramUserCollectionProgress currentProgress(Long currentProgress) {
        this.currentProgress = currentProgress;
        return this;
    }

    public void setCurrentProgress(Long currentProgress) {
        this.currentProgress = currentProgress;
    }

    public CollectionStatus getStatus() {
        return status;
    }

    public ProgramUserCollectionProgress status(CollectionStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(CollectionStatus status) {
        this.status = status;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ProgramUserCollectionProgress createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public ProgramUserCollectionProgress modifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public ProgramUserCohort getProgramUserCohort() {
        return programUserCohort;
    }

    public ProgramUserCollectionProgress programUserCohort(ProgramUserCohort programUserCohort) {
        this.programUserCohort = programUserCohort;
        return this;
    }

    public void setProgramUserCohort(ProgramUserCohort programUserCohort) {
        this.programUserCohort = programUserCohort;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramUserCollectionProgress)) {
            return false;
        }
        return id != null && id.equals(((ProgramUserCollectionProgress) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramUserCollectionProgress{" +
            "id=" + getId() +
            ", collectionId=" + getCollectionId() +
            ", totalRequiredItems=" + getTotalRequiredItems() +
            ", currentProgress=" + getCurrentProgress() +
            ", status='" + getStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
