package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * A ClientProgramRewardSetting.
 */
@Entity
@Table(name = "client_program_reward_setting")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientProgramRewardSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "client_id", nullable = false)
    private String clientId;

    @NotNull
    @Column(name = "client_name", nullable = false)
    private String clientName;

    @NotNull
    @Column(name = "updated_date", nullable = false)
    private Instant updatedDate;

    @NotNull
    @Column(name = "updated_by", nullable = false)
    private String updatedBy;

    @Column(name = "customer_identifier")
    private String customerIdentifier;

    @Column(name = "account_identifier")
    private String accountIdentifier;

    @Column(name = "account_threshold", precision = 21, scale = 2)
    private BigDecimal accountThreshold;

    @Column(name = "current_balance", precision = 21, scale = 2)
    private BigDecimal currentBalance;

    @Column(name = "client_email")
    private String clientEmail;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @NotNull
    @JsonIgnoreProperties("clientProgramRewardSettings")
    private Program program;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public ClientProgramRewardSetting clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public ClientProgramRewardSetting clientName(String clientName) {
        this.clientName = clientName;
        return this;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public ClientProgramRewardSetting updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public ClientProgramRewardSetting updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCustomerIdentifier() {
        return customerIdentifier;
    }

    public ClientProgramRewardSetting customerIdentifier(String customerIdentifier) {
        this.customerIdentifier = customerIdentifier;
        return this;
    }

    public void setCustomerIdentifier(String customerIdentifier) {
        this.customerIdentifier = customerIdentifier;
    }

    public String getAccountIdentifier() {
        return accountIdentifier;
    }

    public ClientProgramRewardSetting accountIdentifier(String accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
        return this;
    }

    public void setAccountIdentifier(String accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }

    public BigDecimal getAccountThreshold() {
        return accountThreshold;
    }

    public ClientProgramRewardSetting accountThreshold(BigDecimal accountThreshold) {
        this.accountThreshold = accountThreshold;
        return this;
    }

    public void setAccountThreshold(BigDecimal accountThreshold) {
        this.accountThreshold = accountThreshold;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public ClientProgramRewardSetting currentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
        return this;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public ClientProgramRewardSetting clientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
        return this;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public Program getProgram() {
        return program;
    }

    public ClientProgramRewardSetting program(Program program) {
        this.program = program;
        return this;
    }

    public void setProgram(Program program) {
        this.program = program;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientProgramRewardSetting)) {
            return false;
        }
        return id != null && id.equals(((ClientProgramRewardSetting) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ClientProgramRewardSetting{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            ", customerIdentifier='" + getCustomerIdentifier() + "'" +
            ", accountIdentifier='" + getAccountIdentifier() + "'" +
            ", accountThreshold=" + getAccountThreshold() +
            ", currentBalance=" + getCurrentBalance() +
            ", clientEmail='" + getClientEmail() + "'" +
            "}";
    }
}
