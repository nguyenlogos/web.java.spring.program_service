package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProgramLevel.
 */
@Entity
@Table(name = "program_level")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramLevel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "start_point")
    private Integer startPoint;

    @Column(name = "end_point")
    private Integer endPoint;

    @Column(name = "level_order")
    private Integer levelOrder;

    @Column(name = "icon_path")
    private String iconPath;

    @Column(name = "name")
    private String name;

    @Column(name = "end_date")
    private LocalDate endDate;

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    @Column(name = "program_id", insertable = false, updatable = false)
    private Long programId;

    @ManyToOne(fetch = FetchType.LAZY)
    private Program program;

    @OneToMany(mappedBy = "programLevel",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramLevelActivity> programLevelActivities = new HashSet<>();

    @OneToMany(mappedBy = "programLevel", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramRequirementItems> programRequirementItems = new HashSet<>();

    public Set<ProgramLevelPath> getProgramLevelPaths() {
        return programLevelPaths;
    }

    public void setProgramLevelPaths(Set<ProgramLevelPath> programLevelPaths) {
        this.programLevelPaths = programLevelPaths;
    }

    @OneToMany(mappedBy = "programLevel",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramLevelPath> programLevelPaths = new HashSet<>();


    @OneToMany(mappedBy = "programLevel",fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramLevelReward> programLevelRewards = new HashSet<>();


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Set<ProgramRequirementItems> getProgramRequirementItems() {
        return programRequirementItems;
    }

    public ProgramLevel programRequirementItems(Set<ProgramRequirementItems> programRequirementItems) {
        this.programRequirementItems = programRequirementItems;
        return this;
    }

    public ProgramLevel addProgramRequirementItems(ProgramRequirementItems programRequirementItems) {
        this.programRequirementItems.add(programRequirementItems);
        programRequirementItems.setProgramLevel(this);
        return this;
    }

    public ProgramLevel removeProgramRequirementItems(ProgramRequirementItems programRequirementItems) {
        this.programRequirementItems.remove(programRequirementItems);
        programRequirementItems.setProgramLevel(null);
        return this;
    }

    public void setProgramRequirementItems(Set<ProgramRequirementItems> programRequirementItems) {
        this.programRequirementItems = programRequirementItems;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }
    public ProgramLevel description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStartPoint() {
        return startPoint;
    }

    public ProgramLevel startPoint(Integer startPoint) {
        this.startPoint = startPoint;
        return this;
    }

    public void setStartPoint(Integer startPoint) {
        this.startPoint = startPoint;
    }

    public Integer getEndPoint() {
        return endPoint;
    }

    public ProgramLevel endPoint(Integer endPoint) {
        this.endPoint = endPoint;
        return this;
    }

    public void setEndPoint(Integer endPoint) {
        this.endPoint = endPoint;
    }

    public Integer getLevelOrder() {
        return levelOrder;
    }

    public ProgramLevel levelOrder(Integer levelOrder) {
        this.levelOrder = levelOrder;
        return this;
    }

    public void setLevelOrder(Integer levelOrder) {
        this.levelOrder = levelOrder;
    }

    public String getIconPath() {
        return iconPath;
    }

    public ProgramLevel iconPath(String iconPath) {
        this.iconPath = iconPath;
        return this;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getName() {
        return name;
    }

    public ProgramLevel name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public ProgramLevel endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Program getProgram() {
        return program;
    }

    public ProgramLevel program(Program program) {
        this.program = program;
        return this;
    }

    public void setProgram(Program program) {
        this.program = program;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramLevel)) {
            return false;
        }
        return id != null && id.equals(((ProgramLevel) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramLevel{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", startPoint=" + getStartPoint() +
            ", endPoint=" + getEndPoint() +
            ", levelOrder=" + getLevelOrder() +
            ", iconPath='" + getIconPath() + "'" +
            ", name='" + getName() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }

    public Set<ProgramLevelActivity> getProgramLevelActivities() {
        return this.programLevelActivities;
    }

    public ProgramLevel setActivities(Set<ProgramLevelActivity> programLevelActivities) {
        this.programLevelActivities = programLevelActivities;
        return this;
    }

    public ProgramLevel addActivity(ProgramLevelActivity programLevelActivity) {
        this.programLevelActivities.add(programLevelActivity);
        programLevelActivity.setProgramLevel(this);
        return this;
    }

    public ProgramLevel removeActivity(ProgramLevelActivity programLevelActivity) {
        this.programLevelActivities.remove(programLevelActivity);
        programLevelActivity.setProgramLevel(null);
        return this;
    }

    public Set<ProgramLevelReward> getProgramLevelRewards() {
        return this.programLevelRewards;
    }

    public ProgramLevel setProgramLevelRewards(Set<ProgramLevelReward> programLevelRewards) {
        this.programLevelRewards = programLevelRewards;
        return this;
    }

    public ProgramLevel addProgramLevelReward(ProgramLevelReward programLevelReward) {
        this.programLevelRewards.add(programLevelReward);
        programLevelReward.setProgramLevel(this);
        return this;
    }

    public ProgramLevel removeProgramLevelReward(ProgramLevelReward programLevelReward) {
        this.programLevelActivities.remove(programLevelReward);
        programLevelReward.setProgramLevel(null);
        return this;
    }

}
