package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ProgramLevelPractice.
 */
@Entity
@Table(name = "program_level_practice")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramLevelPractice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "practice_id", nullable = false)
    private String practiceId;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "subgroup_name")
    private String subgroupName;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @NotNull
    @JoinColumn(name = "program_level_id")
    private ProgramLevel programLevel;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPracticeId() {
        return practiceId;
    }

    public ProgramLevelPractice practiceId(String practiceId) {
        this.practiceId = practiceId;
        return this;
    }

    public void setPracticeId(String practiceId) {
        this.practiceId = practiceId;
    }

    public String getName() {
        return name;
    }

    public ProgramLevelPractice name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public ProgramLevelPractice subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public ProgramLevelPractice subgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
        return this;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public ProgramLevel getProgramLevel() {
        return programLevel;
    }

    public ProgramLevelPractice programLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
        return this;
    }

    public void setProgramLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramLevelPractice)) {
            return false;
        }
        return id != null && id.equals(((ProgramLevelPractice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramLevelPractice{" +
            "id=" + getId() +
            ", practiceId='" + getPracticeId() + "'" +
            ", name='" + getName() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            "}";
    }
}
