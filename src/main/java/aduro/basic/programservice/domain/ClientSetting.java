package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * A ClientSetting.
 */
@Entity
@Table(name = "client_setting")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "client_id", nullable = false, unique = true)
    private String clientId;

    @NotNull
    @Column(name = "client_name", nullable = false, unique = true)
    private String clientName;

    @Column(name = "client_email")
    private String clientEmail;

    @Column(name = "tango_account_identifier")
    private String tangoAccountIdentifier;

    @Column(name = "tango_customer_identifier")
    private String tangoCustomerIdentifier;

    @Column(name = "tango_current_balance", precision = 21, scale = 2)
    private BigDecimal tangoCurrentBalance;

    @Column(name = "tango_threshold_balance", precision = 21, scale = 2)
    private BigDecimal tangoThresholdBalance;

    @Column(name = "tango_credit_token")
    private String tangoCreditToken;

    @NotNull
    @Column(name = "updated_date", nullable = false)
    private Instant updatedDate;

    @NotNull
    @Column(name = "updated_by", nullable = false)
    private String updatedBy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public ClientSetting clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public ClientSetting clientName(String clientName) {
        this.clientName = clientName;
        return this;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public ClientSetting clientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
        return this;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getTangoAccountIdentifier() {
        return tangoAccountIdentifier;
    }

    public ClientSetting tangoAccountIdentifier(String tangoAccountIdentifier) {
        this.tangoAccountIdentifier = tangoAccountIdentifier;
        return this;
    }

    public void setTangoAccountIdentifier(String tangoAccountIdentifier) {
        this.tangoAccountIdentifier = tangoAccountIdentifier;
    }

    public String getTangoCustomerIdentifier() {
        return tangoCustomerIdentifier;
    }

    public ClientSetting tangoCustomerIdentifier(String tangoCustomerIdentifier) {
        this.tangoCustomerIdentifier = tangoCustomerIdentifier;
        return this;
    }

    public void setTangoCustomerIdentifier(String tangoCustomerIdentifier) {
        this.tangoCustomerIdentifier = tangoCustomerIdentifier;
    }

    public BigDecimal getTangoCurrentBalance() {
        return tangoCurrentBalance;
    }

    public ClientSetting tangoCurrentBalance(BigDecimal tangoCurrentBalance) {
        this.tangoCurrentBalance = tangoCurrentBalance;
        return this;
    }

    public void setTangoCurrentBalance(BigDecimal tangoCurrentBalance) {
        this.tangoCurrentBalance = tangoCurrentBalance;
    }

    public BigDecimal getTangoThresholdBalance() {
        return tangoThresholdBalance;
    }

    public ClientSetting tangoThresholdBalance(BigDecimal tangoThresholdBalance) {
        this.tangoThresholdBalance = tangoThresholdBalance;
        return this;
    }

    public void setTangoThresholdBalance(BigDecimal tangoThresholdBalance) {
        this.tangoThresholdBalance = tangoThresholdBalance;
    }

    public String getTangoCreditToken() {
        return tangoCreditToken;
    }

    public ClientSetting tangoCreditToken(String tangoCreditToken) {
        this.tangoCreditToken = tangoCreditToken;
        return this;
    }

    public void setTangoCreditToken(String tangoCreditToken) {
        this.tangoCreditToken = tangoCreditToken;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public ClientSetting updatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public ClientSetting updatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientSetting)) {
            return false;
        }
        return id != null && id.equals(((ClientSetting) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ClientSetting{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", clientEmail='" + getClientEmail() + "'" +
            ", tangoAccountIdentifier='" + getTangoAccountIdentifier() + "'" +
            ", tangoCustomerIdentifier='" + getTangoCustomerIdentifier() + "'" +
            ", tangoCurrentBalance=" + getTangoCurrentBalance() +
            ", tangoThresholdBalance=" + getTangoThresholdBalance() +
            ", tangoCreditToken='" + getTangoCreditToken() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", updatedBy='" + getUpdatedBy() + "'" +
            "}";
    }
}
