package aduro.basic.programservice.domain;

import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import aduro.basic.programservice.service.dto.ProgramUserDTO;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EarnedUserLevel {

    private Long id;

    @NotNull
    private String participantId;

    @NotNull
    private String clientId;

    private BigDecimal totalUserPoint;

    @NotNull
    private Long programId;

    private Integer currentLevel;

    private String clientName;

    private String subgroupId;

    private String subgroupName;

    private Boolean isTobaccoUser;

    List<ExtractedEarnLevelDto> earnedLevels;


}
