package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ProgramUserLevelProgress.
 */
@Entity
@Table(name = "program_user_level_progress")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramUserLevelProgress implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "user_event_id", nullable = false)
    private Long userEventId;

    @NotNull
    @Column(name = "program_user_id", nullable = false)
    private Long programUserId;

    @Column(name = "created_at")
    private Instant createdAt;

    @NotNull
    @Column(name = "level_up_at", nullable = false)
    private Instant levelUpAt;

    @Column(name = "level_from")
    private Integer levelFrom;

    @Column(name = "level_to")
    private Integer levelTo;

    @Column(name = "has_reward")
    private Boolean hasReward;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserEventId() {
        return userEventId;
    }

    public ProgramUserLevelProgress userEventId(Long userEventId) {
        this.userEventId = userEventId;
        return this;
    }

    public void setUserEventId(Long userEventId) {
        this.userEventId = userEventId;
    }

    public Long getProgramUserId() {
        return programUserId;
    }

    public ProgramUserLevelProgress programUserId(Long programUserId) {
        this.programUserId = programUserId;
        return this;
    }

    public void setProgramUserId(Long programUserId) {
        this.programUserId = programUserId;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public ProgramUserLevelProgress createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getLevelUpAt() {
        return levelUpAt;
    }

    public ProgramUserLevelProgress levelUpAt(Instant levelUpAt) {
        this.levelUpAt = levelUpAt;
        return this;
    }

    public void setLevelUpAt(Instant levelUpAt) {
        this.levelUpAt = levelUpAt;
    }

    public Integer getLevelFrom() {
        return levelFrom;
    }

    public ProgramUserLevelProgress levelFrom(Integer levelFrom) {
        this.levelFrom = levelFrom;
        return this;
    }

    public void setLevelFrom(Integer levelFrom) {
        this.levelFrom = levelFrom;
    }

    public Integer getLevelTo() {
        return levelTo;
    }

    public ProgramUserLevelProgress levelTo(Integer levelTo) {
        this.levelTo = levelTo;
        return this;
    }

    public void setLevelTo(Integer levelTo) {
        this.levelTo = levelTo;
    }

    public Boolean isHasReward() {
        return hasReward;
    }

    public ProgramUserLevelProgress hasReward(Boolean hasReward) {
        this.hasReward = hasReward;
        return this;
    }

    public void setHasReward(Boolean hasReward) {
        this.hasReward = hasReward;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramUserLevelProgress)) {
            return false;
        }
        return id != null && id.equals(((ProgramUserLevelProgress) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramUserLevelProgress{" +
            "id=" + getId() +
            ", userEventId=" + getUserEventId() +
            ", programUserId=" + getProgramUserId() +
            ", createdAt='" + getCreatedAt() + "'" +
            ", levelUpAt='" + getLevelUpAt() + "'" +
            ", levelFrom=" + getLevelFrom() +
            ", levelTo=" + getLevelTo() +
            ", hasReward='" + isHasReward() + "'" +
            "}";
    }
}
