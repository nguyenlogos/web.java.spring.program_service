package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * A UserEvent.
 */
@Entity
@Table(name = "user_event")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "event_code", nullable = false)
    private String eventCode;

    @NotNull
    @Column(name = "event_id", nullable = false)
    private String eventId;

    @NotNull
    @Column(name = "event_date", nullable = false)
    private Instant eventDate;

    @Column(name = "event_point", precision = 21, scale = 2)
    private BigDecimal eventPoint;

    @Column(name = "event_category")
    private String eventCategory;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @NotNull
    @JsonIgnoreProperties("userEvents")
    @JoinColumn(name = "program_user_id")
    private ProgramUser programUser;

    @Column(name = "appeal_applied")
    private boolean appealApplied;

    @Column(name = "created_by_admin")
    private String createdByAdmin;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "biometric_event_id")
    private String biometricEventId;

    @Column(name = "is_migration")
    private boolean isMigration;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventCode() {
        return eventCode;
    }

    public UserEvent eventCode(String eventCode) {
        this.eventCode = eventCode;
        return this;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventId() {
        return eventId;
    }

    public UserEvent eventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Instant getEventDate() {
        return eventDate;
    }

    public UserEvent eventDate(Instant eventDate) {
        this.eventDate = eventDate;
        return this;
    }

    public void setEventDate(Instant eventDate) {
        this.eventDate = eventDate;
    }

    public BigDecimal getEventPoint() {
        return eventPoint;
    }

    public UserEvent eventPoint(BigDecimal eventPoint) {
        this.eventPoint = eventPoint;
        return this;
    }

    public void setEventPoint(BigDecimal eventPoint) {
        this.eventPoint = eventPoint;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public UserEvent eventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
        return this;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public ProgramUser getProgramUser() {
        return programUser;
    }

    public UserEvent programUser(ProgramUser programUser) {
        this.programUser = programUser;
        return this;
    }

    public void setProgramUser(ProgramUser programUser) {
        this.programUser = programUser;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserEvent)) {
            return false;
        }
        return id != null && id.equals(((UserEvent) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserEvent{" +
            "id=" + getId() +
            ", eventCode='" + getEventCode() + "'" +
            ", eventId='" + getEventId() + "'" +
            ", eventDate='" + getEventDate() + "'" +
            ", eventPoint=" + getEventPoint() +
            ", eventCategory='" + getEventCategory() + "'" +
            "}";
    }

    public boolean isAppealApplied() {
        return appealApplied;
    }

    public void setAppealApplied(boolean appealApplied) {
        this.appealApplied = appealApplied;
    }

    public String getCreatedByAdmin() {
        return createdByAdmin;
    }

    public void setCreatedByAdmin(String createdByAdmin) {
        this.createdByAdmin = createdByAdmin;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getBiometricEventId() {
        return biometricEventId;
    }

    public void setBiometricEventId(String biometricEventId) {
        this.biometricEventId = biometricEventId;
    }

    public boolean isMigration() {
        return isMigration;
    }

    public void setMigration(boolean migration) {
        isMigration = migration;
    }
}
