package aduro.basic.programservice.domain;

import aduro.basic.programservice.domain.enumeration.ProgramType;
import aduro.basic.programservice.helpers.DateHelpers;
import aduro.basic.programservice.service.dto.ProgramStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Program.
 */
@Entity
@Table(name = "program")
public class Program implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "start_date")
    private Instant startDate;

    @Column(name = "reset_date")
    private Instant resetDate;

    @Column(name = "last_sent")
    private LocalDate lastSent;

    @Column(name = "is_retrigger_email")
    private Boolean isRetriggerEmail;

    @Column(name = "is_eligible")
    private Boolean isEligible;

    @Column(name = "is_sent_registration_email")
    private Boolean isSentRegistrationEmail;

    @Column(name = "is_Registered_for_platform")
    private Boolean isRegisteredForPlatform;

    @Column(name = "is_scheduled_screening")
    private Boolean isScheduledScreening;

    @Column(name = "is_functionally")
    private Boolean isFunctionally;

    @Column(name = "logo_url")
    private String logoUrl;

    @Column(name = "description")
    private String description;

    @Column(name = "user_point", precision = 21, scale = 2)
    private BigDecimal userPoint;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "status")
    private String status;

    @Column(name = "is_use_point")
    private Boolean isUsePoint;

    @Column(name = "is_screen")
    private Boolean isScreen;

    @Column(name = "is_hpsf")
    private Boolean isHPSF;

    @Column(name = "is_htk")
    private Boolean isHTK;

    @Column(name = "is_labcorp")
    private Boolean isLabcorp;

    @Column(name = "labcorp_account_number")
    private String labcorpAccountNumber;

    @Column(name = "labcorp_file_url")
    private String labcorpFileUrl;

    @Column(name = "biometric_lookback_date")
    private Instant biometricLookbackDate;

    @Column(name = "appeals_form_file_url")
    private String appealsFormFileUrl;

    @Column(name = "appeals_form_text")
    private String appealsFormText;

    @Column(name = "is_appeals_form")
    private Boolean isAppealsForm;

    @Column(name = "economy_point")
    private BigDecimal economyPoint;

    @Column(name = "is_extractable")
    private Boolean isExtractable;

    @Column(name = "hpsfFormType")
    private String hpsfFormType;

    @Column(name = "wellmetric_scheduler10_url")
    private String wellmetricSchedulerV1Url;

    public String getAppealsFormFileUrl() {
        return appealsFormFileUrl;
    }

    public void setAppealsFormFileUrl(String appealsFormFileUrl) {
        this.appealsFormFileUrl = appealsFormFileUrl;
    }

    public String getAppealsFormText() {
        return appealsFormText;
    }

    public void setAppealsFormText(String appealsFormText) {
        this.appealsFormText = appealsFormText;
    }

    public Boolean getIsAppealsForm() {
        return isAppealsForm;
    }

    public void setIsAppealsForm(Boolean isAppealsForm) {
        this.isAppealsForm = isAppealsForm;
    }

    public String getLandingBackgroundImageUrl() {
        return landingBackgroundImageUrl;
    }

    public void setLandingBackgroundImageUrl(String landingBackgroundImageUrl) {
        this.landingBackgroundImageUrl = landingBackgroundImageUrl;
    }

    @Column(name = "landing_background_image_url")
    private String landingBackgroundImageUrl;


    public Instant getBiometricLookbackDate() {
        return biometricLookbackDate;
    }

    public void setBiometricLookbackDate(Instant biometricLookbackDate) {
        this.biometricLookbackDate = biometricLookbackDate;
    }

    public Instant getBiometricDeadlineDate() {
        return biometricDeadlineDate;
    }

    public void setBiometricDeadlineDate(Instant biometricDeadlineDate) {
        this.biometricDeadlineDate = biometricDeadlineDate;
    }

    @Column(name = "biometric_deadline_date")
    private Instant biometricDeadlineDate;

    public Boolean getIsTemplate() {
        return isTemplate;
    }

    public void setIsTemplate(Boolean isTemplate) {
        this.isTemplate = isTemplate;
    }

    @Column(name = "is_template")
    private Boolean isTemplate;

    @Column(name = "is_well_matric")
    private Boolean isWellMatric;
    public void setIsWellMatric(Boolean isWellMatric) {
        this.isWellMatric = isWellMatric;
    }

    @Column(name = "is_preview")
    private Boolean isPreview;

    public String getColorValue() {
        return colorValue;
    }

    public void setColorValue(String colorValue) {
        this.colorValue = colorValue;
    }

    @Column(name = "color_value")
    private String colorValue;

    public Instant getPreviewDate() {
        return previewDate;
    }

    public void setPreviewDate(Instant previewDate) {
        this.previewDate = previewDate;
    }

    public String getPreviewTimeZone() {
        return previewTimeZone;
    }

    public void setPreviewTimeZone(String previewTimeZone) {
        this.previewTimeZone = previewTimeZone;
    }

    public String getStartTimeZone() {
        return startTimeZone;
    }

    public void setStartTimeZone(String startTimeZone) {
        this.startTimeZone = startTimeZone;
    }

    public String getEndTimeZone() {
        return endTimeZone;
    }

    public void setEndTimeZone(String endTimeZone) {
        this.endTimeZone = endTimeZone;
    }

    @Column(name = "program_length")
    private Integer programLength;


    @Column(name = "preview_date")
    private Instant previewDate;

    @Column(name = "preview_time_zone")
    private String previewTimeZone;

    @Column(name = "start_time_zone")
    private String startTimeZone;

    @Column(name = "end_time_zone")
    private String endTimeZone;

    @Column(name = "apply_reward_all_subgroup")
    private Boolean applyRewardAllSubgroup;

    @Column(name = "program_register_message")
    private String programRegisterMessage;

    public Boolean isApplyRewardAllSubgroup() {
        return applyRewardAllSubgroup;
    }

    public Program applyRewardAllSubgroup(Boolean applyRewardAllSubgroup) {
        this.applyRewardAllSubgroup = applyRewardAllSubgroup;
        return this;
    }

    public void setApplyRewardAllSubgroup(Boolean applyRewardAllSubgroup) {
        this.applyRewardAllSubgroup = applyRewardAllSubgroup;
    }



    public Boolean isIsWellMatric() {
        return isWellMatric;
    }

    public Program isWellMatric(Boolean isWellMatric) {
        this.isWellMatric = isWellMatric;
        return this;
    }


    @Column(name = "is_coaching")
    private Boolean isCoaching;

    public void setIsCoaching(Boolean isCoaching) {
        this.isCoaching = isCoaching;
    }

    public Boolean isIsCoaching() {
        return isCoaching;
    }

    public Program isCoaching(Boolean isCoaching) {
        this.isCoaching = isCoaching;
        return this;
    }

    public String getLevelStructure() {
        return levelStructure;
    }

    public void setLevelStructure(String levelStructure) {
        this.levelStructure = levelStructure;
    }

    @Column(name = "level_structure")
    private String levelStructure;


    @Column(name = "is_hp")
    private Boolean isHp;

    public Boolean isIsHp() {
        return isHp;
    }

    public Program isHp(Boolean isHp) {
        this.isHp = isHp;
        return this;
    }

    public void setIsHp(Boolean isHp) {
        this.isHp = isHp;
    }


    public Boolean isIsUseLevel() {
        return isUseLevel;
    }

    public Program isUseLevel(Boolean isUseLevel) {
        this.isUseLevel = isUseLevel;
        return this;
    }

    public void setIsUseLevel(Boolean isUseLevel) {
        this.isUseLevel = isUseLevel;
    }

    public Integer getProgramLength() {
        return programLength;
    }

    public Program programLength(Integer programLength) {
        this.programLength = programLength;
        return this;
    }

    public void setProgramLength(Integer programLength) {
        this.programLength = programLength;
    }


    @Column(name = "is_use_level")
    private Boolean isUseLevel;

    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramCategoryPoint> programCategoryPoints = new HashSet<>();

    public Set<ProgramSubgroup> getProgramSubgroups() {
        return programSubgroups;
    }

    public void setProgramSubgroups(Set<ProgramSubgroup> programSubgroups) {
        this.programSubgroups = programSubgroups;
    }

    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramSubgroup> programSubgroups = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Program name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public Program startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getResetDate() {
        return resetDate;
    }

    public Program resetDate(Instant resetDate) {
        this.resetDate = resetDate;
        return this;
    }

    public void setResetDate(Instant resetDate) {
        this.resetDate = resetDate;
    }

    public LocalDate getLastSent() {
        return lastSent;
    }

    public Program lastSent(LocalDate lastSent) {
        this.lastSent = lastSent;
        return this;
    }

    public void setLastSent(LocalDate lastSent) {
        this.lastSent = lastSent;
    }

    public Boolean isIsRetriggerEmail() {
        return isRetriggerEmail;
    }

    public Program isRetriggerEmail(Boolean isRetriggerEmail) {
        this.isRetriggerEmail = isRetriggerEmail;
        return this;
    }

    public void setIsRetriggerEmail(Boolean isRetriggerEmail) {
        this.isRetriggerEmail = isRetriggerEmail;
    }

    public Boolean isIsEligible() {
        return isEligible;
    }

    public Program isEligible(Boolean isEligible) {
        this.isEligible = isEligible;
        return this;
    }

    public void setIsEligible(Boolean isEligible) {
        this.isEligible = isEligible;
    }

    public Boolean isIsSentRegistrationEmail() {
        return isSentRegistrationEmail;
    }

    public Program isSentRegistrationEmail(Boolean isSentRegistrationEmail) {
        this.isSentRegistrationEmail = isSentRegistrationEmail;
        return this;
    }

    public void setIsSentRegistrationEmail(Boolean isSentRegistrationEmail) {
        this.isSentRegistrationEmail = isSentRegistrationEmail;
    }

    public Boolean isIsRegisteredForPlatform() {
        return isRegisteredForPlatform;
    }

    public Program isRegisteredForPlatform(Boolean isRegisteredForPlatform) {
        this.isRegisteredForPlatform = isRegisteredForPlatform;
        return this;
    }

    public void setIsRegisteredForPlatform(Boolean isRegisteredForPlatform) {
        this.isRegisteredForPlatform = isRegisteredForPlatform;
    }

    public Boolean isIsScheduledScreening() {
        return isScheduledScreening;
    }

    public Program isScheduledScreening(Boolean isScheduledScreening) {
        this.isScheduledScreening = isScheduledScreening;
        return this;
    }

    public void setIsScheduledScreening(Boolean isScheduledScreening) {
        this.isScheduledScreening = isScheduledScreening;
    }

    public Boolean isIsFunctionally() {
        return isFunctionally;
    }

    public Program isFunctionally(Boolean isFunctionally) {
        this.isFunctionally = isFunctionally;
        return this;
    }

    public void setIsFunctionally(Boolean isFunctionally) {
        this.isFunctionally = isFunctionally;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public Program logoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
        return this;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getDescription() {
        return description;
    }

    public Program description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getUserPoint() {
        return userPoint;
    }

    public Program userPoint(BigDecimal userPoint) {
        this.userPoint = userPoint;
        return this;
    }

    public void setUserPoint(BigDecimal userPoint) {
        this.userPoint = userPoint;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public Program lastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public Program createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Program lastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Program createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public Program status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean isIsUsePoint() {
        return isUsePoint;
    }

    public Program isUsePoint(Boolean isUsePoint) {
        this.isUsePoint = isUsePoint;
        return this;
    }

    public void setIsUsePoint(Boolean isUsePoint) {
        this.isUsePoint = isUsePoint;
    }

    public Boolean isIsScreen() {
        return isScreen;
    }

    public Program isScreen(Boolean isScreen) {
        this.isScreen = isScreen;
        return this;
    }

    public void setIsScreen(Boolean isScreen) {
        this.isScreen = isScreen;
    }

    public Set<ProgramCategoryPoint> getProgramCategoryPoints() {
        return programCategoryPoints;
    }

    public Program programCategoryPoints(Set<ProgramCategoryPoint> programCategoryPoints) {
        this.programCategoryPoints = programCategoryPoints;
        return this;
    }

    public Program addProgramCategoryPoint(ProgramCategoryPoint programCategoryPoint) {
        this.programCategoryPoints.add(programCategoryPoint);
        programCategoryPoint.setProgram(this);
        return this;
    }

    public Program removeProgramCategoryPoint(ProgramCategoryPoint programCategoryPoint) {
        this.programCategoryPoints.remove(programCategoryPoint);
        programCategoryPoint.setProgram(null);
        return this;
    }

    public void setProgramCategoryPoints(Set<ProgramCategoryPoint> programCategoryPoints) {
        this.programCategoryPoints = programCategoryPoints;
    }


    public Boolean isIsHPSF() {
        return isHPSF;
    }

    public Program isHPSF(Boolean isHPSF) {
        this.isHPSF = isHPSF;
        return this;
    }

    public void setIsHPSF(Boolean isHPSF) {
        this.isHPSF = isHPSF;
    }

    public Boolean isIsHTK() {
        return isHTK;
    }

    public Program isHTK(Boolean isHTK) {
        this.isHTK = isHTK;
        return this;
    }

    public void setIsHTK(Boolean isHTK) {
        this.isHTK = isHTK;
    }

    public Boolean isIsLabcorp() {
        return isLabcorp;
    }

    public Program isLabcorp(Boolean isLabcorp) {
        this.isLabcorp = isLabcorp;
        return this;
    }

    public void setIsLabcorp(Boolean isLabcorp) {
        this.isLabcorp = isLabcorp;
    }

    public String getLabcorpAccountNumber() {
        return labcorpAccountNumber;
    }

    public Program labcorpAccountNumber(String labcorpAccountNumber) {
        this.labcorpAccountNumber = labcorpAccountNumber;
        return this;
    }

    public void setLabcorpAccountNumber(String labcorpAccountNumber) {
        this.labcorpAccountNumber = labcorpAccountNumber;
    }

    public String getLabcorpFileUrl() {
        return labcorpFileUrl;
    }

    public Program labcorpFileUrl(String labcorpFileUrl) {
        this.labcorpFileUrl = labcorpFileUrl;
        return this;
    }

    public void setLabcorpFileUrl(String labcorpFileUrl) {
        this.labcorpFileUrl = labcorpFileUrl;
    }


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Program)) {
            return false;
        }
        return id != null && id.equals(((Program) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Program{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", resetDate='" + getResetDate() + "'" +
            ", lastSent='" + getLastSent() + "'" +
            ", isRetriggerEmail='" + isIsRetriggerEmail() + "'" +
            ", isEligible='" + isIsEligible() + "'" +
            ", isSentRegistrationEmail='" + isIsSentRegistrationEmail() + "'" +
            ", isRegisteredForPlatform='" + isIsRegisteredForPlatform() + "'" +
            ", isScheduledScreening='" + isIsScheduledScreening() + "'" +
            ", isFunctionally='" + isIsFunctionally() + "'" +
            ", logoUrl='" + getLogoUrl() + "'" +
            ", description='" + getDescription() + "'" +
            ", userPoint=" + getUserPoint() +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", status='" + getStatus() + "'" +
            ", isUsePoint='" + isIsUsePoint() + "'" +
            ", isScreen='" + isIsScreen() + "'" +
            ", isHPSF='" + isIsHPSF() + "'" +
            ", isHTK='" + isIsHTK() + "'" +
            ", isLabcorp='" + isIsLabcorp() + "'" +
            ", labcorpAccountNumber='" + getLabcorpAccountNumber() + "'" +
            ", labcorpFileUrl='" + getLabcorpFileUrl() + "'" +

            "}";
    }

    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ClientProgram> clientPrograms = new HashSet<>();

    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramLevel> programLevels = new HashSet<>();

    public Set<ProgramLevel> getProgramLevels() {
        return this.programLevels;
    }

    public Program setProgramLevels(Set<ProgramLevel> programLevels) {
        this.programLevels = programLevels;
        return this;
    }

    public Program addLevel(ProgramLevel level) {
        this.programLevels.add(level);
        level.setProgram(this);
        return this;
    }

    public Program removeLevel(ProgramLevel level) {
        this.programLevels.remove(level);
        level.setProgram(null);
        return this;
    }

    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramActivity> programActivities = new HashSet<>();

    public Set<ProgramActivity> getProgramActivities() {
        return this.programActivities;
    }

    public Program setProgramActivities(Set<ProgramActivity> programActivities) {
        this.programActivities = programActivities;
        return this;
    }

    public Program addProgramActivity(ProgramActivity programActivity) {
        this.programActivities.add(programActivity);
        programActivity.setProgram(this);
        return this;
    }

    public Program removeProgramActivity(ProgramActivity programActivity) {
        this.programActivities.remove(programActivity);
        programActivity.setProgram(null);
        return this;
    }

    public Set<ClientProgram> getClientPrograms() {
        return clientPrograms;
    }

    public void setClientPrograms(Set<ClientProgram> clientPrograms) {
        this.clientPrograms = clientPrograms;
    }

    public Boolean isIsPreview() {
        return isPreview;
    }

    public Program isPreview(Boolean isPreview) {
        this.isPreview = isPreview;
        return this;
    }

    public void setIsPreview(Boolean isPreview) {
        this.isPreview = isPreview;
    }


    public Program isTemplate(Boolean isTemplate) {
        this.isTemplate = isTemplate;
        return this;
    }


    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramHealthyRange> programHealthyRanges = new HashSet<>();

    public Program removeProgramHealthyRange(ProgramHealthyRange programHealthyRange) {
        this.programHealthyRanges.remove(programHealthyRange);
        programHealthyRange.setProgram(null);
        return this;
    }

    public Program addProgramHealthyRange(ProgramHealthyRange programHealthyRange) {
        this.programHealthyRanges.add(programHealthyRange);
        programHealthyRange.setProgram(this);
        return this;
    }

    public void setProgramHealthyRanges(Set<ProgramHealthyRange> programHealthyRanges) {
        this.programHealthyRanges = programHealthyRanges;
    }

    public Program programHealthyRanges(Set<ProgramHealthyRange> programHealthyRanges) {
        this.programHealthyRanges = programHealthyRanges;
        return this;
    }

    public Set<ProgramHealthyRange> getProgramHealthyRanges() {
        return programHealthyRanges;
    }

    @Column(name = "program_type")
    @Enumerated(EnumType.STRING)
    private ProgramType programType;

    @Column(name = "tobacco_program_deadline")
    private Instant tobaccoProgramDeadline;

    @Column(name = "tobacco_attestation_deadline")
    private Instant tobaccoAttestationDeadline;

    @Column(name = "is_tobacco_surcharge_management")
    private Boolean isTobaccoSurchargeManagement;

    @Column(name = "is_awarded_for_tobacco_ras")
    private Boolean isAwardedForTobaccoRas;

    @Column(name = "is_awarded_for_tobacco_attestation")
    private Boolean isAwardedForTobaccoAttestation;

    public Boolean isIsTobaccoSurchargeManagement() {
        return isTobaccoSurchargeManagement;
    }

    public void setTobaccoSurchageManagement(Boolean tobaccoSurchargeManagement) {
        isTobaccoSurchargeManagement = tobaccoSurchargeManagement;
    }

    public void setIsTobaccoSurchageManagement(Boolean isTobaccoSurchargeManagement) {
        this.isTobaccoSurchargeManagement = isTobaccoSurchargeManagement;
    }

    public Program isTobaccoSurchargeManagement(Boolean isTobaccoSurchargeManagement) {
        this.isTobaccoSurchargeManagement = isTobaccoSurchargeManagement;
        return this;
    }

    public Instant getTobaccoAttestationDeadline() {
        return tobaccoAttestationDeadline;
    }

    public Program tobaccoAttestationDeadline(Instant tobaccoAttestationDeadline) {
        this.tobaccoAttestationDeadline = tobaccoAttestationDeadline;
        return this;
    }


    public void setTobaccoAttestationDeadline(Instant tobaccoAttestationDeadline) {
        this.tobaccoAttestationDeadline = tobaccoAttestationDeadline;
    }

    public void setTobaccoProgramDeadline(Instant tobaccoProgramDeadline) {
        this.tobaccoProgramDeadline = tobaccoProgramDeadline;
    }

    public Instant getTobaccoProgramDeadline() {
        return tobaccoProgramDeadline;
    }

    public Program tobaccoProgramDeadline(Instant tobaccoProgramDeadline) {
        this.tobaccoProgramDeadline = tobaccoProgramDeadline;
        return this;
    }

    public Program isHP(Boolean defaultIsHp) {
        this.isHp = defaultIsHp;
        return  this;
    }

    public Program previewDate(Instant defaultPreviewDate) {
        this.previewDate = defaultPreviewDate;
        return this;
    }

    public Program previewTimeZone(String defaultPreviewTimeZone) {
        this.previewTimeZone = defaultPreviewTimeZone;
        return  this;
    }

    public Program startTimeZone(String defaultStartTimeZone) {
        this.startTimeZone = defaultStartTimeZone;
        return this;
    }

    public Program endTimeZone(String defaultEndTimeZone) {
        this.endTimeZone = defaultEndTimeZone;
        return this;
    }

    public Program levelStructure(String updatedLevelStructure) {
        this.levelStructure = updatedLevelStructure;
        return this;
    }

    public Program biometricDeadlineDate(Instant updatedBiometricDeadlineDate) {
        this.biometricDeadlineDate = updatedBiometricDeadlineDate;
        return this;
    }

    public Program biometricLookbackDate(Instant updatedBiometricLookbackDate) {
        this.biometricLookbackDate = updatedBiometricLookbackDate;
        return this;
    }

    public Program landingBackgroundImageUrl(String updatedLandingBackgroundImageUrl) {
        this.landingBackgroundImageUrl = updatedLandingBackgroundImageUrl;
        return this;
    }

    public boolean isIsTemplate() {
        return isTemplate;
    }

    public String getProgramRegisterMessage() {
        return programRegisterMessage;
    }

    public void setProgramRegisterMessage(String programRegisterMessage) {
        this.programRegisterMessage = programRegisterMessage;
    }

    public Boolean isAwardedForTobaccoRas() {
        return isAwardedForTobaccoRas;
    }

    public Boolean isIsAwardedForTobaccoRas() {
        return isAwardedForTobaccoRas;
    }

    public void setIsAwardedForTobaccoRas(Boolean awardedForTobaccoRas) {
        isAwardedForTobaccoRas = awardedForTobaccoRas;
    }

    public Program isAwardedForTobaccoRas(Boolean isAwardedForTobaccoRas) {
        this.isAwardedForTobaccoRas = isAwardedForTobaccoRas;
        return this;
    }

    public Program isAwardedForTobaccoAttestation(Boolean isAwardedForTobaccoAttestation) {
        this.isAwardedForTobaccoAttestation = isAwardedForTobaccoAttestation;
        return this;
    }

    public Boolean isIsAwardedForTobaccoAttestation() {
        return isAwardedForTobaccoAttestation;
    }

    public void setIsAwardedForTobaccoAttestation(Boolean awardedForTobaccoAttestation) {
        isAwardedForTobaccoAttestation = awardedForTobaccoAttestation;
    }

    public Program isIsAwardedForTobaccoAttestation(Boolean isAwardedForTobaccoAttestation) {
        this.isAwardedForTobaccoAttestation = isAwardedForTobaccoAttestation;
        return this;
    }

    @Column(name = "is_program_qa_verify")
    private boolean qaVerify;

    public boolean getQaVerify() {
        return qaVerify;
    }

    public void setQaVerify(boolean qaVerify) {
        this.qaVerify = qaVerify;
    }

    @Column(name = "program_qa_start_date")
    private Instant programQAStartDate;

    public Instant getProgramQAStartDate() {
        return programQAStartDate;
    }

    public void setProgramQAStartDate(Instant programQAStartDate) {
        this.programQAStartDate = programQAStartDate;
    }

    @Column(name = "program_qa_end_date")
    private Instant programQAEndDate;

    public Instant getProgramQAEndDate() {
        return programQAEndDate;
    }

    public void setProgramQAEndDate(Instant programQAEndDate) {
        this.programQAEndDate = programQAEndDate;
    }

    public void setAsProgramQaVerify(String status, Instant programQAStartDate, Instant programQAEndDate) {
        // Check if program status is set for QA verify
        // or current date is between qa dates
        if (status.equals(ProgramStatus.QA_Verify.toString())
            || isQaDateSetForQaVerify(status, programQAStartDate, programQAEndDate)) {
            // To make program is used for QA verify, set is_program_qa_verify is true and status is Active
            setQaVerify(true);
            setStatus(ProgramStatus.Active.toString());
        } else {
            setQaVerify(false);
        }
    }

    public boolean isQaDateSetForQaVerify(String status, Instant programQAStartDate, Instant programQAEndDate) {
        Instant curDateIns = DateHelpers.getCurrentDateTimeUTC().toInstant();

        if ((status.equals(ProgramStatus.Draft.toString()) || status.equals(ProgramStatus.QA_Verify.toString()))
            && (programQAStartDate != null
                && programQAEndDate != null
                && programQAStartDate.isBefore(curDateIns)
                && programQAEndDate.isAfter(curDateIns))) {
            return true;
        }

        return false;
    }

    public BigDecimal getEconomyPoint() {
        return economyPoint;
    }

    public void setEconomyPoint(BigDecimal economyPoint) {
        this.economyPoint = economyPoint;
    }


    public Boolean getExtractable() {
        return isExtractable;
    }

    public void setExtractable(Boolean extractable) {
        isExtractable = extractable;
    }

    public String getHpsfFormType() {
        return hpsfFormType;
    }

    public void setHpsfFormType(String hpsfFormType) {
        this.hpsfFormType = hpsfFormType;
    }


    public void setProgramType(ProgramType programType) {
        this.programType = programType;
    }

    public ProgramType getProgramType() {
        return programType;
    }


    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramCohort> programCohorts = new HashSet<>();

    public Set<ProgramCohort> getProgramCohorts() {
        return programCohorts;
    }

    public void setProgramCohorts(Set<ProgramCohort> programCohorts) {
        this.programCohorts = programCohorts;
    }

    @OneToMany(mappedBy = "program", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramCollection> programCollections= new HashSet<>();

    public Set<ProgramCollection> getProgramCollections() {
        return programCollections;
    }

    public void setProgramCollections(Set<ProgramCollection> programCollections) {
        this.programCollections = programCollections;
    }

    public String getWellmetricSchedulerV1Url() {
        return wellmetricSchedulerV1Url;
    }

    public void setWellmetricSchedulerV1Url(String wellmetricSchedulerV1Url) {
        this.wellmetricSchedulerV1Url = wellmetricSchedulerV1Url;
    }
}
