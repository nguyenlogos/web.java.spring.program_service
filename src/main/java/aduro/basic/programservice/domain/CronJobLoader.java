package aduro.basic.programservice.domain;


import aduro.basic.programservice.domain.enumeration.ProcessLevelType;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(name = "cron_job_loader")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CronJobLoader implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "program_id")
    private Long programId;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @NotNull
    @Column(name = "client_id")
    private String clientId;

    @Column(name = "participant_id")
    private String participantId;

    @Column(name = "process_type") //all or individual
    @Enumerated(EnumType.STRING)
    private ProcessLevelType type;

    @Column(name = "is_completed")
    private Boolean isCompleted;

    @Column(name = "is_force")
    private Boolean isForce;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "attempt_count")
    private int attemptCount;

    @Column(name = "read_at")
    private Instant readAt;

    @Column(name = "is_pass_required")
    private Boolean isPassRequired;

    @Column(name = "list_required_items")
    private String requiredItems;

}
