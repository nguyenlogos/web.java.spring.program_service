package aduro.basic.programservice.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "program_member_consent_detail", schema = "program_service_dev", catalog = "")
public class ProgramMemberConsentDetail implements Serializable {
    private Long id;
    private Long programUserId;
    private Long programId;
    private Long consentId;
    private String consentName;
    private String participantId;
    private String clientId;
    private Date consentDate;
    private Double consentVersion;
    private String createdBy;
    private Instant createdDate;
    private String modifiedBy;
    private Instant modifiedDate;
    private String deletedBy;
    private Instant deletedDate;
    private Boolean statusConsent;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "program_user_id", nullable = true)
    public Long getProgramUserId() {
        return programUserId;
    }

    public void setProgramUserId(Long programUserId) {
        this.programUserId = programUserId;
    }

    @Basic
    @Column(name = "client_id", nullable = true)
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "program_id", nullable = true)
    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }
    @Basic
    @Column(name = "participant_id", nullable = true)
    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }
    @Basic
    @Column(name = "consent_id", nullable = true)
    public Long getConsentId() {
        return consentId;
    }

    public void setConsentId(Long consentId) {
        this.consentId = consentId;
    }
    @Basic
    @Column(name = "consent_name", nullable = true, length = 50)
    public String getConsentName() {
        return consentName;
    }

    public void setConsentName(String consentName) {
        this.consentName = consentName;
    }

    @Basic
    @Column(name = "consent_date", nullable = true)
    public Date getConsentDate() {
        return consentDate;
    }

    public void setConsentDate(Date consentDate) {
        this.consentDate = consentDate;
    }

    @Basic
    @Column(name = "status_consent", nullable = true)

    public Boolean getStatusConsent() { return statusConsent; }

    public void setStatusConsent(Boolean statusConsent) { this.statusConsent = statusConsent; }

    @Basic
    @Column(name = "consent_version", nullable = true, precision = 0)
    public Double getConsentVersion() {
        return consentVersion;
    }

    public void setConsentVersion(Double consentVersion) {
        this.consentVersion = consentVersion;
    }

    @Basic
    @Column(name = "created_by", nullable = false, length = 100)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Basic
    @Column(name = "created_date", nullable = false)
    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    @Basic
    @Column(name = "modified_by", nullable = true, length = 100)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Basic
    @Column(name = "modified_date", nullable = false)
    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Basic
    @Column(name = "deleted_by", nullable = true, length = 100)
    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Basic
    @Column(name = "deleted_date", nullable = true)
    public Instant getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Instant deletedDate) {
        this.deletedDate = deletedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProgramMemberConsentDetail that = (ProgramMemberConsentDetail) o;
        return id == that.id && Objects.equals(programUserId, that.programUserId) && Objects.equals(programId, that.programId) && Objects.equals(consentName, that.consentName) && Objects.equals(consentDate, that.consentDate) && Objects.equals(consentVersion, that.consentVersion) && Objects.equals(createdBy, that.createdBy) && Objects.equals(createdDate, that.createdDate) && Objects.equals(modifiedBy, that.modifiedBy) && Objects.equals(modifiedDate, that.modifiedDate) && Objects.equals(deletedBy, that.deletedBy) && Objects.equals(deletedDate, that.deletedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, programUserId, programId, consentName, consentDate, consentVersion, createdBy, createdDate, modifiedBy, modifiedDate, deletedBy, deletedDate);
    }

    @Override
    public String toString() {
        return "ProgramMemberConsentDetailDTO{" +
            "id=" + getId() +
            ", programUserId='" + getProgramUserId() + "'" +
            ", programId='" + getProgramId() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", participantId='" + getParticipantId() + "'" +
            ", consentId='" + getConsentId() + "'" +
            ", consentName='" + getConsentName() + "'" +
            ", consentDate='" + getConsentDate() + "'" +
            ", consentVersion='" + getConsentVersion() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedBy='" + getModifiedBy() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", deletedBy='" + getDeletedBy() + "'" +
            ", deletedDate='" + getDeletedDate() + "'" +
            "}";
    }
}

