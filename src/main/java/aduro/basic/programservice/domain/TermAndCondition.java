package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A TermAndCondition.
 */
@Entity
@Table(name = "term_and_condition")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TermAndCondition implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "client_id", nullable = false)
    private String clientId;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "title")
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "is_target_subgroup")
    private Boolean isTargetSubgroup;

    @Column(name = "subgroup_name")
    private String subgroupName;

    @Column(name = "is_required_annually")
    private Boolean isRequiredAnnually;

    @Column(name = "consent_order")
    private Integer consentOrder;

    @Column(name = "is_terminated")
    private Boolean isTerminated;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public TermAndCondition clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public TermAndCondition subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getTitle() {
        return title;
    }

    public TermAndCondition title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public TermAndCondition content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean isIsTargetSubgroup() {
        return isTargetSubgroup;
    }

    public TermAndCondition isTargetSubgroup(Boolean isTargetSubgroup) {
        this.isTargetSubgroup = isTargetSubgroup;
        return this;
    }

    public void setIsTargetSubgroup(Boolean isTargetSubgroup) {
        this.isTargetSubgroup = isTargetSubgroup;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public TermAndCondition subgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
        return this;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public Boolean getIsRequiredAnnually() {
        return isRequiredAnnually;
    }
    public TermAndCondition isRequiredAnnually(Boolean isRequiredAnnually) {
        this.isRequiredAnnually = isRequiredAnnually;
        return this;
    }
    public void setIsRequiredAnnually(Boolean isRequiredAnnually) {
        this.isRequiredAnnually = isRequiredAnnually;
    }

    public Integer getConsentOrder() {
        return consentOrder;
    }

    public TermAndCondition consentOrder(Integer isRequiredAnnually) {
        this.consentOrder = consentOrder;
        return this;
    }

    public void setConsentOrder(Integer consentOrder) {
        this.consentOrder = consentOrder;
    }

    public Boolean getIsTerminated() {
        return isTerminated;
    }

    public TermAndCondition isTerminated(Boolean isTerminated) {
        this.isTerminated = isTerminated;
        return this;
    }

    public void setIsTerminated(Boolean isTerminated) {
        this.isTerminated = isTerminated;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TermAndCondition)) {
            return false;
        }
        return id != null && id.equals(((TermAndCondition) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "TermAndCondition{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", title='" + getTitle() + "'" +
            ", content='" + getContent() + "'" +
            ", isTargetSubgroup='" + isIsTargetSubgroup() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", isRequiredAnnually='" + getIsRequiredAnnually() + "'" +
            ", consentOrder='" + getConsentOrder() + "'" +
            ", isTerminated='" + getIsTerminated() + "'" +
            "}";
    }
}
