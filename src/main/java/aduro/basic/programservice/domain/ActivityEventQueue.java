package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ActivityEventQueue.
 */
@Entity
@Table(name = "activity_event_queue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ActivityEventQueue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "participant_id", nullable = false)
    private String participantId;

    @NotNull
    @Column(name = "client_id", nullable = false)
    private String clientId;

    @NotNull
    @Column(name = "activity_code", nullable = false)
    private String activityCode;

    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @Column(name = "transaction_id")
    private String transactionId;

    @Column(name = "execute_status")
    private String executeStatus;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "custom_activity_id")
    private Long customActivityId;

    @Column(name = "attempted_count")
    private Integer attemptedCount;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public ActivityEventQueue participantId(String participantId) {
        this.participantId = participantId;
        return this;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public ActivityEventQueue clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public ActivityEventQueue activityCode(String activityCode) {
        this.activityCode = activityCode;
        return this;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public ActivityEventQueue firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ActivityEventQueue lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public ActivityEventQueue email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ActivityEventQueue createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public ActivityEventQueue transactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public ActivityEventQueue executeStatus(String executeStatus) {
        this.executeStatus = executeStatus;
        return this;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public ActivityEventQueue errorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public ActivityEventQueue subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Long getCustomActivityId() {
        return customActivityId;
    }

    public void setCustomActivityId(Long customActivityId) {
        this.customActivityId = customActivityId;
    }

    public Integer getAttemptedCount() {
        return attemptedCount;
    }

    public void setAttemptedCount(Integer attemptedCount) {
        this.attemptedCount = attemptedCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ActivityEventQueue)) {
            return false;
        }
        return id != null && id.equals(((ActivityEventQueue) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ActivityEventQueue{" +
            "id=" + getId() +
            ", participantId='" + getParticipantId() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", activityCode='" + getActivityCode() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", transactionId='" + getTransactionId() + "'" +
            ", executeStatus='" + getExecuteStatus() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            "}";
    }
}
