package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A ProgramLevelActivity.
 */
@Entity
@Table(name = "program_level_activity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramLevelActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "activity_code")
    private String activityCode;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "subgroup_name")
    private String subgroupName;

    @NotNull
    @Column(name = "activity_id", nullable = false)
    private String activityId;

    public Long getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(Long programLevelId) {
        this.programLevelId = programLevelId;
    }

    @Column(name = "program_level_id", insertable = false, updatable = false)
    private Long programLevelId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("programLevelActivities")
    @JoinColumn(name = "program_level_id")
    private ProgramLevel programLevel;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public ProgramLevelActivity activityCode(String activityCode) {
        this.activityCode = activityCode;
        return this;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }



    public String getActivityId() {
        return activityId;
    }

    public ProgramLevelActivity activityId(String activityId) {
        this.activityId = activityId;
        return this;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public ProgramLevel getProgramLevel() {
        return programLevel;
    }

    public ProgramLevelActivity programLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
        return this;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public ProgramLevelActivity subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public ProgramLevelActivity subgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
        return this;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }


    public void setProgramLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramLevelActivity)) {
            return false;
        }
        return id != null && id.equals(((ProgramLevelActivity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramLevelActivity{" +
            "id=" + getId() +
            ", activityCode='" + getActivityCode() + "'" +
            ", activityId='" + getActivityId() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +

            "}";
    }

    public ProgramLevelActivity programLevelId(Long id) {
        this.programLevelId = id;
        return this;
    }
}
