package aduro.basic.programservice.domain.enumeration;


public enum ActivityType {
    INFORMATIONAL,
    ONE_TIME_TRACKING,
    WEEKLY_TRACKING,
    GROUP_TRACKING,
    PASSIVE_TRACKING,
    SELF_TRACKING,
    EMPLOYER_VERIFIED {
        @Override
        public String toString() {
            return "EMPLOYER_VERIFIED";
        }
    },
    BONUS_ACTIVITY_EMPLOYER_VERIFIED {
        @Override
        public String toString() {
            return "BONUS_ACTIVITY_EMPLOYER_VERIFIED";
        }
    },
    ASSESSMENT
}
