package aduro.basic.programservice.domain.enumeration;

/**
 * The CollectionStatus enumeration.
 */
public enum CollectionStatus {
    DOING, DONE, NOT_DOING
}
