package aduro.basic.programservice.domain.enumeration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class PublishMessageDto<T> {
    @JsonProperty("event_name")
    private String eventName;
    private T data;
}
