package aduro.basic.programservice.domain.enumeration;

/**
 * The CohortStatus enumeration.
 */
public enum CohortStatus {
    INPROGRESS, PENDING, COMPLETED, DISABLED
}
