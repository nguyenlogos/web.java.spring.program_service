package aduro.basic.programservice.domain.enumeration;


public enum RecurrenceEnum {

    ONCE("once"), WEEKLY("Weekly"), DAILY("Daily"), MONTHLY("Monthly");

    private final String value;

    RecurrenceEnum(final String text) {
        this.value = text;
    }

    @Override
    public String toString() {
        return value;
    }

    public static RecurrenceEnum fromString(String text) {
        for (RecurrenceEnum b : RecurrenceEnum.values()) {
            if (b.toString().equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
