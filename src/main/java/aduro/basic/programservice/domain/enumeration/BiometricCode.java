package aduro.basic.programservice.domain.enumeration;

public enum BiometricCode {
    BP_Systolic__c("BP_Systolic__c"),
    BP_Diastolic__c("BP_Diastolic__c"),
    Waist__c("Waist__c"),
    RFpg__c("RFpg__c"),
    Non_Fasting_Y_N__c("Non_Fasting_Y_N__c"),
    BMI__c("BMI__c"),
    TC_HDL_Ratio__c("TC_HDL_Ratio__c"),
    RCho__c("RCho__c"),
    RHdl__c("RHdl__c"),
    RTrig__c("RTrig__c"),


    //  heartRate;
    //  a1C;
    //  tobaccoUse;
    //  wthr;
    //  ldl;
    waist_to_height_ratio("waist_to_height_ratio"),
    RLdl__c("RLdl__c"),
    HR_Pulse__c("HR_Pulse__c"),
    a1c("a1c"),
    tobacco("tobacco");


    private String name;

    public String getValue()
    {
        return this.name;
    }

     BiometricCode(String name)
    {
        this.name = name;
    }
}
