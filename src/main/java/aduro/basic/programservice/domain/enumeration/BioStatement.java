package aduro.basic.programservice.domain.enumeration;

public enum BioStatement {

    PASS("PASS"), FAIL("FAIL");

    private String name;

    public String getValue()
    {
        return this.name;
    }

    BioStatement(String name)
    {
        this.name = name;
    }
}
