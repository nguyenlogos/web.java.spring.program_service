package aduro.basic.programservice.domain.enumeration;

public enum BioCategory {
    BLOOD_PRESSURE("WELLMETRIC_BLOOD_PRESSURE"),
    CHOLESTEROL("WELLMETRIC_CHOLESTEROL"),
    GLUCOSE("WELLMETRIC_GLUCOSE"),
    BODY_COMPOSITION("WELLMETRIC_BODY_COMPOSITION");

    private String name;

    public String getValue()
    {
        return this.name;
    }

     BioCategory(String name)
    {
        this.name = name;
    }
}
