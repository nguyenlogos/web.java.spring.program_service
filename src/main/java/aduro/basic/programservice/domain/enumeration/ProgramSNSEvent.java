package aduro.basic.programservice.domain.enumeration;

public enum ProgramSNSEvent {

    PROGRAM_UPDATED("PROGRAM_UPDATED"),
    PROGRAM_TEXT_SNS("PROGRAM_TEXT_SNS"),
    PROGRAM_USER_COHORT_UPDATED("PROGRAM_USER_COHORT_UPDATED"),
    PROGRAM_INCENTIVE("PROGRAM_INCENTIVE");
    private final String value;

    ProgramSNSEvent(final String text) {
        this.value = text;
    }

    @Override
    public String toString() {
        return value;
    }

    public static ProgramSNSEvent fromString(String text) {
        for (ProgramSNSEvent b : ProgramSNSEvent.values()) {
            if (b.toString().equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
