package aduro.basic.programservice.domain.enumeration;

public enum PreventiveScreeningType {
    // this enum is delare on CC FE to mark custom activity screening type of preventive activity
    AnnualPCPExam("Annual PCP Exam"),
    FollowupPCPExam("Follow-up PCP Exam"),
    Mamography("Mammography"),
    Colonoscopy("Colonoscopy"),
    ColorectalCancer("Colorectal Cancer"),
    ProstateCancer("Prostate Cancer"),
    CervicalCancer("Cervical Cancer"),
    VisionScreening("Vision Screening"),
    DentalVisit("Dental Visit"),
    PrenatalVisit("Pre-natal Visit"),
    HemoglobinA1CTest("Hemoglobin A1c Test"),
    CreatinineUrineProteinTest("Creatinine or Urine Protein Test"),
    DilatedEyeExam("Dilated Eye Exam"),
    FluVaccine("Flu Shot"),
    CovidVaccine1of2("COVID-19 Vaccine (1 of 2)"),
    CovidVaccine2of2("COVID-19 Vaccine (2 of 2)"),
    CovidVaccineSingleDose("COVID-19 Vaccine (Single Dose)"),
    CovidVaccineBooster("COVID-19 Vaccine Booster");

    private final String text;

    /**
     * @param text
     */
    private PreventiveScreeningType(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

    public static PreventiveScreeningType fromString(String text) {
        for (PreventiveScreeningType b : PreventiveScreeningType.values()) {
            if (b.toString().equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
}
