package aduro.basic.programservice.domain.enumeration;

public enum CoachEvent {
    COMPLETE_ONE_ONE_COACHING, REGISTRATION_ONE_ONE_COACHING, COMPLETE_ONE_ONE_WEEKLY_SESSION
}
