package aduro.basic.programservice.domain.enumeration;

/**
 * The NotificationType enumeration.
 */
public enum NotificationType {
    INCENTIVE_RESULT, WELLMETRIC_SCREENING_RESULT, TOBACCO_USER
}
