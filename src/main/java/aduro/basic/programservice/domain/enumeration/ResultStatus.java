package aduro.basic.programservice.domain.enumeration;

/**
 * The ResultStatus enumeration.
 */
public enum ResultStatus {
    PASS, FAIL, UNAVAILABLE
}
