package aduro.basic.programservice.domain.enumeration;

/**
 * The Gender enumeration.
 */
public enum Gender {
    MALE, FEMALE, UNIDENTIFIED, UNDEFINED
}
