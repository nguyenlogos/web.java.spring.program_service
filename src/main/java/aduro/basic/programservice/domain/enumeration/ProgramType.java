package aduro.basic.programservice.domain.enumeration;

public enum ProgramType {
    PARTICIPANT_PROGRAM,
    FULL_OUTCOMES,
    OUTCOMES_LITE;
}
