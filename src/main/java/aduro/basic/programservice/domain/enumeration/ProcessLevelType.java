package aduro.basic.programservice.domain.enumeration;

/**
 * The ResultStatus enumeration.
 */
public enum ProcessLevelType {
    ALL, INDIVIDUAL
}
