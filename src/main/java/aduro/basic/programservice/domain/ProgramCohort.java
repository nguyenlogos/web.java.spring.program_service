package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ProgramCohort.
 */
@Builder
@Entity
@Table(name = "program_cohort")
@NoArgsConstructor
@AllArgsConstructor
public class ProgramCohort implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 150)
    @Column(name = "cohort_name", length = 150, nullable = false)
    private String cohortName;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @NotNull
    @Column(name = "modified_date", nullable = false)
    private Instant modifiedDate;

    @Column(name = "is_default")
    private Boolean isDefault;

    @Size(max = 250)
    @Column(name = "cohort_description")
    private String cohortDescription;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "program_id")
    @JsonIgnoreProperties("programCohorts")
    private Program program;

    @OneToMany(mappedBy = "programCohort", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<ProgramCohortRule> programCohortRules = new HashSet<>();

    @OneToMany(mappedBy = "programCohort", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<ProgramCohortCollection> programCohortCollections = new HashSet<>();

    @OneToMany(mappedBy = "programCohort", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<ProgramUserCohort> programUserCohorts = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCohortName() {
        return cohortName;
    }

    public ProgramCohort cohortName(String cohortName) {
        this.cohortName = cohortName;
        return this;
    }

    public void setCohortName(String cohortName) {
        this.cohortName = cohortName;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ProgramCohort createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public ProgramCohort modifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Boolean isIsDefault() {
        return isDefault;
    }

    public ProgramCohort isDefault(Boolean isDefault) {
        this.isDefault = isDefault;
        return this;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getCohortDescription() {
        return cohortDescription;
    }

    public ProgramCohort cohortDescription(String cohortDescription) {
        this.cohortDescription = cohortDescription;
        return this;
    }

    public void setCohortDescription(String cohortDescription) {
        this.cohortDescription = cohortDescription;
    }

    public Program getProgram() {
        return program;
    }

    public ProgramCohort program(Program program) {
        this.program = program;
        return this;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public Set<ProgramCohortRule> getProgramCohortRules() {
        return programCohortRules;
    }

    public ProgramCohort programCohortRules(Set<ProgramCohortRule> programCohortRules) {
        this.programCohortRules = programCohortRules;
        return this;
    }

    public ProgramCohort addProgramCohortRule(ProgramCohortRule programCohortRule) {
        this.programCohortRules.add(programCohortRule);
        programCohortRule.setProgramCohort(this);
        return this;
    }

    public ProgramCohort removeProgramCohortRule(ProgramCohortRule programCohortRule) {
        this.programCohortRules.remove(programCohortRule);
        programCohortRule.setProgramCohort(null);
        return this;
    }

    public void setProgramCohortRules(Set<ProgramCohortRule> programCohortRules) {
        this.programCohortRules = programCohortRules;
    }

    public Set<ProgramCohortCollection> getProgramCohortCollections() {
        return programCohortCollections;
    }

    public ProgramCohort programCohortCollections(Set<ProgramCohortCollection> programCohortCollections) {
        this.programCohortCollections = programCohortCollections;
        return this;
    }

    public ProgramCohort addProgramCohortCollection(ProgramCohortCollection programCohortCollection) {
        this.programCohortCollections.add(programCohortCollection);
        programCohortCollection.setProgramCohort(this);
        return this;
    }

    public ProgramCohort removeProgramCohortCollection(ProgramCohortCollection programCohortCollection) {
        this.programCohortCollections.remove(programCohortCollection);
        programCohortCollection.setProgramCohort(null);
        return this;
    }

    public void setProgramCohortCollections(Set<ProgramCohortCollection> programCohortCollections) {
        this.programCohortCollections = programCohortCollections;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Set<ProgramUserCohort> getProgramUserCohorts() {
        return programUserCohorts;
    }

    public void setProgramUserCohorts(Set<ProgramUserCohort> programUserCohorts) {
        this.programUserCohorts = programUserCohorts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramCohort)) {
            return false;
        }
        return id != null && id.equals(((ProgramCohort) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramCohort{" +
            "id=" + getId() +
            ", cohortName='" + getCohortName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            ", isDefault='" + isIsDefault() + "'" +
            "}";
    }
}
