package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ProgramCohortCollection.
 */
@Entity
@Table(name = "program_cohort_collection")
public class ProgramCohortCollection implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "required_completion")
    private Integer requiredCompletion;

    @Column(name = "required_level")
    private Integer requiredLevel;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "modified_date")
    private Instant modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "program_collection_id")
    @JsonIgnoreProperties("programCohortCollections")
    private ProgramCollection programCollection;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "program_Cohort_id")
    @JsonIgnoreProperties("programCohortCollections")
    private ProgramCohort programCohort;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getRequiredCompletion() {
        return requiredCompletion;
    }

    public ProgramCohortCollection requiredCompletion(Integer requiredCompletion) {
        this.requiredCompletion = requiredCompletion;
        return this;
    }

    public void setRequiredCompletion(Integer requiredCompletion) {
        this.requiredCompletion = requiredCompletion;
    }

    public Integer getRequiredLevel() {
        return requiredLevel;
    }

    public ProgramCohortCollection requiredLevel(Integer requiredLevel) {
        this.requiredLevel = requiredLevel;
        return this;
    }

    public void setRequiredLevel(Integer requiredLevel) {
        this.requiredLevel = requiredLevel;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ProgramCohortCollection createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public ProgramCohortCollection modifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public ProgramCollection getProgramCollection() {
        return programCollection;
    }

    public ProgramCohortCollection programCollection(ProgramCollection programCollection) {
        this.programCollection = programCollection;
        return this;
    }

    public void setProgramCollection(ProgramCollection programCollection) {
        this.programCollection = programCollection;
    }

    public ProgramCohort getProgramCohort() {
        return programCohort;
    }

    public ProgramCohortCollection programCohort(ProgramCohort programCohort) {
        this.programCohort = programCohort;
        return this;
    }

    public void setProgramCohort(ProgramCohort programCohort) {
        this.programCohort = programCohort;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramCohortCollection)) {
            return false;
        }
        return id != null && id.equals(((ProgramCohortCollection) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramCohortCollection{" +
            "id=" + getId() +
            ", requiredCompletion=" + getRequiredCompletion() +
            ", requiredLevel=" + getRequiredLevel() +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
