package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A EConsent.
 */
@Entity
@Table(name = "e_consent")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EConsent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 36)
    @Column(name = "participant_id", length = 36)
    private String participantId;

    @NotNull
    @Column(name = "program_id", nullable = false)
    private Long programId;

    @Column(name = "has_confirmed")
    private Boolean hasConfirmed;

    @Column(name = "created_at")
    private Instant createdAt;

    @NotNull
    @Column(name = "term_and_condition_id", nullable = false)
    private Long termAndConditionId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public EConsent participantId(String participantId) {
        this.participantId = participantId;
        return this;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public Long getProgramId() {
        return programId;
    }

    public EConsent programId(Long programId) {
        this.programId = programId;
        return this;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Boolean isHasConfirmed() {
        return hasConfirmed;
    }

    public EConsent hasConfirmed(Boolean hasConfirmed) {
        this.hasConfirmed = hasConfirmed;
        return this;
    }

    public void setHasConfirmed(Boolean hasConfirmed) {
        this.hasConfirmed = hasConfirmed;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public EConsent createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Long getTermAndConditionId() {
        return termAndConditionId;
    }

    public EConsent termAndConditionId(Long termAndConditionId) {
        this.termAndConditionId = termAndConditionId;
        return this;
    }

    public void setTermAndConditionId(Long termAndConditionId) {
        this.termAndConditionId = termAndConditionId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EConsent)) {
            return false;
        }
        return id != null && id.equals(((EConsent) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "EConsent{" +
            "id=" + getId() +
            ", participantId='" + getParticipantId() + "'" +
            ", programId=" + getProgramId() +
            ", hasConfirmed='" + isHasConfirmed() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", termAndConditionId=" + getTermAndConditionId() +
            "}";
    }
}
