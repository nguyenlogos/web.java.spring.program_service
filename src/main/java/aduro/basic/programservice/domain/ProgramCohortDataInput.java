package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ProgramCohortDataInput.
 */
@Entity
@Table(name = "program_cohort_data_input")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramCohortDataInput implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 150)
    @Column(name = "name_data_input", length = 150)
    private String nameDataInput;

    @Size(max = 100)
    @Column(name = "input_type", length = 100)
    private String inputType;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @NotNull
    @Column(name = "modified_date", nullable = false)
    private Instant modifiedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameDataInput() {
        return nameDataInput;
    }

    public ProgramCohortDataInput nameDataInput(String nameDataInput) {
        this.nameDataInput = nameDataInput;
        return this;
    }

    public void setNameDataInput(String nameDataInput) {
        this.nameDataInput = nameDataInput;
    }

    public String getInputType() {
        return inputType;
    }

    public ProgramCohortDataInput inputType(String inputType) {
        this.inputType = inputType;
        return this;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ProgramCohortDataInput createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public ProgramCohortDataInput modifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramCohortDataInput)) {
            return false;
        }
        return id != null && id.equals(((ProgramCohortDataInput) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramCohortDataInput{" +
            "id=" + getId() +
            ", nameDataInput='" + getNameDataInput() + "'" +
            ", inputType='" + getInputType() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
