package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A OrderTransaction.
 */
@Entity
@Table(name = "order_transaction")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrderTransaction implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "program_name")
    private String programName;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "participant_name")
    private String participantName;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "message")
    private String message;

    @Column(name = "json_content")
    private String jsonContent;

    @Column(name = "email")
    private String email;

    @Column(name = "external_order_id")
    private String externalOrderId;

    @Column(name = "program_id")
    private Long programId;

    @Column(name = "user_reward_id")
    private Long userRewardId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProgramName() {
        return programName;
    }

    public OrderTransaction programName(String programName) {
        this.programName = programName;
        return this;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getClientName() {
        return clientName;
    }

    public OrderTransaction clientName(String clientName) {
        this.clientName = clientName;
        return this;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getParticipantName() {
        return participantName;
    }

    public OrderTransaction participantName(String participantName) {
        this.participantName = participantName;
        return this;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public OrderTransaction createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getMessage() {
        return message;
    }

    public OrderTransaction message(String message) {
        this.message = message;
        return this;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getJsonContent() {
        return jsonContent;
    }

    public OrderTransaction jsonContent(String jsonContent) {
        this.jsonContent = jsonContent;
        return this;
    }

    public void setJsonContent(String jsonContent) {
        this.jsonContent = jsonContent;
    }

    public String getEmail() {
        return email;
    }

    public OrderTransaction email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getExternalOrderId() {
        return externalOrderId;
    }

    public OrderTransaction externalOrderId(String externalOrderId) {
        this.externalOrderId = externalOrderId;
        return this;
    }

    public void setExternalOrderId(String externalOrderId) {
        this.externalOrderId = externalOrderId;
    }

    public Long getProgramId() {
        return programId;
    }

    public OrderTransaction programId(Long programId) {
        this.programId = programId;
        return this;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Long getUserRewardId() {
        return userRewardId;
    }

    public OrderTransaction userRewardId(Long userRewardId) {
        this.userRewardId = userRewardId;
        return this;
    }

    public void setUserRewardId(Long userRewardId) {
        this.userRewardId = userRewardId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderTransaction)) {
            return false;
        }
        return id != null && id.equals(((OrderTransaction) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrderTransaction{" +
            "id=" + getId() +
            ", programName='" + getProgramName() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", participantName='" + getParticipantName() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", message='" + getMessage() + "'" +
            ", jsonContent='" + getJsonContent() + "'" +
            ", email='" + getEmail() + "'" +
            ", externalOrderId='" + getExternalOrderId() + "'" +
            ", programId=" + getProgramId() +
            ", userRewardId=" + getUserRewardId() +
            "}";
    }
}
