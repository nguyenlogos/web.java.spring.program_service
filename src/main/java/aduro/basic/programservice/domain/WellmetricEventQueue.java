package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import aduro.basic.programservice.domain.enumeration.ResultStatus;

import aduro.basic.programservice.domain.enumeration.Gender;

/**
 * A WellmetricEventQueue.
 */
@Entity
@Table(name = "wellmetric_event_queue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class WellmetricEventQueue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 36)
    @Column(name = "participant_id", length = 36, nullable = false)
    private String participantId;

    @NotNull
    @Size(max = 36)
    @Column(name = "client_id", length = 36, nullable = false)
    private String clientId;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "error_message")
    private String errorMessage;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "result", nullable = false)
    private ResultStatus result;

    @NotNull
    @Column(name = "event_code", nullable = false)
    private String eventCode;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "gender", nullable = false)
    private Gender gender;

    @Column(name = "attempt_count")
    private Integer attemptCount;

    @NotNull
    @Column(name = "last_attempt_time", nullable = false)
    private Instant lastAttemptTime;

    @NotNull
    @Column(name = "created_at", nullable = false)
    private Instant createdAt;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @NotNull
    @Column(name = "healthy_value", nullable = false)
    private Float healthyValue;

    @NotNull
    @Column(name = "execute_status", nullable = false)
    private String executeStatus;

    @NotNull
    @Column(name = "event_id", nullable = false)
    private String eventId;

    @Column(name = "has_incentive")
    private Boolean hasIncentive;

    @Column(name = "is_waiting_push")
    private Boolean isWaitingPush;

    @NotNull
    @Column(name = "attempted_at", nullable = false)
    private Instant attemptedAt;

    @NotNull
    @Column(name = "program_id", nullable = false)
    private Long programId;

    @NotNull
    @Column(name = "event_date", nullable = false)
    private String eventDate;

    @Column(name = "sub_category_code")
    private String subCategoryCode;


    @Column(name = "appeal_applied")
    private boolean appealApplied;

    @Column(name = "appeal_event_id")
    private String appealEventId;

    @NotNull
    @Column(name = "modified_date", nullable = false)
    private Instant modifiedDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public WellmetricEventQueue participantId(String participantId) {
        this.participantId = participantId;
        return this;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public WellmetricEventQueue clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getEmail() {
        return email;
    }

    public WellmetricEventQueue email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public WellmetricEventQueue errorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ResultStatus getResult() {
        return result;
    }

    public WellmetricEventQueue result(ResultStatus result) {
        this.result = result;
        return this;
    }

    public void setResult(ResultStatus result) {
        this.result = result;
    }

    public String getEventCode() {
        return eventCode;
    }

    public WellmetricEventQueue eventCode(String eventCode) {
        this.eventCode = eventCode;
        return this;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public Gender getGender() {
        return gender;
    }

    public WellmetricEventQueue gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Integer getAttemptCount() {
        return attemptCount;
    }

    public WellmetricEventQueue attemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
        return this;
    }

    public void setAttemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
    }

    public Instant getLastAttemptTime() {
        return lastAttemptTime;
    }

    public WellmetricEventQueue lastAttemptTime(Instant lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
        return this;
    }

    public void setLastAttemptTime(Instant lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public WellmetricEventQueue createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public WellmetricEventQueue subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public Float getHealthyValue() {
        return healthyValue;
    }

    public WellmetricEventQueue healthyValue(Float healthyValue) {
        this.healthyValue = healthyValue;
        return this;
    }

    public void setHealthyValue(Float healthyValue) {
        this.healthyValue = healthyValue;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public WellmetricEventQueue executeStatus(String executeStatus) {
        this.executeStatus = executeStatus;
        return this;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public String getEventId() {
        return eventId;
    }

    public WellmetricEventQueue eventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Boolean isHasIncentive() {
        return hasIncentive;
    }

    public WellmetricEventQueue hasIncentive(Boolean hasIncentive) {
        this.hasIncentive = hasIncentive;
        return this;
    }

    public void setHasIncentive(Boolean hasIncentive) {
        this.hasIncentive = hasIncentive;
    }

    public Boolean isIsWaitingPush() {
        return isWaitingPush;
    }

    public WellmetricEventQueue isWaitingPush(Boolean isWaitingPush) {
        this.isWaitingPush = isWaitingPush;
        return this;
    }

    public void setIsWaitingPush(Boolean isWaitingPush) {
        this.isWaitingPush = isWaitingPush;
    }

    public Instant getAttemptedAt() {
        return attemptedAt;
    }

    public WellmetricEventQueue attemptedAt(Instant attemptedAt) {
        this.attemptedAt = attemptedAt;
        return this;
    }

    public void setAttemptedAt(Instant attemptedAt) {
        this.attemptedAt = attemptedAt;
    }

    public Long getProgramId() {
        return programId;
    }

    public WellmetricEventQueue programId(Long programId) {
        this.programId = programId;
        return this;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getEventDate() {
        return eventDate;
    }

    public WellmetricEventQueue eventDate(String eventDate) {
        this.eventDate = eventDate;
        return this;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getSubCategoryCode() {
        return subCategoryCode;
    }

    public WellmetricEventQueue subCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
        return this;
    }

    public void setSubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof WellmetricEventQueue)) {
            return false;
        }
        return id != null && id.equals(((WellmetricEventQueue) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "WellmetricEventQueue{" +
            "id=" + getId() +
            ", participantId='" + getParticipantId() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", email='" + getEmail() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", result='" + getResult() + "'" +
            ", eventCode='" + getEventCode() + "'" +
            ", gender='" + getGender() + "'" +
            ", attemptCount=" + getAttemptCount() +
            ", lastAttemptTime='" + getLastAttemptTime() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", healthyValue=" + getHealthyValue() +
            ", executeStatus='" + getExecuteStatus() + "'" +
            ", eventId='" + getEventId() + "'" +
            ", hasIncentive='" + isHasIncentive() + "'" +
            ", isWaitingPush='" + isIsWaitingPush() + "'" +
            ", attemptedAt='" + getAttemptedAt() + "'" +
            ", programId=" + getProgramId() +
            ", eventDate='" + getEventDate() + "'" +
            ", subCategoryCode='" + getSubCategoryCode() + "'" +
            "}";
    }

    public boolean isAppealApplied() {
        return appealApplied;
    }

    public void setAppealApplied(boolean appealApplied) {
        this.appealApplied = appealApplied;
    }

    public String getAppealEventId() {
        return appealEventId;
    }

    public void setAppealEventId(String appealEventId) {
        this.appealEventId = appealEventId;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
