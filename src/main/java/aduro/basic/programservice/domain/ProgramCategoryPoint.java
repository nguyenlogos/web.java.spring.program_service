package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * A ProgramCategoryPoint.
 */
@Entity
@Table(name = "program_category_point")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramCategoryPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "category_code", nullable = false)
    private String categoryCode;

    @Column(name = "category_name")
    private String categoryName;

    @Column(name = "percent_point", precision = 19, scale = 4)
    private BigDecimal percentPoint;

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    @Column(name = "locked")
    private Boolean locked;

    @Column(name = "value_point", precision = 21, scale = 2)
    private BigDecimal valuePoint;

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    @Column(name = "program_id", insertable = false, updatable = false)
    private Long programId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @NotNull
    private Program program;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public ProgramCategoryPoint categoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
        return this;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public ProgramCategoryPoint categoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public BigDecimal getPercentPoint() {
        return percentPoint;
    }

    public ProgramCategoryPoint percentPoint(BigDecimal percentPoint) {
        this.percentPoint = percentPoint;
        return this;
    }

    public void setPercentPoint(BigDecimal percentPoint) {
        this.percentPoint = percentPoint;
    }

    public BigDecimal getValuePoint() {
        return valuePoint;
    }

    public ProgramCategoryPoint valuePoint(BigDecimal valuePoint) {
        this.valuePoint = valuePoint;
        return this;
    }

    public void setValuePoint(BigDecimal valuePoint) {
        this.valuePoint = valuePoint;
    }

    public Program getProgram() {
        return program;
    }

    public ProgramCategoryPoint program(Program program) {
        this.program = program;
        return this;
    }

    public void setProgram(Program program) {
        this.program = program;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramCategoryPoint)) {
            return false;
        }
        return id != null && id.equals(((ProgramCategoryPoint) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramCategoryPoint{" +
            "id=" + getId() +
            ", categoryCode='" + getCategoryCode() + "'" +
            ", categoryName='" + getCategoryName() + "'" +
            ", percentPoint=" + getPercentPoint() +
            ", valuePoint=" + getValuePoint() +
            "}";
    }

    @OneToMany(mappedBy = "programCategoryPoint", fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramSubCategoryPoint> programSubCategoryPoints = new HashSet<>();

    public Set<ProgramSubCategoryPoint> getProgramSubCategoryPoints() {
        return this.programSubCategoryPoints;
    }

    public ProgramCategoryPoint setProgramSubCategoryPoints(Set<ProgramSubCategoryPoint> subCategoryPoints) {
        this.programSubCategoryPoints = subCategoryPoints;
        return this;
    }

    public ProgramCategoryPoint addProgramSubCategoryPoint(ProgramSubCategoryPoint programSubCategoryPoint) {
        this.programSubCategoryPoints.add(programSubCategoryPoint);
        programSubCategoryPoint.setProgramCategoryPoint(this);
        return this;
    }

    public ProgramCategoryPoint removeProgramSubCategoryPoint(ProgramSubCategoryPoint programSubCategoryPoint) {
        this.programSubCategoryPoints.remove(programSubCategoryPoint);
        programSubCategoryPoint.setProgramCategoryPoint(null);
        return this;
    }

    public ProgramCategoryPoint locked(Boolean locked) {
        this.locked = locked;
        return this;
    }

    public ProgramCategoryPoint programId(Long programId) {
        this.programId = programId;
        return this;
    }
}
