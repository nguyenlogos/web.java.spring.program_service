package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ProgramSubCategoryConfiguration.
 */
@Entity
@Table(name = "program_sc_point_configuration")
public class ProgramSubCategoryConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "sub_category_code", nullable = false)
    private String subCategoryCode;

    @Column(name = "is_blood_pressure_single_test")
    private Boolean isBloodPressureSingleTest;

    @Column(name = "is_blood_pressure_individual_test")
    private Boolean isBloodPressureIndividualTest;

    @Column(name = "is_glucose_awarded_in_range")
    private Boolean isGlucoseAwardedInRange;

    @Column(name = "is_bmi_awarded_in_range")
    private Boolean isBmiAwardedInRange;

    @Column(name = "is_bmi_awarded_fasting")
    private Boolean isBmiAwardedFasting;

    @Column(name = "is_bmi_awarded_non_fasting")
    private Boolean isBmiAwardedNonFasting;

    @NotNull
    @Column(name = "program_id", nullable = false)
    private Long programId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubCategoryCode() {
        return subCategoryCode;
    }

    public ProgramSubCategoryConfiguration subCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
        return this;
    }

    public void setSubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public Boolean isIsBloodPressureSingleTest() {
        return isBloodPressureSingleTest;
    }

    public ProgramSubCategoryConfiguration isBloodPressureSingleTest(Boolean isBloodPressureSingleTest) {
        this.isBloodPressureSingleTest = isBloodPressureSingleTest;
        return this;
    }

    public void setIsBloodPressureSingleTest(Boolean isBloodPressureSingleTest) {
        this.isBloodPressureSingleTest = isBloodPressureSingleTest;
    }

    public Boolean isIsBloodPressureIndividualTest() {
        return isBloodPressureIndividualTest;
    }

    public ProgramSubCategoryConfiguration isBloodPressureIndividualTest(Boolean isBloodPressureIndividualTest) {
        this.isBloodPressureIndividualTest = isBloodPressureIndividualTest;
        return this;
    }

    public void setIsBloodPressureIndividualTest(Boolean isBloodPressureIndividualTest) {
        this.isBloodPressureIndividualTest = isBloodPressureIndividualTest;
    }

    public Boolean isIsGlucoseAwardedInRange() {
        return isGlucoseAwardedInRange;
    }

    public ProgramSubCategoryConfiguration isGlucoseAwardedInRange(Boolean isGlucoseAwardedInRange) {
        this.isGlucoseAwardedInRange = isGlucoseAwardedInRange;
        return this;
    }

    public void setIsGlucoseAwardedInRange(Boolean isGlucoseAwardedInRange) {
        this.isGlucoseAwardedInRange = isGlucoseAwardedInRange;
    }

    public Boolean isIsBmiAwardedInRange() {
        return isBmiAwardedInRange;
    }

    public ProgramSubCategoryConfiguration isBmiAwardedInRange(Boolean isBmiAwardedInRange) {
        this.isBmiAwardedInRange = isBmiAwardedInRange;
        return this;
    }

    public void setIsBmiAwardedInRange(Boolean isBmiAwardedInRange) {
        this.isBmiAwardedInRange = isBmiAwardedInRange;
    }

    public Boolean isIsBmiAwardedFasting() {
        return isBmiAwardedFasting;
    }

    public ProgramSubCategoryConfiguration isBmiAwardedFasting(Boolean isBmiAwardedFasting) {
        this.isBmiAwardedFasting = isBmiAwardedFasting;
        return this;
    }

    public void setIsBmiAwardedFasting(Boolean isBmiAwardedFasting) {
        this.isBmiAwardedFasting = isBmiAwardedFasting;
    }

    public Boolean isIsBmiAwardedNonFasting() {
        return isBmiAwardedNonFasting;
    }

    public ProgramSubCategoryConfiguration isBmiAwardedNonFasting(Boolean isBmiAwardedNonFasting) {
        this.isBmiAwardedNonFasting = isBmiAwardedNonFasting;
        return this;
    }

    public void setIsBmiAwardedNonFasting(Boolean isBmiAwardedNonFasting) {
        this.isBmiAwardedNonFasting = isBmiAwardedNonFasting;
    }

    public Long getProgramId() {
        return programId;
    }

    public ProgramSubCategoryConfiguration programId(Long programId) {
        this.programId = programId;
        return this;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramSubCategoryConfiguration)) {
            return false;
        }
        return id != null && id.equals(((ProgramSubCategoryConfiguration) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramSubCategoryConfiguration{" +
            "id=" + getId() +
            ", subCategoryCode='" + getSubCategoryCode() + "'" +
            ", isBloodPressureSingleTest='" + isIsBloodPressureSingleTest() + "'" +
            ", isBloodPressureIndividualTest='" + isIsBloodPressureIndividualTest() + "'" +
            ", isGlucoseAwardedInRange='" + isIsGlucoseAwardedInRange() + "'" +
            ", isBmiAwardedInRange='" + isIsBmiAwardedInRange() + "'" +
            ", isBmiAwardedFasting='" + isIsBmiAwardedFasting() + "'" +
            ", isBmiAwardedNonFasting='" + isIsBmiAwardedNonFasting() + "'" +
            ", programId=" + getProgramId() +
            "}";
    }
}
