package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ProgramIntegrationTracking.
 */
@Entity
@Table(name = "program_integration_tracking")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramIntegrationTracking implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "end_point")
    private String endPoint;

    @Column(name = "payload")
    private String payload;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "status")
    private String status;

    @Column(name = "service_type")
    private String serviceType;

    @Column(name = "attempted_at")
    private Instant attemptedAt;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public ProgramIntegrationTracking endPoint(String endPoint) {
        this.endPoint = endPoint;
        return this;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getPayload() {
        return payload;
    }

    public ProgramIntegrationTracking payload(String payload) {
        this.payload = payload;
        return this;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public ProgramIntegrationTracking createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public ProgramIntegrationTracking errorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatus() {
        return status;
    }

    public ProgramIntegrationTracking status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getServiceType() {
        return serviceType;
    }

    public ProgramIntegrationTracking serviceType(String serviceType) {
        this.serviceType = serviceType;
        return this;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public Instant getAttemptedAt() {
        return attemptedAt;
    }

    public ProgramIntegrationTracking attemptedAt(Instant attemptedAt) {
        this.attemptedAt = attemptedAt;
        return this;
    }

    public void setAttemptedAt(Instant attemptedAt) {
        this.attemptedAt = attemptedAt;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramIntegrationTracking)) {
            return false;
        }
        return id != null && id.equals(((ProgramIntegrationTracking) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramIntegrationTracking{" +
            "id=" + getId() +
            ", endPoint='" + getEndPoint() + "'" +
            ", payload='" + getPayload() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", status='" + getStatus() + "'" +
            ", serviceType='" + getServiceType() + "'" +
            ", attemptedAt='" + getAttemptedAt() + "'" +
            "}";
    }
}
