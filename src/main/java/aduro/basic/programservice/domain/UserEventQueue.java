package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * A UserEventQueue.
 */
@Entity
@Table(name = "user_event_queue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserEventQueue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "event_code", nullable = false)
    private String eventCode;

    @NotNull
    @Column(name = "event_id", nullable = false)
    private String eventId;

    @NotNull
    @Column(name = "event_date", nullable = false)
    private Instant eventDate;

    @Column(name = "event_point", precision = 21, scale = 2)
    private BigDecimal eventPoint;

    @Column(name = "event_category")
    private String eventCategory;

    @NotNull
    @Column(name = "program_user_id", nullable = false)
    private Long programUserId;

    @NotNull
    @Column(name = "execute_status", nullable = false)
    private String executeStatus;

    @NotNull
    @Column(name = "program_id", nullable = false)
    private Long programId;

    @Column(name = "read_at")
    private Instant readAt;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "current_step")
    private String currentStep;

    @Column(name = "attempt_count")
    private int attemptCount;

    @Column(name = "created_by_admin")
    private String createdByAdmin;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "preventive_event_id")
    private String preventiveEventId;

    @Column(name = "is_migration")
    private boolean isMigration;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventCode() {
        return eventCode;
    }

    public UserEventQueue eventCode(String eventCode) {
        this.eventCode = eventCode;
        return this;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public String getEventId() {
        return eventId;
    }

    public UserEventQueue eventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Instant getEventDate() {
        return eventDate;
    }

    public UserEventQueue eventDate(Instant eventDate) {
        this.eventDate = eventDate;
        return this;
    }

    public void setEventDate(Instant eventDate) {
        this.eventDate = eventDate;
    }

    public BigDecimal getEventPoint() {
        return eventPoint;
    }

    public UserEventQueue eventPoint(BigDecimal eventPoint) {
        this.eventPoint = eventPoint;
        return this;
    }

    public void setEventPoint(BigDecimal eventPoint) {
        this.eventPoint = eventPoint;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public UserEventQueue eventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
        return this;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public Long getProgramUserId() {
        return programUserId;
    }

    public UserEventQueue programUserId(Long programUserId) {
        this.programUserId = programUserId;
        return this;
    }

    public void setProgramUserId(Long programUserId) {
        this.programUserId = programUserId;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public UserEventQueue executeStatus(String executeStatus) {
        this.executeStatus = executeStatus;
        return this;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public Long getProgramId() {
        return programId;
    }

    public UserEventQueue programId(Long programId) {
        this.programId = programId;
        return this;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Instant getReadAt() {
        return readAt;
    }

    public UserEventQueue readAt(Instant readAt) {
        this.readAt = readAt;
        return this;
    }

    public void setReadAt(Instant readAt) {
        this.readAt = readAt;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public UserEventQueue errorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getCurrentStep() {
        return currentStep;
    }

    public UserEventQueue currentStep(String currentStep) {
        this.currentStep = currentStep;
        return this;
    }

    public void setCurrentStep(String currentStep) {
        this.currentStep = currentStep;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public int getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(int attemptCount) {
        this.attemptCount = attemptCount;
    }

    public String getCreatedByAdmin() {
        return createdByAdmin;
    }

    public void setCreatedByAdmin(String createdByAdmin) {
        this.createdByAdmin = createdByAdmin;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserEventQueue)) {
            return false;
        }
        return id != null && id.equals(((UserEventQueue) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserEventQueue{" +
            "id=" + getId() +
            ", eventCode='" + getEventCode() + "'" +
            ", eventId='" + getEventId() + "'" +
            ", eventDate='" + getEventDate() + "'" +
            ", eventPoint=" + getEventPoint() +
            ", eventCategory='" + getEventCategory() + "'" +
            ", programUserId=" + getProgramUserId() +
            ", executeStatus='" + getExecuteStatus() + "'" +
            ", programId=" + getProgramId() +
            ", readAt='" + getReadAt() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", currentStep='" + getCurrentStep() + "'" +
            "}";
    }

    public String getPreventiveEventId() {
        return preventiveEventId;
    }

    public void setPreventiveEventId(String preventiveEventId) {
        this.preventiveEventId = preventiveEventId;
    }

    public boolean isMigration() {
        return isMigration;
    }

    public void setMigration(boolean migration) {
        isMigration = migration;
    }
}
