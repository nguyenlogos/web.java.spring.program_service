package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A ProgramActivity.
 */
@Entity
@Table(name = "program_activity")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)

public class ProgramActivity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "activity_id", nullable = false)
    private String activityId;

    @NotNull
    @Column(name = "activity_code", nullable = false)
    private String activityCode;


    public Boolean isIsCustomized() {
        return isCustomized;
    }

    public ProgramActivity isCustomized(Boolean isCustomized) {
        this.isCustomized = isCustomized;
        return this;
    }

    public void setIsCustomized(Boolean isCustomized) {
        this.isCustomized = isCustomized;
    }

    @Column(name = "is_customized", nullable = false)
    private boolean isCustomized;

    @Column(name = "master_id")
    private String masterId;

    public Long getProgramPhaseId() {
        return programPhaseId;
    }

    public void setProgramPhaseId(Long programPhaseId) {
        this.programPhaseId = programPhaseId;
    }

    @Column(name = "program_phase_id", insertable = false, updatable = false)
    private Long programPhaseId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @NotNull
    @JsonIgnoreProperties("programActivities")
    private Program program;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JsonIgnoreProperties("programActivities")
    @JoinColumn(name = "program_phase_id")
    private ProgramPhase programPhase;

    @Column(name = "bonus_points_enabled")
    private Boolean bonusPointsEnabled;

    @Column(name = "bonus_points", precision = 21, scale = 2)
    private BigDecimal bonusPoints;

    public Boolean isBonusPointsEnabled() {
        return bonusPointsEnabled;
    }

    public void setBonusPointsEnabled(Boolean bonusPointsEnabled) {
        this.bonusPointsEnabled = bonusPointsEnabled;
    }

    public BigDecimal getBonusPoints() {
        return bonusPoints;
    }

    public void setBonusPoints(BigDecimal bonusPoints) {
        this.bonusPoints = bonusPoints;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    @Column(name = "program_id", insertable = false, updatable = false)
    private Long programId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActivityId() {
        return activityId;
    }

    public ProgramActivity activityId(String activityId) {
        this.activityId = activityId;
        return this;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public String getActivityCode() {
        return activityCode;
    }

    public ProgramActivity activityCode(String activityCode) {
        this.activityCode = activityCode;
        return this;
    }

    public void setActivityCode(String activityCode) {
        this.activityCode = activityCode;
    }


    public Program getProgram() {
        return program;
    }

    public ProgramActivity program(Program program) {
        this.program = program;
        return this;
    }

    public void setProgram(Program program) {
        this.program = program;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public ProgramPhase getProgramPhase() {
        return programPhase;
    }

    public ProgramActivity programPhase(ProgramPhase programPhase) {
        this.programPhase = programPhase;
        return this;
    }

    public void setProgramPhase(ProgramPhase programPhase) {
        this.programPhase = programPhase;
    }

    public String getMasterId() {
        return masterId;
    }

    public ProgramActivity masterId(String masterId) {
        this.masterId = masterId;
        return this;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramActivity)) {
            return false;
        }
        return id != null && id.equals(((ProgramActivity) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramActivity{" +
            "id=" + getId() +
            ", activityId='" + getActivityId() + "'" +
            ", activityCode='" + getActivityCode() + "'" +
            ", isCustomized='" + isIsCustomized() + "'" +
            "}";
    }
}
