package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import aduro.basic.programservice.domain.enumeration.NotificationType;

/**
 * A NotificationCenter.
 */
@Entity
@Table(name = "notification_center")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NotificationCenter implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Column(name = "participant_id", nullable = false)
    private String participantId;

    @NotNull
    @Column(name = "execute_status", nullable = false)
    private String executeStatus;

    @NotNull
    @Column(name = "created_at", nullable = false)
    private Instant createdAt;

    @Column(name = "attempt_count")
    private Integer attemptCount;

    @NotNull
    @Column(name = "last_attempt_time", nullable = false)
    private Instant lastAttemptTime;

    @Column(name = "error_message")
    private String errorMessage;

    @NotNull
    @Column(name = "content", nullable = false)
    private String content;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "notification_type", nullable = false)
    private NotificationType notificationType;

    @NotNull
    @Column(name = "event_id", nullable = false)
    private String eventId;

    @Column(name = "is_ready")
    private Boolean isReady;

    @NotNull
    @Column(name = "receiver_name", nullable = false)
    private String receiverName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public NotificationCenter title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParticipantId() {
        return participantId;
    }

    public NotificationCenter participantId(String participantId) {
        this.participantId = participantId;
        return this;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public NotificationCenter executeStatus(String executeStatus) {
        this.executeStatus = executeStatus;
        return this;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public NotificationCenter createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getAttemptCount() {
        return attemptCount;
    }

    public NotificationCenter attemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
        return this;
    }

    public void setAttemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
    }

    public Instant getLastAttemptTime() {
        return lastAttemptTime;
    }

    public NotificationCenter lastAttemptTime(Instant lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
        return this;
    }

    public void setLastAttemptTime(Instant lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public NotificationCenter errorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
        return this;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getContent() {
        return content;
    }

    public NotificationCenter content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NotificationType getNotificationType() {
        return notificationType;
    }

    public NotificationCenter notificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
        return this;
    }

    public void setNotificationType(NotificationType notificationType) {
        this.notificationType = notificationType;
    }

    public String getEventId() {
        return eventId;
    }

    public NotificationCenter eventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public Boolean isIsReady() {
        return isReady;
    }

    public NotificationCenter isReady(Boolean isReady) {
        this.isReady = isReady;
        return this;
    }

    public void setIsReady(Boolean isReady) {
        this.isReady = isReady;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public NotificationCenter receiverName(String receiverName) {
        this.receiverName = receiverName;
        return this;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NotificationCenter)) {
            return false;
        }
        return id != null && id.equals(((NotificationCenter) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NotificationCenter{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", participantId='" + getParticipantId() + "'" +
            ", executeStatus='" + getExecuteStatus() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", attemptCount=" + getAttemptCount() +
            ", lastAttemptTime='" + getLastAttemptTime() + "'" +
            ", errorMessage='" + getErrorMessage() + "'" +
            ", content='" + getContent() + "'" +
            ", notificationType='" + getNotificationType() + "'" +
            ", eventId='" + getEventId() + "'" +
            ", isReady='" + isIsReady() + "'" +
            ", receiverName='" + getReceiverName() + "'" +
            "}";
    }
}
