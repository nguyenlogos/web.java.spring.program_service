package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A ProgramSubCategoryPoint.
 */
@Entity
@Table(name = "program_sub_category_point")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramSubCategoryPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "percent_point", precision = 21, scale = 2)
    private BigDecimal percentPoint;

    @Column(name = "value_point", precision = 21, scale = 2)
    private BigDecimal valuePoint;

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    @Column(name = "locked")
    private Boolean locked;

    @Column(name = "completions_cap")
    private Integer completionsCap;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @NotNull
    @JsonIgnoreProperties("programSubCategoryPoints")
    @JoinColumn(name = "program_category_point_id")
    private ProgramCategoryPoint programCategoryPoint;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Column(name = "program_category_point_id", insertable = false, updatable = false)
    private Long programCategoryPointId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public ProgramSubCategoryPoint code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getPercentPoint() {
        return percentPoint;
    }

    public ProgramSubCategoryPoint percentPoint(BigDecimal percentPoint) {
        this.percentPoint = percentPoint;
        return this;
    }

    public void setPercentPoint(BigDecimal percentPoint) {
        this.percentPoint = percentPoint;
    }

    public BigDecimal getValuePoint() {
        return valuePoint;
    }

    public ProgramSubCategoryPoint valuePoint(BigDecimal valuePoint) {
        this.valuePoint = valuePoint;
        return this;
    }

    public void setValuePoint(BigDecimal valuePoint) {
        this.valuePoint = valuePoint;
    }

    public Integer getCompletionsCap() {
        return completionsCap;
    }

    public ProgramSubCategoryPoint completionsCap(Integer completionsCap) {
        this.completionsCap = completionsCap;
        return this;
    }

    public void setCompletionsCap(Integer completionsCap) {
        this.completionsCap = completionsCap;
    }

    public ProgramCategoryPoint getProgramCategoryPoint() {
        return programCategoryPoint;
    }

    public ProgramSubCategoryPoint programCategoryPoint(ProgramCategoryPoint programCategoryPoint) {
        this.programCategoryPoint = programCategoryPoint;
        return this;
    }

    public void setProgramCategoryPoint(ProgramCategoryPoint programCategoryPoint) {
        this.programCategoryPoint = programCategoryPoint;
    }

    public Long getProgramCategoryPointId() {
        return programCategoryPointId;
    }

    public void setProgramCategoryPointId(Long programCategoryPointId) {
        this.programCategoryPointId = programCategoryPointId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramSubCategoryPoint)) {
            return false;
        }
        return id != null && id.equals(((ProgramSubCategoryPoint) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramSubCategoryPoint{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", percentPoint=" + getPercentPoint() +
            ", valuePoint=" + getValuePoint() +
            ", completionsCap=" + getCompletionsCap() +
            "}";
    }
}
