package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A ProgramUser.
 */
@Entity
@Table(name = "program_user")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramUser extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "participant_id", nullable = false)
    private String participantId;

    @NotNull
    @Column(name = "client_id", nullable = false)
    private String clientId;

    @Column(name = "total_user_point", precision = 21, scale = 2)
    private BigDecimal totalUserPoint;

    @NotNull
    @Column(name = "program_id", nullable = false)
    private Long programId;

    @Column(name = "current_level")
    private Integer currentLevel;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "subgroup_name")
    private String subgroupName;

    @Column(name = "is_tobacco_user")
    private Boolean isTobaccoUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public Boolean isIsTobaccoUser() {
        return isTobaccoUser;
    }

    public ProgramUser isTobaccoUser(Boolean isTobaccoUser) {
        this.isTobaccoUser = isTobaccoUser;
        return this;
    }

    public void setIsTobaccoUser(Boolean isTobaccoUser) {
        this.isTobaccoUser = isTobaccoUser;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public ProgramUser subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public ProgramUser subgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
        return this;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public ProgramUser participantId(String participantId) {
        this.participantId = participantId;
        return this;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public ProgramUser clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public BigDecimal getTotalUserPoint() {
        return totalUserPoint;
    }

    public ProgramUser totalUserPoint(BigDecimal totalUserPoint) {
        this.totalUserPoint = totalUserPoint;
        return this;
    }

    public void setTotalUserPoint(BigDecimal totalUserPoint) {
        this.totalUserPoint = totalUserPoint;
    }

    public Long getProgramId() {
        return programId;
    }

    public ProgramUser programId(Long programId) {
        this.programId = programId;
        return this;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public Integer getCurrentLevel() {
        return currentLevel;
    }

    public ProgramUser currentLevel(Integer currentLevel) {
        this.currentLevel = currentLevel;
        return this;
    }

    public void setCurrentLevel(Integer currentLevel) {
        this.currentLevel = currentLevel;
    }

    public String getEmail() {
        return email;
    }

    public ProgramUser email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public ProgramUser firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ProgramUser lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public ProgramUser phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getClientName() {
        return clientName;
    }

    public ProgramUser clientName(String clientName) {
        this.clientName = clientName;
        return this;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramUser)) {
            return false;
        }
        return id != null && id.equals(((ProgramUser) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramUser{" +
            "id=" + getId() +
            ", participantId='" + getParticipantId() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", totalUserPoint=" + getTotalUserPoint() +
            ", programId=" + getProgramId() +
            ", currentLevel=" + getCurrentLevel() +
            ", email='" + getEmail() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", phone='" + getPhone() + "'" +
            ", clientName='" + getClientName() + "'" +
            "}";
    }

    public Set<UserEvent> getUserEvents() {
        return userEvents;
    }

    public void setUserEvents(Set<UserEvent> userEvents) {
        this.userEvents = userEvents;
    }


    @OneToMany(mappedBy = "programUser", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private Set<UserEvent> userEvents = new HashSet<>();

    public Set<UserReward> getUserRewards() {
        return userRewards;
    }

    public void setUserRewards(Set<UserReward> userRewards) {
        this.userRewards = userRewards;
    }

    @OneToMany(mappedBy = "programUser", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private Set<UserReward> userRewards = new HashSet<>();

    @OneToMany(mappedBy = "programUser", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    private Set<ProgramUserCohort> programUserCohorts = new HashSet<>();

    public Set<ProgramUserCohort> getProgramUserCohorts() {
        return programUserCohorts;
    }

    public void setProgramUserCohorts(Set<ProgramUserCohort> programUserCohorts) {
        this.programUserCohorts = programUserCohorts;
    }

}
