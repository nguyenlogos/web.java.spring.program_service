package aduro.basic.programservice.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Table(name = "process_additional_user_points_queue")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SupportAdditionalUserPointQueue implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "participant_id", nullable = false)
    private String participantId;

    @NotNull
    @Column(name = "client_id", nullable = false)
    private String clientId;

    @NotNull
    @Column(name = "program_id", nullable = false)
    private Long programId;

    @Column(name = "user_event_queue_id")
    private Long userEventQueueId;

    @Column(name = "user_event_id")
    private Long userEventId;

    @NotNull
    @Column(name = "event_id", nullable = false)
    private String eventId;

    @NotNull
    @Column(name = "event_code", nullable = false)
    private String eventCode;

    @NotNull
    @Column(name = "updated_event_code", nullable = false)
    private String updatedEventCode;

    @Column(name = "awarded_event_point", precision = 21, scale = 2)
    private BigDecimal awardedEventPoint;

    @Column(name = "additional_point", precision = 21, scale = 2)
    private BigDecimal additionalPoint;

    @Column(name = "event_category")
    private String eventCategory;

    @Column(name = "updated_event_category")
    private String updatedEventCategory;

    @NotNull
    @Column(name = "execute_status", nullable = false)
    private String executeStatus;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "updated_date")
    private Instant updatedDate;

    @Column(name = "attempt_count")
    private int attemptCount;

    @Column(name= "error_message")
    private String errorMessage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCode) {
        this.eventCode = eventCode;
    }

    public BigDecimal getAwardedEventPoint() {
        return awardedEventPoint;
    }

    public void setAwardedEventPoint(BigDecimal awardedEventPoint) {
        this.awardedEventPoint = awardedEventPoint;
    }

    public BigDecimal getAdditionalPoint() {
        return additionalPoint;
    }

    public void setAdditionalPoint(BigDecimal additionalPoint) {
        this.additionalPoint = additionalPoint;
    }

    public String getEventCategory() {
        return eventCategory;
    }

    public void setEventCategory(String eventCategory) {
        this.eventCategory = eventCategory;
    }

    public String getExecuteStatus() {
        return executeStatus;
    }

    public void setExecuteStatus(String executeStatus) {
        this.executeStatus = executeStatus;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Instant updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(int attemptCount) {
        this.attemptCount = attemptCount;
    }

    public String getUpdatedEventCode() {
        return updatedEventCode;
    }

    public void setUpdatedEventCode(String updatedEventCode) {
        this.updatedEventCode = updatedEventCode;
    }

    public String getUpdatedEventCategory() {
        return updatedEventCategory;
    }

    public void setUpdatedEventCategory(String updatedEventCategory) {
        this.updatedEventCategory = updatedEventCategory;
    }

    public Long getProgramId() {
        return programId;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Long getUserEventQueueId() {
        return userEventQueueId;
    }

    public void setUserEventQueueId(Long userEventQueueId) {
        this.userEventQueueId = userEventQueueId;
    }

    public Long getUserEventId() {
        return userEventId;
    }

    public void setUserEventId(Long userEventId) {
        this.userEventId = userEventId;
    }
}
