package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.joda.time.DateTime;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * A UserReward.
 */
@Entity
@Table(name = "user_reward")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserReward implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "program_level")
    private Integer programLevel;

    @Column(name = "status")
    private String status;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "order_date")
    private Instant orderDate;

    @Column(name = "gift_id")
    private String giftId;

    @Column(name = "level_completed_date")
    private Instant levelCompletedDate;

    @NotNull
    @Column(name = "participant_id", nullable = false)
    private String participantId;

    @NotNull
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "client_id", nullable = false)
    private String clientId;

    @Column(name = "reward_type")
    private String rewardType;

    @Column(name = "reward_code")
    private String rewardCode;

    @Column(name = "reward_amount", precision = 21, scale = 2)
    private BigDecimal rewardAmount;

    @Column(name = "campaign_id")
    private String campaignId;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @NotNull
    @JsonIgnoreProperties("userRewards")
    @JoinColumn(name = "program_user_id", nullable = false)
    private ProgramUser programUser;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @NotNull
    @JsonIgnoreProperties("userRewards")
    @JoinColumn(name = "program_id", nullable = false)
    private Program program;

    @Column(name = "reward_message")
    private String rewardMessage;

    @Column(name = "event_happen_at")
    private Instant eventHappenAt;

    @Column(name = "event_id")
    private String eventId;


    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove


    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public UserReward eventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    public Instant getEventHappenAt() {
        return eventHappenAt;
    }

    public void setEventHappenAt(Instant eventHappenAt) {
        this.eventHappenAt = eventHappenAt;
    }

    public String getRewardMessage() {
        return rewardMessage;
    }

    public UserReward rewardMessage(String rewardMessage) {
        this.rewardMessage = rewardMessage;
        return this;
    }

    public void setRewardMessage(String rewardMessage) {
        this.rewardMessage = rewardMessage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getProgramLevel() {
        return programLevel;
    }

    public UserReward programLevel(Integer programLevel) {
        this.programLevel = programLevel;
        return this;
    }

    public void setProgramLevel(Integer programLevel) {
        this.programLevel = programLevel;
    }

    public String getStatus() {
        return status;
    }

    public UserReward status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public UserReward orderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Instant getOrderDate() {
        return orderDate;
    }

    public UserReward orderDate(Instant orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public void setOrderDate(Instant orderDate) {
        this.orderDate = orderDate;
    }

    public String getGiftId() {
        return giftId;
    }

    public UserReward giftId(String giftId) {
        this.giftId = giftId;
        return this;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public Instant getLevelCompletedDate() {
        return levelCompletedDate;
    }

    public UserReward levelCompletedDate(Instant levelCompletedDate) {
        this.levelCompletedDate = levelCompletedDate;
        return this;
    }

    public void setLevelCompletedDate(Instant levelCompletedDate) {
        this.levelCompletedDate = levelCompletedDate;
    }

    public String getParticipantId() {
        return participantId;
    }

    public UserReward participantId(String participantId) {
        this.participantId = participantId;
        return this;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getEmail() {
        return email;
    }

    public UserReward email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClientId() {
        return clientId;
    }

    public UserReward clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getRewardType() {
        return rewardType;
    }

    public UserReward rewardType(String rewardType) {
        this.rewardType = rewardType;
        return this;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getRewardCode() {
        return rewardCode;
    }

    public UserReward rewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
        return this;
    }

    public void setRewardCode(String rewardCode) {
        this.rewardCode = rewardCode;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public UserReward rewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
        return this;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public UserReward campaignId(String campaignId) {
        this.campaignId = campaignId;
        return this;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public ProgramUser getProgramUser() {
        return programUser;
    }

    public UserReward programUser(ProgramUser programUser) {
        this.programUser = programUser;
        return this;
    }

    public void setProgramUser(ProgramUser programUser) {
        this.programUser = programUser;
    }

    public Program getProgram() {
        return program;
    }

    public UserReward program(Program program) {
        this.program = program;
        return this;
    }

    public void setProgram(Program program) {
        this.program = program;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserReward)) {
            return false;
        }
        return id != null && id.equals(((UserReward) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserReward{" +
            "id=" + getId() +
            ", programLevel=" + getProgramLevel() +
            ", status='" + getStatus() + "'" +
            ", orderId='" + getOrderId() + "'" +
            ", orderDate='" + getOrderDate() + "'" +
            ", giftId='" + getGiftId() + "'" +
            ", levelCompletedDate='" + getLevelCompletedDate() + "'" +
            ", participantId='" + getParticipantId() + "'" +
            ", email='" + getEmail() + "'" +
            ", clientId='" + getClientId() + "'" +
            ", rewardType='" + getRewardType() + "'" +
            ", rewardCode='" + getRewardCode() + "'" +
            ", rewardAmount=" + getRewardAmount() +
            ", campaignId='" + getCampaignId() + "'" +
            "}";
    }
}
