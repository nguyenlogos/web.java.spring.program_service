package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ClientBrandSetting.
 */
@Entity
@Table(name = "client_brand_setting")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientBrandSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "client_id", nullable = false, unique = true)
    private String clientId;

    @NotNull
    @Column(name = "sub_path_url", nullable = false, unique = true)
    private String subPathUrl;

    @Column(name = "web_logo_url")
    private String webLogoUrl;

    @Column(name = "primary_color_value")
    private String primaryColorValue;

    @Column(name = "client_name")
    private String clientName;

    @Column(name = "client_url")
    private String clientUrl;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public ClientBrandSetting clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSubPathUrl() {
        return subPathUrl;
    }

    public ClientBrandSetting subPathUrl(String subPathUrl) {
        this.subPathUrl = subPathUrl;
        return this;
    }

    public void setSubPathUrl(String subPathUrl) {
        this.subPathUrl = subPathUrl;
    }

    public String getWebLogoUrl() {
        return webLogoUrl;
    }

    public ClientBrandSetting webLogoUrl(String webLogoUrl) {
        this.webLogoUrl = webLogoUrl;
        return this;
    }

    public void setWebLogoUrl(String webLogoUrl) {
        this.webLogoUrl = webLogoUrl;
    }

    public String getPrimaryColorValue() {
        return primaryColorValue;
    }

    public ClientBrandSetting primaryColorValue(String primaryColorValue) {
        this.primaryColorValue = primaryColorValue;
        return this;
    }

    public void setPrimaryColorValue(String primaryColorValue) {
        this.primaryColorValue = primaryColorValue;
    }

    public String getClientName() {
        return clientName;
    }

    public ClientBrandSetting clientName(String clientName) {
        this.clientName = clientName;
        return this;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public ClientBrandSetting clientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
        return this;
    }

    public void setClientUrl(String clientUrl) {
        this.clientUrl = clientUrl;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientBrandSetting)) {
            return false;
        }
        return id != null && id.equals(((ClientBrandSetting) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ClientBrandSetting{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", subPathUrl='" + getSubPathUrl() + "'" +
            ", webLogoUrl='" + getWebLogoUrl() + "'" +
            ", primaryColorValue='" + getPrimaryColorValue() + "'" +
            ", clientName='" + getClientName() + "'" +
            ", clientUrl='" + getClientUrl() + "'" +
            "}";
    }
}
