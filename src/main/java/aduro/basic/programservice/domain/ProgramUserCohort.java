package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import aduro.basic.programservice.domain.enumeration.CohortStatus;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * A ProgramUserCohort.
 */
@Entity
@Table(name = "program_user_cohort")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProgramUserCohort implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "progress")
    private Long progress;

    @Column(name = "number_of_pass")
    private Integer numberOfPass;

    @Column(name = "number_of_failure")
    private Integer numberOfFailure;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CohortStatus status;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @NotNull
    @Column(name = "modified_date", nullable = false)
    private Instant modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "program_user_id")
    @JsonIgnoreProperties("programUserCohorts")
    private ProgramUser programUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "program_cohort_id")
    @JsonIgnoreProperties("programUserCohorts")
    private ProgramCohort programCohort;

    @OneToMany(mappedBy = "programUserCohort", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, orphanRemoval = true)
    @Fetch(FetchMode.SUBSELECT)
    private Set<ProgramUserCollectionProgress> programUserCollectionProgresses;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProgress() {
        return progress;
    }

    public ProgramUserCohort progress(Long progress) {
        this.progress = progress;
        return this;
    }

    public void setProgress(Long progress) {
        this.progress = progress;
    }

    public Integer getNumberOfPass() {
        return numberOfPass;
    }

    public ProgramUserCohort numberOfPass(Integer numberOfPass) {
        this.numberOfPass = numberOfPass;
        return this;
    }

    public void setNumberOfPass(Integer numberOfPass) {
        this.numberOfPass = numberOfPass;
    }

    public Integer getNumberOfFailure() {
        return numberOfFailure;
    }

    public ProgramUserCohort numberOfFailure(Integer numberOfFailure) {
        this.numberOfFailure = numberOfFailure;
        return this;
    }

    public void setNumberOfFailure(Integer numberOfFailure) {
        this.numberOfFailure = numberOfFailure;
    }

    public CohortStatus getStatus() {
        return status;
    }

    public ProgramUserCohort status(CohortStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(CohortStatus status) {
        this.status = status;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ProgramUserCohort createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public ProgramUserCohort modifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public ProgramUser getProgramUser() {
        return programUser;
    }

    public ProgramUserCohort programUser(ProgramUser programUser) {
        this.programUser = programUser;
        return this;
    }

    public void setProgramUser(ProgramUser programUser) {
        this.programUser = programUser;
    }

    public ProgramCohort getProgramCohort() {
        return programCohort;
    }

    public ProgramUserCohort programCohort(ProgramCohort programCohort) {
        this.programCohort = programCohort;
        return this;
    }

    public void setProgramCohort(ProgramCohort programCohort) {
        this.programCohort = programCohort;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove


    public Set<ProgramUserCollectionProgress> getProgramUserCollectionProgresses() {
        return programUserCollectionProgresses;
    }

    public void setProgramUserCollectionProgresses(Set<ProgramUserCollectionProgress> programUserCollectionProgresses) {
        this.programUserCollectionProgresses = programUserCollectionProgresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramUserCohort)) {
            return false;
        }
        return id != null && id.equals(((ProgramUserCohort) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramUserCohort{" +
            "id=" + getId() +
            ", progress=" + getProgress() +
            ", numberOfPass=" + getNumberOfPass() +
            ", numberOfFailure=" + getNumberOfFailure() +
            ", status='" + getStatus() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
