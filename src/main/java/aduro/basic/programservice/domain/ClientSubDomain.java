package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ClientSubDomain.
 */
@Entity
@Table(name = "client_sub_domain")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ClientSubDomain implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "client_id", nullable = false)
    private String clientId;

    @NotNull
    @Column(name = "sub_domain_url", nullable = false, unique = true)
    private String subDomainUrl;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "modified_at")
    private Instant modifiedAt;

    @Column(name = "program_id")
    private Long programId;

    @Column(name = "program_name")
    private String programName;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public ClientSubDomain clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getSubDomainUrl() {
        return subDomainUrl;
    }

    public ClientSubDomain subDomainUrl(String subDomainUrl) {
        this.subDomainUrl = subDomainUrl;
        return this;
    }

    public void setSubDomainUrl(String subDomainUrl) {
        this.subDomainUrl = subDomainUrl;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public ClientSubDomain createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getModifiedAt() {
        return modifiedAt;
    }

    public ClientSubDomain modifiedAt(Instant modifiedAt) {
        this.modifiedAt = modifiedAt;
        return this;
    }

    public void setModifiedAt(Instant modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public Long getProgramId() {
        return programId;
    }

    public ClientSubDomain programId(Long programId) {
        this.programId = programId;
        return this;
    }

    public void setProgramId(Long programId) {
        this.programId = programId;
    }

    public String getProgramName() {
        return programName;
    }

    public ClientSubDomain programName(String programName) {
        this.programName = programName;
        return this;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ClientSubDomain)) {
            return false;
        }
        return id != null && id.equals(((ClientSubDomain) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ClientSubDomain{" +
            "id=" + getId() +
            ", clientId='" + getClientId() + "'" +
            ", subDomainUrl='" + getSubDomainUrl() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", modifiedAt='" + getModifiedAt() + "'" +
            ", programId=" + getProgramId() +
            ", programName='" + getProgramName() + "'" +
            "}";
    }
}
