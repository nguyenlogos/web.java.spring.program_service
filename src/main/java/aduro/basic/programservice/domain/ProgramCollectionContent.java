package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ProgramCollectionContent.
 */
@Entity
@Table(name = "program_collection_content")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramCollectionContent implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 255)
    @Column(name = "item_name", length = 255)
    private String itemName;

    @Size(max = 100)
    @Column(name = "item_id", length = 100)
    private String itemId;

    @NotNull
    @Size(max = 100)
    @Column(name = "item_type", length = 100, nullable = false)
    private String itemType;

    @NotNull
    @Size(max = 100)
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Size(max = 255)
    @Column(name = "item_icon", length = 255)
    private String itemIcon;

    @Column(name = "has_required")
    private Boolean hasRequired;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "modified_date")
    private Instant modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "program_collection_id")
    @JsonIgnoreProperties("programCollectionContents")
    private ProgramCollection programCollection;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public ProgramCollectionContent itemName(String itemName) {
        this.itemName = itemName;
        return this;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemId() {
        return itemId;
    }

    public ProgramCollectionContent itemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public ProgramCollectionContent itemType(String itemType) {
        this.itemType = itemType;
        return this;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getContentType() {
        return contentType;
    }

    public ProgramCollectionContent contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getItemIcon() {
        return itemIcon;
    }

    public ProgramCollectionContent itemIcon(String itemIcon) {
        this.itemIcon = itemIcon;
        return this;
    }

    public void setItemIcon(String itemIcon) {
        this.itemIcon = itemIcon;
    }

    public Boolean isHasRequired() {
        return hasRequired;
    }

    public ProgramCollectionContent hasRequired(Boolean hasRequired) {
        this.hasRequired = hasRequired;
        return this;
    }

    public void setHasRequired(Boolean hasRequired) {
        this.hasRequired = hasRequired;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ProgramCollectionContent createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public ProgramCollectionContent modifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public ProgramCollection getProgramCollection() {
        return programCollection;
    }

    public ProgramCollectionContent programCollection(ProgramCollection programCollection) {
        this.programCollection = programCollection;
        return this;
    }

    public void setProgramCollection(ProgramCollection programCollection) {
        this.programCollection = programCollection;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramCollectionContent)) {
            return false;
        }
        return id != null && id.equals(((ProgramCollectionContent) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramCollectionContent{" +
            "id=" + getId() +
            ", itemName='" + getItemName() + "'" +
            ", itemId='" + getItemId() + "'" +
            ", itemType='" + getItemType() + "'" +
            ", contentType='" + getContentType() + "'" +
            ", itemIcon='" + getItemIcon() + "'" +
            ", hasRequired='" + isHasRequired() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
