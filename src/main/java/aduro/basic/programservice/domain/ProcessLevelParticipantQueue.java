package aduro.basic.programservice.domain;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "process_level_participant_queue")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProcessLevelParticipantQueue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @Column(name = "program_user_id")
    private Long programUserId;

    @Column(name = "attempt_count")
    private Integer attemptCount;

    @Column(name = "current_level")
    private Integer currentLevel;

    @Column(name = "archive_level")
    private Integer archiveLevel;

    @Column(name = "program_level_id") // level pass
    private Long programLevelId;

    @Column(name = "number_of_reward")
    private Integer numberOfReward;

    @Column(name = "process_status")
    private String status;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "is_terminated")
    private Boolean isTerminated;

    @Column(name = "read_at")
    private Instant readAt;

}
