package aduro.basic.programservice.domain;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ProgramBiometricData.
 */
@Entity
@Table(name = "program_biometric_data")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramBiometricData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "biometric_code")
    private String biometricCode;

    @Column(name = "biometric_name")
    private String biometricName;

    @Column(name = "male_min")
    private Float maleMin;

    @Column(name = "male_max")
    private Float maleMax;

    @Column(name = "female_min")
    private Float femaleMin;

    @Column(name = "female_max")
    private Float femaleMax;

    @Column(name = "unidentified_min")
    private Float unidentifiedMin;

    @Column(name = "unidentified_max")
    private Float unidentifiedMax;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBiometricCode() {
        return biometricCode;
    }

    public ProgramBiometricData biometricCode(String biometricCode) {
        this.biometricCode = biometricCode;
        return this;
    }

    public void setBiometricCode(String biometricCode) {
        this.biometricCode = biometricCode;
    }

    public String getBiometricName() {
        return biometricName;
    }

    public ProgramBiometricData biometricName(String biometricName) {
        this.biometricName = biometricName;
        return this;
    }

    public void setBiometricName(String biometricName) {
        this.biometricName = biometricName;
    }

    public Float getMaleMin() {
        return maleMin;
    }

    public ProgramBiometricData maleMin(Float maleMin) {
        this.maleMin = maleMin;
        return this;
    }

    public void setMaleMin(Float maleMin) {
        this.maleMin = maleMin;
    }

    public Float getMaleMax() {
        return maleMax;
    }

    public ProgramBiometricData maleMax(Float maleMax) {
        this.maleMax = maleMax;
        return this;
    }

    public void setMaleMax(Float maleMax) {
        this.maleMax = maleMax;
    }

    public Float getFemaleMin() {
        return femaleMin;
    }

    public ProgramBiometricData femaleMin(Float femaleMin) {
        this.femaleMin = femaleMin;
        return this;
    }

    public void setFemaleMin(Float femaleMin) {
        this.femaleMin = femaleMin;
    }

    public Float getFemaleMax() {
        return femaleMax;
    }

    public ProgramBiometricData femaleMax(Float femaleMax) {
        this.femaleMax = femaleMax;
        return this;
    }

    public void setFemaleMax(Float femaleMax) {
        this.femaleMax = femaleMax;
    }

    public Float getUnidentifiedMin() {
        return unidentifiedMin;
    }

    public ProgramBiometricData unidentifiedMin(Float unidentifiedMin) {
        this.unidentifiedMin = unidentifiedMin;
        return this;
    }

    public void setUnidentifiedMin(Float unidentifiedMin) {
        this.unidentifiedMin = unidentifiedMin;
    }

    public Float getUnidentifiedMax() {
        return unidentifiedMax;
    }

    public ProgramBiometricData unidentifiedMax(Float unidentifiedMax) {
        this.unidentifiedMax = unidentifiedMax;
        return this;
    }

    public void setUnidentifiedMax(Float unidentifiedMax) {
        this.unidentifiedMax = unidentifiedMax;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramBiometricData)) {
            return false;
        }
        return id != null && id.equals(((ProgramBiometricData) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramBiometricData{" +
            "id=" + getId() +
            ", biometricCode='" + getBiometricCode() + "'" +
            ", biometricName='" + getBiometricName() + "'" +
            ", maleMin=" + getMaleMin() +
            ", maleMax=" + getMaleMax() +
            ", femaleMin=" + getFemaleMin() +
            ", femaleMax=" + getFemaleMax() +
            ", unidentifiedMin=" + getUnidentifiedMin() +
            ", unidentifiedMax=" + getUnidentifiedMax() +
            "}";
    }
}
