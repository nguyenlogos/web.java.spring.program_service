package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ProgramHealthyRange.
 */
@Entity
@Table(name = "program_healthy_range")
public class ProgramHealthyRange implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "male_min", nullable = false)
    private Float maleMin;

    @NotNull
    @Column(name = "male_max", nullable = false)
    private Float maleMax;

    @NotNull
    @Column(name = "female_min", nullable = false)
    private Float femaleMin;

    @NotNull
    @Column(name = "female_max", nullable = false)
    private Float femaleMax;

    @NotNull
    @Column(name = "unidentified_min", nullable = false)
    private Float unidentifiedMin;

    @NotNull
    @Column(name = "unidentified_max", nullable = false)
    private Float unidentifiedMax;

    @Column(name = "is_apply_point")
    private Boolean isApplyPoint;

    @NotNull
    @Column(name = "biometric_code", nullable = false)
    private String biometricCode;

    @Column(name = "sub_category_code")
    private String subCategoryCode;

    @NotNull
    @Column(name = "sub_category_id", nullable = false)
    private Long subCategoryId;

    @ManyToOne
    @JsonIgnoreProperties("programHealthyRanges")
    private Program program;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getMaleMin() {
        return maleMin;
    }

    public ProgramHealthyRange maleMin(Float maleMin) {
        this.maleMin = maleMin;
        return this;
    }

    public void setMaleMin(Float maleMin) {
        this.maleMin = maleMin;
    }

    public Float getMaleMax() {
        return maleMax;
    }

    public ProgramHealthyRange maleMax(Float maleMax) {
        this.maleMax = maleMax;
        return this;
    }

    public void setMaleMax(Float maleMax) {
        this.maleMax = maleMax;
    }

    public Float getFemaleMin() {
        return femaleMin;
    }

    public ProgramHealthyRange femaleMin(Float femaleMin) {
        this.femaleMin = femaleMin;
        return this;
    }

    public void setFemaleMin(Float femaleMin) {
        this.femaleMin = femaleMin;
    }

    public Float getFemaleMax() {
        return femaleMax;
    }

    public ProgramHealthyRange femaleMax(Float femaleMax) {
        this.femaleMax = femaleMax;
        return this;
    }

    public void setFemaleMax(Float femaleMax) {
        this.femaleMax = femaleMax;
    }

    public Float getUnidentifiedMin() {
        return unidentifiedMin;
    }

    public ProgramHealthyRange unidentifiedMin(Float unidentifiedMin) {
        this.unidentifiedMin = unidentifiedMin;
        return this;
    }

    public void setUnidentifiedMin(Float unidentifiedMin) {
        this.unidentifiedMin = unidentifiedMin;
    }

    public Float getUnidentifiedMax() {
        return unidentifiedMax;
    }

    public ProgramHealthyRange unidentifiedMax(Float unidentifiedMax) {
        this.unidentifiedMax = unidentifiedMax;
        return this;
    }

    public void setUnidentifiedMax(Float unidentifiedMax) {
        this.unidentifiedMax = unidentifiedMax;
    }

    public Boolean isIsApplyPoint() {
        return isApplyPoint;
    }

    public ProgramHealthyRange isApplyPoint(Boolean isApplyPoint) {
        this.isApplyPoint = isApplyPoint;
        return this;
    }

    public void setIsApplyPoint(Boolean isApplyPoint) {
        this.isApplyPoint = isApplyPoint;
    }

    public String getBiometricCode() {
        return biometricCode;
    }

    public ProgramHealthyRange biometricCode(String biometricCode) {
        this.biometricCode = biometricCode;
        return this;
    }

    public void setBiometricCode(String biometricCode) {
        this.biometricCode = biometricCode;
    }

    public String getSubCategoryCode() {
        return subCategoryCode;
    }

    public ProgramHealthyRange subCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
        return this;
    }

    public void setSubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public ProgramHealthyRange subCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
        return this;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Program getProgram() {
        return program;
    }

    public ProgramHealthyRange program(Program program) {
        this.program = program;
        return this;
    }

    public void setProgram(Program program) {
        this.program = program;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramHealthyRange)) {
            return false;
        }
        return id != null && id.equals(((ProgramHealthyRange) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramHealthyRange{" +
            "id=" + getId() +
            ", maleMin=" + getMaleMin() +
            ", maleMax=" + getMaleMax() +
            ", femaleMin=" + getFemaleMin() +
            ", femaleMax=" + getFemaleMax() +
            ", unidentifiedMin=" + getUnidentifiedMin() +
            ", unidentifiedMax=" + getUnidentifiedMax() +
            ", isApplyPoint='" + isIsApplyPoint() + "'" +
            ", biometricCode='" + getBiometricCode() + "'" +
            ", subCategoryCode='" + getSubCategoryCode() + "'" +
            ", subCategoryId=" + getSubCategoryId() +
            "}";
    }
}
