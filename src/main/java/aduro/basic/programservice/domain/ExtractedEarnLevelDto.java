package aduro.basic.programservice.domain;

import aduro.basic.programservice.service.dto.UserRewardDTO;
import lombok.*;

import java.time.Instant;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExtractedEarnLevelDto {

    private Long id;

    private Integer levelOrder;

    private String name;

    private Instant archivedDate;

    private List<UserRewardDTO> userRewardList;
}
