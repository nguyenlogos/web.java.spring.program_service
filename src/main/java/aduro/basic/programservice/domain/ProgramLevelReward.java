package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A ProgramLevelReward.
 */
@Entity
@Table(name = "program_level_reward")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramLevelReward implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "quantity")
    private Integer quantity;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "reward_amount", precision = 21, scale = 2, nullable = false)
    private BigDecimal rewardAmount;

    @NotNull
    @Column(name = "reward_type", nullable = false)
    private String rewardType;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "subgroup_name")
    private String subgroupName;




    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    @Column(name = "campaign_id")
    private String campaignId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @NotNull
    @JsonIgnoreProperties("programLevelRewards")
    @JoinColumn(name = "program_level_id")
    private ProgramLevel programLevel;

    public Long getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(Long programLevelId) {
        this.programLevelId = programLevelId;
    }

    @Column(name = "program_level_id", insertable = false, updatable = false)
    private Long programLevelId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public ProgramLevelReward description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public ProgramLevelReward quantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCode() {
        return code;
    }

    public ProgramLevelReward code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public ProgramLevelReward rewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
        return this;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getRewardType() {
        return rewardType;
    }

    public ProgramLevelReward rewardType(String rewardType) {
        this.rewardType = rewardType;
        return this;
    }

    public ProgramLevelReward campaignId(String campaignId) {
        this.campaignId = campaignId;
        return this;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public ProgramLevel getProgramLevel() {
        return programLevel;
    }

    public ProgramLevelReward programLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
        return this;
    }

    public void setProgramLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramLevelReward)) {
            return false;
        }
        return id != null && id.equals(((ProgramLevelReward) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramLevelReward{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", quantity=" + getQuantity() +
            ", code='" + getCode() + "'" +
            ", rewardAmount=" + getRewardAmount() +
            ", rewardType='" + getRewardType() + "'" +
            ", campaignId='" + getCampaignId() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            "}";
    }


    public String getSubgroupId() {
        return subgroupId;
    }

    public ProgramLevelReward subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public ProgramLevelReward subgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
        return this;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public ProgramLevelReward programLevelId(Long id) {
        this.programLevelId = id;
        return this;
    }
}
