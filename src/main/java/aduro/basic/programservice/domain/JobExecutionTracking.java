package aduro.basic.programservice.domain;

import aduro.basic.programservice.service.dto.IncentiveStatusEnum;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "cron_job_execution_tracking")
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JobExecutionTracking  implements Serializable  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "updated_at")
    private Instant updatedAt;

    @NotNull
    @Column(name = "client_id")
    private String clientId;

    @Column(name = "error_message")
    private String errorMessage;

    @Column(name = "number_of_records")
    private Integer numberOfRecords;

    @Column(name = "payload")
    private String payload;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "process_status")
    @Enumerated(EnumType.STRING)
    private IncentiveStatusEnum processStatus;
}
