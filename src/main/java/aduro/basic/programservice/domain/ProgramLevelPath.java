package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ProgramLevelPath.
 */
@Entity
@Table(name = "program_level_path")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramLevelPath implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "path_id", nullable = false)
    private String pathId;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "subgroup_name")
    private String subgroupName;

    @Column(name = "path_type")
    private String pathType;

    @Column(name = "path_category")
    private String pathCategory;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @NotNull
    @JsonIgnoreProperties("programLevelPaths")
    @JoinColumn(name = "program_level_id")
    private ProgramLevel programLevel;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPathId() {
        return pathId;
    }

    public ProgramLevelPath pathId(String pathId) {
        this.pathId = pathId;
        return this;
    }

    public void setPathId(String pathId) {
        this.pathId = pathId;
    }

    public String getName() {
        return name;
    }

    public ProgramLevelPath name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public ProgramLevelPath subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public ProgramLevelPath subgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
        return this;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public String getPathType() {
        return pathType;
    }

    public ProgramLevelPath pathType(String pathType) {
        this.pathType = pathType;
        return this;
    }

    public void setPathType(String pathType) {
        this.pathType = pathType;
    }

    public String getPathCategory() {
        return pathCategory;
    }

    public ProgramLevelPath pathCategory(String pathCategory) {
        this.pathCategory = pathCategory;
        return this;
    }

    public void setPathCategory(String pathCategory) {
        this.pathCategory = pathCategory;
    }

    public ProgramLevel getProgramLevel() {
        return programLevel;
    }

    public ProgramLevelPath programLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
        return this;
    }

    public void setProgramLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramLevelPath)) {
            return false;
        }
        return id != null && id.equals(((ProgramLevelPath) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramLevelPath{" +
            "id=" + getId() +
            ", pathId='" + getPathId() + "'" +
            ", name='" + getName() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", pathType='" + getPathType() + "'" +
            ", pathCategory='" + getPathCategory() + "'" +
            "}";
    }
}
