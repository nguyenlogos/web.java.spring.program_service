package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A ProgramCollection.
 */
@Entity
@Table(name = "program_collection")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProgramCollection implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 150)
    @Column(name = "display_name", length = 150, nullable = false)
    private String displayName;

    @Size(max = 255)
    @Column(name = "long_description", length = 255)
    private String longDescription;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "modified_date")
    private Instant modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "program_id")
    @JsonIgnoreProperties("programCollections")
    private Program program;

    @OneToMany(mappedBy = "programCollection", fetch = FetchType.LAZY,cascade = CascadeType.REMOVE)
    private Set<ProgramCollectionContent> programCollectionContents = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public ProgramCollection displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public ProgramCollection longDescription(String longDescription) {
        this.longDescription = longDescription;
        return this;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ProgramCollection createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public ProgramCollection modifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Program getProgram() {
        return program;
    }

    public ProgramCollection program(Program program) {
        this.program = program;
        return this;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public Set<ProgramCollectionContent> getProgramCollectionContents() {
        return programCollectionContents;
    }

    public ProgramCollection programCollectionContents(Set<ProgramCollectionContent> programCollectionContents) {
        this.programCollectionContents = programCollectionContents;
        return this;
    }

    public ProgramCollection addProgramCollectionContent(ProgramCollectionContent programCollectionContent) {
        this.programCollectionContents.add(programCollectionContent);
        programCollectionContent.setProgramCollection(this);
        return this;
    }

    public ProgramCollection removeProgramCollectionContent(ProgramCollectionContent programCollectionContent) {
        this.programCollectionContents.remove(programCollectionContent);
        programCollectionContent.setProgramCollection(null);
        return this;
    }

    public void setProgramCollectionContents(Set<ProgramCollectionContent> programCollectionContents) {
        this.programCollectionContents = programCollectionContents;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramCollection)) {
            return false;
        }
        return id != null && id.equals(((ProgramCollection) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramCollection{" +
            "id=" + getId() +
            ", displayName='" + getDisplayName() + "'" +
            ", longDescription='" + getLongDescription() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
