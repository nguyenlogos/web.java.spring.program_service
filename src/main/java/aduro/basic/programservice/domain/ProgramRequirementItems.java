package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ProgramRequirementItems.
 */
@Entity
@Table(name = "program_requirement_items")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ProgramRequirementItems implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "item_id", nullable = false)
    private String itemId;

    @Column(name = "name_of_item")
    private String nameOfItem;

    @NotNull
    @Column(name = "type_of_item", nullable = false)
    private String typeOfItem;

    @NotNull
    @Column(name = "item_code", nullable = false)
    private String itemCode;

    @Column(name = "subgroup_id")
    private String subgroupId;

    @Column(name = "subgroup_name")
    private String subgroupName;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "program_level_id", insertable = false, updatable = false)
    private Long programLevelId;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("programRequirementItems")
    @JoinColumn(name = "program_level_id")
    private ProgramLevel programLevel;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemId() {
        return itemId;
    }

    public ProgramRequirementItems itemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getNameOfItem() {
        return nameOfItem;
    }

    public ProgramRequirementItems nameOfItem(String nameOfItem) {
        this.nameOfItem = nameOfItem;
        return this;
    }

    public void setNameOfItem(String nameOfItem) {
        this.nameOfItem = nameOfItem;
    }

    public String getTypeOfItem() {
        return typeOfItem;
    }

    public ProgramRequirementItems typeOfItem(String typeOfItem) {
        this.typeOfItem = typeOfItem;
        return this;
    }

    public void setTypeOfItem(String typeOfItem) {
        this.typeOfItem = typeOfItem;
    }

    public String getItemCode() {
        return itemCode;
    }

    public ProgramRequirementItems itemCode(String itemCode) {
        this.itemCode = itemCode;
        return this;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSubgroupId() {
        return subgroupId;
    }

    public ProgramRequirementItems subgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
        return this;
    }

    public void setSubgroupId(String subgroupId) {
        this.subgroupId = subgroupId;
    }

    public String getSubgroupName() {
        return subgroupName;
    }

    public ProgramRequirementItems subgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
        return this;
    }

    public void setSubgroupName(String subgroupName) {
        this.subgroupName = subgroupName;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public ProgramRequirementItems createdAt(Instant createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public ProgramLevel getProgramLevel() {
        return programLevel;
    }

    public ProgramRequirementItems programLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
        return this;
    }

    public void setProgramLevel(ProgramLevel programLevel) {
        this.programLevel = programLevel;
    }

    public Long getProgramLevelId() {
        return programLevelId;
    }

    public void setProgramLevelId(Long programLevelId) {
        this.programLevelId = programLevelId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramRequirementItems)) {
            return false;
        }
        return id != null && id.equals(((ProgramRequirementItems) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramRequirementItems{" +
            "id=" + getId() +
            ", itemId='" + getItemId() + "'" +
            ", nameOfItem='" + getNameOfItem() + "'" +
            ", typeOfItem='" + getTypeOfItem() + "'" +
            ", itemCode='" + getItemCode() + "'" +
            ", subgroupId='" + getSubgroupId() + "'" +
            ", subgroupName='" + getSubgroupName() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            "}";
    }

    public ProgramRequirementItems programLevelId(Long id) {
        this.programLevelId = id;
        return this;
    }
}
