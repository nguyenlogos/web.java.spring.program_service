package aduro.basic.programservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A ProgramCohortRule.
 */
@Entity
@Table(name = "program_cohort_rule")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ProgramCohortRule implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "data_input_type", nullable = false)
    private String dataInputType;

    @NotNull
    @Column(name = "rules", nullable = false)
    private String rules;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @NotNull
    @Column(name = "modified_date", nullable = false)
    private Instant modifiedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "program_cohort_id")
    @JsonIgnoreProperties("programCohortRules")
    private ProgramCohort programCohort;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataInputType() {
        return dataInputType;
    }

    public ProgramCohortRule dataInputType(String dataInputType) {
        this.dataInputType = dataInputType;
        return this;
    }

    public void setDataInputType(String dataInputType) {
        this.dataInputType = dataInputType;
    }

    public String getRules() {
        return rules;
    }

    public ProgramCohortRule rules(String rules) {
        this.rules = rules;
        return this;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public ProgramCohortRule createdDate(Instant createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getModifiedDate() {
        return modifiedDate;
    }

    public ProgramCohortRule modifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
        return this;
    }

    public void setModifiedDate(Instant modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public ProgramCohort getProgramCohort() {
        return programCohort;
    }

    public ProgramCohortRule programCohort(ProgramCohort programCohort) {
        this.programCohort = programCohort;
        return this;
    }

    public void setProgramCohort(ProgramCohort programCohort) {
        this.programCohort = programCohort;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProgramCohortRule)) {
            return false;
        }
        return id != null && id.equals(((ProgramCohortRule) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ProgramCohortRule{" +
            "id=" + getId() +
            ", dataInputType='" + getDataInputType() + "'" +
            ", rules='" + getRules() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", modifiedDate='" + getModifiedDate() + "'" +
            "}";
    }
}
