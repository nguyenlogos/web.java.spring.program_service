
-- set default date for existing data in program_user
update 	program_user u
    inner join (
        select max(event_date) as max_event_date, program_user_id
        from user_event
        group by program_user_id ) t_max on u.id=t_max.program_user_id
    inner join (
        select min(event_date) as min_event_date, program_user_id
        from user_event
        group by program_user_id ) t_min on u.id=t_min.program_user_id
set u.modified_date=t_max.max_event_date , u.created_date=t_min.min_event_date;

-- set default date for existing data in user_event
update user_event
set created_date = event_date,
    modified_date=event_date
where 1=1;

-- set default date for existing data in user_reward
update user_reward
set created_date = level_completed_date,
    modified_date = case when event_happen_at is null then level_completed_date else event_happen_at end
where 1=1;


-- set default date for existing data in program_level
update program_level pl
    inner join  program  on pl.program_id=program.id
set pl.created_date= program.created_date, pl.modified_date=program.last_modified_date
where 1=1;

-- set default date for existing data in client_program
update client_program cp
    inner join program on cp.program_id=program.id
set cp.created_date= program.created_date, cp.modified_date=program.last_modified_date;
