create table program_collection_content (
	id  bigint not null auto_increment primary key,
	item_name varchar(255) not null,
	item_type varchar(100) not null, -- type of activities, paths
	content_type varchar(100), -- activities or paths
	item_icon VARCHAR(255) default null, -- link icon
	has_required tinyint default false,
    program_collection_id bigint not null,
	created_date datetime not null default  CURRENT_TIMESTAMP,
	modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,

    constraint program_collection_id
        foreign key (program_collection_id) references program_collection(id)
);

create index program_collection_content_program_collection_id_index on program_collection_content(program_collection_id);
create index program_collection_content_item_type_index on program_collection_content(item_type);
