
ALTER TABLE  program add tobacco_program_deadline DATETIME;
ALTER TABLE  program add tobacco_attestation_deadline DATETIME;
ALTER TABLE  program add is_tobacco_surcharge_management boolean default NULL;

alter table program drop column is_tobacco_free;
