create table program_tracking_status (
 id  bigint not null auto_increment primary key,
 program_id bigint not null,
 program_status varchar(100),
 created_date datetime not null default  CURRENT_TIMESTAMP,
 modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
);

create index program_tracking_status_program_status_index on program_tracking_status(program_status);
create index program_tracking_status_program_id_index on program_tracking_status(program_id);

insert into program_tracking_status (program_id, program_status, created_date, modified_date)
select p.id as program_id, p.status as program_status,
       case when p.created_date is null then CURRENT_TIMESTAMP else p.created_date end as created_date , case when p.last_modified_date is null then CURRENT_TIMESTAMP else p.last_modified_date end as modified_date from  program p;
