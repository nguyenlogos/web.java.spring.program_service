create table process_level_participant_queue
(
    id  bigint auto_increment primary key,
    program_user_id bigint not null,
    program_level_id bigint not null,
    created_at datetime,
    updated_at datetime,
    current_level integer,
    archive_level integer,
    number_of_reward integer,
    process_status varchar(100),
    error_message text null ,
    is_terminated boolean default false,
    attempt_count integer,
    read_at datetime
);

create index process_level_participant_queue_program_user_id_index
    on process_level_participant_queue (program_user_id);

create index process_level_participant_queue_status_index
    on process_level_participant_queue (process_status);

create index process_level_participant_queue_is_terminated_index
    on process_level_participant_queue (is_terminated);

