alter table term_and_condition add column is_required_annually boolean default false;
alter table term_and_condition add column is_terminated boolean default false;
alter table term_and_condition add column consent_order integer;
