create table program_user_collection_progress(
                                                 id  bigint not null auto_increment primary key,
                                                 program_user_cohort_id bigint not null,
                                                 collection_id bigint not null,
                                                 status varchar(100) not null,
                                                 current_progress int,
                                                 total_required_items int,
                                                 created_date datetime not null default  CURRENT_TIMESTAMP,
                                                 modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
                                                 foreign key (program_user_cohort_id) references program_user_cohort(id)
);
