create table user_event_queue
(
    id  bigint auto_increment primary key,
    event_code varchar(255) not null ,
    event_id varchar(255) not null,
    event_date datetime not null,
    event_point decimal,
    event_category varchar(255) not null,
    program_user_id bigint not null,
    program_id bigint not null,
    execute_status varchar(255) not null,
    read_at datetime,
    error_message varchar(255) null,
    current_step varchar(255)
);

create index user_event_queue_program_user_id_index
    on user_event_queue (program_user_id);

create index user_event_queue_program_id_index
    on user_event_queue (program_id);

