create table process_additional_user_points_queue
(
    id  bigint auto_increment primary key,
    participant_id   varchar(255)   not null,
    client_id        varchar(255)   not null,
    program_id       bigint           not null,
    event_id        varchar(255)   not null,
    event_code      varchar(255)   not null,
    updated_event_code      varchar(255)   not null,
    awarded_event_point     decimal(21, 2) null,
    additional_point     decimal(21, 2) null,
    event_category  varchar(255)   null,
    updated_event_category  varchar(255)   null,
    execute_status varchar(255) not null,
    created_date datetime,
    updated_date datetime,
    attempt_count integer
);

create index additional_points_queue__aduro_id_idx
    on process_additional_user_points_queue (participant_id);

create index additional_points_queue__client_id_idx
    on process_additional_user_points_queue (client_id);

create index additional_points_queue__program_id_idx
    on process_additional_user_points_queue (program_id);

create index additional_points_queue__event_id_idx
    on process_additional_user_points_queue (event_id);

create index additional_points_queue__status_idx
    on process_additional_user_points_queue (execute_status);

create index additional_points_queue__event_id_count_idx
    on process_additional_user_points_queue (event_id, attempt_count);
