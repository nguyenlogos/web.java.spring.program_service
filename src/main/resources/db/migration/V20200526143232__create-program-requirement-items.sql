create table program_requirement_items
(
    id  bigint auto_increment primary key,
    item_id varchar(36) not null ,
    name_of_item varchar(255) ,
    type_of_item varchar(150),
    item_code varchar(150),
    subgroup_id varchar(150),
    subgroup_name varchar(150),
    created_at datetime,
    program_level_id bigint not null,
    constraint fk_program_requirement_items_program_level
        foreign key (program_level_id) references program_level (id)
);
