create table DATABASECHANGELOG
(
    ID            varchar(255) not null,
    AUTHOR        varchar(255) not null,
    FILENAME      varchar(255) not null,
    DATEEXECUTED  datetime     not null,
    ORDEREXECUTED int          not null,
    EXECTYPE      varchar(10)  not null,
    MD5SUM        varchar(35)  null,
    DESCRIPTION   varchar(255) null,
    COMMENTS      varchar(255) null,
    TAG           varchar(255) null,
    LIQUIBASE     varchar(20)  null,
    CONTEXTS      varchar(255) null,
    LABELS        varchar(255) null,
    DEPLOYMENT_ID varchar(10)  null
)
    charset = latin1;

create table DATABASECHANGELOGLOCK
(
    ID          int          not null
        primary key,
    LOCKED      bit          not null,
    LOCKGRANTED datetime     null,
    LOCKEDBY    varchar(255) null
)
    charset = latin1;

create table activity_event_queue
(
    id             bigint auto_increment
        primary key,
    participant_id varchar(255) not null,
    client_id      varchar(255) not null,
    activity_code  varchar(255) not null,
    email          varchar(255) not null,
    created_date   datetime     not null,
    first_name     varchar(255) not null,
    last_name      varchar(255) not null,
    transaction_id varchar(255) not null,
    error_message  varchar(255) null,
    execute_status varchar(255) null,
    subgroup_id    varchar(255) null
)
    charset = latin1;

create index activity_event_queue__activity_code_index
    on activity_event_queue (activity_code);

create index activity_event_queue__client_id_index
    on activity_event_queue (client_id);

create index activity_event_queue__email_index
    on activity_event_queue (email);

create index activity_event_queue__participant_id_index
    on activity_event_queue (participant_id);

create index activity_event_queue__subgroup_id_index
    on activity_event_queue (subgroup_id);

create table category
(
    id            bigint auto_increment
        primary key,
    code          varchar(255) not null,
    name          varchar(255) not null,
    category_type varchar(255) null,
    constraint ux_category_code
        unique (code)
)
    charset = latin1;

create table client_brand_setting
(
    id                  bigint auto_increment
        primary key,
    client_id           varchar(255) not null,
    client_name         varchar(255) null,
    sub_path_url        varchar(255) not null,
    web_logo_url        varchar(255) null,
    primary_color_value varchar(255) null,
    constraint ux_client_brand_setting_client_id
        unique (client_id),
    constraint ux_client_brand_setting_sub_path_url
        unique (sub_path_url)
)
    charset = latin1;

create index client_brand_setting__client_name_index
    on client_brand_setting (client_name);

create table client_setting
(
    id                        bigint auto_increment
        primary key,
    client_id                 varchar(255)   not null,
    client_name               varchar(255)   not null,
    client_email              varchar(255)   null,
    tango_account_identifier  varchar(255)   null,
    tango_customer_identifier varchar(255)   null,
    tango_current_balance     decimal(21, 2) null,
    tango_threshold_balance   decimal(21, 2) null,
    tango_credit_token        varchar(255)   null,
    updated_date              datetime       not null,
    updated_by                varchar(255)   not null,
    constraint ux_client_setting_client_id
        unique (client_id),
    constraint ux_client_setting_client_name
        unique (client_name)
)
    charset = latin1;

create table incentive_data
(
    id             int auto_increment
        primary key,
    EmployerId     char(36)                         not null,
    EmployeeId     char(36)                         not null,
    FirstName      varchar(100)                     not null,
    LastName       varchar(100)                     not null,
    Email          varchar(150)                     null,
    SubgroupId     int                              null,
    ProgramID      int                              null,
    EventId        int                              null,
    IncentiveDate  int                              null,
    Status         enum ('NEW', 'UPDATED', 'ERROR') null,
    Transaction_id char(36)                         not null
)
    charset = latin1;

create table jhi_authority
(
    name varchar(50) not null
        primary key
)
    charset = latin1;

create table jhi_persistent_audit_event
(
    event_id   bigint auto_increment
        primary key,
    principal  varchar(50)  not null,
    event_date timestamp    null,
    event_type varchar(255) null
)
    charset = latin1;

create index idx_persistent_audit_event
    on jhi_persistent_audit_event (principal, event_date);

create table jhi_persistent_audit_evt_data
(
    event_id bigint       not null,
    name     varchar(150) not null,
    value    varchar(255) null,
    primary key (event_id, name),
    constraint fk_evt_pers_audit_evt_data
        foreign key (event_id) references jhi_persistent_audit_event (event_id)
)
    charset = latin1;

create index idx_persistent_audit_evt_data
    on jhi_persistent_audit_evt_data (event_id);

create table jhi_user
(
    id                 bigint auto_increment
        primary key,
    login              varchar(50)  not null,
    password_hash      varchar(60)  not null,
    first_name         varchar(50)  null,
    last_name          varchar(50)  null,
    email              varchar(191) null,
    image_url          varchar(256) null,
    activated          bit          not null,
    lang_key           varchar(6)   null,
    activation_key     varchar(20)  null,
    reset_key          varchar(20)  null,
    created_by         varchar(50)  not null,
    created_date       timestamp    null,
    reset_date         timestamp    null,
    last_modified_by   varchar(50)  null,
    last_modified_date timestamp    null,
    constraint ux_user_email
        unique (email),
    constraint ux_user_login
        unique (login)
)
    charset = latin1;

create table jhi_user_authority
(
    user_id        bigint      not null,
    authority_name varchar(50) not null,
    primary key (user_id, authority_name),
    constraint fk_authority_name
        foreign key (authority_name) references jhi_authority (name),
    constraint fk_user_id
        foreign key (user_id) references jhi_user (id)
)
    charset = latin1;

create table notification_center
(
    id                bigint auto_increment
        primary key,
    title             varchar(255) not null,
    participant_id    char(36)     not null,
    execute_status    varchar(36)  not null,
    created_at        datetime     not null,
    attempt_count     int          not null,
    last_attempt_time datetime     not null,
    error_message     text         null,
    event_id          varchar(150) not null,
    content           text         not null,
    notification_type varchar(36)  not null,
    receiver_name     varchar(36)  not null,
    is_ready          tinyint(1)   null
)
    charset = latin1;

create index notification_center_execute_status_index
    on notification_center (execute_status);

create index notification_center_id_index
    on notification_center (id);

create index notification_center_is_ready_index
    on notification_center (is_ready);

create table order_transaction
(
    id                bigint auto_increment
        primary key,
    program_id        bigint        null,
    program_name      varchar(255)  null,
    client_name       varchar(255)  null,
    participant_name  varchar(255)  null,
    created_date      datetime      null,
    message           varchar(1020) null,
    json_content      json          null,
    email             varchar(255)  null,
    external_order_id varchar(255)  null,
    user_reward_id    bigint        null
)
    charset = latin1;

create table program
(
    id                           bigint auto_increment
        primary key,
    name                         varchar(255)            not null,
    start_date                   datetime                null,
    reset_date                   datetime                null,
    last_sent                    date                    null,
    is_retrigger_email           bit                     null,
    is_eligible                  bit                     null,
    is_sent_registration_email   bit                     null,
    is_Registered_for_platform   bit                     null,
    is_scheduled_screening       bit                     null,
    is_functionally              bit                     null,
    logo_url                     varchar(255)            null,
    description                  varchar(255)            null,
    user_point                   decimal(21, 2)          null,
    last_modified_date           datetime                null,
    created_date                 datetime                null,
    last_modified_by             varchar(255)            null,
    created_by                   varchar(255)            null,
    status                       varchar(255)            null,
    is_use_point                 bit                     null,
    is_screen                    bit                     null,
    is_hp                        bit                     null,
    is_well_matric               bit                     null,
    is_coaching                  bit                     null,
    is_use_level                 bit        default b'0' null,
    is_template                  bit        default b'0' not null,
    end_time_zone                varchar(255)            null,
    start_time_zone              varchar(255)            null,
    preview_time_zone            varchar(255)            null,
    preview_date                 datetime                null,
    is_preview                   bit        default b'0' null,
    level_structure              varchar(255)            null,
    program_length               int                     null,
    apply_reward_all_subgroup    bit        default b'0' null,
    is_hpsf                      bit        default b'0' null,
    is_htk                       bit        default b'0' null,
    is_labcorp                   bit        default b'0' null,
    labcorp_account_number       varchar(255)            null,
    labcorp_file_url             varchar(255)            null,
    color_value                  varchar(255)            null,
    biometric_lookback_date      datetime                null,
    biometric_deadline_date      datetime                null,
    landing_background_image_url varchar(400)            null,
    is_outcomes_lite             tinyint(1) default 0    null,
    is_tobacco_free              tinyint(1) default 0    null,
    is_outcomes                  tinyint(1) default 0    null
)
    charset = latin1;

create table client_program
(
    id          bigint auto_increment
        primary key,
    client_id   varchar(255) not null,
    program_id  bigint       not null,
    client_name varchar(255) null,
    constraint ux_clientId_programId
        unique (client_id, program_id),
    constraint fk_client_program_program_id
        foreign key (program_id) references program (id)
)
    charset = latin1;

create index client_program__client_name_index
    on client_program (client_name);

create table client_program_reward_setting
(
    id           bigint auto_increment
        primary key,
    client_id    varchar(255) not null,
    client_name  varchar(255) not null,
    access_token varchar(255) null,
    team_name    varchar(255) null,
    updated_date datetime     not null,
    updated_by   varchar(255) not null,
    program_id   bigint       not null,
    team_id      varchar(255) null,
    constraint fk_client_program_reward_setting_program_id
        foreign key (program_id) references program (id)
)
    charset = latin1;

create index program_name_index
    on program (name);

create index program_status_index
    on program (status);

create table program_activity
(
    id               bigint auto_increment
        primary key,
    activity_id      varchar(255)     not null,
    activity_code    varchar(255)     not null,
    program_id       bigint           not null,
    is_customized    bit default b'0' null,
    program_phase_id bigint           null,
    master_id        varchar(255)     null,
    constraint ux_program_activityid
        unique (program_id, activity_id),
    constraint fk_program_activity_program_id
        foreign key (program_id) references program (id)
)
    charset = latin1;

create index program_activity__activity_code_index
    on program_activity (activity_code);

create index program_activity__program_phase_id_index
    on program_activity (program_phase_id);

create table program_biometric_data
(
    id               bigint auto_increment
        primary key,
    biometric_code   varchar(50)  not null,
    biometric_name   varchar(255) not null,
    male_min         float        not null,
    male_max         float        not null,
    female_min       float        not null,
    female_max       float        not null,
    unidentified_min float        not null,
    unidentified_max float        not null
)
    charset = latin1;

create index program_biometric_data__biometric_code_index
    on program_biometric_data (biometric_code);

create index program_biometric_data__biometric_name_index
    on program_biometric_data (biometric_name);

create table program_category_point
(
    id            bigint auto_increment
        primary key,
    category_code varchar(255)     not null,
    category_name varchar(255)     null,
    percent_point decimal(19, 4)   null,
    value_point   decimal(21, 2)   null,
    program_id    bigint           not null,
    locked        bit default b'0' null,
    constraint ux_category_code_program
        unique (category_code, program_id),
    constraint fk_program_category_point_program_id
        foreign key (program_id) references program (id)
)
    charset = latin1;

create index program_category_point__category_code_index
    on program_category_point (category_code);

create index program_category_point__category_name_index
    on program_category_point (category_name);

create index program_category_point__program_id_index
    on program_category_point (program_id);

create table program_healthy_range
(
    id               bigint auto_increment
        primary key,
    male_min         float                not null,
    male_max         float                not null,
    female_min       float                not null,
    female_max       float                not null,
    unidentified_min float                not null,
    unidentified_max float                not null,
    is_apply_point   tinyint(1) default 0 null,
    program_id       bigint               not null,
    biometric_code   varchar(255)         not null,
    constraint fk_program_healthy_range_program
        foreign key (program_id) references program (id)
)
    charset = latin1;

create table program_level
(
    id          bigint auto_increment
        primary key,
    description varchar(255) null,
    start_point int          null,
    end_point   int          null,
    level_order int          null,
    icon_path   varchar(255) null,
    name        varchar(255) null,
    end_date    date         null,
    program_id  bigint       not null,
    constraint fk_program_level_program_id
        foreign key (program_id) references program (id)
)
    charset = latin1;

create index program_level_name_index
    on program_level (name);

create table program_level_activity
(
    id               bigint auto_increment
        primary key,
    activity_code    varchar(255) null,
    activity_id      varchar(255) not null,
    program_level_id bigint       not null,
    subgroup_id      varchar(255) null,
    subgroup_name    varchar(255) null,
    constraint fk_program_level_activity_program_level_id
        foreign key (program_level_id) references program_level (id)
)
    charset = latin1;

create index program_level_activity__activity_code_index
    on program_level_activity (activity_code);

create index program_level_activity__activity_id_index
    on program_level_activity (activity_id);

create index program_level_activity__subgroup_id_index
    on program_level_activity (subgroup_id);

create index program_level_activity__subgroup_name_index
    on program_level_activity (subgroup_name);

create table program_level_path
(
    id               bigint auto_increment
        primary key,
    path_id          varchar(255) not null,
    name             varchar(255) not null,
    subgroup_id      varchar(255) null,
    subgroup_name    varchar(255) null,
    path_type        varchar(255) null,
    path_category    varchar(255) null,
    program_level_id bigint       not null,
    constraint fk_program_level_path_program_level_id
        foreign key (program_level_id) references program_level (id)
)
    charset = latin1;

create index program_level_path__name_index
    on program_level_path (name);

create index program_level_path__path_category_index
    on program_level_path (path_category);

create index program_level_path__path_id_index
    on program_level_path (path_id);

create index program_level_path__path_type_index
    on program_level_path (path_type);

create index program_level_path__subgroup_id_index
    on program_level_path (subgroup_id);

create index program_level_path__subgroup_name_index
    on program_level_path (subgroup_name);

create table program_level_practice
(
    id            bigint auto_increment
        primary key,
    practice_id   varchar(255) not null,
    name          varchar(255) not null,
    subgroup_id   varchar(255) null,
    subgroup_name varchar(255) null
)
    charset = latin1;

create index program_level_practice__name_index
    on program_level_practice (name);

create index program_level_practice__practice_id_index
    on program_level_practice (practice_id);

create index program_level_practice__subgroup_id_index
    on program_level_practice (subgroup_id);

create table program_level_reward
(
    id               bigint auto_increment
        primary key,
    description      varchar(255)   null,
    quantity         int            null,
    code             varchar(255)   not null,
    reward_amount    decimal(21, 2) null,
    program_level_id bigint         not null,
    reward_type      varchar(255)   not null,
    campaign_id      varchar(255)   null,
    subgroup_id      varchar(255)   null,
    subgroup_name    varchar(255)   null,
    constraint fk_program_level_reward_program_level_id
        foreign key (program_level_id) references program_level (id)
)
    charset = latin1;

create index program_level_reward__campaign_id_index
    on program_level_reward (campaign_id);

create index program_level_reward__code_index
    on program_level_reward (code);

create index program_level_reward__reward_type_index
    on program_level_reward (reward_type);

create index program_level_reward__subgroup_id_index
    on program_level_reward (subgroup_id);

create table program_phase
(
    id          bigint auto_increment
        primary key,
    program_id  bigint   not null,
    start_date  datetime not null,
    end_date    datetime not null,
    phase_order int      not null,
    constraint fk_program_phase_program_id
        foreign key (program_id) references program (id)
)
    charset = latin1;

create table program_sub_category_point
(
    id                        bigint auto_increment
        primary key,
    code                      varchar(255)     not null,
    percent_point             decimal(19, 4)   null,
    value_point               decimal(21, 2)   null,
    completions_cap           int              null,
    program_category_point_id bigint           not null,
    name                      varchar(255)     null,
    locked                    bit default b'0' null,
    constraint ux_sub__category_code_sub
        unique (code, program_category_point_id),
    constraint fk_program_sub_category_point_program_category_point_id
        foreign key (program_category_point_id) references program_category_point (id)
)
    charset = latin1;

create index program_sub_category_point__code_index
    on program_sub_category_point (code);

create index program_sub_category_point__name_index
    on program_sub_category_point (name);

create table program_subgroup
(
    id            bigint auto_increment
        primary key,
    program_id    bigint       not null,
    subgroup_id   varchar(255) not null,
    subgroup_name varchar(255) not null,
    constraint fk_program_subgroup_program_id
        foreign key (program_id) references program (id)
)
    charset = latin1;

create index program_subgroup__subgroup_id_index
    on program_subgroup (subgroup_id);

create index program_subgroup__subgroup_name_index
    on program_subgroup (subgroup_name);

create table program_user
(
    id               bigint auto_increment
        primary key,
    participant_id   varchar(255)   not null,
    client_id        varchar(255)   not null,
    total_user_point decimal(21, 2) null,
    program_id       bigint         not null,
    current_level    int            null,
    email            varchar(255)   null,
    first_name       varchar(255)   null,
    last_name        varchar(255)   null,
    phone            varchar(255)   null,
    client_name      varchar(255)   null,
    subgroup_id      varchar(255)   null,
    subgroup_name    varchar(255)   null,
    constraint ux_participantId_clientId_programId
        unique (participant_id, client_id, program_id)
)
    charset = latin1;

create index program_user__client_name_index
    on program_user (client_name);

create index program_user__subgroup_id_index
    on program_user (subgroup_id);

create index program_user__subgroup_name_index
    on program_user (subgroup_name);

create table reward
(
    id          bigint auto_increment
        primary key,
    description varchar(255) null,
    code        varchar(255) not null,
    constraint ux_reward_code
        unique (code)
)
    charset = latin1;

create table setting_configuration
(
    id           bigint auto_increment
        primary key,
    name         varchar(255) not null,
    value_string varchar(255) null,
    constraint ux_setting_configuration
        unique (name)
)
    charset = latin1;

create table shedlock
(
    name       varchar(64)  not null
        primary key,
    lock_until timestamp(3) null,
    locked_at  timestamp(3) null,
    locked_by  varchar(255) null
)
    charset = latin1;

create table sub_category
(
    id          bigint auto_increment
        primary key,
    code        varchar(255) not null,
    name        varchar(255) null,
    category_id bigint       not null,
    constraint ux_sub_category_code
        unique (code),
    constraint fk_sub_category_category_id
        foreign key (category_id) references category (id)
)
    charset = latin1;

create table term_and_condition
(
    id                 bigint auto_increment
        primary key,
    client_id          varchar(255) not null,
    subgroup_id        varchar(255) null,
    title              varchar(255) null,
    content            text         null,
    is_target_subgroup tinyint(1)   null,
    subgroup_name      varchar(255) null
)
    charset = latin1;

create index term_and_condition__client_id_index
    on term_and_condition (client_id);

create index term_and_condition__subgroup_id_index
    on term_and_condition (subgroup_id);

create index term_and_condition__subgroup_name_index
    on term_and_condition (subgroup_name);

create index term_and_condition__title_index
    on term_and_condition (title);

create table user_event
(
    id              bigint auto_increment
        primary key,
    event_code      varchar(255)   not null,
    event_id        varchar(255)   not null,
    event_date      datetime       not null,
    event_point     decimal(21, 2) null,
    program_user_id bigint         not null,
    event_category  varchar(255)   null,
    constraint fk_user_event_program_user_id
        foreign key (program_user_id) references program_user (id)
)
    charset = latin1;

create index user_event_event_category_index
    on user_event (event_category);

create index user_event_event_code_index
    on user_event (event_code);

create index user_event_event_id_index
    on user_event (event_id);

create table user_reward
(
    id                   bigint auto_increment
        primary key,
    program_level        int            null,
    status               varchar(255)   null,
    order_id             varchar(255)   null,
    order_date           datetime       null,
    gift_id              varchar(255)   null,
    level_completed_date datetime       null,
    participant_id       varchar(255)   not null,
    email                varchar(255)   not null,
    client_id            varchar(255)   not null,
    program_user_id      bigint         not null,
    program_id           bigint         not null,
    reward_type          varchar(255)   null,
    reward_code          varchar(255)   null,
    reward_amount        decimal(21, 2) null,
    campaign_id          varchar(255)   null,
    reward_message       text           null,
    event_happen_at      datetime       null,
    event_id             int            null,
    constraint fk_user_reward_program_id
        foreign key (program_id) references program (id),
    constraint fk_user_reward_program_user_id
        foreign key (program_user_id) references program_user (id)
)
    charset = latin1;

create index user_reward__client_id_index
    on user_reward (client_id);

create index user_reward__gift_id_index
    on user_reward (gift_id);

create index user_reward__order_id_index
    on user_reward (order_id);

create index user_reward__participant_id_index
    on user_reward (participant_id);

create index user_reward__reward_code_index
    on user_reward (reward_code);

create index user_reward__reward_type_index
    on user_reward (reward_type);

create index user_reward__status_index
    on user_reward (status);

create table wellmetric_event_queue
(
    id                bigint auto_increment
        primary key,
    email             char(36)     not null,
    client_id         varchar(40)  null,
    participant_id    char(36)     not null,
    execute_status    varchar(36)  not null,
    gender            varchar(20)  not null,
    created_at        datetime     not null,
    attempt_count     int          not null,
    last_attempt_time datetime     not null,
    error_message     text         null,
    subgroup_id       varchar(36)  null,
    result            varchar(36)  null,
    event_code        varchar(150) not null,
    healthy_value     float        not null,
    event_id          varchar(150) not null,
    has_incentive     tinyint(1)   null,
    attempted_at      datetime     not null,
    program_id        bigint       not null,
    event_date        varchar(100) not null
)
    charset = latin1;

create index wellmetric_event_queue_eventCode_index
    on wellmetric_event_queue (event_code);

create index wellmetric_event_queue_execute_status_index
    on wellmetric_event_queue (execute_status);

create index wellmetric_event_queue_id_index
    on wellmetric_event_queue (id);

