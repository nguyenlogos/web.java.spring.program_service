/*Blood Pressure*/
UPDATE sub_category t SET t.name = 'Blood Pressure - Systolic' WHERE  t.code = 'BP_Systolic__c';
UPDATE sub_category t SET t.name = 'Blood Pressure - Diastolic' WHERE  t.code = 'BP_Diastolic__c';

/*Glucose*/
UPDATE sub_category t SET t.name = 'Glucose - Fasting' WHERE  t.code = 'RFpg__c';
UPDATE sub_category t SET t.name = 'Glucose - Non-Fasting' WHERE  t.code = 'Non_Fasting_Y_N__c';

/*Body Composition*/
UPDATE sub_category t SET t.name = 'Body Composition - BMI' WHERE  t.code = 'BMI__c';
UPDATE sub_category t SET t.name = 'Body Composition - Waist Circumference' WHERE  t.code = 'Waist__c';

/*Cholestorol*/
DELETE from sub_category  where sub_category.code = 'RCho__c';
DELETE from sub_category  where sub_category.code = 'RTrig__c';
DELETE from sub_category  where sub_category.code = 'TC_HDL_Ratio__c';
DELETE from sub_category  where sub_category.code = 'RHdl__c';
