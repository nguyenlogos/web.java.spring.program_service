create  table if not exists client_sub_domain (
        id  bigint auto_increment primary key,
        created_at datetime,
        modified_at datetime,
        client_id varchar(255),
        program_id bigint,
        program_name varchar(255),
        sub_domain_url varchar(255) not null unique
);

create index client_sub_domain_sub_domain_url_index
    on client_sub_domain (sub_domain_url);


