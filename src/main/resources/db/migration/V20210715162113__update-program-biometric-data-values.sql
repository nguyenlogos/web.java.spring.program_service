UPDATE program_biometric_data
SET male_min=1, male_max=120, female_min=1, female_max=120, unidentified_min=1, unidentified_max=120, modified_date=CURRENT_TIMESTAMP
WHERE biometric_code = 'BP_Systolic__c';

UPDATE program_biometric_data
SET male_min=1, male_max=50, female_min=1, female_max=50, unidentified_min=1, unidentified_max=50, modified_date=CURRENT_TIMESTAMP
WHERE biometric_code = 'RHdl__c';

UPDATE program_biometric_data
SET male_min=1, male_max=149, female_min=1, female_max=149, unidentified_min=1, unidentified_max=149, modified_date=CURRENT_TIMESTAMP
WHERE biometric_code = 'RTrig__c';

UPDATE program_biometric_data
SET male_min=1, male_max=200, female_min=1, female_max=200, unidentified_min=1, unidentified_max=200, modified_date=CURRENT_TIMESTAMP
WHERE biometric_code = 'RCho__c';

UPDATE program_biometric_data
SET male_min=1, male_max=125, female_min=1, female_max=125, unidentified_min=1, unidentified_max=125, modified_date=CURRENT_TIMESTAMP
WHERE biometric_code = 'RFpg__c';

UPDATE program_biometric_data
SET male_min=1, male_max=125, female_min=1, female_max=125, unidentified_min=1, unidentified_max=125, modified_date=CURRENT_TIMESTAMP
WHERE biometric_code = 'Non_Fasting_Y_N__c';

UPDATE program_biometric_data
SET male_min=1, male_max=4, female_min=1, female_max=4, unidentified_min=1, unidentified_max=4, modified_date=CURRENT_TIMESTAMP
WHERE biometric_code = 'TC_HDL_Ratio__c';

UPDATE program_biometric_data
SET male_min=1, male_max=35, female_min=1, female_max=35, unidentified_min=1, unidentified_max=35, modified_date=CURRENT_TIMESTAMP
WHERE biometric_code = 'Waist__c';
