create index e_consent__participant_id_index
    on e_consent (participant_id);

create index e_consent__program_id_index
    on e_consent (program_id);
