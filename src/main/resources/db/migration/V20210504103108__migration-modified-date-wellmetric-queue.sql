alter table wellmetric_event_queue add column modified_date datetime null;
update wellmetric_event_queue
set modified_date=created_at
where 1=1;

