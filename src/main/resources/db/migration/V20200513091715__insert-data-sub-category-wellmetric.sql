SET @code = 'WELLMETRIC_BLOOD_PRESSURE',
    @name = 'Blood Pressure',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
code = @code,
name = @name,
category_id = @category_id;


SET @code = 'WELLMETRIC_CHOLESTEROL',
    @name = 'Cholesterol',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
                     code = @code,
                     name = @name,
                     category_id = @category_id;

SET @code = 'WELLMETRIC_GLUCOSE',
    @name = 'Glucose',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
                     code = @code,
                     name = @name,
                     category_id = @category_id;

SET @code = 'WELLMETRIC_BODY_COMPOSITION',
    @name = 'Body Composition',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
                     code = @code,
                     name = @name,
                     category_id = @category_id;
