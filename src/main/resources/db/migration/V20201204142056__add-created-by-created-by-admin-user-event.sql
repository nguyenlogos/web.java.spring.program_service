alter table user_event add column  created_by varchar(50) null;
alter table user_event add column  created_by_admin varchar(160) null;

update user_event ue
    INNER JOIN  program_user pu ON ue.program_user_id = pu.id
set ue.created_by = pu.participant_id where 1=1;


alter table user_event_queue add column  created_by varchar(50) null;
alter table user_event_queue add column  created_by_admin varchar(160) null;

update user_event_queue ue
    INNER JOIN  program_user pu ON ue.program_user_id = pu.id
set ue.created_by = pu.participant_id where 1=1;
