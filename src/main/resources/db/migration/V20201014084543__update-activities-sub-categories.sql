UPDATE sub_category SET name = 'Individual: Informational' WHERE code = 'INFORMATIONAL';
UPDATE sub_category SET name = 'Individual: Self-tracking' WHERE code = 'SELF_TRACKING';
UPDATE sub_category SET name = 'Individual: Passive tracking' WHERE code = 'PASSIVE_TRACKING';
UPDATE sub_category SET name = 'Individual: Employer-verified' WHERE code = 'EMPLOYER_VERIFIED';

UPDATE program_sub_category_point SET name = 'Individual: Informational' WHERE code = 'INFORMATIONAL';
UPDATE program_sub_category_point SET name = 'Individual: Self-tracking' WHERE code = 'SELF_TRACKING';
UPDATE program_sub_category_point SET name = 'Individual: Passive tracking' WHERE code = 'PASSIVE_TRACKING';
UPDATE program_sub_category_point SET name = 'Individual: Employer-verified' WHERE code = 'EMPLOYER_VERIFIED';

SET @category_id = (SELECT id FROM category WHERE code = 'ACTIVITIES');
INSERT INTO sub_category (code, name, category_id) VALUES ('SELF_TRACKING_TEAM', 'Team: Self-tracking', @category_id);
INSERT INTO sub_category (code, name, category_id) VALUES ('PASSIVE_TRACKING_TEAM', 'Team: Passive tracking', @category_id);
INSERT INTO sub_category (code, name, category_id) VALUES ('SELF_TRACKING_COMPANY', 'All-company: Self-tracking', @category_id);
INSERT INTO sub_category (code, name, category_id) VALUES ('PASSIVE_TRACKING_COMPANY', 'All-company: Passive tracking', @category_id);
