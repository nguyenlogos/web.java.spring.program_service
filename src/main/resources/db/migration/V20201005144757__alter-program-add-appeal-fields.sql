ALTER TABLE program add is_appeals_form boolean default false;
ALTER TABLE program add appeals_form_file_url varchar(255) default null;
ALTER TABLE program add appeals_form_text varchar(5000) null;
