UPDATE program_biometric_data t SET t.female_max = 79,
                                    t.female_min = 1,
                                    t.male_min = 1,
                                    t.male_max = 79,
                                    t.unidentified_min = 1,
                                    t.unidentified_max = 79
WHERE  t.biometric_code = 'BP_Systolic__c';


UPDATE program_biometric_data t SET t.female_max = 34.9,
                                    t.female_min = 1,
                                    t.male_min = 1,
                                    t.male_max = 39.9,
                                    t.unidentified_min = 1,
                                    t.unidentified_max = 39.9
WHERE  t.biometric_code = 'Waist__c';
