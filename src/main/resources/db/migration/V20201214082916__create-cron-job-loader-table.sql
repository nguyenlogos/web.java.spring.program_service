create table cron_job_loader
(
    id  bigint auto_increment primary key,
    program_id bigint not null,
    created_at datetime,
    updated_at datetime,
    client_id varchar(255) not null,
    participant_id varchar(255),
    process_type varchar(100) null ,
    is_completed boolean default false,
    is_force boolean default false,
    error_message text null ,
    attempt_count integer,
    read_at datetime,
    is_pass_required boolean default false,
    list_required_items text
);

create index cron_job_loader_participant_id_index
    on cron_job_loader (participant_id);

create index user_event_queue_is_completed_index
    on cron_job_loader (is_completed);

create index user_event_queue_is_force_index
    on cron_job_loader (is_force);

