SET @code = 'WELLMETRIC_TOBACCO_ATTESTATION',
    @name = 'Tobacco Attestation',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
                     code = @code,
                     name = @name,
                     category_id = @category_id;


SET @code = 'WELLMETRIC_TOBACCO_RAS',
    @name = 'Tobacco Ras (if Tobacco free are award)',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
                     code = @code,
                     name = @name,
                     category_id = @category_id;


delete from sub_category where sub_category.code = "WELLMETRIC_TOBACCO_USER_PROGRAM";
delete from sub_category where sub_category.code = "WELLMETRIC_TOBACCO_FREE";
