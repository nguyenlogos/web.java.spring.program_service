create index user_event_date_index
    on user_event (event_date);

create index user_event_program_user_id_index
    on user_event (program_user_id);
