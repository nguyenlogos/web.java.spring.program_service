SET @code = 'WELLMETRIC_TOBACCO_ATTESTATION',
    @name = 'Tobacco Attestation',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
                     code = @code,
                     name = @name,
                     category_id = @category_id;


SET @code = 'WELLMETRIC_TOBACCO_USER_PROGRAM',
    @name = 'Tobacco User Program',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
                     code = @code,
                     name = @name,
                     category_id = @category_id;

SET @code = 'WELLMETRIC_TOBACCO_FREE',
    @name = 'Tobacco Free',
    @category_id = 7;
INSERT INTO  sub_category ( code, name, category_id)
VALUES (@code, @name, @category_id)
ON DUPLICATE KEY UPDATE
                     code = @code,
                     name = @name,
                     category_id = @category_id;
