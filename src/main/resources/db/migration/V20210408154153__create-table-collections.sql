create table program_collection (
	id  bigint not null auto_increment primary key,
	display_name VARCHAR(150) not null, -- limit character ?
	long_description VARCHAR(255), -- limit character ?
	program_id bigint not null,
	created_date datetime not null default  CURRENT_TIMESTAMP,
	modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	foreign key (program_id) references program(id)
);

create index program_collection_program_id_index on program_collection(program_id);
