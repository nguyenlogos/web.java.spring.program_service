create table program_sc_point_configuration
(
    id  bigint auto_increment primary key,
    sub_category_code varchar(255) not null ,
    is_blood_pressure_single_test boolean,
    is_blood_pressure_individual_test boolean,
    is_glucose_awarded_in_range boolean,
    is_bmi_awarded_in_range boolean,
    is_bmi_awarded_fasting boolean,
    is_bmi_awarded_non_fasting boolean,
    program_id bigint not null
);

create index program_sub_category_configuration_sub_category_code_index
    on program_sc_point_configuration (sub_category_code);

