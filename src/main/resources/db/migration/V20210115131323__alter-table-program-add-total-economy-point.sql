alter table program add column economy_point decimal(21,2) not null default 0.00;
update program set economy_point = user_point * 1.5 where user_point is not null;
