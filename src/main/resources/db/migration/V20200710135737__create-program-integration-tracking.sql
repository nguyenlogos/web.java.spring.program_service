create  table if not exists program_integration_tracking (
                            id  bigint auto_increment primary key,
                            payload text,
                            created_at datetime,
                            attempted_at datetime,
                            error_message text,
                            status varchar(50),
                            service_type varchar(255),
                            end_point text
);
