-- Insert Default Category
SET @code = 'COACH';
INSERT INTO category(id, code, name, category_type) VALUES (1, 'COACH', '1:1 Coaching', 'Change')
ON DUPLICATE KEY UPDATE code = @code;

SET @code = 'ACTIVITIES';
INSERT INTO category(id, code, name, category_type) VALUES (2, 'ACTIVITIES', 'Activities', 'Change')
ON DUPLICATE KEY UPDATE code = @code;

SET @code = 'ASSESSMENTS';
INSERT INTO category(id, code, name, category_type) VALUES (3, 'ASSESSMENTS', 'Assessments', 'Data Collection')
ON DUPLICATE KEY UPDATE code = @code;

SET @code = 'HPCONTENT';
INSERT INTO category(id, code, name, category_type) VALUES (4, 'HPCONTENT', 'HPCONTENT', 'HPCONTENT')
ON DUPLICATE KEY UPDATE code = @code;

SET @code = 'HPINTERACTIONS';
INSERT INTO category(id, code, name, category_type) VALUES (5, 'HPINTERACTIONS', 'HPINTERACTIONS', 'HPINTERACTIONS')
ON DUPLICATE KEY UPDATE code = @code;

SET @code = 'BONUS';
INSERT INTO category(id, code, name, category_type) VALUES (6, 'BONUS', 'BONUS', 'BONUS')
ON DUPLICATE KEY UPDATE code = @code;

SET @code = 'WELLMETRICS';
INSERT INTO category(id, code, name, category_type) VALUES (7, 'WELLMETRICS', 'Wellmetrics', 'Change')
ON DUPLICATE KEY UPDATE code = @code;

-- Insert Default Sub Category
SET @code = 'INFORMATIONAL',
    @category_id = 2;
INSERT INTO sub_category(code, name, category_id) VALUES ('INFORMATIONAL', 'Info Type', 2)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'PASSIVE_TRACKING',
    @category_id = 2;
INSERT INTO sub_category(code, name, category_id) VALUES ('PASSIVE_TRACKING', 'Passive Tracker', 2)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'FLOURING_INDEX',
    @category_id = 3;
INSERT INTO sub_category(code, name, category_id) VALUES ('FLOURING_INDEX', 'Flourishing Index (Quarterly)', 3)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'SELF_TRACKING',
    @category_id = 2;
INSERT INTO sub_category(code, name, category_id) VALUES ('SELF_TRACKING', 'Self Tracking', 2)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'REGISTRATION_ONE_ONE_COACHING',
    @category_id = 1;
INSERT INTO sub_category(code, name, category_id) VALUES ('REGISTRATION_ONE_ONE_COACHING', 'Registration', 1)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'COMPLETE_ONE_ONE_WEEKLY_SESSION',
    @category_id = 1;
INSERT INTO sub_category(code, name, category_id) VALUES ('COMPLETE_ONE_ONE_WEEKLY_SESSION', 'Scheduled sessions – pts per session w/ cap', 1)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'COMPLETE_ONE_ONE_COACHING',
    @category_id = 1;
INSERT INTO sub_category(code, name, category_id) VALUES ('COMPLETE_ONE_ONE_COACHING', 'Completion', 1)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'SYSTEM_REPORTED',
    @category_id = 2;
INSERT INTO sub_category(code, name, category_id) VALUES ('SYSTEM_REPORTED', 'System reported', 2)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'SEFT_REPORTED',
    @category_id = 2;
INSERT INTO sub_category(code, name, category_id) VALUES ('SEFT_REPORTED', 'Self reported', 2)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'HUMAN_PERFORMANCE_ASSESSMENT',
    @category_id = 3;
INSERT INTO sub_category(code, name, category_id) VALUES ('HUMAN_PERFORMANCE_ASSESSMENT', 'Human Performance Assessment', 3)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'OTHER_ASSESSMENT',
    @category_id = 3;
INSERT INTO sub_category(code, name, category_id) VALUES ('OTHER_ASSESSMENT', 'Other Assessments', 3)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'WELlMETRIC_SCREENNING',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('WELlMETRIC_SCREENNING', 'Wellmetric Screening(1-time)', 7)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'WELLMETRIC_REGISTRATION',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('WELLMETRIC_REGISTRATION', 'Wellmetrics Registration (1-Time)', 7)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'MOOD',
    @category_id = 5;
INSERT INTO sub_category(code, name, category_id) VALUES ('MOOD', 'Mood Tracking', 5)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'GRATITUDE',
    @category_id = 5;
INSERT INTO sub_category(code, name, category_id) VALUES ('GRATITUDE', 'Gratitude Journal', 5)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'VISION',
    @category_id = 5;
INSERT INTO sub_category(code, name, category_id) VALUES ('VISION', 'Vision', 5)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'WINS',
    @category_id = 5;
INSERT INTO sub_category(code, name, category_id) VALUES ('WINS', 'Wins', 5)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'PATHS',
    @category_id = 4;
INSERT INTO sub_category(code, name, category_id) VALUES ('PATHS', 'Path Completion', 4)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'PRACTICES',
    @category_id = 4;
INSERT INTO sub_category(code, name, category_id) VALUES ('PRACTICES', 'Practice Completion', 4)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'ACTIONS',
    @category_id = 4;
INSERT INTO sub_category(code, name, category_id) VALUES ('ACTIONS', 'Actions', 4)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'EMPLOYER_VERIFIED',
    @category_id = 2;
INSERT INTO sub_category(code, name, category_id) VALUES ('EMPLOYER_VERIFIED', 'EMPLOYER VERIFIED', 2)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'BP_Systolic__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('BP_Systolic__c', 'Blood Pressure - Systolic', 7)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'BP_Diastolic__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('BP_Diastolic__c', 'Blood Pressure - Diastolic', 7)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'Waist__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('Waist__c', 'Body Composition - Waist Circumference', 7)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'RFpg__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('RFpg__c', 'Glucose - Fasting', 7)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'Non_Fasting_Y_N__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('Non_Fasting_Y_N__c', 'Glucose - Non-Fasting', 7)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'BMI__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('BMI__c', 'Body Composition - BMI', 7)
ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'WELLMETRIC_BLOOD_PRESSURE',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('WELLMETRIC_BLOOD_PRESSURE', 'Blood Pressure', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'WELLMETRIC_GLUCOSE',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('WELLMETRIC_GLUCOSE', 'Glucose', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'WELLMETRIC_BODY_COMPOSITION',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('WELLMETRIC_BODY_COMPOSITION', 'Body Composition', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'TC_HDL_Ratio__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('TC_HDL_Ratio__c', 'Total Cholesterol / HDL Ratio', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'RTrig__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('RTrig__c', 'Triglycerides', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'RCho__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('RCho__c', 'Total Cholesterol', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'RHdl__c',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('RHdl__c', 'HDL', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'WELLMETRIC_TOBACCO_ATTESTATION',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('WELLMETRIC_TOBACCO_ATTESTATION', 'Tobacco Attestation', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;

SET @code = 'WELLMETRIC_TOBACCO_RAS',
    @category_id = 7;
INSERT INTO sub_category(code, name, category_id) VALUES ('WELLMETRIC_TOBACCO_RAS', 'Tobacco Ras (if Tobacco free are award)', 7) ON DUPLICATE KEY UPDATE code = @code, category_id = @category_id;


DELETE FROM sub_category
WHERE sub_category.code = 'ONE_TIME_TRACKING';

DELETE FROM sub_category
WHERE sub_category.code = 'WEEKLY_TRACKING';

DELETE FROM sub_category
WHERE sub_category.code = 'GROUP_TRACKING';

DELETE FROM sub_category
WHERE sub_category.code = 'REQUIRED';

-- Insert Default bio
SET @biometric_code = 'BP_Systolic__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'Systolic' as biometric_name, 1 as male_min, 79 as male_max, 1 as female_min, 79 as female_max, 1 as unidentified_min, 79 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'BP_Diastolic__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'Diastolic' as biometric_name, 1 as male_min, 80 as male_max, 1 as female_min, 80 as female_max, 1 as unidentified_min, 80 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'Waist__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'Waist' as biometric_name, 1 as male_min, 39.0 as male_max, 1 as female_min, 34.9 as female_max, 1 as unidentified_min, 39.9 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'RFpg__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'Glucose' as biometric_name, 70 as male_min, 99 as male_max, 70 as female_min, 99 as female_max, 70 as unidentified_min, 99 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'Non_Fasting_Y_N__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'Non-Fasting Glucose' as biometric_name, 70 as male_min, 125 as male_max, 70 as female_min, 125 as female_max, 70 as unidentified_min, 125 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'BMI__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'BMI' as biometric_name, 1 as male_min, 25 as male_max, 1 as female_min, 25 as female_max, 1 as unidentified_min, 25 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'TC_HDL_Ratio__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'Total Cholesterol / HDL Ratio' as biometric_name, 1 as male_min, 4.5 as male_max, 1 as female_min, 4 as female_max, 1 as unidentified_min, 4.5 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'RCho__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'Total Cholesterol' as biometric_name, 0 as male_min, 0 as male_max, 0 as female_min, 0 as female_max, 0 as unidentified_min, 0 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'RHdl__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'HDL' as biometric_name, 0 as male_min, 0 as male_max, 0 as female_min, 0 as female_max, 0 as unidentified_min, 0 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;

SET @biometric_code = 'RTrig__c';
INSERT INTO program_biometric_data(biometric_code, biometric_name, male_min, male_max, female_min, female_max, unidentified_min, unidentified_max)
SELECT * FROM (SELECT @biometric_code as biometric_code, 'Triglycerides' as biometric_name, 0 as male_min, 0 as male_max, 0 as female_min, 0 as female_max, 0 as unidentified_min, 0 as unidentified_max) AS tmp
WHERE NOT EXISTS (
    SELECT biometric_name FROM program_biometric_data WHERE biometric_code = @biometric_code
) LIMIT 1;
