ALTER TABLE user_event ADD appeal_applied tinyint(1) default 0;
ALTER TABLE wellmetric_event_queue ADD appeal_applied tinyint(1) default 0;
