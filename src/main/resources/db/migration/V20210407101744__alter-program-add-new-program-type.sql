alter table program add column program_type varchar(100) not null default 'PARTICIPANT_PROGRAM';

update program
set program_type = 'OUTCOMES_LITE'
where is_outcomes_lite = 1 ;

alter table program drop column is_outcomes_lite;
