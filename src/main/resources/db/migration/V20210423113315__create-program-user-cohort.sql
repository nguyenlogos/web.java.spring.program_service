create table program_user_cohort (
                                     id  bigint not null auto_increment primary key,
                                     program_cohort_id bigint not null,
                                     program_user_id bigint not null,
                                     progress bigint null,
                                     status varchar(100) not null,
                                     number_of_pass integer,
                                     number_of_failure integer,
                                     created_date datetime not null default  CURRENT_TIMESTAMP,
                                     modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
                                     foreign key (program_user_id) references program_user(id),
                                     foreign key (program_cohort_id) references program_cohort(id));


create table program_user_logs (
                                     id  bigint not null auto_increment primary key,
                                     participant_id varchar(150) not null,
                                     program_id bigint not null,
                                     code varchar(100) default null ,
                                     message varchar(4000) default null,
                                     created_date datetime not null default  CURRENT_TIMESTAMP,
                                     modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
);

create index program_user_cohort_status_index on program_user_cohort(status);
