create table cron_job_execution_tracking
(
    id  bigint auto_increment primary key,
    name varchar(255) not null,
    created_at datetime,
    updated_at datetime,
    client_id varchar(255) not null,
    error_message text null,
    number_of_records integer,
    process_status varchar(160),
    payload text
);

create index cron_job_execution_tracking_client_id_index
    on cron_job_execution_tracking (client_id);
