create  table e_consent (
                            id  bigint auto_increment primary key,
                            participant_id varchar(36) not null ,
                            program_id bigint not null,
                            has_confirmed boolean,
                            created_at datetime,
                            term_and_condition_id bigint
);
