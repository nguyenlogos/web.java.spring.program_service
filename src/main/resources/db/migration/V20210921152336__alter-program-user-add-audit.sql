alter table program_user add last_modified_date datetime default  CURRENT_TIMESTAMP;
alter table program_user add created_by  varchar(50) null;
alter table program_user add  last_modified_by varchar(50) null;


update program_user
set last_modified_date = modified_date,
    created_by = 'system',
    last_modified_by = 'system'
where 1=1;
