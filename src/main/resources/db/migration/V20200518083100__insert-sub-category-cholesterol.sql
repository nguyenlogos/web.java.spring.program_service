INSERT INTO sub_category ( code, name, category_id) VALUES ( 'TC_HDL_Ratio__c', 'Total Cholesterol / HDL Ratio', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'RTrig__c', 'Triglycerides', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'RCho__c', 'Total Cholesterol', 7);
INSERT INTO sub_category ( code, name, category_id) VALUES ( 'RHdl__c', 'HDL', 7);
