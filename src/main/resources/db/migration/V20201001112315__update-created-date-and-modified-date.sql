alter table program_user add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_user add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table user_event add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table user_event add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table user_reward add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table user_reward add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table program_level add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table program_level add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;

alter table client_program add column  created_date datetime not null default  CURRENT_TIMESTAMP;
alter table client_program add column  modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP;
