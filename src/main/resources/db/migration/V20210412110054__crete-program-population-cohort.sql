create table program_cohort (
                                           id  bigint not null auto_increment primary key,
                                           cohort_name varchar(150),
                                           program_id bigint not null,
                                           created_date datetime not null default  CURRENT_TIMESTAMP,
                                           modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
                                           foreign key (program_id) references program(id)
);

create table program_cohort_data_input(
	id  bigint not null auto_increment primary key,
	name_data_input varchar(150),
	input_type varchar(100),
	created_date datetime not null default  CURRENT_TIMESTAMP,
	modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP
);


create table program_cohort_rule (
	id  bigint not null auto_increment primary key,
	rules varchar(1000) default null,
    data_input_type varchar(150) not null,
	program_cohort_id bigint not null,
	created_date datetime not null default  CURRENT_TIMESTAMP,
	modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	foreign key (program_cohort_id) references program_cohort(id)
);


create table program_cohort_collection(
		id  bigint not null auto_increment primary key,
		program_collection_id bigint not null,
		program_cohort_id bigint not null,
		required_completion int default 0,
		required_level integer default null, -- add level ?
		created_date datetime not null default  CURRENT_TIMESTAMP,
		modified_date datetime not null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		foreign key (program_collection_id) references program_collection(id),
		foreign key (program_cohort_id) references program_cohort(id)
);


create index program_population_rule_program_cohort_id_index on program_cohort_rule(program_cohort_id);
create index program_population_cohort_collection_id_index on program_cohort_collection(program_collection_id);
create index program_population_cohort_program_population_id_index on program_cohort_collection(program_cohort_id);
create index program_population_cohort_collection_required_level_index on program_cohort_collection(required_level);


insert into program_cohort_data_input(name_data_input, input_type, created_date, modified_date) values ('Biometric Data', 'BIOMETRIC_INPUT', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP);
