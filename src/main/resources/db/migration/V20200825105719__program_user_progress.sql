create  table if not exists program_user_level_progress (
                                                             id  bigint auto_increment primary key,
                                                             user_event_id bigint,
                                                             program_user_id bigint,
                                                             created_at datetime,
                                                             level_up_at datetime,
                                                             level_from integer,
                                                             level_to integer,
                                                             has_reward boolean
);
