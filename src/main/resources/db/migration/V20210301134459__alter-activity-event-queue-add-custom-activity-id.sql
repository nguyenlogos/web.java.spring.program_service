alter table activity_event_queue add column custom_activity_id bigint;
alter table activity_event_queue add column attempted_count INTEGER default 0;
