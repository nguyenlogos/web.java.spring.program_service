ALTER TABLE program_activity add bonus_points_enabled boolean default null;
ALTER TABLE program_activity add bonus_points decimal(21, 2) default null;
