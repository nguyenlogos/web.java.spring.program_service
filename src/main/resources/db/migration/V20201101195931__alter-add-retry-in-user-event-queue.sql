alter table user_event_queue add attempt_count int default 0;
create index user_event_queue__execute_status_index
    on user_event_queue (execute_status);

create index user_event_queue__attempt_count_index
    on user_event_queue (attempt_count);

create index user_event_queue__execute_status_attemp_index
    on user_event_queue (execute_status, attempt_count);

create index user_event_queue__point_execute_status_attemp_index
    on user_event_queue (event_point, execute_status, attempt_count);
