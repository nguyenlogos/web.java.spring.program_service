export interface IProgramLevelActivity {
  id?: number;
  activityCode?: string;
  activityId?: string;
  subgroupId?: string;
  subgroupName?: string;
  programLevelDescription?: string;
  programLevelId?: number;
}

export const defaultValue: Readonly<IProgramLevelActivity> = {};
