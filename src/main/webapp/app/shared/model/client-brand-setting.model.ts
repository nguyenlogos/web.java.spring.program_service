export interface IClientBrandSetting {
  id?: number;
  clientId?: string;
  subPathUrl?: string;
  webLogoUrl?: string;
  primaryColorValue?: string;
  clientName?: string;
  clientUrl?: string;
}

export const defaultValue: Readonly<IClientBrandSetting> = {};
