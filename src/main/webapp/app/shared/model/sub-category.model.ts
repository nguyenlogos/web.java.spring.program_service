export interface ISubCategory {
  id?: number;
  code?: string;
  name?: string;
  categoryCode?: string;
  categoryId?: number;
}

export const defaultValue: Readonly<ISubCategory> = {};
