import { Moment } from 'moment';

export const enum CohortStatus {
  INPROGRESS = 'INPROGRESS',
  PENDING = 'PENDING',
  COMPLETED = 'COMPLETED'
}

export interface IProgramUserCohort {
  id?: number;
  progress?: number;
  numberOfPass?: number;
  numberOfFailure?: number;
  status?: CohortStatus;
  createdDate?: Moment;
  modifiedDate?: Moment;
  programUserId?: number;
  programCohortId?: number;
}

export const defaultValue: Readonly<IProgramUserCohort> = {};
