import { Moment } from 'moment';

export const enum NotificationType {
  WELLMETRIC_SCREENING_RESULT = 'WELLMETRIC_SCREENING_RESULT'
}

export interface INotificationCenter {
  id?: number;
  title?: string;
  participantId?: string;
  executeStatus?: string;
  createdAt?: Moment;
  attemptCount?: number;
  lastAttemptTime?: Moment;
  errorMessage?: string;
  content?: string;
  notificationType?: NotificationType;
  eventId?: string;
  isReady?: boolean;
  receiverName?: string;
}

export const defaultValue: Readonly<INotificationCenter> = {
  isReady: false
};
