import { Moment } from 'moment';

export interface IProgramPhase {
  id?: number;
  phaseOrder?: number;
  startDate?: Moment;
  endDate?: Moment;
  programName?: string;
  programId?: number;
}

export const defaultValue: Readonly<IProgramPhase> = {};
