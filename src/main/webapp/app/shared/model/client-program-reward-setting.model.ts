import { Moment } from 'moment';

export interface IClientProgramRewardSetting {
  id?: number;
  clientId?: string;
  clientName?: string;
  updatedDate?: Moment;
  updatedBy?: string;
  customerIdentifier?: string;
  accountIdentifier?: string;
  accountThreshold?: number;
  currentBalance?: number;
  clientEmail?: string;
  programName?: string;
  programId?: number;
}

export const defaultValue: Readonly<IClientProgramRewardSetting> = {};
