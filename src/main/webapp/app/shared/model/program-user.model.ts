export interface IProgramUser {
  id?: number;
  participantId?: string;
  clientId?: string;
  totalUserPoint?: number;
  programId?: number;
  currentLevel?: number;
  email?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  clientName?: string;
  subgroupId?: string;
  subgroupName?: string;
  isTobaccoUser?: boolean;
}

export const defaultValue: Readonly<IProgramUser> = {
  isTobaccoUser: false
};
