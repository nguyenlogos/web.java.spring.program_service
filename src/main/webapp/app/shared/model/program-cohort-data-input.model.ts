import { Moment } from 'moment';

export interface IProgramCohortDataInput {
  id?: number;
  nameDataInput?: string;
  inputType?: string;
  createdDate?: Moment;
  modifiedDate?: Moment;
}

export const defaultValue: Readonly<IProgramCohortDataInput> = {};
