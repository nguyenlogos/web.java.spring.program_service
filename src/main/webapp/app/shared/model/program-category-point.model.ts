export interface IProgramCategoryPoint {
  id?: number;
  categoryCode?: string;
  categoryName?: string;
  percentPoint?: number;
  valuePoint?: number;
  locked?: boolean;
  programName?: string;
  programId?: number;
}

export const defaultValue: Readonly<IProgramCategoryPoint> = {
  locked: false
};
