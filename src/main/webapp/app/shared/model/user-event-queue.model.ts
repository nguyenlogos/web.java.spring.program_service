import { Moment } from 'moment';

export interface IUserEventQueue {
  id?: number;
  eventCode?: string;
  eventId?: string;
  eventDate?: Moment;
  eventPoint?: number;
  eventCategory?: string;
  programUserId?: number;
  executeStatus?: string;
  programId?: number;
  readAt?: Moment;
  errorMessage?: string;
  currentStep?: string;
}

export const defaultValue: Readonly<IUserEventQueue> = {};
