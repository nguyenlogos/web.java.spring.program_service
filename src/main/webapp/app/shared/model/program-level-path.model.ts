export interface IProgramLevelPath {
  id?: number;
  pathId?: string;
  name?: string;
  subgroupId?: string;
  subgroupName?: string;
  pathType?: string;
  pathCategory?: string;
  programLevelName?: string;
  programLevelId?: number;
}

export const defaultValue: Readonly<IProgramLevelPath> = {};
