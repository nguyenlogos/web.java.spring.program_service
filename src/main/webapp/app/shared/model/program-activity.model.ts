export interface IProgramActivity {
  id?: number;
  activityId?: string;
  activityCode?: string;
  isCustomized?: boolean;
  masterId?: string;
  programName?: string;
  programId?: number;
  programPhasePhaseOrder?: string;
  programPhaseId?: number;
}

export const defaultValue: Readonly<IProgramActivity> = {
  isCustomized: false
};
