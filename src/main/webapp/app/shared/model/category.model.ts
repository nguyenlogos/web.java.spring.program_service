export interface ICategory {
  id?: number;
  code?: string;
  name?: string;
  categoryType?: string;
}

export const defaultValue: Readonly<ICategory> = {};
