import { Moment } from 'moment';

export interface IProgamLevelUser {
  id?: number;
  level?: number;
  completedDate?: Moment;
  currentPoint?: number;
  programUserProgramId?: string;
  programUserId?: number;
}

export const defaultValue: Readonly<IProgamLevelUser> = {};
