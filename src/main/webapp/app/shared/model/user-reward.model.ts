import { Moment } from 'moment';

export interface IUserReward {
  id?: number;
  programLevel?: number;
  status?: string;
  orderId?: string;
  orderDate?: Moment;
  giftId?: string;
  levelCompletedDate?: Moment;
  participantId?: string;
  email?: string;
  clientId?: string;
  rewardType?: string;
  rewardCode?: string;
  rewardAmount?: number;
  campaignId?: string;
  rewardMessage?: string;
  eventId?: string;
  programUserParticipantId?: string;
  programUserId?: number;
  programName?: string;
  programId?: number;
}

export const defaultValue: Readonly<IUserReward> = {};
