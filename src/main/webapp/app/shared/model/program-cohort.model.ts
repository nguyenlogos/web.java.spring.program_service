import { Moment } from 'moment';
import { IProgramCohortRule } from 'app/shared/model/program-cohort-rule.model';
import { IProgramCohortCollection } from 'app/shared/model/program-cohort-collection.model';

export interface IProgramCohort {
  id?: number;
  cohortName?: string;
  createdDate?: Moment;
  modifiedDate?: Moment;
  isDefault?: boolean;
  programId?: number;
  programCohortRules?: IProgramCohortRule[];
  programCohortCollections?: IProgramCohortCollection[];
}

export const defaultValue: Readonly<IProgramCohort> = {
  isDefault: false
};
