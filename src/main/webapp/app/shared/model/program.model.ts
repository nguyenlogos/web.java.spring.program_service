import { Moment } from 'moment';
import { IProgramCategoryPoint } from 'app/shared/model/program-category-point.model';
import { IProgramHealthyRange } from 'app/shared/model/program-healthy-range.model';

export interface IProgram {
  id?: number;
  name?: string;
  lastSent?: Moment;
  isRetriggerEmail?: boolean;
  isEligible?: boolean;
  isSentRegistrationEmail?: boolean;
  isRegisteredForPlatform?: boolean;
  isScheduledScreening?: boolean;
  isFunctionally?: boolean;
  logoUrl?: string;
  description?: string;
  userPoint?: number;
  lastModifiedDate?: Moment;
  createdDate?: Moment;
  lastModifiedBy?: string;
  createdBy?: string;
  status?: string;
  isUsePoint?: boolean;
  isScreen?: boolean;
  isCoaching?: boolean;
  isWellMatric?: boolean;
  isHP?: boolean;
  isUseLevel?: boolean;
  startDate?: Moment;
  resetDate?: Moment;
  isTemplate?: boolean;
  isPreview?: boolean;
  previewDate?: Moment;
  previewTimeZone?: string;
  startTimeZone?: string;
  endTimeZone?: string;
  levelStructure?: string;
  programLength?: number;
  isHPSF?: boolean;
  isHTK?: boolean;
  isLabcorp?: boolean;
  labcorpAccountNumber?: string;
  labcorpFileUrl?: string;
  applyRewardAllSubgroup?: boolean;
  biometricDeadlineDate?: Moment;
  biometricLookbackDate?: Moment;
  landingBackgroundImageUrl?: string;
  isTobaccoSurchargeManagement?: boolean;
  tobaccoAttestationDeadline?: Moment;
  tobaccoProgramDeadline?: Moment;
  isAwardedForTobaccoAttestation?: boolean;
  isAwardedForTobaccoRas?: boolean;
  programCategoryPoints?: IProgramCategoryPoint[];
  programHealthyRanges?: IProgramHealthyRange[];
}

export const defaultValue: Readonly<IProgram> = {
  isRetriggerEmail: false,
  isEligible: false,
  isSentRegistrationEmail: false,
  isRegisteredForPlatform: false,
  isScheduledScreening: false,
  isFunctionally: false,
  isUsePoint: false,
  isScreen: false,
  isCoaching: false,
  isWellMatric: false,
  isHP: false,
  isUseLevel: false,
  isTemplate: false,
  isPreview: false,
  isHPSF: false,
  isHTK: false,
  isLabcorp: false,
  applyRewardAllSubgroup: false,
  isTobaccoSurchargeManagement: false,
  isAwardedForTobaccoAttestation: false,
  isAwardedForTobaccoRas: false
};
