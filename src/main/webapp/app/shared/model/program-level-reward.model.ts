export interface IProgramLevelReward {
  id?: number;
  description?: string;
  quantity?: number;
  code?: string;
  rewardAmount?: number;
  rewardType?: string;
  campaignId?: string;
  subgroupId?: string;
  subgroupName?: string;
  programLevelName?: string;
  programLevelId?: number;
}

export const defaultValue: Readonly<IProgramLevelReward> = {};
