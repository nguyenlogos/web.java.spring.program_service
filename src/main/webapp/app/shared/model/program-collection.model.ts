import { Moment } from 'moment';
import { IProgramCollectionContent } from 'app/shared/model/program-collection-content.model';

export interface IProgramCollection {
  id?: number;
  displayName?: string;
  longDescription?: string;
  createdDate?: Moment;
  modifiedDate?: Moment;
  programId?: number;
  programCollectionContents?: IProgramCollectionContent[];
}

export const defaultValue: Readonly<IProgramCollection> = {};
