export interface IClientProgram {
  id?: number;
  clientId?: string;
  clientName?: string;
  programName?: string;
  programId?: number;
}

export const defaultValue: Readonly<IClientProgram> = {};
