import { Moment } from 'moment';

export interface IProgramCohortRule {
  id?: number;
  dataInputType?: string;
  rules?: string;
  createdDate?: Moment;
  modifiedDate?: Moment;
  programCohortId?: number;
}

export const defaultValue: Readonly<IProgramCohortRule> = {};
