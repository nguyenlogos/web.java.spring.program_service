export interface IProgramSubCategoryConfiguration {
  id?: number;
  subCategoryCode?: string;
  isBloodPressureSingleTest?: boolean;
  isBloodPressureIndividualTest?: boolean;
  isGlucoseAwardedInRange?: boolean;
  isBmiAwardedInRange?: boolean;
  isBmiAwardedFasting?: boolean;
  isBmiAwardedNonFasting?: boolean;
  programId?: number;
}

export const defaultValue: Readonly<IProgramSubCategoryConfiguration> = {
  isBloodPressureSingleTest: false,
  isBloodPressureIndividualTest: false,
  isGlucoseAwardedInRange: false,
  isBmiAwardedInRange: false,
  isBmiAwardedFasting: false,
  isBmiAwardedNonFasting: false
};
