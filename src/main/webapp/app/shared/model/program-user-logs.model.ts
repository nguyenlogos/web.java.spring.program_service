import { Moment } from 'moment';

export interface IProgramUserLogs {
  id?: number;
  participantId?: string;
  programId?: number;
  code?: string;
  message?: string;
  createdDate?: Moment;
  modifiedDate?: Moment;
}

export const defaultValue: Readonly<IProgramUserLogs> = {};
