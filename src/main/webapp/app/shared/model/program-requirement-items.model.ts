import { Moment } from 'moment';

export interface IProgramRequirementItems {
  id?: number;
  itemId?: string;
  nameOfItem?: string;
  typeOfItem?: string;
  itemCode?: string;
  subgroupId?: string;
  subgroupName?: string;
  createdAt?: Moment;
  programLevelId?: number;
}

export const defaultValue: Readonly<IProgramRequirementItems> = {};
