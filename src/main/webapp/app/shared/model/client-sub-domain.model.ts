import { Moment } from 'moment';

export interface IClientSubDomain {
  id?: number;
  clientId?: string;
  subDomainUrl?: string;
  createdAt?: Moment;
  modifiedAt?: Moment;
  programId?: number;
  programName?: string;
}

export const defaultValue: Readonly<IClientSubDomain> = {};
