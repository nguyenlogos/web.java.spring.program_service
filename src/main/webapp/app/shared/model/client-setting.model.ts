import { Moment } from 'moment';

export interface IClientSetting {
  id?: number;
  clientId?: string;
  clientName?: string;
  clientEmail?: string;
  tangoAccountIdentifier?: string;
  tangoCustomerIdentifier?: string;
  tangoCurrentBalance?: number;
  tangoThresholdBalance?: number;
  tangoCreditToken?: string;
  updatedDate?: Moment;
  updatedBy?: string;
}

export const defaultValue: Readonly<IClientSetting> = {};
