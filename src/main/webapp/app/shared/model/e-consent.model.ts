import { Moment } from 'moment';

export interface IEConsent {
  id?: number;
  participantId?: string;
  programId?: number;
  hasConfirmed?: boolean;
  createdAt?: Moment;
  termAndConditionId?: number;
}

export const defaultValue: Readonly<IEConsent> = {
  hasConfirmed: false
};
