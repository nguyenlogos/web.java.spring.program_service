export interface IProgramSubgroup {
  id?: number;
  subgroupId?: string;
  subgroupName?: string;
  programName?: string;
  programId?: number;
}

export const defaultValue: Readonly<IProgramSubgroup> = {};
