import { Moment } from 'moment';

export interface IActivityEventQueue {
  id?: number;
  participantId?: string;
  clientId?: string;
  activityCode?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  createdDate?: Moment;
  transactionId?: string;
}

export const defaultValue: Readonly<IActivityEventQueue> = {};
