export interface IReward {
  id?: number;
  description?: string;
  code?: string;
}

export const defaultValue: Readonly<IReward> = {};
