export interface ITermAndCondition {
  id?: number;
  clientId?: string;
  subgroupId?: string;
  title?: string;
  content?: string;
  isTargetSubgroup?: boolean;
  subgroupName?: string;
}

export const defaultValue: Readonly<ITermAndCondition> = {
  isTargetSubgroup: false
};
