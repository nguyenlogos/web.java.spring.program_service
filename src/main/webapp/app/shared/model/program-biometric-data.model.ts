export interface IProgramBiometricData {
  id?: number;
  biometricCode?: string;
  biometricName?: string;
  maleMin?: number;
  maleMax?: number;
  femaleMin?: number;
  femaleMax?: number;
  unidentifiedMin?: number;
  unidentifiedMax?: number;
}

export const defaultValue: Readonly<IProgramBiometricData> = {};
