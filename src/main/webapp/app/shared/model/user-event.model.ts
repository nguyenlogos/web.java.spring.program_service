import { Moment } from 'moment';

export interface IUserEvent {
  id?: number;
  eventCode?: string;
  eventId?: string;
  eventDate?: Moment;
  eventPoint?: number;
  eventCategory?: string;
  programUserParticipantId?: string;
  programUserId?: number;
}

export const defaultValue: Readonly<IUserEvent> = {};
