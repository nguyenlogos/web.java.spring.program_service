import { Moment } from 'moment';

export interface IProgramCollectionContent {
  id?: number;
  itemName?: string;
  itemId?: string;
  itemType?: string;
  contentType?: string;
  itemIcon?: string;
  hasRequired?: boolean;
  createdDate?: Moment;
  modifiedDate?: Moment;
  programCollectionId?: number;
}

export const defaultValue: Readonly<IProgramCollectionContent> = {
  hasRequired: false
};
