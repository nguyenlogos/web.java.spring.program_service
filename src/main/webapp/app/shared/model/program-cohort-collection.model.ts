import { Moment } from 'moment';

export interface IProgramCohortCollection {
  id?: number;
  requiredCompletion?: number;
  requiredLevel?: number;
  createdDate?: Moment;
  modifiedDate?: Moment;
  programCollectionId?: number;
  programCohortId?: number;
}

export const defaultValue: Readonly<IProgramCohortCollection> = {};
