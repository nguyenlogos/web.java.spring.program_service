import { Moment } from 'moment';
import { IProgramRequirementItems } from 'app/shared/model/program-requirement-items.model';

export interface IProgramLevel {
  id?: number;
  description?: string;
  startPoint?: number;
  endPoint?: number;
  levelOrder?: number;
  iconPath?: string;
  name?: string;
  endDate?: Moment;
  programId?: number;
  programRequirementItems?: IProgramRequirementItems[];
}

export const defaultValue: Readonly<IProgramLevel> = {};
