export interface IProgramLevelPractice {
  id?: number;
  practiceId?: string;
  name?: string;
  subgroupId?: string;
  subgroupName?: string;
  programLevelName?: string;
  programLevelId?: number;
}

export const defaultValue: Readonly<IProgramLevelPractice> = {};
