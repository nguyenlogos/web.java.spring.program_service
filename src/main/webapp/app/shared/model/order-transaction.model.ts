import { Moment } from 'moment';

export interface IOrderTransaction {
  id?: number;
  programName?: string;
  clientName?: string;
  participantName?: string;
  createdDate?: Moment;
  message?: string;
  jsonContent?: string;
  email?: string;
  externalOrderId?: string;
  programId?: number;
  userRewardId?: number;
}

export const defaultValue: Readonly<IOrderTransaction> = {};
