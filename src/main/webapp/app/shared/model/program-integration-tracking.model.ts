import { Moment } from 'moment';

export interface IProgramIntegrationTracking {
  id?: number;
  endPoint?: string;
  payload?: string;
  createdAt?: Moment;
  errorMessage?: string;
  status?: string;
  serviceType?: string;
  attemptedAt?: Moment;
}

export const defaultValue: Readonly<IProgramIntegrationTracking> = {};
