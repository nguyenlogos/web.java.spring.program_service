import { Moment } from 'moment';

export interface IProgramUserLevelProgress {
  id?: number;
  userEventId?: number;
  programUserId?: number;
  createdAt?: Moment;
  levelUpAt?: Moment;
  levelFrom?: number;
  levelTo?: number;
  hasReward?: boolean;
}

export const defaultValue: Readonly<IProgramUserLevelProgress> = {
  hasReward: false
};
