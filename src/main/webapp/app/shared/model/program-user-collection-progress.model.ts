import { Moment } from 'moment';

export const enum CollectionStatus {
  DOING = 'DOING',
  DONE = 'DONE'
}

export interface IProgramUserCollectionProgress {
  id?: number;
  collectionId?: number;
  totalRequiredItems?: number;
  currentProgress?: number;
  status?: CollectionStatus;
  createdDate?: Moment;
  modifiedDate?: Moment;
  programUserCohortId?: number;
}

export const defaultValue: Readonly<IProgramUserCollectionProgress> = {};
