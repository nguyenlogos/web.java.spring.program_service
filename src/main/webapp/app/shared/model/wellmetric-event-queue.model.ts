import { Moment } from 'moment';

export const enum ResultStatus {
  PASS = 'PASS',
  FAIL = 'FAIL',
  UNAVAILABLE = 'UNAVAILABLE'
}

export const enum Gender {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  UNIDENTIFIED = 'UNIDENTIFIED',
  UNDEFINED = 'UNDEFINED'
}

export interface IWellmetricEventQueue {
  id?: number;
  participantId?: string;
  clientId?: string;
  email?: string;
  errorMessage?: string;
  result?: ResultStatus;
  eventCode?: string;
  gender?: Gender;
  attemptCount?: number;
  lastAttemptTime?: Moment;
  createdAt?: Moment;
  subgroupId?: string;
  healthyValue?: number;
  executeStatus?: string;
  eventId?: string;
  hasIncentive?: boolean;
  isWaitingPush?: boolean;
  attemptedAt?: Moment;
  programId?: number;
  eventDate?: string;
  subCategoryCode?: string;
}

export const defaultValue: Readonly<IWellmetricEventQueue> = {
  hasIncentive: false,
  isWaitingPush: false
};
