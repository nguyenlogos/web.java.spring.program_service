export interface IProgramSubCategoryPoint {
  id?: number;
  code?: string;
  percentPoint?: number;
  valuePoint?: number;
  completionsCap?: number;
  programCategoryPointCategoryCode?: string;
  programCategoryPointId?: number;
}

export const defaultValue: Readonly<IProgramSubCategoryPoint> = {};
