import { Moment } from 'moment';

export interface IProgramTrackingStatus {
  id?: number;
  programId?: number;
  programStatus?: string;
  createdDate?: Moment;
  modifiedDate?: Moment;
}

export const defaultValue: Readonly<IProgramTrackingStatus> = {};
