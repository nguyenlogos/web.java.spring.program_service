export interface IProgramHealthyRange {
  id?: number;
  maleMin?: number;
  maleMax?: number;
  femaleMin?: number;
  femaleMax?: number;
  unidentifiedMin?: number;
  unidentifiedMax?: number;
  isApplyPoint?: boolean;
  biometricCode?: string;
  subCategoryCode?: string;
  subCategoryId?: number;
  programId?: number;
}

export const defaultValue: Readonly<IProgramHealthyRange> = {
  isApplyPoint: false
};
