import React from 'react';
import MenuItem from 'app/shared/layout/menus/menu-item';
import { DropdownItem } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Translate, translate } from 'react-jhipster';
import { NavLink as Link } from 'react-router-dom';
import { NavDropdown } from './menu-components';

export const EntitiesMenu = props => (
  // tslint:disable-next-line:jsx-self-close
  <NavDropdown icon="th-list" name={translate('global.menu.entities.main')} id="entity-menu">
    <MenuItem icon="asterisk" to="/entity/category">
      <Translate contentKey="global.menu.entities.category" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program">
      <Translate contentKey="global.menu.entities.program" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-category-point">
      <Translate contentKey="global.menu.entities.programCategoryPoint" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/client-program">
      <Translate contentKey="global.menu.entities.clientProgram" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-level">
      <Translate contentKey="global.menu.entities.programLevel" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-level-activity">
      <Translate contentKey="global.menu.entities.programLevelActivity" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/reward">
      <Translate contentKey="global.menu.entities.reward" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-activity">
      <Translate contentKey="global.menu.entities.programActivity" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-level-reward">
      <Translate contentKey="global.menu.entities.programLevelReward" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/sub-category">
      <Translate contentKey="global.menu.entities.subCategory" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-sub-category-point">
      <Translate contentKey="global.menu.entities.programSubCategoryPoint" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-user">
      <Translate contentKey="global.menu.entities.programUser" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/user-event">
      <Translate contentKey="global.menu.entities.userEvent" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/progam-level-user">
      <Translate contentKey="global.menu.entities.progamLevelUser" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/user-reward">
      <Translate contentKey="global.menu.entities.userReward" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/order-transaction">
      <Translate contentKey="global.menu.entities.orderTransaction" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/client-program-reward-setting">
      <Translate contentKey="global.menu.entities.clientProgramRewardSetting" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-phase">
      <Translate contentKey="global.menu.entities.programPhase" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/client-setting">
      <Translate contentKey="global.menu.entities.clientSetting" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-subgroup">
      <Translate contentKey="global.menu.entities.programSubgroup" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/activity-event-queue">
      <Translate contentKey="global.menu.entities.activityEventQueue" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/term-and-condition">
      <Translate contentKey="global.menu.entities.termAndCondition" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-level-path">
      <Translate contentKey="global.menu.entities.programLevelPath" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-level-practice">
      <Translate contentKey="global.menu.entities.programLevelPractice" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/client-brand-setting">
      <Translate contentKey="global.menu.entities.clientBrandSetting" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-biometric-data">
      <Translate contentKey="global.menu.entities.programBiometricData" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-healthy-range">
      <Translate contentKey="global.menu.entities.programHealthyRange" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/wellmetric-event-queue">
      <Translate contentKey="global.menu.entities.wellmetricEventQueue" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/notification-center">
      <Translate contentKey="global.menu.entities.notificationCenter" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-sub-category-configuration">
      <Translate contentKey="global.menu.entities.programSubCategoryConfiguration" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/user-event-queue">
      <Translate contentKey="global.menu.entities.userEventQueue" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-requirement-items">
      <Translate contentKey="global.menu.entities.programRequirementItems" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/e-consent">
      <Translate contentKey="global.menu.entities.eConsent" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-integration-tracking">
      <Translate contentKey="global.menu.entities.programIntegrationTracking" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/client-sub-domain">
      <Translate contentKey="global.menu.entities.clientSubDomain" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/program-user-level-progress">
      <Translate contentKey="global.menu.entities.programUserLevelProgress" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-user-level-progress">
      <Translate contentKey="global.menu.entities.programUserLevelProgress" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-collection">
      <Translate contentKey="global.menu.entities.programCollection" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-collection-content">
      <Translate contentKey="global.menu.entities.programCollectionContent" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-tracking-status">
      <Translate contentKey="global.menu.entities.programTrackingStatus" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-cohort">
      <Translate contentKey="global.menu.entities.programCohort" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-cohort-data-input">
      <Translate contentKey="global.menu.entities.programCohortDataInput" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-cohort-rule">
      <Translate contentKey="global.menu.entities.programCohortRule" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-cohort-collection">
      <Translate contentKey="global.menu.entities.programCohortCollection" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-user-cohort">
      <Translate contentKey="global.menu.entities.programUserCohort" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-user-logs">
      <Translate contentKey="global.menu.entities.programUserLogs" />
    </MenuItem>
    <MenuItem icon="asterisk" to="/entity/program-user-collection-progress">
      <Translate contentKey="global.menu.entities.programUserCollectionProgress" />
    </MenuItem>
    {/* jhipster-needle-add-entity-to-menu - JHipster will add entities to the menu here */}
  </NavDropdown>
);
