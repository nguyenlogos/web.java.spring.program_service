import { combineReducers } from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';

import locale, { LocaleState } from './locale';
import authentication, { AuthenticationState } from './authentication';
import applicationProfile, { ApplicationProfileState } from './application-profile';

import administration, { AdministrationState } from 'app/modules/administration/administration.reducer';
import userManagement, { UserManagementState } from 'app/modules/administration/user-management/user-management.reducer';
import register, { RegisterState } from 'app/modules/account/register/register.reducer';
import activate, { ActivateState } from 'app/modules/account/activate/activate.reducer';
import password, { PasswordState } from 'app/modules/account/password/password.reducer';
import settings, { SettingsState } from 'app/modules/account/settings/settings.reducer';
import passwordReset, { PasswordResetState } from 'app/modules/account/password-reset/password-reset.reducer';
// prettier-ignore
import category, {
  CategoryState
} from 'app/entities/category/category.reducer';
// prettier-ignore
import program, {
  ProgramState
} from 'app/entities/program/program.reducer';
// prettier-ignore

// prettier-ignore
import programCategoryPoint, {
  ProgramCategoryPointState
} from 'app/entities/program-category-point/program-category-point.reducer';
// prettier-ignore
import clientProgram, {
  ClientProgramState
} from 'app/entities/client-program/client-program.reducer';
// prettier-ignore
import programLevel, {
  ProgramLevelState
} from 'app/entities/program-level/program-level.reducer';
// prettier-ignore
import programLevelActivity, {
  ProgramLevelActivityState
} from 'app/entities/program-level-activity/program-level-activity.reducer';
// prettier-ignore
import reward, {
  RewardState
} from 'app/entities/reward/reward.reducer';
// prettier-ignore
import programActivity, {
  ProgramActivityState
} from 'app/entities/program-activity/program-activity.reducer';
// prettier-ignore
import programLevelReward, {
  ProgramLevelRewardState
} from 'app/entities/program-level-reward/program-level-reward.reducer';
// prettier-ignore
import subCategory, {
  SubCategoryState
} from 'app/entities/sub-category/sub-category.reducer';
// prettier-ignore
import programSubCategoryPoint, {
  ProgramSubCategoryPointState
} from 'app/entities/program-sub-category-point/program-sub-category-point.reducer';
// prettier-ignore
import programUser, {
  ProgramUserState
} from 'app/entities/program-user/program-user.reducer';
// prettier-ignore
import userEvent, {
  UserEventState
} from 'app/entities/user-event/user-event.reducer';
// prettier-ignore
import progamLevelUser, {
  ProgamLevelUserState
} from 'app/entities/progam-level-user/progam-level-user.reducer';
// prettier-ignore
import userReward, {
  UserRewardState
} from 'app/entities/user-reward/user-reward.reducer';
// prettier-ignore
import orderTransaction, {
  OrderTransactionState
} from 'app/entities/order-transaction/order-transaction.reducer';
// prettier-ignore
import clientProgramRewardSetting, {
  ClientProgramRewardSettingState
} from 'app/entities/client-program-reward-setting/client-program-reward-setting.reducer';
// prettier-ignore
import programPhase, {
  ProgramPhaseState
} from 'app/entities/program-phase/program-phase.reducer';
// prettier-ignore
import clientSetting, {
  ClientSettingState
} from 'app/entities/client-setting/client-setting.reducer';
// prettier-ignore
import programSubgroup, {
  ProgramSubgroupState
} from 'app/entities/program-subgroup/program-subgroup.reducer';
// prettier-ignore
import activityEventQueue, {
  ActivityEventQueueState
} from 'app/entities/activity-event-queue/activity-event-queue.reducer';
// prettier-ignore
import termAndCondition, {
  TermAndConditionState
} from 'app/entities/term-and-condition/term-and-condition.reducer';
// prettier-ignore
import programLevelPath, {
  ProgramLevelPathState
} from 'app/entities/program-level-path/program-level-path.reducer';
// prettier-ignore
import programLevelPractice, {
  ProgramLevelPracticeState
} from 'app/entities/program-level-practice/program-level-practice.reducer';
// prettier-ignore
import clientBrandSetting, {
  ClientBrandSettingState
} from 'app/entities/client-brand-setting/client-brand-setting.reducer';
// prettier-ignore
import programBiometricData, {
  ProgramBiometricDataState
} from 'app/entities/program-biometric-data/program-biometric-data.reducer';
// prettier-ignore
import programHealthyRange, {
  ProgramHealthyRangeState
} from 'app/entities/program-healthy-range/program-healthy-range.reducer';
// prettier-ignore
import wellmetricEventQueue, {
  WellmetricEventQueueState
} from 'app/entities/wellmetric-event-queue/wellmetric-event-queue.reducer';
// prettier-ignore
import notificationCenter, {
  NotificationCenterState
} from 'app/entities/notification-center/notification-center.reducer';
// prettier-ignore
import programSubCategoryConfiguration, {
  ProgramSubCategoryConfigurationState
} from 'app/entities/program-sub-category-configuration/program-sub-category-configuration.reducer';
// prettier-ignore
import userEventQueue, {
  UserEventQueueState
} from 'app/entities/user-event-queue/user-event-queue.reducer';
// prettier-ignore
import programRequirementItems, {
  ProgramRequirementItemsState
} from 'app/entities/program-requirement-items/program-requirement-items.reducer';
// prettier-ignore
import eConsent, {
  EConsentState
} from 'app/entities/e-consent/e-consent.reducer';
// prettier-ignore
import programIntegrationTracking, {
  ProgramIntegrationTrackingState
} from 'app/entities/program-integration-tracking/program-integration-tracking.reducer';
// prettier-ignore
import clientSubDomain, {
  ClientSubDomainState
} from 'app/entities/client-sub-domain/client-sub-domain.reducer';
// prettier-ignore
import programUserLevelProgress, {
  ProgramUserLevelProgressState
} from 'app/entities/program-user-level-progress/program-user-level-progress.reducer';
// prettier-ignore
import programCollection, {
  ProgramCollectionState
} from 'app/entities/program-collection/program-collection.reducer';
// prettier-ignore
import programCollectionContent, {
  ProgramCollectionContentState
} from 'app/entities/program-collection-content/program-collection-content.reducer';
// prettier-ignore
import programTrackingStatus, {
  ProgramTrackingStatusState
} from 'app/entities/program-tracking-status/program-tracking-status.reducer';
// prettier-ignore
import programCohort, {
  ProgramCohortState
} from 'app/entities/program-cohort/program-cohort.reducer';
// prettier-ignore
import programCohortDataInput, {
  ProgramCohortDataInputState
} from 'app/entities/program-cohort-data-input/program-cohort-data-input.reducer';
// prettier-ignore
import programCohortRule, {
  ProgramCohortRuleState
} from 'app/entities/program-cohort-rule/program-cohort-rule.reducer';
// prettier-ignore
import programCohortCollection, {
  ProgramCohortCollectionState
} from 'app/entities/program-cohort-collection/program-cohort-collection.reducer';
// prettier-ignore
import programUserCohort, {
  ProgramUserCohortState
} from 'app/entities/program-user-cohort/program-user-cohort.reducer';
// prettier-ignore
import programUserLogs, {
  ProgramUserLogsState
} from 'app/entities/program-user-logs/program-user-logs.reducer';
// prettier-ignore
import programUserCollectionProgress, {
  ProgramUserCollectionProgressState
} from 'app/entities/program-user-collection-progress/program-user-collection-progress.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

export interface IRootState {
  readonly authentication: AuthenticationState;
  readonly locale: LocaleState;
  readonly applicationProfile: ApplicationProfileState;
  readonly administration: AdministrationState;
  readonly userManagement: UserManagementState;
  readonly register: RegisterState;
  readonly activate: ActivateState;
  readonly passwordReset: PasswordResetState;
  readonly password: PasswordState;
  readonly settings: SettingsState;
  readonly category: CategoryState;
  readonly program: ProgramState;
  readonly programCategoryPoint: ProgramCategoryPointState;
  readonly clientProgram: ClientProgramState;
  readonly programLevel: ProgramLevelState;
  readonly programLevelActivity: ProgramLevelActivityState;
  readonly reward: RewardState;
  readonly programActivity: ProgramActivityState;
  readonly programLevelReward: ProgramLevelRewardState;
  readonly subCategory: SubCategoryState;
  readonly programSubCategoryPoint: ProgramSubCategoryPointState;
  readonly programUser: ProgramUserState;
  readonly userEvent: UserEventState;
  readonly progamLevelUser: ProgamLevelUserState;
  readonly userReward: UserRewardState;
  readonly orderTransaction: OrderTransactionState;
  readonly clientProgramRewardSetting: ClientProgramRewardSettingState;
  readonly programPhase: ProgramPhaseState;
  readonly clientSetting: ClientSettingState;
  readonly programSubgroup: ProgramSubgroupState;
  readonly activityEventQueue: ActivityEventQueueState;
  readonly termAndCondition: TermAndConditionState;
  readonly programLevelPath: ProgramLevelPathState;
  readonly programLevelPractice: ProgramLevelPracticeState;
  readonly clientBrandSetting: ClientBrandSettingState;
  readonly programBiometricData: ProgramBiometricDataState;
  readonly programHealthyRange: ProgramHealthyRangeState;
  readonly wellmetricEventQueue: WellmetricEventQueueState;
  readonly notificationCenter: NotificationCenterState;
  readonly programSubCategoryConfiguration: ProgramSubCategoryConfigurationState;
  readonly userEventQueue: UserEventQueueState;
  readonly programRequirementItems: ProgramRequirementItemsState;
  readonly eConsent: EConsentState;
  readonly programIntegrationTracking: ProgramIntegrationTrackingState;
  readonly clientSubDomain: ClientSubDomainState;
  readonly programUserLevelProgress: ProgramUserLevelProgressState;
  readonly programCollection: ProgramCollectionState;
  readonly programCollectionContent: ProgramCollectionContentState;
  readonly programTrackingStatus: ProgramTrackingStatusState;
  readonly programCohort: ProgramCohortState;
  readonly programCohortDataInput: ProgramCohortDataInputState;
  readonly programCohortRule: ProgramCohortRuleState;
  readonly programCohortCollection: ProgramCohortCollectionState;
  readonly programUserCohort: ProgramUserCohortState;
  readonly programUserLogs: ProgramUserLogsState;
  readonly programUserCollectionProgress: ProgramUserCollectionProgressState;
  /* jhipster-needle-add-reducer-type - JHipster will add reducer type here */
  readonly loadingBar: any;
}

const rootReducer = combineReducers<IRootState>({
  authentication,
  locale,
  applicationProfile,
  administration,
  userManagement,
  register,
  activate,
  passwordReset,
  password,
  settings,
  category,
  program,
  programCategoryPoint,
  clientProgram,
  programLevel,
  programLevelActivity,
  reward,
  programActivity,
  programLevelReward,
  subCategory,
  programSubCategoryPoint,
  programUser,
  userEvent,
  progamLevelUser,
  userReward,
  orderTransaction,
  clientProgramRewardSetting,
  programPhase,
  clientSetting,
  programSubgroup,
  activityEventQueue,
  termAndCondition,
  programLevelPath,
  programLevelPractice,
  clientBrandSetting,
  programBiometricData,
  programHealthyRange,
  wellmetricEventQueue,
  notificationCenter,
  programSubCategoryConfiguration,
  userEventQueue,
  programRequirementItems,
  eConsent,
  programIntegrationTracking,
  clientSubDomain,
  programUserLevelProgress,
  programCollection,
  programCollectionContent,
  programTrackingStatus,
  programCohort,
  programCohortDataInput,
  programCohortRule,
  programCohortCollection,
  programUserCohort,
  programUserLogs,
  programUserCollectionProgress,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
  loadingBar
});

export default rootReducer;
