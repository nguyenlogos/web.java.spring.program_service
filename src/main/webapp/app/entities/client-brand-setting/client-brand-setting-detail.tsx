import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './client-brand-setting.reducer';
import { IClientBrandSetting } from 'app/shared/model/client-brand-setting.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IClientBrandSettingDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ClientBrandSettingDetail extends React.Component<IClientBrandSettingDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { clientBrandSettingEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.clientBrandSetting.detail.title">ClientBrandSetting</Translate> [
            <b>{clientBrandSettingEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.clientBrandSetting.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{clientBrandSettingEntity.clientId}</dd>
            <dt>
              <span id="subPathUrl">
                <Translate contentKey="programserviceApp.clientBrandSetting.subPathUrl">Sub Path Url</Translate>
              </span>
            </dt>
            <dd>{clientBrandSettingEntity.subPathUrl}</dd>
            <dt>
              <span id="webLogoUrl">
                <Translate contentKey="programserviceApp.clientBrandSetting.webLogoUrl">Web Logo Url</Translate>
              </span>
            </dt>
            <dd>{clientBrandSettingEntity.webLogoUrl}</dd>
            <dt>
              <span id="primaryColorValue">
                <Translate contentKey="programserviceApp.clientBrandSetting.primaryColorValue">Primary Color Value</Translate>
              </span>
            </dt>
            <dd>{clientBrandSettingEntity.primaryColorValue}</dd>
            <dt>
              <span id="clientName">
                <Translate contentKey="programserviceApp.clientBrandSetting.clientName">Client Name</Translate>
              </span>
            </dt>
            <dd>{clientBrandSettingEntity.clientName}</dd>
            <dt>
              <span id="clientUrl">
                <Translate contentKey="programserviceApp.clientBrandSetting.clientUrl">Client Url</Translate>
              </span>
            </dt>
            <dd>{clientBrandSettingEntity.clientUrl}</dd>
          </dl>
          <Button tag={Link} to="/entity/client-brand-setting" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/client-brand-setting/${clientBrandSettingEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ clientBrandSetting }: IRootState) => ({
  clientBrandSettingEntity: clientBrandSetting.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientBrandSettingDetail);
