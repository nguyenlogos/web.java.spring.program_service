import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IClientBrandSetting, defaultValue } from 'app/shared/model/client-brand-setting.model';

export const ACTION_TYPES = {
  FETCH_CLIENTBRANDSETTING_LIST: 'clientBrandSetting/FETCH_CLIENTBRANDSETTING_LIST',
  FETCH_CLIENTBRANDSETTING: 'clientBrandSetting/FETCH_CLIENTBRANDSETTING',
  CREATE_CLIENTBRANDSETTING: 'clientBrandSetting/CREATE_CLIENTBRANDSETTING',
  UPDATE_CLIENTBRANDSETTING: 'clientBrandSetting/UPDATE_CLIENTBRANDSETTING',
  DELETE_CLIENTBRANDSETTING: 'clientBrandSetting/DELETE_CLIENTBRANDSETTING',
  RESET: 'clientBrandSetting/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IClientBrandSetting>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ClientBrandSettingState = Readonly<typeof initialState>;

// Reducer

export default (state: ClientBrandSettingState = initialState, action): ClientBrandSettingState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CLIENTBRANDSETTING_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CLIENTBRANDSETTING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CLIENTBRANDSETTING):
    case REQUEST(ACTION_TYPES.UPDATE_CLIENTBRANDSETTING):
    case REQUEST(ACTION_TYPES.DELETE_CLIENTBRANDSETTING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CLIENTBRANDSETTING_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CLIENTBRANDSETTING):
    case FAILURE(ACTION_TYPES.CREATE_CLIENTBRANDSETTING):
    case FAILURE(ACTION_TYPES.UPDATE_CLIENTBRANDSETTING):
    case FAILURE(ACTION_TYPES.DELETE_CLIENTBRANDSETTING):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLIENTBRANDSETTING_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLIENTBRANDSETTING):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CLIENTBRANDSETTING):
    case SUCCESS(ACTION_TYPES.UPDATE_CLIENTBRANDSETTING):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CLIENTBRANDSETTING):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/client-brand-settings';

// Actions

export const getEntities: ICrudGetAllAction<IClientBrandSetting> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CLIENTBRANDSETTING_LIST,
    payload: axios.get<IClientBrandSetting>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IClientBrandSetting> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CLIENTBRANDSETTING,
    payload: axios.get<IClientBrandSetting>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IClientBrandSetting> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CLIENTBRANDSETTING,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IClientBrandSetting> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CLIENTBRANDSETTING,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IClientBrandSetting> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CLIENTBRANDSETTING,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
