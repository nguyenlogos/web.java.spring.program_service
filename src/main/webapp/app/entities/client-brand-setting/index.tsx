import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ClientBrandSetting from './client-brand-setting';
import ClientBrandSettingDetail from './client-brand-setting-detail';
import ClientBrandSettingUpdate from './client-brand-setting-update';
import ClientBrandSettingDeleteDialog from './client-brand-setting-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ClientBrandSettingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ClientBrandSettingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ClientBrandSettingDetail} />
      <ErrorBoundaryRoute path={match.url} component={ClientBrandSetting} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ClientBrandSettingDeleteDialog} />
  </>
);

export default Routes;
