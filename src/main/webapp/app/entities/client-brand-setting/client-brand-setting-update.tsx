import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './client-brand-setting.reducer';
import { IClientBrandSetting } from 'app/shared/model/client-brand-setting.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IClientBrandSettingUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IClientBrandSettingUpdateState {
  isNew: boolean;
}

export class ClientBrandSettingUpdate extends React.Component<IClientBrandSettingUpdateProps, IClientBrandSettingUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { clientBrandSettingEntity } = this.props;
      const entity = {
        ...clientBrandSettingEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/client-brand-setting');
  };

  render() {
    const { clientBrandSettingEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.clientBrandSetting.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.clientBrandSetting.home.createOrEditLabel">
                Create or edit a ClientBrandSetting
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : clientBrandSettingEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="client-brand-setting-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="client-brand-setting-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="clientIdLabel" for="client-brand-setting-clientId">
                    <Translate contentKey="programserviceApp.clientBrandSetting.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="client-brand-setting-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subPathUrlLabel" for="client-brand-setting-subPathUrl">
                    <Translate contentKey="programserviceApp.clientBrandSetting.subPathUrl">Sub Path Url</Translate>
                  </Label>
                  <AvField
                    id="client-brand-setting-subPathUrl"
                    type="text"
                    name="subPathUrl"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="webLogoUrlLabel" for="client-brand-setting-webLogoUrl">
                    <Translate contentKey="programserviceApp.clientBrandSetting.webLogoUrl">Web Logo Url</Translate>
                  </Label>
                  <AvField id="client-brand-setting-webLogoUrl" type="text" name="webLogoUrl" />
                </AvGroup>
                <AvGroup>
                  <Label id="primaryColorValueLabel" for="client-brand-setting-primaryColorValue">
                    <Translate contentKey="programserviceApp.clientBrandSetting.primaryColorValue">Primary Color Value</Translate>
                  </Label>
                  <AvField id="client-brand-setting-primaryColorValue" type="text" name="primaryColorValue" />
                </AvGroup>
                <AvGroup>
                  <Label id="clientNameLabel" for="client-brand-setting-clientName">
                    <Translate contentKey="programserviceApp.clientBrandSetting.clientName">Client Name</Translate>
                  </Label>
                  <AvField id="client-brand-setting-clientName" type="text" name="clientName" />
                </AvGroup>
                <AvGroup>
                  <Label id="clientUrlLabel" for="client-brand-setting-clientUrl">
                    <Translate contentKey="programserviceApp.clientBrandSetting.clientUrl">Client Url</Translate>
                  </Label>
                  <AvField id="client-brand-setting-clientUrl" type="text" name="clientUrl" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/client-brand-setting" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  clientBrandSettingEntity: storeState.clientBrandSetting.entity,
  loading: storeState.clientBrandSetting.loading,
  updating: storeState.clientBrandSetting.updating,
  updateSuccess: storeState.clientBrandSetting.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientBrandSettingUpdate);
