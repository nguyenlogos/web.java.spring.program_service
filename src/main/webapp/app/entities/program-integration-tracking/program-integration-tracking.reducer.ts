import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramIntegrationTracking, defaultValue } from 'app/shared/model/program-integration-tracking.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMINTEGRATIONTRACKING_LIST: 'programIntegrationTracking/FETCH_PROGRAMINTEGRATIONTRACKING_LIST',
  FETCH_PROGRAMINTEGRATIONTRACKING: 'programIntegrationTracking/FETCH_PROGRAMINTEGRATIONTRACKING',
  CREATE_PROGRAMINTEGRATIONTRACKING: 'programIntegrationTracking/CREATE_PROGRAMINTEGRATIONTRACKING',
  UPDATE_PROGRAMINTEGRATIONTRACKING: 'programIntegrationTracking/UPDATE_PROGRAMINTEGRATIONTRACKING',
  DELETE_PROGRAMINTEGRATIONTRACKING: 'programIntegrationTracking/DELETE_PROGRAMINTEGRATIONTRACKING',
  RESET: 'programIntegrationTracking/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramIntegrationTracking>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramIntegrationTrackingState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramIntegrationTrackingState = initialState, action): ProgramIntegrationTrackingState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMINTEGRATIONTRACKING_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMINTEGRATIONTRACKING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMINTEGRATIONTRACKING):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMINTEGRATIONTRACKING):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMINTEGRATIONTRACKING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMINTEGRATIONTRACKING_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMINTEGRATIONTRACKING):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMINTEGRATIONTRACKING):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMINTEGRATIONTRACKING):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMINTEGRATIONTRACKING):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMINTEGRATIONTRACKING_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMINTEGRATIONTRACKING):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMINTEGRATIONTRACKING):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMINTEGRATIONTRACKING):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMINTEGRATIONTRACKING):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-integration-trackings';

// Actions

export const getEntities: ICrudGetAllAction<IProgramIntegrationTracking> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMINTEGRATIONTRACKING_LIST,
    payload: axios.get<IProgramIntegrationTracking>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramIntegrationTracking> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMINTEGRATIONTRACKING,
    payload: axios.get<IProgramIntegrationTracking>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramIntegrationTracking> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMINTEGRATIONTRACKING,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramIntegrationTracking> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMINTEGRATIONTRACKING,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramIntegrationTracking> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMINTEGRATIONTRACKING,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
