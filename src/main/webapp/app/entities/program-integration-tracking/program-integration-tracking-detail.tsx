import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-integration-tracking.reducer';
import { IProgramIntegrationTracking } from 'app/shared/model/program-integration-tracking.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramIntegrationTrackingDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramIntegrationTrackingDetail extends React.Component<IProgramIntegrationTrackingDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programIntegrationTrackingEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programIntegrationTracking.detail.title">ProgramIntegrationTracking</Translate> [
            <b>{programIntegrationTrackingEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="endPoint">
                <Translate contentKey="programserviceApp.programIntegrationTracking.endPoint">End Point</Translate>
              </span>
            </dt>
            <dd>{programIntegrationTrackingEntity.endPoint}</dd>
            <dt>
              <span id="payload">
                <Translate contentKey="programserviceApp.programIntegrationTracking.payload">Payload</Translate>
              </span>
            </dt>
            <dd>{programIntegrationTrackingEntity.payload}</dd>
            <dt>
              <span id="createdAt">
                <Translate contentKey="programserviceApp.programIntegrationTracking.createdAt">Created At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programIntegrationTrackingEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="errorMessage">
                <Translate contentKey="programserviceApp.programIntegrationTracking.errorMessage">Error Message</Translate>
              </span>
            </dt>
            <dd>{programIntegrationTrackingEntity.errorMessage}</dd>
            <dt>
              <span id="status">
                <Translate contentKey="programserviceApp.programIntegrationTracking.status">Status</Translate>
              </span>
            </dt>
            <dd>{programIntegrationTrackingEntity.status}</dd>
            <dt>
              <span id="serviceType">
                <Translate contentKey="programserviceApp.programIntegrationTracking.serviceType">Service Type</Translate>
              </span>
            </dt>
            <dd>{programIntegrationTrackingEntity.serviceType}</dd>
            <dt>
              <span id="attemptedAt">
                <Translate contentKey="programserviceApp.programIntegrationTracking.attemptedAt">Attempted At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programIntegrationTrackingEntity.attemptedAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/program-integration-tracking" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button
            tag={Link}
            to={`/entity/program-integration-tracking/${programIntegrationTrackingEntity.id}/edit`}
            replace
            color="primary"
          >
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programIntegrationTracking }: IRootState) => ({
  programIntegrationTrackingEntity: programIntegrationTracking.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramIntegrationTrackingDetail);
