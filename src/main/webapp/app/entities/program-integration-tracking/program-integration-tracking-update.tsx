import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './program-integration-tracking.reducer';
import { IProgramIntegrationTracking } from 'app/shared/model/program-integration-tracking.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramIntegrationTrackingUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramIntegrationTrackingUpdateState {
  isNew: boolean;
}

export class ProgramIntegrationTrackingUpdate extends React.Component<
  IProgramIntegrationTrackingUpdateProps,
  IProgramIntegrationTrackingUpdateState
> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.attemptedAt = convertDateTimeToServer(values.attemptedAt);

    if (errors.length === 0) {
      const { programIntegrationTrackingEntity } = this.props;
      const entity = {
        ...programIntegrationTrackingEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-integration-tracking');
  };

  render() {
    const { programIntegrationTrackingEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programIntegrationTracking.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programIntegrationTracking.home.createOrEditLabel">
                Create or edit a ProgramIntegrationTracking
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programIntegrationTrackingEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-integration-tracking-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-integration-tracking-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="endPointLabel" for="program-integration-tracking-endPoint">
                    <Translate contentKey="programserviceApp.programIntegrationTracking.endPoint">End Point</Translate>
                  </Label>
                  <AvField id="program-integration-tracking-endPoint" type="text" name="endPoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="payloadLabel" for="program-integration-tracking-payload">
                    <Translate contentKey="programserviceApp.programIntegrationTracking.payload">Payload</Translate>
                  </Label>
                  <AvField id="program-integration-tracking-payload" type="text" name="payload" />
                </AvGroup>
                <AvGroup>
                  <Label id="createdAtLabel" for="program-integration-tracking-createdAt">
                    <Translate contentKey="programserviceApp.programIntegrationTracking.createdAt">Created At</Translate>
                  </Label>
                  <AvInput
                    id="program-integration-tracking-createdAt"
                    type="datetime-local"
                    className="form-control"
                    name="createdAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programIntegrationTrackingEntity.createdAt)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="errorMessageLabel" for="program-integration-tracking-errorMessage">
                    <Translate contentKey="programserviceApp.programIntegrationTracking.errorMessage">Error Message</Translate>
                  </Label>
                  <AvField id="program-integration-tracking-errorMessage" type="text" name="errorMessage" />
                </AvGroup>
                <AvGroup>
                  <Label id="statusLabel" for="program-integration-tracking-status">
                    <Translate contentKey="programserviceApp.programIntegrationTracking.status">Status</Translate>
                  </Label>
                  <AvField id="program-integration-tracking-status" type="text" name="status" />
                </AvGroup>
                <AvGroup>
                  <Label id="serviceTypeLabel" for="program-integration-tracking-serviceType">
                    <Translate contentKey="programserviceApp.programIntegrationTracking.serviceType">Service Type</Translate>
                  </Label>
                  <AvField id="program-integration-tracking-serviceType" type="text" name="serviceType" />
                </AvGroup>
                <AvGroup>
                  <Label id="attemptedAtLabel" for="program-integration-tracking-attemptedAt">
                    <Translate contentKey="programserviceApp.programIntegrationTracking.attemptedAt">Attempted At</Translate>
                  </Label>
                  <AvInput
                    id="program-integration-tracking-attemptedAt"
                    type="datetime-local"
                    className="form-control"
                    name="attemptedAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programIntegrationTrackingEntity.attemptedAt)}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-integration-tracking" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programIntegrationTrackingEntity: storeState.programIntegrationTracking.entity,
  loading: storeState.programIntegrationTracking.loading,
  updating: storeState.programIntegrationTracking.updating,
  updateSuccess: storeState.programIntegrationTracking.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramIntegrationTrackingUpdate);
