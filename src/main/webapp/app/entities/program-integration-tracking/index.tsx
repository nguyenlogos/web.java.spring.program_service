import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramIntegrationTracking from './program-integration-tracking';
import ProgramIntegrationTrackingDetail from './program-integration-tracking-detail';
import ProgramIntegrationTrackingUpdate from './program-integration-tracking-update';
import ProgramIntegrationTrackingDeleteDialog from './program-integration-tracking-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramIntegrationTrackingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramIntegrationTrackingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramIntegrationTrackingDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramIntegrationTracking} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramIntegrationTrackingDeleteDialog} />
  </>
);

export default Routes;
