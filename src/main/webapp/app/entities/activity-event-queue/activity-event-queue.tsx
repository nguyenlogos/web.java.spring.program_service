import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './activity-event-queue.reducer';
import { IActivityEventQueue } from 'app/shared/model/activity-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IActivityEventQueueProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IActivityEventQueueState = IPaginationBaseState;

export class ActivityEventQueue extends React.Component<IActivityEventQueueProps, IActivityEventQueueState> {
  state: IActivityEventQueueState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { activityEventQueueList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="activity-event-queue-heading">
          <Translate contentKey="programserviceApp.activityEventQueue.home.title">Activity Event Queues</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.activityEventQueue.home.createLabel">Create new Activity Event Queue</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('participantId')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.participantId">Participant Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientId')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.clientId">Client Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('activityCode')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.activityCode">Activity Code</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('firstName')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.firstName">First Name</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lastName')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.lastName">Last Name</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('email')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.email">Email</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('createdDate')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.createdDate">Created Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('transactionId')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.transactionId">Transaction Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('executeStatus')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.executeStatus">Execute Status</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('errorMessage')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.errorMessage">Error Message</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('subgroupId')}>
                  <Translate contentKey="programserviceApp.activityEventQueue.subgroupId">Subgroup Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {activityEventQueueList.map((activityEventQueue, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${activityEventQueue.id}`} color="link" size="sm">
                      {activityEventQueue.id}
                    </Button>
                  </td>
                  <td>{activityEventQueue.participantId}</td>
                  <td>{activityEventQueue.clientId}</td>
                  <td>{activityEventQueue.activityCode}</td>
                  <td>{activityEventQueue.firstName}</td>
                  <td>{activityEventQueue.lastName}</td>
                  <td>{activityEventQueue.email}</td>
                  <td>
                    <TextFormat type="date" value={activityEventQueue.createdDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{activityEventQueue.transactionId}</td>
                  <td>{activityEventQueue.executeStatus}</td>
                  <td>{activityEventQueue.errorMessage}</td>
                  <td>{activityEventQueue.subgroupId}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${activityEventQueue.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${activityEventQueue.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${activityEventQueue.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ activityEventQueue }: IRootState) => ({
  activityEventQueueList: activityEventQueue.entities,
  totalItems: activityEventQueue.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityEventQueue);
