import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ActivityEventQueue from './activity-event-queue';
import ActivityEventQueueDetail from './activity-event-queue-detail';
import ActivityEventQueueUpdate from './activity-event-queue-update';
import ActivityEventQueueDeleteDialog from './activity-event-queue-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ActivityEventQueueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ActivityEventQueueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ActivityEventQueueDetail} />
      <ErrorBoundaryRoute path={match.url} component={ActivityEventQueue} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ActivityEventQueueDeleteDialog} />
  </>
);

export default Routes;
