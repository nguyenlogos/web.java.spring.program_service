import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './activity-event-queue.reducer';
import { IActivityEventQueue } from 'app/shared/model/activity-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IActivityEventQueueUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IActivityEventQueueUpdateState {
  isNew: boolean;
}

export class ActivityEventQueueUpdate extends React.Component<IActivityEventQueueUpdateProps, IActivityEventQueueUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);

    if (errors.length === 0) {
      const { activityEventQueueEntity } = this.props;
      const entity = {
        ...activityEventQueueEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/activity-event-queue');
  };

  render() {
    const { activityEventQueueEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.activityEventQueue.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.activityEventQueue.home.createOrEditLabel">
                Create or edit a ActivityEventQueue
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : activityEventQueueEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="activity-event-queue-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="activity-event-queue-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="participantIdLabel" for="activity-event-queue-participantId">
                    <Translate contentKey="programserviceApp.activityEventQueue.participantId">Participant Id</Translate>
                  </Label>
                  <AvField
                    id="activity-event-queue-participantId"
                    type="text"
                    name="participantId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="clientIdLabel" for="activity-event-queue-clientId">
                    <Translate contentKey="programserviceApp.activityEventQueue.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="activity-event-queue-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="activityCodeLabel" for="activity-event-queue-activityCode">
                    <Translate contentKey="programserviceApp.activityEventQueue.activityCode">Activity Code</Translate>
                  </Label>
                  <AvField
                    id="activity-event-queue-activityCode"
                    type="text"
                    name="activityCode"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="firstNameLabel" for="activity-event-queue-firstName">
                    <Translate contentKey="programserviceApp.activityEventQueue.firstName">First Name</Translate>
                  </Label>
                  <AvField
                    id="activity-event-queue-firstName"
                    type="text"
                    name="firstName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="lastNameLabel" for="activity-event-queue-lastName">
                    <Translate contentKey="programserviceApp.activityEventQueue.lastName">Last Name</Translate>
                  </Label>
                  <AvField
                    id="activity-event-queue-lastName"
                    type="text"
                    name="lastName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="activity-event-queue-email">
                    <Translate contentKey="programserviceApp.activityEventQueue.email">Email</Translate>
                  </Label>
                  <AvField
                    id="activity-event-queue-email"
                    type="text"
                    name="email"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="activity-event-queue-createdDate">
                    <Translate contentKey="programserviceApp.activityEventQueue.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="activity-event-queue-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.activityEventQueueEntity.createdDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="transactionIdLabel" for="activity-event-queue-transactionId">
                    <Translate contentKey="programserviceApp.activityEventQueue.transactionId">Transaction Id</Translate>
                  </Label>
                  <AvField id="activity-event-queue-transactionId" type="text" name="transactionId" />
                </AvGroup>
                <AvGroup>
                  <Label id="executeStatusLabel" for="activity-event-queue-executeStatus">
                    <Translate contentKey="programserviceApp.activityEventQueue.executeStatus">Execute Status</Translate>
                  </Label>
                  <AvField id="activity-event-queue-executeStatus" type="text" name="executeStatus" />
                </AvGroup>
                <AvGroup>
                  <Label id="errorMessageLabel" for="activity-event-queue-errorMessage">
                    <Translate contentKey="programserviceApp.activityEventQueue.errorMessage">Error Message</Translate>
                  </Label>
                  <AvField id="activity-event-queue-errorMessage" type="text" name="errorMessage" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="activity-event-queue-subgroupId">
                    <Translate contentKey="programserviceApp.activityEventQueue.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="activity-event-queue-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/activity-event-queue" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  activityEventQueueEntity: storeState.activityEventQueue.entity,
  loading: storeState.activityEventQueue.loading,
  updating: storeState.activityEventQueue.updating,
  updateSuccess: storeState.activityEventQueue.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityEventQueueUpdate);
