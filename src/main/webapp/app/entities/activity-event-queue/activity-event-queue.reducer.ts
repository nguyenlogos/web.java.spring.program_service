import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IActivityEventQueue, defaultValue } from 'app/shared/model/activity-event-queue.model';

export const ACTION_TYPES = {
  FETCH_ACTIVITYEVENTQUEUE_LIST: 'activityEventQueue/FETCH_ACTIVITYEVENTQUEUE_LIST',
  FETCH_ACTIVITYEVENTQUEUE: 'activityEventQueue/FETCH_ACTIVITYEVENTQUEUE',
  CREATE_ACTIVITYEVENTQUEUE: 'activityEventQueue/CREATE_ACTIVITYEVENTQUEUE',
  UPDATE_ACTIVITYEVENTQUEUE: 'activityEventQueue/UPDATE_ACTIVITYEVENTQUEUE',
  DELETE_ACTIVITYEVENTQUEUE: 'activityEventQueue/DELETE_ACTIVITYEVENTQUEUE',
  RESET: 'activityEventQueue/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IActivityEventQueue>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ActivityEventQueueState = Readonly<typeof initialState>;

// Reducer

export default (state: ActivityEventQueueState = initialState, action): ActivityEventQueueState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ACTIVITYEVENTQUEUE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ACTIVITYEVENTQUEUE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ACTIVITYEVENTQUEUE):
    case REQUEST(ACTION_TYPES.UPDATE_ACTIVITYEVENTQUEUE):
    case REQUEST(ACTION_TYPES.DELETE_ACTIVITYEVENTQUEUE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ACTIVITYEVENTQUEUE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ACTIVITYEVENTQUEUE):
    case FAILURE(ACTION_TYPES.CREATE_ACTIVITYEVENTQUEUE):
    case FAILURE(ACTION_TYPES.UPDATE_ACTIVITYEVENTQUEUE):
    case FAILURE(ACTION_TYPES.DELETE_ACTIVITYEVENTQUEUE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ACTIVITYEVENTQUEUE_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_ACTIVITYEVENTQUEUE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ACTIVITYEVENTQUEUE):
    case SUCCESS(ACTION_TYPES.UPDATE_ACTIVITYEVENTQUEUE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ACTIVITYEVENTQUEUE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/activity-event-queues';

// Actions

export const getEntities: ICrudGetAllAction<IActivityEventQueue> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ACTIVITYEVENTQUEUE_LIST,
    payload: axios.get<IActivityEventQueue>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IActivityEventQueue> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ACTIVITYEVENTQUEUE,
    payload: axios.get<IActivityEventQueue>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IActivityEventQueue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ACTIVITYEVENTQUEUE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IActivityEventQueue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ACTIVITYEVENTQUEUE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IActivityEventQueue> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ACTIVITYEVENTQUEUE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
