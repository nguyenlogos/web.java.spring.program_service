import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './activity-event-queue.reducer';
import { IActivityEventQueue } from 'app/shared/model/activity-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IActivityEventQueueDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ActivityEventQueueDetail extends React.Component<IActivityEventQueueDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { activityEventQueueEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.activityEventQueue.detail.title">ActivityEventQueue</Translate> [
            <b>{activityEventQueueEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="participantId">
                <Translate contentKey="programserviceApp.activityEventQueue.participantId">Participant Id</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.participantId}</dd>
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.activityEventQueue.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.clientId}</dd>
            <dt>
              <span id="activityCode">
                <Translate contentKey="programserviceApp.activityEventQueue.activityCode">Activity Code</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.activityCode}</dd>
            <dt>
              <span id="firstName">
                <Translate contentKey="programserviceApp.activityEventQueue.firstName">First Name</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.firstName}</dd>
            <dt>
              <span id="lastName">
                <Translate contentKey="programserviceApp.activityEventQueue.lastName">Last Name</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.lastName}</dd>
            <dt>
              <span id="email">
                <Translate contentKey="programserviceApp.activityEventQueue.email">Email</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.email}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.activityEventQueue.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={activityEventQueueEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="transactionId">
                <Translate contentKey="programserviceApp.activityEventQueue.transactionId">Transaction Id</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.transactionId}</dd>
            <dt>
              <span id="executeStatus">
                <Translate contentKey="programserviceApp.activityEventQueue.executeStatus">Execute Status</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.executeStatus}</dd>
            <dt>
              <span id="errorMessage">
                <Translate contentKey="programserviceApp.activityEventQueue.errorMessage">Error Message</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.errorMessage}</dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.activityEventQueue.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{activityEventQueueEntity.subgroupId}</dd>
          </dl>
          <Button tag={Link} to="/entity/activity-event-queue" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/activity-event-queue/${activityEventQueueEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ activityEventQueue }: IRootState) => ({
  activityEventQueueEntity: activityEventQueue.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActivityEventQueueDetail);
