import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './user-event-queue.reducer';
import { IUserEventQueue } from 'app/shared/model/user-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IUserEventQueueProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IUserEventQueueState = IPaginationBaseState;

export class UserEventQueue extends React.Component<IUserEventQueueProps, IUserEventQueueState> {
  state: IUserEventQueueState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { userEventQueueList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="user-event-queue-heading">
          <Translate contentKey="programserviceApp.userEventQueue.home.title">User Event Queues</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.userEventQueue.home.createLabel">Create new User Event Queue</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventCode')}>
                  <Translate contentKey="programserviceApp.userEventQueue.eventCode">Event Code</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventId')}>
                  <Translate contentKey="programserviceApp.userEventQueue.eventId">Event Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventDate')}>
                  <Translate contentKey="programserviceApp.userEventQueue.eventDate">Event Date</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventPoint')}>
                  <Translate contentKey="programserviceApp.userEventQueue.eventPoint">Event Point</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventCategory')}>
                  <Translate contentKey="programserviceApp.userEventQueue.eventCategory">Event Category</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('programUserId')}>
                  <Translate contentKey="programserviceApp.userEventQueue.programUserId">Program User Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('executeStatus')}>
                  <Translate contentKey="programserviceApp.userEventQueue.executeStatus">Execute Status</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('programId')}>
                  <Translate contentKey="programserviceApp.userEventQueue.programId">Program Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('readAt')}>
                  <Translate contentKey="programserviceApp.userEventQueue.readAt">Read At</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('errorMessage')}>
                  <Translate contentKey="programserviceApp.userEventQueue.errorMessage">Error Message</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('currentStep')}>
                  <Translate contentKey="programserviceApp.userEventQueue.currentStep">Current Step</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {userEventQueueList.map((userEventQueue, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${userEventQueue.id}`} color="link" size="sm">
                      {userEventQueue.id}
                    </Button>
                  </td>
                  <td>{userEventQueue.eventCode}</td>
                  <td>{userEventQueue.eventId}</td>
                  <td>
                    <TextFormat type="date" value={userEventQueue.eventDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{userEventQueue.eventPoint}</td>
                  <td>{userEventQueue.eventCategory}</td>
                  <td>{userEventQueue.programUserId}</td>
                  <td>{userEventQueue.executeStatus}</td>
                  <td>{userEventQueue.programId}</td>
                  <td>
                    <TextFormat type="date" value={userEventQueue.readAt} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{userEventQueue.errorMessage}</td>
                  <td>{userEventQueue.currentStep}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${userEventQueue.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userEventQueue.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userEventQueue.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ userEventQueue }: IRootState) => ({
  userEventQueueList: userEventQueue.entities,
  totalItems: userEventQueue.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEventQueue);
