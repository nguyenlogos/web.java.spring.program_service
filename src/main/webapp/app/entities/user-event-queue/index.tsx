import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserEventQueue from './user-event-queue';
import UserEventQueueDetail from './user-event-queue-detail';
import UserEventQueueUpdate from './user-event-queue-update';
import UserEventQueueDeleteDialog from './user-event-queue-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UserEventQueueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UserEventQueueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UserEventQueueDetail} />
      <ErrorBoundaryRoute path={match.url} component={UserEventQueue} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={UserEventQueueDeleteDialog} />
  </>
);

export default Routes;
