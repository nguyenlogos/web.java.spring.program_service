import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './user-event-queue.reducer';
import { IUserEventQueue } from 'app/shared/model/user-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUserEventQueueUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IUserEventQueueUpdateState {
  isNew: boolean;
}

export class UserEventQueueUpdate extends React.Component<IUserEventQueueUpdateProps, IUserEventQueueUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.eventDate = convertDateTimeToServer(values.eventDate);
    values.readAt = convertDateTimeToServer(values.readAt);

    if (errors.length === 0) {
      const { userEventQueueEntity } = this.props;
      const entity = {
        ...userEventQueueEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/user-event-queue');
  };

  render() {
    const { userEventQueueEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.userEventQueue.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.userEventQueue.home.createOrEditLabel">Create or edit a UserEventQueue</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : userEventQueueEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="user-event-queue-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="user-event-queue-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="eventCodeLabel" for="user-event-queue-eventCode">
                    <Translate contentKey="programserviceApp.userEventQueue.eventCode">Event Code</Translate>
                  </Label>
                  <AvField
                    id="user-event-queue-eventCode"
                    type="text"
                    name="eventCode"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="eventIdLabel" for="user-event-queue-eventId">
                    <Translate contentKey="programserviceApp.userEventQueue.eventId">Event Id</Translate>
                  </Label>
                  <AvField
                    id="user-event-queue-eventId"
                    type="text"
                    name="eventId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="eventDateLabel" for="user-event-queue-eventDate">
                    <Translate contentKey="programserviceApp.userEventQueue.eventDate">Event Date</Translate>
                  </Label>
                  <AvInput
                    id="user-event-queue-eventDate"
                    type="datetime-local"
                    className="form-control"
                    name="eventDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.userEventQueueEntity.eventDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="eventPointLabel" for="user-event-queue-eventPoint">
                    <Translate contentKey="programserviceApp.userEventQueue.eventPoint">Event Point</Translate>
                  </Label>
                  <AvField id="user-event-queue-eventPoint" type="text" name="eventPoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="eventCategoryLabel" for="user-event-queue-eventCategory">
                    <Translate contentKey="programserviceApp.userEventQueue.eventCategory">Event Category</Translate>
                  </Label>
                  <AvField id="user-event-queue-eventCategory" type="text" name="eventCategory" />
                </AvGroup>
                <AvGroup>
                  <Label id="programUserIdLabel" for="user-event-queue-programUserId">
                    <Translate contentKey="programserviceApp.userEventQueue.programUserId">Program User Id</Translate>
                  </Label>
                  <AvField
                    id="user-event-queue-programUserId"
                    type="string"
                    className="form-control"
                    name="programUserId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="executeStatusLabel" for="user-event-queue-executeStatus">
                    <Translate contentKey="programserviceApp.userEventQueue.executeStatus">Execute Status</Translate>
                  </Label>
                  <AvField
                    id="user-event-queue-executeStatus"
                    type="text"
                    name="executeStatus"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="programIdLabel" for="user-event-queue-programId">
                    <Translate contentKey="programserviceApp.userEventQueue.programId">Program Id</Translate>
                  </Label>
                  <AvField
                    id="user-event-queue-programId"
                    type="string"
                    className="form-control"
                    name="programId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="readAtLabel" for="user-event-queue-readAt">
                    <Translate contentKey="programserviceApp.userEventQueue.readAt">Read At</Translate>
                  </Label>
                  <AvInput
                    id="user-event-queue-readAt"
                    type="datetime-local"
                    className="form-control"
                    name="readAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.userEventQueueEntity.readAt)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="errorMessageLabel" for="user-event-queue-errorMessage">
                    <Translate contentKey="programserviceApp.userEventQueue.errorMessage">Error Message</Translate>
                  </Label>
                  <AvField id="user-event-queue-errorMessage" type="text" name="errorMessage" />
                </AvGroup>
                <AvGroup>
                  <Label id="currentStepLabel" for="user-event-queue-currentStep">
                    <Translate contentKey="programserviceApp.userEventQueue.currentStep">Current Step</Translate>
                  </Label>
                  <AvField id="user-event-queue-currentStep" type="text" name="currentStep" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/user-event-queue" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  userEventQueueEntity: storeState.userEventQueue.entity,
  loading: storeState.userEventQueue.loading,
  updating: storeState.userEventQueue.updating,
  updateSuccess: storeState.userEventQueue.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEventQueueUpdate);
