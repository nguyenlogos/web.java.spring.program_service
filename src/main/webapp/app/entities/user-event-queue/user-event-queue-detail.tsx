import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-event-queue.reducer';
import { IUserEventQueue } from 'app/shared/model/user-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserEventQueueDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class UserEventQueueDetail extends React.Component<IUserEventQueueDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { userEventQueueEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.userEventQueue.detail.title">UserEventQueue</Translate> [
            <b>{userEventQueueEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="eventCode">
                <Translate contentKey="programserviceApp.userEventQueue.eventCode">Event Code</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.eventCode}</dd>
            <dt>
              <span id="eventId">
                <Translate contentKey="programserviceApp.userEventQueue.eventId">Event Id</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.eventId}</dd>
            <dt>
              <span id="eventDate">
                <Translate contentKey="programserviceApp.userEventQueue.eventDate">Event Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={userEventQueueEntity.eventDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="eventPoint">
                <Translate contentKey="programserviceApp.userEventQueue.eventPoint">Event Point</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.eventPoint}</dd>
            <dt>
              <span id="eventCategory">
                <Translate contentKey="programserviceApp.userEventQueue.eventCategory">Event Category</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.eventCategory}</dd>
            <dt>
              <span id="programUserId">
                <Translate contentKey="programserviceApp.userEventQueue.programUserId">Program User Id</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.programUserId}</dd>
            <dt>
              <span id="executeStatus">
                <Translate contentKey="programserviceApp.userEventQueue.executeStatus">Execute Status</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.executeStatus}</dd>
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.userEventQueue.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.programId}</dd>
            <dt>
              <span id="readAt">
                <Translate contentKey="programserviceApp.userEventQueue.readAt">Read At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={userEventQueueEntity.readAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="errorMessage">
                <Translate contentKey="programserviceApp.userEventQueue.errorMessage">Error Message</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.errorMessage}</dd>
            <dt>
              <span id="currentStep">
                <Translate contentKey="programserviceApp.userEventQueue.currentStep">Current Step</Translate>
              </span>
            </dt>
            <dd>{userEventQueueEntity.currentStep}</dd>
          </dl>
          <Button tag={Link} to="/entity/user-event-queue" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/user-event-queue/${userEventQueueEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ userEventQueue }: IRootState) => ({
  userEventQueueEntity: userEventQueue.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEventQueueDetail);
