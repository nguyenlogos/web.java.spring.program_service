import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-subgroup.reducer';
import { IProgramSubgroup } from 'app/shared/model/program-subgroup.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramSubgroupUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramSubgroupUpdateState {
  isNew: boolean;
  programId: string;
}

export class ProgramSubgroupUpdate extends React.Component<IProgramSubgroupUpdateProps, IProgramSubgroupUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPrograms();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programSubgroupEntity } = this.props;
      const entity = {
        ...programSubgroupEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-subgroup');
  };

  render() {
    const { programSubgroupEntity, programs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programSubgroup.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programSubgroup.home.createOrEditLabel">Create or edit a ProgramSubgroup</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programSubgroupEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-subgroup-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-subgroup-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="subgroupIdLabel" for="program-subgroup-subgroupId">
                    <Translate contentKey="programserviceApp.programSubgroup.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField
                    id="program-subgroup-subgroupId"
                    type="text"
                    name="subgroupId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupNameLabel" for="program-subgroup-subgroupName">
                    <Translate contentKey="programserviceApp.programSubgroup.subgroupName">Subgroup Name</Translate>
                  </Label>
                  <AvField
                    id="program-subgroup-subgroupName"
                    type="text"
                    name="subgroupName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-subgroup-program">
                    <Translate contentKey="programserviceApp.programSubgroup.program">Program</Translate>
                  </Label>
                  <AvInput id="program-subgroup-program" type="select" className="form-control" name="programId" required>
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-subgroup" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programs: storeState.program.entities,
  programSubgroupEntity: storeState.programSubgroup.entity,
  loading: storeState.programSubgroup.loading,
  updating: storeState.programSubgroup.updating,
  updateSuccess: storeState.programSubgroup.updateSuccess
});

const mapDispatchToProps = {
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramSubgroupUpdate);
