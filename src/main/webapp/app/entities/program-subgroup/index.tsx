import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramSubgroup from './program-subgroup';
import ProgramSubgroupDetail from './program-subgroup-detail';
import ProgramSubgroupUpdate from './program-subgroup-update';
import ProgramSubgroupDeleteDialog from './program-subgroup-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramSubgroupUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramSubgroupUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramSubgroupDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramSubgroup} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramSubgroupDeleteDialog} />
  </>
);

export default Routes;
