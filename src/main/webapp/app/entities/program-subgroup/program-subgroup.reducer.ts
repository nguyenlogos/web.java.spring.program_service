import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramSubgroup, defaultValue } from 'app/shared/model/program-subgroup.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMSUBGROUP_LIST: 'programSubgroup/FETCH_PROGRAMSUBGROUP_LIST',
  FETCH_PROGRAMSUBGROUP: 'programSubgroup/FETCH_PROGRAMSUBGROUP',
  CREATE_PROGRAMSUBGROUP: 'programSubgroup/CREATE_PROGRAMSUBGROUP',
  UPDATE_PROGRAMSUBGROUP: 'programSubgroup/UPDATE_PROGRAMSUBGROUP',
  DELETE_PROGRAMSUBGROUP: 'programSubgroup/DELETE_PROGRAMSUBGROUP',
  RESET: 'programSubgroup/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramSubgroup>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramSubgroupState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramSubgroupState = initialState, action): ProgramSubgroupState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMSUBGROUP_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMSUBGROUP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMSUBGROUP):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMSUBGROUP):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMSUBGROUP):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMSUBGROUP_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMSUBGROUP):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMSUBGROUP):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMSUBGROUP):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMSUBGROUP):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMSUBGROUP_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMSUBGROUP):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMSUBGROUP):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMSUBGROUP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMSUBGROUP):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-subgroups';

// Actions

export const getEntities: ICrudGetAllAction<IProgramSubgroup> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMSUBGROUP_LIST,
    payload: axios.get<IProgramSubgroup>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramSubgroup> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMSUBGROUP,
    payload: axios.get<IProgramSubgroup>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramSubgroup> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMSUBGROUP,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramSubgroup> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMSUBGROUP,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramSubgroup> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMSUBGROUP,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
