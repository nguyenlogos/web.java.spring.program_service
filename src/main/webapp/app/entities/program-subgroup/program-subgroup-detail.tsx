import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-subgroup.reducer';
import { IProgramSubgroup } from 'app/shared/model/program-subgroup.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramSubgroupDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramSubgroupDetail extends React.Component<IProgramSubgroupDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programSubgroupEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programSubgroup.detail.title">ProgramSubgroup</Translate> [
            <b>{programSubgroupEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.programSubgroup.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{programSubgroupEntity.subgroupId}</dd>
            <dt>
              <span id="subgroupName">
                <Translate contentKey="programserviceApp.programSubgroup.subgroupName">Subgroup Name</Translate>
              </span>
            </dt>
            <dd>{programSubgroupEntity.subgroupName}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programSubgroup.program">Program</Translate>
            </dt>
            <dd>{programSubgroupEntity.programName ? programSubgroupEntity.programName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-subgroup" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-subgroup/${programSubgroupEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programSubgroup }: IRootState) => ({
  programSubgroupEntity: programSubgroup.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramSubgroupDetail);
