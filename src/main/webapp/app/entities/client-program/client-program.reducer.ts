import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IClientProgram, defaultValue } from 'app/shared/model/client-program.model';

export const ACTION_TYPES = {
  FETCH_CLIENTPROGRAM_LIST: 'clientProgram/FETCH_CLIENTPROGRAM_LIST',
  FETCH_CLIENTPROGRAM: 'clientProgram/FETCH_CLIENTPROGRAM',
  CREATE_CLIENTPROGRAM: 'clientProgram/CREATE_CLIENTPROGRAM',
  UPDATE_CLIENTPROGRAM: 'clientProgram/UPDATE_CLIENTPROGRAM',
  DELETE_CLIENTPROGRAM: 'clientProgram/DELETE_CLIENTPROGRAM',
  RESET: 'clientProgram/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IClientProgram>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ClientProgramState = Readonly<typeof initialState>;

// Reducer

export default (state: ClientProgramState = initialState, action): ClientProgramState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CLIENTPROGRAM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CLIENTPROGRAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CLIENTPROGRAM):
    case REQUEST(ACTION_TYPES.UPDATE_CLIENTPROGRAM):
    case REQUEST(ACTION_TYPES.DELETE_CLIENTPROGRAM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CLIENTPROGRAM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CLIENTPROGRAM):
    case FAILURE(ACTION_TYPES.CREATE_CLIENTPROGRAM):
    case FAILURE(ACTION_TYPES.UPDATE_CLIENTPROGRAM):
    case FAILURE(ACTION_TYPES.DELETE_CLIENTPROGRAM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLIENTPROGRAM_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLIENTPROGRAM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CLIENTPROGRAM):
    case SUCCESS(ACTION_TYPES.UPDATE_CLIENTPROGRAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CLIENTPROGRAM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/client-programs';

// Actions

export const getEntities: ICrudGetAllAction<IClientProgram> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CLIENTPROGRAM_LIST,
    payload: axios.get<IClientProgram>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IClientProgram> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CLIENTPROGRAM,
    payload: axios.get<IClientProgram>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IClientProgram> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CLIENTPROGRAM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IClientProgram> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CLIENTPROGRAM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IClientProgram> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CLIENTPROGRAM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
