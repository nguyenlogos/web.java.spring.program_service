import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './client-program.reducer';
import { IClientProgram } from 'app/shared/model/client-program.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IClientProgramDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ClientProgramDetail extends React.Component<IClientProgramDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { clientProgramEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.clientProgram.detail.title">ClientProgram</Translate> [<b>{clientProgramEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.clientProgram.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{clientProgramEntity.clientId}</dd>
            <dt>
              <span id="clientName">
                <Translate contentKey="programserviceApp.clientProgram.clientName">Client Name</Translate>
              </span>
            </dt>
            <dd>{clientProgramEntity.clientName}</dd>
            <dt>
              <Translate contentKey="programserviceApp.clientProgram.program">Program</Translate>
            </dt>
            <dd>{clientProgramEntity.programName ? clientProgramEntity.programName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/client-program" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/client-program/${clientProgramEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ clientProgram }: IRootState) => ({
  clientProgramEntity: clientProgram.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientProgramDetail);
