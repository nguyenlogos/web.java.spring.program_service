import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ClientProgram from './client-program';
import ClientProgramDetail from './client-program-detail';
import ClientProgramUpdate from './client-program-update';
import ClientProgramDeleteDialog from './client-program-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ClientProgramUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ClientProgramUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ClientProgramDetail} />
      <ErrorBoundaryRoute path={match.url} component={ClientProgram} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ClientProgramDeleteDialog} />
  </>
);

export default Routes;
