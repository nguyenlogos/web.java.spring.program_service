import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramLevelPractice, defaultValue } from 'app/shared/model/program-level-practice.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMLEVELPRACTICE_LIST: 'programLevelPractice/FETCH_PROGRAMLEVELPRACTICE_LIST',
  FETCH_PROGRAMLEVELPRACTICE: 'programLevelPractice/FETCH_PROGRAMLEVELPRACTICE',
  CREATE_PROGRAMLEVELPRACTICE: 'programLevelPractice/CREATE_PROGRAMLEVELPRACTICE',
  UPDATE_PROGRAMLEVELPRACTICE: 'programLevelPractice/UPDATE_PROGRAMLEVELPRACTICE',
  DELETE_PROGRAMLEVELPRACTICE: 'programLevelPractice/DELETE_PROGRAMLEVELPRACTICE',
  RESET: 'programLevelPractice/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramLevelPractice>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramLevelPracticeState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramLevelPracticeState = initialState, action): ProgramLevelPracticeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVELPRACTICE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVELPRACTICE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMLEVELPRACTICE):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMLEVELPRACTICE):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMLEVELPRACTICE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVELPRACTICE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVELPRACTICE):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMLEVELPRACTICE):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMLEVELPRACTICE):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMLEVELPRACTICE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVELPRACTICE_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVELPRACTICE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMLEVELPRACTICE):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMLEVELPRACTICE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMLEVELPRACTICE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-level-practices';

// Actions

export const getEntities: ICrudGetAllAction<IProgramLevelPractice> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVELPRACTICE_LIST,
    payload: axios.get<IProgramLevelPractice>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramLevelPractice> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVELPRACTICE,
    payload: axios.get<IProgramLevelPractice>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramLevelPractice> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMLEVELPRACTICE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramLevelPractice> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMLEVELPRACTICE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramLevelPractice> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMLEVELPRACTICE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
