import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramLevelPractice from './program-level-practice';
import ProgramLevelPracticeDetail from './program-level-practice-detail';
import ProgramLevelPracticeUpdate from './program-level-practice-update';
import ProgramLevelPracticeDeleteDialog from './program-level-practice-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramLevelPracticeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramLevelPracticeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramLevelPracticeDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramLevelPractice} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramLevelPracticeDeleteDialog} />
  </>
);

export default Routes;
