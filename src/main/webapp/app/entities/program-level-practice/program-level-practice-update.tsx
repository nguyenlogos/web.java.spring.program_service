import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramLevel } from 'app/shared/model/program-level.model';
import { getEntities as getProgramLevels } from 'app/entities/program-level/program-level.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-level-practice.reducer';
import { IProgramLevelPractice } from 'app/shared/model/program-level-practice.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramLevelPracticeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramLevelPracticeUpdateState {
  isNew: boolean;
  programLevelId: string;
}

export class ProgramLevelPracticeUpdate extends React.Component<IProgramLevelPracticeUpdateProps, IProgramLevelPracticeUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programLevelId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramLevels();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programLevelPracticeEntity } = this.props;
      const entity = {
        ...programLevelPracticeEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-level-practice');
  };

  render() {
    const { programLevelPracticeEntity, programLevels, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programLevelPractice.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programLevelPractice.home.createOrEditLabel">
                Create or edit a ProgramLevelPractice
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programLevelPracticeEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-level-practice-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-level-practice-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="practiceIdLabel" for="program-level-practice-practiceId">
                    <Translate contentKey="programserviceApp.programLevelPractice.practiceId">Practice Id</Translate>
                  </Label>
                  <AvField
                    id="program-level-practice-practiceId"
                    type="text"
                    name="practiceId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="nameLabel" for="program-level-practice-name">
                    <Translate contentKey="programserviceApp.programLevelPractice.name">Name</Translate>
                  </Label>
                  <AvField
                    id="program-level-practice-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="program-level-practice-subgroupId">
                    <Translate contentKey="programserviceApp.programLevelPractice.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="program-level-practice-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupNameLabel" for="program-level-practice-subgroupName">
                    <Translate contentKey="programserviceApp.programLevelPractice.subgroupName">Subgroup Name</Translate>
                  </Label>
                  <AvField id="program-level-practice-subgroupName" type="text" name="subgroupName" />
                </AvGroup>
                <AvGroup>
                  <Label for="program-level-practice-programLevel">
                    <Translate contentKey="programserviceApp.programLevelPractice.programLevel">Program Level</Translate>
                  </Label>
                  <AvInput id="program-level-practice-programLevel" type="select" className="form-control" name="programLevelId" required>
                    {programLevels
                      ? programLevels.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-level-practice" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programLevels: storeState.programLevel.entities,
  programLevelPracticeEntity: storeState.programLevelPractice.entity,
  loading: storeState.programLevelPractice.loading,
  updating: storeState.programLevelPractice.updating,
  updateSuccess: storeState.programLevelPractice.updateSuccess
});

const mapDispatchToProps = {
  getProgramLevels,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelPracticeUpdate);
