import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-level-practice.reducer';
import { IProgramLevelPractice } from 'app/shared/model/program-level-practice.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramLevelPracticeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramLevelPracticeDetail extends React.Component<IProgramLevelPracticeDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programLevelPracticeEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programLevelPractice.detail.title">ProgramLevelPractice</Translate> [
            <b>{programLevelPracticeEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="practiceId">
                <Translate contentKey="programserviceApp.programLevelPractice.practiceId">Practice Id</Translate>
              </span>
            </dt>
            <dd>{programLevelPracticeEntity.practiceId}</dd>
            <dt>
              <span id="name">
                <Translate contentKey="programserviceApp.programLevelPractice.name">Name</Translate>
              </span>
            </dt>
            <dd>{programLevelPracticeEntity.name}</dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.programLevelPractice.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{programLevelPracticeEntity.subgroupId}</dd>
            <dt>
              <span id="subgroupName">
                <Translate contentKey="programserviceApp.programLevelPractice.subgroupName">Subgroup Name</Translate>
              </span>
            </dt>
            <dd>{programLevelPracticeEntity.subgroupName}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programLevelPractice.programLevel">Program Level</Translate>
            </dt>
            <dd>{programLevelPracticeEntity.programLevelName ? programLevelPracticeEntity.programLevelName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-level-practice" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-level-practice/${programLevelPracticeEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programLevelPractice }: IRootState) => ({
  programLevelPracticeEntity: programLevelPractice.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelPracticeDetail);
