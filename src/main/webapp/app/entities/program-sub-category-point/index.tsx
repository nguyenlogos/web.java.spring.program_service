import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramSubCategoryPoint from './program-sub-category-point';
import ProgramSubCategoryPointDetail from './program-sub-category-point-detail';
import ProgramSubCategoryPointUpdate from './program-sub-category-point-update';
import ProgramSubCategoryPointDeleteDialog from './program-sub-category-point-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramSubCategoryPointUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramSubCategoryPointUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramSubCategoryPointDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramSubCategoryPoint} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramSubCategoryPointDeleteDialog} />
  </>
);

export default Routes;
