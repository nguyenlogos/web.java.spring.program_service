import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-sub-category-point.reducer';
import { IProgramSubCategoryPoint } from 'app/shared/model/program-sub-category-point.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramSubCategoryPointDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramSubCategoryPointDetail extends React.Component<IProgramSubCategoryPointDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programSubCategoryPointEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programSubCategoryPoint.detail.title">ProgramSubCategoryPoint</Translate> [
            <b>{programSubCategoryPointEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="code">
                <Translate contentKey="programserviceApp.programSubCategoryPoint.code">Code</Translate>
              </span>
            </dt>
            <dd>{programSubCategoryPointEntity.code}</dd>
            <dt>
              <span id="percentPoint">
                <Translate contentKey="programserviceApp.programSubCategoryPoint.percentPoint">Percent Point</Translate>
              </span>
            </dt>
            <dd>{programSubCategoryPointEntity.percentPoint}</dd>
            <dt>
              <span id="valuePoint">
                <Translate contentKey="programserviceApp.programSubCategoryPoint.valuePoint">Value Point</Translate>
              </span>
            </dt>
            <dd>{programSubCategoryPointEntity.valuePoint}</dd>
            <dt>
              <span id="completionsCap">
                <Translate contentKey="programserviceApp.programSubCategoryPoint.completionsCap">Completions Cap</Translate>
              </span>
            </dt>
            <dd>{programSubCategoryPointEntity.completionsCap}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programSubCategoryPoint.programCategoryPoint">Program Category Point</Translate>
            </dt>
            <dd>
              {programSubCategoryPointEntity.programCategoryPointCategoryCode
                ? programSubCategoryPointEntity.programCategoryPointCategoryCode
                : ''}
            </dd>
          </dl>
          <Button tag={Link} to="/entity/program-sub-category-point" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-sub-category-point/${programSubCategoryPointEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programSubCategoryPoint }: IRootState) => ({
  programSubCategoryPointEntity: programSubCategoryPoint.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramSubCategoryPointDetail);
