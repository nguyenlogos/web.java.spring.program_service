import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './program-sub-category-point.reducer';
import { IProgramSubCategoryPoint } from 'app/shared/model/program-sub-category-point.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramSubCategoryPointProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramSubCategoryPointState = IPaginationBaseState;

export class ProgramSubCategoryPoint extends React.Component<IProgramSubCategoryPointProps, IProgramSubCategoryPointState> {
  state: IProgramSubCategoryPointState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programSubCategoryPointList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="program-sub-category-point-heading">
          <Translate contentKey="programserviceApp.programSubCategoryPoint.home.title">Program Sub Category Points</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.programSubCategoryPoint.home.createLabel">
              Create new Program Sub Category Point
            </Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('code')}>
                  <Translate contentKey="programserviceApp.programSubCategoryPoint.code">Code</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('percentPoint')}>
                  <Translate contentKey="programserviceApp.programSubCategoryPoint.percentPoint">Percent Point</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('valuePoint')}>
                  <Translate contentKey="programserviceApp.programSubCategoryPoint.valuePoint">Value Point</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('completionsCap')}>
                  <Translate contentKey="programserviceApp.programSubCategoryPoint.completionsCap">Completions Cap</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="programserviceApp.programSubCategoryPoint.programCategoryPoint">Program Category Point</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {programSubCategoryPointList.map((programSubCategoryPoint, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${programSubCategoryPoint.id}`} color="link" size="sm">
                      {programSubCategoryPoint.id}
                    </Button>
                  </td>
                  <td>{programSubCategoryPoint.code}</td>
                  <td>{programSubCategoryPoint.percentPoint}</td>
                  <td>{programSubCategoryPoint.valuePoint}</td>
                  <td>{programSubCategoryPoint.completionsCap}</td>
                  <td>
                    {programSubCategoryPoint.programCategoryPointCategoryCode ? (
                      <Link to={`program-category-point/${programSubCategoryPoint.programCategoryPointId}`}>
                        {programSubCategoryPoint.programCategoryPointCategoryCode}
                      </Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${programSubCategoryPoint.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programSubCategoryPoint.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programSubCategoryPoint.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ programSubCategoryPoint }: IRootState) => ({
  programSubCategoryPointList: programSubCategoryPoint.entities,
  totalItems: programSubCategoryPoint.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramSubCategoryPoint);
