import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramSubCategoryPoint, defaultValue } from 'app/shared/model/program-sub-category-point.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMSUBCATEGORYPOINT_LIST: 'programSubCategoryPoint/FETCH_PROGRAMSUBCATEGORYPOINT_LIST',
  FETCH_PROGRAMSUBCATEGORYPOINT: 'programSubCategoryPoint/FETCH_PROGRAMSUBCATEGORYPOINT',
  CREATE_PROGRAMSUBCATEGORYPOINT: 'programSubCategoryPoint/CREATE_PROGRAMSUBCATEGORYPOINT',
  UPDATE_PROGRAMSUBCATEGORYPOINT: 'programSubCategoryPoint/UPDATE_PROGRAMSUBCATEGORYPOINT',
  DELETE_PROGRAMSUBCATEGORYPOINT: 'programSubCategoryPoint/DELETE_PROGRAMSUBCATEGORYPOINT',
  RESET: 'programSubCategoryPoint/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramSubCategoryPoint>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramSubCategoryPointState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramSubCategoryPointState = initialState, action): ProgramSubCategoryPointState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYPOINT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYPOINT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMSUBCATEGORYPOINT):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMSUBCATEGORYPOINT):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMSUBCATEGORYPOINT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYPOINT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYPOINT):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMSUBCATEGORYPOINT):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMSUBCATEGORYPOINT):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMSUBCATEGORYPOINT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYPOINT_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYPOINT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMSUBCATEGORYPOINT):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMSUBCATEGORYPOINT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMSUBCATEGORYPOINT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-sub-category-points';

// Actions

export const getEntities: ICrudGetAllAction<IProgramSubCategoryPoint> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYPOINT_LIST,
    payload: axios.get<IProgramSubCategoryPoint>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramSubCategoryPoint> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYPOINT,
    payload: axios.get<IProgramSubCategoryPoint>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramSubCategoryPoint> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMSUBCATEGORYPOINT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramSubCategoryPoint> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMSUBCATEGORYPOINT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramSubCategoryPoint> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMSUBCATEGORYPOINT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
