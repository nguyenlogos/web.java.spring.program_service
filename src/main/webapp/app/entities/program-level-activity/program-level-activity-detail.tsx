import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-level-activity.reducer';
import { IProgramLevelActivity } from 'app/shared/model/program-level-activity.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramLevelActivityDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramLevelActivityDetail extends React.Component<IProgramLevelActivityDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programLevelActivityEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programLevelActivity.detail.title">ProgramLevelActivity</Translate> [
            <b>{programLevelActivityEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="activityCode">
                <Translate contentKey="programserviceApp.programLevelActivity.activityCode">Activity Code</Translate>
              </span>
            </dt>
            <dd>{programLevelActivityEntity.activityCode}</dd>
            <dt>
              <span id="activityId">
                <Translate contentKey="programserviceApp.programLevelActivity.activityId">Activity Id</Translate>
              </span>
            </dt>
            <dd>{programLevelActivityEntity.activityId}</dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.programLevelActivity.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{programLevelActivityEntity.subgroupId}</dd>
            <dt>
              <span id="subgroupName">
                <Translate contentKey="programserviceApp.programLevelActivity.subgroupName">Subgroup Name</Translate>
              </span>
            </dt>
            <dd>{programLevelActivityEntity.subgroupName}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programLevelActivity.programLevel">Program Level</Translate>
            </dt>
            <dd>{programLevelActivityEntity.programLevelDescription ? programLevelActivityEntity.programLevelDescription : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-level-activity" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-level-activity/${programLevelActivityEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programLevelActivity }: IRootState) => ({
  programLevelActivityEntity: programLevelActivity.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelActivityDetail);
