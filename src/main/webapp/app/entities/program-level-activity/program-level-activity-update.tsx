import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramLevel } from 'app/shared/model/program-level.model';
import { getEntities as getProgramLevels } from 'app/entities/program-level/program-level.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-level-activity.reducer';
import { IProgramLevelActivity } from 'app/shared/model/program-level-activity.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramLevelActivityUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramLevelActivityUpdateState {
  isNew: boolean;
  programLevelId: string;
}

export class ProgramLevelActivityUpdate extends React.Component<IProgramLevelActivityUpdateProps, IProgramLevelActivityUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programLevelId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramLevels();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programLevelActivityEntity } = this.props;
      const entity = {
        ...programLevelActivityEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-level-activity');
  };

  render() {
    const { programLevelActivityEntity, programLevels, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programLevelActivity.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programLevelActivity.home.createOrEditLabel">
                Create or edit a ProgramLevelActivity
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programLevelActivityEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-level-activity-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-level-activity-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="activityCodeLabel" for="program-level-activity-activityCode">
                    <Translate contentKey="programserviceApp.programLevelActivity.activityCode">Activity Code</Translate>
                  </Label>
                  <AvField id="program-level-activity-activityCode" type="text" name="activityCode" />
                </AvGroup>
                <AvGroup>
                  <Label id="activityIdLabel" for="program-level-activity-activityId">
                    <Translate contentKey="programserviceApp.programLevelActivity.activityId">Activity Id</Translate>
                  </Label>
                  <AvField
                    id="program-level-activity-activityId"
                    type="text"
                    name="activityId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="program-level-activity-subgroupId">
                    <Translate contentKey="programserviceApp.programLevelActivity.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="program-level-activity-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupNameLabel" for="program-level-activity-subgroupName">
                    <Translate contentKey="programserviceApp.programLevelActivity.subgroupName">Subgroup Name</Translate>
                  </Label>
                  <AvField id="program-level-activity-subgroupName" type="text" name="subgroupName" />
                </AvGroup>
                <AvGroup>
                  <Label for="program-level-activity-programLevel">
                    <Translate contentKey="programserviceApp.programLevelActivity.programLevel">Program Level</Translate>
                  </Label>
                  <AvInput id="program-level-activity-programLevel" type="select" className="form-control" name="programLevelId" required>
                    {programLevels
                      ? programLevels.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.description}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-level-activity" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programLevels: storeState.programLevel.entities,
  programLevelActivityEntity: storeState.programLevelActivity.entity,
  loading: storeState.programLevelActivity.loading,
  updating: storeState.programLevelActivity.updating,
  updateSuccess: storeState.programLevelActivity.updateSuccess
});

const mapDispatchToProps = {
  getProgramLevels,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelActivityUpdate);
