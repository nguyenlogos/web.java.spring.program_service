import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramLevelActivity, defaultValue } from 'app/shared/model/program-level-activity.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMLEVELACTIVITY_LIST: 'programLevelActivity/FETCH_PROGRAMLEVELACTIVITY_LIST',
  FETCH_PROGRAMLEVELACTIVITY: 'programLevelActivity/FETCH_PROGRAMLEVELACTIVITY',
  CREATE_PROGRAMLEVELACTIVITY: 'programLevelActivity/CREATE_PROGRAMLEVELACTIVITY',
  UPDATE_PROGRAMLEVELACTIVITY: 'programLevelActivity/UPDATE_PROGRAMLEVELACTIVITY',
  DELETE_PROGRAMLEVELACTIVITY: 'programLevelActivity/DELETE_PROGRAMLEVELACTIVITY',
  RESET: 'programLevelActivity/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramLevelActivity>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramLevelActivityState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramLevelActivityState = initialState, action): ProgramLevelActivityState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVELACTIVITY_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVELACTIVITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMLEVELACTIVITY):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMLEVELACTIVITY):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMLEVELACTIVITY):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVELACTIVITY_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVELACTIVITY):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMLEVELACTIVITY):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMLEVELACTIVITY):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMLEVELACTIVITY):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVELACTIVITY_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVELACTIVITY):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMLEVELACTIVITY):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMLEVELACTIVITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMLEVELACTIVITY):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-level-activities';

// Actions

export const getEntities: ICrudGetAllAction<IProgramLevelActivity> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVELACTIVITY_LIST,
    payload: axios.get<IProgramLevelActivity>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramLevelActivity> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVELACTIVITY,
    payload: axios.get<IProgramLevelActivity>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramLevelActivity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMLEVELACTIVITY,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramLevelActivity> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMLEVELACTIVITY,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramLevelActivity> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMLEVELACTIVITY,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
