import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramLevelActivity from './program-level-activity';
import ProgramLevelActivityDetail from './program-level-activity-detail';
import ProgramLevelActivityUpdate from './program-level-activity-update';
import ProgramLevelActivityDeleteDialog from './program-level-activity-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramLevelActivityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramLevelActivityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramLevelActivityDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramLevelActivity} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramLevelActivityDeleteDialog} />
  </>
);

export default Routes;
