import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ClientSetting from './client-setting';
import ClientSettingDetail from './client-setting-detail';
import ClientSettingUpdate from './client-setting-update';
import ClientSettingDeleteDialog from './client-setting-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ClientSettingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ClientSettingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ClientSettingDetail} />
      <ErrorBoundaryRoute path={match.url} component={ClientSetting} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ClientSettingDeleteDialog} />
  </>
);

export default Routes;
