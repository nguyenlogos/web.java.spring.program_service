import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './client-setting.reducer';
import { IClientSetting } from 'app/shared/model/client-setting.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IClientSettingProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IClientSettingState = IPaginationBaseState;

export class ClientSetting extends React.Component<IClientSettingProps, IClientSettingState> {
  state: IClientSettingState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { clientSettingList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="client-setting-heading">
          <Translate contentKey="programserviceApp.clientSetting.home.title">Client Settings</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.clientSetting.home.createLabel">Create new Client Setting</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientId')}>
                  <Translate contentKey="programserviceApp.clientSetting.clientId">Client Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientName')}>
                  <Translate contentKey="programserviceApp.clientSetting.clientName">Client Name</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientEmail')}>
                  <Translate contentKey="programserviceApp.clientSetting.clientEmail">Client Email</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('tangoAccountIdentifier')}>
                  <Translate contentKey="programserviceApp.clientSetting.tangoAccountIdentifier">Tango Account Identifier</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('tangoCustomerIdentifier')}>
                  <Translate contentKey="programserviceApp.clientSetting.tangoCustomerIdentifier">Tango Customer Identifier</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('tangoCurrentBalance')}>
                  <Translate contentKey="programserviceApp.clientSetting.tangoCurrentBalance">Tango Current Balance</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('tangoThresholdBalance')}>
                  <Translate contentKey="programserviceApp.clientSetting.tangoThresholdBalance">Tango Threshold Balance</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('tangoCreditToken')}>
                  <Translate contentKey="programserviceApp.clientSetting.tangoCreditToken">Tango Credit Token</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('updatedDate')}>
                  <Translate contentKey="programserviceApp.clientSetting.updatedDate">Updated Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('updatedBy')}>
                  <Translate contentKey="programserviceApp.clientSetting.updatedBy">Updated By</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {clientSettingList.map((clientSetting, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${clientSetting.id}`} color="link" size="sm">
                      {clientSetting.id}
                    </Button>
                  </td>
                  <td>{clientSetting.clientId}</td>
                  <td>{clientSetting.clientName}</td>
                  <td>{clientSetting.clientEmail}</td>
                  <td>{clientSetting.tangoAccountIdentifier}</td>
                  <td>{clientSetting.tangoCustomerIdentifier}</td>
                  <td>{clientSetting.tangoCurrentBalance}</td>
                  <td>{clientSetting.tangoThresholdBalance}</td>
                  <td>{clientSetting.tangoCreditToken}</td>
                  <td>
                    <TextFormat type="date" value={clientSetting.updatedDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{clientSetting.updatedBy}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${clientSetting.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${clientSetting.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${clientSetting.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ clientSetting }: IRootState) => ({
  clientSettingList: clientSetting.entities,
  totalItems: clientSetting.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientSetting);
