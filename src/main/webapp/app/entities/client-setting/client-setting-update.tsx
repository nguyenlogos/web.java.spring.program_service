import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './client-setting.reducer';
import { IClientSetting } from 'app/shared/model/client-setting.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IClientSettingUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IClientSettingUpdateState {
  isNew: boolean;
}

export class ClientSettingUpdate extends React.Component<IClientSettingUpdateProps, IClientSettingUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.updatedDate = convertDateTimeToServer(values.updatedDate);

    if (errors.length === 0) {
      const { clientSettingEntity } = this.props;
      const entity = {
        ...clientSettingEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/client-setting');
  };

  render() {
    const { clientSettingEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.clientSetting.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.clientSetting.home.createOrEditLabel">Create or edit a ClientSetting</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : clientSettingEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="client-setting-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="client-setting-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="clientIdLabel" for="client-setting-clientId">
                    <Translate contentKey="programserviceApp.clientSetting.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="client-setting-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="clientNameLabel" for="client-setting-clientName">
                    <Translate contentKey="programserviceApp.clientSetting.clientName">Client Name</Translate>
                  </Label>
                  <AvField
                    id="client-setting-clientName"
                    type="text"
                    name="clientName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="clientEmailLabel" for="client-setting-clientEmail">
                    <Translate contentKey="programserviceApp.clientSetting.clientEmail">Client Email</Translate>
                  </Label>
                  <AvField id="client-setting-clientEmail" type="text" name="clientEmail" />
                </AvGroup>
                <AvGroup>
                  <Label id="tangoAccountIdentifierLabel" for="client-setting-tangoAccountIdentifier">
                    <Translate contentKey="programserviceApp.clientSetting.tangoAccountIdentifier">Tango Account Identifier</Translate>
                  </Label>
                  <AvField id="client-setting-tangoAccountIdentifier" type="text" name="tangoAccountIdentifier" />
                </AvGroup>
                <AvGroup>
                  <Label id="tangoCustomerIdentifierLabel" for="client-setting-tangoCustomerIdentifier">
                    <Translate contentKey="programserviceApp.clientSetting.tangoCustomerIdentifier">Tango Customer Identifier</Translate>
                  </Label>
                  <AvField id="client-setting-tangoCustomerIdentifier" type="text" name="tangoCustomerIdentifier" />
                </AvGroup>
                <AvGroup>
                  <Label id="tangoCurrentBalanceLabel" for="client-setting-tangoCurrentBalance">
                    <Translate contentKey="programserviceApp.clientSetting.tangoCurrentBalance">Tango Current Balance</Translate>
                  </Label>
                  <AvField id="client-setting-tangoCurrentBalance" type="text" name="tangoCurrentBalance" />
                </AvGroup>
                <AvGroup>
                  <Label id="tangoThresholdBalanceLabel" for="client-setting-tangoThresholdBalance">
                    <Translate contentKey="programserviceApp.clientSetting.tangoThresholdBalance">Tango Threshold Balance</Translate>
                  </Label>
                  <AvField id="client-setting-tangoThresholdBalance" type="text" name="tangoThresholdBalance" />
                </AvGroup>
                <AvGroup>
                  <Label id="tangoCreditTokenLabel" for="client-setting-tangoCreditToken">
                    <Translate contentKey="programserviceApp.clientSetting.tangoCreditToken">Tango Credit Token</Translate>
                  </Label>
                  <AvField id="client-setting-tangoCreditToken" type="text" name="tangoCreditToken" />
                </AvGroup>
                <AvGroup>
                  <Label id="updatedDateLabel" for="client-setting-updatedDate">
                    <Translate contentKey="programserviceApp.clientSetting.updatedDate">Updated Date</Translate>
                  </Label>
                  <AvInput
                    id="client-setting-updatedDate"
                    type="datetime-local"
                    className="form-control"
                    name="updatedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.clientSettingEntity.updatedDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="updatedByLabel" for="client-setting-updatedBy">
                    <Translate contentKey="programserviceApp.clientSetting.updatedBy">Updated By</Translate>
                  </Label>
                  <AvField
                    id="client-setting-updatedBy"
                    type="text"
                    name="updatedBy"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/client-setting" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  clientSettingEntity: storeState.clientSetting.entity,
  loading: storeState.clientSetting.loading,
  updating: storeState.clientSetting.updating,
  updateSuccess: storeState.clientSetting.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientSettingUpdate);
