import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './client-setting.reducer';
import { IClientSetting } from 'app/shared/model/client-setting.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IClientSettingDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ClientSettingDetail extends React.Component<IClientSettingDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { clientSettingEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.clientSetting.detail.title">ClientSetting</Translate> [<b>{clientSettingEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.clientSetting.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.clientId}</dd>
            <dt>
              <span id="clientName">
                <Translate contentKey="programserviceApp.clientSetting.clientName">Client Name</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.clientName}</dd>
            <dt>
              <span id="clientEmail">
                <Translate contentKey="programserviceApp.clientSetting.clientEmail">Client Email</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.clientEmail}</dd>
            <dt>
              <span id="tangoAccountIdentifier">
                <Translate contentKey="programserviceApp.clientSetting.tangoAccountIdentifier">Tango Account Identifier</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.tangoAccountIdentifier}</dd>
            <dt>
              <span id="tangoCustomerIdentifier">
                <Translate contentKey="programserviceApp.clientSetting.tangoCustomerIdentifier">Tango Customer Identifier</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.tangoCustomerIdentifier}</dd>
            <dt>
              <span id="tangoCurrentBalance">
                <Translate contentKey="programserviceApp.clientSetting.tangoCurrentBalance">Tango Current Balance</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.tangoCurrentBalance}</dd>
            <dt>
              <span id="tangoThresholdBalance">
                <Translate contentKey="programserviceApp.clientSetting.tangoThresholdBalance">Tango Threshold Balance</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.tangoThresholdBalance}</dd>
            <dt>
              <span id="tangoCreditToken">
                <Translate contentKey="programserviceApp.clientSetting.tangoCreditToken">Tango Credit Token</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.tangoCreditToken}</dd>
            <dt>
              <span id="updatedDate">
                <Translate contentKey="programserviceApp.clientSetting.updatedDate">Updated Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={clientSettingEntity.updatedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="updatedBy">
                <Translate contentKey="programserviceApp.clientSetting.updatedBy">Updated By</Translate>
              </span>
            </dt>
            <dd>{clientSettingEntity.updatedBy}</dd>
          </dl>
          <Button tag={Link} to="/entity/client-setting" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/client-setting/${clientSettingEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ clientSetting }: IRootState) => ({
  clientSettingEntity: clientSetting.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientSettingDetail);
