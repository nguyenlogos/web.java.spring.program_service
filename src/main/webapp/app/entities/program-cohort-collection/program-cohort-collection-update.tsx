import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramCollection } from 'app/shared/model/program-collection.model';
import { getEntities as getProgramCollections } from 'app/entities/program-collection/program-collection.reducer';
import { IProgramCohort } from 'app/shared/model/program-cohort.model';
import { getEntities as getProgramCohorts } from 'app/entities/program-cohort/program-cohort.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-cohort-collection.reducer';
import { IProgramCohortCollection } from 'app/shared/model/program-cohort-collection.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramCohortCollectionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramCohortCollectionUpdateState {
  isNew: boolean;
  programCollectionId: string;
  programCohortId: string;
}

export class ProgramCohortCollectionUpdate extends React.Component<
  IProgramCohortCollectionUpdateProps,
  IProgramCohortCollectionUpdateState
> {
  constructor(props) {
    super(props);
    this.state = {
      programCollectionId: '0',
      programCohortId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramCollections();
    this.props.getProgramCohorts();
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programCohortCollectionEntity } = this.props;
      const entity = {
        ...programCohortCollectionEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-cohort-collection');
  };

  render() {
    const { programCohortCollectionEntity, programCollections, programCohorts, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programCohortCollection.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programCohortCollection.home.createOrEditLabel">
                Create or edit a ProgramCohortCollection
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programCohortCollectionEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-cohort-collection-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-cohort-collection-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="requiredCompletionLabel" for="program-cohort-collection-requiredCompletion">
                    <Translate contentKey="programserviceApp.programCohortCollection.requiredCompletion">Required Completion</Translate>
                  </Label>
                  <AvField
                    id="program-cohort-collection-requiredCompletion"
                    type="string"
                    className="form-control"
                    name="requiredCompletion"
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="requiredLevelLabel" for="program-cohort-collection-requiredLevel">
                    <Translate contentKey="programserviceApp.programCohortCollection.requiredLevel">Required Level</Translate>
                  </Label>
                  <AvField id="program-cohort-collection-requiredLevel" type="string" className="form-control" name="requiredLevel" />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-cohort-collection-createdDate">
                    <Translate contentKey="programserviceApp.programCohortCollection.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-collection-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCohortCollectionEntity.createdDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-cohort-collection-modifiedDate">
                    <Translate contentKey="programserviceApp.programCohortCollection.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-collection-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCohortCollectionEntity.modifiedDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-cohort-collection-programCollection">
                    <Translate contentKey="programserviceApp.programCohortCollection.programCollection">Program Collection</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-collection-programCollection"
                    type="select"
                    className="form-control"
                    name="programCollectionId"
                  >
                    <option value="" key="0" />
                    {programCollections
                      ? programCollections.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="program-cohort-collection-programCohort">
                    <Translate contentKey="programserviceApp.programCohortCollection.programCohort">Program Cohort</Translate>
                  </Label>
                  <AvInput id="program-cohort-collection-programCohort" type="select" className="form-control" name="programCohortId">
                    <option value="" key="0" />
                    {programCohorts
                      ? programCohorts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-cohort-collection" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programCollections: storeState.programCollection.entities,
  programCohorts: storeState.programCohort.entities,
  programCohortCollectionEntity: storeState.programCohortCollection.entity,
  loading: storeState.programCohortCollection.loading,
  updating: storeState.programCohortCollection.updating,
  updateSuccess: storeState.programCohortCollection.updateSuccess
});

const mapDispatchToProps = {
  getProgramCollections,
  getProgramCohorts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortCollectionUpdate);
