import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramCohortCollection from './program-cohort-collection';
import ProgramCohortCollectionDetail from './program-cohort-collection-detail';
import ProgramCohortCollectionUpdate from './program-cohort-collection-update';
import ProgramCohortCollectionDeleteDialog from './program-cohort-collection-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramCohortCollectionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramCohortCollectionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramCohortCollectionDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramCohortCollection} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramCohortCollectionDeleteDialog} />
  </>
);

export default Routes;
