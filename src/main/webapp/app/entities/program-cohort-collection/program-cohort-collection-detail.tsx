import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-cohort-collection.reducer';
import { IProgramCohortCollection } from 'app/shared/model/program-cohort-collection.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCohortCollectionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramCohortCollectionDetail extends React.Component<IProgramCohortCollectionDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programCohortCollectionEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programCohortCollection.detail.title">ProgramCohortCollection</Translate> [
            <b>{programCohortCollectionEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="requiredCompletion">
                <Translate contentKey="programserviceApp.programCohortCollection.requiredCompletion">Required Completion</Translate>
              </span>
            </dt>
            <dd>{programCohortCollectionEntity.requiredCompletion}</dd>
            <dt>
              <span id="requiredLevel">
                <Translate contentKey="programserviceApp.programCohortCollection.requiredLevel">Required Level</Translate>
              </span>
            </dt>
            <dd>{programCohortCollectionEntity.requiredLevel}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programCohortCollection.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCohortCollectionEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programCohortCollection.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCohortCollectionEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programCohortCollection.programCollection">Program Collection</Translate>
            </dt>
            <dd>{programCohortCollectionEntity.programCollectionId ? programCohortCollectionEntity.programCollectionId : ''}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programCohortCollection.programCohort">Program Cohort</Translate>
            </dt>
            <dd>{programCohortCollectionEntity.programCohortId ? programCohortCollectionEntity.programCohortId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-cohort-collection" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-cohort-collection/${programCohortCollectionEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programCohortCollection }: IRootState) => ({
  programCohortCollectionEntity: programCohortCollection.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortCollectionDetail);
