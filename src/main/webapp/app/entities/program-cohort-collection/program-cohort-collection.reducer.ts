import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramCohortCollection, defaultValue } from 'app/shared/model/program-cohort-collection.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMCOHORTCOLLECTION_LIST: 'programCohortCollection/FETCH_PROGRAMCOHORTCOLLECTION_LIST',
  FETCH_PROGRAMCOHORTCOLLECTION: 'programCohortCollection/FETCH_PROGRAMCOHORTCOLLECTION',
  CREATE_PROGRAMCOHORTCOLLECTION: 'programCohortCollection/CREATE_PROGRAMCOHORTCOLLECTION',
  UPDATE_PROGRAMCOHORTCOLLECTION: 'programCohortCollection/UPDATE_PROGRAMCOHORTCOLLECTION',
  DELETE_PROGRAMCOHORTCOLLECTION: 'programCohortCollection/DELETE_PROGRAMCOHORTCOLLECTION',
  RESET: 'programCohortCollection/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramCohortCollection>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramCohortCollectionState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramCohortCollectionState = initialState, action): ProgramCohortCollectionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOHORTCOLLECTION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOHORTCOLLECTION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMCOHORTCOLLECTION):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMCOHORTCOLLECTION):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMCOHORTCOLLECTION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOHORTCOLLECTION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOHORTCOLLECTION):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMCOHORTCOLLECTION):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMCOHORTCOLLECTION):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMCOHORTCOLLECTION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOHORTCOLLECTION_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOHORTCOLLECTION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMCOHORTCOLLECTION):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMCOHORTCOLLECTION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMCOHORTCOLLECTION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-cohort-collections';

// Actions

export const getEntities: ICrudGetAllAction<IProgramCohortCollection> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOHORTCOLLECTION_LIST,
    payload: axios.get<IProgramCohortCollection>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramCohortCollection> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOHORTCOLLECTION,
    payload: axios.get<IProgramCohortCollection>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramCohortCollection> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMCOHORTCOLLECTION,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramCohortCollection> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMCOHORTCOLLECTION,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramCohortCollection> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMCOHORTCOLLECTION,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
