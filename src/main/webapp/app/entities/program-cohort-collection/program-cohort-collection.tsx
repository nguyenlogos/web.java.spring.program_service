import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './program-cohort-collection.reducer';
import { IProgramCohortCollection } from 'app/shared/model/program-cohort-collection.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramCohortCollectionProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramCohortCollectionState = IPaginationBaseState;

export class ProgramCohortCollection extends React.Component<IProgramCohortCollectionProps, IProgramCohortCollectionState> {
  state: IProgramCohortCollectionState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  componentDidUpdate() {
    if (this.props.updateSuccess) {
      this.reset();
    }
  }

  reset = () => {
    this.props.reset();
    this.setState({ activePage: 1 }, () => {
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => {
        this.reset();
      }
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programCohortCollectionList, match } = this.props;
    return (
      <div>
        <h2 id="program-cohort-collection-heading">
          <Translate contentKey="programserviceApp.programCohortCollection.home.title">Program Cohort Collections</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.programCohortCollection.home.createLabel">
              Create new Program Cohort Collection
            </Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage - 1 < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('requiredCompletion')}>
                    <Translate contentKey="programserviceApp.programCohortCollection.requiredCompletion">Required Completion</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('requiredLevel')}>
                    <Translate contentKey="programserviceApp.programCohortCollection.requiredLevel">Required Level</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('createdDate')}>
                    <Translate contentKey="programserviceApp.programCohortCollection.createdDate">Created Date</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('modifiedDate')}>
                    <Translate contentKey="programserviceApp.programCohortCollection.modifiedDate">Modified Date</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="programserviceApp.programCohortCollection.programCollection">Program Collection</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="programserviceApp.programCohortCollection.programCohort">Program Cohort</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {programCohortCollectionList.map((programCohortCollection, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${programCohortCollection.id}`} color="link" size="sm">
                        {programCohortCollection.id}
                      </Button>
                    </td>
                    <td>{programCohortCollection.requiredCompletion}</td>
                    <td>{programCohortCollection.requiredLevel}</td>
                    <td>
                      <TextFormat type="date" value={programCohortCollection.createdDate} format={APP_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={programCohortCollection.modifiedDate} format={APP_DATE_FORMAT} />
                    </td>
                    <td>
                      {programCohortCollection.programCollectionId ? (
                        <Link to={`program-collection/${programCohortCollection.programCollectionId}`}>
                          {programCohortCollection.programCollectionId}
                        </Link>
                      ) : (
                        ''
                      )}
                    </td>
                    <td>
                      {programCohortCollection.programCohortId ? (
                        <Link to={`program-cohort/${programCohortCollection.programCohortId}`}>
                          {programCohortCollection.programCohortId}
                        </Link>
                      ) : (
                        ''
                      )}
                    </td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${programCohortCollection.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${programCohortCollection.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${programCohortCollection.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ programCohortCollection }: IRootState) => ({
  programCohortCollectionList: programCohortCollection.entities,
  totalItems: programCohortCollection.totalItems,
  links: programCohortCollection.links,
  entity: programCohortCollection.entity,
  updateSuccess: programCohortCollection.updateSuccess
});

const mapDispatchToProps = {
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortCollection);
