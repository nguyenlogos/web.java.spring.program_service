import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramLevel } from 'app/shared/model/program-level.model';
import { getEntities as getProgramLevels } from 'app/entities/program-level/program-level.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-level-reward.reducer';
import { IProgramLevelReward } from 'app/shared/model/program-level-reward.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramLevelRewardUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramLevelRewardUpdateState {
  isNew: boolean;
  programLevelId: string;
}

export class ProgramLevelRewardUpdate extends React.Component<IProgramLevelRewardUpdateProps, IProgramLevelRewardUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programLevelId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramLevels();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programLevelRewardEntity } = this.props;
      const entity = {
        ...programLevelRewardEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-level-reward');
  };

  render() {
    const { programLevelRewardEntity, programLevels, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programLevelReward.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programLevelReward.home.createOrEditLabel">
                Create or edit a ProgramLevelReward
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programLevelRewardEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-level-reward-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-level-reward-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="descriptionLabel" for="program-level-reward-description">
                    <Translate contentKey="programserviceApp.programLevelReward.description">Description</Translate>
                  </Label>
                  <AvField id="program-level-reward-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="quantityLabel" for="program-level-reward-quantity">
                    <Translate contentKey="programserviceApp.programLevelReward.quantity">Quantity</Translate>
                  </Label>
                  <AvField
                    id="program-level-reward-quantity"
                    type="string"
                    className="form-control"
                    name="quantity"
                    validate={{
                      min: { value: 1, errorMessage: translate('entity.validation.min', { min: 1 }) },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="codeLabel" for="program-level-reward-code">
                    <Translate contentKey="programserviceApp.programLevelReward.code">Code</Translate>
                  </Label>
                  <AvField
                    id="program-level-reward-code"
                    type="text"
                    name="code"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="rewardAmountLabel" for="program-level-reward-rewardAmount">
                    <Translate contentKey="programserviceApp.programLevelReward.rewardAmount">Reward Amount</Translate>
                  </Label>
                  <AvField
                    id="program-level-reward-rewardAmount"
                    type="text"
                    name="rewardAmount"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      min: { value: 1, errorMessage: translate('entity.validation.min', { min: 1 }) },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="rewardTypeLabel" for="program-level-reward-rewardType">
                    <Translate contentKey="programserviceApp.programLevelReward.rewardType">Reward Type</Translate>
                  </Label>
                  <AvField
                    id="program-level-reward-rewardType"
                    type="text"
                    name="rewardType"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="campaignIdLabel" for="program-level-reward-campaignId">
                    <Translate contentKey="programserviceApp.programLevelReward.campaignId">Campaign Id</Translate>
                  </Label>
                  <AvField id="program-level-reward-campaignId" type="text" name="campaignId" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="program-level-reward-subgroupId">
                    <Translate contentKey="programserviceApp.programLevelReward.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="program-level-reward-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupNameLabel" for="program-level-reward-subgroupName">
                    <Translate contentKey="programserviceApp.programLevelReward.subgroupName">Subgroup Name</Translate>
                  </Label>
                  <AvField id="program-level-reward-subgroupName" type="text" name="subgroupName" />
                </AvGroup>
                <AvGroup>
                  <Label for="program-level-reward-programLevel">
                    <Translate contentKey="programserviceApp.programLevelReward.programLevel">Program Level</Translate>
                  </Label>
                  <AvInput id="program-level-reward-programLevel" type="select" className="form-control" name="programLevelId" required>
                    {programLevels
                      ? programLevels.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-level-reward" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programLevels: storeState.programLevel.entities,
  programLevelRewardEntity: storeState.programLevelReward.entity,
  loading: storeState.programLevelReward.loading,
  updating: storeState.programLevelReward.updating,
  updateSuccess: storeState.programLevelReward.updateSuccess
});

const mapDispatchToProps = {
  getProgramLevels,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelRewardUpdate);
