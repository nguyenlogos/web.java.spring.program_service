import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramLevelReward, defaultValue } from 'app/shared/model/program-level-reward.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMLEVELREWARD_LIST: 'programLevelReward/FETCH_PROGRAMLEVELREWARD_LIST',
  FETCH_PROGRAMLEVELREWARD: 'programLevelReward/FETCH_PROGRAMLEVELREWARD',
  CREATE_PROGRAMLEVELREWARD: 'programLevelReward/CREATE_PROGRAMLEVELREWARD',
  UPDATE_PROGRAMLEVELREWARD: 'programLevelReward/UPDATE_PROGRAMLEVELREWARD',
  DELETE_PROGRAMLEVELREWARD: 'programLevelReward/DELETE_PROGRAMLEVELREWARD',
  RESET: 'programLevelReward/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramLevelReward>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramLevelRewardState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramLevelRewardState = initialState, action): ProgramLevelRewardState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVELREWARD_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVELREWARD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMLEVELREWARD):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMLEVELREWARD):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMLEVELREWARD):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVELREWARD_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVELREWARD):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMLEVELREWARD):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMLEVELREWARD):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMLEVELREWARD):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVELREWARD_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVELREWARD):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMLEVELREWARD):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMLEVELREWARD):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMLEVELREWARD):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-level-rewards';

// Actions

export const getEntities: ICrudGetAllAction<IProgramLevelReward> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVELREWARD_LIST,
    payload: axios.get<IProgramLevelReward>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramLevelReward> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVELREWARD,
    payload: axios.get<IProgramLevelReward>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramLevelReward> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMLEVELREWARD,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramLevelReward> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMLEVELREWARD,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramLevelReward> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMLEVELREWARD,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
