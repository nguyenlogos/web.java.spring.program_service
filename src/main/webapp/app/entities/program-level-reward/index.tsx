import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramLevelReward from './program-level-reward';
import ProgramLevelRewardDetail from './program-level-reward-detail';
import ProgramLevelRewardUpdate from './program-level-reward-update';
import ProgramLevelRewardDeleteDialog from './program-level-reward-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramLevelRewardUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramLevelRewardUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramLevelRewardDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramLevelReward} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramLevelRewardDeleteDialog} />
  </>
);

export default Routes;
