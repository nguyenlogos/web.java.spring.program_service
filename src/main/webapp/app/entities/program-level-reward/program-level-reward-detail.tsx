import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-level-reward.reducer';
import { IProgramLevelReward } from 'app/shared/model/program-level-reward.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramLevelRewardDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramLevelRewardDetail extends React.Component<IProgramLevelRewardDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programLevelRewardEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programLevelReward.detail.title">ProgramLevelReward</Translate> [
            <b>{programLevelRewardEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="description">
                <Translate contentKey="programserviceApp.programLevelReward.description">Description</Translate>
              </span>
            </dt>
            <dd>{programLevelRewardEntity.description}</dd>
            <dt>
              <span id="quantity">
                <Translate contentKey="programserviceApp.programLevelReward.quantity">Quantity</Translate>
              </span>
            </dt>
            <dd>{programLevelRewardEntity.quantity}</dd>
            <dt>
              <span id="code">
                <Translate contentKey="programserviceApp.programLevelReward.code">Code</Translate>
              </span>
            </dt>
            <dd>{programLevelRewardEntity.code}</dd>
            <dt>
              <span id="rewardAmount">
                <Translate contentKey="programserviceApp.programLevelReward.rewardAmount">Reward Amount</Translate>
              </span>
            </dt>
            <dd>{programLevelRewardEntity.rewardAmount}</dd>
            <dt>
              <span id="rewardType">
                <Translate contentKey="programserviceApp.programLevelReward.rewardType">Reward Type</Translate>
              </span>
            </dt>
            <dd>{programLevelRewardEntity.rewardType}</dd>
            <dt>
              <span id="campaignId">
                <Translate contentKey="programserviceApp.programLevelReward.campaignId">Campaign Id</Translate>
              </span>
            </dt>
            <dd>{programLevelRewardEntity.campaignId}</dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.programLevelReward.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{programLevelRewardEntity.subgroupId}</dd>
            <dt>
              <span id="subgroupName">
                <Translate contentKey="programserviceApp.programLevelReward.subgroupName">Subgroup Name</Translate>
              </span>
            </dt>
            <dd>{programLevelRewardEntity.subgroupName}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programLevelReward.programLevel">Program Level</Translate>
            </dt>
            <dd>{programLevelRewardEntity.programLevelName ? programLevelRewardEntity.programLevelName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-level-reward" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-level-reward/${programLevelRewardEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programLevelReward }: IRootState) => ({
  programLevelRewardEntity: programLevelReward.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelRewardDetail);
