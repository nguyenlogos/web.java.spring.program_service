import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramUserCollectionProgress from './program-user-collection-progress';
import ProgramUserCollectionProgressDetail from './program-user-collection-progress-detail';
import ProgramUserCollectionProgressUpdate from './program-user-collection-progress-update';
import ProgramUserCollectionProgressDeleteDialog from './program-user-collection-progress-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramUserCollectionProgressUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramUserCollectionProgressUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramUserCollectionProgressDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramUserCollectionProgress} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramUserCollectionProgressDeleteDialog} />
  </>
);

export default Routes;
