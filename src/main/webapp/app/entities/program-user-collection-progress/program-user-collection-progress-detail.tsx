import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-user-collection-progress.reducer';
import { IProgramUserCollectionProgress } from 'app/shared/model/program-user-collection-progress.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramUserCollectionProgressDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramUserCollectionProgressDetail extends React.Component<IProgramUserCollectionProgressDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programUserCollectionProgressEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programUserCollectionProgress.detail.title">ProgramUserCollectionProgress</Translate> [
            <b>{programUserCollectionProgressEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="collectionId">
                <Translate contentKey="programserviceApp.programUserCollectionProgress.collectionId">Collection Id</Translate>
              </span>
            </dt>
            <dd>{programUserCollectionProgressEntity.collectionId}</dd>
            <dt>
              <span id="totalRequiredItems">
                <Translate contentKey="programserviceApp.programUserCollectionProgress.totalRequiredItems">Total Required Items</Translate>
              </span>
            </dt>
            <dd>{programUserCollectionProgressEntity.totalRequiredItems}</dd>
            <dt>
              <span id="currentProgress">
                <Translate contentKey="programserviceApp.programUserCollectionProgress.currentProgress">Current Progress</Translate>
              </span>
            </dt>
            <dd>{programUserCollectionProgressEntity.currentProgress}</dd>
            <dt>
              <span id="status">
                <Translate contentKey="programserviceApp.programUserCollectionProgress.status">Status</Translate>
              </span>
            </dt>
            <dd>{programUserCollectionProgressEntity.status}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programUserCollectionProgress.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programUserCollectionProgressEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programUserCollectionProgress.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programUserCollectionProgressEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programUserCollectionProgress.programUserCohort">Program User Cohort</Translate>
            </dt>
            <dd>
              {programUserCollectionProgressEntity.programUserCohortId ? programUserCollectionProgressEntity.programUserCohortId : ''}
            </dd>
          </dl>
          <Button tag={Link} to="/entity/program-user-collection-progress" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button
            tag={Link}
            to={`/entity/program-user-collection-progress/${programUserCollectionProgressEntity.id}/edit`}
            replace
            color="primary"
          >
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programUserCollectionProgress }: IRootState) => ({
  programUserCollectionProgressEntity: programUserCollectionProgress.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserCollectionProgressDetail);
