import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramUserCohort } from 'app/shared/model/program-user-cohort.model';
import { getEntities as getProgramUserCohorts } from 'app/entities/program-user-cohort/program-user-cohort.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-user-collection-progress.reducer';
import { IProgramUserCollectionProgress } from 'app/shared/model/program-user-collection-progress.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramUserCollectionProgressUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramUserCollectionProgressUpdateState {
  isNew: boolean;
  programUserCohortId: string;
}

export class ProgramUserCollectionProgressUpdate extends React.Component<
  IProgramUserCollectionProgressUpdateProps,
  IProgramUserCollectionProgressUpdateState
> {
  constructor(props) {
    super(props);
    this.state = {
      programUserCohortId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramUserCohorts();
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programUserCollectionProgressEntity } = this.props;
      const entity = {
        ...programUserCollectionProgressEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-user-collection-progress');
  };

  render() {
    const { programUserCollectionProgressEntity, programUserCohorts, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programUserCollectionProgress.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programUserCollectionProgress.home.createOrEditLabel">
                Create or edit a ProgramUserCollectionProgress
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programUserCollectionProgressEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-user-collection-progress-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-user-collection-progress-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="collectionIdLabel" for="program-user-collection-progress-collectionId">
                    <Translate contentKey="programserviceApp.programUserCollectionProgress.collectionId">Collection Id</Translate>
                  </Label>
                  <AvField
                    id="program-user-collection-progress-collectionId"
                    type="string"
                    className="form-control"
                    name="collectionId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="totalRequiredItemsLabel" for="program-user-collection-progress-totalRequiredItems">
                    <Translate contentKey="programserviceApp.programUserCollectionProgress.totalRequiredItems">
                      Total Required Items
                    </Translate>
                  </Label>
                  <AvField
                    id="program-user-collection-progress-totalRequiredItems"
                    type="string"
                    className="form-control"
                    name="totalRequiredItems"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="currentProgressLabel" for="program-user-collection-progress-currentProgress">
                    <Translate contentKey="programserviceApp.programUserCollectionProgress.currentProgress">Current Progress</Translate>
                  </Label>
                  <AvField
                    id="program-user-collection-progress-currentProgress"
                    type="string"
                    className="form-control"
                    name="currentProgress"
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="statusLabel" for="program-user-collection-progress-status">
                    <Translate contentKey="programserviceApp.programUserCollectionProgress.status">Status</Translate>
                  </Label>
                  <AvInput
                    id="program-user-collection-progress-status"
                    type="select"
                    className="form-control"
                    name="status"
                    value={(!isNew && programUserCollectionProgressEntity.status) || 'DOING'}
                  >
                    <option value="DOING">
                      <Translate contentKey="programserviceApp.CollectionStatus.DOING" />
                    </option>
                    <option value="DONE">
                      <Translate contentKey="programserviceApp.CollectionStatus.DONE" />
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-user-collection-progress-createdDate">
                    <Translate contentKey="programserviceApp.programUserCollectionProgress.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-user-collection-progress-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programUserCollectionProgressEntity.createdDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-user-collection-progress-modifiedDate">
                    <Translate contentKey="programserviceApp.programUserCollectionProgress.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-user-collection-progress-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programUserCollectionProgressEntity.modifiedDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-user-collection-progress-programUserCohort">
                    <Translate contentKey="programserviceApp.programUserCollectionProgress.programUserCohort">
                      Program User Cohort
                    </Translate>
                  </Label>
                  <AvInput
                    id="program-user-collection-progress-programUserCohort"
                    type="select"
                    className="form-control"
                    name="programUserCohortId"
                    required
                  >
                    {programUserCohorts
                      ? programUserCohorts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-user-collection-progress" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programUserCohorts: storeState.programUserCohort.entities,
  programUserCollectionProgressEntity: storeState.programUserCollectionProgress.entity,
  loading: storeState.programUserCollectionProgress.loading,
  updating: storeState.programUserCollectionProgress.updating,
  updateSuccess: storeState.programUserCollectionProgress.updateSuccess
});

const mapDispatchToProps = {
  getProgramUserCohorts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserCollectionProgressUpdate);
