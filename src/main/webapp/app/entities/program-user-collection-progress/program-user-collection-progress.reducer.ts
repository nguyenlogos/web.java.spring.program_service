import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramUserCollectionProgress, defaultValue } from 'app/shared/model/program-user-collection-progress.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMUSERCOLLECTIONPROGRESS_LIST: 'programUserCollectionProgress/FETCH_PROGRAMUSERCOLLECTIONPROGRESS_LIST',
  FETCH_PROGRAMUSERCOLLECTIONPROGRESS: 'programUserCollectionProgress/FETCH_PROGRAMUSERCOLLECTIONPROGRESS',
  CREATE_PROGRAMUSERCOLLECTIONPROGRESS: 'programUserCollectionProgress/CREATE_PROGRAMUSERCOLLECTIONPROGRESS',
  UPDATE_PROGRAMUSERCOLLECTIONPROGRESS: 'programUserCollectionProgress/UPDATE_PROGRAMUSERCOLLECTIONPROGRESS',
  DELETE_PROGRAMUSERCOLLECTIONPROGRESS: 'programUserCollectionProgress/DELETE_PROGRAMUSERCOLLECTIONPROGRESS',
  RESET: 'programUserCollectionProgress/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramUserCollectionProgress>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramUserCollectionProgressState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramUserCollectionProgressState = initialState, action): ProgramUserCollectionProgressState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMUSERCOLLECTIONPROGRESS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMUSERCOLLECTIONPROGRESS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMUSERCOLLECTIONPROGRESS):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMUSERCOLLECTIONPROGRESS):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMUSERCOLLECTIONPROGRESS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMUSERCOLLECTIONPROGRESS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMUSERCOLLECTIONPROGRESS):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMUSERCOLLECTIONPROGRESS):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMUSERCOLLECTIONPROGRESS):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMUSERCOLLECTIONPROGRESS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMUSERCOLLECTIONPROGRESS_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMUSERCOLLECTIONPROGRESS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMUSERCOLLECTIONPROGRESS):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMUSERCOLLECTIONPROGRESS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMUSERCOLLECTIONPROGRESS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-user-collection-progresses';

// Actions

export const getEntities: ICrudGetAllAction<IProgramUserCollectionProgress> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMUSERCOLLECTIONPROGRESS_LIST,
    payload: axios.get<IProgramUserCollectionProgress>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramUserCollectionProgress> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMUSERCOLLECTIONPROGRESS,
    payload: axios.get<IProgramUserCollectionProgress>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramUserCollectionProgress> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMUSERCOLLECTIONPROGRESS,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramUserCollectionProgress> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMUSERCOLLECTIONPROGRESS,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramUserCollectionProgress> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMUSERCOLLECTIONPROGRESS,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
