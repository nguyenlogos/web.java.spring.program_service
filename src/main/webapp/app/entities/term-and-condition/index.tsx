import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import TermAndCondition from './term-and-condition';
import TermAndConditionDetail from './term-and-condition-detail';
import TermAndConditionUpdate from './term-and-condition-update';
import TermAndConditionDeleteDialog from './term-and-condition-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={TermAndConditionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={TermAndConditionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={TermAndConditionDetail} />
      <ErrorBoundaryRoute path={match.url} component={TermAndCondition} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={TermAndConditionDeleteDialog} />
  </>
);

export default Routes;
