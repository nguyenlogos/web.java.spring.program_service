import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './term-and-condition.reducer';
import { ITermAndCondition } from 'app/shared/model/term-and-condition.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITermAndConditionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ITermAndConditionUpdateState {
  isNew: boolean;
}

export class TermAndConditionUpdate extends React.Component<ITermAndConditionUpdateProps, ITermAndConditionUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { termAndConditionEntity } = this.props;
      const entity = {
        ...termAndConditionEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/term-and-condition');
  };

  render() {
    const { termAndConditionEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.termAndCondition.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.termAndCondition.home.createOrEditLabel">
                Create or edit a TermAndCondition
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : termAndConditionEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="term-and-condition-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="term-and-condition-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="clientIdLabel" for="term-and-condition-clientId">
                    <Translate contentKey="programserviceApp.termAndCondition.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="term-and-condition-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="term-and-condition-subgroupId">
                    <Translate contentKey="programserviceApp.termAndCondition.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="term-and-condition-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="titleLabel" for="term-and-condition-title">
                    <Translate contentKey="programserviceApp.termAndCondition.title">Title</Translate>
                  </Label>
                  <AvField id="term-and-condition-title" type="text" name="title" />
                </AvGroup>
                <AvGroup>
                  <Label id="contentLabel" for="term-and-condition-content">
                    <Translate contentKey="programserviceApp.termAndCondition.content">Content</Translate>
                  </Label>
                  <AvField id="term-and-condition-content" type="text" name="content" />
                </AvGroup>
                <AvGroup>
                  <Label id="isTargetSubgroupLabel" check>
                    <AvInput id="term-and-condition-isTargetSubgroup" type="checkbox" className="form-control" name="isTargetSubgroup" />
                    <Translate contentKey="programserviceApp.termAndCondition.isTargetSubgroup">Is Target Subgroup</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupNameLabel" for="term-and-condition-subgroupName">
                    <Translate contentKey="programserviceApp.termAndCondition.subgroupName">Subgroup Name</Translate>
                  </Label>
                  <AvField id="term-and-condition-subgroupName" type="text" name="subgroupName" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/term-and-condition" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  termAndConditionEntity: storeState.termAndCondition.entity,
  loading: storeState.termAndCondition.loading,
  updating: storeState.termAndCondition.updating,
  updateSuccess: storeState.termAndCondition.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TermAndConditionUpdate);
