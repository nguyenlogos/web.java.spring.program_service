import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './term-and-condition.reducer';
import { ITermAndCondition } from 'app/shared/model/term-and-condition.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITermAndConditionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class TermAndConditionDetail extends React.Component<ITermAndConditionDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { termAndConditionEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.termAndCondition.detail.title">TermAndCondition</Translate> [
            <b>{termAndConditionEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.termAndCondition.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{termAndConditionEntity.clientId}</dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.termAndCondition.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{termAndConditionEntity.subgroupId}</dd>
            <dt>
              <span id="title">
                <Translate contentKey="programserviceApp.termAndCondition.title">Title</Translate>
              </span>
            </dt>
            <dd>{termAndConditionEntity.title}</dd>
            <dt>
              <span id="content">
                <Translate contentKey="programserviceApp.termAndCondition.content">Content</Translate>
              </span>
            </dt>
            <dd>{termAndConditionEntity.content}</dd>
            <dt>
              <span id="isTargetSubgroup">
                <Translate contentKey="programserviceApp.termAndCondition.isTargetSubgroup">Is Target Subgroup</Translate>
              </span>
            </dt>
            <dd>{termAndConditionEntity.isTargetSubgroup ? 'true' : 'false'}</dd>
            <dt>
              <span id="subgroupName">
                <Translate contentKey="programserviceApp.termAndCondition.subgroupName">Subgroup Name</Translate>
              </span>
            </dt>
            <dd>{termAndConditionEntity.subgroupName}</dd>
          </dl>
          <Button tag={Link} to="/entity/term-and-condition" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/term-and-condition/${termAndConditionEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ termAndCondition }: IRootState) => ({
  termAndConditionEntity: termAndCondition.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TermAndConditionDetail);
