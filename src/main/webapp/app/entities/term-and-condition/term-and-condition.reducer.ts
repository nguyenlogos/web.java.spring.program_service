import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ITermAndCondition, defaultValue } from 'app/shared/model/term-and-condition.model';

export const ACTION_TYPES = {
  FETCH_TERMANDCONDITION_LIST: 'termAndCondition/FETCH_TERMANDCONDITION_LIST',
  FETCH_TERMANDCONDITION: 'termAndCondition/FETCH_TERMANDCONDITION',
  CREATE_TERMANDCONDITION: 'termAndCondition/CREATE_TERMANDCONDITION',
  UPDATE_TERMANDCONDITION: 'termAndCondition/UPDATE_TERMANDCONDITION',
  DELETE_TERMANDCONDITION: 'termAndCondition/DELETE_TERMANDCONDITION',
  RESET: 'termAndCondition/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ITermAndCondition>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type TermAndConditionState = Readonly<typeof initialState>;

// Reducer

export default (state: TermAndConditionState = initialState, action): TermAndConditionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_TERMANDCONDITION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_TERMANDCONDITION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_TERMANDCONDITION):
    case REQUEST(ACTION_TYPES.UPDATE_TERMANDCONDITION):
    case REQUEST(ACTION_TYPES.DELETE_TERMANDCONDITION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_TERMANDCONDITION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_TERMANDCONDITION):
    case FAILURE(ACTION_TYPES.CREATE_TERMANDCONDITION):
    case FAILURE(ACTION_TYPES.UPDATE_TERMANDCONDITION):
    case FAILURE(ACTION_TYPES.DELETE_TERMANDCONDITION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_TERMANDCONDITION_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_TERMANDCONDITION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_TERMANDCONDITION):
    case SUCCESS(ACTION_TYPES.UPDATE_TERMANDCONDITION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_TERMANDCONDITION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/term-and-conditions';

// Actions

export const getEntities: ICrudGetAllAction<ITermAndCondition> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_TERMANDCONDITION_LIST,
    payload: axios.get<ITermAndCondition>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<ITermAndCondition> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TERMANDCONDITION,
    payload: axios.get<ITermAndCondition>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<ITermAndCondition> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TERMANDCONDITION,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ITermAndCondition> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TERMANDCONDITION,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<ITermAndCondition> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TERMANDCONDITION,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
