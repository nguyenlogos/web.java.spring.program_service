import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramBiometricData from './program-biometric-data';
import ProgramBiometricDataDetail from './program-biometric-data-detail';
import ProgramBiometricDataUpdate from './program-biometric-data-update';
import ProgramBiometricDataDeleteDialog from './program-biometric-data-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramBiometricDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramBiometricDataUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramBiometricDataDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramBiometricData} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramBiometricDataDeleteDialog} />
  </>
);

export default Routes;
