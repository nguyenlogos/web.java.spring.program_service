import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-biometric-data.reducer';
import { IProgramBiometricData } from 'app/shared/model/program-biometric-data.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramBiometricDataDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramBiometricDataDetail extends React.Component<IProgramBiometricDataDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programBiometricDataEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programBiometricData.detail.title">ProgramBiometricData</Translate> [
            <b>{programBiometricDataEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="biometricCode">
                <Translate contentKey="programserviceApp.programBiometricData.biometricCode">Biometric Code</Translate>
              </span>
            </dt>
            <dd>{programBiometricDataEntity.biometricCode}</dd>
            <dt>
              <span id="biometricName">
                <Translate contentKey="programserviceApp.programBiometricData.biometricName">Biometric Name</Translate>
              </span>
            </dt>
            <dd>{programBiometricDataEntity.biometricName}</dd>
            <dt>
              <span id="maleMin">
                <Translate contentKey="programserviceApp.programBiometricData.maleMin">Male Min</Translate>
              </span>
            </dt>
            <dd>{programBiometricDataEntity.maleMin}</dd>
            <dt>
              <span id="maleMax">
                <Translate contentKey="programserviceApp.programBiometricData.maleMax">Male Max</Translate>
              </span>
            </dt>
            <dd>{programBiometricDataEntity.maleMax}</dd>
            <dt>
              <span id="femaleMin">
                <Translate contentKey="programserviceApp.programBiometricData.femaleMin">Female Min</Translate>
              </span>
            </dt>
            <dd>{programBiometricDataEntity.femaleMin}</dd>
            <dt>
              <span id="femaleMax">
                <Translate contentKey="programserviceApp.programBiometricData.femaleMax">Female Max</Translate>
              </span>
            </dt>
            <dd>{programBiometricDataEntity.femaleMax}</dd>
            <dt>
              <span id="unidentifiedMin">
                <Translate contentKey="programserviceApp.programBiometricData.unidentifiedMin">Unidentified Min</Translate>
              </span>
            </dt>
            <dd>{programBiometricDataEntity.unidentifiedMin}</dd>
            <dt>
              <span id="unidentifiedMax">
                <Translate contentKey="programserviceApp.programBiometricData.unidentifiedMax">Unidentified Max</Translate>
              </span>
            </dt>
            <dd>{programBiometricDataEntity.unidentifiedMax}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-biometric-data" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-biometric-data/${programBiometricDataEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programBiometricData }: IRootState) => ({
  programBiometricDataEntity: programBiometricData.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramBiometricDataDetail);
