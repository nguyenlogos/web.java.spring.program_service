import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './program-biometric-data.reducer';
import { IProgramBiometricData } from 'app/shared/model/program-biometric-data.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramBiometricDataUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramBiometricDataUpdateState {
  isNew: boolean;
}

export class ProgramBiometricDataUpdate extends React.Component<IProgramBiometricDataUpdateProps, IProgramBiometricDataUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programBiometricDataEntity } = this.props;
      const entity = {
        ...programBiometricDataEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-biometric-data');
  };

  render() {
    const { programBiometricDataEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programBiometricData.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programBiometricData.home.createOrEditLabel">
                Create or edit a ProgramBiometricData
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programBiometricDataEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-biometric-data-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-biometric-data-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="biometricCodeLabel" for="program-biometric-data-biometricCode">
                    <Translate contentKey="programserviceApp.programBiometricData.biometricCode">Biometric Code</Translate>
                  </Label>
                  <AvField id="program-biometric-data-biometricCode" type="text" name="biometricCode" />
                </AvGroup>
                <AvGroup>
                  <Label id="biometricNameLabel" for="program-biometric-data-biometricName">
                    <Translate contentKey="programserviceApp.programBiometricData.biometricName">Biometric Name</Translate>
                  </Label>
                  <AvField id="program-biometric-data-biometricName" type="text" name="biometricName" />
                </AvGroup>
                <AvGroup>
                  <Label id="maleMinLabel" for="program-biometric-data-maleMin">
                    <Translate contentKey="programserviceApp.programBiometricData.maleMin">Male Min</Translate>
                  </Label>
                  <AvField id="program-biometric-data-maleMin" type="string" className="form-control" name="maleMin" />
                </AvGroup>
                <AvGroup>
                  <Label id="maleMaxLabel" for="program-biometric-data-maleMax">
                    <Translate contentKey="programserviceApp.programBiometricData.maleMax">Male Max</Translate>
                  </Label>
                  <AvField id="program-biometric-data-maleMax" type="string" className="form-control" name="maleMax" />
                </AvGroup>
                <AvGroup>
                  <Label id="femaleMinLabel" for="program-biometric-data-femaleMin">
                    <Translate contentKey="programserviceApp.programBiometricData.femaleMin">Female Min</Translate>
                  </Label>
                  <AvField id="program-biometric-data-femaleMin" type="string" className="form-control" name="femaleMin" />
                </AvGroup>
                <AvGroup>
                  <Label id="femaleMaxLabel" for="program-biometric-data-femaleMax">
                    <Translate contentKey="programserviceApp.programBiometricData.femaleMax">Female Max</Translate>
                  </Label>
                  <AvField id="program-biometric-data-femaleMax" type="string" className="form-control" name="femaleMax" />
                </AvGroup>
                <AvGroup>
                  <Label id="unidentifiedMinLabel" for="program-biometric-data-unidentifiedMin">
                    <Translate contentKey="programserviceApp.programBiometricData.unidentifiedMin">Unidentified Min</Translate>
                  </Label>
                  <AvField id="program-biometric-data-unidentifiedMin" type="string" className="form-control" name="unidentifiedMin" />
                </AvGroup>
                <AvGroup>
                  <Label id="unidentifiedMaxLabel" for="program-biometric-data-unidentifiedMax">
                    <Translate contentKey="programserviceApp.programBiometricData.unidentifiedMax">Unidentified Max</Translate>
                  </Label>
                  <AvField id="program-biometric-data-unidentifiedMax" type="string" className="form-control" name="unidentifiedMax" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-biometric-data" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programBiometricDataEntity: storeState.programBiometricData.entity,
  loading: storeState.programBiometricData.loading,
  updating: storeState.programBiometricData.updating,
  updateSuccess: storeState.programBiometricData.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramBiometricDataUpdate);
