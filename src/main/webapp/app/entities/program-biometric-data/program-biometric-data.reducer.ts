import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramBiometricData, defaultValue } from 'app/shared/model/program-biometric-data.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMBIOMETRICDATA_LIST: 'programBiometricData/FETCH_PROGRAMBIOMETRICDATA_LIST',
  FETCH_PROGRAMBIOMETRICDATA: 'programBiometricData/FETCH_PROGRAMBIOMETRICDATA',
  CREATE_PROGRAMBIOMETRICDATA: 'programBiometricData/CREATE_PROGRAMBIOMETRICDATA',
  UPDATE_PROGRAMBIOMETRICDATA: 'programBiometricData/UPDATE_PROGRAMBIOMETRICDATA',
  DELETE_PROGRAMBIOMETRICDATA: 'programBiometricData/DELETE_PROGRAMBIOMETRICDATA',
  RESET: 'programBiometricData/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramBiometricData>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramBiometricDataState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramBiometricDataState = initialState, action): ProgramBiometricDataState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMBIOMETRICDATA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMBIOMETRICDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMBIOMETRICDATA):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMBIOMETRICDATA):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMBIOMETRICDATA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMBIOMETRICDATA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMBIOMETRICDATA):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMBIOMETRICDATA):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMBIOMETRICDATA):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMBIOMETRICDATA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMBIOMETRICDATA_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMBIOMETRICDATA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMBIOMETRICDATA):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMBIOMETRICDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMBIOMETRICDATA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-biometric-data';

// Actions

export const getEntities: ICrudGetAllAction<IProgramBiometricData> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMBIOMETRICDATA_LIST,
    payload: axios.get<IProgramBiometricData>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramBiometricData> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMBIOMETRICDATA,
    payload: axios.get<IProgramBiometricData>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramBiometricData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMBIOMETRICDATA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramBiometricData> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMBIOMETRICDATA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramBiometricData> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMBIOMETRICDATA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
