import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './program-biometric-data.reducer';
import { IProgramBiometricData } from 'app/shared/model/program-biometric-data.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramBiometricDataProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramBiometricDataState = IPaginationBaseState;

export class ProgramBiometricData extends React.Component<IProgramBiometricDataProps, IProgramBiometricDataState> {
  state: IProgramBiometricDataState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programBiometricDataList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="program-biometric-data-heading">
          <Translate contentKey="programserviceApp.programBiometricData.home.title">Program Biometric Data</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.programBiometricData.home.createLabel">Create new Program Biometric Data</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('biometricCode')}>
                  <Translate contentKey="programserviceApp.programBiometricData.biometricCode">Biometric Code</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('biometricName')}>
                  <Translate contentKey="programserviceApp.programBiometricData.biometricName">Biometric Name</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('maleMin')}>
                  <Translate contentKey="programserviceApp.programBiometricData.maleMin">Male Min</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('maleMax')}>
                  <Translate contentKey="programserviceApp.programBiometricData.maleMax">Male Max</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('femaleMin')}>
                  <Translate contentKey="programserviceApp.programBiometricData.femaleMin">Female Min</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('femaleMax')}>
                  <Translate contentKey="programserviceApp.programBiometricData.femaleMax">Female Max</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('unidentifiedMin')}>
                  <Translate contentKey="programserviceApp.programBiometricData.unidentifiedMin">Unidentified Min</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('unidentifiedMax')}>
                  <Translate contentKey="programserviceApp.programBiometricData.unidentifiedMax">Unidentified Max</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {programBiometricDataList.map((programBiometricData, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${programBiometricData.id}`} color="link" size="sm">
                      {programBiometricData.id}
                    </Button>
                  </td>
                  <td>{programBiometricData.biometricCode}</td>
                  <td>{programBiometricData.biometricName}</td>
                  <td>{programBiometricData.maleMin}</td>
                  <td>{programBiometricData.maleMax}</td>
                  <td>{programBiometricData.femaleMin}</td>
                  <td>{programBiometricData.femaleMax}</td>
                  <td>{programBiometricData.unidentifiedMin}</td>
                  <td>{programBiometricData.unidentifiedMax}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${programBiometricData.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programBiometricData.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programBiometricData.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ programBiometricData }: IRootState) => ({
  programBiometricDataList: programBiometricData.entities,
  totalItems: programBiometricData.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramBiometricData);
