import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './e-consent.reducer';
import { IEConsent } from 'app/shared/model/e-consent.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IEConsentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IEConsentUpdateState {
  isNew: boolean;
}

export class EConsentUpdate extends React.Component<IEConsentUpdateProps, IEConsentUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdAt = convertDateTimeToServer(values.createdAt);

    if (errors.length === 0) {
      const { eConsentEntity } = this.props;
      const entity = {
        ...eConsentEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/e-consent');
  };

  render() {
    const { eConsentEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.eConsent.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.eConsent.home.createOrEditLabel">Create or edit a EConsent</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : eConsentEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="e-consent-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="e-consent-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="participantIdLabel" for="e-consent-participantId">
                    <Translate contentKey="programserviceApp.eConsent.participantId">Participant Id</Translate>
                  </Label>
                  <AvField
                    id="e-consent-participantId"
                    type="text"
                    name="participantId"
                    validate={{
                      maxLength: { value: 36, errorMessage: translate('entity.validation.maxlength', { max: 36 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="programIdLabel" for="e-consent-programId">
                    <Translate contentKey="programserviceApp.eConsent.programId">Program Id</Translate>
                  </Label>
                  <AvField
                    id="e-consent-programId"
                    type="string"
                    className="form-control"
                    name="programId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="hasConfirmedLabel" check>
                    <AvInput id="e-consent-hasConfirmed" type="checkbox" className="form-control" name="hasConfirmed" />
                    <Translate contentKey="programserviceApp.eConsent.hasConfirmed">Has Confirmed</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="createdAtLabel" for="e-consent-createdAt">
                    <Translate contentKey="programserviceApp.eConsent.createdAt">Created At</Translate>
                  </Label>
                  <AvInput
                    id="e-consent-createdAt"
                    type="datetime-local"
                    className="form-control"
                    name="createdAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.eConsentEntity.createdAt)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="termAndConditionIdLabel" for="e-consent-termAndConditionId">
                    <Translate contentKey="programserviceApp.eConsent.termAndConditionId">Term And Condition Id</Translate>
                  </Label>
                  <AvField
                    id="e-consent-termAndConditionId"
                    type="string"
                    className="form-control"
                    name="termAndConditionId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/e-consent" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  eConsentEntity: storeState.eConsent.entity,
  loading: storeState.eConsent.loading,
  updating: storeState.eConsent.updating,
  updateSuccess: storeState.eConsent.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EConsentUpdate);
