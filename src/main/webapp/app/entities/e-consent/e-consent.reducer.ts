import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IEConsent, defaultValue } from 'app/shared/model/e-consent.model';

export const ACTION_TYPES = {
  FETCH_ECONSENT_LIST: 'eConsent/FETCH_ECONSENT_LIST',
  FETCH_ECONSENT: 'eConsent/FETCH_ECONSENT',
  CREATE_ECONSENT: 'eConsent/CREATE_ECONSENT',
  UPDATE_ECONSENT: 'eConsent/UPDATE_ECONSENT',
  DELETE_ECONSENT: 'eConsent/DELETE_ECONSENT',
  RESET: 'eConsent/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IEConsent>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type EConsentState = Readonly<typeof initialState>;

// Reducer

export default (state: EConsentState = initialState, action): EConsentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ECONSENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ECONSENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ECONSENT):
    case REQUEST(ACTION_TYPES.UPDATE_ECONSENT):
    case REQUEST(ACTION_TYPES.DELETE_ECONSENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ECONSENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ECONSENT):
    case FAILURE(ACTION_TYPES.CREATE_ECONSENT):
    case FAILURE(ACTION_TYPES.UPDATE_ECONSENT):
    case FAILURE(ACTION_TYPES.DELETE_ECONSENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ECONSENT_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_ECONSENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ECONSENT):
    case SUCCESS(ACTION_TYPES.UPDATE_ECONSENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ECONSENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/e-consents';

// Actions

export const getEntities: ICrudGetAllAction<IEConsent> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ECONSENT_LIST,
    payload: axios.get<IEConsent>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IEConsent> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ECONSENT,
    payload: axios.get<IEConsent>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IEConsent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ECONSENT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IEConsent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ECONSENT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IEConsent> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ECONSENT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
