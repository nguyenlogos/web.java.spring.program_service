import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import EConsent from './e-consent';
import EConsentDetail from './e-consent-detail';
import EConsentUpdate from './e-consent-update';
import EConsentDeleteDialog from './e-consent-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EConsentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EConsentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EConsentDetail} />
      <ErrorBoundaryRoute path={match.url} component={EConsent} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={EConsentDeleteDialog} />
  </>
);

export default Routes;
