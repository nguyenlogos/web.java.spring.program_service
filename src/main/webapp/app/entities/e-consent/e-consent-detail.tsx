import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './e-consent.reducer';
import { IEConsent } from 'app/shared/model/e-consent.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEConsentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class EConsentDetail extends React.Component<IEConsentDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { eConsentEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.eConsent.detail.title">EConsent</Translate> [<b>{eConsentEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="participantId">
                <Translate contentKey="programserviceApp.eConsent.participantId">Participant Id</Translate>
              </span>
            </dt>
            <dd>{eConsentEntity.participantId}</dd>
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.eConsent.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{eConsentEntity.programId}</dd>
            <dt>
              <span id="hasConfirmed">
                <Translate contentKey="programserviceApp.eConsent.hasConfirmed">Has Confirmed</Translate>
              </span>
            </dt>
            <dd>{eConsentEntity.hasConfirmed ? 'true' : 'false'}</dd>
            <dt>
              <span id="createdAt">
                <Translate contentKey="programserviceApp.eConsent.createdAt">Created At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={eConsentEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="termAndConditionId">
                <Translate contentKey="programserviceApp.eConsent.termAndConditionId">Term And Condition Id</Translate>
              </span>
            </dt>
            <dd>{eConsentEntity.termAndConditionId}</dd>
          </dl>
          <Button tag={Link} to="/entity/e-consent" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/e-consent/${eConsentEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ eConsent }: IRootState) => ({
  eConsentEntity: eConsent.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EConsentDetail);
