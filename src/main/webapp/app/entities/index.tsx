import React from 'react';
import { Switch } from 'react-router-dom';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Category from './category';
import Program from './program';
import ProgramCategoryPoint from './program-category-point';
import ClientProgram from './client-program';
import ProgramLevel from './program-level';
import ProgramLevelActivity from './program-level-activity';
import Reward from './reward';
import ProgramActivity from './program-activity';
import ProgramLevelReward from './program-level-reward';
import SubCategory from './sub-category';
import ProgramSubCategoryPoint from './program-sub-category-point';
import ProgramUser from './program-user';
import UserEvent from './user-event';
import UserReward from './user-reward';
import OrderTransaction from './order-transaction';
import ClientProgramRewardSetting from './client-program-reward-setting';
import ProgramPhase from './program-phase';
import ClientSetting from './client-setting';
import ProgramSubgroup from './program-subgroup';
import ActivityEventQueue from './activity-event-queue';
import TermAndCondition from './term-and-condition';
import ProgramLevelPath from './program-level-path';
import ProgramLevelPractice from './program-level-practice';
import ClientBrandSetting from './client-brand-setting';
import ProgramBiometricData from './program-biometric-data';
import ProgramHealthyRange from './program-healthy-range';
import WellmetricEventQueue from './wellmetric-event-queue';
import NotificationCenter from './notification-center';
import ProgramSubCategoryConfiguration from './program-sub-category-configuration';
import UserEventQueue from './user-event-queue';
import ProgramRequirementItems from './program-requirement-items';
import EConsent from './e-consent';
import ProgramIntegrationTracking from './program-integration-tracking';
import ClientSubDomain from './client-sub-domain';
import ProgramUserLevelProgress from './program-user-level-progress';
import ProgramCollection from './program-collection';
import ProgramCollectionContent from './program-collection-content';
import ProgramTrackingStatus from './program-tracking-status';
import ProgramCohort from './program-cohort';
import ProgramCohortDataInput from './program-cohort-data-input';
import ProgramCohortRule from './program-cohort-rule';
import ProgramCohortCollection from './program-cohort-collection';
import ProgramUserCohort from './program-user-cohort';
import ProgramUserLogs from './program-user-logs';
import ProgramUserCollectionProgress from './program-user-collection-progress';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}/category`} component={Category} />
      <ErrorBoundaryRoute path={`${match.url}/program`} component={Program} />
      <ErrorBoundaryRoute path={`${match.url}/program-category-point`} component={ProgramCategoryPoint} />
      <ErrorBoundaryRoute path={`${match.url}/client-program`} component={ClientProgram} />
      <ErrorBoundaryRoute path={`${match.url}/program-level`} component={ProgramLevel} />
      <ErrorBoundaryRoute path={`${match.url}/program-level-activity`} component={ProgramLevelActivity} />
      <ErrorBoundaryRoute path={`${match.url}/reward`} component={Reward} />
      <ErrorBoundaryRoute path={`${match.url}/program-activity`} component={ProgramActivity} />
      <ErrorBoundaryRoute path={`${match.url}/program-level-reward`} component={ProgramLevelReward} />
      <ErrorBoundaryRoute path={`${match.url}/sub-category`} component={SubCategory} />
      <ErrorBoundaryRoute path={`${match.url}/program-sub-category-point`} component={ProgramSubCategoryPoint} />
      <ErrorBoundaryRoute path={`${match.url}/program-user`} component={ProgramUser} />
      <ErrorBoundaryRoute path={`${match.url}/user-event`} component={UserEvent} />
      <ErrorBoundaryRoute path={`${match.url}/user-reward`} component={UserReward} />
      <ErrorBoundaryRoute path={`${match.url}/order-transaction`} component={OrderTransaction} />
      <ErrorBoundaryRoute path={`${match.url}/client-program-reward-setting`} component={ClientProgramRewardSetting} />
      <ErrorBoundaryRoute path={`${match.url}/program-phase`} component={ProgramPhase} />
      <ErrorBoundaryRoute path={`${match.url}/client-setting`} component={ClientSetting} />
      <ErrorBoundaryRoute path={`${match.url}/program-subgroup`} component={ProgramSubgroup} />
      <ErrorBoundaryRoute path={`${match.url}/activity-event-queue`} component={ActivityEventQueue} />
      <ErrorBoundaryRoute path={`${match.url}/term-and-condition`} component={TermAndCondition} />
      <ErrorBoundaryRoute path={`${match.url}/program-level-path`} component={ProgramLevelPath} />
      <ErrorBoundaryRoute path={`${match.url}/program-level-practice`} component={ProgramLevelPractice} />
      <ErrorBoundaryRoute path={`${match.url}/client-brand-setting`} component={ClientBrandSetting} />
      <ErrorBoundaryRoute path={`${match.url}/program-biometric-data`} component={ProgramBiometricData} />
      <ErrorBoundaryRoute path={`${match.url}/program-healthy-range`} component={ProgramHealthyRange} />
      <ErrorBoundaryRoute path={`${match.url}/wellmetric-event-queue`} component={WellmetricEventQueue} />
      <ErrorBoundaryRoute path={`${match.url}/notification-center`} component={NotificationCenter} />
      <ErrorBoundaryRoute path={`${match.url}/program-sub-category-configuration`} component={ProgramSubCategoryConfiguration} />
      <ErrorBoundaryRoute path={`${match.url}/user-event-queue`} component={UserEventQueue} />
      <ErrorBoundaryRoute path={`${match.url}/program-requirement-items`} component={ProgramRequirementItems} />
      <ErrorBoundaryRoute path={`${match.url}/e-consent`} component={EConsent} />
      <ErrorBoundaryRoute path={`${match.url}/program-integration-tracking`} component={ProgramIntegrationTracking} />
      <ErrorBoundaryRoute path={`${match.url}/client-sub-domain`} component={ClientSubDomain} />
      <ErrorBoundaryRoute path={`${match.url}program-user-level-progress`} component={ProgramUserLevelProgress} />
      <ErrorBoundaryRoute path={`${match.url}/program-user-level-progress`} component={ProgramUserLevelProgress} />
      <ErrorBoundaryRoute path={`${match.url}/program-collection`} component={ProgramCollection} />
      <ErrorBoundaryRoute path={`${match.url}/program-collection-content`} component={ProgramCollectionContent} />
      <ErrorBoundaryRoute path={`${match.url}/program-tracking-status`} component={ProgramTrackingStatus} />
      <ErrorBoundaryRoute path={`${match.url}/program-cohort`} component={ProgramCohort} />
      <ErrorBoundaryRoute path={`${match.url}/program-cohort-data-input`} component={ProgramCohortDataInput} />
      <ErrorBoundaryRoute path={`${match.url}/program-cohort-rule`} component={ProgramCohortRule} />
      <ErrorBoundaryRoute path={`${match.url}/program-cohort-collection`} component={ProgramCohortCollection} />
      <ErrorBoundaryRoute path={`${match.url}/program-user-cohort`} component={ProgramUserCohort} />
      <ErrorBoundaryRoute path={`${match.url}/program-user-logs`} component={ProgramUserLogs} />
      <ErrorBoundaryRoute path={`${match.url}/program-user-collection-progress`} component={ProgramUserCollectionProgress} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
