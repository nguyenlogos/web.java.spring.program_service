import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramTrackingStatus from './program-tracking-status';
import ProgramTrackingStatusDetail from './program-tracking-status-detail';
import ProgramTrackingStatusUpdate from './program-tracking-status-update';
import ProgramTrackingStatusDeleteDialog from './program-tracking-status-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramTrackingStatusUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramTrackingStatusUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramTrackingStatusDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramTrackingStatus} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramTrackingStatusDeleteDialog} />
  </>
);

export default Routes;
