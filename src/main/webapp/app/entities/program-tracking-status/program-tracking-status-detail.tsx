import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-tracking-status.reducer';
import { IProgramTrackingStatus } from 'app/shared/model/program-tracking-status.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramTrackingStatusDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramTrackingStatusDetail extends React.Component<IProgramTrackingStatusDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programTrackingStatusEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programTrackingStatus.detail.title">ProgramTrackingStatus</Translate> [
            <b>{programTrackingStatusEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.programTrackingStatus.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{programTrackingStatusEntity.programId}</dd>
            <dt>
              <span id="programStatus">
                <Translate contentKey="programserviceApp.programTrackingStatus.programStatus">Program Status</Translate>
              </span>
            </dt>
            <dd>{programTrackingStatusEntity.programStatus}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programTrackingStatus.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programTrackingStatusEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programTrackingStatus.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programTrackingStatusEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/program-tracking-status" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-tracking-status/${programTrackingStatusEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programTrackingStatus }: IRootState) => ({
  programTrackingStatusEntity: programTrackingStatus.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramTrackingStatusDetail);
