import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './program-tracking-status.reducer';
import { IProgramTrackingStatus } from 'app/shared/model/program-tracking-status.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramTrackingStatusUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramTrackingStatusUpdateState {
  isNew: boolean;
}

export class ProgramTrackingStatusUpdate extends React.Component<IProgramTrackingStatusUpdateProps, IProgramTrackingStatusUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programTrackingStatusEntity } = this.props;
      const entity = {
        ...programTrackingStatusEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-tracking-status');
  };

  render() {
    const { programTrackingStatusEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programTrackingStatus.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programTrackingStatus.home.createOrEditLabel">
                Create or edit a ProgramTrackingStatus
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programTrackingStatusEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-tracking-status-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-tracking-status-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="programIdLabel" for="program-tracking-status-programId">
                    <Translate contentKey="programserviceApp.programTrackingStatus.programId">Program Id</Translate>
                  </Label>
                  <AvField
                    id="program-tracking-status-programId"
                    type="string"
                    className="form-control"
                    name="programId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="programStatusLabel" for="program-tracking-status-programStatus">
                    <Translate contentKey="programserviceApp.programTrackingStatus.programStatus">Program Status</Translate>
                  </Label>
                  <AvField id="program-tracking-status-programStatus" type="text" name="programStatus" />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-tracking-status-createdDate">
                    <Translate contentKey="programserviceApp.programTrackingStatus.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-tracking-status-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programTrackingStatusEntity.createdDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-tracking-status-modifiedDate">
                    <Translate contentKey="programserviceApp.programTrackingStatus.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-tracking-status-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programTrackingStatusEntity.modifiedDate)}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-tracking-status" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programTrackingStatusEntity: storeState.programTrackingStatus.entity,
  loading: storeState.programTrackingStatus.loading,
  updating: storeState.programTrackingStatus.updating,
  updateSuccess: storeState.programTrackingStatus.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramTrackingStatusUpdate);
