import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramTrackingStatus, defaultValue } from 'app/shared/model/program-tracking-status.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMTRACKINGSTATUS_LIST: 'programTrackingStatus/FETCH_PROGRAMTRACKINGSTATUS_LIST',
  FETCH_PROGRAMTRACKINGSTATUS: 'programTrackingStatus/FETCH_PROGRAMTRACKINGSTATUS',
  CREATE_PROGRAMTRACKINGSTATUS: 'programTrackingStatus/CREATE_PROGRAMTRACKINGSTATUS',
  UPDATE_PROGRAMTRACKINGSTATUS: 'programTrackingStatus/UPDATE_PROGRAMTRACKINGSTATUS',
  DELETE_PROGRAMTRACKINGSTATUS: 'programTrackingStatus/DELETE_PROGRAMTRACKINGSTATUS',
  RESET: 'programTrackingStatus/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramTrackingStatus>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramTrackingStatusState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramTrackingStatusState = initialState, action): ProgramTrackingStatusState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMTRACKINGSTATUS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMTRACKINGSTATUS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMTRACKINGSTATUS):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMTRACKINGSTATUS):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMTRACKINGSTATUS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMTRACKINGSTATUS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMTRACKINGSTATUS):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMTRACKINGSTATUS):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMTRACKINGSTATUS):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMTRACKINGSTATUS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMTRACKINGSTATUS_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMTRACKINGSTATUS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMTRACKINGSTATUS):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMTRACKINGSTATUS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMTRACKINGSTATUS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-tracking-statuses';

// Actions

export const getEntities: ICrudGetAllAction<IProgramTrackingStatus> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMTRACKINGSTATUS_LIST,
    payload: axios.get<IProgramTrackingStatus>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramTrackingStatus> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMTRACKINGSTATUS,
    payload: axios.get<IProgramTrackingStatus>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramTrackingStatus> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMTRACKINGSTATUS,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramTrackingStatus> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMTRACKINGSTATUS,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramTrackingStatus> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMTRACKINGSTATUS,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
