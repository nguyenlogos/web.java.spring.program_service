import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgamLevelUser from './progam-level-user';
import ProgamLevelUserDetail from './progam-level-user-detail';
import ProgamLevelUserUpdate from './progam-level-user-update';
import ProgamLevelUserDeleteDialog from './progam-level-user-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgamLevelUserUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgamLevelUserUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgamLevelUserDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgamLevelUser} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgamLevelUserDeleteDialog} />
  </>
);

export default Routes;
