import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramUser } from 'app/shared/model/program-user.model';
import { getEntities as getProgramUsers } from 'app/entities/program-user/program-user.reducer';
import { getEntity, updateEntity, createEntity, reset } from './progam-level-user.reducer';
import { IProgamLevelUser } from 'app/shared/model/progam-level-user.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgamLevelUserUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgamLevelUserUpdateState {
  isNew: boolean;
  programUserId: string;
}

export class ProgamLevelUserUpdate extends React.Component<IProgamLevelUserUpdateProps, IProgamLevelUserUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programUserId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramUsers();
  }

  saveEntity = (event, errors, values) => {
    values.completedDate = convertDateTimeToServer(values.completedDate);

    if (errors.length === 0) {
      const { progamLevelUserEntity } = this.props;
      const entity = {
        ...progamLevelUserEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/progam-level-user');
  };

  render() {
    const { progamLevelUserEntity, programUsers, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.progamLevelUser.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.progamLevelUser.home.createOrEditLabel">Create or edit a ProgamLevelUser</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : progamLevelUserEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="progam-level-user-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="progam-level-user-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="levelLabel" for="progam-level-user-level">
                    <Translate contentKey="programserviceApp.progamLevelUser.level">Level</Translate>
                  </Label>
                  <AvField id="progam-level-user-level" type="string" className="form-control" name="level" />
                </AvGroup>
                <AvGroup>
                  <Label id="completedDateLabel" for="progam-level-user-completedDate">
                    <Translate contentKey="programserviceApp.progamLevelUser.completedDate">Completed Date</Translate>
                  </Label>
                  <AvInput
                    id="progam-level-user-completedDate"
                    type="datetime-local"
                    className="form-control"
                    name="completedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.progamLevelUserEntity.completedDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="currentPointLabel" for="progam-level-user-currentPoint">
                    <Translate contentKey="programserviceApp.progamLevelUser.currentPoint">Current Point</Translate>
                  </Label>
                  <AvField id="progam-level-user-currentPoint" type="text" name="currentPoint" />
                </AvGroup>
                <AvGroup>
                  <Label for="progam-level-user-programUser">
                    <Translate contentKey="programserviceApp.progamLevelUser.programUser">Program User</Translate>
                  </Label>
                  <AvInput id="progam-level-user-programUser" type="select" className="form-control" name="programUserId" required>
                    {programUsers
                      ? programUsers.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.programId}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/progam-level-user" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programUsers: storeState.programUser.entities,
  progamLevelUserEntity: storeState.progamLevelUser.entity,
  loading: storeState.progamLevelUser.loading,
  updating: storeState.progamLevelUser.updating,
  updateSuccess: storeState.progamLevelUser.updateSuccess
});

const mapDispatchToProps = {
  getProgramUsers,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgamLevelUserUpdate);
