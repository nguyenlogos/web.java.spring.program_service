import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgamLevelUser, defaultValue } from 'app/shared/model/progam-level-user.model';

export const ACTION_TYPES = {
  FETCH_PROGAMLEVELUSER_LIST: 'progamLevelUser/FETCH_PROGAMLEVELUSER_LIST',
  FETCH_PROGAMLEVELUSER: 'progamLevelUser/FETCH_PROGAMLEVELUSER',
  CREATE_PROGAMLEVELUSER: 'progamLevelUser/CREATE_PROGAMLEVELUSER',
  UPDATE_PROGAMLEVELUSER: 'progamLevelUser/UPDATE_PROGAMLEVELUSER',
  DELETE_PROGAMLEVELUSER: 'progamLevelUser/DELETE_PROGAMLEVELUSER',
  RESET: 'progamLevelUser/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgamLevelUser>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgamLevelUserState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgamLevelUserState = initialState, action): ProgamLevelUserState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGAMLEVELUSER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGAMLEVELUSER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGAMLEVELUSER):
    case REQUEST(ACTION_TYPES.UPDATE_PROGAMLEVELUSER):
    case REQUEST(ACTION_TYPES.DELETE_PROGAMLEVELUSER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGAMLEVELUSER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGAMLEVELUSER):
    case FAILURE(ACTION_TYPES.CREATE_PROGAMLEVELUSER):
    case FAILURE(ACTION_TYPES.UPDATE_PROGAMLEVELUSER):
    case FAILURE(ACTION_TYPES.DELETE_PROGAMLEVELUSER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGAMLEVELUSER_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGAMLEVELUSER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGAMLEVELUSER):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGAMLEVELUSER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGAMLEVELUSER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/progam-level-users';

// Actions

export const getEntities: ICrudGetAllAction<IProgamLevelUser> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGAMLEVELUSER_LIST,
    payload: axios.get<IProgamLevelUser>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgamLevelUser> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGAMLEVELUSER,
    payload: axios.get<IProgamLevelUser>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgamLevelUser> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGAMLEVELUSER,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgamLevelUser> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGAMLEVELUSER,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgamLevelUser> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGAMLEVELUSER,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
