import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './progam-level-user.reducer';
import { IProgamLevelUser } from 'app/shared/model/progam-level-user.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgamLevelUserDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgamLevelUserDetail extends React.Component<IProgamLevelUserDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { progamLevelUserEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.progamLevelUser.detail.title">ProgamLevelUser</Translate> [
            <b>{progamLevelUserEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="level">
                <Translate contentKey="programserviceApp.progamLevelUser.level">Level</Translate>
              </span>
            </dt>
            <dd>{progamLevelUserEntity.level}</dd>
            <dt>
              <span id="completedDate">
                <Translate contentKey="programserviceApp.progamLevelUser.completedDate">Completed Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={progamLevelUserEntity.completedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="currentPoint">
                <Translate contentKey="programserviceApp.progamLevelUser.currentPoint">Current Point</Translate>
              </span>
            </dt>
            <dd>{progamLevelUserEntity.currentPoint}</dd>
            <dt>
              <Translate contentKey="programserviceApp.progamLevelUser.programUser">Program User</Translate>
            </dt>
            <dd>{progamLevelUserEntity.programUserProgramId ? progamLevelUserEntity.programUserProgramId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/progam-level-user" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/progam-level-user/${progamLevelUserEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ progamLevelUser }: IRootState) => ({
  progamLevelUserEntity: progamLevelUser.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgamLevelUserDetail);
