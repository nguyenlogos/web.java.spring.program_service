import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-cohort-rule.reducer';
import { IProgramCohortRule } from 'app/shared/model/program-cohort-rule.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCohortRuleDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramCohortRuleDetail extends React.Component<IProgramCohortRuleDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programCohortRuleEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programCohortRule.detail.title">ProgramCohortRule</Translate> [
            <b>{programCohortRuleEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="dataInputType">
                <Translate contentKey="programserviceApp.programCohortRule.dataInputType">Data Input Type</Translate>
              </span>
            </dt>
            <dd>{programCohortRuleEntity.dataInputType}</dd>
            <dt>
              <span id="rules">
                <Translate contentKey="programserviceApp.programCohortRule.rules">Rules</Translate>
              </span>
            </dt>
            <dd>{programCohortRuleEntity.rules}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programCohortRule.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCohortRuleEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programCohortRule.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCohortRuleEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programCohortRule.programCohort">Program Cohort</Translate>
            </dt>
            <dd>{programCohortRuleEntity.programCohortId ? programCohortRuleEntity.programCohortId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-cohort-rule" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-cohort-rule/${programCohortRuleEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programCohortRule }: IRootState) => ({
  programCohortRuleEntity: programCohortRule.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortRuleDetail);
