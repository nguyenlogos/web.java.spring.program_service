import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramCohortRule, defaultValue } from 'app/shared/model/program-cohort-rule.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMCOHORTRULE_LIST: 'programCohortRule/FETCH_PROGRAMCOHORTRULE_LIST',
  FETCH_PROGRAMCOHORTRULE: 'programCohortRule/FETCH_PROGRAMCOHORTRULE',
  CREATE_PROGRAMCOHORTRULE: 'programCohortRule/CREATE_PROGRAMCOHORTRULE',
  UPDATE_PROGRAMCOHORTRULE: 'programCohortRule/UPDATE_PROGRAMCOHORTRULE',
  DELETE_PROGRAMCOHORTRULE: 'programCohortRule/DELETE_PROGRAMCOHORTRULE',
  RESET: 'programCohortRule/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramCohortRule>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramCohortRuleState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramCohortRuleState = initialState, action): ProgramCohortRuleState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOHORTRULE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOHORTRULE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMCOHORTRULE):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMCOHORTRULE):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMCOHORTRULE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOHORTRULE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOHORTRULE):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMCOHORTRULE):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMCOHORTRULE):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMCOHORTRULE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOHORTRULE_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOHORTRULE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMCOHORTRULE):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMCOHORTRULE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMCOHORTRULE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-cohort-rules';

// Actions

export const getEntities: ICrudGetAllAction<IProgramCohortRule> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOHORTRULE_LIST,
    payload: axios.get<IProgramCohortRule>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramCohortRule> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOHORTRULE,
    payload: axios.get<IProgramCohortRule>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramCohortRule> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMCOHORTRULE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramCohortRule> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMCOHORTRULE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramCohortRule> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMCOHORTRULE,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
