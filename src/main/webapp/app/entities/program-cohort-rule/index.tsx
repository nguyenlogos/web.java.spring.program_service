import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramCohortRule from './program-cohort-rule';
import ProgramCohortRuleDetail from './program-cohort-rule-detail';
import ProgramCohortRuleUpdate from './program-cohort-rule-update';
import ProgramCohortRuleDeleteDialog from './program-cohort-rule-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramCohortRuleUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramCohortRuleUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramCohortRuleDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramCohortRule} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramCohortRuleDeleteDialog} />
  </>
);

export default Routes;
