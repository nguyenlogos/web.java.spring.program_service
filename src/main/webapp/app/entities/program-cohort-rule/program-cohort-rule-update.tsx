import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramCohort } from 'app/shared/model/program-cohort.model';
import { getEntities as getProgramCohorts } from 'app/entities/program-cohort/program-cohort.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-cohort-rule.reducer';
import { IProgramCohortRule } from 'app/shared/model/program-cohort-rule.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramCohortRuleUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramCohortRuleUpdateState {
  isNew: boolean;
  programCohortId: string;
}

export class ProgramCohortRuleUpdate extends React.Component<IProgramCohortRuleUpdateProps, IProgramCohortRuleUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programCohortId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramCohorts();
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programCohortRuleEntity } = this.props;
      const entity = {
        ...programCohortRuleEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-cohort-rule');
  };

  render() {
    const { programCohortRuleEntity, programCohorts, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programCohortRule.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programCohortRule.home.createOrEditLabel">
                Create or edit a ProgramCohortRule
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programCohortRuleEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-cohort-rule-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-cohort-rule-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="dataInputTypeLabel" for="program-cohort-rule-dataInputType">
                    <Translate contentKey="programserviceApp.programCohortRule.dataInputType">Data Input Type</Translate>
                  </Label>
                  <AvField
                    id="program-cohort-rule-dataInputType"
                    type="text"
                    name="dataInputType"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="rulesLabel" for="program-cohort-rule-rules">
                    <Translate contentKey="programserviceApp.programCohortRule.rules">Rules</Translate>
                  </Label>
                  <AvField
                    id="program-cohort-rule-rules"
                    type="text"
                    name="rules"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-cohort-rule-createdDate">
                    <Translate contentKey="programserviceApp.programCohortRule.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-rule-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCohortRuleEntity.createdDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-cohort-rule-modifiedDate">
                    <Translate contentKey="programserviceApp.programCohortRule.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-rule-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCohortRuleEntity.modifiedDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-cohort-rule-programCohort">
                    <Translate contentKey="programserviceApp.programCohortRule.programCohort">Program Cohort</Translate>
                  </Label>
                  <AvInput id="program-cohort-rule-programCohort" type="select" className="form-control" name="programCohortId">
                    <option value="" key="0" />
                    {programCohorts
                      ? programCohorts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-cohort-rule" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programCohorts: storeState.programCohort.entities,
  programCohortRuleEntity: storeState.programCohortRule.entity,
  loading: storeState.programCohortRule.loading,
  updating: storeState.programCohortRule.updating,
  updateSuccess: storeState.programCohortRule.updateSuccess
});

const mapDispatchToProps = {
  getProgramCohorts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortRuleUpdate);
