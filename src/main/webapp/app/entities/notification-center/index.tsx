import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import NotificationCenter from './notification-center';
import NotificationCenterDetail from './notification-center-detail';
import NotificationCenterUpdate from './notification-center-update';
import NotificationCenterDeleteDialog from './notification-center-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NotificationCenterUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NotificationCenterUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NotificationCenterDetail} />
      <ErrorBoundaryRoute path={match.url} component={NotificationCenter} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={NotificationCenterDeleteDialog} />
  </>
);

export default Routes;
