import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './notification-center.reducer';
import { INotificationCenter } from 'app/shared/model/notification-center.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface INotificationCenterProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type INotificationCenterState = IPaginationBaseState;

export class NotificationCenter extends React.Component<INotificationCenterProps, INotificationCenterState> {
  state: INotificationCenterState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { notificationCenterList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="notification-center-heading">
          <Translate contentKey="programserviceApp.notificationCenter.home.title">Notification Centers</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.notificationCenter.home.createLabel">Create new Notification Center</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('title')}>
                  <Translate contentKey="programserviceApp.notificationCenter.title">Title</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('participantId')}>
                  <Translate contentKey="programserviceApp.notificationCenter.participantId">Participant Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('executeStatus')}>
                  <Translate contentKey="programserviceApp.notificationCenter.executeStatus">Execute Status</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('createdAt')}>
                  <Translate contentKey="programserviceApp.notificationCenter.createdAt">Created At</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('attemptCount')}>
                  <Translate contentKey="programserviceApp.notificationCenter.attemptCount">Attempt Count</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lastAttemptTime')}>
                  <Translate contentKey="programserviceApp.notificationCenter.lastAttemptTime">Last Attempt Time</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('errorMessage')}>
                  <Translate contentKey="programserviceApp.notificationCenter.errorMessage">Error Message</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('content')}>
                  <Translate contentKey="programserviceApp.notificationCenter.content">Content</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('notificationType')}>
                  <Translate contentKey="programserviceApp.notificationCenter.notificationType">Notification Type</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventId')}>
                  <Translate contentKey="programserviceApp.notificationCenter.eventId">Event Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isReady')}>
                  <Translate contentKey="programserviceApp.notificationCenter.isReady">Is Ready</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('receiverName')}>
                  <Translate contentKey="programserviceApp.notificationCenter.receiverName">Receiver Name</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {notificationCenterList.map((notificationCenter, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${notificationCenter.id}`} color="link" size="sm">
                      {notificationCenter.id}
                    </Button>
                  </td>
                  <td>{notificationCenter.title}</td>
                  <td>{notificationCenter.participantId}</td>
                  <td>{notificationCenter.executeStatus}</td>
                  <td>
                    <TextFormat type="date" value={notificationCenter.createdAt} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{notificationCenter.attemptCount}</td>
                  <td>
                    <TextFormat type="date" value={notificationCenter.lastAttemptTime} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{notificationCenter.errorMessage}</td>
                  <td>{notificationCenter.content}</td>
                  <td>
                    <Translate contentKey={`programserviceApp.NotificationType.${notificationCenter.notificationType}`} />
                  </td>
                  <td>{notificationCenter.eventId}</td>
                  <td>{notificationCenter.isReady ? 'true' : 'false'}</td>
                  <td>{notificationCenter.receiverName}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${notificationCenter.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${notificationCenter.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${notificationCenter.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ notificationCenter }: IRootState) => ({
  notificationCenterList: notificationCenter.entities,
  totalItems: notificationCenter.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationCenter);
