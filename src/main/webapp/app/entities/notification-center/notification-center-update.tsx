import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './notification-center.reducer';
import { INotificationCenter } from 'app/shared/model/notification-center.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface INotificationCenterUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface INotificationCenterUpdateState {
  isNew: boolean;
}

export class NotificationCenterUpdate extends React.Component<INotificationCenterUpdateProps, INotificationCenterUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.lastAttemptTime = convertDateTimeToServer(values.lastAttemptTime);

    if (errors.length === 0) {
      const { notificationCenterEntity } = this.props;
      const entity = {
        ...notificationCenterEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/notification-center');
  };

  render() {
    const { notificationCenterEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.notificationCenter.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.notificationCenter.home.createOrEditLabel">
                Create or edit a NotificationCenter
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : notificationCenterEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="notification-center-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="notification-center-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="titleLabel" for="notification-center-title">
                    <Translate contentKey="programserviceApp.notificationCenter.title">Title</Translate>
                  </Label>
                  <AvField
                    id="notification-center-title"
                    type="text"
                    name="title"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="participantIdLabel" for="notification-center-participantId">
                    <Translate contentKey="programserviceApp.notificationCenter.participantId">Participant Id</Translate>
                  </Label>
                  <AvField
                    id="notification-center-participantId"
                    type="text"
                    name="participantId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="executeStatusLabel" for="notification-center-executeStatus">
                    <Translate contentKey="programserviceApp.notificationCenter.executeStatus">Execute Status</Translate>
                  </Label>
                  <AvField
                    id="notification-center-executeStatus"
                    type="text"
                    name="executeStatus"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdAtLabel" for="notification-center-createdAt">
                    <Translate contentKey="programserviceApp.notificationCenter.createdAt">Created At</Translate>
                  </Label>
                  <AvInput
                    id="notification-center-createdAt"
                    type="datetime-local"
                    className="form-control"
                    name="createdAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.notificationCenterEntity.createdAt)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="attemptCountLabel" for="notification-center-attemptCount">
                    <Translate contentKey="programserviceApp.notificationCenter.attemptCount">Attempt Count</Translate>
                  </Label>
                  <AvField id="notification-center-attemptCount" type="string" className="form-control" name="attemptCount" />
                </AvGroup>
                <AvGroup>
                  <Label id="lastAttemptTimeLabel" for="notification-center-lastAttemptTime">
                    <Translate contentKey="programserviceApp.notificationCenter.lastAttemptTime">Last Attempt Time</Translate>
                  </Label>
                  <AvInput
                    id="notification-center-lastAttemptTime"
                    type="datetime-local"
                    className="form-control"
                    name="lastAttemptTime"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.notificationCenterEntity.lastAttemptTime)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="errorMessageLabel" for="notification-center-errorMessage">
                    <Translate contentKey="programserviceApp.notificationCenter.errorMessage">Error Message</Translate>
                  </Label>
                  <AvField id="notification-center-errorMessage" type="text" name="errorMessage" />
                </AvGroup>
                <AvGroup>
                  <Label id="contentLabel" for="notification-center-content">
                    <Translate contentKey="programserviceApp.notificationCenter.content">Content</Translate>
                  </Label>
                  <AvField
                    id="notification-center-content"
                    type="text"
                    name="content"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="notificationTypeLabel" for="notification-center-notificationType">
                    <Translate contentKey="programserviceApp.notificationCenter.notificationType">Notification Type</Translate>
                  </Label>
                  <AvInput
                    id="notification-center-notificationType"
                    type="select"
                    className="form-control"
                    name="notificationType"
                    value={(!isNew && notificationCenterEntity.notificationType) || 'WELLMETRIC_SCREENING_RESULT'}
                  >
                    <option value="WELLMETRIC_SCREENING_RESULT">
                      <Translate contentKey="programserviceApp.NotificationType.WELLMETRIC_SCREENING_RESULT" />
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="eventIdLabel" for="notification-center-eventId">
                    <Translate contentKey="programserviceApp.notificationCenter.eventId">Event Id</Translate>
                  </Label>
                  <AvField
                    id="notification-center-eventId"
                    type="text"
                    name="eventId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="isReadyLabel" check>
                    <AvInput id="notification-center-isReady" type="checkbox" className="form-control" name="isReady" />
                    <Translate contentKey="programserviceApp.notificationCenter.isReady">Is Ready</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="receiverNameLabel" for="notification-center-receiverName">
                    <Translate contentKey="programserviceApp.notificationCenter.receiverName">Receiver Name</Translate>
                  </Label>
                  <AvField
                    id="notification-center-receiverName"
                    type="text"
                    name="receiverName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/notification-center" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  notificationCenterEntity: storeState.notificationCenter.entity,
  loading: storeState.notificationCenter.loading,
  updating: storeState.notificationCenter.updating,
  updateSuccess: storeState.notificationCenter.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationCenterUpdate);
