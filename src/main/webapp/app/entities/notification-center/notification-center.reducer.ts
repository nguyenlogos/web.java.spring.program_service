import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INotificationCenter, defaultValue } from 'app/shared/model/notification-center.model';

export const ACTION_TYPES = {
  FETCH_NOTIFICATIONCENTER_LIST: 'notificationCenter/FETCH_NOTIFICATIONCENTER_LIST',
  FETCH_NOTIFICATIONCENTER: 'notificationCenter/FETCH_NOTIFICATIONCENTER',
  CREATE_NOTIFICATIONCENTER: 'notificationCenter/CREATE_NOTIFICATIONCENTER',
  UPDATE_NOTIFICATIONCENTER: 'notificationCenter/UPDATE_NOTIFICATIONCENTER',
  DELETE_NOTIFICATIONCENTER: 'notificationCenter/DELETE_NOTIFICATIONCENTER',
  RESET: 'notificationCenter/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INotificationCenter>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type NotificationCenterState = Readonly<typeof initialState>;

// Reducer

export default (state: NotificationCenterState = initialState, action): NotificationCenterState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NOTIFICATIONCENTER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NOTIFICATIONCENTER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NOTIFICATIONCENTER):
    case REQUEST(ACTION_TYPES.UPDATE_NOTIFICATIONCENTER):
    case REQUEST(ACTION_TYPES.DELETE_NOTIFICATIONCENTER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NOTIFICATIONCENTER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NOTIFICATIONCENTER):
    case FAILURE(ACTION_TYPES.CREATE_NOTIFICATIONCENTER):
    case FAILURE(ACTION_TYPES.UPDATE_NOTIFICATIONCENTER):
    case FAILURE(ACTION_TYPES.DELETE_NOTIFICATIONCENTER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NOTIFICATIONCENTER_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_NOTIFICATIONCENTER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NOTIFICATIONCENTER):
    case SUCCESS(ACTION_TYPES.UPDATE_NOTIFICATIONCENTER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NOTIFICATIONCENTER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/notification-centers';

// Actions

export const getEntities: ICrudGetAllAction<INotificationCenter> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_NOTIFICATIONCENTER_LIST,
    payload: axios.get<INotificationCenter>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<INotificationCenter> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NOTIFICATIONCENTER,
    payload: axios.get<INotificationCenter>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INotificationCenter> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NOTIFICATIONCENTER,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INotificationCenter> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NOTIFICATIONCENTER,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INotificationCenter> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NOTIFICATIONCENTER,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
