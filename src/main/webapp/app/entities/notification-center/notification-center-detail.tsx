import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './notification-center.reducer';
import { INotificationCenter } from 'app/shared/model/notification-center.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INotificationCenterDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class NotificationCenterDetail extends React.Component<INotificationCenterDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { notificationCenterEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.notificationCenter.detail.title">NotificationCenter</Translate> [
            <b>{notificationCenterEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="title">
                <Translate contentKey="programserviceApp.notificationCenter.title">Title</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.title}</dd>
            <dt>
              <span id="participantId">
                <Translate contentKey="programserviceApp.notificationCenter.participantId">Participant Id</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.participantId}</dd>
            <dt>
              <span id="executeStatus">
                <Translate contentKey="programserviceApp.notificationCenter.executeStatus">Execute Status</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.executeStatus}</dd>
            <dt>
              <span id="createdAt">
                <Translate contentKey="programserviceApp.notificationCenter.createdAt">Created At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={notificationCenterEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="attemptCount">
                <Translate contentKey="programserviceApp.notificationCenter.attemptCount">Attempt Count</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.attemptCount}</dd>
            <dt>
              <span id="lastAttemptTime">
                <Translate contentKey="programserviceApp.notificationCenter.lastAttemptTime">Last Attempt Time</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={notificationCenterEntity.lastAttemptTime} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="errorMessage">
                <Translate contentKey="programserviceApp.notificationCenter.errorMessage">Error Message</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.errorMessage}</dd>
            <dt>
              <span id="content">
                <Translate contentKey="programserviceApp.notificationCenter.content">Content</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.content}</dd>
            <dt>
              <span id="notificationType">
                <Translate contentKey="programserviceApp.notificationCenter.notificationType">Notification Type</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.notificationType}</dd>
            <dt>
              <span id="eventId">
                <Translate contentKey="programserviceApp.notificationCenter.eventId">Event Id</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.eventId}</dd>
            <dt>
              <span id="isReady">
                <Translate contentKey="programserviceApp.notificationCenter.isReady">Is Ready</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.isReady ? 'true' : 'false'}</dd>
            <dt>
              <span id="receiverName">
                <Translate contentKey="programserviceApp.notificationCenter.receiverName">Receiver Name</Translate>
              </span>
            </dt>
            <dd>{notificationCenterEntity.receiverName}</dd>
          </dl>
          <Button tag={Link} to="/entity/notification-center" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/notification-center/${notificationCenterEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ notificationCenter }: IRootState) => ({
  notificationCenterEntity: notificationCenter.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationCenterDetail);
