import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramCollection from './program-collection';
import ProgramCollectionDetail from './program-collection-detail';
import ProgramCollectionUpdate from './program-collection-update';
import ProgramCollectionDeleteDialog from './program-collection-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramCollectionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramCollectionUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramCollectionDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramCollection} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramCollectionDeleteDialog} />
  </>
);

export default Routes;
