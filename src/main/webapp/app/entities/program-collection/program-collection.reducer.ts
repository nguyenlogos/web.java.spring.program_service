import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramCollection, defaultValue } from 'app/shared/model/program-collection.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMCOLLECTION_LIST: 'programCollection/FETCH_PROGRAMCOLLECTION_LIST',
  FETCH_PROGRAMCOLLECTION: 'programCollection/FETCH_PROGRAMCOLLECTION',
  CREATE_PROGRAMCOLLECTION: 'programCollection/CREATE_PROGRAMCOLLECTION',
  UPDATE_PROGRAMCOLLECTION: 'programCollection/UPDATE_PROGRAMCOLLECTION',
  DELETE_PROGRAMCOLLECTION: 'programCollection/DELETE_PROGRAMCOLLECTION',
  RESET: 'programCollection/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramCollection>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramCollectionState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramCollectionState = initialState, action): ProgramCollectionState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOLLECTION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOLLECTION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMCOLLECTION):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMCOLLECTION):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMCOLLECTION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOLLECTION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOLLECTION):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMCOLLECTION):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMCOLLECTION):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMCOLLECTION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOLLECTION_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOLLECTION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMCOLLECTION):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMCOLLECTION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMCOLLECTION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-collections';

// Actions

export const getEntities: ICrudGetAllAction<IProgramCollection> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOLLECTION_LIST,
    payload: axios.get<IProgramCollection>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramCollection> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOLLECTION,
    payload: axios.get<IProgramCollection>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramCollection> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMCOLLECTION,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramCollection> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMCOLLECTION,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramCollection> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMCOLLECTION,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
