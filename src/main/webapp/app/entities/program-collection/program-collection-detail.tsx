import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-collection.reducer';
import { IProgramCollection } from 'app/shared/model/program-collection.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCollectionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramCollectionDetail extends React.Component<IProgramCollectionDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programCollectionEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programCollection.detail.title">ProgramCollection</Translate> [
            <b>{programCollectionEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="displayName">
                <Translate contentKey="programserviceApp.programCollection.displayName">Display Name</Translate>
              </span>
            </dt>
            <dd>{programCollectionEntity.displayName}</dd>
            <dt>
              <span id="longDescription">
                <Translate contentKey="programserviceApp.programCollection.longDescription">Long Description</Translate>
              </span>
            </dt>
            <dd>{programCollectionEntity.longDescription}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programCollection.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCollectionEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programCollection.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCollectionEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programCollection.program">Program</Translate>
            </dt>
            <dd>{programCollectionEntity.programId ? programCollectionEntity.programId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-collection" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-collection/${programCollectionEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programCollection }: IRootState) => ({
  programCollectionEntity: programCollection.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCollectionDetail);
