import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-collection.reducer';
import { IProgramCollection } from 'app/shared/model/program-collection.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramCollectionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramCollectionUpdateState {
  isNew: boolean;
  programId: string;
}

export class ProgramCollectionUpdate extends React.Component<IProgramCollectionUpdateProps, IProgramCollectionUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPrograms();
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programCollectionEntity } = this.props;
      const entity = {
        ...programCollectionEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-collection');
  };

  render() {
    const { programCollectionEntity, programs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programCollection.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programCollection.home.createOrEditLabel">
                Create or edit a ProgramCollection
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programCollectionEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-collection-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-collection-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="displayNameLabel" for="program-collection-displayName">
                    <Translate contentKey="programserviceApp.programCollection.displayName">Display Name</Translate>
                  </Label>
                  <AvField
                    id="program-collection-displayName"
                    type="text"
                    name="displayName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 150, errorMessage: translate('entity.validation.maxlength', { max: 150 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="longDescriptionLabel" for="program-collection-longDescription">
                    <Translate contentKey="programserviceApp.programCollection.longDescription">Long Description</Translate>
                  </Label>
                  <AvField
                    id="program-collection-longDescription"
                    type="text"
                    name="longDescription"
                    validate={{
                      maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-collection-createdDate">
                    <Translate contentKey="programserviceApp.programCollection.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-collection-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCollectionEntity.createdDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-collection-modifiedDate">
                    <Translate contentKey="programserviceApp.programCollection.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-collection-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCollectionEntity.modifiedDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-collection-program">
                    <Translate contentKey="programserviceApp.programCollection.program">Program</Translate>
                  </Label>
                  <AvInput id="program-collection-program" type="select" className="form-control" name="programId">
                    <option value="" key="0" />
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-collection" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programs: storeState.program.entities,
  programCollectionEntity: storeState.programCollection.entity,
  loading: storeState.programCollection.loading,
  updating: storeState.programCollection.updating,
  updateSuccess: storeState.programCollection.updateSuccess
});

const mapDispatchToProps = {
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCollectionUpdate);
