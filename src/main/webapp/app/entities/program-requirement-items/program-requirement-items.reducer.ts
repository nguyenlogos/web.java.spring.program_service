import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramRequirementItems, defaultValue } from 'app/shared/model/program-requirement-items.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMREQUIREMENTITEMS_LIST: 'programRequirementItems/FETCH_PROGRAMREQUIREMENTITEMS_LIST',
  FETCH_PROGRAMREQUIREMENTITEMS: 'programRequirementItems/FETCH_PROGRAMREQUIREMENTITEMS',
  CREATE_PROGRAMREQUIREMENTITEMS: 'programRequirementItems/CREATE_PROGRAMREQUIREMENTITEMS',
  UPDATE_PROGRAMREQUIREMENTITEMS: 'programRequirementItems/UPDATE_PROGRAMREQUIREMENTITEMS',
  DELETE_PROGRAMREQUIREMENTITEMS: 'programRequirementItems/DELETE_PROGRAMREQUIREMENTITEMS',
  RESET: 'programRequirementItems/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramRequirementItems>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramRequirementItemsState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramRequirementItemsState = initialState, action): ProgramRequirementItemsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMREQUIREMENTITEMS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMREQUIREMENTITEMS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMREQUIREMENTITEMS):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMREQUIREMENTITEMS):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMREQUIREMENTITEMS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMREQUIREMENTITEMS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMREQUIREMENTITEMS):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMREQUIREMENTITEMS):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMREQUIREMENTITEMS):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMREQUIREMENTITEMS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMREQUIREMENTITEMS_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMREQUIREMENTITEMS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMREQUIREMENTITEMS):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMREQUIREMENTITEMS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMREQUIREMENTITEMS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-requirement-items';

// Actions

export const getEntities: ICrudGetAllAction<IProgramRequirementItems> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMREQUIREMENTITEMS_LIST,
    payload: axios.get<IProgramRequirementItems>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramRequirementItems> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMREQUIREMENTITEMS,
    payload: axios.get<IProgramRequirementItems>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramRequirementItems> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMREQUIREMENTITEMS,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramRequirementItems> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMREQUIREMENTITEMS,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramRequirementItems> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMREQUIREMENTITEMS,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
