import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './program-requirement-items.reducer';
import { IProgramRequirementItems } from 'app/shared/model/program-requirement-items.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramRequirementItemsProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramRequirementItemsState = IPaginationBaseState;

export class ProgramRequirementItems extends React.Component<IProgramRequirementItemsProps, IProgramRequirementItemsState> {
  state: IProgramRequirementItemsState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programRequirementItemsList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="program-requirement-items-heading">
          <Translate contentKey="programserviceApp.programRequirementItems.home.title">Program Requirement Items</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.programRequirementItems.home.createLabel">
              Create new Program Requirement Items
            </Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('itemId')}>
                  <Translate contentKey="programserviceApp.programRequirementItems.itemId">Item Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('nameOfItem')}>
                  <Translate contentKey="programserviceApp.programRequirementItems.nameOfItem">Name Of Item</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('typeOfItem')}>
                  <Translate contentKey="programserviceApp.programRequirementItems.typeOfItem">Type Of Item</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('itemCode')}>
                  <Translate contentKey="programserviceApp.programRequirementItems.itemCode">Item Code</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('subgroupId')}>
                  <Translate contentKey="programserviceApp.programRequirementItems.subgroupId">Subgroup Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('subgroupName')}>
                  <Translate contentKey="programserviceApp.programRequirementItems.subgroupName">Subgroup Name</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('createdAt')}>
                  <Translate contentKey="programserviceApp.programRequirementItems.createdAt">Created At</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="programserviceApp.programRequirementItems.programLevel">Program Level</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {programRequirementItemsList.map((programRequirementItems, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${programRequirementItems.id}`} color="link" size="sm">
                      {programRequirementItems.id}
                    </Button>
                  </td>
                  <td>{programRequirementItems.itemId}</td>
                  <td>{programRequirementItems.nameOfItem}</td>
                  <td>{programRequirementItems.typeOfItem}</td>
                  <td>{programRequirementItems.itemCode}</td>
                  <td>{programRequirementItems.subgroupId}</td>
                  <td>{programRequirementItems.subgroupName}</td>
                  <td>
                    <TextFormat type="date" value={programRequirementItems.createdAt} format={APP_DATE_FORMAT} />
                  </td>
                  <td>
                    {programRequirementItems.programLevelId ? (
                      <Link to={`program-level/${programRequirementItems.programLevelId}`}>{programRequirementItems.programLevelId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${programRequirementItems.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programRequirementItems.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programRequirementItems.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ programRequirementItems }: IRootState) => ({
  programRequirementItemsList: programRequirementItems.entities,
  totalItems: programRequirementItems.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramRequirementItems);
