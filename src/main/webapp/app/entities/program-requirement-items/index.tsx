import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramRequirementItems from './program-requirement-items';
import ProgramRequirementItemsDetail from './program-requirement-items-detail';
import ProgramRequirementItemsUpdate from './program-requirement-items-update';
import ProgramRequirementItemsDeleteDialog from './program-requirement-items-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramRequirementItemsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramRequirementItemsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramRequirementItemsDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramRequirementItems} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramRequirementItemsDeleteDialog} />
  </>
);

export default Routes;
