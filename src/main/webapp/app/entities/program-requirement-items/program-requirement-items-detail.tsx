import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-requirement-items.reducer';
import { IProgramRequirementItems } from 'app/shared/model/program-requirement-items.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramRequirementItemsDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramRequirementItemsDetail extends React.Component<IProgramRequirementItemsDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programRequirementItemsEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programRequirementItems.detail.title">ProgramRequirementItems</Translate> [
            <b>{programRequirementItemsEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="itemId">
                <Translate contentKey="programserviceApp.programRequirementItems.itemId">Item Id</Translate>
              </span>
            </dt>
            <dd>{programRequirementItemsEntity.itemId}</dd>
            <dt>
              <span id="nameOfItem">
                <Translate contentKey="programserviceApp.programRequirementItems.nameOfItem">Name Of Item</Translate>
              </span>
            </dt>
            <dd>{programRequirementItemsEntity.nameOfItem}</dd>
            <dt>
              <span id="typeOfItem">
                <Translate contentKey="programserviceApp.programRequirementItems.typeOfItem">Type Of Item</Translate>
              </span>
            </dt>
            <dd>{programRequirementItemsEntity.typeOfItem}</dd>
            <dt>
              <span id="itemCode">
                <Translate contentKey="programserviceApp.programRequirementItems.itemCode">Item Code</Translate>
              </span>
            </dt>
            <dd>{programRequirementItemsEntity.itemCode}</dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.programRequirementItems.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{programRequirementItemsEntity.subgroupId}</dd>
            <dt>
              <span id="subgroupName">
                <Translate contentKey="programserviceApp.programRequirementItems.subgroupName">Subgroup Name</Translate>
              </span>
            </dt>
            <dd>{programRequirementItemsEntity.subgroupName}</dd>
            <dt>
              <span id="createdAt">
                <Translate contentKey="programserviceApp.programRequirementItems.createdAt">Created At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programRequirementItemsEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programRequirementItems.programLevel">Program Level</Translate>
            </dt>
            <dd>{programRequirementItemsEntity.programLevelId ? programRequirementItemsEntity.programLevelId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-requirement-items" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-requirement-items/${programRequirementItemsEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programRequirementItems }: IRootState) => ({
  programRequirementItemsEntity: programRequirementItems.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramRequirementItemsDetail);
