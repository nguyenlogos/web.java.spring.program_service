import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramLevel } from 'app/shared/model/program-level.model';
import { getEntities as getProgramLevels } from 'app/entities/program-level/program-level.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-requirement-items.reducer';
import { IProgramRequirementItems } from 'app/shared/model/program-requirement-items.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramRequirementItemsUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramRequirementItemsUpdateState {
  isNew: boolean;
  programLevelId: string;
}

export class ProgramRequirementItemsUpdate extends React.Component<
  IProgramRequirementItemsUpdateProps,
  IProgramRequirementItemsUpdateState
> {
  constructor(props) {
    super(props);
    this.state = {
      programLevelId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramLevels();
  }

  saveEntity = (event, errors, values) => {
    values.createdAt = convertDateTimeToServer(values.createdAt);

    if (errors.length === 0) {
      const { programRequirementItemsEntity } = this.props;
      const entity = {
        ...programRequirementItemsEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-requirement-items');
  };

  render() {
    const { programRequirementItemsEntity, programLevels, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programRequirementItems.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programRequirementItems.home.createOrEditLabel">
                Create or edit a ProgramRequirementItems
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programRequirementItemsEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-requirement-items-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-requirement-items-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="itemIdLabel" for="program-requirement-items-itemId">
                    <Translate contentKey="programserviceApp.programRequirementItems.itemId">Item Id</Translate>
                  </Label>
                  <AvField
                    id="program-requirement-items-itemId"
                    type="text"
                    name="itemId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="nameOfItemLabel" for="program-requirement-items-nameOfItem">
                    <Translate contentKey="programserviceApp.programRequirementItems.nameOfItem">Name Of Item</Translate>
                  </Label>
                  <AvField id="program-requirement-items-nameOfItem" type="text" name="nameOfItem" />
                </AvGroup>
                <AvGroup>
                  <Label id="typeOfItemLabel" for="program-requirement-items-typeOfItem">
                    <Translate contentKey="programserviceApp.programRequirementItems.typeOfItem">Type Of Item</Translate>
                  </Label>
                  <AvField
                    id="program-requirement-items-typeOfItem"
                    type="text"
                    name="typeOfItem"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="itemCodeLabel" for="program-requirement-items-itemCode">
                    <Translate contentKey="programserviceApp.programRequirementItems.itemCode">Item Code</Translate>
                  </Label>
                  <AvField
                    id="program-requirement-items-itemCode"
                    type="text"
                    name="itemCode"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="program-requirement-items-subgroupId">
                    <Translate contentKey="programserviceApp.programRequirementItems.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="program-requirement-items-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupNameLabel" for="program-requirement-items-subgroupName">
                    <Translate contentKey="programserviceApp.programRequirementItems.subgroupName">Subgroup Name</Translate>
                  </Label>
                  <AvField id="program-requirement-items-subgroupName" type="text" name="subgroupName" />
                </AvGroup>
                <AvGroup>
                  <Label id="createdAtLabel" for="program-requirement-items-createdAt">
                    <Translate contentKey="programserviceApp.programRequirementItems.createdAt">Created At</Translate>
                  </Label>
                  <AvInput
                    id="program-requirement-items-createdAt"
                    type="datetime-local"
                    className="form-control"
                    name="createdAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programRequirementItemsEntity.createdAt)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-requirement-items-programLevel">
                    <Translate contentKey="programserviceApp.programRequirementItems.programLevel">Program Level</Translate>
                  </Label>
                  <AvInput
                    id="program-requirement-items-programLevel"
                    type="select"
                    className="form-control"
                    name="programLevelId"
                    required
                  >
                    {programLevels
                      ? programLevels.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-requirement-items" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programLevels: storeState.programLevel.entities,
  programRequirementItemsEntity: storeState.programRequirementItems.entity,
  loading: storeState.programRequirementItems.loading,
  updating: storeState.programRequirementItems.updating,
  updateSuccess: storeState.programRequirementItems.updateSuccess
});

const mapDispatchToProps = {
  getProgramLevels,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramRequirementItemsUpdate);
