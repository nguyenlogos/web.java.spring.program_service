import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramPhase from './program-phase';
import ProgramPhaseDetail from './program-phase-detail';
import ProgramPhaseUpdate from './program-phase-update';
import ProgramPhaseDeleteDialog from './program-phase-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramPhaseUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramPhaseUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramPhaseDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramPhase} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramPhaseDeleteDialog} />
  </>
);

export default Routes;
