import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-phase.reducer';
import { IProgramPhase } from 'app/shared/model/program-phase.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramPhaseDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramPhaseDetail extends React.Component<IProgramPhaseDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programPhaseEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programPhase.detail.title">ProgramPhase</Translate> [<b>{programPhaseEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="phaseOrder">
                <Translate contentKey="programserviceApp.programPhase.phaseOrder">Phase Order</Translate>
              </span>
            </dt>
            <dd>{programPhaseEntity.phaseOrder}</dd>
            <dt>
              <span id="startDate">
                <Translate contentKey="programserviceApp.programPhase.startDate">Start Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programPhaseEntity.startDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="endDate">
                <Translate contentKey="programserviceApp.programPhase.endDate">End Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programPhaseEntity.endDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programPhase.program">Program</Translate>
            </dt>
            <dd>{programPhaseEntity.programName ? programPhaseEntity.programName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-phase" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-phase/${programPhaseEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programPhase }: IRootState) => ({
  programPhaseEntity: programPhase.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramPhaseDetail);
