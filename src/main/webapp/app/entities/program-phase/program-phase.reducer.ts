import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramPhase, defaultValue } from 'app/shared/model/program-phase.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMPHASE_LIST: 'programPhase/FETCH_PROGRAMPHASE_LIST',
  FETCH_PROGRAMPHASE: 'programPhase/FETCH_PROGRAMPHASE',
  CREATE_PROGRAMPHASE: 'programPhase/CREATE_PROGRAMPHASE',
  UPDATE_PROGRAMPHASE: 'programPhase/UPDATE_PROGRAMPHASE',
  DELETE_PROGRAMPHASE: 'programPhase/DELETE_PROGRAMPHASE',
  RESET: 'programPhase/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramPhase>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramPhaseState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramPhaseState = initialState, action): ProgramPhaseState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMPHASE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMPHASE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMPHASE):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMPHASE):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMPHASE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMPHASE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMPHASE):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMPHASE):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMPHASE):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMPHASE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMPHASE_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMPHASE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMPHASE):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMPHASE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMPHASE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-phases';

// Actions

export const getEntities: ICrudGetAllAction<IProgramPhase> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMPHASE_LIST,
    payload: axios.get<IProgramPhase>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramPhase> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMPHASE,
    payload: axios.get<IProgramPhase>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramPhase> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMPHASE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramPhase> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMPHASE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramPhase> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMPHASE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
