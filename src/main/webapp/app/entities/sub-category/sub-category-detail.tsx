import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './sub-category.reducer';
import { ISubCategory } from 'app/shared/model/sub-category.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISubCategoryDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class SubCategoryDetail extends React.Component<ISubCategoryDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { subCategoryEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.subCategory.detail.title">SubCategory</Translate> [<b>{subCategoryEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="code">
                <Translate contentKey="programserviceApp.subCategory.code">Code</Translate>
              </span>
            </dt>
            <dd>{subCategoryEntity.code}</dd>
            <dt>
              <span id="name">
                <Translate contentKey="programserviceApp.subCategory.name">Name</Translate>
              </span>
            </dt>
            <dd>{subCategoryEntity.name}</dd>
            <dt>
              <Translate contentKey="programserviceApp.subCategory.category">Category</Translate>
            </dt>
            <dd>{subCategoryEntity.categoryCode ? subCategoryEntity.categoryCode : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/sub-category" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/sub-category/${subCategoryEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ subCategory }: IRootState) => ({
  subCategoryEntity: subCategory.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubCategoryDetail);
