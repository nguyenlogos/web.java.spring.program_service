import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserEvent from './user-event';
import UserEventDetail from './user-event-detail';
import UserEventUpdate from './user-event-update';
import UserEventDeleteDialog from './user-event-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UserEventUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UserEventUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UserEventDetail} />
      <ErrorBoundaryRoute path={match.url} component={UserEvent} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={UserEventDeleteDialog} />
  </>
);

export default Routes;
