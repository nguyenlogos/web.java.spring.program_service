import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IUserEvent, defaultValue } from 'app/shared/model/user-event.model';

export const ACTION_TYPES = {
  FETCH_USEREVENT_LIST: 'userEvent/FETCH_USEREVENT_LIST',
  FETCH_USEREVENT: 'userEvent/FETCH_USEREVENT',
  CREATE_USEREVENT: 'userEvent/CREATE_USEREVENT',
  UPDATE_USEREVENT: 'userEvent/UPDATE_USEREVENT',
  DELETE_USEREVENT: 'userEvent/DELETE_USEREVENT',
  RESET: 'userEvent/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IUserEvent>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type UserEventState = Readonly<typeof initialState>;

// Reducer

export default (state: UserEventState = initialState, action): UserEventState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_USEREVENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_USEREVENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_USEREVENT):
    case REQUEST(ACTION_TYPES.UPDATE_USEREVENT):
    case REQUEST(ACTION_TYPES.DELETE_USEREVENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_USEREVENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_USEREVENT):
    case FAILURE(ACTION_TYPES.CREATE_USEREVENT):
    case FAILURE(ACTION_TYPES.UPDATE_USEREVENT):
    case FAILURE(ACTION_TYPES.DELETE_USEREVENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_USEREVENT_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_USEREVENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_USEREVENT):
    case SUCCESS(ACTION_TYPES.UPDATE_USEREVENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_USEREVENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/user-events';

// Actions

export const getEntities: ICrudGetAllAction<IUserEvent> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_USEREVENT_LIST,
    payload: axios.get<IUserEvent>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IUserEvent> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_USEREVENT,
    payload: axios.get<IUserEvent>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IUserEvent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_USEREVENT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IUserEvent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_USEREVENT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IUserEvent> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_USEREVENT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
