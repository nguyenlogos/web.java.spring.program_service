import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-event.reducer';
import { IUserEvent } from 'app/shared/model/user-event.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserEventDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class UserEventDetail extends React.Component<IUserEventDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { userEventEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.userEvent.detail.title">UserEvent</Translate> [<b>{userEventEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="eventCode">
                <Translate contentKey="programserviceApp.userEvent.eventCode">Event Code</Translate>
              </span>
            </dt>
            <dd>{userEventEntity.eventCode}</dd>
            <dt>
              <span id="eventId">
                <Translate contentKey="programserviceApp.userEvent.eventId">Event Id</Translate>
              </span>
            </dt>
            <dd>{userEventEntity.eventId}</dd>
            <dt>
              <span id="eventDate">
                <Translate contentKey="programserviceApp.userEvent.eventDate">Event Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={userEventEntity.eventDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="eventPoint">
                <Translate contentKey="programserviceApp.userEvent.eventPoint">Event Point</Translate>
              </span>
            </dt>
            <dd>{userEventEntity.eventPoint}</dd>
            <dt>
              <span id="eventCategory">
                <Translate contentKey="programserviceApp.userEvent.eventCategory">Event Category</Translate>
              </span>
            </dt>
            <dd>{userEventEntity.eventCategory}</dd>
            <dt>
              <Translate contentKey="programserviceApp.userEvent.programUser">Program User</Translate>
            </dt>
            <dd>{userEventEntity.programUserParticipantId ? userEventEntity.programUserParticipantId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/user-event" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/user-event/${userEventEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ userEvent }: IRootState) => ({
  userEventEntity: userEvent.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEventDetail);
