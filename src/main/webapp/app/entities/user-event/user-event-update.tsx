import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramUser } from 'app/shared/model/program-user.model';
import { getEntities as getProgramUsers } from 'app/entities/program-user/program-user.reducer';
import { getEntity, updateEntity, createEntity, reset } from './user-event.reducer';
import { IUserEvent } from 'app/shared/model/user-event.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUserEventUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IUserEventUpdateState {
  isNew: boolean;
  programUserId: string;
}

export class UserEventUpdate extends React.Component<IUserEventUpdateProps, IUserEventUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programUserId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramUsers();
  }

  saveEntity = (event, errors, values) => {
    values.eventDate = convertDateTimeToServer(values.eventDate);

    if (errors.length === 0) {
      const { userEventEntity } = this.props;
      const entity = {
        ...userEventEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/user-event');
  };

  render() {
    const { userEventEntity, programUsers, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.userEvent.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.userEvent.home.createOrEditLabel">Create or edit a UserEvent</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : userEventEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="user-event-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="user-event-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="eventCodeLabel" for="user-event-eventCode">
                    <Translate contentKey="programserviceApp.userEvent.eventCode">Event Code</Translate>
                  </Label>
                  <AvField
                    id="user-event-eventCode"
                    type="text"
                    name="eventCode"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="eventIdLabel" for="user-event-eventId">
                    <Translate contentKey="programserviceApp.userEvent.eventId">Event Id</Translate>
                  </Label>
                  <AvField
                    id="user-event-eventId"
                    type="text"
                    name="eventId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="eventDateLabel" for="user-event-eventDate">
                    <Translate contentKey="programserviceApp.userEvent.eventDate">Event Date</Translate>
                  </Label>
                  <AvInput
                    id="user-event-eventDate"
                    type="datetime-local"
                    className="form-control"
                    name="eventDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.userEventEntity.eventDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="eventPointLabel" for="user-event-eventPoint">
                    <Translate contentKey="programserviceApp.userEvent.eventPoint">Event Point</Translate>
                  </Label>
                  <AvField id="user-event-eventPoint" type="text" name="eventPoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="eventCategoryLabel" for="user-event-eventCategory">
                    <Translate contentKey="programserviceApp.userEvent.eventCategory">Event Category</Translate>
                  </Label>
                  <AvField id="user-event-eventCategory" type="text" name="eventCategory" />
                </AvGroup>
                <AvGroup>
                  <Label for="user-event-programUser">
                    <Translate contentKey="programserviceApp.userEvent.programUser">Program User</Translate>
                  </Label>
                  <AvInput id="user-event-programUser" type="select" className="form-control" name="programUserId" required>
                    {programUsers
                      ? programUsers.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.participantId}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/user-event" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programUsers: storeState.programUser.entities,
  userEventEntity: storeState.userEvent.entity,
  loading: storeState.userEvent.loading,
  updating: storeState.userEvent.updating,
  updateSuccess: storeState.userEvent.updateSuccess
});

const mapDispatchToProps = {
  getProgramUsers,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserEventUpdate);
