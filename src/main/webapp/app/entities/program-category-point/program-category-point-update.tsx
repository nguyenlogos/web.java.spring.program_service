import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-category-point.reducer';
import { IProgramCategoryPoint } from 'app/shared/model/program-category-point.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramCategoryPointUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramCategoryPointUpdateState {
  isNew: boolean;
  programId: string;
}

export class ProgramCategoryPointUpdate extends React.Component<IProgramCategoryPointUpdateProps, IProgramCategoryPointUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPrograms();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programCategoryPointEntity } = this.props;
      const entity = {
        ...programCategoryPointEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-category-point');
  };

  render() {
    const { programCategoryPointEntity, programs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programCategoryPoint.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programCategoryPoint.home.createOrEditLabel">
                Create or edit a ProgramCategoryPoint
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programCategoryPointEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-category-point-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-category-point-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="categoryCodeLabel" for="program-category-point-categoryCode">
                    <Translate contentKey="programserviceApp.programCategoryPoint.categoryCode">Category Code</Translate>
                  </Label>
                  <AvField
                    id="program-category-point-categoryCode"
                    type="text"
                    name="categoryCode"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="categoryNameLabel" for="program-category-point-categoryName">
                    <Translate contentKey="programserviceApp.programCategoryPoint.categoryName">Category Name</Translate>
                  </Label>
                  <AvField id="program-category-point-categoryName" type="text" name="categoryName" />
                </AvGroup>
                <AvGroup>
                  <Label id="percentPointLabel" for="program-category-point-percentPoint">
                    <Translate contentKey="programserviceApp.programCategoryPoint.percentPoint">Percent Point</Translate>
                  </Label>
                  <AvField id="program-category-point-percentPoint" type="text" name="percentPoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="valuePointLabel" for="program-category-point-valuePoint">
                    <Translate contentKey="programserviceApp.programCategoryPoint.valuePoint">Value Point</Translate>
                  </Label>
                  <AvField id="program-category-point-valuePoint" type="text" name="valuePoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="lockedLabel" check>
                    <AvInput id="program-category-point-locked" type="checkbox" className="form-control" name="locked" />
                    <Translate contentKey="programserviceApp.programCategoryPoint.locked">Locked</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label for="program-category-point-program">
                    <Translate contentKey="programserviceApp.programCategoryPoint.program">Program</Translate>
                  </Label>
                  <AvInput id="program-category-point-program" type="select" className="form-control" name="programId" required>
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-category-point" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programs: storeState.program.entities,
  programCategoryPointEntity: storeState.programCategoryPoint.entity,
  loading: storeState.programCategoryPoint.loading,
  updating: storeState.programCategoryPoint.updating,
  updateSuccess: storeState.programCategoryPoint.updateSuccess
});

const mapDispatchToProps = {
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCategoryPointUpdate);
