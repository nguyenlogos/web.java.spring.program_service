import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramCategoryPoint, defaultValue } from 'app/shared/model/program-category-point.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMCATEGORYPOINT_LIST: 'programCategoryPoint/FETCH_PROGRAMCATEGORYPOINT_LIST',
  FETCH_PROGRAMCATEGORYPOINT: 'programCategoryPoint/FETCH_PROGRAMCATEGORYPOINT',
  CREATE_PROGRAMCATEGORYPOINT: 'programCategoryPoint/CREATE_PROGRAMCATEGORYPOINT',
  UPDATE_PROGRAMCATEGORYPOINT: 'programCategoryPoint/UPDATE_PROGRAMCATEGORYPOINT',
  DELETE_PROGRAMCATEGORYPOINT: 'programCategoryPoint/DELETE_PROGRAMCATEGORYPOINT',
  RESET: 'programCategoryPoint/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramCategoryPoint>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramCategoryPointState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramCategoryPointState = initialState, action): ProgramCategoryPointState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCATEGORYPOINT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCATEGORYPOINT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMCATEGORYPOINT):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMCATEGORYPOINT):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMCATEGORYPOINT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCATEGORYPOINT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCATEGORYPOINT):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMCATEGORYPOINT):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMCATEGORYPOINT):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMCATEGORYPOINT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCATEGORYPOINT_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCATEGORYPOINT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMCATEGORYPOINT):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMCATEGORYPOINT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMCATEGORYPOINT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-category-points';

// Actions

export const getEntities: ICrudGetAllAction<IProgramCategoryPoint> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCATEGORYPOINT_LIST,
    payload: axios.get<IProgramCategoryPoint>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramCategoryPoint> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCATEGORYPOINT,
    payload: axios.get<IProgramCategoryPoint>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramCategoryPoint> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMCATEGORYPOINT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramCategoryPoint> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMCATEGORYPOINT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramCategoryPoint> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMCATEGORYPOINT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
