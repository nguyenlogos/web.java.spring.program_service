import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramCategoryPoint from './program-category-point';
import ProgramCategoryPointDetail from './program-category-point-detail';
import ProgramCategoryPointUpdate from './program-category-point-update';
import ProgramCategoryPointDeleteDialog from './program-category-point-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramCategoryPointUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramCategoryPointUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramCategoryPointDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramCategoryPoint} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramCategoryPointDeleteDialog} />
  </>
);

export default Routes;
