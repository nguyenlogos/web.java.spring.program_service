import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-category-point.reducer';
import { IProgramCategoryPoint } from 'app/shared/model/program-category-point.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCategoryPointDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramCategoryPointDetail extends React.Component<IProgramCategoryPointDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programCategoryPointEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programCategoryPoint.detail.title">ProgramCategoryPoint</Translate> [
            <b>{programCategoryPointEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="categoryCode">
                <Translate contentKey="programserviceApp.programCategoryPoint.categoryCode">Category Code</Translate>
              </span>
            </dt>
            <dd>{programCategoryPointEntity.categoryCode}</dd>
            <dt>
              <span id="categoryName">
                <Translate contentKey="programserviceApp.programCategoryPoint.categoryName">Category Name</Translate>
              </span>
            </dt>
            <dd>{programCategoryPointEntity.categoryName}</dd>
            <dt>
              <span id="percentPoint">
                <Translate contentKey="programserviceApp.programCategoryPoint.percentPoint">Percent Point</Translate>
              </span>
            </dt>
            <dd>{programCategoryPointEntity.percentPoint}</dd>
            <dt>
              <span id="valuePoint">
                <Translate contentKey="programserviceApp.programCategoryPoint.valuePoint">Value Point</Translate>
              </span>
            </dt>
            <dd>{programCategoryPointEntity.valuePoint}</dd>
            <dt>
              <span id="locked">
                <Translate contentKey="programserviceApp.programCategoryPoint.locked">Locked</Translate>
              </span>
            </dt>
            <dd>{programCategoryPointEntity.locked ? 'true' : 'false'}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programCategoryPoint.program">Program</Translate>
            </dt>
            <dd>{programCategoryPointEntity.programName ? programCategoryPointEntity.programName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-category-point" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-category-point/${programCategoryPointEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programCategoryPoint }: IRootState) => ({
  programCategoryPointEntity: programCategoryPoint.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCategoryPointDetail);
