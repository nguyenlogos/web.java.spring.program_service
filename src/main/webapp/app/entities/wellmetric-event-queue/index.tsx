import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import WellmetricEventQueue from './wellmetric-event-queue';
import WellmetricEventQueueDetail from './wellmetric-event-queue-detail';
import WellmetricEventQueueUpdate from './wellmetric-event-queue-update';
import WellmetricEventQueueDeleteDialog from './wellmetric-event-queue-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={WellmetricEventQueueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={WellmetricEventQueueUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={WellmetricEventQueueDetail} />
      <ErrorBoundaryRoute path={match.url} component={WellmetricEventQueue} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={WellmetricEventQueueDeleteDialog} />
  </>
);

export default Routes;
