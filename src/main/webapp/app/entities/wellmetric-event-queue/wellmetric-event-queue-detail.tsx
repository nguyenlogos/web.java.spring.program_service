import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './wellmetric-event-queue.reducer';
import { IWellmetricEventQueue } from 'app/shared/model/wellmetric-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IWellmetricEventQueueDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class WellmetricEventQueueDetail extends React.Component<IWellmetricEventQueueDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { wellmetricEventQueueEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.wellmetricEventQueue.detail.title">WellmetricEventQueue</Translate> [
            <b>{wellmetricEventQueueEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="participantId">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.participantId">Participant Id</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.participantId}</dd>
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.clientId}</dd>
            <dt>
              <span id="email">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.email">Email</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.email}</dd>
            <dt>
              <span id="errorMessage">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.errorMessage">Error Message</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.errorMessage}</dd>
            <dt>
              <span id="result">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.result">Result</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.result}</dd>
            <dt>
              <span id="eventCode">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.eventCode">Event Code</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.eventCode}</dd>
            <dt>
              <span id="gender">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.gender">Gender</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.gender}</dd>
            <dt>
              <span id="attemptCount">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.attemptCount">Attempt Count</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.attemptCount}</dd>
            <dt>
              <span id="lastAttemptTime">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.lastAttemptTime">Last Attempt Time</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={wellmetricEventQueueEntity.lastAttemptTime} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="createdAt">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.createdAt">Created At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={wellmetricEventQueueEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.subgroupId}</dd>
            <dt>
              <span id="healthyValue">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.healthyValue">Healthy Value</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.healthyValue}</dd>
            <dt>
              <span id="executeStatus">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.executeStatus">Execute Status</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.executeStatus}</dd>
            <dt>
              <span id="eventId">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.eventId">Event Id</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.eventId}</dd>
            <dt>
              <span id="hasIncentive">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.hasIncentive">Has Incentive</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.hasIncentive ? 'true' : 'false'}</dd>
            <dt>
              <span id="isWaitingPush">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.isWaitingPush">Is Waiting Push</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.isWaitingPush ? 'true' : 'false'}</dd>
            <dt>
              <span id="attemptedAt">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.attemptedAt">Attempted At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={wellmetricEventQueueEntity.attemptedAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.programId}</dd>
            <dt>
              <span id="eventDate">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.eventDate">Event Date</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.eventDate}</dd>
            <dt>
              <span id="subCategoryCode">
                <Translate contentKey="programserviceApp.wellmetricEventQueue.subCategoryCode">Sub Category Code</Translate>
              </span>
            </dt>
            <dd>{wellmetricEventQueueEntity.subCategoryCode}</dd>
          </dl>
          <Button tag={Link} to="/entity/wellmetric-event-queue" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/wellmetric-event-queue/${wellmetricEventQueueEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ wellmetricEventQueue }: IRootState) => ({
  wellmetricEventQueueEntity: wellmetricEventQueue.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WellmetricEventQueueDetail);
