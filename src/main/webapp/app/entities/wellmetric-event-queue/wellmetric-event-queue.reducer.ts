import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IWellmetricEventQueue, defaultValue } from 'app/shared/model/wellmetric-event-queue.model';

export const ACTION_TYPES = {
  FETCH_WELLMETRICEVENTQUEUE_LIST: 'wellmetricEventQueue/FETCH_WELLMETRICEVENTQUEUE_LIST',
  FETCH_WELLMETRICEVENTQUEUE: 'wellmetricEventQueue/FETCH_WELLMETRICEVENTQUEUE',
  CREATE_WELLMETRICEVENTQUEUE: 'wellmetricEventQueue/CREATE_WELLMETRICEVENTQUEUE',
  UPDATE_WELLMETRICEVENTQUEUE: 'wellmetricEventQueue/UPDATE_WELLMETRICEVENTQUEUE',
  DELETE_WELLMETRICEVENTQUEUE: 'wellmetricEventQueue/DELETE_WELLMETRICEVENTQUEUE',
  RESET: 'wellmetricEventQueue/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IWellmetricEventQueue>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type WellmetricEventQueueState = Readonly<typeof initialState>;

// Reducer

export default (state: WellmetricEventQueueState = initialState, action): WellmetricEventQueueState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_WELLMETRICEVENTQUEUE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_WELLMETRICEVENTQUEUE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_WELLMETRICEVENTQUEUE):
    case REQUEST(ACTION_TYPES.UPDATE_WELLMETRICEVENTQUEUE):
    case REQUEST(ACTION_TYPES.DELETE_WELLMETRICEVENTQUEUE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_WELLMETRICEVENTQUEUE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_WELLMETRICEVENTQUEUE):
    case FAILURE(ACTION_TYPES.CREATE_WELLMETRICEVENTQUEUE):
    case FAILURE(ACTION_TYPES.UPDATE_WELLMETRICEVENTQUEUE):
    case FAILURE(ACTION_TYPES.DELETE_WELLMETRICEVENTQUEUE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_WELLMETRICEVENTQUEUE_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_WELLMETRICEVENTQUEUE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_WELLMETRICEVENTQUEUE):
    case SUCCESS(ACTION_TYPES.UPDATE_WELLMETRICEVENTQUEUE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_WELLMETRICEVENTQUEUE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/wellmetric-event-queues';

// Actions

export const getEntities: ICrudGetAllAction<IWellmetricEventQueue> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_WELLMETRICEVENTQUEUE_LIST,
    payload: axios.get<IWellmetricEventQueue>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IWellmetricEventQueue> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_WELLMETRICEVENTQUEUE,
    payload: axios.get<IWellmetricEventQueue>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IWellmetricEventQueue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_WELLMETRICEVENTQUEUE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IWellmetricEventQueue> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_WELLMETRICEVENTQUEUE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IWellmetricEventQueue> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_WELLMETRICEVENTQUEUE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
