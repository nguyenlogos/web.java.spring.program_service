import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate, ICrudGetAction, ICrudDeleteAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IWellmetricEventQueue } from 'app/shared/model/wellmetric-event-queue.model';
import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './wellmetric-event-queue.reducer';

export interface IWellmetricEventQueueDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class WellmetricEventQueueDeleteDialog extends React.Component<IWellmetricEventQueueDeleteDialogProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  confirmDelete = event => {
    this.props.deleteEntity(this.props.wellmetricEventQueueEntity.id);
    this.handleClose(event);
  };

  handleClose = event => {
    event.stopPropagation();
    this.props.history.goBack();
  };

  render() {
    const { wellmetricEventQueueEntity } = this.props;
    return (
      <Modal isOpen toggle={this.handleClose}>
        <ModalHeader toggle={this.handleClose}>
          <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
        </ModalHeader>
        <ModalBody id="programserviceApp.wellmetricEventQueue.delete.question">
          <Translate
            contentKey="programserviceApp.wellmetricEventQueue.delete.question"
            interpolate={{ id: wellmetricEventQueueEntity.id }}
          >
            Are you sure you want to delete this WellmetricEventQueue?
          </Translate>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.handleClose}>
            <FontAwesomeIcon icon="ban" />
            &nbsp;
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          <Button id="jhi-confirm-delete-wellmetricEventQueue" color="danger" onClick={this.confirmDelete}>
            <FontAwesomeIcon icon="trash" />
            &nbsp;
            <Translate contentKey="entity.action.delete">Delete</Translate>
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapStateToProps = ({ wellmetricEventQueue }: IRootState) => ({
  wellmetricEventQueueEntity: wellmetricEventQueue.entity
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WellmetricEventQueueDeleteDialog);
