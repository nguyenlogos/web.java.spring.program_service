import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './wellmetric-event-queue.reducer';
import { IWellmetricEventQueue } from 'app/shared/model/wellmetric-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IWellmetricEventQueueUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IWellmetricEventQueueUpdateState {
  isNew: boolean;
}

export class WellmetricEventQueueUpdate extends React.Component<IWellmetricEventQueueUpdateProps, IWellmetricEventQueueUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.lastAttemptTime = convertDateTimeToServer(values.lastAttemptTime);
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.attemptedAt = convertDateTimeToServer(values.attemptedAt);

    if (errors.length === 0) {
      const { wellmetricEventQueueEntity } = this.props;
      const entity = {
        ...wellmetricEventQueueEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/wellmetric-event-queue');
  };

  render() {
    const { wellmetricEventQueueEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.wellmetricEventQueue.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.wellmetricEventQueue.home.createOrEditLabel">
                Create or edit a WellmetricEventQueue
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : wellmetricEventQueueEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="wellmetric-event-queue-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="wellmetric-event-queue-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="participantIdLabel" for="wellmetric-event-queue-participantId">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.participantId">Participant Id</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-participantId"
                    type="text"
                    name="participantId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 36, errorMessage: translate('entity.validation.maxlength', { max: 36 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="clientIdLabel" for="wellmetric-event-queue-clientId">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 36, errorMessage: translate('entity.validation.maxlength', { max: 36 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="wellmetric-event-queue-email">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.email">Email</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-email"
                    type="text"
                    name="email"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="errorMessageLabel" for="wellmetric-event-queue-errorMessage">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.errorMessage">Error Message</Translate>
                  </Label>
                  <AvField id="wellmetric-event-queue-errorMessage" type="text" name="errorMessage" />
                </AvGroup>
                <AvGroup>
                  <Label id="resultLabel" for="wellmetric-event-queue-result">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.result">Result</Translate>
                  </Label>
                  <AvInput
                    id="wellmetric-event-queue-result"
                    type="select"
                    className="form-control"
                    name="result"
                    value={(!isNew && wellmetricEventQueueEntity.result) || 'PASS'}
                  >
                    <option value="PASS">
                      <Translate contentKey="programserviceApp.ResultStatus.PASS" />
                    </option>
                    <option value="FAIL">
                      <Translate contentKey="programserviceApp.ResultStatus.FAIL" />
                    </option>
                    <option value="UNAVAILABLE">
                      <Translate contentKey="programserviceApp.ResultStatus.UNAVAILABLE" />
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="eventCodeLabel" for="wellmetric-event-queue-eventCode">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.eventCode">Event Code</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-eventCode"
                    type="text"
                    name="eventCode"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="genderLabel" for="wellmetric-event-queue-gender">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.gender">Gender</Translate>
                  </Label>
                  <AvInput
                    id="wellmetric-event-queue-gender"
                    type="select"
                    className="form-control"
                    name="gender"
                    value={(!isNew && wellmetricEventQueueEntity.gender) || 'MALE'}
                  >
                    <option value="MALE">
                      <Translate contentKey="programserviceApp.Gender.MALE" />
                    </option>
                    <option value="FEMALE">
                      <Translate contentKey="programserviceApp.Gender.FEMALE" />
                    </option>
                    <option value="UNIDENTIFIED">
                      <Translate contentKey="programserviceApp.Gender.UNIDENTIFIED" />
                    </option>
                    <option value="UNDEFINED">
                      <Translate contentKey="programserviceApp.Gender.UNDEFINED" />
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="attemptCountLabel" for="wellmetric-event-queue-attemptCount">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.attemptCount">Attempt Count</Translate>
                  </Label>
                  <AvField id="wellmetric-event-queue-attemptCount" type="string" className="form-control" name="attemptCount" />
                </AvGroup>
                <AvGroup>
                  <Label id="lastAttemptTimeLabel" for="wellmetric-event-queue-lastAttemptTime">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.lastAttemptTime">Last Attempt Time</Translate>
                  </Label>
                  <AvInput
                    id="wellmetric-event-queue-lastAttemptTime"
                    type="datetime-local"
                    className="form-control"
                    name="lastAttemptTime"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.wellmetricEventQueueEntity.lastAttemptTime)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdAtLabel" for="wellmetric-event-queue-createdAt">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.createdAt">Created At</Translate>
                  </Label>
                  <AvInput
                    id="wellmetric-event-queue-createdAt"
                    type="datetime-local"
                    className="form-control"
                    name="createdAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.wellmetricEventQueueEntity.createdAt)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="wellmetric-event-queue-subgroupId">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="wellmetric-event-queue-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="healthyValueLabel" for="wellmetric-event-queue-healthyValue">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.healthyValue">Healthy Value</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-healthyValue"
                    type="string"
                    className="form-control"
                    name="healthyValue"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="executeStatusLabel" for="wellmetric-event-queue-executeStatus">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.executeStatus">Execute Status</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-executeStatus"
                    type="text"
                    name="executeStatus"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="eventIdLabel" for="wellmetric-event-queue-eventId">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.eventId">Event Id</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-eventId"
                    type="text"
                    name="eventId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="hasIncentiveLabel" check>
                    <AvInput id="wellmetric-event-queue-hasIncentive" type="checkbox" className="form-control" name="hasIncentive" />
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.hasIncentive">Has Incentive</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isWaitingPushLabel" check>
                    <AvInput id="wellmetric-event-queue-isWaitingPush" type="checkbox" className="form-control" name="isWaitingPush" />
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.isWaitingPush">Is Waiting Push</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="attemptedAtLabel" for="wellmetric-event-queue-attemptedAt">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.attemptedAt">Attempted At</Translate>
                  </Label>
                  <AvInput
                    id="wellmetric-event-queue-attemptedAt"
                    type="datetime-local"
                    className="form-control"
                    name="attemptedAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.wellmetricEventQueueEntity.attemptedAt)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="programIdLabel" for="wellmetric-event-queue-programId">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.programId">Program Id</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-programId"
                    type="string"
                    className="form-control"
                    name="programId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="eventDateLabel" for="wellmetric-event-queue-eventDate">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.eventDate">Event Date</Translate>
                  </Label>
                  <AvField
                    id="wellmetric-event-queue-eventDate"
                    type="text"
                    name="eventDate"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subCategoryCodeLabel" for="wellmetric-event-queue-subCategoryCode">
                    <Translate contentKey="programserviceApp.wellmetricEventQueue.subCategoryCode">Sub Category Code</Translate>
                  </Label>
                  <AvField id="wellmetric-event-queue-subCategoryCode" type="text" name="subCategoryCode" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/wellmetric-event-queue" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  wellmetricEventQueueEntity: storeState.wellmetricEventQueue.entity,
  loading: storeState.wellmetricEventQueue.loading,
  updating: storeState.wellmetricEventQueue.updating,
  updateSuccess: storeState.wellmetricEventQueue.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WellmetricEventQueueUpdate);
