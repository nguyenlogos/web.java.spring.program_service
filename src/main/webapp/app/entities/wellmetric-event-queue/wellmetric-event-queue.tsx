import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './wellmetric-event-queue.reducer';
import { IWellmetricEventQueue } from 'app/shared/model/wellmetric-event-queue.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IWellmetricEventQueueProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IWellmetricEventQueueState = IPaginationBaseState;

export class WellmetricEventQueue extends React.Component<IWellmetricEventQueueProps, IWellmetricEventQueueState> {
  state: IWellmetricEventQueueState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { wellmetricEventQueueList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="wellmetric-event-queue-heading">
          <Translate contentKey="programserviceApp.wellmetricEventQueue.home.title">Wellmetric Event Queues</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.wellmetricEventQueue.home.createLabel">Create new Wellmetric Event Queue</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('participantId')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.participantId">Participant Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientId')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.clientId">Client Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('email')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.email">Email</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('errorMessage')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.errorMessage">Error Message</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('result')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.result">Result</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventCode')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.eventCode">Event Code</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('gender')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.gender">Gender</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('attemptCount')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.attemptCount">Attempt Count</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lastAttemptTime')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.lastAttemptTime">Last Attempt Time</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('createdAt')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.createdAt">Created At</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('subgroupId')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.subgroupId">Subgroup Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('healthyValue')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.healthyValue">Healthy Value</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('executeStatus')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.executeStatus">Execute Status</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventId')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.eventId">Event Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('hasIncentive')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.hasIncentive">Has Incentive</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isWaitingPush')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.isWaitingPush">Is Waiting Push</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('attemptedAt')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.attemptedAt">Attempted At</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('programId')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.programId">Program Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventDate')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.eventDate">Event Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('subCategoryCode')}>
                  <Translate contentKey="programserviceApp.wellmetricEventQueue.subCategoryCode">Sub Category Code</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {wellmetricEventQueueList.map((wellmetricEventQueue, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${wellmetricEventQueue.id}`} color="link" size="sm">
                      {wellmetricEventQueue.id}
                    </Button>
                  </td>
                  <td>{wellmetricEventQueue.participantId}</td>
                  <td>{wellmetricEventQueue.clientId}</td>
                  <td>{wellmetricEventQueue.email}</td>
                  <td>{wellmetricEventQueue.errorMessage}</td>
                  <td>
                    <Translate contentKey={`programserviceApp.ResultStatus.${wellmetricEventQueue.result}`} />
                  </td>
                  <td>{wellmetricEventQueue.eventCode}</td>
                  <td>
                    <Translate contentKey={`programserviceApp.Gender.${wellmetricEventQueue.gender}`} />
                  </td>
                  <td>{wellmetricEventQueue.attemptCount}</td>
                  <td>
                    <TextFormat type="date" value={wellmetricEventQueue.lastAttemptTime} format={APP_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={wellmetricEventQueue.createdAt} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{wellmetricEventQueue.subgroupId}</td>
                  <td>{wellmetricEventQueue.healthyValue}</td>
                  <td>{wellmetricEventQueue.executeStatus}</td>
                  <td>{wellmetricEventQueue.eventId}</td>
                  <td>{wellmetricEventQueue.hasIncentive ? 'true' : 'false'}</td>
                  <td>{wellmetricEventQueue.isWaitingPush ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={wellmetricEventQueue.attemptedAt} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{wellmetricEventQueue.programId}</td>
                  <td>{wellmetricEventQueue.eventDate}</td>
                  <td>{wellmetricEventQueue.subCategoryCode}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${wellmetricEventQueue.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${wellmetricEventQueue.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${wellmetricEventQueue.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ wellmetricEventQueue }: IRootState) => ({
  wellmetricEventQueueList: wellmetricEventQueue.entities,
  totalItems: wellmetricEventQueue.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WellmetricEventQueue);
