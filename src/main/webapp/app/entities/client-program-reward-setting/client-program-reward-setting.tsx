import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './client-program-reward-setting.reducer';
import { IClientProgramRewardSetting } from 'app/shared/model/client-program-reward-setting.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IClientProgramRewardSettingProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IClientProgramRewardSettingState = IPaginationBaseState;

export class ClientProgramRewardSetting extends React.Component<IClientProgramRewardSettingProps, IClientProgramRewardSettingState> {
  state: IClientProgramRewardSettingState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { clientProgramRewardSettingList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="client-program-reward-setting-heading">
          <Translate contentKey="programserviceApp.clientProgramRewardSetting.home.title">Client Program Reward Settings</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.clientProgramRewardSetting.home.createLabel">
              Create new Client Program Reward Setting
            </Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientId')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientId">Client Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientName')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientName">Client Name</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('updatedDate')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.updatedDate">Updated Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('updatedBy')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.updatedBy">Updated By</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('customerIdentifier')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.customerIdentifier">Customer Identifier</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('accountIdentifier')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.accountIdentifier">Account Identifier</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('accountThreshold')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.accountThreshold">Account Threshold</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('currentBalance')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.currentBalance">Current Balance</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientEmail')}>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientEmail">Client Email</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="programserviceApp.clientProgramRewardSetting.program">Program</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {clientProgramRewardSettingList.map((clientProgramRewardSetting, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${clientProgramRewardSetting.id}`} color="link" size="sm">
                      {clientProgramRewardSetting.id}
                    </Button>
                  </td>
                  <td>{clientProgramRewardSetting.clientId}</td>
                  <td>{clientProgramRewardSetting.clientName}</td>
                  <td>
                    <TextFormat type="date" value={clientProgramRewardSetting.updatedDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{clientProgramRewardSetting.updatedBy}</td>
                  <td>{clientProgramRewardSetting.customerIdentifier}</td>
                  <td>{clientProgramRewardSetting.accountIdentifier}</td>
                  <td>{clientProgramRewardSetting.accountThreshold}</td>
                  <td>{clientProgramRewardSetting.currentBalance}</td>
                  <td>{clientProgramRewardSetting.clientEmail}</td>
                  <td>
                    {clientProgramRewardSetting.programName ? (
                      <Link to={`program/${clientProgramRewardSetting.programId}`}>{clientProgramRewardSetting.programName}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${clientProgramRewardSetting.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${clientProgramRewardSetting.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${clientProgramRewardSetting.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ clientProgramRewardSetting }: IRootState) => ({
  clientProgramRewardSettingList: clientProgramRewardSetting.entities,
  totalItems: clientProgramRewardSetting.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientProgramRewardSetting);
