import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './client-program-reward-setting.reducer';
import { IClientProgramRewardSetting } from 'app/shared/model/client-program-reward-setting.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IClientProgramRewardSettingUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IClientProgramRewardSettingUpdateState {
  isNew: boolean;
  programId: string;
}

export class ClientProgramRewardSettingUpdate extends React.Component<
  IClientProgramRewardSettingUpdateProps,
  IClientProgramRewardSettingUpdateState
> {
  constructor(props) {
    super(props);
    this.state = {
      programId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPrograms();
  }

  saveEntity = (event, errors, values) => {
    values.updatedDate = convertDateTimeToServer(values.updatedDate);

    if (errors.length === 0) {
      const { clientProgramRewardSettingEntity } = this.props;
      const entity = {
        ...clientProgramRewardSettingEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/client-program-reward-setting');
  };

  render() {
    const { clientProgramRewardSettingEntity, programs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.clientProgramRewardSetting.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.clientProgramRewardSetting.home.createOrEditLabel">
                Create or edit a ClientProgramRewardSetting
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : clientProgramRewardSettingEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="client-program-reward-setting-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="client-program-reward-setting-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="clientIdLabel" for="client-program-reward-setting-clientId">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="client-program-reward-setting-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="clientNameLabel" for="client-program-reward-setting-clientName">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientName">Client Name</Translate>
                  </Label>
                  <AvField
                    id="client-program-reward-setting-clientName"
                    type="text"
                    name="clientName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="updatedDateLabel" for="client-program-reward-setting-updatedDate">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.updatedDate">Updated Date</Translate>
                  </Label>
                  <AvInput
                    id="client-program-reward-setting-updatedDate"
                    type="datetime-local"
                    className="form-control"
                    name="updatedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.clientProgramRewardSettingEntity.updatedDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="updatedByLabel" for="client-program-reward-setting-updatedBy">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.updatedBy">Updated By</Translate>
                  </Label>
                  <AvField
                    id="client-program-reward-setting-updatedBy"
                    type="text"
                    name="updatedBy"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="customerIdentifierLabel" for="client-program-reward-setting-customerIdentifier">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.customerIdentifier">Customer Identifier</Translate>
                  </Label>
                  <AvField id="client-program-reward-setting-customerIdentifier" type="text" name="customerIdentifier" />
                </AvGroup>
                <AvGroup>
                  <Label id="accountIdentifierLabel" for="client-program-reward-setting-accountIdentifier">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.accountIdentifier">Account Identifier</Translate>
                  </Label>
                  <AvField id="client-program-reward-setting-accountIdentifier" type="text" name="accountIdentifier" />
                </AvGroup>
                <AvGroup>
                  <Label id="accountThresholdLabel" for="client-program-reward-setting-accountThreshold">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.accountThreshold">Account Threshold</Translate>
                  </Label>
                  <AvField id="client-program-reward-setting-accountThreshold" type="text" name="accountThreshold" />
                </AvGroup>
                <AvGroup>
                  <Label id="currentBalanceLabel" for="client-program-reward-setting-currentBalance">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.currentBalance">Current Balance</Translate>
                  </Label>
                  <AvField id="client-program-reward-setting-currentBalance" type="text" name="currentBalance" />
                </AvGroup>
                <AvGroup>
                  <Label id="clientEmailLabel" for="client-program-reward-setting-clientEmail">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientEmail">Client Email</Translate>
                  </Label>
                  <AvField id="client-program-reward-setting-clientEmail" type="text" name="clientEmail" />
                </AvGroup>
                <AvGroup>
                  <Label for="client-program-reward-setting-program">
                    <Translate contentKey="programserviceApp.clientProgramRewardSetting.program">Program</Translate>
                  </Label>
                  <AvInput id="client-program-reward-setting-program" type="select" className="form-control" name="programId" required>
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/client-program-reward-setting" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programs: storeState.program.entities,
  clientProgramRewardSettingEntity: storeState.clientProgramRewardSetting.entity,
  loading: storeState.clientProgramRewardSetting.loading,
  updating: storeState.clientProgramRewardSetting.updating,
  updateSuccess: storeState.clientProgramRewardSetting.updateSuccess
});

const mapDispatchToProps = {
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientProgramRewardSettingUpdate);
