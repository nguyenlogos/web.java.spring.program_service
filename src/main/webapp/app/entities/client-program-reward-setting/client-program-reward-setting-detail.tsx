import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './client-program-reward-setting.reducer';
import { IClientProgramRewardSetting } from 'app/shared/model/client-program-reward-setting.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IClientProgramRewardSettingDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ClientProgramRewardSettingDetail extends React.Component<IClientProgramRewardSettingDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { clientProgramRewardSettingEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.clientProgramRewardSetting.detail.title">ClientProgramRewardSetting</Translate> [
            <b>{clientProgramRewardSettingEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{clientProgramRewardSettingEntity.clientId}</dd>
            <dt>
              <span id="clientName">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientName">Client Name</Translate>
              </span>
            </dt>
            <dd>{clientProgramRewardSettingEntity.clientName}</dd>
            <dt>
              <span id="updatedDate">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.updatedDate">Updated Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={clientProgramRewardSettingEntity.updatedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="updatedBy">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.updatedBy">Updated By</Translate>
              </span>
            </dt>
            <dd>{clientProgramRewardSettingEntity.updatedBy}</dd>
            <dt>
              <span id="customerIdentifier">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.customerIdentifier">Customer Identifier</Translate>
              </span>
            </dt>
            <dd>{clientProgramRewardSettingEntity.customerIdentifier}</dd>
            <dt>
              <span id="accountIdentifier">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.accountIdentifier">Account Identifier</Translate>
              </span>
            </dt>
            <dd>{clientProgramRewardSettingEntity.accountIdentifier}</dd>
            <dt>
              <span id="accountThreshold">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.accountThreshold">Account Threshold</Translate>
              </span>
            </dt>
            <dd>{clientProgramRewardSettingEntity.accountThreshold}</dd>
            <dt>
              <span id="currentBalance">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.currentBalance">Current Balance</Translate>
              </span>
            </dt>
            <dd>{clientProgramRewardSettingEntity.currentBalance}</dd>
            <dt>
              <span id="clientEmail">
                <Translate contentKey="programserviceApp.clientProgramRewardSetting.clientEmail">Client Email</Translate>
              </span>
            </dt>
            <dd>{clientProgramRewardSettingEntity.clientEmail}</dd>
            <dt>
              <Translate contentKey="programserviceApp.clientProgramRewardSetting.program">Program</Translate>
            </dt>
            <dd>{clientProgramRewardSettingEntity.programName ? clientProgramRewardSettingEntity.programName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/client-program-reward-setting" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button
            tag={Link}
            to={`/entity/client-program-reward-setting/${clientProgramRewardSettingEntity.id}/edit`}
            replace
            color="primary"
          >
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ clientProgramRewardSetting }: IRootState) => ({
  clientProgramRewardSettingEntity: clientProgramRewardSetting.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientProgramRewardSettingDetail);
