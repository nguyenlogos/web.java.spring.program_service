import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IClientProgramRewardSetting, defaultValue } from 'app/shared/model/client-program-reward-setting.model';

export const ACTION_TYPES = {
  FETCH_CLIENTPROGRAMREWARDSETTING_LIST: 'clientProgramRewardSetting/FETCH_CLIENTPROGRAMREWARDSETTING_LIST',
  FETCH_CLIENTPROGRAMREWARDSETTING: 'clientProgramRewardSetting/FETCH_CLIENTPROGRAMREWARDSETTING',
  CREATE_CLIENTPROGRAMREWARDSETTING: 'clientProgramRewardSetting/CREATE_CLIENTPROGRAMREWARDSETTING',
  UPDATE_CLIENTPROGRAMREWARDSETTING: 'clientProgramRewardSetting/UPDATE_CLIENTPROGRAMREWARDSETTING',
  DELETE_CLIENTPROGRAMREWARDSETTING: 'clientProgramRewardSetting/DELETE_CLIENTPROGRAMREWARDSETTING',
  RESET: 'clientProgramRewardSetting/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IClientProgramRewardSetting>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ClientProgramRewardSettingState = Readonly<typeof initialState>;

// Reducer

export default (state: ClientProgramRewardSettingState = initialState, action): ClientProgramRewardSettingState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CLIENTPROGRAMREWARDSETTING_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CLIENTPROGRAMREWARDSETTING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CLIENTPROGRAMREWARDSETTING):
    case REQUEST(ACTION_TYPES.UPDATE_CLIENTPROGRAMREWARDSETTING):
    case REQUEST(ACTION_TYPES.DELETE_CLIENTPROGRAMREWARDSETTING):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CLIENTPROGRAMREWARDSETTING_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CLIENTPROGRAMREWARDSETTING):
    case FAILURE(ACTION_TYPES.CREATE_CLIENTPROGRAMREWARDSETTING):
    case FAILURE(ACTION_TYPES.UPDATE_CLIENTPROGRAMREWARDSETTING):
    case FAILURE(ACTION_TYPES.DELETE_CLIENTPROGRAMREWARDSETTING):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLIENTPROGRAMREWARDSETTING_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLIENTPROGRAMREWARDSETTING):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CLIENTPROGRAMREWARDSETTING):
    case SUCCESS(ACTION_TYPES.UPDATE_CLIENTPROGRAMREWARDSETTING):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CLIENTPROGRAMREWARDSETTING):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/client-program-reward-settings';

// Actions

export const getEntities: ICrudGetAllAction<IClientProgramRewardSetting> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CLIENTPROGRAMREWARDSETTING_LIST,
    payload: axios.get<IClientProgramRewardSetting>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IClientProgramRewardSetting> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CLIENTPROGRAMREWARDSETTING,
    payload: axios.get<IClientProgramRewardSetting>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IClientProgramRewardSetting> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CLIENTPROGRAMREWARDSETTING,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IClientProgramRewardSetting> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CLIENTPROGRAMREWARDSETTING,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IClientProgramRewardSetting> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CLIENTPROGRAMREWARDSETTING,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
