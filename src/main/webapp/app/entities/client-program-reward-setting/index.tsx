import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ClientProgramRewardSetting from './client-program-reward-setting';
import ClientProgramRewardSettingDetail from './client-program-reward-setting-detail';
import ClientProgramRewardSettingUpdate from './client-program-reward-setting-update';
import ClientProgramRewardSettingDeleteDialog from './client-program-reward-setting-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ClientProgramRewardSettingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ClientProgramRewardSettingUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ClientProgramRewardSettingDetail} />
      <ErrorBoundaryRoute path={match.url} component={ClientProgramRewardSetting} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ClientProgramRewardSettingDeleteDialog} />
  </>
);

export default Routes;
