import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-user-logs.reducer';
import { IProgramUserLogs } from 'app/shared/model/program-user-logs.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramUserLogsDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramUserLogsDetail extends React.Component<IProgramUserLogsDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programUserLogsEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programUserLogs.detail.title">ProgramUserLogs</Translate> [
            <b>{programUserLogsEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="participantId">
                <Translate contentKey="programserviceApp.programUserLogs.participantId">Participant Id</Translate>
              </span>
            </dt>
            <dd>{programUserLogsEntity.participantId}</dd>
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.programUserLogs.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{programUserLogsEntity.programId}</dd>
            <dt>
              <span id="code">
                <Translate contentKey="programserviceApp.programUserLogs.code">Code</Translate>
              </span>
            </dt>
            <dd>{programUserLogsEntity.code}</dd>
            <dt>
              <span id="message">
                <Translate contentKey="programserviceApp.programUserLogs.message">Message</Translate>
              </span>
            </dt>
            <dd>{programUserLogsEntity.message}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programUserLogs.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programUserLogsEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programUserLogs.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programUserLogsEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/program-user-logs" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-user-logs/${programUserLogsEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programUserLogs }: IRootState) => ({
  programUserLogsEntity: programUserLogs.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserLogsDetail);
