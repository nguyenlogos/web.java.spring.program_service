import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramUserLogs from './program-user-logs';
import ProgramUserLogsDetail from './program-user-logs-detail';
import ProgramUserLogsUpdate from './program-user-logs-update';
import ProgramUserLogsDeleteDialog from './program-user-logs-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramUserLogsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramUserLogsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramUserLogsDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramUserLogs} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramUserLogsDeleteDialog} />
  </>
);

export default Routes;
