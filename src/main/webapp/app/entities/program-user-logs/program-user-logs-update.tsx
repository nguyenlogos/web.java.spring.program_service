import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './program-user-logs.reducer';
import { IProgramUserLogs } from 'app/shared/model/program-user-logs.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramUserLogsUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramUserLogsUpdateState {
  isNew: boolean;
}

export class ProgramUserLogsUpdate extends React.Component<IProgramUserLogsUpdateProps, IProgramUserLogsUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programUserLogsEntity } = this.props;
      const entity = {
        ...programUserLogsEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-user-logs');
  };

  render() {
    const { programUserLogsEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programUserLogs.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programUserLogs.home.createOrEditLabel">Create or edit a ProgramUserLogs</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programUserLogsEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-user-logs-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-user-logs-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="participantIdLabel" for="program-user-logs-participantId">
                    <Translate contentKey="programserviceApp.programUserLogs.participantId">Participant Id</Translate>
                  </Label>
                  <AvField
                    id="program-user-logs-participantId"
                    type="text"
                    name="participantId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="programIdLabel" for="program-user-logs-programId">
                    <Translate contentKey="programserviceApp.programUserLogs.programId">Program Id</Translate>
                  </Label>
                  <AvField
                    id="program-user-logs-programId"
                    type="string"
                    className="form-control"
                    name="programId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="codeLabel" for="program-user-logs-code">
                    <Translate contentKey="programserviceApp.programUserLogs.code">Code</Translate>
                  </Label>
                  <AvField id="program-user-logs-code" type="text" name="code" />
                </AvGroup>
                <AvGroup>
                  <Label id="messageLabel" for="program-user-logs-message">
                    <Translate contentKey="programserviceApp.programUserLogs.message">Message</Translate>
                  </Label>
                  <AvField id="program-user-logs-message" type="text" name="message" />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-user-logs-createdDate">
                    <Translate contentKey="programserviceApp.programUserLogs.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-user-logs-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programUserLogsEntity.createdDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-user-logs-modifiedDate">
                    <Translate contentKey="programserviceApp.programUserLogs.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-user-logs-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programUserLogsEntity.modifiedDate)}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-user-logs" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programUserLogsEntity: storeState.programUserLogs.entity,
  loading: storeState.programUserLogs.loading,
  updating: storeState.programUserLogs.updating,
  updateSuccess: storeState.programUserLogs.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserLogsUpdate);
