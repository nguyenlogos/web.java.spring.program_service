import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramActivity from './program-activity';
import ProgramActivityDetail from './program-activity-detail';
import ProgramActivityUpdate from './program-activity-update';
import ProgramActivityDeleteDialog from './program-activity-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramActivityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramActivityUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramActivityDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramActivity} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramActivityDeleteDialog} />
  </>
);

export default Routes;
