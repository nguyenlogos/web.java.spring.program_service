import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-activity.reducer';
import { IProgramActivity } from 'app/shared/model/program-activity.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramActivityDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramActivityDetail extends React.Component<IProgramActivityDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programActivityEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programActivity.detail.title">ProgramActivity</Translate> [
            <b>{programActivityEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="activityId">
                <Translate contentKey="programserviceApp.programActivity.activityId">Activity Id</Translate>
              </span>
            </dt>
            <dd>{programActivityEntity.activityId}</dd>
            <dt>
              <span id="activityCode">
                <Translate contentKey="programserviceApp.programActivity.activityCode">Activity Code</Translate>
              </span>
            </dt>
            <dd>{programActivityEntity.activityCode}</dd>
            <dt>
              <span id="isCustomized">
                <Translate contentKey="programserviceApp.programActivity.isCustomized">Is Customized</Translate>
              </span>
            </dt>
            <dd>{programActivityEntity.isCustomized ? 'true' : 'false'}</dd>
            <dt>
              <span id="masterId">
                <Translate contentKey="programserviceApp.programActivity.masterId">Master Id</Translate>
              </span>
            </dt>
            <dd>{programActivityEntity.masterId}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programActivity.program">Program</Translate>
            </dt>
            <dd>{programActivityEntity.programName ? programActivityEntity.programName : ''}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programActivity.programPhase">Program Phase</Translate>
            </dt>
            <dd>{programActivityEntity.programPhasePhaseOrder ? programActivityEntity.programPhasePhaseOrder : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-activity" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-activity/${programActivityEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programActivity }: IRootState) => ({
  programActivityEntity: programActivity.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramActivityDetail);
