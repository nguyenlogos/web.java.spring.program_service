import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { IProgramPhase } from 'app/shared/model/program-phase.model';
import { getEntities as getProgramPhases } from 'app/entities/program-phase/program-phase.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-activity.reducer';
import { IProgramActivity } from 'app/shared/model/program-activity.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramActivityUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramActivityUpdateState {
  isNew: boolean;
  programId: string;
  programPhaseId: string;
}

export class ProgramActivityUpdate extends React.Component<IProgramActivityUpdateProps, IProgramActivityUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programId: '0',
      programPhaseId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPrograms();
    this.props.getProgramPhases();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programActivityEntity } = this.props;
      const entity = {
        ...programActivityEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-activity');
  };

  render() {
    const { programActivityEntity, programs, programPhases, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programActivity.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programActivity.home.createOrEditLabel">Create or edit a ProgramActivity</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programActivityEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-activity-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-activity-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="activityIdLabel" for="program-activity-activityId">
                    <Translate contentKey="programserviceApp.programActivity.activityId">Activity Id</Translate>
                  </Label>
                  <AvField
                    id="program-activity-activityId"
                    type="text"
                    name="activityId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="activityCodeLabel" for="program-activity-activityCode">
                    <Translate contentKey="programserviceApp.programActivity.activityCode">Activity Code</Translate>
                  </Label>
                  <AvField
                    id="program-activity-activityCode"
                    type="text"
                    name="activityCode"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="isCustomizedLabel" check>
                    <AvInput id="program-activity-isCustomized" type="checkbox" className="form-control" name="isCustomized" />
                    <Translate contentKey="programserviceApp.programActivity.isCustomized">Is Customized</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="masterIdLabel" for="program-activity-masterId">
                    <Translate contentKey="programserviceApp.programActivity.masterId">Master Id</Translate>
                  </Label>
                  <AvField id="program-activity-masterId" type="text" name="masterId" />
                </AvGroup>
                <AvGroup>
                  <Label for="program-activity-program">
                    <Translate contentKey="programserviceApp.programActivity.program">Program</Translate>
                  </Label>
                  <AvInput id="program-activity-program" type="select" className="form-control" name="programId" required>
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <AvGroup>
                  <Label for="program-activity-programPhase">
                    <Translate contentKey="programserviceApp.programActivity.programPhase">Program Phase</Translate>
                  </Label>
                  <AvInput id="program-activity-programPhase" type="select" className="form-control" name="programPhaseId" required>
                    {programPhases
                      ? programPhases.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.phaseOrder}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-activity" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programs: storeState.program.entities,
  programPhases: storeState.programPhase.entities,
  programActivityEntity: storeState.programActivity.entity,
  loading: storeState.programActivity.loading,
  updating: storeState.programActivity.updating,
  updateSuccess: storeState.programActivity.updateSuccess
});

const mapDispatchToProps = {
  getPrograms,
  getProgramPhases,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramActivityUpdate);
