import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-reward.reducer';
import { IUserReward } from 'app/shared/model/user-reward.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserRewardDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class UserRewardDetail extends React.Component<IUserRewardDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { userRewardEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.userReward.detail.title">UserReward</Translate> [<b>{userRewardEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="programLevel">
                <Translate contentKey="programserviceApp.userReward.programLevel">Program Level</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.programLevel}</dd>
            <dt>
              <span id="status">
                <Translate contentKey="programserviceApp.userReward.status">Status</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.status}</dd>
            <dt>
              <span id="orderId">
                <Translate contentKey="programserviceApp.userReward.orderId">Order Id</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.orderId}</dd>
            <dt>
              <span id="orderDate">
                <Translate contentKey="programserviceApp.userReward.orderDate">Order Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={userRewardEntity.orderDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="giftId">
                <Translate contentKey="programserviceApp.userReward.giftId">Gift Id</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.giftId}</dd>
            <dt>
              <span id="levelCompletedDate">
                <Translate contentKey="programserviceApp.userReward.levelCompletedDate">Level Completed Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={userRewardEntity.levelCompletedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="participantId">
                <Translate contentKey="programserviceApp.userReward.participantId">Participant Id</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.participantId}</dd>
            <dt>
              <span id="email">
                <Translate contentKey="programserviceApp.userReward.email">Email</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.email}</dd>
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.userReward.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.clientId}</dd>
            <dt>
              <span id="rewardType">
                <Translate contentKey="programserviceApp.userReward.rewardType">Reward Type</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.rewardType}</dd>
            <dt>
              <span id="rewardCode">
                <Translate contentKey="programserviceApp.userReward.rewardCode">Reward Code</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.rewardCode}</dd>
            <dt>
              <span id="rewardAmount">
                <Translate contentKey="programserviceApp.userReward.rewardAmount">Reward Amount</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.rewardAmount}</dd>
            <dt>
              <span id="campaignId">
                <Translate contentKey="programserviceApp.userReward.campaignId">Campaign Id</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.campaignId}</dd>
            <dt>
              <span id="rewardMessage">
                <Translate contentKey="programserviceApp.userReward.rewardMessage">Reward Message</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.rewardMessage}</dd>
            <dt>
              <span id="eventId">
                <Translate contentKey="programserviceApp.userReward.eventId">Event Id</Translate>
              </span>
            </dt>
            <dd>{userRewardEntity.eventId}</dd>
            <dt>
              <Translate contentKey="programserviceApp.userReward.programUser">Program User</Translate>
            </dt>
            <dd>{userRewardEntity.programUserParticipantId ? userRewardEntity.programUserParticipantId : ''}</dd>
            <dt>
              <Translate contentKey="programserviceApp.userReward.program">Program</Translate>
            </dt>
            <dd>{userRewardEntity.programName ? userRewardEntity.programName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/user-reward" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/user-reward/${userRewardEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ userReward }: IRootState) => ({
  userRewardEntity: userReward.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserRewardDetail);
