import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './user-reward.reducer';
import { IUserReward } from 'app/shared/model/user-reward.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IUserRewardProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IUserRewardState = IPaginationBaseState;

export class UserReward extends React.Component<IUserRewardProps, IUserRewardState> {
  state: IUserRewardState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { userRewardList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="user-reward-heading">
          <Translate contentKey="programserviceApp.userReward.home.title">User Rewards</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.userReward.home.createLabel">Create new User Reward</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('programLevel')}>
                  <Translate contentKey="programserviceApp.userReward.programLevel">Program Level</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('status')}>
                  <Translate contentKey="programserviceApp.userReward.status">Status</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('orderId')}>
                  <Translate contentKey="programserviceApp.userReward.orderId">Order Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('orderDate')}>
                  <Translate contentKey="programserviceApp.userReward.orderDate">Order Date</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('giftId')}>
                  <Translate contentKey="programserviceApp.userReward.giftId">Gift Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('levelCompletedDate')}>
                  <Translate contentKey="programserviceApp.userReward.levelCompletedDate">Level Completed Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('participantId')}>
                  <Translate contentKey="programserviceApp.userReward.participantId">Participant Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('email')}>
                  <Translate contentKey="programserviceApp.userReward.email">Email</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('clientId')}>
                  <Translate contentKey="programserviceApp.userReward.clientId">Client Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('rewardType')}>
                  <Translate contentKey="programserviceApp.userReward.rewardType">Reward Type</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('rewardCode')}>
                  <Translate contentKey="programserviceApp.userReward.rewardCode">Reward Code</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('rewardAmount')}>
                  <Translate contentKey="programserviceApp.userReward.rewardAmount">Reward Amount</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('campaignId')}>
                  <Translate contentKey="programserviceApp.userReward.campaignId">Campaign Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('rewardMessage')}>
                  <Translate contentKey="programserviceApp.userReward.rewardMessage">Reward Message</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('eventId')}>
                  <Translate contentKey="programserviceApp.userReward.eventId">Event Id</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="programserviceApp.userReward.programUser">Program User</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="programserviceApp.userReward.program">Program</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {userRewardList.map((userReward, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${userReward.id}`} color="link" size="sm">
                      {userReward.id}
                    </Button>
                  </td>
                  <td>{userReward.programLevel}</td>
                  <td>{userReward.status}</td>
                  <td>{userReward.orderId}</td>
                  <td>
                    <TextFormat type="date" value={userReward.orderDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{userReward.giftId}</td>
                  <td>
                    <TextFormat type="date" value={userReward.levelCompletedDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{userReward.participantId}</td>
                  <td>{userReward.email}</td>
                  <td>{userReward.clientId}</td>
                  <td>{userReward.rewardType}</td>
                  <td>{userReward.rewardCode}</td>
                  <td>{userReward.rewardAmount}</td>
                  <td>{userReward.campaignId}</td>
                  <td>{userReward.rewardMessage}</td>
                  <td>{userReward.eventId}</td>
                  <td>
                    {userReward.programUserParticipantId ? (
                      <Link to={`program-user/${userReward.programUserId}`}>{userReward.programUserParticipantId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>{userReward.programName ? <Link to={`program/${userReward.programId}`}>{userReward.programName}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${userReward.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userReward.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userReward.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ userReward }: IRootState) => ({
  userRewardList: userReward.entities,
  totalItems: userReward.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserReward);
