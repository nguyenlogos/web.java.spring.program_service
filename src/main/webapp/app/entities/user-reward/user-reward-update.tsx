import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramUser } from 'app/shared/model/program-user.model';
import { getEntities as getProgramUsers } from 'app/entities/program-user/program-user.reducer';
import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './user-reward.reducer';
import { IUserReward } from 'app/shared/model/user-reward.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUserRewardUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IUserRewardUpdateState {
  isNew: boolean;
  programUserId: string;
  programId: string;
}

export class UserRewardUpdate extends React.Component<IUserRewardUpdateProps, IUserRewardUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programUserId: '0',
      programId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramUsers();
    this.props.getPrograms();
  }

  saveEntity = (event, errors, values) => {
    values.orderDate = convertDateTimeToServer(values.orderDate);
    values.levelCompletedDate = convertDateTimeToServer(values.levelCompletedDate);

    if (errors.length === 0) {
      const { userRewardEntity } = this.props;
      const entity = {
        ...userRewardEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/user-reward');
  };

  render() {
    const { userRewardEntity, programUsers, programs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.userReward.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.userReward.home.createOrEditLabel">Create or edit a UserReward</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : userRewardEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="user-reward-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="user-reward-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="programLevelLabel" for="user-reward-programLevel">
                    <Translate contentKey="programserviceApp.userReward.programLevel">Program Level</Translate>
                  </Label>
                  <AvField id="user-reward-programLevel" type="string" className="form-control" name="programLevel" />
                </AvGroup>
                <AvGroup>
                  <Label id="statusLabel" for="user-reward-status">
                    <Translate contentKey="programserviceApp.userReward.status">Status</Translate>
                  </Label>
                  <AvField id="user-reward-status" type="text" name="status" />
                </AvGroup>
                <AvGroup>
                  <Label id="orderIdLabel" for="user-reward-orderId">
                    <Translate contentKey="programserviceApp.userReward.orderId">Order Id</Translate>
                  </Label>
                  <AvField id="user-reward-orderId" type="text" name="orderId" />
                </AvGroup>
                <AvGroup>
                  <Label id="orderDateLabel" for="user-reward-orderDate">
                    <Translate contentKey="programserviceApp.userReward.orderDate">Order Date</Translate>
                  </Label>
                  <AvInput
                    id="user-reward-orderDate"
                    type="datetime-local"
                    className="form-control"
                    name="orderDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.userRewardEntity.orderDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="giftIdLabel" for="user-reward-giftId">
                    <Translate contentKey="programserviceApp.userReward.giftId">Gift Id</Translate>
                  </Label>
                  <AvField id="user-reward-giftId" type="text" name="giftId" />
                </AvGroup>
                <AvGroup>
                  <Label id="levelCompletedDateLabel" for="user-reward-levelCompletedDate">
                    <Translate contentKey="programserviceApp.userReward.levelCompletedDate">Level Completed Date</Translate>
                  </Label>
                  <AvInput
                    id="user-reward-levelCompletedDate"
                    type="datetime-local"
                    className="form-control"
                    name="levelCompletedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.userRewardEntity.levelCompletedDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="participantIdLabel" for="user-reward-participantId">
                    <Translate contentKey="programserviceApp.userReward.participantId">Participant Id</Translate>
                  </Label>
                  <AvField
                    id="user-reward-participantId"
                    type="text"
                    name="participantId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="user-reward-email">
                    <Translate contentKey="programserviceApp.userReward.email">Email</Translate>
                  </Label>
                  <AvField
                    id="user-reward-email"
                    type="text"
                    name="email"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="clientIdLabel" for="user-reward-clientId">
                    <Translate contentKey="programserviceApp.userReward.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="user-reward-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="rewardTypeLabel" for="user-reward-rewardType">
                    <Translate contentKey="programserviceApp.userReward.rewardType">Reward Type</Translate>
                  </Label>
                  <AvField id="user-reward-rewardType" type="text" name="rewardType" />
                </AvGroup>
                <AvGroup>
                  <Label id="rewardCodeLabel" for="user-reward-rewardCode">
                    <Translate contentKey="programserviceApp.userReward.rewardCode">Reward Code</Translate>
                  </Label>
                  <AvField id="user-reward-rewardCode" type="text" name="rewardCode" />
                </AvGroup>
                <AvGroup>
                  <Label id="rewardAmountLabel" for="user-reward-rewardAmount">
                    <Translate contentKey="programserviceApp.userReward.rewardAmount">Reward Amount</Translate>
                  </Label>
                  <AvField id="user-reward-rewardAmount" type="text" name="rewardAmount" />
                </AvGroup>
                <AvGroup>
                  <Label id="campaignIdLabel" for="user-reward-campaignId">
                    <Translate contentKey="programserviceApp.userReward.campaignId">Campaign Id</Translate>
                  </Label>
                  <AvField id="user-reward-campaignId" type="text" name="campaignId" />
                </AvGroup>
                <AvGroup>
                  <Label id="rewardMessageLabel" for="user-reward-rewardMessage">
                    <Translate contentKey="programserviceApp.userReward.rewardMessage">Reward Message</Translate>
                  </Label>
                  <AvField id="user-reward-rewardMessage" type="text" name="rewardMessage" />
                </AvGroup>
                <AvGroup>
                  <Label id="eventIdLabel" for="user-reward-eventId">
                    <Translate contentKey="programserviceApp.userReward.eventId">Event Id</Translate>
                  </Label>
                  <AvField id="user-reward-eventId" type="text" name="eventId" />
                </AvGroup>
                <AvGroup>
                  <Label for="user-reward-programUser">
                    <Translate contentKey="programserviceApp.userReward.programUser">Program User</Translate>
                  </Label>
                  <AvInput id="user-reward-programUser" type="select" className="form-control" name="programUserId" required>
                    {programUsers
                      ? programUsers.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.participantId}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <AvGroup>
                  <Label for="user-reward-program">
                    <Translate contentKey="programserviceApp.userReward.program">Program</Translate>
                  </Label>
                  <AvInput id="user-reward-program" type="select" className="form-control" name="programId" required>
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/user-reward" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programUsers: storeState.programUser.entities,
  programs: storeState.program.entities,
  userRewardEntity: storeState.userReward.entity,
  loading: storeState.userReward.loading,
  updating: storeState.userReward.updating,
  updateSuccess: storeState.userReward.updateSuccess
});

const mapDispatchToProps = {
  getProgramUsers,
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserRewardUpdate);
