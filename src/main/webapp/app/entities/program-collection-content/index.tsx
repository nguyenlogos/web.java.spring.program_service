import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramCollectionContent from './program-collection-content';
import ProgramCollectionContentDetail from './program-collection-content-detail';
import ProgramCollectionContentUpdate from './program-collection-content-update';
import ProgramCollectionContentDeleteDialog from './program-collection-content-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramCollectionContentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramCollectionContentUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramCollectionContentDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramCollectionContent} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramCollectionContentDeleteDialog} />
  </>
);

export default Routes;
