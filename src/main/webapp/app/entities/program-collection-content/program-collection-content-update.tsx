import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramCollection } from 'app/shared/model/program-collection.model';
import { getEntities as getProgramCollections } from 'app/entities/program-collection/program-collection.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-collection-content.reducer';
import { IProgramCollectionContent } from 'app/shared/model/program-collection-content.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramCollectionContentUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramCollectionContentUpdateState {
  isNew: boolean;
  programCollectionId: string;
}

export class ProgramCollectionContentUpdate extends React.Component<
  IProgramCollectionContentUpdateProps,
  IProgramCollectionContentUpdateState
> {
  constructor(props) {
    super(props);
    this.state = {
      programCollectionId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramCollections();
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programCollectionContentEntity } = this.props;
      const entity = {
        ...programCollectionContentEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-collection-content');
  };

  render() {
    const { programCollectionContentEntity, programCollections, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programCollectionContent.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programCollectionContent.home.createOrEditLabel">
                Create or edit a ProgramCollectionContent
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programCollectionContentEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-collection-content-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-collection-content-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="itemNameLabel" for="program-collection-content-itemName">
                    <Translate contentKey="programserviceApp.programCollectionContent.itemName">Item Name</Translate>
                  </Label>
                  <AvField
                    id="program-collection-content-itemName"
                    type="text"
                    name="itemName"
                    validate={{
                      maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="itemIdLabel" for="program-collection-content-itemId">
                    <Translate contentKey="programserviceApp.programCollectionContent.itemId">Item Id</Translate>
                  </Label>
                  <AvField
                    id="program-collection-content-itemId"
                    type="text"
                    name="itemId"
                    validate={{
                      maxLength: { value: 100, errorMessage: translate('entity.validation.maxlength', { max: 100 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="itemTypeLabel" for="program-collection-content-itemType">
                    <Translate contentKey="programserviceApp.programCollectionContent.itemType">Item Type</Translate>
                  </Label>
                  <AvField
                    id="program-collection-content-itemType"
                    type="text"
                    name="itemType"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 100, errorMessage: translate('entity.validation.maxlength', { max: 100 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="contentTypeLabel" for="program-collection-content-contentType">
                    <Translate contentKey="programserviceApp.programCollectionContent.contentType">Content Type</Translate>
                  </Label>
                  <AvField
                    id="program-collection-content-contentType"
                    type="text"
                    name="contentType"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 100, errorMessage: translate('entity.validation.maxlength', { max: 100 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="itemIconLabel" for="program-collection-content-itemIcon">
                    <Translate contentKey="programserviceApp.programCollectionContent.itemIcon">Item Icon</Translate>
                  </Label>
                  <AvField
                    id="program-collection-content-itemIcon"
                    type="text"
                    name="itemIcon"
                    validate={{
                      maxLength: { value: 255, errorMessage: translate('entity.validation.maxlength', { max: 255 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="hasRequiredLabel" check>
                    <AvInput id="program-collection-content-hasRequired" type="checkbox" className="form-control" name="hasRequired" />
                    <Translate contentKey="programserviceApp.programCollectionContent.hasRequired">Has Required</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-collection-content-createdDate">
                    <Translate contentKey="programserviceApp.programCollectionContent.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-collection-content-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCollectionContentEntity.createdDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-collection-content-modifiedDate">
                    <Translate contentKey="programserviceApp.programCollectionContent.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-collection-content-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCollectionContentEntity.modifiedDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-collection-content-programCollection">
                    <Translate contentKey="programserviceApp.programCollectionContent.programCollection">Program Collection</Translate>
                  </Label>
                  <AvInput
                    id="program-collection-content-programCollection"
                    type="select"
                    className="form-control"
                    name="programCollectionId"
                  >
                    <option value="" key="0" />
                    {programCollections
                      ? programCollections.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-collection-content" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programCollections: storeState.programCollection.entities,
  programCollectionContentEntity: storeState.programCollectionContent.entity,
  loading: storeState.programCollectionContent.loading,
  updating: storeState.programCollectionContent.updating,
  updateSuccess: storeState.programCollectionContent.updateSuccess
});

const mapDispatchToProps = {
  getProgramCollections,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCollectionContentUpdate);
