import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramCollectionContent, defaultValue } from 'app/shared/model/program-collection-content.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMCOLLECTIONCONTENT_LIST: 'programCollectionContent/FETCH_PROGRAMCOLLECTIONCONTENT_LIST',
  FETCH_PROGRAMCOLLECTIONCONTENT: 'programCollectionContent/FETCH_PROGRAMCOLLECTIONCONTENT',
  CREATE_PROGRAMCOLLECTIONCONTENT: 'programCollectionContent/CREATE_PROGRAMCOLLECTIONCONTENT',
  UPDATE_PROGRAMCOLLECTIONCONTENT: 'programCollectionContent/UPDATE_PROGRAMCOLLECTIONCONTENT',
  DELETE_PROGRAMCOLLECTIONCONTENT: 'programCollectionContent/DELETE_PROGRAMCOLLECTIONCONTENT',
  RESET: 'programCollectionContent/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramCollectionContent>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramCollectionContentState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramCollectionContentState = initialState, action): ProgramCollectionContentState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOLLECTIONCONTENT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOLLECTIONCONTENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMCOLLECTIONCONTENT):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMCOLLECTIONCONTENT):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMCOLLECTIONCONTENT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOLLECTIONCONTENT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOLLECTIONCONTENT):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMCOLLECTIONCONTENT):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMCOLLECTIONCONTENT):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMCOLLECTIONCONTENT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOLLECTIONCONTENT_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOLLECTIONCONTENT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMCOLLECTIONCONTENT):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMCOLLECTIONCONTENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMCOLLECTIONCONTENT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-collection-contents';

// Actions

export const getEntities: ICrudGetAllAction<IProgramCollectionContent> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOLLECTIONCONTENT_LIST,
    payload: axios.get<IProgramCollectionContent>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramCollectionContent> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOLLECTIONCONTENT,
    payload: axios.get<IProgramCollectionContent>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramCollectionContent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMCOLLECTIONCONTENT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramCollectionContent> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMCOLLECTIONCONTENT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramCollectionContent> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMCOLLECTIONCONTENT,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
