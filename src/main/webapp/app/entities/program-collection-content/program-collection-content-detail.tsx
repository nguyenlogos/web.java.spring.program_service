import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-collection-content.reducer';
import { IProgramCollectionContent } from 'app/shared/model/program-collection-content.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCollectionContentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramCollectionContentDetail extends React.Component<IProgramCollectionContentDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programCollectionContentEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programCollectionContent.detail.title">ProgramCollectionContent</Translate> [
            <b>{programCollectionContentEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="itemName">
                <Translate contentKey="programserviceApp.programCollectionContent.itemName">Item Name</Translate>
              </span>
            </dt>
            <dd>{programCollectionContentEntity.itemName}</dd>
            <dt>
              <span id="itemId">
                <Translate contentKey="programserviceApp.programCollectionContent.itemId">Item Id</Translate>
              </span>
            </dt>
            <dd>{programCollectionContentEntity.itemId}</dd>
            <dt>
              <span id="itemType">
                <Translate contentKey="programserviceApp.programCollectionContent.itemType">Item Type</Translate>
              </span>
            </dt>
            <dd>{programCollectionContentEntity.itemType}</dd>
            <dt>
              <span id="contentType">
                <Translate contentKey="programserviceApp.programCollectionContent.contentType">Content Type</Translate>
              </span>
            </dt>
            <dd>{programCollectionContentEntity.contentType}</dd>
            <dt>
              <span id="itemIcon">
                <Translate contentKey="programserviceApp.programCollectionContent.itemIcon">Item Icon</Translate>
              </span>
            </dt>
            <dd>{programCollectionContentEntity.itemIcon}</dd>
            <dt>
              <span id="hasRequired">
                <Translate contentKey="programserviceApp.programCollectionContent.hasRequired">Has Required</Translate>
              </span>
            </dt>
            <dd>{programCollectionContentEntity.hasRequired ? 'true' : 'false'}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programCollectionContent.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCollectionContentEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programCollectionContent.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCollectionContentEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programCollectionContent.programCollection">Program Collection</Translate>
            </dt>
            <dd>{programCollectionContentEntity.programCollectionId ? programCollectionContentEntity.programCollectionId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-collection-content" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-collection-content/${programCollectionContentEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programCollectionContent }: IRootState) => ({
  programCollectionContentEntity: programCollectionContent.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCollectionContentDetail);
