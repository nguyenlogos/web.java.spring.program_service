import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './program-user-cohort.reducer';
import { IProgramUserCohort } from 'app/shared/model/program-user-cohort.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramUserCohortProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramUserCohortState = IPaginationBaseState;

export class ProgramUserCohort extends React.Component<IProgramUserCohortProps, IProgramUserCohortState> {
  state: IProgramUserCohortState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  componentDidUpdate() {
    if (this.props.updateSuccess) {
      this.reset();
    }
  }

  reset = () => {
    this.props.reset();
    this.setState({ activePage: 1 }, () => {
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => {
        this.reset();
      }
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programUserCohortList, match } = this.props;
    return (
      <div>
        <h2 id="program-user-cohort-heading">
          <Translate contentKey="programserviceApp.programUserCohort.home.title">Program User Cohorts</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.programUserCohort.home.createLabel">Create new Program User Cohort</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage - 1 < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('progress')}>
                    <Translate contentKey="programserviceApp.programUserCohort.progress">Progress</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('numberOfPass')}>
                    <Translate contentKey="programserviceApp.programUserCohort.numberOfPass">Number Of Pass</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('numberOfFailure')}>
                    <Translate contentKey="programserviceApp.programUserCohort.numberOfFailure">Number Of Failure</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('status')}>
                    <Translate contentKey="programserviceApp.programUserCohort.status">Status</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('createdDate')}>
                    <Translate contentKey="programserviceApp.programUserCohort.createdDate">Created Date</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('modifiedDate')}>
                    <Translate contentKey="programserviceApp.programUserCohort.modifiedDate">Modified Date</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="programserviceApp.programUserCohort.programUser">Program User</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    <Translate contentKey="programserviceApp.programUserCohort.programCohort">Program Cohort</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {programUserCohortList.map((programUserCohort, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${programUserCohort.id}`} color="link" size="sm">
                        {programUserCohort.id}
                      </Button>
                    </td>
                    <td>{programUserCohort.progress}</td>
                    <td>{programUserCohort.numberOfPass}</td>
                    <td>{programUserCohort.numberOfFailure}</td>
                    <td>
                      <Translate contentKey={`programserviceApp.CohortStatus.${programUserCohort.status}`} />
                    </td>
                    <td>
                      <TextFormat type="date" value={programUserCohort.createdDate} format={APP_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={programUserCohort.modifiedDate} format={APP_DATE_FORMAT} />
                    </td>
                    <td>
                      {programUserCohort.programUserId ? (
                        <Link to={`program-user/${programUserCohort.programUserId}`}>{programUserCohort.programUserId}</Link>
                      ) : (
                        ''
                      )}
                    </td>
                    <td>
                      {programUserCohort.programCohortId ? (
                        <Link to={`program-cohort/${programUserCohort.programCohortId}`}>{programUserCohort.programCohortId}</Link>
                      ) : (
                        ''
                      )}
                    </td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${programUserCohort.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${programUserCohort.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${programUserCohort.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ programUserCohort }: IRootState) => ({
  programUserCohortList: programUserCohort.entities,
  totalItems: programUserCohort.totalItems,
  links: programUserCohort.links,
  entity: programUserCohort.entity,
  updateSuccess: programUserCohort.updateSuccess
});

const mapDispatchToProps = {
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserCohort);
