import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramUserCohort from './program-user-cohort';
import ProgramUserCohortDetail from './program-user-cohort-detail';
import ProgramUserCohortUpdate from './program-user-cohort-update';
import ProgramUserCohortDeleteDialog from './program-user-cohort-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramUserCohortUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramUserCohortUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramUserCohortDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramUserCohort} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramUserCohortDeleteDialog} />
  </>
);

export default Routes;
