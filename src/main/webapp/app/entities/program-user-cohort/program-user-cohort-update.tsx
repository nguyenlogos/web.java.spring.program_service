import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramUser } from 'app/shared/model/program-user.model';
import { getEntities as getProgramUsers } from 'app/entities/program-user/program-user.reducer';
import { IProgramCohort } from 'app/shared/model/program-cohort.model';
import { getEntities as getProgramCohorts } from 'app/entities/program-cohort/program-cohort.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-user-cohort.reducer';
import { IProgramUserCohort } from 'app/shared/model/program-user-cohort.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramUserCohortUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramUserCohortUpdateState {
  isNew: boolean;
  programUserId: string;
  programCohortId: string;
}

export class ProgramUserCohortUpdate extends React.Component<IProgramUserCohortUpdateProps, IProgramUserCohortUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programUserId: '0',
      programCohortId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramUsers();
    this.props.getProgramCohorts();
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programUserCohortEntity } = this.props;
      const entity = {
        ...programUserCohortEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-user-cohort');
  };

  render() {
    const { programUserCohortEntity, programUsers, programCohorts, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programUserCohort.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programUserCohort.home.createOrEditLabel">
                Create or edit a ProgramUserCohort
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programUserCohortEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-user-cohort-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-user-cohort-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="progressLabel" for="program-user-cohort-progress">
                    <Translate contentKey="programserviceApp.programUserCohort.progress">Progress</Translate>
                  </Label>
                  <AvField id="program-user-cohort-progress" type="string" className="form-control" name="progress" />
                </AvGroup>
                <AvGroup>
                  <Label id="numberOfPassLabel" for="program-user-cohort-numberOfPass">
                    <Translate contentKey="programserviceApp.programUserCohort.numberOfPass">Number Of Pass</Translate>
                  </Label>
                  <AvField id="program-user-cohort-numberOfPass" type="string" className="form-control" name="numberOfPass" />
                </AvGroup>
                <AvGroup>
                  <Label id="numberOfFailureLabel" for="program-user-cohort-numberOfFailure">
                    <Translate contentKey="programserviceApp.programUserCohort.numberOfFailure">Number Of Failure</Translate>
                  </Label>
                  <AvField id="program-user-cohort-numberOfFailure" type="string" className="form-control" name="numberOfFailure" />
                </AvGroup>
                <AvGroup>
                  <Label id="statusLabel" for="program-user-cohort-status">
                    <Translate contentKey="programserviceApp.programUserCohort.status">Status</Translate>
                  </Label>
                  <AvInput
                    id="program-user-cohort-status"
                    type="select"
                    className="form-control"
                    name="status"
                    value={(!isNew && programUserCohortEntity.status) || 'INPROGRESS'}
                  >
                    <option value="INPROGRESS">
                      <Translate contentKey="programserviceApp.CohortStatus.INPROGRESS" />
                    </option>
                    <option value="PENDING">
                      <Translate contentKey="programserviceApp.CohortStatus.PENDING" />
                    </option>
                    <option value="COMPLETED">
                      <Translate contentKey="programserviceApp.CohortStatus.COMPLETED" />
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-user-cohort-createdDate">
                    <Translate contentKey="programserviceApp.programUserCohort.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-user-cohort-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programUserCohortEntity.createdDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-user-cohort-modifiedDate">
                    <Translate contentKey="programserviceApp.programUserCohort.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-user-cohort-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programUserCohortEntity.modifiedDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-user-cohort-programUser">
                    <Translate contentKey="programserviceApp.programUserCohort.programUser">Program User</Translate>
                  </Label>
                  <AvInput id="program-user-cohort-programUser" type="select" className="form-control" name="programUserId">
                    <option value="" key="0" />
                    {programUsers
                      ? programUsers.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="program-user-cohort-programCohort">
                    <Translate contentKey="programserviceApp.programUserCohort.programCohort">Program Cohort</Translate>
                  </Label>
                  <AvInput id="program-user-cohort-programCohort" type="select" className="form-control" name="programCohortId">
                    <option value="" key="0" />
                    {programCohorts
                      ? programCohorts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-user-cohort" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programUsers: storeState.programUser.entities,
  programCohorts: storeState.programCohort.entities,
  programUserCohortEntity: storeState.programUserCohort.entity,
  loading: storeState.programUserCohort.loading,
  updating: storeState.programUserCohort.updating,
  updateSuccess: storeState.programUserCohort.updateSuccess
});

const mapDispatchToProps = {
  getProgramUsers,
  getProgramCohorts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserCohortUpdate);
