import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-user-cohort.reducer';
import { IProgramUserCohort } from 'app/shared/model/program-user-cohort.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramUserCohortDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramUserCohortDetail extends React.Component<IProgramUserCohortDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programUserCohortEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programUserCohort.detail.title">ProgramUserCohort</Translate> [
            <b>{programUserCohortEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="progress">
                <Translate contentKey="programserviceApp.programUserCohort.progress">Progress</Translate>
              </span>
            </dt>
            <dd>{programUserCohortEntity.progress}</dd>
            <dt>
              <span id="numberOfPass">
                <Translate contentKey="programserviceApp.programUserCohort.numberOfPass">Number Of Pass</Translate>
              </span>
            </dt>
            <dd>{programUserCohortEntity.numberOfPass}</dd>
            <dt>
              <span id="numberOfFailure">
                <Translate contentKey="programserviceApp.programUserCohort.numberOfFailure">Number Of Failure</Translate>
              </span>
            </dt>
            <dd>{programUserCohortEntity.numberOfFailure}</dd>
            <dt>
              <span id="status">
                <Translate contentKey="programserviceApp.programUserCohort.status">Status</Translate>
              </span>
            </dt>
            <dd>{programUserCohortEntity.status}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programUserCohort.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programUserCohortEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programUserCohort.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programUserCohortEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programUserCohort.programUser">Program User</Translate>
            </dt>
            <dd>{programUserCohortEntity.programUserId ? programUserCohortEntity.programUserId : ''}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programUserCohort.programCohort">Program Cohort</Translate>
            </dt>
            <dd>{programUserCohortEntity.programCohortId ? programUserCohortEntity.programCohortId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-user-cohort" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-user-cohort/${programUserCohortEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programUserCohort }: IRootState) => ({
  programUserCohortEntity: programUserCohort.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserCohortDetail);
