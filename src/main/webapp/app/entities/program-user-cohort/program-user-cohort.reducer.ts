import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramUserCohort, defaultValue } from 'app/shared/model/program-user-cohort.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMUSERCOHORT_LIST: 'programUserCohort/FETCH_PROGRAMUSERCOHORT_LIST',
  FETCH_PROGRAMUSERCOHORT: 'programUserCohort/FETCH_PROGRAMUSERCOHORT',
  CREATE_PROGRAMUSERCOHORT: 'programUserCohort/CREATE_PROGRAMUSERCOHORT',
  UPDATE_PROGRAMUSERCOHORT: 'programUserCohort/UPDATE_PROGRAMUSERCOHORT',
  DELETE_PROGRAMUSERCOHORT: 'programUserCohort/DELETE_PROGRAMUSERCOHORT',
  RESET: 'programUserCohort/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramUserCohort>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramUserCohortState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramUserCohortState = initialState, action): ProgramUserCohortState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMUSERCOHORT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMUSERCOHORT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMUSERCOHORT):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMUSERCOHORT):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMUSERCOHORT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMUSERCOHORT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMUSERCOHORT):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMUSERCOHORT):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMUSERCOHORT):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMUSERCOHORT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMUSERCOHORT_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMUSERCOHORT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMUSERCOHORT):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMUSERCOHORT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMUSERCOHORT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-user-cohorts';

// Actions

export const getEntities: ICrudGetAllAction<IProgramUserCohort> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMUSERCOHORT_LIST,
    payload: axios.get<IProgramUserCohort>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramUserCohort> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMUSERCOHORT,
    payload: axios.get<IProgramUserCohort>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramUserCohort> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMUSERCOHORT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramUserCohort> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMUSERCOHORT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramUserCohort> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMUSERCOHORT,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
