import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import {
  Translate,
  ICrudGetAllAction,
  TextFormat,
  getSortState,
  IPaginationBaseState,
  getPaginationItemsNumber,
  JhiPagination
} from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './program.reducer';
import { IProgram } from 'app/shared/model/program.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramState = IPaginationBaseState;

export class Program extends React.Component<IProgramProps, IProgramState> {
  state: IProgramState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="program-heading">
          <Translate contentKey="programserviceApp.program.home.title">Programs</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.program.home.createLabel">Create new Program</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('name')}>
                  <Translate contentKey="programserviceApp.program.name">Name</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lastSent')}>
                  <Translate contentKey="programserviceApp.program.lastSent">Last Sent</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isRetriggerEmail')}>
                  <Translate contentKey="programserviceApp.program.isRetriggerEmail">Is Retrigger Email</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isEligible')}>
                  <Translate contentKey="programserviceApp.program.isEligible">Is Eligible</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isSentRegistrationEmail')}>
                  <Translate contentKey="programserviceApp.program.isSentRegistrationEmail">Is Sent Registration Email</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isRegisteredForPlatform')}>
                  <Translate contentKey="programserviceApp.program.isRegisteredForPlatform">Is Registered For Platform</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isScheduledScreening')}>
                  <Translate contentKey="programserviceApp.program.isScheduledScreening">Is Scheduled Screening</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isFunctionally')}>
                  <Translate contentKey="programserviceApp.program.isFunctionally">Is Functionally</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('logoUrl')}>
                  <Translate contentKey="programserviceApp.program.logoUrl">Logo Url</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('description')}>
                  <Translate contentKey="programserviceApp.program.description">Description</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('userPoint')}>
                  <Translate contentKey="programserviceApp.program.userPoint">User Point</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lastModifiedDate')}>
                  <Translate contentKey="programserviceApp.program.lastModifiedDate">Last Modified Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('createdDate')}>
                  <Translate contentKey="programserviceApp.program.createdDate">Created Date</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('lastModifiedBy')}>
                  <Translate contentKey="programserviceApp.program.lastModifiedBy">Last Modified By</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('createdBy')}>
                  <Translate contentKey="programserviceApp.program.createdBy">Created By</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('status')}>
                  <Translate contentKey="programserviceApp.program.status">Status</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isUsePoint')}>
                  <Translate contentKey="programserviceApp.program.isUsePoint">Is Use Point</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isScreen')}>
                  <Translate contentKey="programserviceApp.program.isScreen">Is Screen</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isCoaching')}>
                  <Translate contentKey="programserviceApp.program.isCoaching">Is Coaching</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isWellMatric')}>
                  <Translate contentKey="programserviceApp.program.isWellMatric">Is Well Matric</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isHP')}>
                  <Translate contentKey="programserviceApp.program.isHP">Is HP</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isUseLevel')}>
                  <Translate contentKey="programserviceApp.program.isUseLevel">Is Use Level</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('startDate')}>
                  <Translate contentKey="programserviceApp.program.startDate">Start Date</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('resetDate')}>
                  <Translate contentKey="programserviceApp.program.resetDate">Reset Date</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isTemplate')}>
                  <Translate contentKey="programserviceApp.program.isTemplate">Is Template</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isPreview')}>
                  <Translate contentKey="programserviceApp.program.isPreview">Is Preview</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('previewDate')}>
                  <Translate contentKey="programserviceApp.program.previewDate">Preview Date</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('previewTimeZone')}>
                  <Translate contentKey="programserviceApp.program.previewTimeZone">Preview Time Zone</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('startTimeZone')}>
                  <Translate contentKey="programserviceApp.program.startTimeZone">Start Time Zone</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('endTimeZone')}>
                  <Translate contentKey="programserviceApp.program.endTimeZone">End Time Zone</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('levelStructure')}>
                  <Translate contentKey="programserviceApp.program.levelStructure">Level Structure</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('programLength')}>
                  <Translate contentKey="programserviceApp.program.programLength">Program Length</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isHPSF')}>
                  <Translate contentKey="programserviceApp.program.isHPSF">Is HPSF</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isHTK')}>
                  <Translate contentKey="programserviceApp.program.isHTK">Is HTK</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isLabcorp')}>
                  <Translate contentKey="programserviceApp.program.isLabcorp">Is Labcorp</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('labcorpAccountNumber')}>
                  <Translate contentKey="programserviceApp.program.labcorpAccountNumber">Labcorp Account Number</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('labcorpFileUrl')}>
                  <Translate contentKey="programserviceApp.program.labcorpFileUrl">Labcorp File Url</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('applyRewardAllSubgroup')}>
                  <Translate contentKey="programserviceApp.program.applyRewardAllSubgroup">Apply Reward All Subgroup</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('biometricDeadlineDate')}>
                  <Translate contentKey="programserviceApp.program.biometricDeadlineDate">Biometric Deadline Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('biometricLookbackDate')}>
                  <Translate contentKey="programserviceApp.program.biometricLookbackDate">Biometric Lookback Date</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('landingBackgroundImageUrl')}>
                  <Translate contentKey="programserviceApp.program.landingBackgroundImageUrl">Landing Background Image Url</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isTobaccoSurchargeManagement')}>
                  <Translate contentKey="programserviceApp.program.isTobaccoSurchargeManagement">Is Tobacco Surcharge Management</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('tobaccoAttestationDeadline')}>
                  <Translate contentKey="programserviceApp.program.tobaccoAttestationDeadline">Tobacco Attestation Deadline</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('tobaccoProgramDeadline')}>
                  <Translate contentKey="programserviceApp.program.tobaccoProgramDeadline">Tobacco Program Deadline</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isAwardedForTobaccoAttestation')}>
                  <Translate contentKey="programserviceApp.program.isAwardedForTobaccoAttestation">
                    Is Awarded For Tobacco Attestation
                  </Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isAwardedForTobaccoRas')}>
                  <Translate contentKey="programserviceApp.program.isAwardedForTobaccoRas">Is Awarded For Tobacco Ras</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {programList.map((program, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${program.id}`} color="link" size="sm">
                      {program.id}
                    </Button>
                  </td>
                  <td>{program.name}</td>
                  <td>
                    <TextFormat type="date" value={program.lastSent} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{program.isRetriggerEmail ? 'true' : 'false'}</td>
                  <td>{program.isEligible ? 'true' : 'false'}</td>
                  <td>{program.isSentRegistrationEmail ? 'true' : 'false'}</td>
                  <td>{program.isRegisteredForPlatform ? 'true' : 'false'}</td>
                  <td>{program.isScheduledScreening ? 'true' : 'false'}</td>
                  <td>{program.isFunctionally ? 'true' : 'false'}</td>
                  <td>{program.logoUrl}</td>
                  <td>{program.description}</td>
                  <td>{program.userPoint}</td>
                  <td>
                    <TextFormat type="date" value={program.lastModifiedDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={program.createdDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{program.lastModifiedBy}</td>
                  <td>{program.createdBy}</td>
                  <td>{program.status}</td>
                  <td>{program.isUsePoint ? 'true' : 'false'}</td>
                  <td>{program.isScreen ? 'true' : 'false'}</td>
                  <td>{program.isCoaching ? 'true' : 'false'}</td>
                  <td>{program.isWellMatric ? 'true' : 'false'}</td>
                  <td>{program.isHP ? 'true' : 'false'}</td>
                  <td>{program.isUseLevel ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={program.startDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={program.resetDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{program.isTemplate ? 'true' : 'false'}</td>
                  <td>{program.isPreview ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={program.previewDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{program.previewTimeZone}</td>
                  <td>{program.startTimeZone}</td>
                  <td>{program.endTimeZone}</td>
                  <td>{program.levelStructure}</td>
                  <td>{program.programLength}</td>
                  <td>{program.isHPSF ? 'true' : 'false'}</td>
                  <td>{program.isHTK ? 'true' : 'false'}</td>
                  <td>{program.isLabcorp ? 'true' : 'false'}</td>
                  <td>{program.labcorpAccountNumber}</td>
                  <td>{program.labcorpFileUrl}</td>
                  <td>{program.applyRewardAllSubgroup ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={program.biometricDeadlineDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={program.biometricLookbackDate} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{program.landingBackgroundImageUrl}</td>
                  <td>{program.isTobaccoSurchargeManagement ? 'true' : 'false'}</td>
                  <td>
                    <TextFormat type="date" value={program.tobaccoAttestationDeadline} format={APP_DATE_FORMAT} />
                  </td>
                  <td>
                    <TextFormat type="date" value={program.tobaccoProgramDeadline} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{program.isAwardedForTobaccoAttestation ? 'true' : 'false'}</td>
                  <td>{program.isAwardedForTobaccoRas ? 'true' : 'false'}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${program.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${program.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${program.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ program }: IRootState) => ({
  programList: program.entities,
  totalItems: program.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Program);
