import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './program.reducer';
import { IProgram } from 'app/shared/model/program.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramUpdateState {
  isNew: boolean;
}

export class ProgramUpdate extends React.Component<IProgramUpdateProps, IProgramUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.lastModifiedDate = convertDateTimeToServer(values.lastModifiedDate);
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.startDate = convertDateTimeToServer(values.startDate);
    values.resetDate = convertDateTimeToServer(values.resetDate);
    values.previewDate = convertDateTimeToServer(values.previewDate);
    values.biometricDeadlineDate = convertDateTimeToServer(values.biometricDeadlineDate);
    values.biometricLookbackDate = convertDateTimeToServer(values.biometricLookbackDate);
    values.tobaccoAttestationDeadline = convertDateTimeToServer(values.tobaccoAttestationDeadline);
    values.tobaccoProgramDeadline = convertDateTimeToServer(values.tobaccoProgramDeadline);

    if (errors.length === 0) {
      const { programEntity } = this.props;
      const entity = {
        ...programEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program');
  };

  render() {
    const { programEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.program.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.program.home.createOrEditLabel">Create or edit a Program</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameLabel" for="program-name">
                    <Translate contentKey="programserviceApp.program.name">Name</Translate>
                  </Label>
                  <AvField
                    id="program-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="lastSentLabel" for="program-lastSent">
                    <Translate contentKey="programserviceApp.program.lastSent">Last Sent</Translate>
                  </Label>
                  <AvField id="program-lastSent" type="date" className="form-control" name="lastSent" />
                </AvGroup>
                <AvGroup>
                  <Label id="isRetriggerEmailLabel" check>
                    <AvInput id="program-isRetriggerEmail" type="checkbox" className="form-control" name="isRetriggerEmail" />
                    <Translate contentKey="programserviceApp.program.isRetriggerEmail">Is Retrigger Email</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isEligibleLabel" check>
                    <AvInput id="program-isEligible" type="checkbox" className="form-control" name="isEligible" />
                    <Translate contentKey="programserviceApp.program.isEligible">Is Eligible</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isSentRegistrationEmailLabel" check>
                    <AvInput id="program-isSentRegistrationEmail" type="checkbox" className="form-control" name="isSentRegistrationEmail" />
                    <Translate contentKey="programserviceApp.program.isSentRegistrationEmail">Is Sent Registration Email</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isRegisteredForPlatformLabel" check>
                    <AvInput id="program-isRegisteredForPlatform" type="checkbox" className="form-control" name="isRegisteredForPlatform" />
                    <Translate contentKey="programserviceApp.program.isRegisteredForPlatform">Is Registered For Platform</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isScheduledScreeningLabel" check>
                    <AvInput id="program-isScheduledScreening" type="checkbox" className="form-control" name="isScheduledScreening" />
                    <Translate contentKey="programserviceApp.program.isScheduledScreening">Is Scheduled Screening</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isFunctionallyLabel" check>
                    <AvInput id="program-isFunctionally" type="checkbox" className="form-control" name="isFunctionally" />
                    <Translate contentKey="programserviceApp.program.isFunctionally">Is Functionally</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="logoUrlLabel" for="program-logoUrl">
                    <Translate contentKey="programserviceApp.program.logoUrl">Logo Url</Translate>
                  </Label>
                  <AvField id="program-logoUrl" type="text" name="logoUrl" />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="program-description">
                    <Translate contentKey="programserviceApp.program.description">Description</Translate>
                  </Label>
                  <AvField id="program-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="userPointLabel" for="program-userPoint">
                    <Translate contentKey="programserviceApp.program.userPoint">User Point</Translate>
                  </Label>
                  <AvField id="program-userPoint" type="text" name="userPoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="lastModifiedDateLabel" for="program-lastModifiedDate">
                    <Translate contentKey="programserviceApp.program.lastModifiedDate">Last Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-lastModifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="lastModifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.lastModifiedDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-createdDate">
                    <Translate contentKey="programserviceApp.program.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.createdDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="lastModifiedByLabel" for="program-lastModifiedBy">
                    <Translate contentKey="programserviceApp.program.lastModifiedBy">Last Modified By</Translate>
                  </Label>
                  <AvField id="program-lastModifiedBy" type="text" name="lastModifiedBy" />
                </AvGroup>
                <AvGroup>
                  <Label id="createdByLabel" for="program-createdBy">
                    <Translate contentKey="programserviceApp.program.createdBy">Created By</Translate>
                  </Label>
                  <AvField id="program-createdBy" type="text" name="createdBy" />
                </AvGroup>
                <AvGroup>
                  <Label id="statusLabel" for="program-status">
                    <Translate contentKey="programserviceApp.program.status">Status</Translate>
                  </Label>
                  <AvField id="program-status" type="text" name="status" />
                </AvGroup>
                <AvGroup>
                  <Label id="isUsePointLabel" check>
                    <AvInput id="program-isUsePoint" type="checkbox" className="form-control" name="isUsePoint" />
                    <Translate contentKey="programserviceApp.program.isUsePoint">Is Use Point</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isScreenLabel" check>
                    <AvInput id="program-isScreen" type="checkbox" className="form-control" name="isScreen" />
                    <Translate contentKey="programserviceApp.program.isScreen">Is Screen</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isCoachingLabel" check>
                    <AvInput id="program-isCoaching" type="checkbox" className="form-control" name="isCoaching" />
                    <Translate contentKey="programserviceApp.program.isCoaching">Is Coaching</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isWellMatricLabel" check>
                    <AvInput id="program-isWellMatric" type="checkbox" className="form-control" name="isWellMatric" />
                    <Translate contentKey="programserviceApp.program.isWellMatric">Is Well Matric</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isHPLabel" check>
                    <AvInput id="program-isHP" type="checkbox" className="form-control" name="isHP" />
                    <Translate contentKey="programserviceApp.program.isHP">Is HP</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isUseLevelLabel" check>
                    <AvInput id="program-isUseLevel" type="checkbox" className="form-control" name="isUseLevel" />
                    <Translate contentKey="programserviceApp.program.isUseLevel">Is Use Level</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="startDateLabel" for="program-startDate">
                    <Translate contentKey="programserviceApp.program.startDate">Start Date</Translate>
                  </Label>
                  <AvInput
                    id="program-startDate"
                    type="datetime-local"
                    className="form-control"
                    name="startDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.startDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="resetDateLabel" for="program-resetDate">
                    <Translate contentKey="programserviceApp.program.resetDate">Reset Date</Translate>
                  </Label>
                  <AvInput
                    id="program-resetDate"
                    type="datetime-local"
                    className="form-control"
                    name="resetDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.resetDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="isTemplateLabel" check>
                    <AvInput id="program-isTemplate" type="checkbox" className="form-control" name="isTemplate" />
                    <Translate contentKey="programserviceApp.program.isTemplate">Is Template</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isPreviewLabel" check>
                    <AvInput id="program-isPreview" type="checkbox" className="form-control" name="isPreview" />
                    <Translate contentKey="programserviceApp.program.isPreview">Is Preview</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="previewDateLabel" for="program-previewDate">
                    <Translate contentKey="programserviceApp.program.previewDate">Preview Date</Translate>
                  </Label>
                  <AvInput
                    id="program-previewDate"
                    type="datetime-local"
                    className="form-control"
                    name="previewDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.previewDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="previewTimeZoneLabel" for="program-previewTimeZone">
                    <Translate contentKey="programserviceApp.program.previewTimeZone">Preview Time Zone</Translate>
                  </Label>
                  <AvField id="program-previewTimeZone" type="text" name="previewTimeZone" />
                </AvGroup>
                <AvGroup>
                  <Label id="startTimeZoneLabel" for="program-startTimeZone">
                    <Translate contentKey="programserviceApp.program.startTimeZone">Start Time Zone</Translate>
                  </Label>
                  <AvField id="program-startTimeZone" type="text" name="startTimeZone" />
                </AvGroup>
                <AvGroup>
                  <Label id="endTimeZoneLabel" for="program-endTimeZone">
                    <Translate contentKey="programserviceApp.program.endTimeZone">End Time Zone</Translate>
                  </Label>
                  <AvField id="program-endTimeZone" type="text" name="endTimeZone" />
                </AvGroup>
                <AvGroup>
                  <Label id="levelStructureLabel" for="program-levelStructure">
                    <Translate contentKey="programserviceApp.program.levelStructure">Level Structure</Translate>
                  </Label>
                  <AvField id="program-levelStructure" type="text" name="levelStructure" />
                </AvGroup>
                <AvGroup>
                  <Label id="programLengthLabel" for="program-programLength">
                    <Translate contentKey="programserviceApp.program.programLength">Program Length</Translate>
                  </Label>
                  <AvField id="program-programLength" type="string" className="form-control" name="programLength" />
                </AvGroup>
                <AvGroup>
                  <Label id="isHPSFLabel" check>
                    <AvInput id="program-isHPSF" type="checkbox" className="form-control" name="isHPSF" />
                    <Translate contentKey="programserviceApp.program.isHPSF">Is HPSF</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isHTKLabel" check>
                    <AvInput id="program-isHTK" type="checkbox" className="form-control" name="isHTK" />
                    <Translate contentKey="programserviceApp.program.isHTK">Is HTK</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isLabcorpLabel" check>
                    <AvInput id="program-isLabcorp" type="checkbox" className="form-control" name="isLabcorp" />
                    <Translate contentKey="programserviceApp.program.isLabcorp">Is Labcorp</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="labcorpAccountNumberLabel" for="program-labcorpAccountNumber">
                    <Translate contentKey="programserviceApp.program.labcorpAccountNumber">Labcorp Account Number</Translate>
                  </Label>
                  <AvField id="program-labcorpAccountNumber" type="text" name="labcorpAccountNumber" />
                </AvGroup>
                <AvGroup>
                  <Label id="labcorpFileUrlLabel" for="program-labcorpFileUrl">
                    <Translate contentKey="programserviceApp.program.labcorpFileUrl">Labcorp File Url</Translate>
                  </Label>
                  <AvField id="program-labcorpFileUrl" type="text" name="labcorpFileUrl" />
                </AvGroup>
                <AvGroup>
                  <Label id="applyRewardAllSubgroupLabel" check>
                    <AvInput id="program-applyRewardAllSubgroup" type="checkbox" className="form-control" name="applyRewardAllSubgroup" />
                    <Translate contentKey="programserviceApp.program.applyRewardAllSubgroup">Apply Reward All Subgroup</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="biometricDeadlineDateLabel" for="program-biometricDeadlineDate">
                    <Translate contentKey="programserviceApp.program.biometricDeadlineDate">Biometric Deadline Date</Translate>
                  </Label>
                  <AvInput
                    id="program-biometricDeadlineDate"
                    type="datetime-local"
                    className="form-control"
                    name="biometricDeadlineDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.biometricDeadlineDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="biometricLookbackDateLabel" for="program-biometricLookbackDate">
                    <Translate contentKey="programserviceApp.program.biometricLookbackDate">Biometric Lookback Date</Translate>
                  </Label>
                  <AvInput
                    id="program-biometricLookbackDate"
                    type="datetime-local"
                    className="form-control"
                    name="biometricLookbackDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.biometricLookbackDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="landingBackgroundImageUrlLabel" for="program-landingBackgroundImageUrl">
                    <Translate contentKey="programserviceApp.program.landingBackgroundImageUrl">Landing Background Image Url</Translate>
                  </Label>
                  <AvField id="program-landingBackgroundImageUrl" type="text" name="landingBackgroundImageUrl" />
                </AvGroup>
                <AvGroup>
                  <Label id="isTobaccoSurchargeManagementLabel" check>
                    <AvInput
                      id="program-isTobaccoSurchargeManagement"
                      type="checkbox"
                      className="form-control"
                      name="isTobaccoSurchargeManagement"
                    />
                    <Translate contentKey="programserviceApp.program.isTobaccoSurchargeManagement">
                      Is Tobacco Surcharge Management
                    </Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="tobaccoAttestationDeadlineLabel" for="program-tobaccoAttestationDeadline">
                    <Translate contentKey="programserviceApp.program.tobaccoAttestationDeadline">Tobacco Attestation Deadline</Translate>
                  </Label>
                  <AvInput
                    id="program-tobaccoAttestationDeadline"
                    type="datetime-local"
                    className="form-control"
                    name="tobaccoAttestationDeadline"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.tobaccoAttestationDeadline)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="tobaccoProgramDeadlineLabel" for="program-tobaccoProgramDeadline">
                    <Translate contentKey="programserviceApp.program.tobaccoProgramDeadline">Tobacco Program Deadline</Translate>
                  </Label>
                  <AvInput
                    id="program-tobaccoProgramDeadline"
                    type="datetime-local"
                    className="form-control"
                    name="tobaccoProgramDeadline"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programEntity.tobaccoProgramDeadline)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="isAwardedForTobaccoAttestationLabel" check>
                    <AvInput
                      id="program-isAwardedForTobaccoAttestation"
                      type="checkbox"
                      className="form-control"
                      name="isAwardedForTobaccoAttestation"
                    />
                    <Translate contentKey="programserviceApp.program.isAwardedForTobaccoAttestation">
                      Is Awarded For Tobacco Attestation
                    </Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="isAwardedForTobaccoRasLabel" check>
                    <AvInput id="program-isAwardedForTobaccoRas" type="checkbox" className="form-control" name="isAwardedForTobaccoRas" />
                    <Translate contentKey="programserviceApp.program.isAwardedForTobaccoRas">Is Awarded For Tobacco Ras</Translate>
                  </Label>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programEntity: storeState.program.entity,
  loading: storeState.program.loading,
  updating: storeState.program.updating,
  updateSuccess: storeState.program.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUpdate);
