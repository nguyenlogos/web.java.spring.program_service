import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program.reducer';
import { IProgram } from 'app/shared/model/program.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramDetail extends React.Component<IProgramDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.program.detail.title">Program</Translate> [<b>{programEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="programserviceApp.program.name">Name</Translate>
              </span>
            </dt>
            <dd>{programEntity.name}</dd>
            <dt>
              <span id="lastSent">
                <Translate contentKey="programserviceApp.program.lastSent">Last Sent</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.lastSent} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="isRetriggerEmail">
                <Translate contentKey="programserviceApp.program.isRetriggerEmail">Is Retrigger Email</Translate>
              </span>
            </dt>
            <dd>{programEntity.isRetriggerEmail ? 'true' : 'false'}</dd>
            <dt>
              <span id="isEligible">
                <Translate contentKey="programserviceApp.program.isEligible">Is Eligible</Translate>
              </span>
            </dt>
            <dd>{programEntity.isEligible ? 'true' : 'false'}</dd>
            <dt>
              <span id="isSentRegistrationEmail">
                <Translate contentKey="programserviceApp.program.isSentRegistrationEmail">Is Sent Registration Email</Translate>
              </span>
            </dt>
            <dd>{programEntity.isSentRegistrationEmail ? 'true' : 'false'}</dd>
            <dt>
              <span id="isRegisteredForPlatform">
                <Translate contentKey="programserviceApp.program.isRegisteredForPlatform">Is Registered For Platform</Translate>
              </span>
            </dt>
            <dd>{programEntity.isRegisteredForPlatform ? 'true' : 'false'}</dd>
            <dt>
              <span id="isScheduledScreening">
                <Translate contentKey="programserviceApp.program.isScheduledScreening">Is Scheduled Screening</Translate>
              </span>
            </dt>
            <dd>{programEntity.isScheduledScreening ? 'true' : 'false'}</dd>
            <dt>
              <span id="isFunctionally">
                <Translate contentKey="programserviceApp.program.isFunctionally">Is Functionally</Translate>
              </span>
            </dt>
            <dd>{programEntity.isFunctionally ? 'true' : 'false'}</dd>
            <dt>
              <span id="logoUrl">
                <Translate contentKey="programserviceApp.program.logoUrl">Logo Url</Translate>
              </span>
            </dt>
            <dd>{programEntity.logoUrl}</dd>
            <dt>
              <span id="description">
                <Translate contentKey="programserviceApp.program.description">Description</Translate>
              </span>
            </dt>
            <dd>{programEntity.description}</dd>
            <dt>
              <span id="userPoint">
                <Translate contentKey="programserviceApp.program.userPoint">User Point</Translate>
              </span>
            </dt>
            <dd>{programEntity.userPoint}</dd>
            <dt>
              <span id="lastModifiedDate">
                <Translate contentKey="programserviceApp.program.lastModifiedDate">Last Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.lastModifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.program.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="lastModifiedBy">
                <Translate contentKey="programserviceApp.program.lastModifiedBy">Last Modified By</Translate>
              </span>
            </dt>
            <dd>{programEntity.lastModifiedBy}</dd>
            <dt>
              <span id="createdBy">
                <Translate contentKey="programserviceApp.program.createdBy">Created By</Translate>
              </span>
            </dt>
            <dd>{programEntity.createdBy}</dd>
            <dt>
              <span id="status">
                <Translate contentKey="programserviceApp.program.status">Status</Translate>
              </span>
            </dt>
            <dd>{programEntity.status}</dd>
            <dt>
              <span id="isUsePoint">
                <Translate contentKey="programserviceApp.program.isUsePoint">Is Use Point</Translate>
              </span>
            </dt>
            <dd>{programEntity.isUsePoint ? 'true' : 'false'}</dd>
            <dt>
              <span id="isScreen">
                <Translate contentKey="programserviceApp.program.isScreen">Is Screen</Translate>
              </span>
            </dt>
            <dd>{programEntity.isScreen ? 'true' : 'false'}</dd>
            <dt>
              <span id="isCoaching">
                <Translate contentKey="programserviceApp.program.isCoaching">Is Coaching</Translate>
              </span>
            </dt>
            <dd>{programEntity.isCoaching ? 'true' : 'false'}</dd>
            <dt>
              <span id="isWellMatric">
                <Translate contentKey="programserviceApp.program.isWellMatric">Is Well Matric</Translate>
              </span>
            </dt>
            <dd>{programEntity.isWellMatric ? 'true' : 'false'}</dd>
            <dt>
              <span id="isHP">
                <Translate contentKey="programserviceApp.program.isHP">Is HP</Translate>
              </span>
            </dt>
            <dd>{programEntity.isHP ? 'true' : 'false'}</dd>
            <dt>
              <span id="isUseLevel">
                <Translate contentKey="programserviceApp.program.isUseLevel">Is Use Level</Translate>
              </span>
            </dt>
            <dd>{programEntity.isUseLevel ? 'true' : 'false'}</dd>
            <dt>
              <span id="startDate">
                <Translate contentKey="programserviceApp.program.startDate">Start Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.startDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="resetDate">
                <Translate contentKey="programserviceApp.program.resetDate">Reset Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.resetDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="isTemplate">
                <Translate contentKey="programserviceApp.program.isTemplate">Is Template</Translate>
              </span>
            </dt>
            <dd>{programEntity.isTemplate ? 'true' : 'false'}</dd>
            <dt>
              <span id="isPreview">
                <Translate contentKey="programserviceApp.program.isPreview">Is Preview</Translate>
              </span>
            </dt>
            <dd>{programEntity.isPreview ? 'true' : 'false'}</dd>
            <dt>
              <span id="previewDate">
                <Translate contentKey="programserviceApp.program.previewDate">Preview Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.previewDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="previewTimeZone">
                <Translate contentKey="programserviceApp.program.previewTimeZone">Preview Time Zone</Translate>
              </span>
            </dt>
            <dd>{programEntity.previewTimeZone}</dd>
            <dt>
              <span id="startTimeZone">
                <Translate contentKey="programserviceApp.program.startTimeZone">Start Time Zone</Translate>
              </span>
            </dt>
            <dd>{programEntity.startTimeZone}</dd>
            <dt>
              <span id="endTimeZone">
                <Translate contentKey="programserviceApp.program.endTimeZone">End Time Zone</Translate>
              </span>
            </dt>
            <dd>{programEntity.endTimeZone}</dd>
            <dt>
              <span id="levelStructure">
                <Translate contentKey="programserviceApp.program.levelStructure">Level Structure</Translate>
              </span>
            </dt>
            <dd>{programEntity.levelStructure}</dd>
            <dt>
              <span id="programLength">
                <Translate contentKey="programserviceApp.program.programLength">Program Length</Translate>
              </span>
            </dt>
            <dd>{programEntity.programLength}</dd>
            <dt>
              <span id="isHPSF">
                <Translate contentKey="programserviceApp.program.isHPSF">Is HPSF</Translate>
              </span>
            </dt>
            <dd>{programEntity.isHPSF ? 'true' : 'false'}</dd>
            <dt>
              <span id="isHTK">
                <Translate contentKey="programserviceApp.program.isHTK">Is HTK</Translate>
              </span>
            </dt>
            <dd>{programEntity.isHTK ? 'true' : 'false'}</dd>
            <dt>
              <span id="isLabcorp">
                <Translate contentKey="programserviceApp.program.isLabcorp">Is Labcorp</Translate>
              </span>
            </dt>
            <dd>{programEntity.isLabcorp ? 'true' : 'false'}</dd>
            <dt>
              <span id="labcorpAccountNumber">
                <Translate contentKey="programserviceApp.program.labcorpAccountNumber">Labcorp Account Number</Translate>
              </span>
            </dt>
            <dd>{programEntity.labcorpAccountNumber}</dd>
            <dt>
              <span id="labcorpFileUrl">
                <Translate contentKey="programserviceApp.program.labcorpFileUrl">Labcorp File Url</Translate>
              </span>
            </dt>
            <dd>{programEntity.labcorpFileUrl}</dd>
            <dt>
              <span id="applyRewardAllSubgroup">
                <Translate contentKey="programserviceApp.program.applyRewardAllSubgroup">Apply Reward All Subgroup</Translate>
              </span>
            </dt>
            <dd>{programEntity.applyRewardAllSubgroup ? 'true' : 'false'}</dd>
            <dt>
              <span id="biometricDeadlineDate">
                <Translate contentKey="programserviceApp.program.biometricDeadlineDate">Biometric Deadline Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.biometricDeadlineDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="biometricLookbackDate">
                <Translate contentKey="programserviceApp.program.biometricLookbackDate">Biometric Lookback Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.biometricLookbackDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="landingBackgroundImageUrl">
                <Translate contentKey="programserviceApp.program.landingBackgroundImageUrl">Landing Background Image Url</Translate>
              </span>
            </dt>
            <dd>{programEntity.landingBackgroundImageUrl}</dd>
            <dt>
              <span id="isTobaccoSurchargeManagement">
                <Translate contentKey="programserviceApp.program.isTobaccoSurchargeManagement">Is Tobacco Surcharge Management</Translate>
              </span>
            </dt>
            <dd>{programEntity.isTobaccoSurchargeManagement ? 'true' : 'false'}</dd>
            <dt>
              <span id="tobaccoAttestationDeadline">
                <Translate contentKey="programserviceApp.program.tobaccoAttestationDeadline">Tobacco Attestation Deadline</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.tobaccoAttestationDeadline} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="tobaccoProgramDeadline">
                <Translate contentKey="programserviceApp.program.tobaccoProgramDeadline">Tobacco Program Deadline</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programEntity.tobaccoProgramDeadline} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="isAwardedForTobaccoAttestation">
                <Translate contentKey="programserviceApp.program.isAwardedForTobaccoAttestation">
                  Is Awarded For Tobacco Attestation
                </Translate>
              </span>
            </dt>
            <dd>{programEntity.isAwardedForTobaccoAttestation ? 'true' : 'false'}</dd>
            <dt>
              <span id="isAwardedForTobaccoRas">
                <Translate contentKey="programserviceApp.program.isAwardedForTobaccoRas">Is Awarded For Tobacco Ras</Translate>
              </span>
            </dt>
            <dd>{programEntity.isAwardedForTobaccoRas ? 'true' : 'false'}</dd>
          </dl>
          <Button tag={Link} to="/entity/program" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program/${programEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ program }: IRootState) => ({
  programEntity: program.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramDetail);
