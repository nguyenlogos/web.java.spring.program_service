import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramSubCategoryConfiguration from './program-sub-category-configuration';
import ProgramSubCategoryConfigurationDetail from './program-sub-category-configuration-detail';
import ProgramSubCategoryConfigurationUpdate from './program-sub-category-configuration-update';
import ProgramSubCategoryConfigurationDeleteDialog from './program-sub-category-configuration-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramSubCategoryConfigurationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramSubCategoryConfigurationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramSubCategoryConfigurationDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramSubCategoryConfiguration} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramSubCategoryConfigurationDeleteDialog} />
  </>
);

export default Routes;
