import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-sub-category-configuration.reducer';
import { IProgramSubCategoryConfiguration } from 'app/shared/model/program-sub-category-configuration.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramSubCategoryConfigurationDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramSubCategoryConfigurationDetail extends React.Component<IProgramSubCategoryConfigurationDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programSubCategoryConfigurationEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programSubCategoryConfiguration.detail.title">
              ProgramSubCategoryConfiguration
            </Translate>{' '}
            [<b>{programSubCategoryConfigurationEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="subCategoryCode">
                <Translate contentKey="programserviceApp.programSubCategoryConfiguration.subCategoryCode">Sub Category Code</Translate>
              </span>
            </dt>
            <dd>{programSubCategoryConfigurationEntity.subCategoryCode}</dd>
            <dt>
              <span id="isBloodPressureSingleTest">
                <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBloodPressureSingleTest">
                  Is Blood Pressure Single Test
                </Translate>
              </span>
            </dt>
            <dd>{programSubCategoryConfigurationEntity.isBloodPressureSingleTest ? 'true' : 'false'}</dd>
            <dt>
              <span id="isBloodPressureIndividualTest">
                <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBloodPressureIndividualTest">
                  Is Blood Pressure Individual Test
                </Translate>
              </span>
            </dt>
            <dd>{programSubCategoryConfigurationEntity.isBloodPressureIndividualTest ? 'true' : 'false'}</dd>
            <dt>
              <span id="isGlucoseAwardedInRange">
                <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isGlucoseAwardedInRange">
                  Is Glucose Awarded In Range
                </Translate>
              </span>
            </dt>
            <dd>{programSubCategoryConfigurationEntity.isGlucoseAwardedInRange ? 'true' : 'false'}</dd>
            <dt>
              <span id="isBmiAwardedInRange">
                <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBmiAwardedInRange">
                  Is Bmi Awarded In Range
                </Translate>
              </span>
            </dt>
            <dd>{programSubCategoryConfigurationEntity.isBmiAwardedInRange ? 'true' : 'false'}</dd>
            <dt>
              <span id="isBmiAwardedFasting">
                <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBmiAwardedFasting">
                  Is Bmi Awarded Fasting
                </Translate>
              </span>
            </dt>
            <dd>{programSubCategoryConfigurationEntity.isBmiAwardedFasting ? 'true' : 'false'}</dd>
            <dt>
              <span id="isBmiAwardedNonFasting">
                <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBmiAwardedNonFasting">
                  Is Bmi Awarded Non Fasting
                </Translate>
              </span>
            </dt>
            <dd>{programSubCategoryConfigurationEntity.isBmiAwardedNonFasting ? 'true' : 'false'}</dd>
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.programSubCategoryConfiguration.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{programSubCategoryConfigurationEntity.programId}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-sub-category-configuration" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button
            tag={Link}
            to={`/entity/program-sub-category-configuration/${programSubCategoryConfigurationEntity.id}/edit`}
            replace
            color="primary"
          >
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programSubCategoryConfiguration }: IRootState) => ({
  programSubCategoryConfigurationEntity: programSubCategoryConfiguration.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramSubCategoryConfigurationDetail);
