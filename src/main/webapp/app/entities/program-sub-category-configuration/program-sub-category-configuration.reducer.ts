import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramSubCategoryConfiguration, defaultValue } from 'app/shared/model/program-sub-category-configuration.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMSUBCATEGORYCONFIGURATION_LIST: 'programSubCategoryConfiguration/FETCH_PROGRAMSUBCATEGORYCONFIGURATION_LIST',
  FETCH_PROGRAMSUBCATEGORYCONFIGURATION: 'programSubCategoryConfiguration/FETCH_PROGRAMSUBCATEGORYCONFIGURATION',
  CREATE_PROGRAMSUBCATEGORYCONFIGURATION: 'programSubCategoryConfiguration/CREATE_PROGRAMSUBCATEGORYCONFIGURATION',
  UPDATE_PROGRAMSUBCATEGORYCONFIGURATION: 'programSubCategoryConfiguration/UPDATE_PROGRAMSUBCATEGORYCONFIGURATION',
  DELETE_PROGRAMSUBCATEGORYCONFIGURATION: 'programSubCategoryConfiguration/DELETE_PROGRAMSUBCATEGORYCONFIGURATION',
  RESET: 'programSubCategoryConfiguration/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramSubCategoryConfiguration>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramSubCategoryConfigurationState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramSubCategoryConfigurationState = initialState, action): ProgramSubCategoryConfigurationState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYCONFIGURATION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYCONFIGURATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMSUBCATEGORYCONFIGURATION):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMSUBCATEGORYCONFIGURATION):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMSUBCATEGORYCONFIGURATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYCONFIGURATION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYCONFIGURATION):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMSUBCATEGORYCONFIGURATION):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMSUBCATEGORYCONFIGURATION):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMSUBCATEGORYCONFIGURATION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYCONFIGURATION_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYCONFIGURATION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMSUBCATEGORYCONFIGURATION):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMSUBCATEGORYCONFIGURATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMSUBCATEGORYCONFIGURATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-sub-category-configurations';

// Actions

export const getEntities: ICrudGetAllAction<IProgramSubCategoryConfiguration> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYCONFIGURATION_LIST,
    payload: axios.get<IProgramSubCategoryConfiguration>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramSubCategoryConfiguration> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMSUBCATEGORYCONFIGURATION,
    payload: axios.get<IProgramSubCategoryConfiguration>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramSubCategoryConfiguration> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMSUBCATEGORYCONFIGURATION,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramSubCategoryConfiguration> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMSUBCATEGORYCONFIGURATION,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramSubCategoryConfiguration> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMSUBCATEGORYCONFIGURATION,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
