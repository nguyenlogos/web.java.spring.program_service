import React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { Translate, ICrudGetAction, ICrudDeleteAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IProgramSubCategoryConfiguration } from 'app/shared/model/program-sub-category-configuration.model';
import { IRootState } from 'app/shared/reducers';
import { getEntity, deleteEntity } from './program-sub-category-configuration.reducer';

export interface IProgramSubCategoryConfigurationDeleteDialogProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramSubCategoryConfigurationDeleteDialog extends React.Component<IProgramSubCategoryConfigurationDeleteDialogProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  confirmDelete = event => {
    this.props.deleteEntity(this.props.programSubCategoryConfigurationEntity.id);
    this.handleClose(event);
  };

  handleClose = event => {
    event.stopPropagation();
    this.props.history.goBack();
  };

  render() {
    const { programSubCategoryConfigurationEntity } = this.props;
    return (
      <Modal isOpen toggle={this.handleClose}>
        <ModalHeader toggle={this.handleClose}>
          <Translate contentKey="entity.delete.title">Confirm delete operation</Translate>
        </ModalHeader>
        <ModalBody id="programserviceApp.programSubCategoryConfiguration.delete.question">
          <Translate
            contentKey="programserviceApp.programSubCategoryConfiguration.delete.question"
            interpolate={{ id: programSubCategoryConfigurationEntity.id }}
          >
            Are you sure you want to delete this ProgramSubCategoryConfiguration?
          </Translate>
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.handleClose}>
            <FontAwesomeIcon icon="ban" />
            &nbsp;
            <Translate contentKey="entity.action.cancel">Cancel</Translate>
          </Button>
          <Button id="jhi-confirm-delete-programSubCategoryConfiguration" color="danger" onClick={this.confirmDelete}>
            <FontAwesomeIcon icon="trash" />
            &nbsp;
            <Translate contentKey="entity.action.delete">Delete</Translate>
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapStateToProps = ({ programSubCategoryConfiguration }: IRootState) => ({
  programSubCategoryConfigurationEntity: programSubCategoryConfiguration.entity
});

const mapDispatchToProps = { getEntity, deleteEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramSubCategoryConfigurationDeleteDialog);
