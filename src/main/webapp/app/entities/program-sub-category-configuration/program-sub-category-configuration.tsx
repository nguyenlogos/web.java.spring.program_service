import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './program-sub-category-configuration.reducer';
import { IProgramSubCategoryConfiguration } from 'app/shared/model/program-sub-category-configuration.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramSubCategoryConfigurationProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramSubCategoryConfigurationState = IPaginationBaseState;

export class ProgramSubCategoryConfiguration extends React.Component<
  IProgramSubCategoryConfigurationProps,
  IProgramSubCategoryConfigurationState
> {
  state: IProgramSubCategoryConfigurationState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programSubCategoryConfigurationList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="program-sub-category-configuration-heading">
          <Translate contentKey="programserviceApp.programSubCategoryConfiguration.home.title">
            Program Sub Category Configurations
          </Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.programSubCategoryConfiguration.home.createLabel">
              Create new Program Sub Category Configuration
            </Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('subCategoryCode')}>
                  <Translate contentKey="programserviceApp.programSubCategoryConfiguration.subCategoryCode">Sub Category Code</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isBloodPressureSingleTest')}>
                  <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBloodPressureSingleTest">
                    Is Blood Pressure Single Test
                  </Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isBloodPressureIndividualTest')}>
                  <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBloodPressureIndividualTest">
                    Is Blood Pressure Individual Test
                  </Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isGlucoseAwardedInRange')}>
                  <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isGlucoseAwardedInRange">
                    Is Glucose Awarded In Range
                  </Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isBmiAwardedInRange')}>
                  <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBmiAwardedInRange">
                    Is Bmi Awarded In Range
                  </Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isBmiAwardedFasting')}>
                  <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBmiAwardedFasting">
                    Is Bmi Awarded Fasting
                  </Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isBmiAwardedNonFasting')}>
                  <Translate contentKey="programserviceApp.programSubCategoryConfiguration.isBmiAwardedNonFasting">
                    Is Bmi Awarded Non Fasting
                  </Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('programId')}>
                  <Translate contentKey="programserviceApp.programSubCategoryConfiguration.programId">Program Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {programSubCategoryConfigurationList.map((programSubCategoryConfiguration, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${programSubCategoryConfiguration.id}`} color="link" size="sm">
                      {programSubCategoryConfiguration.id}
                    </Button>
                  </td>
                  <td>{programSubCategoryConfiguration.subCategoryCode}</td>
                  <td>{programSubCategoryConfiguration.isBloodPressureSingleTest ? 'true' : 'false'}</td>
                  <td>{programSubCategoryConfiguration.isBloodPressureIndividualTest ? 'true' : 'false'}</td>
                  <td>{programSubCategoryConfiguration.isGlucoseAwardedInRange ? 'true' : 'false'}</td>
                  <td>{programSubCategoryConfiguration.isBmiAwardedInRange ? 'true' : 'false'}</td>
                  <td>{programSubCategoryConfiguration.isBmiAwardedFasting ? 'true' : 'false'}</td>
                  <td>{programSubCategoryConfiguration.isBmiAwardedNonFasting ? 'true' : 'false'}</td>
                  <td>{programSubCategoryConfiguration.programId}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${programSubCategoryConfiguration.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programSubCategoryConfiguration.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programSubCategoryConfiguration.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ programSubCategoryConfiguration }: IRootState) => ({
  programSubCategoryConfigurationList: programSubCategoryConfiguration.entities,
  totalItems: programSubCategoryConfiguration.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramSubCategoryConfiguration);
