import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './order-transaction.reducer';
import { IOrderTransaction } from 'app/shared/model/order-transaction.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOrderTransactionDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class OrderTransactionDetail extends React.Component<IOrderTransactionDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { orderTransactionEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.orderTransaction.detail.title">OrderTransaction</Translate> [
            <b>{orderTransactionEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="programName">
                <Translate contentKey="programserviceApp.orderTransaction.programName">Program Name</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.programName}</dd>
            <dt>
              <span id="clientName">
                <Translate contentKey="programserviceApp.orderTransaction.clientName">Client Name</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.clientName}</dd>
            <dt>
              <span id="participantName">
                <Translate contentKey="programserviceApp.orderTransaction.participantName">Participant Name</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.participantName}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.orderTransaction.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={orderTransactionEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="message">
                <Translate contentKey="programserviceApp.orderTransaction.message">Message</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.message}</dd>
            <dt>
              <span id="jsonContent">
                <Translate contentKey="programserviceApp.orderTransaction.jsonContent">Json Content</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.jsonContent}</dd>
            <dt>
              <span id="email">
                <Translate contentKey="programserviceApp.orderTransaction.email">Email</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.email}</dd>
            <dt>
              <span id="externalOrderId">
                <Translate contentKey="programserviceApp.orderTransaction.externalOrderId">External Order Id</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.externalOrderId}</dd>
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.orderTransaction.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.programId}</dd>
            <dt>
              <span id="userRewardId">
                <Translate contentKey="programserviceApp.orderTransaction.userRewardId">User Reward Id</Translate>
              </span>
            </dt>
            <dd>{orderTransactionEntity.userRewardId}</dd>
          </dl>
          <Button tag={Link} to="/entity/order-transaction" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/order-transaction/${orderTransactionEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ orderTransaction }: IRootState) => ({
  orderTransactionEntity: orderTransaction.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderTransactionDetail);
