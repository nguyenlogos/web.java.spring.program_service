import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './order-transaction.reducer';
import { IOrderTransaction } from 'app/shared/model/order-transaction.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IOrderTransactionUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IOrderTransactionUpdateState {
  isNew: boolean;
}

export class OrderTransactionUpdate extends React.Component<IOrderTransactionUpdateProps, IOrderTransactionUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);

    if (errors.length === 0) {
      const { orderTransactionEntity } = this.props;
      const entity = {
        ...orderTransactionEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/order-transaction');
  };

  render() {
    const { orderTransactionEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.orderTransaction.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.orderTransaction.home.createOrEditLabel">
                Create or edit a OrderTransaction
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : orderTransactionEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="order-transaction-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="order-transaction-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="programNameLabel" for="order-transaction-programName">
                    <Translate contentKey="programserviceApp.orderTransaction.programName">Program Name</Translate>
                  </Label>
                  <AvField id="order-transaction-programName" type="text" name="programName" />
                </AvGroup>
                <AvGroup>
                  <Label id="clientNameLabel" for="order-transaction-clientName">
                    <Translate contentKey="programserviceApp.orderTransaction.clientName">Client Name</Translate>
                  </Label>
                  <AvField id="order-transaction-clientName" type="text" name="clientName" />
                </AvGroup>
                <AvGroup>
                  <Label id="participantNameLabel" for="order-transaction-participantName">
                    <Translate contentKey="programserviceApp.orderTransaction.participantName">Participant Name</Translate>
                  </Label>
                  <AvField id="order-transaction-participantName" type="text" name="participantName" />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="order-transaction-createdDate">
                    <Translate contentKey="programserviceApp.orderTransaction.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="order-transaction-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.orderTransactionEntity.createdDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="messageLabel" for="order-transaction-message">
                    <Translate contentKey="programserviceApp.orderTransaction.message">Message</Translate>
                  </Label>
                  <AvField id="order-transaction-message" type="text" name="message" />
                </AvGroup>
                <AvGroup>
                  <Label id="jsonContentLabel" for="order-transaction-jsonContent">
                    <Translate contentKey="programserviceApp.orderTransaction.jsonContent">Json Content</Translate>
                  </Label>
                  <AvField id="order-transaction-jsonContent" type="text" name="jsonContent" />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="order-transaction-email">
                    <Translate contentKey="programserviceApp.orderTransaction.email">Email</Translate>
                  </Label>
                  <AvField id="order-transaction-email" type="text" name="email" />
                </AvGroup>
                <AvGroup>
                  <Label id="externalOrderIdLabel" for="order-transaction-externalOrderId">
                    <Translate contentKey="programserviceApp.orderTransaction.externalOrderId">External Order Id</Translate>
                  </Label>
                  <AvField id="order-transaction-externalOrderId" type="text" name="externalOrderId" />
                </AvGroup>
                <AvGroup>
                  <Label id="programIdLabel" for="order-transaction-programId">
                    <Translate contentKey="programserviceApp.orderTransaction.programId">Program Id</Translate>
                  </Label>
                  <AvField id="order-transaction-programId" type="string" className="form-control" name="programId" />
                </AvGroup>
                <AvGroup>
                  <Label id="userRewardIdLabel" for="order-transaction-userRewardId">
                    <Translate contentKey="programserviceApp.orderTransaction.userRewardId">User Reward Id</Translate>
                  </Label>
                  <AvField id="order-transaction-userRewardId" type="string" className="form-control" name="userRewardId" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/order-transaction" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  orderTransactionEntity: storeState.orderTransaction.entity,
  loading: storeState.orderTransaction.loading,
  updating: storeState.orderTransaction.updating,
  updateSuccess: storeState.orderTransaction.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderTransactionUpdate);
