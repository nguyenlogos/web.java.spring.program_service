import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-level-path.reducer';
import { IProgramLevelPath } from 'app/shared/model/program-level-path.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramLevelPathDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramLevelPathDetail extends React.Component<IProgramLevelPathDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programLevelPathEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programLevelPath.detail.title">ProgramLevelPath</Translate> [
            <b>{programLevelPathEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="pathId">
                <Translate contentKey="programserviceApp.programLevelPath.pathId">Path Id</Translate>
              </span>
            </dt>
            <dd>{programLevelPathEntity.pathId}</dd>
            <dt>
              <span id="name">
                <Translate contentKey="programserviceApp.programLevelPath.name">Name</Translate>
              </span>
            </dt>
            <dd>{programLevelPathEntity.name}</dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.programLevelPath.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{programLevelPathEntity.subgroupId}</dd>
            <dt>
              <span id="subgroupName">
                <Translate contentKey="programserviceApp.programLevelPath.subgroupName">Subgroup Name</Translate>
              </span>
            </dt>
            <dd>{programLevelPathEntity.subgroupName}</dd>
            <dt>
              <span id="pathType">
                <Translate contentKey="programserviceApp.programLevelPath.pathType">Path Type</Translate>
              </span>
            </dt>
            <dd>{programLevelPathEntity.pathType}</dd>
            <dt>
              <span id="pathCategory">
                <Translate contentKey="programserviceApp.programLevelPath.pathCategory">Path Category</Translate>
              </span>
            </dt>
            <dd>{programLevelPathEntity.pathCategory}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programLevelPath.programLevel">Program Level</Translate>
            </dt>
            <dd>{programLevelPathEntity.programLevelName ? programLevelPathEntity.programLevelName : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-level-path" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-level-path/${programLevelPathEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programLevelPath }: IRootState) => ({
  programLevelPathEntity: programLevelPath.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelPathDetail);
