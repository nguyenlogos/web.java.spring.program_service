import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramLevelPath, defaultValue } from 'app/shared/model/program-level-path.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMLEVELPATH_LIST: 'programLevelPath/FETCH_PROGRAMLEVELPATH_LIST',
  FETCH_PROGRAMLEVELPATH: 'programLevelPath/FETCH_PROGRAMLEVELPATH',
  CREATE_PROGRAMLEVELPATH: 'programLevelPath/CREATE_PROGRAMLEVELPATH',
  UPDATE_PROGRAMLEVELPATH: 'programLevelPath/UPDATE_PROGRAMLEVELPATH',
  DELETE_PROGRAMLEVELPATH: 'programLevelPath/DELETE_PROGRAMLEVELPATH',
  RESET: 'programLevelPath/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramLevelPath>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramLevelPathState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramLevelPathState = initialState, action): ProgramLevelPathState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVELPATH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVELPATH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMLEVELPATH):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMLEVELPATH):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMLEVELPATH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVELPATH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVELPATH):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMLEVELPATH):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMLEVELPATH):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMLEVELPATH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVELPATH_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVELPATH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMLEVELPATH):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMLEVELPATH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMLEVELPATH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-level-paths';

// Actions

export const getEntities: ICrudGetAllAction<IProgramLevelPath> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVELPATH_LIST,
    payload: axios.get<IProgramLevelPath>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramLevelPath> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVELPATH,
    payload: axios.get<IProgramLevelPath>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramLevelPath> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMLEVELPATH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramLevelPath> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMLEVELPATH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramLevelPath> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMLEVELPATH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
