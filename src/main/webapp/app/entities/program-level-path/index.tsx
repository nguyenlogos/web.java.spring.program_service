import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramLevelPath from './program-level-path';
import ProgramLevelPathDetail from './program-level-path-detail';
import ProgramLevelPathUpdate from './program-level-path-update';
import ProgramLevelPathDeleteDialog from './program-level-path-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramLevelPathUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramLevelPathUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramLevelPathDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramLevelPath} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramLevelPathDeleteDialog} />
  </>
);

export default Routes;
