import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgramLevel } from 'app/shared/model/program-level.model';
import { getEntities as getProgramLevels } from 'app/entities/program-level/program-level.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-level-path.reducer';
import { IProgramLevelPath } from 'app/shared/model/program-level-path.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramLevelPathUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramLevelPathUpdateState {
  isNew: boolean;
  programLevelId: string;
}

export class ProgramLevelPathUpdate extends React.Component<IProgramLevelPathUpdateProps, IProgramLevelPathUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programLevelId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getProgramLevels();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programLevelPathEntity } = this.props;
      const entity = {
        ...programLevelPathEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-level-path');
  };

  render() {
    const { programLevelPathEntity, programLevels, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programLevelPath.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programLevelPath.home.createOrEditLabel">
                Create or edit a ProgramLevelPath
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programLevelPathEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-level-path-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-level-path-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="pathIdLabel" for="program-level-path-pathId">
                    <Translate contentKey="programserviceApp.programLevelPath.pathId">Path Id</Translate>
                  </Label>
                  <AvField
                    id="program-level-path-pathId"
                    type="text"
                    name="pathId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="nameLabel" for="program-level-path-name">
                    <Translate contentKey="programserviceApp.programLevelPath.name">Name</Translate>
                  </Label>
                  <AvField
                    id="program-level-path-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="program-level-path-subgroupId">
                    <Translate contentKey="programserviceApp.programLevelPath.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="program-level-path-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupNameLabel" for="program-level-path-subgroupName">
                    <Translate contentKey="programserviceApp.programLevelPath.subgroupName">Subgroup Name</Translate>
                  </Label>
                  <AvField id="program-level-path-subgroupName" type="text" name="subgroupName" />
                </AvGroup>
                <AvGroup>
                  <Label id="pathTypeLabel" for="program-level-path-pathType">
                    <Translate contentKey="programserviceApp.programLevelPath.pathType">Path Type</Translate>
                  </Label>
                  <AvField id="program-level-path-pathType" type="text" name="pathType" />
                </AvGroup>
                <AvGroup>
                  <Label id="pathCategoryLabel" for="program-level-path-pathCategory">
                    <Translate contentKey="programserviceApp.programLevelPath.pathCategory">Path Category</Translate>
                  </Label>
                  <AvField id="program-level-path-pathCategory" type="text" name="pathCategory" />
                </AvGroup>
                <AvGroup>
                  <Label for="program-level-path-programLevel">
                    <Translate contentKey="programserviceApp.programLevelPath.programLevel">Program Level</Translate>
                  </Label>
                  <AvInput id="program-level-path-programLevel" type="select" className="form-control" name="programLevelId" required>
                    {programLevels
                      ? programLevels.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.name}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-level-path" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programLevels: storeState.programLevel.entities,
  programLevelPathEntity: storeState.programLevelPath.entity,
  loading: storeState.programLevelPath.loading,
  updating: storeState.programLevelPath.updating,
  updateSuccess: storeState.programLevelPath.updateSuccess
});

const mapDispatchToProps = {
  getProgramLevels,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelPathUpdate);
