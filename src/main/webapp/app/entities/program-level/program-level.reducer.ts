import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramLevel, defaultValue } from 'app/shared/model/program-level.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMLEVEL_LIST: 'programLevel/FETCH_PROGRAMLEVEL_LIST',
  FETCH_PROGRAMLEVEL: 'programLevel/FETCH_PROGRAMLEVEL',
  CREATE_PROGRAMLEVEL: 'programLevel/CREATE_PROGRAMLEVEL',
  UPDATE_PROGRAMLEVEL: 'programLevel/UPDATE_PROGRAMLEVEL',
  DELETE_PROGRAMLEVEL: 'programLevel/DELETE_PROGRAMLEVEL',
  RESET: 'programLevel/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramLevel>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramLevelState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramLevelState = initialState, action): ProgramLevelState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVEL_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMLEVEL):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMLEVEL):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMLEVEL):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMLEVEL):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVEL_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMLEVEL):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMLEVEL):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMLEVEL):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMLEVEL):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVEL_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMLEVEL):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMLEVEL):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMLEVEL):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMLEVEL):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-levels';

// Actions

export const getEntities: ICrudGetAllAction<IProgramLevel> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVEL_LIST,
    payload: axios.get<IProgramLevel>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramLevel> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMLEVEL,
    payload: axios.get<IProgramLevel>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramLevel> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMLEVEL,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramLevel> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMLEVEL,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramLevel> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMLEVEL,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
