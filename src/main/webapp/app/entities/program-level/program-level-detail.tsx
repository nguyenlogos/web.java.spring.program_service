import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-level.reducer';
import { IProgramLevel } from 'app/shared/model/program-level.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramLevelDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramLevelDetail extends React.Component<IProgramLevelDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programLevelEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programLevel.detail.title">ProgramLevel</Translate> [<b>{programLevelEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="description">
                <Translate contentKey="programserviceApp.programLevel.description">Description</Translate>
              </span>
            </dt>
            <dd>{programLevelEntity.description}</dd>
            <dt>
              <span id="startPoint">
                <Translate contentKey="programserviceApp.programLevel.startPoint">Start Point</Translate>
              </span>
            </dt>
            <dd>{programLevelEntity.startPoint}</dd>
            <dt>
              <span id="endPoint">
                <Translate contentKey="programserviceApp.programLevel.endPoint">End Point</Translate>
              </span>
            </dt>
            <dd>{programLevelEntity.endPoint}</dd>
            <dt>
              <span id="levelOrder">
                <Translate contentKey="programserviceApp.programLevel.levelOrder">Level Order</Translate>
              </span>
            </dt>
            <dd>{programLevelEntity.levelOrder}</dd>
            <dt>
              <span id="iconPath">
                <Translate contentKey="programserviceApp.programLevel.iconPath">Icon Path</Translate>
              </span>
            </dt>
            <dd>{programLevelEntity.iconPath}</dd>
            <dt>
              <span id="name">
                <Translate contentKey="programserviceApp.programLevel.name">Name</Translate>
              </span>
            </dt>
            <dd>{programLevelEntity.name}</dd>
            <dt>
              <span id="endDate">
                <Translate contentKey="programserviceApp.programLevel.endDate">End Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programLevelEntity.endDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="programserviceApp.programLevel.program">Program</Translate>
            </dt>
            <dd>{programLevelEntity.programId ? programLevelEntity.programId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-level" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-level/${programLevelEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programLevel }: IRootState) => ({
  programLevelEntity: programLevel.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelDetail);
