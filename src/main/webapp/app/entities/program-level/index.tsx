import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramLevel from './program-level';
import ProgramLevelDetail from './program-level-detail';
import ProgramLevelUpdate from './program-level-update';
import ProgramLevelDeleteDialog from './program-level-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramLevelUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramLevelUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramLevelDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramLevel} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramLevelDeleteDialog} />
  </>
);

export default Routes;
