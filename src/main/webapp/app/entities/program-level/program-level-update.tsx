import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-level.reducer';
import { IProgramLevel } from 'app/shared/model/program-level.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramLevelUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramLevelUpdateState {
  isNew: boolean;
  programId: string;
}

export class ProgramLevelUpdate extends React.Component<IProgramLevelUpdateProps, IProgramLevelUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPrograms();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programLevelEntity } = this.props;
      const entity = {
        ...programLevelEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-level');
  };

  render() {
    const { programLevelEntity, programs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programLevel.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programLevel.home.createOrEditLabel">Create or edit a ProgramLevel</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programLevelEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-level-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-level-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="descriptionLabel" for="program-level-description">
                    <Translate contentKey="programserviceApp.programLevel.description">Description</Translate>
                  </Label>
                  <AvField id="program-level-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="startPointLabel" for="program-level-startPoint">
                    <Translate contentKey="programserviceApp.programLevel.startPoint">Start Point</Translate>
                  </Label>
                  <AvField id="program-level-startPoint" type="string" className="form-control" name="startPoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="endPointLabel" for="program-level-endPoint">
                    <Translate contentKey="programserviceApp.programLevel.endPoint">End Point</Translate>
                  </Label>
                  <AvField id="program-level-endPoint" type="string" className="form-control" name="endPoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="levelOrderLabel" for="program-level-levelOrder">
                    <Translate contentKey="programserviceApp.programLevel.levelOrder">Level Order</Translate>
                  </Label>
                  <AvField id="program-level-levelOrder" type="string" className="form-control" name="levelOrder" />
                </AvGroup>
                <AvGroup>
                  <Label id="iconPathLabel" for="program-level-iconPath">
                    <Translate contentKey="programserviceApp.programLevel.iconPath">Icon Path</Translate>
                  </Label>
                  <AvField id="program-level-iconPath" type="text" name="iconPath" />
                </AvGroup>
                <AvGroup>
                  <Label id="nameLabel" for="program-level-name">
                    <Translate contentKey="programserviceApp.programLevel.name">Name</Translate>
                  </Label>
                  <AvField id="program-level-name" type="text" name="name" />
                </AvGroup>
                <AvGroup>
                  <Label id="endDateLabel" for="program-level-endDate">
                    <Translate contentKey="programserviceApp.programLevel.endDate">End Date</Translate>
                  </Label>
                  <AvField id="program-level-endDate" type="date" className="form-control" name="endDate" />
                </AvGroup>
                <AvGroup>
                  <Label for="program-level-program">
                    <Translate contentKey="programserviceApp.programLevel.program">Program</Translate>
                  </Label>
                  <AvInput id="program-level-program" type="select" className="form-control" name="programId" required>
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                  <AvFeedback>
                    <Translate contentKey="entity.validation.required">This field is required.</Translate>
                  </AvFeedback>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-level" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programs: storeState.program.entities,
  programLevelEntity: storeState.programLevel.entity,
  loading: storeState.programLevel.loading,
  updating: storeState.programLevel.updating,
  updateSuccess: storeState.programLevel.updateSuccess
});

const mapDispatchToProps = {
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramLevelUpdate);
