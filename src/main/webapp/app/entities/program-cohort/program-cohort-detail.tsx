import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-cohort.reducer';
import { IProgramCohort } from 'app/shared/model/program-cohort.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCohortDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramCohortDetail extends React.Component<IProgramCohortDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programCohortEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programCohort.detail.title">ProgramCohort</Translate> [<b>{programCohortEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="cohortName">
                <Translate contentKey="programserviceApp.programCohort.cohortName">Cohort Name</Translate>
              </span>
            </dt>
            <dd>{programCohortEntity.cohortName}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programCohort.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCohortEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programCohort.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCohortEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="isDefault">
                <Translate contentKey="programserviceApp.programCohort.isDefault">Is Default</Translate>
              </span>
            </dt>
            <dd>{programCohortEntity.isDefault ? 'true' : 'false'}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programCohort.program">Program</Translate>
            </dt>
            <dd>{programCohortEntity.programId ? programCohortEntity.programId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-cohort" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-cohort/${programCohortEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programCohort }: IRootState) => ({
  programCohortEntity: programCohort.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortDetail);
