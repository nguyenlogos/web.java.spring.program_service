import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramCohort, defaultValue } from 'app/shared/model/program-cohort.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMCOHORT_LIST: 'programCohort/FETCH_PROGRAMCOHORT_LIST',
  FETCH_PROGRAMCOHORT: 'programCohort/FETCH_PROGRAMCOHORT',
  CREATE_PROGRAMCOHORT: 'programCohort/CREATE_PROGRAMCOHORT',
  UPDATE_PROGRAMCOHORT: 'programCohort/UPDATE_PROGRAMCOHORT',
  DELETE_PROGRAMCOHORT: 'programCohort/DELETE_PROGRAMCOHORT',
  RESET: 'programCohort/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramCohort>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramCohortState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramCohortState = initialState, action): ProgramCohortState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOHORT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOHORT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMCOHORT):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMCOHORT):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMCOHORT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOHORT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOHORT):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMCOHORT):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMCOHORT):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMCOHORT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOHORT_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOHORT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMCOHORT):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMCOHORT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMCOHORT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-cohorts';

// Actions

export const getEntities: ICrudGetAllAction<IProgramCohort> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOHORT_LIST,
    payload: axios.get<IProgramCohort>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramCohort> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOHORT,
    payload: axios.get<IProgramCohort>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramCohort> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMCOHORT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramCohort> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMCOHORT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramCohort> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMCOHORT,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
