import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramCohort from './program-cohort';
import ProgramCohortDetail from './program-cohort-detail';
import ProgramCohortUpdate from './program-cohort-update';
import ProgramCohortDeleteDialog from './program-cohort-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramCohortUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramCohortUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramCohortDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramCohort} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramCohortDeleteDialog} />
  </>
);

export default Routes;
