import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-cohort.reducer';
import { IProgramCohort } from 'app/shared/model/program-cohort.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramCohortUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramCohortUpdateState {
  isNew: boolean;
  programId: string;
}

export class ProgramCohortUpdate extends React.Component<IProgramCohortUpdateProps, IProgramCohortUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPrograms();
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programCohortEntity } = this.props;
      const entity = {
        ...programCohortEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-cohort');
  };

  render() {
    const { programCohortEntity, programs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programCohort.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programCohort.home.createOrEditLabel">Create or edit a ProgramCohort</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programCohortEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-cohort-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-cohort-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="cohortNameLabel" for="program-cohort-cohortName">
                    <Translate contentKey="programserviceApp.programCohort.cohortName">Cohort Name</Translate>
                  </Label>
                  <AvField
                    id="program-cohort-cohortName"
                    type="text"
                    name="cohortName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 150, errorMessage: translate('entity.validation.maxlength', { max: 150 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-cohort-createdDate">
                    <Translate contentKey="programserviceApp.programCohort.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCohortEntity.createdDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-cohort-modifiedDate">
                    <Translate contentKey="programserviceApp.programCohort.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCohortEntity.modifiedDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="isDefaultLabel" check>
                    <AvInput id="program-cohort-isDefault" type="checkbox" className="form-control" name="isDefault" />
                    <Translate contentKey="programserviceApp.programCohort.isDefault">Is Default</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label for="program-cohort-program">
                    <Translate contentKey="programserviceApp.programCohort.program">Program</Translate>
                  </Label>
                  <AvInput id="program-cohort-program" type="select" className="form-control" name="programId">
                    <option value="" key="0" />
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-cohort" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programs: storeState.program.entities,
  programCohortEntity: storeState.programCohort.entity,
  loading: storeState.programCohort.loading,
  updating: storeState.programCohort.updating,
  updateSuccess: storeState.programCohort.updateSuccess
});

const mapDispatchToProps = {
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortUpdate);
