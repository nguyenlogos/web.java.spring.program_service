import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-cohort-data-input.reducer';
import { IProgramCohortDataInput } from 'app/shared/model/program-cohort-data-input.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramCohortDataInputDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramCohortDataInputDetail extends React.Component<IProgramCohortDataInputDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programCohortDataInputEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programCohortDataInput.detail.title">ProgramCohortDataInput</Translate> [
            <b>{programCohortDataInputEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="nameDataInput">
                <Translate contentKey="programserviceApp.programCohortDataInput.nameDataInput">Name Data Input</Translate>
              </span>
            </dt>
            <dd>{programCohortDataInputEntity.nameDataInput}</dd>
            <dt>
              <span id="inputType">
                <Translate contentKey="programserviceApp.programCohortDataInput.inputType">Input Type</Translate>
              </span>
            </dt>
            <dd>{programCohortDataInputEntity.inputType}</dd>
            <dt>
              <span id="createdDate">
                <Translate contentKey="programserviceApp.programCohortDataInput.createdDate">Created Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCohortDataInputEntity.createdDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedDate">
                <Translate contentKey="programserviceApp.programCohortDataInput.modifiedDate">Modified Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programCohortDataInputEntity.modifiedDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
          </dl>
          <Button tag={Link} to="/entity/program-cohort-data-input" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-cohort-data-input/${programCohortDataInputEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programCohortDataInput }: IRootState) => ({
  programCohortDataInputEntity: programCohortDataInput.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortDataInputDetail);
