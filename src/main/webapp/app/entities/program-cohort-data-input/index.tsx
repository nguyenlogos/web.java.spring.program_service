import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramCohortDataInput from './program-cohort-data-input';
import ProgramCohortDataInputDetail from './program-cohort-data-input-detail';
import ProgramCohortDataInputUpdate from './program-cohort-data-input-update';
import ProgramCohortDataInputDeleteDialog from './program-cohort-data-input-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramCohortDataInputUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramCohortDataInputUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramCohortDataInputDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramCohortDataInput} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramCohortDataInputDeleteDialog} />
  </>
);

export default Routes;
