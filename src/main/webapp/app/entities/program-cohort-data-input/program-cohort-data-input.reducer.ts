import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramCohortDataInput, defaultValue } from 'app/shared/model/program-cohort-data-input.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMCOHORTDATAINPUT_LIST: 'programCohortDataInput/FETCH_PROGRAMCOHORTDATAINPUT_LIST',
  FETCH_PROGRAMCOHORTDATAINPUT: 'programCohortDataInput/FETCH_PROGRAMCOHORTDATAINPUT',
  CREATE_PROGRAMCOHORTDATAINPUT: 'programCohortDataInput/CREATE_PROGRAMCOHORTDATAINPUT',
  UPDATE_PROGRAMCOHORTDATAINPUT: 'programCohortDataInput/UPDATE_PROGRAMCOHORTDATAINPUT',
  DELETE_PROGRAMCOHORTDATAINPUT: 'programCohortDataInput/DELETE_PROGRAMCOHORTDATAINPUT',
  RESET: 'programCohortDataInput/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramCohortDataInput>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramCohortDataInputState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramCohortDataInputState = initialState, action): ProgramCohortDataInputState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOHORTDATAINPUT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMCOHORTDATAINPUT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMCOHORTDATAINPUT):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMCOHORTDATAINPUT):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMCOHORTDATAINPUT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOHORTDATAINPUT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMCOHORTDATAINPUT):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMCOHORTDATAINPUT):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMCOHORTDATAINPUT):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMCOHORTDATAINPUT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOHORTDATAINPUT_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMCOHORTDATAINPUT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMCOHORTDATAINPUT):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMCOHORTDATAINPUT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMCOHORTDATAINPUT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-cohort-data-inputs';

// Actions

export const getEntities: ICrudGetAllAction<IProgramCohortDataInput> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOHORTDATAINPUT_LIST,
    payload: axios.get<IProgramCohortDataInput>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramCohortDataInput> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMCOHORTDATAINPUT,
    payload: axios.get<IProgramCohortDataInput>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramCohortDataInput> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMCOHORTDATAINPUT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramCohortDataInput> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMCOHORTDATAINPUT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramCohortDataInput> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMCOHORTDATAINPUT,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
