import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './program-cohort-data-input.reducer';
import { IProgramCohortDataInput } from 'app/shared/model/program-cohort-data-input.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramCohortDataInputUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramCohortDataInputUpdateState {
  isNew: boolean;
}

export class ProgramCohortDataInputUpdate extends React.Component<IProgramCohortDataInputUpdateProps, IProgramCohortDataInputUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdDate = convertDateTimeToServer(values.createdDate);
    values.modifiedDate = convertDateTimeToServer(values.modifiedDate);

    if (errors.length === 0) {
      const { programCohortDataInputEntity } = this.props;
      const entity = {
        ...programCohortDataInputEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-cohort-data-input');
  };

  render() {
    const { programCohortDataInputEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programCohortDataInput.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programCohortDataInput.home.createOrEditLabel">
                Create or edit a ProgramCohortDataInput
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programCohortDataInputEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-cohort-data-input-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-cohort-data-input-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="nameDataInputLabel" for="program-cohort-data-input-nameDataInput">
                    <Translate contentKey="programserviceApp.programCohortDataInput.nameDataInput">Name Data Input</Translate>
                  </Label>
                  <AvField
                    id="program-cohort-data-input-nameDataInput"
                    type="text"
                    name="nameDataInput"
                    validate={{
                      maxLength: { value: 150, errorMessage: translate('entity.validation.maxlength', { max: 150 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="inputTypeLabel" for="program-cohort-data-input-inputType">
                    <Translate contentKey="programserviceApp.programCohortDataInput.inputType">Input Type</Translate>
                  </Label>
                  <AvField
                    id="program-cohort-data-input-inputType"
                    type="text"
                    name="inputType"
                    validate={{
                      maxLength: { value: 100, errorMessage: translate('entity.validation.maxlength', { max: 100 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdDateLabel" for="program-cohort-data-input-createdDate">
                    <Translate contentKey="programserviceApp.programCohortDataInput.createdDate">Created Date</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-data-input-createdDate"
                    type="datetime-local"
                    className="form-control"
                    name="createdDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCohortDataInputEntity.createdDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedDateLabel" for="program-cohort-data-input-modifiedDate">
                    <Translate contentKey="programserviceApp.programCohortDataInput.modifiedDate">Modified Date</Translate>
                  </Label>
                  <AvInput
                    id="program-cohort-data-input-modifiedDate"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedDate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programCohortDataInputEntity.modifiedDate)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-cohort-data-input" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programCohortDataInputEntity: storeState.programCohortDataInput.entity,
  loading: storeState.programCohortDataInput.loading,
  updating: storeState.programCohortDataInput.updating,
  updateSuccess: storeState.programCohortDataInput.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramCohortDataInputUpdate);
