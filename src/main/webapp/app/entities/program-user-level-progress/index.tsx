import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramUserLevelProgress from './program-user-level-progress';
import ProgramUserLevelProgressDetail from './program-user-level-progress-detail';
import ProgramUserLevelProgressUpdate from './program-user-level-progress-update';
import ProgramUserLevelProgressDeleteDialog from './program-user-level-progress-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramUserLevelProgressUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramUserLevelProgressUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramUserLevelProgressDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramUserLevelProgress} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramUserLevelProgressDeleteDialog} />
  </>
);

export default Routes;
