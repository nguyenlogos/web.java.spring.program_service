import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-user-level-progress.reducer';
import { IProgramUserLevelProgress } from 'app/shared/model/program-user-level-progress.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramUserLevelProgressDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramUserLevelProgressDetail extends React.Component<IProgramUserLevelProgressDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programUserLevelProgressEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programUserLevelProgress.detail.title">ProgramUserLevelProgress</Translate> [
            <b>{programUserLevelProgressEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="userEventId">
                <Translate contentKey="programserviceApp.programUserLevelProgress.userEventId">User Event Id</Translate>
              </span>
            </dt>
            <dd>{programUserLevelProgressEntity.userEventId}</dd>
            <dt>
              <span id="programUserId">
                <Translate contentKey="programserviceApp.programUserLevelProgress.programUserId">Program User Id</Translate>
              </span>
            </dt>
            <dd>{programUserLevelProgressEntity.programUserId}</dd>
            <dt>
              <span id="createdAt">
                <Translate contentKey="programserviceApp.programUserLevelProgress.createdAt">Created At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programUserLevelProgressEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="levelUpAt">
                <Translate contentKey="programserviceApp.programUserLevelProgress.levelUpAt">Level Up At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={programUserLevelProgressEntity.levelUpAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="levelFrom">
                <Translate contentKey="programserviceApp.programUserLevelProgress.levelFrom">Level From</Translate>
              </span>
            </dt>
            <dd>{programUserLevelProgressEntity.levelFrom}</dd>
            <dt>
              <span id="levelTo">
                <Translate contentKey="programserviceApp.programUserLevelProgress.levelTo">Level To</Translate>
              </span>
            </dt>
            <dd>{programUserLevelProgressEntity.levelTo}</dd>
            <dt>
              <span id="hasReward">
                <Translate contentKey="programserviceApp.programUserLevelProgress.hasReward">Has Reward</Translate>
              </span>
            </dt>
            <dd>{programUserLevelProgressEntity.hasReward ? 'true' : 'false'}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-user-level-progress" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-user-level-progress/${programUserLevelProgressEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programUserLevelProgress }: IRootState) => ({
  programUserLevelProgressEntity: programUserLevelProgress.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserLevelProgressDetail);
