import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './program-user-level-progress.reducer';
import { IProgramUserLevelProgress } from 'app/shared/model/program-user-level-progress.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramUserLevelProgressUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramUserLevelProgressUpdateState {
  isNew: boolean;
}

export class ProgramUserLevelProgressUpdate extends React.Component<
  IProgramUserLevelProgressUpdateProps,
  IProgramUserLevelProgressUpdateState
> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (!this.state.isNew) {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.levelUpAt = convertDateTimeToServer(values.levelUpAt);

    if (errors.length === 0) {
      const { programUserLevelProgressEntity } = this.props;
      const entity = {
        ...programUserLevelProgressEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-user-level-progress');
  };

  render() {
    const { programUserLevelProgressEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programUserLevelProgress.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programUserLevelProgress.home.createOrEditLabel">
                Create or edit a ProgramUserLevelProgress
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programUserLevelProgressEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-user-level-progress-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-user-level-progress-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="userEventIdLabel" for="program-user-level-progress-userEventId">
                    <Translate contentKey="programserviceApp.programUserLevelProgress.userEventId">User Event Id</Translate>
                  </Label>
                  <AvField
                    id="program-user-level-progress-userEventId"
                    type="string"
                    className="form-control"
                    name="userEventId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="programUserIdLabel" for="program-user-level-progress-programUserId">
                    <Translate contentKey="programserviceApp.programUserLevelProgress.programUserId">Program User Id</Translate>
                  </Label>
                  <AvField
                    id="program-user-level-progress-programUserId"
                    type="string"
                    className="form-control"
                    name="programUserId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdAtLabel" for="program-user-level-progress-createdAt">
                    <Translate contentKey="programserviceApp.programUserLevelProgress.createdAt">Created At</Translate>
                  </Label>
                  <AvInput
                    id="program-user-level-progress-createdAt"
                    type="datetime-local"
                    className="form-control"
                    name="createdAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programUserLevelProgressEntity.createdAt)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="levelUpAtLabel" for="program-user-level-progress-levelUpAt">
                    <Translate contentKey="programserviceApp.programUserLevelProgress.levelUpAt">Level Up At</Translate>
                  </Label>
                  <AvInput
                    id="program-user-level-progress-levelUpAt"
                    type="datetime-local"
                    className="form-control"
                    name="levelUpAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.programUserLevelProgressEntity.levelUpAt)}
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="levelFromLabel" for="program-user-level-progress-levelFrom">
                    <Translate contentKey="programserviceApp.programUserLevelProgress.levelFrom">Level From</Translate>
                  </Label>
                  <AvField id="program-user-level-progress-levelFrom" type="string" className="form-control" name="levelFrom" />
                </AvGroup>
                <AvGroup>
                  <Label id="levelToLabel" for="program-user-level-progress-levelTo">
                    <Translate contentKey="programserviceApp.programUserLevelProgress.levelTo">Level To</Translate>
                  </Label>
                  <AvField id="program-user-level-progress-levelTo" type="string" className="form-control" name="levelTo" />
                </AvGroup>
                <AvGroup>
                  <Label id="hasRewardLabel" check>
                    <AvInput id="program-user-level-progress-hasReward" type="checkbox" className="form-control" name="hasReward" />
                    <Translate contentKey="programserviceApp.programUserLevelProgress.hasReward">Has Reward</Translate>
                  </Label>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-user-level-progress" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programUserLevelProgressEntity: storeState.programUserLevelProgress.entity,
  loading: storeState.programUserLevelProgress.loading,
  updating: storeState.programUserLevelProgress.updating,
  updateSuccess: storeState.programUserLevelProgress.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserLevelProgressUpdate);
