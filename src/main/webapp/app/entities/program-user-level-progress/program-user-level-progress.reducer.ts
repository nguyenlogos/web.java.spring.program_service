import axios from 'axios';
import {
  parseHeaderForLinks,
  loadMoreDataWhenScrolled,
  ICrudGetAction,
  ICrudGetAllAction,
  ICrudPutAction,
  ICrudDeleteAction
} from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramUserLevelProgress, defaultValue } from 'app/shared/model/program-user-level-progress.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMUSERLEVELPROGRESS_LIST: 'programUserLevelProgress/FETCH_PROGRAMUSERLEVELPROGRESS_LIST',
  FETCH_PROGRAMUSERLEVELPROGRESS: 'programUserLevelProgress/FETCH_PROGRAMUSERLEVELPROGRESS',
  CREATE_PROGRAMUSERLEVELPROGRESS: 'programUserLevelProgress/CREATE_PROGRAMUSERLEVELPROGRESS',
  UPDATE_PROGRAMUSERLEVELPROGRESS: 'programUserLevelProgress/UPDATE_PROGRAMUSERLEVELPROGRESS',
  DELETE_PROGRAMUSERLEVELPROGRESS: 'programUserLevelProgress/DELETE_PROGRAMUSERLEVELPROGRESS',
  RESET: 'programUserLevelProgress/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramUserLevelProgress>,
  entity: defaultValue,
  links: { next: 0 },
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramUserLevelProgressState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramUserLevelProgressState = initialState, action): ProgramUserLevelProgressState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMUSERLEVELPROGRESS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMUSERLEVELPROGRESS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMUSERLEVELPROGRESS):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMUSERLEVELPROGRESS):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMUSERLEVELPROGRESS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMUSERLEVELPROGRESS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMUSERLEVELPROGRESS):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMUSERLEVELPROGRESS):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMUSERLEVELPROGRESS):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMUSERLEVELPROGRESS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMUSERLEVELPROGRESS_LIST):
      const links = parseHeaderForLinks(action.payload.headers.link);
      return {
        ...state,
        links,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: loadMoreDataWhenScrolled(state.entities, action.payload.data, links)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMUSERLEVELPROGRESS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMUSERLEVELPROGRESS):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMUSERLEVELPROGRESS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMUSERLEVELPROGRESS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-user-level-progresses';

// Actions

export const getEntities: ICrudGetAllAction<IProgramUserLevelProgress> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMUSERLEVELPROGRESS_LIST,
    payload: axios.get<IProgramUserLevelProgress>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramUserLevelProgress> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMUSERLEVELPROGRESS,
    payload: axios.get<IProgramUserLevelProgress>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramUserLevelProgress> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMUSERLEVELPROGRESS,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const updateEntity: ICrudPutAction<IProgramUserLevelProgress> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMUSERLEVELPROGRESS,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramUserLevelProgress> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMUSERLEVELPROGRESS,
    payload: axios.delete(requestUrl)
  });
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
