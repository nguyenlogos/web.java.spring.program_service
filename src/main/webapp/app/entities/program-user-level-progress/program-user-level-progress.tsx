import React from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities, reset } from './program-user-level-progress.reducer';
import { IProgramUserLevelProgress } from 'app/shared/model/program-user-level-progress.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramUserLevelProgressProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramUserLevelProgressState = IPaginationBaseState;

export class ProgramUserLevelProgress extends React.Component<IProgramUserLevelProgressProps, IProgramUserLevelProgressState> {
  state: IProgramUserLevelProgressState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.reset();
  }

  componentDidUpdate() {
    if (this.props.updateSuccess) {
      this.reset();
    }
  }

  reset = () => {
    this.props.reset();
    this.setState({ activePage: 1 }, () => {
      this.getEntities();
    });
  };

  handleLoadMore = () => {
    if (window.pageYOffset > 0) {
      this.setState({ activePage: this.state.activePage + 1 }, () => this.getEntities());
    }
  };

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => {
        this.reset();
      }
    );
  };

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programUserLevelProgressList, match } = this.props;
    return (
      <div>
        <h2 id="program-user-level-progress-heading">
          <Translate contentKey="programserviceApp.programUserLevelProgress.home.title">Program User Level Progresses</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.programUserLevelProgress.home.createLabel">
              Create new Program User Level Progress
            </Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <InfiniteScroll
            pageStart={this.state.activePage}
            loadMore={this.handleLoadMore}
            hasMore={this.state.activePage - 1 < this.props.links.next}
            loader={<div className="loader">Loading ...</div>}
            threshold={0}
            initialLoad={false}
          >
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('userEventId')}>
                    <Translate contentKey="programserviceApp.programUserLevelProgress.userEventId">User Event Id</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('programUserId')}>
                    <Translate contentKey="programserviceApp.programUserLevelProgress.programUserId">Program User Id</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('createdAt')}>
                    <Translate contentKey="programserviceApp.programUserLevelProgress.createdAt">Created At</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('levelUpAt')}>
                    <Translate contentKey="programserviceApp.programUserLevelProgress.levelUpAt">Level Up At</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('levelFrom')}>
                    <Translate contentKey="programserviceApp.programUserLevelProgress.levelFrom">Level From</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('levelTo')}>
                    <Translate contentKey="programserviceApp.programUserLevelProgress.levelTo">Level To</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('hasReward')}>
                    <Translate contentKey="programserviceApp.programUserLevelProgress.hasReward">Has Reward</Translate>{' '}
                    <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {programUserLevelProgressList.map((programUserLevelProgress, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${programUserLevelProgress.id}`} color="link" size="sm">
                        {programUserLevelProgress.id}
                      </Button>
                    </td>
                    <td>{programUserLevelProgress.userEventId}</td>
                    <td>{programUserLevelProgress.programUserId}</td>
                    <td>
                      <TextFormat type="date" value={programUserLevelProgress.createdAt} format={APP_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={programUserLevelProgress.levelUpAt} format={APP_DATE_FORMAT} />
                    </td>
                    <td>{programUserLevelProgress.levelFrom}</td>
                    <td>{programUserLevelProgress.levelTo}</td>
                    <td>{programUserLevelProgress.hasReward ? 'true' : 'false'}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${programUserLevelProgress.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.view">View</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${programUserLevelProgress.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.edit">Edit</Translate>
                          </span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${programUserLevelProgress.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" />{' '}
                          <span className="d-none d-md-inline">
                            <Translate contentKey="entity.action.delete">Delete</Translate>
                          </span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ programUserLevelProgress }: IRootState) => ({
  programUserLevelProgressList: programUserLevelProgress.entities,
  totalItems: programUserLevelProgress.totalItems,
  links: programUserLevelProgress.links,
  entity: programUserLevelProgress.entity,
  updateSuccess: programUserLevelProgress.updateSuccess
});

const mapDispatchToProps = {
  getEntities,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserLevelProgress);
