import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './client-sub-domain.reducer';
import { IClientSubDomain } from 'app/shared/model/client-sub-domain.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IClientSubDomainUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IClientSubDomainUpdateState {
  isNew: boolean;
}

export class ClientSubDomainUpdate extends React.Component<IClientSubDomainUpdateProps, IClientSubDomainUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.createdAt = convertDateTimeToServer(values.createdAt);
    values.modifiedAt = convertDateTimeToServer(values.modifiedAt);

    if (errors.length === 0) {
      const { clientSubDomainEntity } = this.props;
      const entity = {
        ...clientSubDomainEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/client-sub-domain');
  };

  render() {
    const { clientSubDomainEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.clientSubDomain.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.clientSubDomain.home.createOrEditLabel">Create or edit a ClientSubDomain</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : clientSubDomainEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="client-sub-domain-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="client-sub-domain-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="clientIdLabel" for="client-sub-domain-clientId">
                    <Translate contentKey="programserviceApp.clientSubDomain.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="client-sub-domain-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subDomainUrlLabel" for="client-sub-domain-subDomainUrl">
                    <Translate contentKey="programserviceApp.clientSubDomain.subDomainUrl">Sub Domain Url</Translate>
                  </Label>
                  <AvField
                    id="client-sub-domain-subDomainUrl"
                    type="text"
                    name="subDomainUrl"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="createdAtLabel" for="client-sub-domain-createdAt">
                    <Translate contentKey="programserviceApp.clientSubDomain.createdAt">Created At</Translate>
                  </Label>
                  <AvInput
                    id="client-sub-domain-createdAt"
                    type="datetime-local"
                    className="form-control"
                    name="createdAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.clientSubDomainEntity.createdAt)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="modifiedAtLabel" for="client-sub-domain-modifiedAt">
                    <Translate contentKey="programserviceApp.clientSubDomain.modifiedAt">Modified At</Translate>
                  </Label>
                  <AvInput
                    id="client-sub-domain-modifiedAt"
                    type="datetime-local"
                    className="form-control"
                    name="modifiedAt"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.clientSubDomainEntity.modifiedAt)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="programIdLabel" for="client-sub-domain-programId">
                    <Translate contentKey="programserviceApp.clientSubDomain.programId">Program Id</Translate>
                  </Label>
                  <AvField id="client-sub-domain-programId" type="string" className="form-control" name="programId" />
                </AvGroup>
                <AvGroup>
                  <Label id="programNameLabel" for="client-sub-domain-programName">
                    <Translate contentKey="programserviceApp.clientSubDomain.programName">Program Name</Translate>
                  </Label>
                  <AvField id="client-sub-domain-programName" type="text" name="programName" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/client-sub-domain" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  clientSubDomainEntity: storeState.clientSubDomain.entity,
  loading: storeState.clientSubDomain.loading,
  updating: storeState.clientSubDomain.updating,
  updateSuccess: storeState.clientSubDomain.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientSubDomainUpdate);
