import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './client-sub-domain.reducer';
import { IClientSubDomain } from 'app/shared/model/client-sub-domain.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IClientSubDomainDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ClientSubDomainDetail extends React.Component<IClientSubDomainDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { clientSubDomainEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.clientSubDomain.detail.title">ClientSubDomain</Translate> [
            <b>{clientSubDomainEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.clientSubDomain.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{clientSubDomainEntity.clientId}</dd>
            <dt>
              <span id="subDomainUrl">
                <Translate contentKey="programserviceApp.clientSubDomain.subDomainUrl">Sub Domain Url</Translate>
              </span>
            </dt>
            <dd>{clientSubDomainEntity.subDomainUrl}</dd>
            <dt>
              <span id="createdAt">
                <Translate contentKey="programserviceApp.clientSubDomain.createdAt">Created At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={clientSubDomainEntity.createdAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="modifiedAt">
                <Translate contentKey="programserviceApp.clientSubDomain.modifiedAt">Modified At</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={clientSubDomainEntity.modifiedAt} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.clientSubDomain.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{clientSubDomainEntity.programId}</dd>
            <dt>
              <span id="programName">
                <Translate contentKey="programserviceApp.clientSubDomain.programName">Program Name</Translate>
              </span>
            </dt>
            <dd>{clientSubDomainEntity.programName}</dd>
          </dl>
          <Button tag={Link} to="/entity/client-sub-domain" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/client-sub-domain/${clientSubDomainEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ clientSubDomain }: IRootState) => ({
  clientSubDomainEntity: clientSubDomain.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ClientSubDomainDetail);
