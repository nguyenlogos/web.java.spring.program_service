import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IClientSubDomain, defaultValue } from 'app/shared/model/client-sub-domain.model';

export const ACTION_TYPES = {
  FETCH_CLIENTSUBDOMAIN_LIST: 'clientSubDomain/FETCH_CLIENTSUBDOMAIN_LIST',
  FETCH_CLIENTSUBDOMAIN: 'clientSubDomain/FETCH_CLIENTSUBDOMAIN',
  CREATE_CLIENTSUBDOMAIN: 'clientSubDomain/CREATE_CLIENTSUBDOMAIN',
  UPDATE_CLIENTSUBDOMAIN: 'clientSubDomain/UPDATE_CLIENTSUBDOMAIN',
  DELETE_CLIENTSUBDOMAIN: 'clientSubDomain/DELETE_CLIENTSUBDOMAIN',
  RESET: 'clientSubDomain/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IClientSubDomain>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ClientSubDomainState = Readonly<typeof initialState>;

// Reducer

export default (state: ClientSubDomainState = initialState, action): ClientSubDomainState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_CLIENTSUBDOMAIN_LIST):
    case REQUEST(ACTION_TYPES.FETCH_CLIENTSUBDOMAIN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_CLIENTSUBDOMAIN):
    case REQUEST(ACTION_TYPES.UPDATE_CLIENTSUBDOMAIN):
    case REQUEST(ACTION_TYPES.DELETE_CLIENTSUBDOMAIN):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_CLIENTSUBDOMAIN_LIST):
    case FAILURE(ACTION_TYPES.FETCH_CLIENTSUBDOMAIN):
    case FAILURE(ACTION_TYPES.CREATE_CLIENTSUBDOMAIN):
    case FAILURE(ACTION_TYPES.UPDATE_CLIENTSUBDOMAIN):
    case FAILURE(ACTION_TYPES.DELETE_CLIENTSUBDOMAIN):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLIENTSUBDOMAIN_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_CLIENTSUBDOMAIN):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_CLIENTSUBDOMAIN):
    case SUCCESS(ACTION_TYPES.UPDATE_CLIENTSUBDOMAIN):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_CLIENTSUBDOMAIN):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/client-sub-domains';

// Actions

export const getEntities: ICrudGetAllAction<IClientSubDomain> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_CLIENTSUBDOMAIN_LIST,
    payload: axios.get<IClientSubDomain>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IClientSubDomain> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_CLIENTSUBDOMAIN,
    payload: axios.get<IClientSubDomain>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IClientSubDomain> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_CLIENTSUBDOMAIN,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IClientSubDomain> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_CLIENTSUBDOMAIN,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IClientSubDomain> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_CLIENTSUBDOMAIN,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
