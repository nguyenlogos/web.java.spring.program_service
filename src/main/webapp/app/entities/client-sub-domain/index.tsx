import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ClientSubDomain from './client-sub-domain';
import ClientSubDomainDetail from './client-sub-domain-detail';
import ClientSubDomainUpdate from './client-sub-domain-update';
import ClientSubDomainDeleteDialog from './client-sub-domain-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ClientSubDomainUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ClientSubDomainUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ClientSubDomainDetail} />
      <ErrorBoundaryRoute path={match.url} component={ClientSubDomain} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ClientSubDomainDeleteDialog} />
  </>
);

export default Routes;
