import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramHealthyRange from './program-healthy-range';
import ProgramHealthyRangeDetail from './program-healthy-range-detail';
import ProgramHealthyRangeUpdate from './program-healthy-range-update';
import ProgramHealthyRangeDeleteDialog from './program-healthy-range-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramHealthyRangeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramHealthyRangeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramHealthyRangeDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramHealthyRange} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramHealthyRangeDeleteDialog} />
  </>
);

export default Routes;
