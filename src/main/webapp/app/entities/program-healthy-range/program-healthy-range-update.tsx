import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProgram } from 'app/shared/model/program.model';
import { getEntities as getPrograms } from 'app/entities/program/program.reducer';
import { getEntity, updateEntity, createEntity, reset } from './program-healthy-range.reducer';
import { IProgramHealthyRange } from 'app/shared/model/program-healthy-range.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramHealthyRangeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramHealthyRangeUpdateState {
  isNew: boolean;
  programId: string;
}

export class ProgramHealthyRangeUpdate extends React.Component<IProgramHealthyRangeUpdateProps, IProgramHealthyRangeUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      programId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getPrograms();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programHealthyRangeEntity } = this.props;
      const entity = {
        ...programHealthyRangeEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-healthy-range');
  };

  render() {
    const { programHealthyRangeEntity, programs, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programHealthyRange.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programHealthyRange.home.createOrEditLabel">
                Create or edit a ProgramHealthyRange
              </Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programHealthyRangeEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-healthy-range-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-healthy-range-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="maleMinLabel" for="program-healthy-range-maleMin">
                    <Translate contentKey="programserviceApp.programHealthyRange.maleMin">Male Min</Translate>
                  </Label>
                  <AvField
                    id="program-healthy-range-maleMin"
                    type="string"
                    className="form-control"
                    name="maleMin"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="maleMaxLabel" for="program-healthy-range-maleMax">
                    <Translate contentKey="programserviceApp.programHealthyRange.maleMax">Male Max</Translate>
                  </Label>
                  <AvField
                    id="program-healthy-range-maleMax"
                    type="string"
                    className="form-control"
                    name="maleMax"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="femaleMinLabel" for="program-healthy-range-femaleMin">
                    <Translate contentKey="programserviceApp.programHealthyRange.femaleMin">Female Min</Translate>
                  </Label>
                  <AvField
                    id="program-healthy-range-femaleMin"
                    type="string"
                    className="form-control"
                    name="femaleMin"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="femaleMaxLabel" for="program-healthy-range-femaleMax">
                    <Translate contentKey="programserviceApp.programHealthyRange.femaleMax">Female Max</Translate>
                  </Label>
                  <AvField
                    id="program-healthy-range-femaleMax"
                    type="string"
                    className="form-control"
                    name="femaleMax"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="unidentifiedMinLabel" for="program-healthy-range-unidentifiedMin">
                    <Translate contentKey="programserviceApp.programHealthyRange.unidentifiedMin">Unidentified Min</Translate>
                  </Label>
                  <AvField
                    id="program-healthy-range-unidentifiedMin"
                    type="string"
                    className="form-control"
                    name="unidentifiedMin"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="unidentifiedMaxLabel" for="program-healthy-range-unidentifiedMax">
                    <Translate contentKey="programserviceApp.programHealthyRange.unidentifiedMax">Unidentified Max</Translate>
                  </Label>
                  <AvField
                    id="program-healthy-range-unidentifiedMax"
                    type="string"
                    className="form-control"
                    name="unidentifiedMax"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="isApplyPointLabel" check>
                    <AvInput id="program-healthy-range-isApplyPoint" type="checkbox" className="form-control" name="isApplyPoint" />
                    <Translate contentKey="programserviceApp.programHealthyRange.isApplyPoint">Is Apply Point</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="biometricCodeLabel" for="program-healthy-range-biometricCode">
                    <Translate contentKey="programserviceApp.programHealthyRange.biometricCode">Biometric Code</Translate>
                  </Label>
                  <AvField
                    id="program-healthy-range-biometricCode"
                    type="text"
                    name="biometricCode"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="subCategoryCodeLabel" for="program-healthy-range-subCategoryCode">
                    <Translate contentKey="programserviceApp.programHealthyRange.subCategoryCode">Sub Category Code</Translate>
                  </Label>
                  <AvField id="program-healthy-range-subCategoryCode" type="text" name="subCategoryCode" />
                </AvGroup>
                <AvGroup>
                  <Label id="subCategoryIdLabel" for="program-healthy-range-subCategoryId">
                    <Translate contentKey="programserviceApp.programHealthyRange.subCategoryId">Sub Category Id</Translate>
                  </Label>
                  <AvField
                    id="program-healthy-range-subCategoryId"
                    type="string"
                    className="form-control"
                    name="subCategoryId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="program-healthy-range-program">
                    <Translate contentKey="programserviceApp.programHealthyRange.program">Program</Translate>
                  </Label>
                  <AvInput id="program-healthy-range-program" type="select" className="form-control" name="programId">
                    <option value="" key="0" />
                    {programs
                      ? programs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-healthy-range" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programs: storeState.program.entities,
  programHealthyRangeEntity: storeState.programHealthyRange.entity,
  loading: storeState.programHealthyRange.loading,
  updating: storeState.programHealthyRange.updating,
  updateSuccess: storeState.programHealthyRange.updateSuccess
});

const mapDispatchToProps = {
  getPrograms,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramHealthyRangeUpdate);
