import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, getSortState, IPaginationBaseState, getPaginationItemsNumber, JhiPagination } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './program-healthy-range.reducer';
import { IProgramHealthyRange } from 'app/shared/model/program-healthy-range.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface IProgramHealthyRangeProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type IProgramHealthyRangeState = IPaginationBaseState;

export class ProgramHealthyRange extends React.Component<IProgramHealthyRangeProps, IProgramHealthyRangeState> {
  state: IProgramHealthyRangeState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { programHealthyRangeList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="program-healthy-range-heading">
          <Translate contentKey="programserviceApp.programHealthyRange.home.title">Program Healthy Ranges</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="programserviceApp.programHealthyRange.home.createLabel">Create new Program Healthy Range</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={this.sort('id')}>
                  <Translate contentKey="global.field.id">ID</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('maleMin')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.maleMin">Male Min</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('maleMax')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.maleMax">Male Max</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('femaleMin')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.femaleMin">Female Min</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('femaleMax')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.femaleMax">Female Max</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('unidentifiedMin')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.unidentifiedMin">Unidentified Min</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('unidentifiedMax')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.unidentifiedMax">Unidentified Max</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('isApplyPoint')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.isApplyPoint">Is Apply Point</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('biometricCode')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.biometricCode">Biometric Code</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('subCategoryCode')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.subCategoryCode">Sub Category Code</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th className="hand" onClick={this.sort('subCategoryId')}>
                  <Translate contentKey="programserviceApp.programHealthyRange.subCategoryId">Sub Category Id</Translate>{' '}
                  <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="programserviceApp.programHealthyRange.program">Program</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {programHealthyRangeList.map((programHealthyRange, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${programHealthyRange.id}`} color="link" size="sm">
                      {programHealthyRange.id}
                    </Button>
                  </td>
                  <td>{programHealthyRange.maleMin}</td>
                  <td>{programHealthyRange.maleMax}</td>
                  <td>{programHealthyRange.femaleMin}</td>
                  <td>{programHealthyRange.femaleMax}</td>
                  <td>{programHealthyRange.unidentifiedMin}</td>
                  <td>{programHealthyRange.unidentifiedMax}</td>
                  <td>{programHealthyRange.isApplyPoint ? 'true' : 'false'}</td>
                  <td>{programHealthyRange.biometricCode}</td>
                  <td>{programHealthyRange.subCategoryCode}</td>
                  <td>{programHealthyRange.subCategoryId}</td>
                  <td>
                    {programHealthyRange.programId ? (
                      <Link to={`program/${programHealthyRange.programId}`}>{programHealthyRange.programId}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${programHealthyRange.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programHealthyRange.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${programHealthyRange.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Row className="justify-content-center">
          <JhiPagination
            items={getPaginationItemsNumber(totalItems, this.state.itemsPerPage)}
            activePage={this.state.activePage}
            onSelect={this.handlePagination}
            maxButtons={5}
          />
        </Row>
      </div>
    );
  }
}

const mapStateToProps = ({ programHealthyRange }: IRootState) => ({
  programHealthyRangeList: programHealthyRange.entities,
  totalItems: programHealthyRange.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramHealthyRange);
