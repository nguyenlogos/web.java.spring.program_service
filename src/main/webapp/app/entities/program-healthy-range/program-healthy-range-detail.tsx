import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-healthy-range.reducer';
import { IProgramHealthyRange } from 'app/shared/model/program-healthy-range.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramHealthyRangeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramHealthyRangeDetail extends React.Component<IProgramHealthyRangeDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programHealthyRangeEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programHealthyRange.detail.title">ProgramHealthyRange</Translate> [
            <b>{programHealthyRangeEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="maleMin">
                <Translate contentKey="programserviceApp.programHealthyRange.maleMin">Male Min</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.maleMin}</dd>
            <dt>
              <span id="maleMax">
                <Translate contentKey="programserviceApp.programHealthyRange.maleMax">Male Max</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.maleMax}</dd>
            <dt>
              <span id="femaleMin">
                <Translate contentKey="programserviceApp.programHealthyRange.femaleMin">Female Min</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.femaleMin}</dd>
            <dt>
              <span id="femaleMax">
                <Translate contentKey="programserviceApp.programHealthyRange.femaleMax">Female Max</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.femaleMax}</dd>
            <dt>
              <span id="unidentifiedMin">
                <Translate contentKey="programserviceApp.programHealthyRange.unidentifiedMin">Unidentified Min</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.unidentifiedMin}</dd>
            <dt>
              <span id="unidentifiedMax">
                <Translate contentKey="programserviceApp.programHealthyRange.unidentifiedMax">Unidentified Max</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.unidentifiedMax}</dd>
            <dt>
              <span id="isApplyPoint">
                <Translate contentKey="programserviceApp.programHealthyRange.isApplyPoint">Is Apply Point</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.isApplyPoint ? 'true' : 'false'}</dd>
            <dt>
              <span id="biometricCode">
                <Translate contentKey="programserviceApp.programHealthyRange.biometricCode">Biometric Code</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.biometricCode}</dd>
            <dt>
              <span id="subCategoryCode">
                <Translate contentKey="programserviceApp.programHealthyRange.subCategoryCode">Sub Category Code</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.subCategoryCode}</dd>
            <dt>
              <span id="subCategoryId">
                <Translate contentKey="programserviceApp.programHealthyRange.subCategoryId">Sub Category Id</Translate>
              </span>
            </dt>
            <dd>{programHealthyRangeEntity.subCategoryId}</dd>
            <dt>
              <Translate contentKey="programserviceApp.programHealthyRange.program">Program</Translate>
            </dt>
            <dd>{programHealthyRangeEntity.programId ? programHealthyRangeEntity.programId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-healthy-range" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-healthy-range/${programHealthyRangeEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programHealthyRange }: IRootState) => ({
  programHealthyRangeEntity: programHealthyRange.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramHealthyRangeDetail);
