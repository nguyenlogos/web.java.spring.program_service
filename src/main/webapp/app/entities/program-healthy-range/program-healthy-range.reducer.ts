import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramHealthyRange, defaultValue } from 'app/shared/model/program-healthy-range.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMHEALTHYRANGE_LIST: 'programHealthyRange/FETCH_PROGRAMHEALTHYRANGE_LIST',
  FETCH_PROGRAMHEALTHYRANGE: 'programHealthyRange/FETCH_PROGRAMHEALTHYRANGE',
  CREATE_PROGRAMHEALTHYRANGE: 'programHealthyRange/CREATE_PROGRAMHEALTHYRANGE',
  UPDATE_PROGRAMHEALTHYRANGE: 'programHealthyRange/UPDATE_PROGRAMHEALTHYRANGE',
  DELETE_PROGRAMHEALTHYRANGE: 'programHealthyRange/DELETE_PROGRAMHEALTHYRANGE',
  RESET: 'programHealthyRange/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramHealthyRange>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramHealthyRangeState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramHealthyRangeState = initialState, action): ProgramHealthyRangeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMHEALTHYRANGE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMHEALTHYRANGE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMHEALTHYRANGE):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMHEALTHYRANGE):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMHEALTHYRANGE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMHEALTHYRANGE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMHEALTHYRANGE):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMHEALTHYRANGE):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMHEALTHYRANGE):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMHEALTHYRANGE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMHEALTHYRANGE_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMHEALTHYRANGE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMHEALTHYRANGE):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMHEALTHYRANGE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMHEALTHYRANGE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-healthy-ranges';

// Actions

export const getEntities: ICrudGetAllAction<IProgramHealthyRange> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMHEALTHYRANGE_LIST,
    payload: axios.get<IProgramHealthyRange>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramHealthyRange> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMHEALTHYRANGE,
    payload: axios.get<IProgramHealthyRange>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramHealthyRange> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMHEALTHYRANGE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramHealthyRange> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMHEALTHYRANGE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramHealthyRange> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMHEALTHYRANGE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
