import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './program-user.reducer';
import { IProgramUser } from 'app/shared/model/program-user.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProgramUserDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ProgramUserDetail extends React.Component<IProgramUserDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { programUserEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="programserviceApp.programUser.detail.title">ProgramUser</Translate> [<b>{programUserEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="participantId">
                <Translate contentKey="programserviceApp.programUser.participantId">Participant Id</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.participantId}</dd>
            <dt>
              <span id="clientId">
                <Translate contentKey="programserviceApp.programUser.clientId">Client Id</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.clientId}</dd>
            <dt>
              <span id="totalUserPoint">
                <Translate contentKey="programserviceApp.programUser.totalUserPoint">Total User Point</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.totalUserPoint}</dd>
            <dt>
              <span id="programId">
                <Translate contentKey="programserviceApp.programUser.programId">Program Id</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.programId}</dd>
            <dt>
              <span id="currentLevel">
                <Translate contentKey="programserviceApp.programUser.currentLevel">Current Level</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.currentLevel}</dd>
            <dt>
              <span id="email">
                <Translate contentKey="programserviceApp.programUser.email">Email</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.email}</dd>
            <dt>
              <span id="firstName">
                <Translate contentKey="programserviceApp.programUser.firstName">First Name</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.firstName}</dd>
            <dt>
              <span id="lastName">
                <Translate contentKey="programserviceApp.programUser.lastName">Last Name</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.lastName}</dd>
            <dt>
              <span id="phone">
                <Translate contentKey="programserviceApp.programUser.phone">Phone</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.phone}</dd>
            <dt>
              <span id="clientName">
                <Translate contentKey="programserviceApp.programUser.clientName">Client Name</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.clientName}</dd>
            <dt>
              <span id="subgroupId">
                <Translate contentKey="programserviceApp.programUser.subgroupId">Subgroup Id</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.subgroupId}</dd>
            <dt>
              <span id="subgroupName">
                <Translate contentKey="programserviceApp.programUser.subgroupName">Subgroup Name</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.subgroupName}</dd>
            <dt>
              <span id="isTobaccoUser">
                <Translate contentKey="programserviceApp.programUser.isTobaccoUser">Is Tobacco User</Translate>
              </span>
            </dt>
            <dd>{programUserEntity.isTobaccoUser ? 'true' : 'false'}</dd>
          </dl>
          <Button tag={Link} to="/entity/program-user" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/program-user/${programUserEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ programUser }: IRootState) => ({
  programUserEntity: programUser.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserDetail);
