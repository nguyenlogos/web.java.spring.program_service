import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './program-user.reducer';
import { IProgramUser } from 'app/shared/model/program-user.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IProgramUserUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IProgramUserUpdateState {
  isNew: boolean;
}

export class ProgramUserUpdate extends React.Component<IProgramUserUpdateProps, IProgramUserUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { programUserEntity } = this.props;
      const entity = {
        ...programUserEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/program-user');
  };

  render() {
    const { programUserEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="programserviceApp.programUser.home.createOrEditLabel">
              <Translate contentKey="programserviceApp.programUser.home.createOrEditLabel">Create or edit a ProgramUser</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : programUserEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="program-user-id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="program-user-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="participantIdLabel" for="program-user-participantId">
                    <Translate contentKey="programserviceApp.programUser.participantId">Participant Id</Translate>
                  </Label>
                  <AvField
                    id="program-user-participantId"
                    type="text"
                    name="participantId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="clientIdLabel" for="program-user-clientId">
                    <Translate contentKey="programserviceApp.programUser.clientId">Client Id</Translate>
                  </Label>
                  <AvField
                    id="program-user-clientId"
                    type="text"
                    name="clientId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="totalUserPointLabel" for="program-user-totalUserPoint">
                    <Translate contentKey="programserviceApp.programUser.totalUserPoint">Total User Point</Translate>
                  </Label>
                  <AvField id="program-user-totalUserPoint" type="text" name="totalUserPoint" />
                </AvGroup>
                <AvGroup>
                  <Label id="programIdLabel" for="program-user-programId">
                    <Translate contentKey="programserviceApp.programUser.programId">Program Id</Translate>
                  </Label>
                  <AvField
                    id="program-user-programId"
                    type="string"
                    className="form-control"
                    name="programId"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      number: { value: true, errorMessage: translate('entity.validation.number') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="currentLevelLabel" for="program-user-currentLevel">
                    <Translate contentKey="programserviceApp.programUser.currentLevel">Current Level</Translate>
                  </Label>
                  <AvField id="program-user-currentLevel" type="string" className="form-control" name="currentLevel" />
                </AvGroup>
                <AvGroup>
                  <Label id="emailLabel" for="program-user-email">
                    <Translate contentKey="programserviceApp.programUser.email">Email</Translate>
                  </Label>
                  <AvField id="program-user-email" type="text" name="email" />
                </AvGroup>
                <AvGroup>
                  <Label id="firstNameLabel" for="program-user-firstName">
                    <Translate contentKey="programserviceApp.programUser.firstName">First Name</Translate>
                  </Label>
                  <AvField id="program-user-firstName" type="text" name="firstName" />
                </AvGroup>
                <AvGroup>
                  <Label id="lastNameLabel" for="program-user-lastName">
                    <Translate contentKey="programserviceApp.programUser.lastName">Last Name</Translate>
                  </Label>
                  <AvField id="program-user-lastName" type="text" name="lastName" />
                </AvGroup>
                <AvGroup>
                  <Label id="phoneLabel" for="program-user-phone">
                    <Translate contentKey="programserviceApp.programUser.phone">Phone</Translate>
                  </Label>
                  <AvField id="program-user-phone" type="text" name="phone" />
                </AvGroup>
                <AvGroup>
                  <Label id="clientNameLabel" for="program-user-clientName">
                    <Translate contentKey="programserviceApp.programUser.clientName">Client Name</Translate>
                  </Label>
                  <AvField id="program-user-clientName" type="text" name="clientName" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupIdLabel" for="program-user-subgroupId">
                    <Translate contentKey="programserviceApp.programUser.subgroupId">Subgroup Id</Translate>
                  </Label>
                  <AvField id="program-user-subgroupId" type="text" name="subgroupId" />
                </AvGroup>
                <AvGroup>
                  <Label id="subgroupNameLabel" for="program-user-subgroupName">
                    <Translate contentKey="programserviceApp.programUser.subgroupName">Subgroup Name</Translate>
                  </Label>
                  <AvField id="program-user-subgroupName" type="text" name="subgroupName" />
                </AvGroup>
                <AvGroup>
                  <Label id="isTobaccoUserLabel" check>
                    <AvInput id="program-user-isTobaccoUser" type="checkbox" className="form-control" name="isTobaccoUser" />
                    <Translate contentKey="programserviceApp.programUser.isTobaccoUser">Is Tobacco User</Translate>
                  </Label>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/program-user" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  programUserEntity: storeState.programUser.entity,
  loading: storeState.programUser.loading,
  updating: storeState.programUser.updating,
  updateSuccess: storeState.programUser.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProgramUserUpdate);
