import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ProgramUser from './program-user';
import ProgramUserDetail from './program-user-detail';
import ProgramUserUpdate from './program-user-update';
import ProgramUserDeleteDialog from './program-user-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ProgramUserUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ProgramUserUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ProgramUserDetail} />
      <ErrorBoundaryRoute path={match.url} component={ProgramUser} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ProgramUserDeleteDialog} />
  </>
);

export default Routes;
