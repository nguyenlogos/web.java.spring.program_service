import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProgramUser, defaultValue } from 'app/shared/model/program-user.model';

export const ACTION_TYPES = {
  FETCH_PROGRAMUSER_LIST: 'programUser/FETCH_PROGRAMUSER_LIST',
  FETCH_PROGRAMUSER: 'programUser/FETCH_PROGRAMUSER',
  CREATE_PROGRAMUSER: 'programUser/CREATE_PROGRAMUSER',
  UPDATE_PROGRAMUSER: 'programUser/UPDATE_PROGRAMUSER',
  DELETE_PROGRAMUSER: 'programUser/DELETE_PROGRAMUSER',
  RESET: 'programUser/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProgramUser>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ProgramUserState = Readonly<typeof initialState>;

// Reducer

export default (state: ProgramUserState = initialState, action): ProgramUserState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMUSER_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PROGRAMUSER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PROGRAMUSER):
    case REQUEST(ACTION_TYPES.UPDATE_PROGRAMUSER):
    case REQUEST(ACTION_TYPES.DELETE_PROGRAMUSER):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMUSER_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PROGRAMUSER):
    case FAILURE(ACTION_TYPES.CREATE_PROGRAMUSER):
    case FAILURE(ACTION_TYPES.UPDATE_PROGRAMUSER):
    case FAILURE(ACTION_TYPES.DELETE_PROGRAMUSER):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMUSER_LIST):
      return {
        ...state,
        loading: false,
        totalItems: action.payload.headers['x-total-count'],
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PROGRAMUSER):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PROGRAMUSER):
    case SUCCESS(ACTION_TYPES.UPDATE_PROGRAMUSER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PROGRAMUSER):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/program-users';

// Actions

export const getEntities: ICrudGetAllAction<IProgramUser> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMUSER_LIST,
    payload: axios.get<IProgramUser>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IProgramUser> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PROGRAMUSER,
    payload: axios.get<IProgramUser>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IProgramUser> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PROGRAMUSER,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProgramUser> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PROGRAMUSER,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProgramUser> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PROGRAMUSER,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
