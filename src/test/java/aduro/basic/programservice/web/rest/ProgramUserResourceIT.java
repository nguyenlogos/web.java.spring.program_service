package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.repository.ProgramUserRepository;
import aduro.basic.programservice.service.ProgramUserService;
import aduro.basic.programservice.service.dto.ProgramUserDTO;
import aduro.basic.programservice.service.mapper.ProgramUserMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramUserCriteria;
import aduro.basic.programservice.service.ProgramUserQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramUserResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramUserResourceIT {

    private static final String DEFAULT_PARTICIPANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TOTAL_USER_POINT = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_USER_POINT = new BigDecimal(2);

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    private static final Integer DEFAULT_CURRENT_LEVEL = 1;
    private static final Integer UPDATED_CURRENT_LEVEL = 2;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_TOBACCO_USER = false;
    private static final Boolean UPDATED_IS_TOBACCO_USER = true;

    @Autowired
    private ProgramUserRepository programUserRepository;

    @Autowired
    private ProgramUserMapper programUserMapper;

    @Autowired
    private ProgramUserService programUserService;

    @Autowired
    private ProgramUserQueryService programUserQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramUserMockMvc;

    private ProgramUser programUser;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramUserResource programUserResource = new ProgramUserResource(programUserService, programUserQueryService);
        this.restProgramUserMockMvc = MockMvcBuilders.standaloneSetup(programUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUser createEntity(EntityManager em) {
        ProgramUser programUser = new ProgramUser()
            .participantId(DEFAULT_PARTICIPANT_ID)
            .clientId(DEFAULT_CLIENT_ID)
            .totalUserPoint(DEFAULT_TOTAL_USER_POINT)
            .programId(DEFAULT_PROGRAM_ID)
            .currentLevel(DEFAULT_CURRENT_LEVEL)
            .email(DEFAULT_EMAIL)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .phone(DEFAULT_PHONE)
            .clientName(DEFAULT_CLIENT_NAME)
            .subgroupId(DEFAULT_SUBGROUP_ID)
            .subgroupName(DEFAULT_SUBGROUP_NAME)
            .isTobaccoUser(DEFAULT_IS_TOBACCO_USER);
        return programUser;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUser createUpdatedEntity(EntityManager em) {
        ProgramUser programUser = new ProgramUser()
            .participantId(UPDATED_PARTICIPANT_ID)
            .clientId(UPDATED_CLIENT_ID)
            .totalUserPoint(UPDATED_TOTAL_USER_POINT)
            .programId(UPDATED_PROGRAM_ID)
            .currentLevel(UPDATED_CURRENT_LEVEL)
            .email(UPDATED_EMAIL)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .phone(UPDATED_PHONE)
            .clientName(UPDATED_CLIENT_NAME)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME)
            .isTobaccoUser(UPDATED_IS_TOBACCO_USER);
        return programUser;
    }

    @BeforeEach
    public void initTest() {
        programUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramUser() throws Exception {
        int databaseSizeBeforeCreate = programUserRepository.findAll().size();

        // Create the ProgramUser
        ProgramUserDTO programUserDTO = programUserMapper.toDto(programUser);
        restProgramUserMockMvc.perform(post("/api/program-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramUser in the database
        List<ProgramUser> programUserList = programUserRepository.findAll();
        assertThat(programUserList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramUser testProgramUser = programUserList.get(programUserList.size() - 1);
        assertThat(testProgramUser.getParticipantId()).isEqualTo(DEFAULT_PARTICIPANT_ID);
        assertThat(testProgramUser.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testProgramUser.getTotalUserPoint()).isEqualTo(DEFAULT_TOTAL_USER_POINT);
        assertThat(testProgramUser.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
        assertThat(testProgramUser.getCurrentLevel()).isEqualTo(DEFAULT_CURRENT_LEVEL);
        assertThat(testProgramUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testProgramUser.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testProgramUser.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testProgramUser.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testProgramUser.getClientName()).isEqualTo(DEFAULT_CLIENT_NAME);
        assertThat(testProgramUser.getSubgroupId()).isEqualTo(DEFAULT_SUBGROUP_ID);
        assertThat(testProgramUser.getSubgroupName()).isEqualTo(DEFAULT_SUBGROUP_NAME);
        assertThat(testProgramUser.isIsTobaccoUser()).isEqualTo(DEFAULT_IS_TOBACCO_USER);
    }

    @Test
    @Transactional
    public void createProgramUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programUserRepository.findAll().size();

        // Create the ProgramUser with an existing ID
        programUser.setId(1L);
        ProgramUserDTO programUserDTO = programUserMapper.toDto(programUser);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramUserMockMvc.perform(post("/api/program-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUser in the database
        List<ProgramUser> programUserList = programUserRepository.findAll();
        assertThat(programUserList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkParticipantIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserRepository.findAll().size();
        // set the field null
        programUser.setParticipantId(null);

        // Create the ProgramUser, which fails.
        ProgramUserDTO programUserDTO = programUserMapper.toDto(programUser);

        restProgramUserMockMvc.perform(post("/api/program-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUser> programUserList = programUserRepository.findAll();
        assertThat(programUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserRepository.findAll().size();
        // set the field null
        programUser.setClientId(null);

        // Create the ProgramUser, which fails.
        ProgramUserDTO programUserDTO = programUserMapper.toDto(programUser);

        restProgramUserMockMvc.perform(post("/api/program-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUser> programUserList = programUserRepository.findAll();
        assertThat(programUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserRepository.findAll().size();
        // set the field null
        programUser.setProgramId(null);

        // Create the ProgramUser, which fails.
        ProgramUserDTO programUserDTO = programUserMapper.toDto(programUser);

        restProgramUserMockMvc.perform(post("/api/program-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUser> programUserList = programUserRepository.findAll();
        assertThat(programUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramUsers() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList
        restProgramUserMockMvc.perform(get("/api/program-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID.toString())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].totalUserPoint").value(hasItem(DEFAULT_TOTAL_USER_POINT.intValue())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].currentLevel").value(hasItem(DEFAULT_CURRENT_LEVEL)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID.toString())))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME.toString())))
            .andExpect(jsonPath("$.[*].isTobaccoUser").value(hasItem(DEFAULT_IS_TOBACCO_USER.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getProgramUser() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get the programUser
        restProgramUserMockMvc.perform(get("/api/program-users/{id}", programUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programUser.getId().intValue()))
            .andExpect(jsonPath("$.participantId").value(DEFAULT_PARTICIPANT_ID.toString()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.totalUserPoint").value(DEFAULT_TOTAL_USER_POINT.intValue()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()))
            .andExpect(jsonPath("$.currentLevel").value(DEFAULT_CURRENT_LEVEL))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE.toString()))
            .andExpect(jsonPath("$.clientName").value(DEFAULT_CLIENT_NAME.toString()))
            .andExpect(jsonPath("$.subgroupId").value(DEFAULT_SUBGROUP_ID.toString()))
            .andExpect(jsonPath("$.subgroupName").value(DEFAULT_SUBGROUP_NAME.toString()))
            .andExpect(jsonPath("$.isTobaccoUser").value(DEFAULT_IS_TOBACCO_USER.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllProgramUsersByParticipantIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where participantId equals to DEFAULT_PARTICIPANT_ID
        defaultProgramUserShouldBeFound("participantId.equals=" + DEFAULT_PARTICIPANT_ID);

        // Get all the programUserList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultProgramUserShouldNotBeFound("participantId.equals=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByParticipantIdIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where participantId in DEFAULT_PARTICIPANT_ID or UPDATED_PARTICIPANT_ID
        defaultProgramUserShouldBeFound("participantId.in=" + DEFAULT_PARTICIPANT_ID + "," + UPDATED_PARTICIPANT_ID);

        // Get all the programUserList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultProgramUserShouldNotBeFound("participantId.in=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByParticipantIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where participantId is not null
        defaultProgramUserShouldBeFound("participantId.specified=true");

        // Get all the programUserList where participantId is null
        defaultProgramUserShouldNotBeFound("participantId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where clientId equals to DEFAULT_CLIENT_ID
        defaultProgramUserShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the programUserList where clientId equals to UPDATED_CLIENT_ID
        defaultProgramUserShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultProgramUserShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the programUserList where clientId equals to UPDATED_CLIENT_ID
        defaultProgramUserShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where clientId is not null
        defaultProgramUserShouldBeFound("clientId.specified=true");

        // Get all the programUserList where clientId is null
        defaultProgramUserShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByTotalUserPointIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where totalUserPoint equals to DEFAULT_TOTAL_USER_POINT
        defaultProgramUserShouldBeFound("totalUserPoint.equals=" + DEFAULT_TOTAL_USER_POINT);

        // Get all the programUserList where totalUserPoint equals to UPDATED_TOTAL_USER_POINT
        defaultProgramUserShouldNotBeFound("totalUserPoint.equals=" + UPDATED_TOTAL_USER_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByTotalUserPointIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where totalUserPoint in DEFAULT_TOTAL_USER_POINT or UPDATED_TOTAL_USER_POINT
        defaultProgramUserShouldBeFound("totalUserPoint.in=" + DEFAULT_TOTAL_USER_POINT + "," + UPDATED_TOTAL_USER_POINT);

        // Get all the programUserList where totalUserPoint equals to UPDATED_TOTAL_USER_POINT
        defaultProgramUserShouldNotBeFound("totalUserPoint.in=" + UPDATED_TOTAL_USER_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByTotalUserPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where totalUserPoint is not null
        defaultProgramUserShouldBeFound("totalUserPoint.specified=true");

        // Get all the programUserList where totalUserPoint is null
        defaultProgramUserShouldNotBeFound("totalUserPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByProgramIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where programId equals to DEFAULT_PROGRAM_ID
        defaultProgramUserShouldBeFound("programId.equals=" + DEFAULT_PROGRAM_ID);

        // Get all the programUserList where programId equals to UPDATED_PROGRAM_ID
        defaultProgramUserShouldNotBeFound("programId.equals=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByProgramIdIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where programId in DEFAULT_PROGRAM_ID or UPDATED_PROGRAM_ID
        defaultProgramUserShouldBeFound("programId.in=" + DEFAULT_PROGRAM_ID + "," + UPDATED_PROGRAM_ID);

        // Get all the programUserList where programId equals to UPDATED_PROGRAM_ID
        defaultProgramUserShouldNotBeFound("programId.in=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByProgramIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where programId is not null
        defaultProgramUserShouldBeFound("programId.specified=true");

        // Get all the programUserList where programId is null
        defaultProgramUserShouldNotBeFound("programId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByProgramIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where programId greater than or equals to DEFAULT_PROGRAM_ID
        defaultProgramUserShouldBeFound("programId.greaterOrEqualThan=" + DEFAULT_PROGRAM_ID);

        // Get all the programUserList where programId greater than or equals to UPDATED_PROGRAM_ID
        defaultProgramUserShouldNotBeFound("programId.greaterOrEqualThan=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByProgramIdIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where programId less than or equals to DEFAULT_PROGRAM_ID
        defaultProgramUserShouldNotBeFound("programId.lessThan=" + DEFAULT_PROGRAM_ID);

        // Get all the programUserList where programId less than or equals to UPDATED_PROGRAM_ID
        defaultProgramUserShouldBeFound("programId.lessThan=" + UPDATED_PROGRAM_ID);
    }


    @Test
    @Transactional
    public void getAllProgramUsersByCurrentLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where currentLevel equals to DEFAULT_CURRENT_LEVEL
        defaultProgramUserShouldBeFound("currentLevel.equals=" + DEFAULT_CURRENT_LEVEL);

        // Get all the programUserList where currentLevel equals to UPDATED_CURRENT_LEVEL
        defaultProgramUserShouldNotBeFound("currentLevel.equals=" + UPDATED_CURRENT_LEVEL);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByCurrentLevelIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where currentLevel in DEFAULT_CURRENT_LEVEL or UPDATED_CURRENT_LEVEL
        defaultProgramUserShouldBeFound("currentLevel.in=" + DEFAULT_CURRENT_LEVEL + "," + UPDATED_CURRENT_LEVEL);

        // Get all the programUserList where currentLevel equals to UPDATED_CURRENT_LEVEL
        defaultProgramUserShouldNotBeFound("currentLevel.in=" + UPDATED_CURRENT_LEVEL);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByCurrentLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where currentLevel is not null
        defaultProgramUserShouldBeFound("currentLevel.specified=true");

        // Get all the programUserList where currentLevel is null
        defaultProgramUserShouldNotBeFound("currentLevel.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByCurrentLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where currentLevel greater than or equals to DEFAULT_CURRENT_LEVEL
        defaultProgramUserShouldBeFound("currentLevel.greaterOrEqualThan=" + DEFAULT_CURRENT_LEVEL);

        // Get all the programUserList where currentLevel greater than or equals to UPDATED_CURRENT_LEVEL
        defaultProgramUserShouldNotBeFound("currentLevel.greaterOrEqualThan=" + UPDATED_CURRENT_LEVEL);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByCurrentLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where currentLevel less than or equals to DEFAULT_CURRENT_LEVEL
        defaultProgramUserShouldNotBeFound("currentLevel.lessThan=" + DEFAULT_CURRENT_LEVEL);

        // Get all the programUserList where currentLevel less than or equals to UPDATED_CURRENT_LEVEL
        defaultProgramUserShouldBeFound("currentLevel.lessThan=" + UPDATED_CURRENT_LEVEL);
    }


    @Test
    @Transactional
    public void getAllProgramUsersByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where email equals to DEFAULT_EMAIL
        defaultProgramUserShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the programUserList where email equals to UPDATED_EMAIL
        defaultProgramUserShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultProgramUserShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the programUserList where email equals to UPDATED_EMAIL
        defaultProgramUserShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where email is not null
        defaultProgramUserShouldBeFound("email.specified=true");

        // Get all the programUserList where email is null
        defaultProgramUserShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByFirstNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where firstName equals to DEFAULT_FIRST_NAME
        defaultProgramUserShouldBeFound("firstName.equals=" + DEFAULT_FIRST_NAME);

        // Get all the programUserList where firstName equals to UPDATED_FIRST_NAME
        defaultProgramUserShouldNotBeFound("firstName.equals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByFirstNameIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where firstName in DEFAULT_FIRST_NAME or UPDATED_FIRST_NAME
        defaultProgramUserShouldBeFound("firstName.in=" + DEFAULT_FIRST_NAME + "," + UPDATED_FIRST_NAME);

        // Get all the programUserList where firstName equals to UPDATED_FIRST_NAME
        defaultProgramUserShouldNotBeFound("firstName.in=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByFirstNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where firstName is not null
        defaultProgramUserShouldBeFound("firstName.specified=true");

        // Get all the programUserList where firstName is null
        defaultProgramUserShouldNotBeFound("firstName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByLastNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where lastName equals to DEFAULT_LAST_NAME
        defaultProgramUserShouldBeFound("lastName.equals=" + DEFAULT_LAST_NAME);

        // Get all the programUserList where lastName equals to UPDATED_LAST_NAME
        defaultProgramUserShouldNotBeFound("lastName.equals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByLastNameIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where lastName in DEFAULT_LAST_NAME or UPDATED_LAST_NAME
        defaultProgramUserShouldBeFound("lastName.in=" + DEFAULT_LAST_NAME + "," + UPDATED_LAST_NAME);

        // Get all the programUserList where lastName equals to UPDATED_LAST_NAME
        defaultProgramUserShouldNotBeFound("lastName.in=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByLastNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where lastName is not null
        defaultProgramUserShouldBeFound("lastName.specified=true");

        // Get all the programUserList where lastName is null
        defaultProgramUserShouldNotBeFound("lastName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where phone equals to DEFAULT_PHONE
        defaultProgramUserShouldBeFound("phone.equals=" + DEFAULT_PHONE);

        // Get all the programUserList where phone equals to UPDATED_PHONE
        defaultProgramUserShouldNotBeFound("phone.equals=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where phone in DEFAULT_PHONE or UPDATED_PHONE
        defaultProgramUserShouldBeFound("phone.in=" + DEFAULT_PHONE + "," + UPDATED_PHONE);

        // Get all the programUserList where phone equals to UPDATED_PHONE
        defaultProgramUserShouldNotBeFound("phone.in=" + UPDATED_PHONE);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where phone is not null
        defaultProgramUserShouldBeFound("phone.specified=true");

        // Get all the programUserList where phone is null
        defaultProgramUserShouldNotBeFound("phone.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByClientNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where clientName equals to DEFAULT_CLIENT_NAME
        defaultProgramUserShouldBeFound("clientName.equals=" + DEFAULT_CLIENT_NAME);

        // Get all the programUserList where clientName equals to UPDATED_CLIENT_NAME
        defaultProgramUserShouldNotBeFound("clientName.equals=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByClientNameIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where clientName in DEFAULT_CLIENT_NAME or UPDATED_CLIENT_NAME
        defaultProgramUserShouldBeFound("clientName.in=" + DEFAULT_CLIENT_NAME + "," + UPDATED_CLIENT_NAME);

        // Get all the programUserList where clientName equals to UPDATED_CLIENT_NAME
        defaultProgramUserShouldNotBeFound("clientName.in=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByClientNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where clientName is not null
        defaultProgramUserShouldBeFound("clientName.specified=true");

        // Get all the programUserList where clientName is null
        defaultProgramUserShouldNotBeFound("clientName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersBySubgroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where subgroupId equals to DEFAULT_SUBGROUP_ID
        defaultProgramUserShouldBeFound("subgroupId.equals=" + DEFAULT_SUBGROUP_ID);

        // Get all the programUserList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramUserShouldNotBeFound("subgroupId.equals=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersBySubgroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where subgroupId in DEFAULT_SUBGROUP_ID or UPDATED_SUBGROUP_ID
        defaultProgramUserShouldBeFound("subgroupId.in=" + DEFAULT_SUBGROUP_ID + "," + UPDATED_SUBGROUP_ID);

        // Get all the programUserList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramUserShouldNotBeFound("subgroupId.in=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUsersBySubgroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where subgroupId is not null
        defaultProgramUserShouldBeFound("subgroupId.specified=true");

        // Get all the programUserList where subgroupId is null
        defaultProgramUserShouldNotBeFound("subgroupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersBySubgroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where subgroupName equals to DEFAULT_SUBGROUP_NAME
        defaultProgramUserShouldBeFound("subgroupName.equals=" + DEFAULT_SUBGROUP_NAME);

        // Get all the programUserList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramUserShouldNotBeFound("subgroupName.equals=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramUsersBySubgroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where subgroupName in DEFAULT_SUBGROUP_NAME or UPDATED_SUBGROUP_NAME
        defaultProgramUserShouldBeFound("subgroupName.in=" + DEFAULT_SUBGROUP_NAME + "," + UPDATED_SUBGROUP_NAME);

        // Get all the programUserList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramUserShouldNotBeFound("subgroupName.in=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramUsersBySubgroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where subgroupName is not null
        defaultProgramUserShouldBeFound("subgroupName.specified=true");

        // Get all the programUserList where subgroupName is null
        defaultProgramUserShouldNotBeFound("subgroupName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUsersByIsTobaccoUserIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where isTobaccoUser equals to DEFAULT_IS_TOBACCO_USER
        defaultProgramUserShouldBeFound("isTobaccoUser.equals=" + DEFAULT_IS_TOBACCO_USER);

        // Get all the programUserList where isTobaccoUser equals to UPDATED_IS_TOBACCO_USER
        defaultProgramUserShouldNotBeFound("isTobaccoUser.equals=" + UPDATED_IS_TOBACCO_USER);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByIsTobaccoUserIsInShouldWork() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where isTobaccoUser in DEFAULT_IS_TOBACCO_USER or UPDATED_IS_TOBACCO_USER
        defaultProgramUserShouldBeFound("isTobaccoUser.in=" + DEFAULT_IS_TOBACCO_USER + "," + UPDATED_IS_TOBACCO_USER);

        // Get all the programUserList where isTobaccoUser equals to UPDATED_IS_TOBACCO_USER
        defaultProgramUserShouldNotBeFound("isTobaccoUser.in=" + UPDATED_IS_TOBACCO_USER);
    }

    @Test
    @Transactional
    public void getAllProgramUsersByIsTobaccoUserIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        // Get all the programUserList where isTobaccoUser is not null
        defaultProgramUserShouldBeFound("isTobaccoUser.specified=true");

        // Get all the programUserList where isTobaccoUser is null
        defaultProgramUserShouldNotBeFound("isTobaccoUser.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramUserShouldBeFound(String filter) throws Exception {
        restProgramUserMockMvc.perform(get("/api/program-users?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID)))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].totalUserPoint").value(hasItem(DEFAULT_TOTAL_USER_POINT.intValue())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].currentLevel").value(hasItem(DEFAULT_CURRENT_LEVEL)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME)))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID)))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME)))
            .andExpect(jsonPath("$.[*].isTobaccoUser").value(hasItem(DEFAULT_IS_TOBACCO_USER.booleanValue())));

        // Check, that the count call also returns 1
        restProgramUserMockMvc.perform(get("/api/program-users/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramUserShouldNotBeFound(String filter) throws Exception {
        restProgramUserMockMvc.perform(get("/api/program-users?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramUserMockMvc.perform(get("/api/program-users/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramUser() throws Exception {
        // Get the programUser
        restProgramUserMockMvc.perform(get("/api/program-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramUser() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        int databaseSizeBeforeUpdate = programUserRepository.findAll().size();

        // Update the programUser
        ProgramUser updatedProgramUser = programUserRepository.findById(programUser.getId()).get();
        // Disconnect from session so that the updates on updatedProgramUser are not directly saved in db
        em.detach(updatedProgramUser);
        updatedProgramUser
            .participantId(UPDATED_PARTICIPANT_ID)
            .clientId(UPDATED_CLIENT_ID)
            .totalUserPoint(UPDATED_TOTAL_USER_POINT)
            .programId(UPDATED_PROGRAM_ID)
            .currentLevel(UPDATED_CURRENT_LEVEL)
            .email(UPDATED_EMAIL)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .phone(UPDATED_PHONE)
            .clientName(UPDATED_CLIENT_NAME)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME)
            .isTobaccoUser(UPDATED_IS_TOBACCO_USER);
        ProgramUserDTO programUserDTO = programUserMapper.toDto(updatedProgramUser);

        restProgramUserMockMvc.perform(put("/api/program-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramUser in the database
        List<ProgramUser> programUserList = programUserRepository.findAll();
        assertThat(programUserList).hasSize(databaseSizeBeforeUpdate);
        ProgramUser testProgramUser = programUserList.get(programUserList.size() - 1);
        assertThat(testProgramUser.getParticipantId()).isEqualTo(UPDATED_PARTICIPANT_ID);
        assertThat(testProgramUser.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testProgramUser.getTotalUserPoint()).isEqualTo(UPDATED_TOTAL_USER_POINT);
        assertThat(testProgramUser.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
        assertThat(testProgramUser.getCurrentLevel()).isEqualTo(UPDATED_CURRENT_LEVEL);
        assertThat(testProgramUser.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testProgramUser.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testProgramUser.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testProgramUser.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testProgramUser.getClientName()).isEqualTo(UPDATED_CLIENT_NAME);
        assertThat(testProgramUser.getSubgroupId()).isEqualTo(UPDATED_SUBGROUP_ID);
        assertThat(testProgramUser.getSubgroupName()).isEqualTo(UPDATED_SUBGROUP_NAME);
        assertThat(testProgramUser.isIsTobaccoUser()).isEqualTo(UPDATED_IS_TOBACCO_USER);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramUser() throws Exception {
        int databaseSizeBeforeUpdate = programUserRepository.findAll().size();

        // Create the ProgramUser
        ProgramUserDTO programUserDTO = programUserMapper.toDto(programUser);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramUserMockMvc.perform(put("/api/program-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUser in the database
        List<ProgramUser> programUserList = programUserRepository.findAll();
        assertThat(programUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramUser() throws Exception {
        // Initialize the database
        programUserRepository.saveAndFlush(programUser);

        int databaseSizeBeforeDelete = programUserRepository.findAll().size();

        // Delete the programUser
        restProgramUserMockMvc.perform(delete("/api/program-users/{id}", programUser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramUser> programUserList = programUserRepository.findAll();
        assertThat(programUserList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUser.class);
        ProgramUser programUser1 = new ProgramUser();
        programUser1.setId(1L);
        ProgramUser programUser2 = new ProgramUser();
        programUser2.setId(programUser1.getId());
        assertThat(programUser1).isEqualTo(programUser2);
        programUser2.setId(2L);
        assertThat(programUser1).isNotEqualTo(programUser2);
        programUser1.setId(null);
        assertThat(programUser1).isNotEqualTo(programUser2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserDTO.class);
        ProgramUserDTO programUserDTO1 = new ProgramUserDTO();
        programUserDTO1.setId(1L);
        ProgramUserDTO programUserDTO2 = new ProgramUserDTO();
        assertThat(programUserDTO1).isNotEqualTo(programUserDTO2);
        programUserDTO2.setId(programUserDTO1.getId());
        assertThat(programUserDTO1).isEqualTo(programUserDTO2);
        programUserDTO2.setId(2L);
        assertThat(programUserDTO1).isNotEqualTo(programUserDTO2);
        programUserDTO1.setId(null);
        assertThat(programUserDTO1).isNotEqualTo(programUserDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programUserMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programUserMapper.fromId(null)).isNull();
    }
}
