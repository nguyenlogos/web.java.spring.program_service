package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramCohortDataInput;
import aduro.basic.programservice.repository.ProgramCohortDataInputRepository;
import aduro.basic.programservice.service.ProgramCohortDataInputService;
import aduro.basic.programservice.service.dto.ProgramCohortDataInputDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortDataInputMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramCohortDataInputCriteria;
import aduro.basic.programservice.service.ProgramCohortDataInputQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramCohortDataInputResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramCohortDataInputResourceIT {

    private static final String DEFAULT_NAME_DATA_INPUT = "AAAAAAAAAA";
    private static final String UPDATED_NAME_DATA_INPUT = "BBBBBBBBBB";

    private static final String DEFAULT_INPUT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_INPUT_TYPE = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramCohortDataInputRepository programCohortDataInputRepository;

    @Autowired
    private ProgramCohortDataInputMapper programCohortDataInputMapper;

    @Autowired
    private ProgramCohortDataInputService programCohortDataInputService;

    @Autowired
    private ProgramCohortDataInputQueryService programCohortDataInputQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramCohortDataInputMockMvc;

    private ProgramCohortDataInput programCohortDataInput;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramCohortDataInputResource programCohortDataInputResource = new ProgramCohortDataInputResource(programCohortDataInputService, programCohortDataInputQueryService);
        this.restProgramCohortDataInputMockMvc = MockMvcBuilders.standaloneSetup(programCohortDataInputResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCohortDataInput createEntity(EntityManager em) {
        ProgramCohortDataInput programCohortDataInput = new ProgramCohortDataInput()
            .nameDataInput(DEFAULT_NAME_DATA_INPUT)
            .inputType(DEFAULT_INPUT_TYPE)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        return programCohortDataInput;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCohortDataInput createUpdatedEntity(EntityManager em) {
        ProgramCohortDataInput programCohortDataInput = new ProgramCohortDataInput()
            .nameDataInput(UPDATED_NAME_DATA_INPUT)
            .inputType(UPDATED_INPUT_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        return programCohortDataInput;
    }

    @BeforeEach
    public void initTest() {
        programCohortDataInput = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramCohortDataInput() throws Exception {
        int databaseSizeBeforeCreate = programCohortDataInputRepository.findAll().size();

        // Create the ProgramCohortDataInput
        ProgramCohortDataInputDTO programCohortDataInputDTO = programCohortDataInputMapper.toDto(programCohortDataInput);
        restProgramCohortDataInputMockMvc.perform(post("/api/program-cohort-data-inputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDataInputDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramCohortDataInput in the database
        List<ProgramCohortDataInput> programCohortDataInputList = programCohortDataInputRepository.findAll();
        assertThat(programCohortDataInputList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramCohortDataInput testProgramCohortDataInput = programCohortDataInputList.get(programCohortDataInputList.size() - 1);
        assertThat(testProgramCohortDataInput.getNameDataInput()).isEqualTo(DEFAULT_NAME_DATA_INPUT);
        assertThat(testProgramCohortDataInput.getInputType()).isEqualTo(DEFAULT_INPUT_TYPE);
        assertThat(testProgramCohortDataInput.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramCohortDataInput.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramCohortDataInputWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programCohortDataInputRepository.findAll().size();

        // Create the ProgramCohortDataInput with an existing ID
        programCohortDataInput.setId(1L);
        ProgramCohortDataInputDTO programCohortDataInputDTO = programCohortDataInputMapper.toDto(programCohortDataInput);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramCohortDataInputMockMvc.perform(post("/api/program-cohort-data-inputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDataInputDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCohortDataInput in the database
        List<ProgramCohortDataInput> programCohortDataInputList = programCohortDataInputRepository.findAll();
        assertThat(programCohortDataInputList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortDataInputRepository.findAll().size();
        // set the field null
        programCohortDataInput.setCreatedDate(null);

        // Create the ProgramCohortDataInput, which fails.
        ProgramCohortDataInputDTO programCohortDataInputDTO = programCohortDataInputMapper.toDto(programCohortDataInput);

        restProgramCohortDataInputMockMvc.perform(post("/api/program-cohort-data-inputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDataInputDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohortDataInput> programCohortDataInputList = programCohortDataInputRepository.findAll();
        assertThat(programCohortDataInputList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModifiedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortDataInputRepository.findAll().size();
        // set the field null
        programCohortDataInput.setModifiedDate(null);

        // Create the ProgramCohortDataInput, which fails.
        ProgramCohortDataInputDTO programCohortDataInputDTO = programCohortDataInputMapper.toDto(programCohortDataInput);

        restProgramCohortDataInputMockMvc.perform(post("/api/program-cohort-data-inputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDataInputDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohortDataInput> programCohortDataInputList = programCohortDataInputRepository.findAll();
        assertThat(programCohortDataInputList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputs() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList
        restProgramCohortDataInputMockMvc.perform(get("/api/program-cohort-data-inputs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCohortDataInput.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameDataInput").value(hasItem(DEFAULT_NAME_DATA_INPUT.toString())))
            .andExpect(jsonPath("$.[*].inputType").value(hasItem(DEFAULT_INPUT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramCohortDataInput() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get the programCohortDataInput
        restProgramCohortDataInputMockMvc.perform(get("/api/program-cohort-data-inputs/{id}", programCohortDataInput.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programCohortDataInput.getId().intValue()))
            .andExpect(jsonPath("$.nameDataInput").value(DEFAULT_NAME_DATA_INPUT.toString()))
            .andExpect(jsonPath("$.inputType").value(DEFAULT_INPUT_TYPE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByNameDataInputIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where nameDataInput equals to DEFAULT_NAME_DATA_INPUT
        defaultProgramCohortDataInputShouldBeFound("nameDataInput.equals=" + DEFAULT_NAME_DATA_INPUT);

        // Get all the programCohortDataInputList where nameDataInput equals to UPDATED_NAME_DATA_INPUT
        defaultProgramCohortDataInputShouldNotBeFound("nameDataInput.equals=" + UPDATED_NAME_DATA_INPUT);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByNameDataInputIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where nameDataInput in DEFAULT_NAME_DATA_INPUT or UPDATED_NAME_DATA_INPUT
        defaultProgramCohortDataInputShouldBeFound("nameDataInput.in=" + DEFAULT_NAME_DATA_INPUT + "," + UPDATED_NAME_DATA_INPUT);

        // Get all the programCohortDataInputList where nameDataInput equals to UPDATED_NAME_DATA_INPUT
        defaultProgramCohortDataInputShouldNotBeFound("nameDataInput.in=" + UPDATED_NAME_DATA_INPUT);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByNameDataInputIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where nameDataInput is not null
        defaultProgramCohortDataInputShouldBeFound("nameDataInput.specified=true");

        // Get all the programCohortDataInputList where nameDataInput is null
        defaultProgramCohortDataInputShouldNotBeFound("nameDataInput.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByInputTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where inputType equals to DEFAULT_INPUT_TYPE
        defaultProgramCohortDataInputShouldBeFound("inputType.equals=" + DEFAULT_INPUT_TYPE);

        // Get all the programCohortDataInputList where inputType equals to UPDATED_INPUT_TYPE
        defaultProgramCohortDataInputShouldNotBeFound("inputType.equals=" + UPDATED_INPUT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByInputTypeIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where inputType in DEFAULT_INPUT_TYPE or UPDATED_INPUT_TYPE
        defaultProgramCohortDataInputShouldBeFound("inputType.in=" + DEFAULT_INPUT_TYPE + "," + UPDATED_INPUT_TYPE);

        // Get all the programCohortDataInputList where inputType equals to UPDATED_INPUT_TYPE
        defaultProgramCohortDataInputShouldNotBeFound("inputType.in=" + UPDATED_INPUT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByInputTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where inputType is not null
        defaultProgramCohortDataInputShouldBeFound("inputType.specified=true");

        // Get all the programCohortDataInputList where inputType is null
        defaultProgramCohortDataInputShouldNotBeFound("inputType.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramCohortDataInputShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programCohortDataInputList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCohortDataInputShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramCohortDataInputShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programCohortDataInputList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCohortDataInputShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where createdDate is not null
        defaultProgramCohortDataInputShouldBeFound("createdDate.specified=true");

        // Get all the programCohortDataInputList where createdDate is null
        defaultProgramCohortDataInputShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramCohortDataInputShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programCohortDataInputList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCohortDataInputShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramCohortDataInputShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programCohortDataInputList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCohortDataInputShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortDataInputsByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        // Get all the programCohortDataInputList where modifiedDate is not null
        defaultProgramCohortDataInputShouldBeFound("modifiedDate.specified=true");

        // Get all the programCohortDataInputList where modifiedDate is null
        defaultProgramCohortDataInputShouldNotBeFound("modifiedDate.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramCohortDataInputShouldBeFound(String filter) throws Exception {
        restProgramCohortDataInputMockMvc.perform(get("/api/program-cohort-data-inputs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCohortDataInput.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameDataInput").value(hasItem(DEFAULT_NAME_DATA_INPUT)))
            .andExpect(jsonPath("$.[*].inputType").value(hasItem(DEFAULT_INPUT_TYPE)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramCohortDataInputMockMvc.perform(get("/api/program-cohort-data-inputs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramCohortDataInputShouldNotBeFound(String filter) throws Exception {
        restProgramCohortDataInputMockMvc.perform(get("/api/program-cohort-data-inputs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramCohortDataInputMockMvc.perform(get("/api/program-cohort-data-inputs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramCohortDataInput() throws Exception {
        // Get the programCohortDataInput
        restProgramCohortDataInputMockMvc.perform(get("/api/program-cohort-data-inputs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramCohortDataInput() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        int databaseSizeBeforeUpdate = programCohortDataInputRepository.findAll().size();

        // Update the programCohortDataInput
        ProgramCohortDataInput updatedProgramCohortDataInput = programCohortDataInputRepository.findById(programCohortDataInput.getId()).get();
        // Disconnect from session so that the updates on updatedProgramCohortDataInput are not directly saved in db
        em.detach(updatedProgramCohortDataInput);
        updatedProgramCohortDataInput
            .nameDataInput(UPDATED_NAME_DATA_INPUT)
            .inputType(UPDATED_INPUT_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        ProgramCohortDataInputDTO programCohortDataInputDTO = programCohortDataInputMapper.toDto(updatedProgramCohortDataInput);

        restProgramCohortDataInputMockMvc.perform(put("/api/program-cohort-data-inputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDataInputDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramCohortDataInput in the database
        List<ProgramCohortDataInput> programCohortDataInputList = programCohortDataInputRepository.findAll();
        assertThat(programCohortDataInputList).hasSize(databaseSizeBeforeUpdate);
        ProgramCohortDataInput testProgramCohortDataInput = programCohortDataInputList.get(programCohortDataInputList.size() - 1);
        assertThat(testProgramCohortDataInput.getNameDataInput()).isEqualTo(UPDATED_NAME_DATA_INPUT);
        assertThat(testProgramCohortDataInput.getInputType()).isEqualTo(UPDATED_INPUT_TYPE);
        assertThat(testProgramCohortDataInput.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramCohortDataInput.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramCohortDataInput() throws Exception {
        int databaseSizeBeforeUpdate = programCohortDataInputRepository.findAll().size();

        // Create the ProgramCohortDataInput
        ProgramCohortDataInputDTO programCohortDataInputDTO = programCohortDataInputMapper.toDto(programCohortDataInput);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramCohortDataInputMockMvc.perform(put("/api/program-cohort-data-inputs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDataInputDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCohortDataInput in the database
        List<ProgramCohortDataInput> programCohortDataInputList = programCohortDataInputRepository.findAll();
        assertThat(programCohortDataInputList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramCohortDataInput() throws Exception {
        // Initialize the database
        programCohortDataInputRepository.saveAndFlush(programCohortDataInput);

        int databaseSizeBeforeDelete = programCohortDataInputRepository.findAll().size();

        // Delete the programCohortDataInput
        restProgramCohortDataInputMockMvc.perform(delete("/api/program-cohort-data-inputs/{id}", programCohortDataInput.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramCohortDataInput> programCohortDataInputList = programCohortDataInputRepository.findAll();
        assertThat(programCohortDataInputList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCohortDataInput.class);
        ProgramCohortDataInput programCohortDataInput1 = new ProgramCohortDataInput();
        programCohortDataInput1.setId(1L);
        ProgramCohortDataInput programCohortDataInput2 = new ProgramCohortDataInput();
        programCohortDataInput2.setId(programCohortDataInput1.getId());
        assertThat(programCohortDataInput1).isEqualTo(programCohortDataInput2);
        programCohortDataInput2.setId(2L);
        assertThat(programCohortDataInput1).isNotEqualTo(programCohortDataInput2);
        programCohortDataInput1.setId(null);
        assertThat(programCohortDataInput1).isNotEqualTo(programCohortDataInput2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCohortDataInputDTO.class);
        ProgramCohortDataInputDTO programCohortDataInputDTO1 = new ProgramCohortDataInputDTO();
        programCohortDataInputDTO1.setId(1L);
        ProgramCohortDataInputDTO programCohortDataInputDTO2 = new ProgramCohortDataInputDTO();
        assertThat(programCohortDataInputDTO1).isNotEqualTo(programCohortDataInputDTO2);
        programCohortDataInputDTO2.setId(programCohortDataInputDTO1.getId());
        assertThat(programCohortDataInputDTO1).isEqualTo(programCohortDataInputDTO2);
        programCohortDataInputDTO2.setId(2L);
        assertThat(programCohortDataInputDTO1).isNotEqualTo(programCohortDataInputDTO2);
        programCohortDataInputDTO1.setId(null);
        assertThat(programCohortDataInputDTO1).isNotEqualTo(programCohortDataInputDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programCohortDataInputMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programCohortDataInputMapper.fromId(null)).isNull();
    }
}
