package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ClientSubDomain;
import aduro.basic.programservice.repository.ClientSubDomainRepository;
import aduro.basic.programservice.service.ClientSubDomainService;
import aduro.basic.programservice.service.dto.ClientSubDomainDTO;
import aduro.basic.programservice.service.mapper.ClientSubDomainMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ClientSubDomainCriteria;
import aduro.basic.programservice.service.ClientSubDomainQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ClientSubDomainResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ClientSubDomainResourceIT {

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_DOMAIN_URL = "AAAAAAAAAA";
    private static final String UPDATED_SUB_DOMAIN_URL = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    private static final String DEFAULT_PROGRAM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PROGRAM_NAME = "BBBBBBBBBB";

    @Autowired
    private ClientSubDomainRepository clientSubDomainRepository;

    @Autowired
    private ClientSubDomainMapper clientSubDomainMapper;

    @Autowired
    private ClientSubDomainService clientSubDomainService;

    @Autowired
    private ClientSubDomainQueryService clientSubDomainQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClientSubDomainMockMvc;

    private ClientSubDomain clientSubDomain;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientSubDomainResource clientSubDomainResource = new ClientSubDomainResource(clientSubDomainService, clientSubDomainQueryService);
        this.restClientSubDomainMockMvc = MockMvcBuilders.standaloneSetup(clientSubDomainResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientSubDomain createEntity(EntityManager em) {
        ClientSubDomain clientSubDomain = new ClientSubDomain()
            .clientId(DEFAULT_CLIENT_ID)
            .subDomainUrl(DEFAULT_SUB_DOMAIN_URL)
            .createdAt(DEFAULT_CREATED_AT)
            .modifiedAt(DEFAULT_MODIFIED_AT)
            .programId(DEFAULT_PROGRAM_ID)
            .programName(DEFAULT_PROGRAM_NAME);
        return clientSubDomain;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientSubDomain createUpdatedEntity(EntityManager em) {
        ClientSubDomain clientSubDomain = new ClientSubDomain()
            .clientId(UPDATED_CLIENT_ID)
            .subDomainUrl(UPDATED_SUB_DOMAIN_URL)
            .createdAt(UPDATED_CREATED_AT)
            .modifiedAt(UPDATED_MODIFIED_AT)
            .programId(UPDATED_PROGRAM_ID)
            .programName(UPDATED_PROGRAM_NAME);
        return clientSubDomain;
    }

    @BeforeEach
    public void initTest() {
        clientSubDomain = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientSubDomain() throws Exception {
        int databaseSizeBeforeCreate = clientSubDomainRepository.findAll().size();

        // Create the ClientSubDomain
        ClientSubDomainDTO clientSubDomainDTO = clientSubDomainMapper.toDto(clientSubDomain);
        restClientSubDomainMockMvc.perform(post("/api/client-sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSubDomainDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientSubDomain in the database
        List<ClientSubDomain> clientSubDomainList = clientSubDomainRepository.findAll();
        assertThat(clientSubDomainList).hasSize(databaseSizeBeforeCreate + 1);
        ClientSubDomain testClientSubDomain = clientSubDomainList.get(clientSubDomainList.size() - 1);
        assertThat(testClientSubDomain.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testClientSubDomain.getSubDomainUrl()).isEqualTo(DEFAULT_SUB_DOMAIN_URL);
        assertThat(testClientSubDomain.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testClientSubDomain.getModifiedAt()).isEqualTo(DEFAULT_MODIFIED_AT);
        assertThat(testClientSubDomain.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
        assertThat(testClientSubDomain.getProgramName()).isEqualTo(DEFAULT_PROGRAM_NAME);
    }

    @Test
    @Transactional
    public void createClientSubDomainWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientSubDomainRepository.findAll().size();

        // Create the ClientSubDomain with an existing ID
        clientSubDomain.setId(1L);
        ClientSubDomainDTO clientSubDomainDTO = clientSubDomainMapper.toDto(clientSubDomain);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientSubDomainMockMvc.perform(post("/api/client-sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSubDomainDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientSubDomain in the database
        List<ClientSubDomain> clientSubDomainList = clientSubDomainRepository.findAll();
        assertThat(clientSubDomainList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientSubDomainRepository.findAll().size();
        // set the field null
        clientSubDomain.setClientId(null);

        // Create the ClientSubDomain, which fails.
        ClientSubDomainDTO clientSubDomainDTO = clientSubDomainMapper.toDto(clientSubDomain);

        restClientSubDomainMockMvc.perform(post("/api/client-sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSubDomainDTO)))
            .andExpect(status().isBadRequest());

        List<ClientSubDomain> clientSubDomainList = clientSubDomainRepository.findAll();
        assertThat(clientSubDomainList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubDomainUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientSubDomainRepository.findAll().size();
        // set the field null
        clientSubDomain.setSubDomainUrl(null);

        // Create the ClientSubDomain, which fails.
        ClientSubDomainDTO clientSubDomainDTO = clientSubDomainMapper.toDto(clientSubDomain);

        restClientSubDomainMockMvc.perform(post("/api/client-sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSubDomainDTO)))
            .andExpect(status().isBadRequest());

        List<ClientSubDomain> clientSubDomainList = clientSubDomainRepository.findAll();
        assertThat(clientSubDomainList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientSubDomains() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList
        restClientSubDomainMockMvc.perform(get("/api/client-sub-domains?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientSubDomain.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].subDomainUrl").value(hasItem(DEFAULT_SUB_DOMAIN_URL.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].modifiedAt").value(hasItem(DEFAULT_MODIFIED_AT.toString())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].programName").value(hasItem(DEFAULT_PROGRAM_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getClientSubDomain() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get the clientSubDomain
        restClientSubDomainMockMvc.perform(get("/api/client-sub-domains/{id}", clientSubDomain.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientSubDomain.getId().intValue()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.subDomainUrl").value(DEFAULT_SUB_DOMAIN_URL.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.modifiedAt").value(DEFAULT_MODIFIED_AT.toString()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()))
            .andExpect(jsonPath("$.programName").value(DEFAULT_PROGRAM_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where clientId equals to DEFAULT_CLIENT_ID
        defaultClientSubDomainShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the clientSubDomainList where clientId equals to UPDATED_CLIENT_ID
        defaultClientSubDomainShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultClientSubDomainShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the clientSubDomainList where clientId equals to UPDATED_CLIENT_ID
        defaultClientSubDomainShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where clientId is not null
        defaultClientSubDomainShouldBeFound("clientId.specified=true");

        // Get all the clientSubDomainList where clientId is null
        defaultClientSubDomainShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsBySubDomainUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where subDomainUrl equals to DEFAULT_SUB_DOMAIN_URL
        defaultClientSubDomainShouldBeFound("subDomainUrl.equals=" + DEFAULT_SUB_DOMAIN_URL);

        // Get all the clientSubDomainList where subDomainUrl equals to UPDATED_SUB_DOMAIN_URL
        defaultClientSubDomainShouldNotBeFound("subDomainUrl.equals=" + UPDATED_SUB_DOMAIN_URL);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsBySubDomainUrlIsInShouldWork() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where subDomainUrl in DEFAULT_SUB_DOMAIN_URL or UPDATED_SUB_DOMAIN_URL
        defaultClientSubDomainShouldBeFound("subDomainUrl.in=" + DEFAULT_SUB_DOMAIN_URL + "," + UPDATED_SUB_DOMAIN_URL);

        // Get all the clientSubDomainList where subDomainUrl equals to UPDATED_SUB_DOMAIN_URL
        defaultClientSubDomainShouldNotBeFound("subDomainUrl.in=" + UPDATED_SUB_DOMAIN_URL);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsBySubDomainUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where subDomainUrl is not null
        defaultClientSubDomainShouldBeFound("subDomainUrl.specified=true");

        // Get all the clientSubDomainList where subDomainUrl is null
        defaultClientSubDomainShouldNotBeFound("subDomainUrl.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where createdAt equals to DEFAULT_CREATED_AT
        defaultClientSubDomainShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the clientSubDomainList where createdAt equals to UPDATED_CREATED_AT
        defaultClientSubDomainShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultClientSubDomainShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the clientSubDomainList where createdAt equals to UPDATED_CREATED_AT
        defaultClientSubDomainShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where createdAt is not null
        defaultClientSubDomainShouldBeFound("createdAt.specified=true");

        // Get all the clientSubDomainList where createdAt is null
        defaultClientSubDomainShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByModifiedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where modifiedAt equals to DEFAULT_MODIFIED_AT
        defaultClientSubDomainShouldBeFound("modifiedAt.equals=" + DEFAULT_MODIFIED_AT);

        // Get all the clientSubDomainList where modifiedAt equals to UPDATED_MODIFIED_AT
        defaultClientSubDomainShouldNotBeFound("modifiedAt.equals=" + UPDATED_MODIFIED_AT);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByModifiedAtIsInShouldWork() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where modifiedAt in DEFAULT_MODIFIED_AT or UPDATED_MODIFIED_AT
        defaultClientSubDomainShouldBeFound("modifiedAt.in=" + DEFAULT_MODIFIED_AT + "," + UPDATED_MODIFIED_AT);

        // Get all the clientSubDomainList where modifiedAt equals to UPDATED_MODIFIED_AT
        defaultClientSubDomainShouldNotBeFound("modifiedAt.in=" + UPDATED_MODIFIED_AT);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByModifiedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where modifiedAt is not null
        defaultClientSubDomainShouldBeFound("modifiedAt.specified=true");

        // Get all the clientSubDomainList where modifiedAt is null
        defaultClientSubDomainShouldNotBeFound("modifiedAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByProgramIdIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where programId equals to DEFAULT_PROGRAM_ID
        defaultClientSubDomainShouldBeFound("programId.equals=" + DEFAULT_PROGRAM_ID);

        // Get all the clientSubDomainList where programId equals to UPDATED_PROGRAM_ID
        defaultClientSubDomainShouldNotBeFound("programId.equals=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByProgramIdIsInShouldWork() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where programId in DEFAULT_PROGRAM_ID or UPDATED_PROGRAM_ID
        defaultClientSubDomainShouldBeFound("programId.in=" + DEFAULT_PROGRAM_ID + "," + UPDATED_PROGRAM_ID);

        // Get all the clientSubDomainList where programId equals to UPDATED_PROGRAM_ID
        defaultClientSubDomainShouldNotBeFound("programId.in=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByProgramIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where programId is not null
        defaultClientSubDomainShouldBeFound("programId.specified=true");

        // Get all the clientSubDomainList where programId is null
        defaultClientSubDomainShouldNotBeFound("programId.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByProgramIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where programId greater than or equals to DEFAULT_PROGRAM_ID
        defaultClientSubDomainShouldBeFound("programId.greaterOrEqualThan=" + DEFAULT_PROGRAM_ID);

        // Get all the clientSubDomainList where programId greater than or equals to UPDATED_PROGRAM_ID
        defaultClientSubDomainShouldNotBeFound("programId.greaterOrEqualThan=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByProgramIdIsLessThanSomething() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where programId less than or equals to DEFAULT_PROGRAM_ID
        defaultClientSubDomainShouldNotBeFound("programId.lessThan=" + DEFAULT_PROGRAM_ID);

        // Get all the clientSubDomainList where programId less than or equals to UPDATED_PROGRAM_ID
        defaultClientSubDomainShouldBeFound("programId.lessThan=" + UPDATED_PROGRAM_ID);
    }


    @Test
    @Transactional
    public void getAllClientSubDomainsByProgramNameIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where programName equals to DEFAULT_PROGRAM_NAME
        defaultClientSubDomainShouldBeFound("programName.equals=" + DEFAULT_PROGRAM_NAME);

        // Get all the clientSubDomainList where programName equals to UPDATED_PROGRAM_NAME
        defaultClientSubDomainShouldNotBeFound("programName.equals=" + UPDATED_PROGRAM_NAME);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByProgramNameIsInShouldWork() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where programName in DEFAULT_PROGRAM_NAME or UPDATED_PROGRAM_NAME
        defaultClientSubDomainShouldBeFound("programName.in=" + DEFAULT_PROGRAM_NAME + "," + UPDATED_PROGRAM_NAME);

        // Get all the clientSubDomainList where programName equals to UPDATED_PROGRAM_NAME
        defaultClientSubDomainShouldNotBeFound("programName.in=" + UPDATED_PROGRAM_NAME);
    }

    @Test
    @Transactional
    public void getAllClientSubDomainsByProgramNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        // Get all the clientSubDomainList where programName is not null
        defaultClientSubDomainShouldBeFound("programName.specified=true");

        // Get all the clientSubDomainList where programName is null
        defaultClientSubDomainShouldNotBeFound("programName.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClientSubDomainShouldBeFound(String filter) throws Exception {
        restClientSubDomainMockMvc.perform(get("/api/client-sub-domains?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientSubDomain.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].subDomainUrl").value(hasItem(DEFAULT_SUB_DOMAIN_URL)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].modifiedAt").value(hasItem(DEFAULT_MODIFIED_AT.toString())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].programName").value(hasItem(DEFAULT_PROGRAM_NAME)));

        // Check, that the count call also returns 1
        restClientSubDomainMockMvc.perform(get("/api/client-sub-domains/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClientSubDomainShouldNotBeFound(String filter) throws Exception {
        restClientSubDomainMockMvc.perform(get("/api/client-sub-domains?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClientSubDomainMockMvc.perform(get("/api/client-sub-domains/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingClientSubDomain() throws Exception {
        // Get the clientSubDomain
        restClientSubDomainMockMvc.perform(get("/api/client-sub-domains/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientSubDomain() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        int databaseSizeBeforeUpdate = clientSubDomainRepository.findAll().size();

        // Update the clientSubDomain
        ClientSubDomain updatedClientSubDomain = clientSubDomainRepository.findById(clientSubDomain.getId()).get();
        // Disconnect from session so that the updates on updatedClientSubDomain are not directly saved in db
        em.detach(updatedClientSubDomain);
        updatedClientSubDomain
            .clientId(UPDATED_CLIENT_ID)
            .subDomainUrl(UPDATED_SUB_DOMAIN_URL)
            .createdAt(UPDATED_CREATED_AT)
            .modifiedAt(UPDATED_MODIFIED_AT)
            .programId(UPDATED_PROGRAM_ID)
            .programName(UPDATED_PROGRAM_NAME);
        ClientSubDomainDTO clientSubDomainDTO = clientSubDomainMapper.toDto(updatedClientSubDomain);

        restClientSubDomainMockMvc.perform(put("/api/client-sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSubDomainDTO)))
            .andExpect(status().isOk());

        // Validate the ClientSubDomain in the database
        List<ClientSubDomain> clientSubDomainList = clientSubDomainRepository.findAll();
        assertThat(clientSubDomainList).hasSize(databaseSizeBeforeUpdate);
        ClientSubDomain testClientSubDomain = clientSubDomainList.get(clientSubDomainList.size() - 1);
        assertThat(testClientSubDomain.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testClientSubDomain.getSubDomainUrl()).isEqualTo(UPDATED_SUB_DOMAIN_URL);
        assertThat(testClientSubDomain.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testClientSubDomain.getModifiedAt()).isEqualTo(UPDATED_MODIFIED_AT);
        assertThat(testClientSubDomain.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
        assertThat(testClientSubDomain.getProgramName()).isEqualTo(UPDATED_PROGRAM_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingClientSubDomain() throws Exception {
        int databaseSizeBeforeUpdate = clientSubDomainRepository.findAll().size();

        // Create the ClientSubDomain
        ClientSubDomainDTO clientSubDomainDTO = clientSubDomainMapper.toDto(clientSubDomain);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientSubDomainMockMvc.perform(put("/api/client-sub-domains")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSubDomainDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientSubDomain in the database
        List<ClientSubDomain> clientSubDomainList = clientSubDomainRepository.findAll();
        assertThat(clientSubDomainList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClientSubDomain() throws Exception {
        // Initialize the database
        clientSubDomainRepository.saveAndFlush(clientSubDomain);

        int databaseSizeBeforeDelete = clientSubDomainRepository.findAll().size();

        // Delete the clientSubDomain
        restClientSubDomainMockMvc.perform(delete("/api/client-sub-domains/{id}", clientSubDomain.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ClientSubDomain> clientSubDomainList = clientSubDomainRepository.findAll();
        assertThat(clientSubDomainList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientSubDomain.class);
        ClientSubDomain clientSubDomain1 = new ClientSubDomain();
        clientSubDomain1.setId(1L);
        ClientSubDomain clientSubDomain2 = new ClientSubDomain();
        clientSubDomain2.setId(clientSubDomain1.getId());
        assertThat(clientSubDomain1).isEqualTo(clientSubDomain2);
        clientSubDomain2.setId(2L);
        assertThat(clientSubDomain1).isNotEqualTo(clientSubDomain2);
        clientSubDomain1.setId(null);
        assertThat(clientSubDomain1).isNotEqualTo(clientSubDomain2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientSubDomainDTO.class);
        ClientSubDomainDTO clientSubDomainDTO1 = new ClientSubDomainDTO();
        clientSubDomainDTO1.setId(1L);
        ClientSubDomainDTO clientSubDomainDTO2 = new ClientSubDomainDTO();
        assertThat(clientSubDomainDTO1).isNotEqualTo(clientSubDomainDTO2);
        clientSubDomainDTO2.setId(clientSubDomainDTO1.getId());
        assertThat(clientSubDomainDTO1).isEqualTo(clientSubDomainDTO2);
        clientSubDomainDTO2.setId(2L);
        assertThat(clientSubDomainDTO1).isNotEqualTo(clientSubDomainDTO2);
        clientSubDomainDTO1.setId(null);
        assertThat(clientSubDomainDTO1).isNotEqualTo(clientSubDomainDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientSubDomainMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientSubDomainMapper.fromId(null)).isNull();
    }
}
