package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramIntegrationTracking;
import aduro.basic.programservice.repository.ProgramIntegrationTrackingRepository;
import aduro.basic.programservice.service.ProgramIntegrationTrackingService;
import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingDTO;
import aduro.basic.programservice.service.mapper.ProgramIntegrationTrackingMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramIntegrationTrackingCriteria;
import aduro.basic.programservice.service.ProgramIntegrationTrackingQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramIntegrationTrackingResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramIntegrationTrackingResourceIT {

    private static final String DEFAULT_END_POINT = "AAAAAAAAAA";
    private static final String UPDATED_END_POINT = "BBBBBBBBBB";

    private static final String DEFAULT_PAYLOAD = "AAAAAAAAAA";
    private static final String UPDATED_PAYLOAD = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ERROR_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_SERVICE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_SERVICE_TYPE = "BBBBBBBBBB";

    private static final Instant DEFAULT_ATTEMPTED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ATTEMPTED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramIntegrationTrackingRepository programIntegrationTrackingRepository;

    @Autowired
    private ProgramIntegrationTrackingMapper programIntegrationTrackingMapper;

    @Autowired
    private ProgramIntegrationTrackingService programIntegrationTrackingService;

    @Autowired
    private ProgramIntegrationTrackingQueryService programIntegrationTrackingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramIntegrationTrackingMockMvc;

    private ProgramIntegrationTracking programIntegrationTracking;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramIntegrationTrackingResource programIntegrationTrackingResource = new ProgramIntegrationTrackingResource(programIntegrationTrackingService, programIntegrationTrackingQueryService);
        this.restProgramIntegrationTrackingMockMvc = MockMvcBuilders.standaloneSetup(programIntegrationTrackingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramIntegrationTracking createEntity(EntityManager em) {
        ProgramIntegrationTracking programIntegrationTracking = new ProgramIntegrationTracking()
            .endPoint(DEFAULT_END_POINT)
            .payload(DEFAULT_PAYLOAD)
            .createdAt(DEFAULT_CREATED_AT)
            .errorMessage(DEFAULT_ERROR_MESSAGE)
            .status(DEFAULT_STATUS)
            .serviceType(DEFAULT_SERVICE_TYPE)
            .attemptedAt(DEFAULT_ATTEMPTED_AT);
        return programIntegrationTracking;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramIntegrationTracking createUpdatedEntity(EntityManager em) {
        ProgramIntegrationTracking programIntegrationTracking = new ProgramIntegrationTracking()
            .endPoint(UPDATED_END_POINT)
            .payload(UPDATED_PAYLOAD)
            .createdAt(UPDATED_CREATED_AT)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .status(UPDATED_STATUS)
            .serviceType(UPDATED_SERVICE_TYPE)
            .attemptedAt(UPDATED_ATTEMPTED_AT);
        return programIntegrationTracking;
    }

    @BeforeEach
    public void initTest() {
        programIntegrationTracking = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramIntegrationTracking() throws Exception {
        int databaseSizeBeforeCreate = programIntegrationTrackingRepository.findAll().size();

        // Create the ProgramIntegrationTracking
        ProgramIntegrationTrackingDTO programIntegrationTrackingDTO = programIntegrationTrackingMapper.toDto(programIntegrationTracking);
        restProgramIntegrationTrackingMockMvc.perform(post("/api/program-integration-trackings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programIntegrationTrackingDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramIntegrationTracking in the database
        List<ProgramIntegrationTracking> programIntegrationTrackingList = programIntegrationTrackingRepository.findAll();
        assertThat(programIntegrationTrackingList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramIntegrationTracking testProgramIntegrationTracking = programIntegrationTrackingList.get(programIntegrationTrackingList.size() - 1);
        assertThat(testProgramIntegrationTracking.getEndPoint()).isEqualTo(DEFAULT_END_POINT);
        assertThat(testProgramIntegrationTracking.getPayload()).isEqualTo(DEFAULT_PAYLOAD);
        assertThat(testProgramIntegrationTracking.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testProgramIntegrationTracking.getErrorMessage()).isEqualTo(DEFAULT_ERROR_MESSAGE);
        assertThat(testProgramIntegrationTracking.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testProgramIntegrationTracking.getServiceType()).isEqualTo(DEFAULT_SERVICE_TYPE);
        assertThat(testProgramIntegrationTracking.getAttemptedAt()).isEqualTo(DEFAULT_ATTEMPTED_AT);
    }

    @Test
    @Transactional
    public void createProgramIntegrationTrackingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programIntegrationTrackingRepository.findAll().size();

        // Create the ProgramIntegrationTracking with an existing ID
        programIntegrationTracking.setId(1L);
        ProgramIntegrationTrackingDTO programIntegrationTrackingDTO = programIntegrationTrackingMapper.toDto(programIntegrationTracking);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramIntegrationTrackingMockMvc.perform(post("/api/program-integration-trackings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programIntegrationTrackingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramIntegrationTracking in the database
        List<ProgramIntegrationTracking> programIntegrationTrackingList = programIntegrationTrackingRepository.findAll();
        assertThat(programIntegrationTrackingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProgramIntegrationTrackings() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList
        restProgramIntegrationTrackingMockMvc.perform(get("/api/program-integration-trackings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programIntegrationTracking.getId().intValue())))
            .andExpect(jsonPath("$.[*].endPoint").value(hasItem(DEFAULT_END_POINT.toString())))
            .andExpect(jsonPath("$.[*].payload").value(hasItem(DEFAULT_PAYLOAD.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].serviceType").value(hasItem(DEFAULT_SERVICE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].attemptedAt").value(hasItem(DEFAULT_ATTEMPTED_AT.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramIntegrationTracking() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get the programIntegrationTracking
        restProgramIntegrationTrackingMockMvc.perform(get("/api/program-integration-trackings/{id}", programIntegrationTracking.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programIntegrationTracking.getId().intValue()))
            .andExpect(jsonPath("$.endPoint").value(DEFAULT_END_POINT.toString()))
            .andExpect(jsonPath("$.payload").value(DEFAULT_PAYLOAD.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.errorMessage").value(DEFAULT_ERROR_MESSAGE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.serviceType").value(DEFAULT_SERVICE_TYPE.toString()))
            .andExpect(jsonPath("$.attemptedAt").value(DEFAULT_ATTEMPTED_AT.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByEndPointIsEqualToSomething() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where endPoint equals to DEFAULT_END_POINT
        defaultProgramIntegrationTrackingShouldBeFound("endPoint.equals=" + DEFAULT_END_POINT);

        // Get all the programIntegrationTrackingList where endPoint equals to UPDATED_END_POINT
        defaultProgramIntegrationTrackingShouldNotBeFound("endPoint.equals=" + UPDATED_END_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByEndPointIsInShouldWork() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where endPoint in DEFAULT_END_POINT or UPDATED_END_POINT
        defaultProgramIntegrationTrackingShouldBeFound("endPoint.in=" + DEFAULT_END_POINT + "," + UPDATED_END_POINT);

        // Get all the programIntegrationTrackingList where endPoint equals to UPDATED_END_POINT
        defaultProgramIntegrationTrackingShouldNotBeFound("endPoint.in=" + UPDATED_END_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByEndPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where endPoint is not null
        defaultProgramIntegrationTrackingShouldBeFound("endPoint.specified=true");

        // Get all the programIntegrationTrackingList where endPoint is null
        defaultProgramIntegrationTrackingShouldNotBeFound("endPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByPayloadIsEqualToSomething() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where payload equals to DEFAULT_PAYLOAD
        defaultProgramIntegrationTrackingShouldBeFound("payload.equals=" + DEFAULT_PAYLOAD);

        // Get all the programIntegrationTrackingList where payload equals to UPDATED_PAYLOAD
        defaultProgramIntegrationTrackingShouldNotBeFound("payload.equals=" + UPDATED_PAYLOAD);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByPayloadIsInShouldWork() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where payload in DEFAULT_PAYLOAD or UPDATED_PAYLOAD
        defaultProgramIntegrationTrackingShouldBeFound("payload.in=" + DEFAULT_PAYLOAD + "," + UPDATED_PAYLOAD);

        // Get all the programIntegrationTrackingList where payload equals to UPDATED_PAYLOAD
        defaultProgramIntegrationTrackingShouldNotBeFound("payload.in=" + UPDATED_PAYLOAD);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByPayloadIsNullOrNotNull() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where payload is not null
        defaultProgramIntegrationTrackingShouldBeFound("payload.specified=true");

        // Get all the programIntegrationTrackingList where payload is null
        defaultProgramIntegrationTrackingShouldNotBeFound("payload.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where createdAt equals to DEFAULT_CREATED_AT
        defaultProgramIntegrationTrackingShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the programIntegrationTrackingList where createdAt equals to UPDATED_CREATED_AT
        defaultProgramIntegrationTrackingShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultProgramIntegrationTrackingShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the programIntegrationTrackingList where createdAt equals to UPDATED_CREATED_AT
        defaultProgramIntegrationTrackingShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where createdAt is not null
        defaultProgramIntegrationTrackingShouldBeFound("createdAt.specified=true");

        // Get all the programIntegrationTrackingList where createdAt is null
        defaultProgramIntegrationTrackingShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByErrorMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where errorMessage equals to DEFAULT_ERROR_MESSAGE
        defaultProgramIntegrationTrackingShouldBeFound("errorMessage.equals=" + DEFAULT_ERROR_MESSAGE);

        // Get all the programIntegrationTrackingList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultProgramIntegrationTrackingShouldNotBeFound("errorMessage.equals=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByErrorMessageIsInShouldWork() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where errorMessage in DEFAULT_ERROR_MESSAGE or UPDATED_ERROR_MESSAGE
        defaultProgramIntegrationTrackingShouldBeFound("errorMessage.in=" + DEFAULT_ERROR_MESSAGE + "," + UPDATED_ERROR_MESSAGE);

        // Get all the programIntegrationTrackingList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultProgramIntegrationTrackingShouldNotBeFound("errorMessage.in=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByErrorMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where errorMessage is not null
        defaultProgramIntegrationTrackingShouldBeFound("errorMessage.specified=true");

        // Get all the programIntegrationTrackingList where errorMessage is null
        defaultProgramIntegrationTrackingShouldNotBeFound("errorMessage.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where status equals to DEFAULT_STATUS
        defaultProgramIntegrationTrackingShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the programIntegrationTrackingList where status equals to UPDATED_STATUS
        defaultProgramIntegrationTrackingShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultProgramIntegrationTrackingShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the programIntegrationTrackingList where status equals to UPDATED_STATUS
        defaultProgramIntegrationTrackingShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where status is not null
        defaultProgramIntegrationTrackingShouldBeFound("status.specified=true");

        // Get all the programIntegrationTrackingList where status is null
        defaultProgramIntegrationTrackingShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByServiceTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where serviceType equals to DEFAULT_SERVICE_TYPE
        defaultProgramIntegrationTrackingShouldBeFound("serviceType.equals=" + DEFAULT_SERVICE_TYPE);

        // Get all the programIntegrationTrackingList where serviceType equals to UPDATED_SERVICE_TYPE
        defaultProgramIntegrationTrackingShouldNotBeFound("serviceType.equals=" + UPDATED_SERVICE_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByServiceTypeIsInShouldWork() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where serviceType in DEFAULT_SERVICE_TYPE or UPDATED_SERVICE_TYPE
        defaultProgramIntegrationTrackingShouldBeFound("serviceType.in=" + DEFAULT_SERVICE_TYPE + "," + UPDATED_SERVICE_TYPE);

        // Get all the programIntegrationTrackingList where serviceType equals to UPDATED_SERVICE_TYPE
        defaultProgramIntegrationTrackingShouldNotBeFound("serviceType.in=" + UPDATED_SERVICE_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByServiceTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where serviceType is not null
        defaultProgramIntegrationTrackingShouldBeFound("serviceType.specified=true");

        // Get all the programIntegrationTrackingList where serviceType is null
        defaultProgramIntegrationTrackingShouldNotBeFound("serviceType.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByAttemptedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where attemptedAt equals to DEFAULT_ATTEMPTED_AT
        defaultProgramIntegrationTrackingShouldBeFound("attemptedAt.equals=" + DEFAULT_ATTEMPTED_AT);

        // Get all the programIntegrationTrackingList where attemptedAt equals to UPDATED_ATTEMPTED_AT
        defaultProgramIntegrationTrackingShouldNotBeFound("attemptedAt.equals=" + UPDATED_ATTEMPTED_AT);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByAttemptedAtIsInShouldWork() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where attemptedAt in DEFAULT_ATTEMPTED_AT or UPDATED_ATTEMPTED_AT
        defaultProgramIntegrationTrackingShouldBeFound("attemptedAt.in=" + DEFAULT_ATTEMPTED_AT + "," + UPDATED_ATTEMPTED_AT);

        // Get all the programIntegrationTrackingList where attemptedAt equals to UPDATED_ATTEMPTED_AT
        defaultProgramIntegrationTrackingShouldNotBeFound("attemptedAt.in=" + UPDATED_ATTEMPTED_AT);
    }

    @Test
    @Transactional
    public void getAllProgramIntegrationTrackingsByAttemptedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        // Get all the programIntegrationTrackingList where attemptedAt is not null
        defaultProgramIntegrationTrackingShouldBeFound("attemptedAt.specified=true");

        // Get all the programIntegrationTrackingList where attemptedAt is null
        defaultProgramIntegrationTrackingShouldNotBeFound("attemptedAt.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramIntegrationTrackingShouldBeFound(String filter) throws Exception {
        restProgramIntegrationTrackingMockMvc.perform(get("/api/program-integration-trackings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programIntegrationTracking.getId().intValue())))
            .andExpect(jsonPath("$.[*].endPoint").value(hasItem(DEFAULT_END_POINT)))
            .andExpect(jsonPath("$.[*].payload").value(hasItem(DEFAULT_PAYLOAD)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].serviceType").value(hasItem(DEFAULT_SERVICE_TYPE)))
            .andExpect(jsonPath("$.[*].attemptedAt").value(hasItem(DEFAULT_ATTEMPTED_AT.toString())));

        // Check, that the count call also returns 1
        restProgramIntegrationTrackingMockMvc.perform(get("/api/program-integration-trackings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramIntegrationTrackingShouldNotBeFound(String filter) throws Exception {
        restProgramIntegrationTrackingMockMvc.perform(get("/api/program-integration-trackings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramIntegrationTrackingMockMvc.perform(get("/api/program-integration-trackings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramIntegrationTracking() throws Exception {
        // Get the programIntegrationTracking
        restProgramIntegrationTrackingMockMvc.perform(get("/api/program-integration-trackings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramIntegrationTracking() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        int databaseSizeBeforeUpdate = programIntegrationTrackingRepository.findAll().size();

        // Update the programIntegrationTracking
        ProgramIntegrationTracking updatedProgramIntegrationTracking = programIntegrationTrackingRepository.findById(programIntegrationTracking.getId()).get();
        // Disconnect from session so that the updates on updatedProgramIntegrationTracking are not directly saved in db
        em.detach(updatedProgramIntegrationTracking);
        updatedProgramIntegrationTracking
            .endPoint(UPDATED_END_POINT)
            .payload(UPDATED_PAYLOAD)
            .createdAt(UPDATED_CREATED_AT)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .status(UPDATED_STATUS)
            .serviceType(UPDATED_SERVICE_TYPE)
            .attemptedAt(UPDATED_ATTEMPTED_AT);
        ProgramIntegrationTrackingDTO programIntegrationTrackingDTO = programIntegrationTrackingMapper.toDto(updatedProgramIntegrationTracking);

        restProgramIntegrationTrackingMockMvc.perform(put("/api/program-integration-trackings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programIntegrationTrackingDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramIntegrationTracking in the database
        List<ProgramIntegrationTracking> programIntegrationTrackingList = programIntegrationTrackingRepository.findAll();
        assertThat(programIntegrationTrackingList).hasSize(databaseSizeBeforeUpdate);
        ProgramIntegrationTracking testProgramIntegrationTracking = programIntegrationTrackingList.get(programIntegrationTrackingList.size() - 1);
        assertThat(testProgramIntegrationTracking.getEndPoint()).isEqualTo(UPDATED_END_POINT);
        assertThat(testProgramIntegrationTracking.getPayload()).isEqualTo(UPDATED_PAYLOAD);
        assertThat(testProgramIntegrationTracking.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testProgramIntegrationTracking.getErrorMessage()).isEqualTo(UPDATED_ERROR_MESSAGE);
        assertThat(testProgramIntegrationTracking.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testProgramIntegrationTracking.getServiceType()).isEqualTo(UPDATED_SERVICE_TYPE);
        assertThat(testProgramIntegrationTracking.getAttemptedAt()).isEqualTo(UPDATED_ATTEMPTED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramIntegrationTracking() throws Exception {
        int databaseSizeBeforeUpdate = programIntegrationTrackingRepository.findAll().size();

        // Create the ProgramIntegrationTracking
        ProgramIntegrationTrackingDTO programIntegrationTrackingDTO = programIntegrationTrackingMapper.toDto(programIntegrationTracking);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramIntegrationTrackingMockMvc.perform(put("/api/program-integration-trackings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programIntegrationTrackingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramIntegrationTracking in the database
        List<ProgramIntegrationTracking> programIntegrationTrackingList = programIntegrationTrackingRepository.findAll();
        assertThat(programIntegrationTrackingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramIntegrationTracking() throws Exception {
        // Initialize the database
        programIntegrationTrackingRepository.saveAndFlush(programIntegrationTracking);

        int databaseSizeBeforeDelete = programIntegrationTrackingRepository.findAll().size();

        // Delete the programIntegrationTracking
        restProgramIntegrationTrackingMockMvc.perform(delete("/api/program-integration-trackings/{id}", programIntegrationTracking.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramIntegrationTracking> programIntegrationTrackingList = programIntegrationTrackingRepository.findAll();
        assertThat(programIntegrationTrackingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramIntegrationTracking.class);
        ProgramIntegrationTracking programIntegrationTracking1 = new ProgramIntegrationTracking();
        programIntegrationTracking1.setId(1L);
        ProgramIntegrationTracking programIntegrationTracking2 = new ProgramIntegrationTracking();
        programIntegrationTracking2.setId(programIntegrationTracking1.getId());
        assertThat(programIntegrationTracking1).isEqualTo(programIntegrationTracking2);
        programIntegrationTracking2.setId(2L);
        assertThat(programIntegrationTracking1).isNotEqualTo(programIntegrationTracking2);
        programIntegrationTracking1.setId(null);
        assertThat(programIntegrationTracking1).isNotEqualTo(programIntegrationTracking2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramIntegrationTrackingDTO.class);
        ProgramIntegrationTrackingDTO programIntegrationTrackingDTO1 = new ProgramIntegrationTrackingDTO();
        programIntegrationTrackingDTO1.setId(1L);
        ProgramIntegrationTrackingDTO programIntegrationTrackingDTO2 = new ProgramIntegrationTrackingDTO();
        assertThat(programIntegrationTrackingDTO1).isNotEqualTo(programIntegrationTrackingDTO2);
        programIntegrationTrackingDTO2.setId(programIntegrationTrackingDTO1.getId());
        assertThat(programIntegrationTrackingDTO1).isEqualTo(programIntegrationTrackingDTO2);
        programIntegrationTrackingDTO2.setId(2L);
        assertThat(programIntegrationTrackingDTO1).isNotEqualTo(programIntegrationTrackingDTO2);
        programIntegrationTrackingDTO1.setId(null);
        assertThat(programIntegrationTrackingDTO1).isNotEqualTo(programIntegrationTrackingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programIntegrationTrackingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programIntegrationTrackingMapper.fromId(null)).isNull();
    }
}
