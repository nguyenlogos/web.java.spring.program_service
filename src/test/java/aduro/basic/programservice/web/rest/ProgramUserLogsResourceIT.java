package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramUserLogs;
import aduro.basic.programservice.repository.ProgramUserLogsRepository;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramUserLogsResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramUserLogsResourceIT {

    private static final String DEFAULT_PARTICIPANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANT_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramUserLogsRepository programUserLogsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramUserLogsMockMvc;

    private ProgramUserLogs programUserLogs;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramUserLogsResource programUserLogsResource = new ProgramUserLogsResource(programUserLogsRepository);
        this.restProgramUserLogsMockMvc = MockMvcBuilders.standaloneSetup(programUserLogsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUserLogs createEntity(EntityManager em) {
        ProgramUserLogs programUserLogs = new ProgramUserLogs()
            .participantId(DEFAULT_PARTICIPANT_ID)
            .programId(DEFAULT_PROGRAM_ID)
            .code(DEFAULT_CODE)
            .message(DEFAULT_MESSAGE)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        return programUserLogs;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUserLogs createUpdatedEntity(EntityManager em) {
        ProgramUserLogs programUserLogs = new ProgramUserLogs()
            .participantId(UPDATED_PARTICIPANT_ID)
            .programId(UPDATED_PROGRAM_ID)
            .code(UPDATED_CODE)
            .message(UPDATED_MESSAGE)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        return programUserLogs;
    }

    @BeforeEach
    public void initTest() {
        programUserLogs = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramUserLogs() throws Exception {
        int databaseSizeBeforeCreate = programUserLogsRepository.findAll().size();

        // Create the ProgramUserLogs
        restProgramUserLogsMockMvc.perform(post("/api/program-user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLogs)))
            .andExpect(status().isCreated());

        // Validate the ProgramUserLogs in the database
        List<ProgramUserLogs> programUserLogsList = programUserLogsRepository.findAll();
        assertThat(programUserLogsList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramUserLogs testProgramUserLogs = programUserLogsList.get(programUserLogsList.size() - 1);
        assertThat(testProgramUserLogs.getParticipantId()).isEqualTo(DEFAULT_PARTICIPANT_ID);
        assertThat(testProgramUserLogs.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
        assertThat(testProgramUserLogs.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testProgramUserLogs.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testProgramUserLogs.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramUserLogs.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramUserLogsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programUserLogsRepository.findAll().size();

        // Create the ProgramUserLogs with an existing ID
        programUserLogs.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramUserLogsMockMvc.perform(post("/api/program-user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLogs)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUserLogs in the database
        List<ProgramUserLogs> programUserLogsList = programUserLogsRepository.findAll();
        assertThat(programUserLogsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkParticipantIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserLogsRepository.findAll().size();
        // set the field null
        programUserLogs.setParticipantId(null);

        // Create the ProgramUserLogs, which fails.

        restProgramUserLogsMockMvc.perform(post("/api/program-user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLogs)))
            .andExpect(status().isBadRequest());

        List<ProgramUserLogs> programUserLogsList = programUserLogsRepository.findAll();
        assertThat(programUserLogsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserLogsRepository.findAll().size();
        // set the field null
        programUserLogs.setProgramId(null);

        // Create the ProgramUserLogs, which fails.

        restProgramUserLogsMockMvc.perform(post("/api/program-user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLogs)))
            .andExpect(status().isBadRequest());

        List<ProgramUserLogs> programUserLogsList = programUserLogsRepository.findAll();
        assertThat(programUserLogsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramUserLogs() throws Exception {
        // Initialize the database
        programUserLogsRepository.saveAndFlush(programUserLogs);

        // Get all the programUserLogsList
        restProgramUserLogsMockMvc.perform(get("/api/program-user-logs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUserLogs.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID.toString())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramUserLogs() throws Exception {
        // Initialize the database
        programUserLogsRepository.saveAndFlush(programUserLogs);

        // Get the programUserLogs
        restProgramUserLogsMockMvc.perform(get("/api/program-user-logs/{id}", programUserLogs.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programUserLogs.getId().intValue()))
            .andExpect(jsonPath("$.participantId").value(DEFAULT_PARTICIPANT_ID.toString()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProgramUserLogs() throws Exception {
        // Get the programUserLogs
        restProgramUserLogsMockMvc.perform(get("/api/program-user-logs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramUserLogs() throws Exception {
        // Initialize the database
        programUserLogsRepository.saveAndFlush(programUserLogs);

        int databaseSizeBeforeUpdate = programUserLogsRepository.findAll().size();

        // Update the programUserLogs
        ProgramUserLogs updatedProgramUserLogs = programUserLogsRepository.findById(programUserLogs.getId()).get();
        // Disconnect from session so that the updates on updatedProgramUserLogs are not directly saved in db
        em.detach(updatedProgramUserLogs);
        updatedProgramUserLogs
            .participantId(UPDATED_PARTICIPANT_ID)
            .programId(UPDATED_PROGRAM_ID)
            .code(UPDATED_CODE)
            .message(UPDATED_MESSAGE)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);

        restProgramUserLogsMockMvc.perform(put("/api/program-user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProgramUserLogs)))
            .andExpect(status().isOk());

        // Validate the ProgramUserLogs in the database
        List<ProgramUserLogs> programUserLogsList = programUserLogsRepository.findAll();
        assertThat(programUserLogsList).hasSize(databaseSizeBeforeUpdate);
        ProgramUserLogs testProgramUserLogs = programUserLogsList.get(programUserLogsList.size() - 1);
        assertThat(testProgramUserLogs.getParticipantId()).isEqualTo(UPDATED_PARTICIPANT_ID);
        assertThat(testProgramUserLogs.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
        assertThat(testProgramUserLogs.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testProgramUserLogs.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testProgramUserLogs.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramUserLogs.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramUserLogs() throws Exception {
        int databaseSizeBeforeUpdate = programUserLogsRepository.findAll().size();

        // Create the ProgramUserLogs

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramUserLogsMockMvc.perform(put("/api/program-user-logs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLogs)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUserLogs in the database
        List<ProgramUserLogs> programUserLogsList = programUserLogsRepository.findAll();
        assertThat(programUserLogsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramUserLogs() throws Exception {
        // Initialize the database
        programUserLogsRepository.saveAndFlush(programUserLogs);

        int databaseSizeBeforeDelete = programUserLogsRepository.findAll().size();

        // Delete the programUserLogs
        restProgramUserLogsMockMvc.perform(delete("/api/program-user-logs/{id}", programUserLogs.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramUserLogs> programUserLogsList = programUserLogsRepository.findAll();
        assertThat(programUserLogsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserLogs.class);
        ProgramUserLogs programUserLogs1 = new ProgramUserLogs();
        programUserLogs1.setId(1L);
        ProgramUserLogs programUserLogs2 = new ProgramUserLogs();
        programUserLogs2.setId(programUserLogs1.getId());
        assertThat(programUserLogs1).isEqualTo(programUserLogs2);
        programUserLogs2.setId(2L);
        assertThat(programUserLogs1).isNotEqualTo(programUserLogs2);
        programUserLogs1.setId(null);
        assertThat(programUserLogs1).isNotEqualTo(programUserLogs2);
    }
}
