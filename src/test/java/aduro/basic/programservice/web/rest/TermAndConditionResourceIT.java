package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.TermAndCondition;
import aduro.basic.programservice.repository.TermAndConditionRepository;
import aduro.basic.programservice.service.TermAndConditionService;
import aduro.basic.programservice.service.dto.TermAndConditionDTO;
import aduro.basic.programservice.service.mapper.TermAndConditionMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.TermAndConditionCriteria;
import aduro.basic.programservice.service.TermAndConditionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link TermAndConditionResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class TermAndConditionResourceIT {

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_TARGET_SUBGROUP = false;
    private static final Boolean UPDATED_IS_TARGET_SUBGROUP = true;

    private static final String DEFAULT_SUBGROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_NAME = "BBBBBBBBBB";

    @Autowired
    private TermAndConditionRepository termAndConditionRepository;

    @Autowired
    private TermAndConditionMapper termAndConditionMapper;

    @Autowired
    private TermAndConditionService termAndConditionService;

    @Autowired
    private TermAndConditionQueryService termAndConditionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTermAndConditionMockMvc;

    private TermAndCondition termAndCondition;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TermAndConditionResource termAndConditionResource = new TermAndConditionResource(termAndConditionService, termAndConditionQueryService);
        this.restTermAndConditionMockMvc = MockMvcBuilders.standaloneSetup(termAndConditionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TermAndCondition createEntity(EntityManager em) {
        TermAndCondition termAndCondition = new TermAndCondition()
            .clientId(DEFAULT_CLIENT_ID)
            .subgroupId(DEFAULT_SUBGROUP_ID)
            .title(DEFAULT_TITLE)
            .content(DEFAULT_CONTENT)
            .isTargetSubgroup(DEFAULT_IS_TARGET_SUBGROUP)
            .subgroupName(DEFAULT_SUBGROUP_NAME);
        return termAndCondition;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TermAndCondition createUpdatedEntity(EntityManager em) {
        TermAndCondition termAndCondition = new TermAndCondition()
            .clientId(UPDATED_CLIENT_ID)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .title(UPDATED_TITLE)
            .content(UPDATED_CONTENT)
            .isTargetSubgroup(UPDATED_IS_TARGET_SUBGROUP)
            .subgroupName(UPDATED_SUBGROUP_NAME);
        return termAndCondition;
    }

    @BeforeEach
    public void initTest() {
        termAndCondition = createEntity(em);
    }

    @Test
    @Transactional
    public void createTermAndCondition() throws Exception {
        int databaseSizeBeforeCreate = termAndConditionRepository.findAll().size();

        // Create the TermAndCondition
        TermAndConditionDTO termAndConditionDTO = termAndConditionMapper.toDto(termAndCondition);
        restTermAndConditionMockMvc.perform(post("/api/term-and-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termAndConditionDTO)))
            .andExpect(status().isCreated());

        // Validate the TermAndCondition in the database
        List<TermAndCondition> termAndConditionList = termAndConditionRepository.findAll();
        assertThat(termAndConditionList).hasSize(databaseSizeBeforeCreate + 1);
        TermAndCondition testTermAndCondition = termAndConditionList.get(termAndConditionList.size() - 1);
        assertThat(testTermAndCondition.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testTermAndCondition.getSubgroupId()).isEqualTo(DEFAULT_SUBGROUP_ID);
        assertThat(testTermAndCondition.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testTermAndCondition.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testTermAndCondition.isIsTargetSubgroup()).isEqualTo(DEFAULT_IS_TARGET_SUBGROUP);
        assertThat(testTermAndCondition.getSubgroupName()).isEqualTo(DEFAULT_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void createTermAndConditionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = termAndConditionRepository.findAll().size();

        // Create the TermAndCondition with an existing ID
        termAndCondition.setId(1L);
        TermAndConditionDTO termAndConditionDTO = termAndConditionMapper.toDto(termAndCondition);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTermAndConditionMockMvc.perform(post("/api/term-and-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termAndConditionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TermAndCondition in the database
        List<TermAndCondition> termAndConditionList = termAndConditionRepository.findAll();
        assertThat(termAndConditionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = termAndConditionRepository.findAll().size();
        // set the field null
        termAndCondition.setClientId(null);

        // Create the TermAndCondition, which fails.
        TermAndConditionDTO termAndConditionDTO = termAndConditionMapper.toDto(termAndCondition);

        restTermAndConditionMockMvc.perform(post("/api/term-and-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termAndConditionDTO)))
            .andExpect(status().isBadRequest());

        List<TermAndCondition> termAndConditionList = termAndConditionRepository.findAll();
        assertThat(termAndConditionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTermAndConditions() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList
        restTermAndConditionMockMvc.perform(get("/api/term-and-conditions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(termAndCondition.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].isTargetSubgroup").value(hasItem(DEFAULT_IS_TARGET_SUBGROUP.booleanValue())))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getTermAndCondition() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get the termAndCondition
        restTermAndConditionMockMvc.perform(get("/api/term-and-conditions/{id}", termAndCondition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(termAndCondition.getId().intValue()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.subgroupId").value(DEFAULT_SUBGROUP_ID.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.isTargetSubgroup").value(DEFAULT_IS_TARGET_SUBGROUP.booleanValue()))
            .andExpect(jsonPath("$.subgroupName").value(DEFAULT_SUBGROUP_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where clientId equals to DEFAULT_CLIENT_ID
        defaultTermAndConditionShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the termAndConditionList where clientId equals to UPDATED_CLIENT_ID
        defaultTermAndConditionShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultTermAndConditionShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the termAndConditionList where clientId equals to UPDATED_CLIENT_ID
        defaultTermAndConditionShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where clientId is not null
        defaultTermAndConditionShouldBeFound("clientId.specified=true");

        // Get all the termAndConditionList where clientId is null
        defaultTermAndConditionShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsBySubgroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where subgroupId equals to DEFAULT_SUBGROUP_ID
        defaultTermAndConditionShouldBeFound("subgroupId.equals=" + DEFAULT_SUBGROUP_ID);

        // Get all the termAndConditionList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultTermAndConditionShouldNotBeFound("subgroupId.equals=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsBySubgroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where subgroupId in DEFAULT_SUBGROUP_ID or UPDATED_SUBGROUP_ID
        defaultTermAndConditionShouldBeFound("subgroupId.in=" + DEFAULT_SUBGROUP_ID + "," + UPDATED_SUBGROUP_ID);

        // Get all the termAndConditionList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultTermAndConditionShouldNotBeFound("subgroupId.in=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsBySubgroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where subgroupId is not null
        defaultTermAndConditionShouldBeFound("subgroupId.specified=true");

        // Get all the termAndConditionList where subgroupId is null
        defaultTermAndConditionShouldNotBeFound("subgroupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where title equals to DEFAULT_TITLE
        defaultTermAndConditionShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the termAndConditionList where title equals to UPDATED_TITLE
        defaultTermAndConditionShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultTermAndConditionShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the termAndConditionList where title equals to UPDATED_TITLE
        defaultTermAndConditionShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where title is not null
        defaultTermAndConditionShouldBeFound("title.specified=true");

        // Get all the termAndConditionList where title is null
        defaultTermAndConditionShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByContentIsEqualToSomething() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where content equals to DEFAULT_CONTENT
        defaultTermAndConditionShouldBeFound("content.equals=" + DEFAULT_CONTENT);

        // Get all the termAndConditionList where content equals to UPDATED_CONTENT
        defaultTermAndConditionShouldNotBeFound("content.equals=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByContentIsInShouldWork() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where content in DEFAULT_CONTENT or UPDATED_CONTENT
        defaultTermAndConditionShouldBeFound("content.in=" + DEFAULT_CONTENT + "," + UPDATED_CONTENT);

        // Get all the termAndConditionList where content equals to UPDATED_CONTENT
        defaultTermAndConditionShouldNotBeFound("content.in=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByContentIsNullOrNotNull() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where content is not null
        defaultTermAndConditionShouldBeFound("content.specified=true");

        // Get all the termAndConditionList where content is null
        defaultTermAndConditionShouldNotBeFound("content.specified=false");
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByIsTargetSubgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where isTargetSubgroup equals to DEFAULT_IS_TARGET_SUBGROUP
        defaultTermAndConditionShouldBeFound("isTargetSubgroup.equals=" + DEFAULT_IS_TARGET_SUBGROUP);

        // Get all the termAndConditionList where isTargetSubgroup equals to UPDATED_IS_TARGET_SUBGROUP
        defaultTermAndConditionShouldNotBeFound("isTargetSubgroup.equals=" + UPDATED_IS_TARGET_SUBGROUP);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByIsTargetSubgroupIsInShouldWork() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where isTargetSubgroup in DEFAULT_IS_TARGET_SUBGROUP or UPDATED_IS_TARGET_SUBGROUP
        defaultTermAndConditionShouldBeFound("isTargetSubgroup.in=" + DEFAULT_IS_TARGET_SUBGROUP + "," + UPDATED_IS_TARGET_SUBGROUP);

        // Get all the termAndConditionList where isTargetSubgroup equals to UPDATED_IS_TARGET_SUBGROUP
        defaultTermAndConditionShouldNotBeFound("isTargetSubgroup.in=" + UPDATED_IS_TARGET_SUBGROUP);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsByIsTargetSubgroupIsNullOrNotNull() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where isTargetSubgroup is not null
        defaultTermAndConditionShouldBeFound("isTargetSubgroup.specified=true");

        // Get all the termAndConditionList where isTargetSubgroup is null
        defaultTermAndConditionShouldNotBeFound("isTargetSubgroup.specified=false");
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsBySubgroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where subgroupName equals to DEFAULT_SUBGROUP_NAME
        defaultTermAndConditionShouldBeFound("subgroupName.equals=" + DEFAULT_SUBGROUP_NAME);

        // Get all the termAndConditionList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultTermAndConditionShouldNotBeFound("subgroupName.equals=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsBySubgroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where subgroupName in DEFAULT_SUBGROUP_NAME or UPDATED_SUBGROUP_NAME
        defaultTermAndConditionShouldBeFound("subgroupName.in=" + DEFAULT_SUBGROUP_NAME + "," + UPDATED_SUBGROUP_NAME);

        // Get all the termAndConditionList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultTermAndConditionShouldNotBeFound("subgroupName.in=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllTermAndConditionsBySubgroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        // Get all the termAndConditionList where subgroupName is not null
        defaultTermAndConditionShouldBeFound("subgroupName.specified=true");

        // Get all the termAndConditionList where subgroupName is null
        defaultTermAndConditionShouldNotBeFound("subgroupName.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTermAndConditionShouldBeFound(String filter) throws Exception {
        restTermAndConditionMockMvc.perform(get("/api/term-and-conditions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(termAndCondition.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID)))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT)))
            .andExpect(jsonPath("$.[*].isTargetSubgroup").value(hasItem(DEFAULT_IS_TARGET_SUBGROUP.booleanValue())))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME)));

        // Check, that the count call also returns 1
        restTermAndConditionMockMvc.perform(get("/api/term-and-conditions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTermAndConditionShouldNotBeFound(String filter) throws Exception {
        restTermAndConditionMockMvc.perform(get("/api/term-and-conditions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTermAndConditionMockMvc.perform(get("/api/term-and-conditions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTermAndCondition() throws Exception {
        // Get the termAndCondition
        restTermAndConditionMockMvc.perform(get("/api/term-and-conditions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTermAndCondition() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        int databaseSizeBeforeUpdate = termAndConditionRepository.findAll().size();

        // Update the termAndCondition
        TermAndCondition updatedTermAndCondition = termAndConditionRepository.findById(termAndCondition.getId()).get();
        // Disconnect from session so that the updates on updatedTermAndCondition are not directly saved in db
        em.detach(updatedTermAndCondition);
        updatedTermAndCondition
            .clientId(UPDATED_CLIENT_ID)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .title(UPDATED_TITLE)
            .content(UPDATED_CONTENT)
            .isTargetSubgroup(UPDATED_IS_TARGET_SUBGROUP)
            .subgroupName(UPDATED_SUBGROUP_NAME);
        TermAndConditionDTO termAndConditionDTO = termAndConditionMapper.toDto(updatedTermAndCondition);

        restTermAndConditionMockMvc.perform(put("/api/term-and-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termAndConditionDTO)))
            .andExpect(status().isOk());

        // Validate the TermAndCondition in the database
        List<TermAndCondition> termAndConditionList = termAndConditionRepository.findAll();
        assertThat(termAndConditionList).hasSize(databaseSizeBeforeUpdate);
        TermAndCondition testTermAndCondition = termAndConditionList.get(termAndConditionList.size() - 1);
        assertThat(testTermAndCondition.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testTermAndCondition.getSubgroupId()).isEqualTo(UPDATED_SUBGROUP_ID);
        assertThat(testTermAndCondition.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testTermAndCondition.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testTermAndCondition.isIsTargetSubgroup()).isEqualTo(UPDATED_IS_TARGET_SUBGROUP);
        assertThat(testTermAndCondition.getSubgroupName()).isEqualTo(UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingTermAndCondition() throws Exception {
        int databaseSizeBeforeUpdate = termAndConditionRepository.findAll().size();

        // Create the TermAndCondition
        TermAndConditionDTO termAndConditionDTO = termAndConditionMapper.toDto(termAndCondition);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTermAndConditionMockMvc.perform(put("/api/term-and-conditions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(termAndConditionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the TermAndCondition in the database
        List<TermAndCondition> termAndConditionList = termAndConditionRepository.findAll();
        assertThat(termAndConditionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTermAndCondition() throws Exception {
        // Initialize the database
        termAndConditionRepository.saveAndFlush(termAndCondition);

        int databaseSizeBeforeDelete = termAndConditionRepository.findAll().size();

        // Delete the termAndCondition
        restTermAndConditionMockMvc.perform(delete("/api/term-and-conditions/{id}", termAndCondition.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<TermAndCondition> termAndConditionList = termAndConditionRepository.findAll();
        assertThat(termAndConditionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TermAndCondition.class);
        TermAndCondition termAndCondition1 = new TermAndCondition();
        termAndCondition1.setId(1L);
        TermAndCondition termAndCondition2 = new TermAndCondition();
        termAndCondition2.setId(termAndCondition1.getId());
        assertThat(termAndCondition1).isEqualTo(termAndCondition2);
        termAndCondition2.setId(2L);
        assertThat(termAndCondition1).isNotEqualTo(termAndCondition2);
        termAndCondition1.setId(null);
        assertThat(termAndCondition1).isNotEqualTo(termAndCondition2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TermAndConditionDTO.class);
        TermAndConditionDTO termAndConditionDTO1 = new TermAndConditionDTO();
        termAndConditionDTO1.setId(1L);
        TermAndConditionDTO termAndConditionDTO2 = new TermAndConditionDTO();
        assertThat(termAndConditionDTO1).isNotEqualTo(termAndConditionDTO2);
        termAndConditionDTO2.setId(termAndConditionDTO1.getId());
        assertThat(termAndConditionDTO1).isEqualTo(termAndConditionDTO2);
        termAndConditionDTO2.setId(2L);
        assertThat(termAndConditionDTO1).isNotEqualTo(termAndConditionDTO2);
        termAndConditionDTO1.setId(null);
        assertThat(termAndConditionDTO1).isNotEqualTo(termAndConditionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(termAndConditionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(termAndConditionMapper.fromId(null)).isNull();
    }
}
