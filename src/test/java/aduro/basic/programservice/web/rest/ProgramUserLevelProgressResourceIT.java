package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramUserLevelProgress;
import aduro.basic.programservice.repository.ProgramUserLevelProgressRepository;
import aduro.basic.programservice.service.ProgramUserLevelProgressService;
import aduro.basic.programservice.service.dto.ProgramUserLevelProgressDTO;
import aduro.basic.programservice.service.mapper.ProgramUserLevelProgressMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramUserLevelProgressCriteria;
import aduro.basic.programservice.service.ProgramUserLevelProgressQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramUserLevelProgressResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramUserLevelProgressResourceIT {

    private static final Long DEFAULT_USER_EVENT_ID = 1L;
    private static final Long UPDATED_USER_EVENT_ID = 2L;

    private static final Long DEFAULT_PROGRAM_USER_ID = 1L;
    private static final Long UPDATED_PROGRAM_USER_ID = 2L;

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LEVEL_UP_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LEVEL_UP_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_LEVEL_FROM = 1;
    private static final Integer UPDATED_LEVEL_FROM = 2;

    private static final Integer DEFAULT_LEVEL_TO = 1;
    private static final Integer UPDATED_LEVEL_TO = 2;

    private static final Boolean DEFAULT_HAS_REWARD = false;
    private static final Boolean UPDATED_HAS_REWARD = true;

    @Autowired
    private ProgramUserLevelProgressRepository programUserLevelProgressRepository;

    @Autowired
    private ProgramUserLevelProgressMapper programUserLevelProgressMapper;

    @Autowired
    private ProgramUserLevelProgressService programUserLevelProgressService;

    @Autowired
    private ProgramUserLevelProgressQueryService programUserLevelProgressQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramUserLevelProgressMockMvc;

    private ProgramUserLevelProgress programUserLevelProgress;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramUserLevelProgressResource programUserLevelProgressResource = new ProgramUserLevelProgressResource(programUserLevelProgressService, programUserLevelProgressQueryService);
        this.restProgramUserLevelProgressMockMvc = MockMvcBuilders.standaloneSetup(programUserLevelProgressResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUserLevelProgress createEntity(EntityManager em) {
        ProgramUserLevelProgress programUserLevelProgress = new ProgramUserLevelProgress()
            .userEventId(DEFAULT_USER_EVENT_ID)
            .programUserId(DEFAULT_PROGRAM_USER_ID)
            .createdAt(DEFAULT_CREATED_AT)
            .levelUpAt(DEFAULT_LEVEL_UP_AT)
            .levelFrom(DEFAULT_LEVEL_FROM)
            .levelTo(DEFAULT_LEVEL_TO)
            .hasReward(DEFAULT_HAS_REWARD);
        return programUserLevelProgress;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUserLevelProgress createUpdatedEntity(EntityManager em) {
        ProgramUserLevelProgress programUserLevelProgress = new ProgramUserLevelProgress()
            .userEventId(UPDATED_USER_EVENT_ID)
            .programUserId(UPDATED_PROGRAM_USER_ID)
            .createdAt(UPDATED_CREATED_AT)
            .levelUpAt(UPDATED_LEVEL_UP_AT)
            .levelFrom(UPDATED_LEVEL_FROM)
            .levelTo(UPDATED_LEVEL_TO)
            .hasReward(UPDATED_HAS_REWARD);
        return programUserLevelProgress;
    }

    @BeforeEach
    public void initTest() {
        programUserLevelProgress = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramUserLevelProgress() throws Exception {
        int databaseSizeBeforeCreate = programUserLevelProgressRepository.findAll().size();

        // Create the ProgramUserLevelProgress
        ProgramUserLevelProgressDTO programUserLevelProgressDTO = programUserLevelProgressMapper.toDto(programUserLevelProgress);
        restProgramUserLevelProgressMockMvc.perform(post("/api/program-user-level-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLevelProgressDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramUserLevelProgress in the database
        List<ProgramUserLevelProgress> programUserLevelProgressList = programUserLevelProgressRepository.findAll();
        assertThat(programUserLevelProgressList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramUserLevelProgress testProgramUserLevelProgress = programUserLevelProgressList.get(programUserLevelProgressList.size() - 1);
        assertThat(testProgramUserLevelProgress.getUserEventId()).isEqualTo(DEFAULT_USER_EVENT_ID);
        assertThat(testProgramUserLevelProgress.getProgramUserId()).isEqualTo(DEFAULT_PROGRAM_USER_ID);
        assertThat(testProgramUserLevelProgress.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testProgramUserLevelProgress.getLevelUpAt()).isEqualTo(DEFAULT_LEVEL_UP_AT);
        assertThat(testProgramUserLevelProgress.getLevelFrom()).isEqualTo(DEFAULT_LEVEL_FROM);
        assertThat(testProgramUserLevelProgress.getLevelTo()).isEqualTo(DEFAULT_LEVEL_TO);
        assertThat(testProgramUserLevelProgress.isHasReward()).isEqualTo(DEFAULT_HAS_REWARD);
    }

    @Test
    @Transactional
    public void createProgramUserLevelProgressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programUserLevelProgressRepository.findAll().size();

        // Create the ProgramUserLevelProgress with an existing ID
        programUserLevelProgress.setId(1L);
        ProgramUserLevelProgressDTO programUserLevelProgressDTO = programUserLevelProgressMapper.toDto(programUserLevelProgress);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramUserLevelProgressMockMvc.perform(post("/api/program-user-level-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLevelProgressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUserLevelProgress in the database
        List<ProgramUserLevelProgress> programUserLevelProgressList = programUserLevelProgressRepository.findAll();
        assertThat(programUserLevelProgressList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUserEventIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserLevelProgressRepository.findAll().size();
        // set the field null
        programUserLevelProgress.setUserEventId(null);

        // Create the ProgramUserLevelProgress, which fails.
        ProgramUserLevelProgressDTO programUserLevelProgressDTO = programUserLevelProgressMapper.toDto(programUserLevelProgress);

        restProgramUserLevelProgressMockMvc.perform(post("/api/program-user-level-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLevelProgressDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserLevelProgress> programUserLevelProgressList = programUserLevelProgressRepository.findAll();
        assertThat(programUserLevelProgressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserLevelProgressRepository.findAll().size();
        // set the field null
        programUserLevelProgress.setProgramUserId(null);

        // Create the ProgramUserLevelProgress, which fails.
        ProgramUserLevelProgressDTO programUserLevelProgressDTO = programUserLevelProgressMapper.toDto(programUserLevelProgress);

        restProgramUserLevelProgressMockMvc.perform(post("/api/program-user-level-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLevelProgressDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserLevelProgress> programUserLevelProgressList = programUserLevelProgressRepository.findAll();
        assertThat(programUserLevelProgressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLevelUpAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserLevelProgressRepository.findAll().size();
        // set the field null
        programUserLevelProgress.setLevelUpAt(null);

        // Create the ProgramUserLevelProgress, which fails.
        ProgramUserLevelProgressDTO programUserLevelProgressDTO = programUserLevelProgressMapper.toDto(programUserLevelProgress);

        restProgramUserLevelProgressMockMvc.perform(post("/api/program-user-level-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLevelProgressDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserLevelProgress> programUserLevelProgressList = programUserLevelProgressRepository.findAll();
        assertThat(programUserLevelProgressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgresses() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList
        restProgramUserLevelProgressMockMvc.perform(get("/api/program-user-level-progresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUserLevelProgress.getId().intValue())))
            .andExpect(jsonPath("$.[*].userEventId").value(hasItem(DEFAULT_USER_EVENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].programUserId").value(hasItem(DEFAULT_PROGRAM_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].levelUpAt").value(hasItem(DEFAULT_LEVEL_UP_AT.toString())))
            .andExpect(jsonPath("$.[*].levelFrom").value(hasItem(DEFAULT_LEVEL_FROM)))
            .andExpect(jsonPath("$.[*].levelTo").value(hasItem(DEFAULT_LEVEL_TO)))
            .andExpect(jsonPath("$.[*].hasReward").value(hasItem(DEFAULT_HAS_REWARD.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getProgramUserLevelProgress() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get the programUserLevelProgress
        restProgramUserLevelProgressMockMvc.perform(get("/api/program-user-level-progresses/{id}", programUserLevelProgress.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programUserLevelProgress.getId().intValue()))
            .andExpect(jsonPath("$.userEventId").value(DEFAULT_USER_EVENT_ID.intValue()))
            .andExpect(jsonPath("$.programUserId").value(DEFAULT_PROGRAM_USER_ID.intValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.levelUpAt").value(DEFAULT_LEVEL_UP_AT.toString()))
            .andExpect(jsonPath("$.levelFrom").value(DEFAULT_LEVEL_FROM))
            .andExpect(jsonPath("$.levelTo").value(DEFAULT_LEVEL_TO))
            .andExpect(jsonPath("$.hasReward").value(DEFAULT_HAS_REWARD.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByUserEventIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where userEventId equals to DEFAULT_USER_EVENT_ID
        defaultProgramUserLevelProgressShouldBeFound("userEventId.equals=" + DEFAULT_USER_EVENT_ID);

        // Get all the programUserLevelProgressList where userEventId equals to UPDATED_USER_EVENT_ID
        defaultProgramUserLevelProgressShouldNotBeFound("userEventId.equals=" + UPDATED_USER_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByUserEventIdIsInShouldWork() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where userEventId in DEFAULT_USER_EVENT_ID or UPDATED_USER_EVENT_ID
        defaultProgramUserLevelProgressShouldBeFound("userEventId.in=" + DEFAULT_USER_EVENT_ID + "," + UPDATED_USER_EVENT_ID);

        // Get all the programUserLevelProgressList where userEventId equals to UPDATED_USER_EVENT_ID
        defaultProgramUserLevelProgressShouldNotBeFound("userEventId.in=" + UPDATED_USER_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByUserEventIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where userEventId is not null
        defaultProgramUserLevelProgressShouldBeFound("userEventId.specified=true");

        // Get all the programUserLevelProgressList where userEventId is null
        defaultProgramUserLevelProgressShouldNotBeFound("userEventId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByUserEventIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where userEventId greater than or equals to DEFAULT_USER_EVENT_ID
        defaultProgramUserLevelProgressShouldBeFound("userEventId.greaterOrEqualThan=" + DEFAULT_USER_EVENT_ID);

        // Get all the programUserLevelProgressList where userEventId greater than or equals to UPDATED_USER_EVENT_ID
        defaultProgramUserLevelProgressShouldNotBeFound("userEventId.greaterOrEqualThan=" + UPDATED_USER_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByUserEventIdIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where userEventId less than or equals to DEFAULT_USER_EVENT_ID
        defaultProgramUserLevelProgressShouldNotBeFound("userEventId.lessThan=" + DEFAULT_USER_EVENT_ID);

        // Get all the programUserLevelProgressList where userEventId less than or equals to UPDATED_USER_EVENT_ID
        defaultProgramUserLevelProgressShouldBeFound("userEventId.lessThan=" + UPDATED_USER_EVENT_ID);
    }


    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByProgramUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where programUserId equals to DEFAULT_PROGRAM_USER_ID
        defaultProgramUserLevelProgressShouldBeFound("programUserId.equals=" + DEFAULT_PROGRAM_USER_ID);

        // Get all the programUserLevelProgressList where programUserId equals to UPDATED_PROGRAM_USER_ID
        defaultProgramUserLevelProgressShouldNotBeFound("programUserId.equals=" + UPDATED_PROGRAM_USER_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByProgramUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where programUserId in DEFAULT_PROGRAM_USER_ID or UPDATED_PROGRAM_USER_ID
        defaultProgramUserLevelProgressShouldBeFound("programUserId.in=" + DEFAULT_PROGRAM_USER_ID + "," + UPDATED_PROGRAM_USER_ID);

        // Get all the programUserLevelProgressList where programUserId equals to UPDATED_PROGRAM_USER_ID
        defaultProgramUserLevelProgressShouldNotBeFound("programUserId.in=" + UPDATED_PROGRAM_USER_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByProgramUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where programUserId is not null
        defaultProgramUserLevelProgressShouldBeFound("programUserId.specified=true");

        // Get all the programUserLevelProgressList where programUserId is null
        defaultProgramUserLevelProgressShouldNotBeFound("programUserId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByProgramUserIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where programUserId greater than or equals to DEFAULT_PROGRAM_USER_ID
        defaultProgramUserLevelProgressShouldBeFound("programUserId.greaterOrEqualThan=" + DEFAULT_PROGRAM_USER_ID);

        // Get all the programUserLevelProgressList where programUserId greater than or equals to UPDATED_PROGRAM_USER_ID
        defaultProgramUserLevelProgressShouldNotBeFound("programUserId.greaterOrEqualThan=" + UPDATED_PROGRAM_USER_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByProgramUserIdIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where programUserId less than or equals to DEFAULT_PROGRAM_USER_ID
        defaultProgramUserLevelProgressShouldNotBeFound("programUserId.lessThan=" + DEFAULT_PROGRAM_USER_ID);

        // Get all the programUserLevelProgressList where programUserId less than or equals to UPDATED_PROGRAM_USER_ID
        defaultProgramUserLevelProgressShouldBeFound("programUserId.lessThan=" + UPDATED_PROGRAM_USER_ID);
    }


    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where createdAt equals to DEFAULT_CREATED_AT
        defaultProgramUserLevelProgressShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the programUserLevelProgressList where createdAt equals to UPDATED_CREATED_AT
        defaultProgramUserLevelProgressShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultProgramUserLevelProgressShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the programUserLevelProgressList where createdAt equals to UPDATED_CREATED_AT
        defaultProgramUserLevelProgressShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where createdAt is not null
        defaultProgramUserLevelProgressShouldBeFound("createdAt.specified=true");

        // Get all the programUserLevelProgressList where createdAt is null
        defaultProgramUserLevelProgressShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelUpAtIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelUpAt equals to DEFAULT_LEVEL_UP_AT
        defaultProgramUserLevelProgressShouldBeFound("levelUpAt.equals=" + DEFAULT_LEVEL_UP_AT);

        // Get all the programUserLevelProgressList where levelUpAt equals to UPDATED_LEVEL_UP_AT
        defaultProgramUserLevelProgressShouldNotBeFound("levelUpAt.equals=" + UPDATED_LEVEL_UP_AT);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelUpAtIsInShouldWork() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelUpAt in DEFAULT_LEVEL_UP_AT or UPDATED_LEVEL_UP_AT
        defaultProgramUserLevelProgressShouldBeFound("levelUpAt.in=" + DEFAULT_LEVEL_UP_AT + "," + UPDATED_LEVEL_UP_AT);

        // Get all the programUserLevelProgressList where levelUpAt equals to UPDATED_LEVEL_UP_AT
        defaultProgramUserLevelProgressShouldNotBeFound("levelUpAt.in=" + UPDATED_LEVEL_UP_AT);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelUpAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelUpAt is not null
        defaultProgramUserLevelProgressShouldBeFound("levelUpAt.specified=true");

        // Get all the programUserLevelProgressList where levelUpAt is null
        defaultProgramUserLevelProgressShouldNotBeFound("levelUpAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelFromIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelFrom equals to DEFAULT_LEVEL_FROM
        defaultProgramUserLevelProgressShouldBeFound("levelFrom.equals=" + DEFAULT_LEVEL_FROM);

        // Get all the programUserLevelProgressList where levelFrom equals to UPDATED_LEVEL_FROM
        defaultProgramUserLevelProgressShouldNotBeFound("levelFrom.equals=" + UPDATED_LEVEL_FROM);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelFromIsInShouldWork() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelFrom in DEFAULT_LEVEL_FROM or UPDATED_LEVEL_FROM
        defaultProgramUserLevelProgressShouldBeFound("levelFrom.in=" + DEFAULT_LEVEL_FROM + "," + UPDATED_LEVEL_FROM);

        // Get all the programUserLevelProgressList where levelFrom equals to UPDATED_LEVEL_FROM
        defaultProgramUserLevelProgressShouldNotBeFound("levelFrom.in=" + UPDATED_LEVEL_FROM);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelFrom is not null
        defaultProgramUserLevelProgressShouldBeFound("levelFrom.specified=true");

        // Get all the programUserLevelProgressList where levelFrom is null
        defaultProgramUserLevelProgressShouldNotBeFound("levelFrom.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelFromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelFrom greater than or equals to DEFAULT_LEVEL_FROM
        defaultProgramUserLevelProgressShouldBeFound("levelFrom.greaterOrEqualThan=" + DEFAULT_LEVEL_FROM);

        // Get all the programUserLevelProgressList where levelFrom greater than or equals to UPDATED_LEVEL_FROM
        defaultProgramUserLevelProgressShouldNotBeFound("levelFrom.greaterOrEqualThan=" + UPDATED_LEVEL_FROM);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelFromIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelFrom less than or equals to DEFAULT_LEVEL_FROM
        defaultProgramUserLevelProgressShouldNotBeFound("levelFrom.lessThan=" + DEFAULT_LEVEL_FROM);

        // Get all the programUserLevelProgressList where levelFrom less than or equals to UPDATED_LEVEL_FROM
        defaultProgramUserLevelProgressShouldBeFound("levelFrom.lessThan=" + UPDATED_LEVEL_FROM);
    }


    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelToIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelTo equals to DEFAULT_LEVEL_TO
        defaultProgramUserLevelProgressShouldBeFound("levelTo.equals=" + DEFAULT_LEVEL_TO);

        // Get all the programUserLevelProgressList where levelTo equals to UPDATED_LEVEL_TO
        defaultProgramUserLevelProgressShouldNotBeFound("levelTo.equals=" + UPDATED_LEVEL_TO);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelToIsInShouldWork() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelTo in DEFAULT_LEVEL_TO or UPDATED_LEVEL_TO
        defaultProgramUserLevelProgressShouldBeFound("levelTo.in=" + DEFAULT_LEVEL_TO + "," + UPDATED_LEVEL_TO);

        // Get all the programUserLevelProgressList where levelTo equals to UPDATED_LEVEL_TO
        defaultProgramUserLevelProgressShouldNotBeFound("levelTo.in=" + UPDATED_LEVEL_TO);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelToIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelTo is not null
        defaultProgramUserLevelProgressShouldBeFound("levelTo.specified=true");

        // Get all the programUserLevelProgressList where levelTo is null
        defaultProgramUserLevelProgressShouldNotBeFound("levelTo.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelToIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelTo greater than or equals to DEFAULT_LEVEL_TO
        defaultProgramUserLevelProgressShouldBeFound("levelTo.greaterOrEqualThan=" + DEFAULT_LEVEL_TO);

        // Get all the programUserLevelProgressList where levelTo greater than or equals to UPDATED_LEVEL_TO
        defaultProgramUserLevelProgressShouldNotBeFound("levelTo.greaterOrEqualThan=" + UPDATED_LEVEL_TO);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByLevelToIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where levelTo less than or equals to DEFAULT_LEVEL_TO
        defaultProgramUserLevelProgressShouldNotBeFound("levelTo.lessThan=" + DEFAULT_LEVEL_TO);

        // Get all the programUserLevelProgressList where levelTo less than or equals to UPDATED_LEVEL_TO
        defaultProgramUserLevelProgressShouldBeFound("levelTo.lessThan=" + UPDATED_LEVEL_TO);
    }


    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByHasRewardIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where hasReward equals to DEFAULT_HAS_REWARD
        defaultProgramUserLevelProgressShouldBeFound("hasReward.equals=" + DEFAULT_HAS_REWARD);

        // Get all the programUserLevelProgressList where hasReward equals to UPDATED_HAS_REWARD
        defaultProgramUserLevelProgressShouldNotBeFound("hasReward.equals=" + UPDATED_HAS_REWARD);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByHasRewardIsInShouldWork() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where hasReward in DEFAULT_HAS_REWARD or UPDATED_HAS_REWARD
        defaultProgramUserLevelProgressShouldBeFound("hasReward.in=" + DEFAULT_HAS_REWARD + "," + UPDATED_HAS_REWARD);

        // Get all the programUserLevelProgressList where hasReward equals to UPDATED_HAS_REWARD
        defaultProgramUserLevelProgressShouldNotBeFound("hasReward.in=" + UPDATED_HAS_REWARD);
    }

    @Test
    @Transactional
    public void getAllProgramUserLevelProgressesByHasRewardIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        // Get all the programUserLevelProgressList where hasReward is not null
        defaultProgramUserLevelProgressShouldBeFound("hasReward.specified=true");

        // Get all the programUserLevelProgressList where hasReward is null
        defaultProgramUserLevelProgressShouldNotBeFound("hasReward.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramUserLevelProgressShouldBeFound(String filter) throws Exception {
        restProgramUserLevelProgressMockMvc.perform(get("/api/program-user-level-progresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUserLevelProgress.getId().intValue())))
            .andExpect(jsonPath("$.[*].userEventId").value(hasItem(DEFAULT_USER_EVENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].programUserId").value(hasItem(DEFAULT_PROGRAM_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].levelUpAt").value(hasItem(DEFAULT_LEVEL_UP_AT.toString())))
            .andExpect(jsonPath("$.[*].levelFrom").value(hasItem(DEFAULT_LEVEL_FROM)))
            .andExpect(jsonPath("$.[*].levelTo").value(hasItem(DEFAULT_LEVEL_TO)))
            .andExpect(jsonPath("$.[*].hasReward").value(hasItem(DEFAULT_HAS_REWARD.booleanValue())));

        // Check, that the count call also returns 1
        restProgramUserLevelProgressMockMvc.perform(get("/api/program-user-level-progresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramUserLevelProgressShouldNotBeFound(String filter) throws Exception {
        restProgramUserLevelProgressMockMvc.perform(get("/api/program-user-level-progresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramUserLevelProgressMockMvc.perform(get("/api/program-user-level-progresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramUserLevelProgress() throws Exception {
        // Get the programUserLevelProgress
        restProgramUserLevelProgressMockMvc.perform(get("/api/program-user-level-progresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramUserLevelProgress() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        int databaseSizeBeforeUpdate = programUserLevelProgressRepository.findAll().size();

        // Update the programUserLevelProgress
        ProgramUserLevelProgress updatedProgramUserLevelProgress = programUserLevelProgressRepository.findById(programUserLevelProgress.getId()).get();
        // Disconnect from session so that the updates on updatedProgramUserLevelProgress are not directly saved in db
        em.detach(updatedProgramUserLevelProgress);
        updatedProgramUserLevelProgress
            .userEventId(UPDATED_USER_EVENT_ID)
            .programUserId(UPDATED_PROGRAM_USER_ID)
            .createdAt(UPDATED_CREATED_AT)
            .levelUpAt(UPDATED_LEVEL_UP_AT)
            .levelFrom(UPDATED_LEVEL_FROM)
            .levelTo(UPDATED_LEVEL_TO)
            .hasReward(UPDATED_HAS_REWARD);
        ProgramUserLevelProgressDTO programUserLevelProgressDTO = programUserLevelProgressMapper.toDto(updatedProgramUserLevelProgress);

        restProgramUserLevelProgressMockMvc.perform(put("/api/program-user-level-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLevelProgressDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramUserLevelProgress in the database
        List<ProgramUserLevelProgress> programUserLevelProgressList = programUserLevelProgressRepository.findAll();
        assertThat(programUserLevelProgressList).hasSize(databaseSizeBeforeUpdate);
        ProgramUserLevelProgress testProgramUserLevelProgress = programUserLevelProgressList.get(programUserLevelProgressList.size() - 1);
        assertThat(testProgramUserLevelProgress.getUserEventId()).isEqualTo(UPDATED_USER_EVENT_ID);
        assertThat(testProgramUserLevelProgress.getProgramUserId()).isEqualTo(UPDATED_PROGRAM_USER_ID);
        assertThat(testProgramUserLevelProgress.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testProgramUserLevelProgress.getLevelUpAt()).isEqualTo(UPDATED_LEVEL_UP_AT);
        assertThat(testProgramUserLevelProgress.getLevelFrom()).isEqualTo(UPDATED_LEVEL_FROM);
        assertThat(testProgramUserLevelProgress.getLevelTo()).isEqualTo(UPDATED_LEVEL_TO);
        assertThat(testProgramUserLevelProgress.isHasReward()).isEqualTo(UPDATED_HAS_REWARD);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramUserLevelProgress() throws Exception {
        int databaseSizeBeforeUpdate = programUserLevelProgressRepository.findAll().size();

        // Create the ProgramUserLevelProgress
        ProgramUserLevelProgressDTO programUserLevelProgressDTO = programUserLevelProgressMapper.toDto(programUserLevelProgress);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramUserLevelProgressMockMvc.perform(put("/api/program-user-level-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserLevelProgressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUserLevelProgress in the database
        List<ProgramUserLevelProgress> programUserLevelProgressList = programUserLevelProgressRepository.findAll();
        assertThat(programUserLevelProgressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramUserLevelProgress() throws Exception {
        // Initialize the database
        programUserLevelProgressRepository.saveAndFlush(programUserLevelProgress);

        int databaseSizeBeforeDelete = programUserLevelProgressRepository.findAll().size();

        // Delete the programUserLevelProgress
        restProgramUserLevelProgressMockMvc.perform(delete("/api/program-user-level-progresses/{id}", programUserLevelProgress.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramUserLevelProgress> programUserLevelProgressList = programUserLevelProgressRepository.findAll();
        assertThat(programUserLevelProgressList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserLevelProgress.class);
        ProgramUserLevelProgress programUserLevelProgress1 = new ProgramUserLevelProgress();
        programUserLevelProgress1.setId(1L);
        ProgramUserLevelProgress programUserLevelProgress2 = new ProgramUserLevelProgress();
        programUserLevelProgress2.setId(programUserLevelProgress1.getId());
        assertThat(programUserLevelProgress1).isEqualTo(programUserLevelProgress2);
        programUserLevelProgress2.setId(2L);
        assertThat(programUserLevelProgress1).isNotEqualTo(programUserLevelProgress2);
        programUserLevelProgress1.setId(null);
        assertThat(programUserLevelProgress1).isNotEqualTo(programUserLevelProgress2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserLevelProgressDTO.class);
        ProgramUserLevelProgressDTO programUserLevelProgressDTO1 = new ProgramUserLevelProgressDTO();
        programUserLevelProgressDTO1.setId(1L);
        ProgramUserLevelProgressDTO programUserLevelProgressDTO2 = new ProgramUserLevelProgressDTO();
        assertThat(programUserLevelProgressDTO1).isNotEqualTo(programUserLevelProgressDTO2);
        programUserLevelProgressDTO2.setId(programUserLevelProgressDTO1.getId());
        assertThat(programUserLevelProgressDTO1).isEqualTo(programUserLevelProgressDTO2);
        programUserLevelProgressDTO2.setId(2L);
        assertThat(programUserLevelProgressDTO1).isNotEqualTo(programUserLevelProgressDTO2);
        programUserLevelProgressDTO1.setId(null);
        assertThat(programUserLevelProgressDTO1).isNotEqualTo(programUserLevelProgressDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programUserLevelProgressMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programUserLevelProgressMapper.fromId(null)).isNull();
    }
}
