package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ProgramRepository;
import aduro.basic.programservice.service.ProgramQueryService;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.service.dto.ProgramTemplateDTO;
import aduro.basic.programservice.service.mapper.ProgramMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramTemplateResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_SENT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_SENT = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_IS_RETRIGGER_EMAIL = false;
    private static final Boolean UPDATED_IS_RETRIGGER_EMAIL = true;

    private static final Boolean DEFAULT_IS_ELIGIBLE = false;
    private static final Boolean UPDATED_IS_ELIGIBLE = true;

    private static final Boolean DEFAULT_IS_SENT_REGISTRATION_EMAIL = false;
    private static final Boolean UPDATED_IS_SENT_REGISTRATION_EMAIL = true;

    private static final Boolean DEFAULT_IS_REGISTERED_FOR_PLATFORM = false;
    private static final Boolean UPDATED_IS_REGISTERED_FOR_PLATFORM = true;

    private static final Boolean DEFAULT_IS_SCHEDULED_SCREENING = false;
    private static final Boolean UPDATED_IS_SCHEDULED_SCREENING = true;

    private static final Boolean DEFAULT_IS_FUNCTIONALLY = false;
    private static final Boolean UPDATED_IS_FUNCTIONALLY = true;

    private static final String DEFAULT_LOGO_URL = "AAAAAAAAAA";
    private static final String UPDATED_LOGO_URL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_USER_POINT = new BigDecimal(1);
    private static final BigDecimal UPDATED_USER_POINT = new BigDecimal(2);

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "Draft";
    private static final String UPDATED_STATUS = "Active";

    private static final Boolean DEFAULT_IS_USE_POINT = false;
    private static final Boolean UPDATED_IS_USE_POINT = true;

    private static final Boolean DEFAULT_IS_SCREEN = false;
    private static final Boolean UPDATED_IS_SCREEN = true;

    private static final Boolean DEFAULT_IS_COACHING = false;
    private static final Boolean UPDATED_IS_COACHING = true;

    private static final Boolean DEFAULT_IS_WELL_MATRIC = false;
    private static final Boolean UPDATED_IS_WELL_MATRIC = true;

    private static final Boolean DEFAULT_IS_HP = false;
    private static final Boolean UPDATED_IS_HP = true;

    private static final Boolean DEFAULT_IS_USE_LEVEL = false;
    private static final Boolean UPDATED_IS_USE_LEVEL = true;

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_RESET_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_RESET_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private ProgramMapper programMapper;

    @Autowired
    private ProgramService programService;

    @Autowired
    private ProgramQueryService programQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramMockMvc;

    private Program program;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramTemplateResource programTemplateResource = new ProgramTemplateResource(programService, programQueryService);
        this.restProgramMockMvc = MockMvcBuilders.standaloneSetup(programTemplateResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Program createEntity(EntityManager em) {
        Program program = new Program()
            .name(DEFAULT_NAME)
            .lastSent(DEFAULT_LAST_SENT)
            .isRetriggerEmail(DEFAULT_IS_RETRIGGER_EMAIL)
            .isEligible(DEFAULT_IS_ELIGIBLE)
            .isSentRegistrationEmail(DEFAULT_IS_SENT_REGISTRATION_EMAIL)
            .isRegisteredForPlatform(DEFAULT_IS_REGISTERED_FOR_PLATFORM)
            .isScheduledScreening(DEFAULT_IS_SCHEDULED_SCREENING)
            .isFunctionally(DEFAULT_IS_FUNCTIONALLY)
            .logoUrl(DEFAULT_LOGO_URL)
            .description(DEFAULT_DESCRIPTION)
            .userPoint(DEFAULT_USER_POINT)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .createdBy(DEFAULT_CREATED_BY)
            .status(DEFAULT_STATUS)
            .isUsePoint(DEFAULT_IS_USE_POINT)
            .isScreen(DEFAULT_IS_SCREEN)
            .isCoaching(DEFAULT_IS_COACHING)
            .isWellMatric(DEFAULT_IS_WELL_MATRIC)
            .isHp(DEFAULT_IS_HP)
            .isUseLevel(DEFAULT_IS_USE_LEVEL)
            .startDate(DEFAULT_START_DATE)
            .resetDate(DEFAULT_RESET_DATE)
            .isTemplate(false);
        return program;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Program createUpdatedEntity(EntityManager em) {
        Program program = new Program()
            .name(UPDATED_NAME)
            .lastSent(UPDATED_LAST_SENT)
            .isRetriggerEmail(UPDATED_IS_RETRIGGER_EMAIL)
            .isEligible(UPDATED_IS_ELIGIBLE)
            .isSentRegistrationEmail(UPDATED_IS_SENT_REGISTRATION_EMAIL)
            .isRegisteredForPlatform(UPDATED_IS_REGISTERED_FOR_PLATFORM)
            .isScheduledScreening(UPDATED_IS_SCHEDULED_SCREENING)
            .isFunctionally(UPDATED_IS_FUNCTIONALLY)
            .logoUrl(UPDATED_LOGO_URL)
            .description(UPDATED_DESCRIPTION)
            .userPoint(UPDATED_USER_POINT)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .createdBy(UPDATED_CREATED_BY)
            .status(UPDATED_STATUS)
            .isUsePoint(UPDATED_IS_USE_POINT)
            .isScreen(UPDATED_IS_SCREEN)
            .isCoaching(UPDATED_IS_COACHING)
            .isWellMatric(UPDATED_IS_WELL_MATRIC)
            .isHp(UPDATED_IS_HP)
            .isUseLevel(UPDATED_IS_USE_LEVEL)
            .startDate(UPDATED_START_DATE)
            .resetDate(UPDATED_RESET_DATE);
        return program;
    }

    @BeforeEach
    public void initTest() {
        program = createEntity(em);


    }

    @Test
    @Transactional
    public void createTemplate() throws Exception {

        Program template  = createEntity(em);
        Program program  = createEntity(em);


        template.setName("name1");
        template.setIsTemplate(true);

        template = programRepository.save(template);

        program.setName("name2");
        program.setIsTemplate(false);
        program = programRepository.save(program);

        List<Program> programList =  programRepository.findAll();

        String url1 = format("/api/program-templates/%s/export-template", program.getId());

        String url2 = format("/api/program-templates/%s/create-program", template.getId());


        ProgramTemplateDTO templateDTO = new ProgramTemplateDTO();
        templateDTO.setClientId("client2");
        templateDTO.setClientName("client2");
        templateDTO.setCreatedBy("abc");
        templateDTO.setName("xxxx");
        templateDTO.setStartDate(Instant.now());
        templateDTO.setResetDate(Instant.now());

        restProgramMockMvc.perform(post(url2)
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateDTO)));
        //    .andExpect(status().isCreated());

        templateDTO.setName("yyyyy");
        restProgramMockMvc.perform(post(url1)
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(templateDTO)));
          //  .andExpect(status().isCreated());



    }








}
