package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramActivity;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramPhase;
import aduro.basic.programservice.repository.ProgramActivityRepository;
import aduro.basic.programservice.service.ProgramActivityService;
import aduro.basic.programservice.service.dto.ProgramActivityDTO;
import aduro.basic.programservice.service.mapper.ProgramActivityMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramActivityCriteria;
import aduro.basic.programservice.service.ProgramActivityQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramActivityResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramActivityResourceIT {

    private static final String DEFAULT_ACTIVITY_ID = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVITY_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIVITY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVITY_CODE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_CUSTOMIZED = false;
    private static final Boolean UPDATED_IS_CUSTOMIZED = true;


    @Autowired
    private ProgramActivityRepository programActivityRepository;

    @Autowired
    private ProgramActivityMapper programActivityMapper;

    @Autowired
    private ProgramActivityService programActivityService;

    @Autowired
    private ProgramActivityQueryService programActivityQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramActivityMockMvc;

    private ProgramActivity programActivity;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramActivityResource programActivityResource = new ProgramActivityResource(programActivityService, programActivityQueryService);
        this.restProgramActivityMockMvc = MockMvcBuilders.standaloneSetup(programActivityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramActivity createEntity(EntityManager em) {
        ProgramActivity programActivity = new ProgramActivity()
            .activityId(DEFAULT_ACTIVITY_ID)
            .activityCode(DEFAULT_ACTIVITY_CODE);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }

        ProgramPhase programPhase;
        if (TestUtil.findAll(em, ProgramPhase.class).isEmpty()) {
            programPhase = ProgramPhaseResourceIT.createEntity(em);
            em.persist(programPhase);
            em.flush();
        } else {
            programPhase = TestUtil.findAll(em, ProgramPhase.class).get(0);
        }

        programActivity.setProgramPhase(programPhase);

        programActivity.setProgramPhaseId(programPhase.getId());
        programActivity.setProgram(program);

        return programActivity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramActivity createUpdatedEntity(EntityManager em) {
        ProgramActivity programActivity = new ProgramActivity()
            .activityId(UPDATED_ACTIVITY_ID)
            .activityCode(UPDATED_ACTIVITY_CODE);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createUpdatedEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        ProgramPhase programPhase;
        if (TestUtil.findAll(em, ProgramPhase.class).isEmpty()) {
            programPhase = ProgramPhaseResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            programPhase = TestUtil.findAll(em, ProgramPhase.class).get(0);
        }

        programActivity.setProgramPhase(programPhase);
        programActivity.setProgram(program);
        programActivity.setProgramPhaseId(programPhase.getId());
        return programActivity;
    }

    @BeforeEach
    public void initTest() {
        programActivity = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramActivity() throws Exception {
        int databaseSizeBeforeCreate = programActivityRepository.findAll().size();

        // Create the ProgramActivity
        ProgramActivityDTO programActivityDTO = programActivityMapper.toDto(programActivity);
        restProgramActivityMockMvc.perform(post("/api/program-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programActivityDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramActivity in the database
        List<ProgramActivity> programActivityList = programActivityRepository.findAll();
        assertThat(programActivityList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramActivity testProgramActivity = programActivityList.get(programActivityList.size() - 1);
        assertThat(testProgramActivity.getActivityId()).isEqualTo(DEFAULT_ACTIVITY_ID);
        assertThat(testProgramActivity.getActivityCode()).isEqualTo(DEFAULT_ACTIVITY_CODE);
    }

    @Test
    @Transactional
    public void createProgramActivityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programActivityRepository.findAll().size();

        // Create the ProgramActivity with an existing ID
        programActivity.setId(1L);
        ProgramActivityDTO programActivityDTO = programActivityMapper.toDto(programActivity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramActivityMockMvc.perform(post("/api/program-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programActivityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramActivity in the database
        List<ProgramActivity> programActivityList = programActivityRepository.findAll();
        assertThat(programActivityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkActivity_idIsRequired() throws Exception {
        int databaseSizeBeforeTest = programActivityRepository.findAll().size();
        // set the field null
        programActivity.setActivityId(null);

        // Create the ProgramActivity, which fails.
        ProgramActivityDTO programActivityDTO = programActivityMapper.toDto(programActivity);

        restProgramActivityMockMvc.perform(post("/api/program-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programActivityDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramActivity> programActivityList = programActivityRepository.findAll();
        assertThat(programActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActivityCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programActivityRepository.findAll().size();
        // set the field null
        programActivity.setActivityCode(null);

        // Create the ProgramActivity, which fails.
        ProgramActivityDTO programActivityDTO = programActivityMapper.toDto(programActivity);

        restProgramActivityMockMvc.perform(post("/api/program-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programActivityDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramActivity> programActivityList = programActivityRepository.findAll();
        assertThat(programActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramActivities() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList
        restProgramActivityMockMvc.perform(get("/api/program-activities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].activityId").value(hasItem(DEFAULT_ACTIVITY_ID.toString())))
            .andExpect(jsonPath("$.[*].activityCode").value(hasItem(DEFAULT_ACTIVITY_CODE.toString())));
    }

    @Test
    @Transactional
    public void getProgramActivity() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get the programActivity
        restProgramActivityMockMvc.perform(get("/api/program-activities/{id}", programActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programActivity.getId().intValue()))
            .andExpect(jsonPath("$.activityId").value(DEFAULT_ACTIVITY_ID.toString()))
            .andExpect(jsonPath("$.activityCode").value(DEFAULT_ACTIVITY_CODE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByActivity_idIsEqualToSomething() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList where activityId equals to DEFAULT_ACTIVITY_ID
        defaultProgramActivityShouldBeFound("activityId.equals=" + DEFAULT_ACTIVITY_ID);

        // Get all the programActivityList where activityId equals to UPDATED_ACTIVITY_ID
        defaultProgramActivityShouldNotBeFound("activityId.equals=" + UPDATED_ACTIVITY_ID);
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByActivity_idIsInShouldWork() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList where activity_id in DEFAULT_ACTIVITY_ID or UPDATED_ACTIVITY_ID
        defaultProgramActivityShouldBeFound("activityId.in=" + DEFAULT_ACTIVITY_ID + "," + UPDATED_ACTIVITY_ID);

        // Get all the programActivityList where activity_id equals to UPDATED_ACTIVITY_ID
        defaultProgramActivityShouldNotBeFound("activityId.in=" + UPDATED_ACTIVITY_ID);
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByActivity_idIsNullOrNotNull() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList where activity_id is not null
        defaultProgramActivityShouldBeFound("activityId.specified=true");

        // Get all the programActivityList where activityId is null
        defaultProgramActivityShouldNotBeFound("activityId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByActivityCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList where activityCode equals to DEFAULT_ACTIVITY_CODE
        defaultProgramActivityShouldBeFound("activityCode.equals=" + DEFAULT_ACTIVITY_CODE);

        // Get all the programActivityList where activityCode equals to UPDATED_ACTIVITY_CODE
        defaultProgramActivityShouldNotBeFound("activityCode.equals=" + UPDATED_ACTIVITY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByActivityCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList where activityCode in DEFAULT_ACTIVITY_CODE or UPDATED_ACTIVITY_CODE
        defaultProgramActivityShouldBeFound("activityCode.in=" + DEFAULT_ACTIVITY_CODE + "," + UPDATED_ACTIVITY_CODE);

        // Get all the programActivityList where activityCode equals to UPDATED_ACTIVITY_CODE
        defaultProgramActivityShouldNotBeFound("activityCode.in=" + UPDATED_ACTIVITY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByActivityCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList where activityCode is not null
        defaultProgramActivityShouldBeFound("activityCode.specified=true");

        // Get all the programActivityList where activityCode is null
        defaultProgramActivityShouldNotBeFound("activityCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByProgramIsEqualToSomething() throws Exception {
        // Get already existing entity
        Program program = programActivity.getProgram();
        programActivityRepository.saveAndFlush(programActivity);
        Long programId = program.getId();

        // Get all the programActivityList where program equals to programId
        defaultProgramActivityShouldBeFound("programId.equals=" + programId);

        // Get all the programActivityList where program equals to programId + 1
        defaultProgramActivityShouldNotBeFound("programId.equals=" + (programId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramActivityShouldBeFound(String filter) throws Exception {
        restProgramActivityMockMvc.perform(get("/api/program-activities?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].activityId").value(hasItem(DEFAULT_ACTIVITY_ID)))
            .andExpect(jsonPath("$.[*].activityCode").value(hasItem(DEFAULT_ACTIVITY_CODE)));

        // Check, that the count call also returns 1
        restProgramActivityMockMvc.perform(get("/api/program-activities/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramActivityShouldNotBeFound(String filter) throws Exception {
        restProgramActivityMockMvc.perform(get("/api/program-activities?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramActivityMockMvc.perform(get("/api/program-activities/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramActivity() throws Exception {
        // Get the programActivity
        restProgramActivityMockMvc.perform(get("/api/program-activities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramActivity() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        int databaseSizeBeforeUpdate = programActivityRepository.findAll().size();

        // Update the programActivity
        ProgramActivity updatedProgramActivity = programActivityRepository.findById(programActivity.getId()).get();
        // Disconnect from session so that the updates on updatedProgramActivity are not directly saved in db
        em.detach(updatedProgramActivity);
        updatedProgramActivity
            .activityId(UPDATED_ACTIVITY_ID)
            .activityCode(UPDATED_ACTIVITY_CODE);
        ProgramActivityDTO programActivityDTO = programActivityMapper.toDto(updatedProgramActivity);

        restProgramActivityMockMvc.perform(put("/api/program-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programActivityDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramActivity in the database
        List<ProgramActivity> programActivityList = programActivityRepository.findAll();
        assertThat(programActivityList).hasSize(databaseSizeBeforeUpdate);
        ProgramActivity testProgramActivity = programActivityList.get(programActivityList.size() - 1);
        assertThat(testProgramActivity.getActivityId()).isEqualTo(UPDATED_ACTIVITY_ID);
        assertThat(testProgramActivity.getActivityCode()).isEqualTo(UPDATED_ACTIVITY_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramActivity() throws Exception {
        int databaseSizeBeforeUpdate = programActivityRepository.findAll().size();

        // Create the ProgramActivity
        ProgramActivityDTO programActivityDTO = programActivityMapper.toDto(programActivity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramActivityMockMvc.perform(put("/api/program-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programActivityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramActivity in the database
        List<ProgramActivity> programActivityList = programActivityRepository.findAll();
        assertThat(programActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramActivity() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        int databaseSizeBeforeDelete = programActivityRepository.findAll().size();

        // Delete the programActivity
        restProgramActivityMockMvc.perform(delete("/api/program-activities/{id}", programActivity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramActivity> programActivityList = programActivityRepository.findAll();
        assertThat(programActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramActivity.class);
        ProgramActivity programActivity1 = new ProgramActivity();
        programActivity1.setId(1L);
        ProgramActivity programActivity2 = new ProgramActivity();
        programActivity2.setId(programActivity1.getId());
        assertThat(programActivity1).isEqualTo(programActivity2);
        programActivity2.setId(2L);
        assertThat(programActivity1).isNotEqualTo(programActivity2);
        programActivity1.setId(null);
        assertThat(programActivity1).isNotEqualTo(programActivity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramActivityDTO.class);
        ProgramActivityDTO programActivityDTO1 = new ProgramActivityDTO();
        programActivityDTO1.setId(1L);
        ProgramActivityDTO programActivityDTO2 = new ProgramActivityDTO();
        assertThat(programActivityDTO1).isNotEqualTo(programActivityDTO2);
        programActivityDTO2.setId(programActivityDTO1.getId());
        assertThat(programActivityDTO1).isEqualTo(programActivityDTO2);
        programActivityDTO2.setId(2L);
        assertThat(programActivityDTO1).isNotEqualTo(programActivityDTO2);
        programActivityDTO1.setId(null);
        assertThat(programActivityDTO1).isNotEqualTo(programActivityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programActivityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programActivityMapper.fromId(null)).isNull();
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByProgramPhaseIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramPhase programPhase = programActivity.getProgramPhase();
        programActivityRepository.saveAndFlush(programActivity);
        Long programPhaseId = programPhase.getId();

        // Get all the programActivityList where programPhase equals to programPhaseId
        defaultProgramActivityShouldBeFound("programPhaseId.equals=" + programPhaseId);

        // Get all the programActivityList where programPhase equals to programPhaseId + 1
        defaultProgramActivityShouldNotBeFound("programPhaseId.equals=" + (programPhaseId + 1));
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByIsCustomizedIsNullOrNotNull() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList where isCustomized is not null
        defaultProgramActivityShouldBeFound("isCustomized.specified=true");

        // Get all the programActivityList where isCustomized is null
        defaultProgramActivityShouldNotBeFound("isCustomized.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramActivitiesByIsCustomizedIsEqualToSomething() throws Exception {
        // Initialize the database
        programActivityRepository.saveAndFlush(programActivity);

        // Get all the programActivityList where isCustomized equals to DEFAULT_IS_CUSTOMIZED
        defaultProgramActivityShouldBeFound("isCustomized.equals=" + DEFAULT_IS_CUSTOMIZED);

        // Get all the programActivityList where isCustomized equals to UPDATED_IS_CUSTOMIZED
        defaultProgramActivityShouldNotBeFound("isCustomized.equals=" + UPDATED_IS_CUSTOMIZED);
    }





    @Test
    @Transactional
    public void createBulkProgramActivity() throws Exception {
        int databaseSizeBeforeCreate = programActivityRepository.findAll().size();

        // Create the ProgramActivity
        ProgramActivityDTO programActivityDTO = programActivityMapper.toDto(programActivity);

        List<ProgramActivityDTO> programActivityDTOList = new ArrayList<>();
        programActivityDTOList.add(programActivityDTO);

        restProgramActivityMockMvc.perform(post("/api/program-activities/1/bulk")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programActivityDTOList)))
            .andExpect(status().isCreated());

    }


    @Test
    @Transactional
    public void testDeleteBulkProgramActivities() throws Exception {
        ProgramActivityDTO programActivityDTO = programActivityMapper.toDto(programActivity);

        List<ProgramActivityDTO> programActivityDTOList = new ArrayList<>();
        programActivityDTOList.add(programActivityDTO);

        List<Long> Ids = new ArrayList<>();
        Ids.add(Long.valueOf(1));

        restProgramActivityMockMvc.perform(post("/api/program-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programActivityDTO)))
            .andExpect(status().isCreated());

        restProgramActivityMockMvc.perform(post("/program-activities/1/delete-bulk")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(Ids)));

    }
}
