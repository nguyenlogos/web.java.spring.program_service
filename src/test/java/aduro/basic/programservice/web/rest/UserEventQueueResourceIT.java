package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.UserEventQueue;
import aduro.basic.programservice.repository.UserEventQueueRepository;
import aduro.basic.programservice.service.UserEventQueueService;
import aduro.basic.programservice.service.dto.UserEventQueueDTO;
import aduro.basic.programservice.service.mapper.UserEventQueueMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.UserEventQueueCriteria;
import aduro.basic.programservice.service.UserEventQueueQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link UserEventQueueResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class UserEventQueueResourceIT {

    private static final String DEFAULT_EVENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_EVENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_EVENT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_EVENT_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final BigDecimal DEFAULT_EVENT_POINT = new BigDecimal(1);
    private static final BigDecimal UPDATED_EVENT_POINT = new BigDecimal(2);

    private static final String DEFAULT_EVENT_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_CATEGORY = "BBBBBBBBBB";

    private static final Long DEFAULT_PROGRAM_USER_ID = 1L;
    private static final Long UPDATED_PROGRAM_USER_ID = 2L;

    private static final String DEFAULT_EXECUTE_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_EXECUTE_STATUS = "BBBBBBBBBB";

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    private static final Instant DEFAULT_READ_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_READ_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ERROR_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENT_STEP = "AAAAAAAAAA";
    private static final String UPDATED_CURRENT_STEP = "BBBBBBBBBB";

    @Autowired
    private UserEventQueueRepository userEventQueueRepository;

    @Autowired
    private UserEventQueueMapper userEventQueueMapper;

    @Autowired
    private UserEventQueueService userEventQueueService;

    @Autowired
    private UserEventQueueQueryService userEventQueueQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserEventQueueMockMvc;

    private UserEventQueue userEventQueue;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserEventQueueResource userEventQueueResource = new UserEventQueueResource(userEventQueueService, userEventQueueQueryService);
        this.restUserEventQueueMockMvc = MockMvcBuilders.standaloneSetup(userEventQueueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEventQueue createEntity(EntityManager em) {
        UserEventQueue userEventQueue = new UserEventQueue()
            .eventCode(DEFAULT_EVENT_CODE)
            .eventId(DEFAULT_EVENT_ID)
            .eventDate(DEFAULT_EVENT_DATE)
            .eventPoint(DEFAULT_EVENT_POINT)
            .eventCategory(DEFAULT_EVENT_CATEGORY)
            .programUserId(DEFAULT_PROGRAM_USER_ID)
            .executeStatus(DEFAULT_EXECUTE_STATUS)
            .programId(DEFAULT_PROGRAM_ID)
            .readAt(DEFAULT_READ_AT)
            .errorMessage(DEFAULT_ERROR_MESSAGE)
            .currentStep(DEFAULT_CURRENT_STEP);
        return userEventQueue;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEventQueue createUpdatedEntity(EntityManager em) {
        UserEventQueue userEventQueue = new UserEventQueue()
            .eventCode(UPDATED_EVENT_CODE)
            .eventId(UPDATED_EVENT_ID)
            .eventDate(UPDATED_EVENT_DATE)
            .eventPoint(UPDATED_EVENT_POINT)
            .eventCategory(UPDATED_EVENT_CATEGORY)
            .programUserId(UPDATED_PROGRAM_USER_ID)
            .executeStatus(UPDATED_EXECUTE_STATUS)
            .programId(UPDATED_PROGRAM_ID)
            .readAt(UPDATED_READ_AT)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .currentStep(UPDATED_CURRENT_STEP);
        return userEventQueue;
    }

    @BeforeEach
    public void initTest() {
        userEventQueue = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserEventQueue() throws Exception {
        int databaseSizeBeforeCreate = userEventQueueRepository.findAll().size();

        // Create the UserEventQueue
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);
        restUserEventQueueMockMvc.perform(post("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isCreated());

        // Validate the UserEventQueue in the database
        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeCreate + 1);
        UserEventQueue testUserEventQueue = userEventQueueList.get(userEventQueueList.size() - 1);
        assertThat(testUserEventQueue.getEventCode()).isEqualTo(DEFAULT_EVENT_CODE);
        assertThat(testUserEventQueue.getEventId()).isEqualTo(DEFAULT_EVENT_ID);
        assertThat(testUserEventQueue.getEventDate()).isEqualTo(DEFAULT_EVENT_DATE);
        assertThat(testUserEventQueue.getEventPoint()).isEqualTo(DEFAULT_EVENT_POINT);
        assertThat(testUserEventQueue.getEventCategory()).isEqualTo(DEFAULT_EVENT_CATEGORY);
        assertThat(testUserEventQueue.getProgramUserId()).isEqualTo(DEFAULT_PROGRAM_USER_ID);
        assertThat(testUserEventQueue.getExecuteStatus()).isEqualTo(DEFAULT_EXECUTE_STATUS);
        assertThat(testUserEventQueue.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
        assertThat(testUserEventQueue.getReadAt()).isEqualTo(DEFAULT_READ_AT);
        assertThat(testUserEventQueue.getErrorMessage()).isEqualTo(DEFAULT_ERROR_MESSAGE);
        assertThat(testUserEventQueue.getCurrentStep()).isEqualTo(DEFAULT_CURRENT_STEP);
    }

    @Test
    @Transactional
    public void createUserEventQueueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userEventQueueRepository.findAll().size();

        // Create the UserEventQueue with an existing ID
        userEventQueue.setId(1L);
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserEventQueueMockMvc.perform(post("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserEventQueue in the database
        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEventCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventQueueRepository.findAll().size();
        // set the field null
        userEventQueue.setEventCode(null);

        // Create the UserEventQueue, which fails.
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);

        restUserEventQueueMockMvc.perform(post("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventQueueRepository.findAll().size();
        // set the field null
        userEventQueue.setEventId(null);

        // Create the UserEventQueue, which fails.
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);

        restUserEventQueueMockMvc.perform(post("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventQueueRepository.findAll().size();
        // set the field null
        userEventQueue.setEventDate(null);

        // Create the UserEventQueue, which fails.
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);

        restUserEventQueueMockMvc.perform(post("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventQueueRepository.findAll().size();
        // set the field null
        userEventQueue.setProgramUserId(null);

        // Create the UserEventQueue, which fails.
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);

        restUserEventQueueMockMvc.perform(post("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkExecuteStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventQueueRepository.findAll().size();
        // set the field null
        userEventQueue.setExecuteStatus(null);

        // Create the UserEventQueue, which fails.
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);

        restUserEventQueueMockMvc.perform(post("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventQueueRepository.findAll().size();
        // set the field null
        userEventQueue.setProgramId(null);

        // Create the UserEventQueue, which fails.
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);

        restUserEventQueueMockMvc.perform(post("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserEventQueues() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList
        restUserEventQueueMockMvc.perform(get("/api/user-event-queues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userEventQueue.getId().intValue())))
            .andExpect(jsonPath("$.[*].eventCode").value(hasItem(DEFAULT_EVENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID.toString())))
            .andExpect(jsonPath("$.[*].eventDate").value(hasItem(DEFAULT_EVENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].eventPoint").value(hasItem(DEFAULT_EVENT_POINT.intValue())))
            .andExpect(jsonPath("$.[*].eventCategory").value(hasItem(DEFAULT_EVENT_CATEGORY.toString())))
            .andExpect(jsonPath("$.[*].programUserId").value(hasItem(DEFAULT_PROGRAM_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].executeStatus").value(hasItem(DEFAULT_EXECUTE_STATUS.toString())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].readAt").value(hasItem(DEFAULT_READ_AT.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].currentStep").value(hasItem(DEFAULT_CURRENT_STEP.toString())));
    }
    
    @Test
    @Transactional
    public void getUserEventQueue() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get the userEventQueue
        restUserEventQueueMockMvc.perform(get("/api/user-event-queues/{id}", userEventQueue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userEventQueue.getId().intValue()))
            .andExpect(jsonPath("$.eventCode").value(DEFAULT_EVENT_CODE.toString()))
            .andExpect(jsonPath("$.eventId").value(DEFAULT_EVENT_ID.toString()))
            .andExpect(jsonPath("$.eventDate").value(DEFAULT_EVENT_DATE.toString()))
            .andExpect(jsonPath("$.eventPoint").value(DEFAULT_EVENT_POINT.intValue()))
            .andExpect(jsonPath("$.eventCategory").value(DEFAULT_EVENT_CATEGORY.toString()))
            .andExpect(jsonPath("$.programUserId").value(DEFAULT_PROGRAM_USER_ID.intValue()))
            .andExpect(jsonPath("$.executeStatus").value(DEFAULT_EXECUTE_STATUS.toString()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()))
            .andExpect(jsonPath("$.readAt").value(DEFAULT_READ_AT.toString()))
            .andExpect(jsonPath("$.errorMessage").value(DEFAULT_ERROR_MESSAGE.toString()))
            .andExpect(jsonPath("$.currentStep").value(DEFAULT_CURRENT_STEP.toString()));
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventCode equals to DEFAULT_EVENT_CODE
        defaultUserEventQueueShouldBeFound("eventCode.equals=" + DEFAULT_EVENT_CODE);

        // Get all the userEventQueueList where eventCode equals to UPDATED_EVENT_CODE
        defaultUserEventQueueShouldNotBeFound("eventCode.equals=" + UPDATED_EVENT_CODE);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventCodeIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventCode in DEFAULT_EVENT_CODE or UPDATED_EVENT_CODE
        defaultUserEventQueueShouldBeFound("eventCode.in=" + DEFAULT_EVENT_CODE + "," + UPDATED_EVENT_CODE);

        // Get all the userEventQueueList where eventCode equals to UPDATED_EVENT_CODE
        defaultUserEventQueueShouldNotBeFound("eventCode.in=" + UPDATED_EVENT_CODE);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventCode is not null
        defaultUserEventQueueShouldBeFound("eventCode.specified=true");

        // Get all the userEventQueueList where eventCode is null
        defaultUserEventQueueShouldNotBeFound("eventCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventId equals to DEFAULT_EVENT_ID
        defaultUserEventQueueShouldBeFound("eventId.equals=" + DEFAULT_EVENT_ID);

        // Get all the userEventQueueList where eventId equals to UPDATED_EVENT_ID
        defaultUserEventQueueShouldNotBeFound("eventId.equals=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventIdIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventId in DEFAULT_EVENT_ID or UPDATED_EVENT_ID
        defaultUserEventQueueShouldBeFound("eventId.in=" + DEFAULT_EVENT_ID + "," + UPDATED_EVENT_ID);

        // Get all the userEventQueueList where eventId equals to UPDATED_EVENT_ID
        defaultUserEventQueueShouldNotBeFound("eventId.in=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventId is not null
        defaultUserEventQueueShouldBeFound("eventId.specified=true");

        // Get all the userEventQueueList where eventId is null
        defaultUserEventQueueShouldNotBeFound("eventId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventDateIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventDate equals to DEFAULT_EVENT_DATE
        defaultUserEventQueueShouldBeFound("eventDate.equals=" + DEFAULT_EVENT_DATE);

        // Get all the userEventQueueList where eventDate equals to UPDATED_EVENT_DATE
        defaultUserEventQueueShouldNotBeFound("eventDate.equals=" + UPDATED_EVENT_DATE);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventDateIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventDate in DEFAULT_EVENT_DATE or UPDATED_EVENT_DATE
        defaultUserEventQueueShouldBeFound("eventDate.in=" + DEFAULT_EVENT_DATE + "," + UPDATED_EVENT_DATE);

        // Get all the userEventQueueList where eventDate equals to UPDATED_EVENT_DATE
        defaultUserEventQueueShouldNotBeFound("eventDate.in=" + UPDATED_EVENT_DATE);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventDate is not null
        defaultUserEventQueueShouldBeFound("eventDate.specified=true");

        // Get all the userEventQueueList where eventDate is null
        defaultUserEventQueueShouldNotBeFound("eventDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventPointIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventPoint equals to DEFAULT_EVENT_POINT
        defaultUserEventQueueShouldBeFound("eventPoint.equals=" + DEFAULT_EVENT_POINT);

        // Get all the userEventQueueList where eventPoint equals to UPDATED_EVENT_POINT
        defaultUserEventQueueShouldNotBeFound("eventPoint.equals=" + UPDATED_EVENT_POINT);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventPointIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventPoint in DEFAULT_EVENT_POINT or UPDATED_EVENT_POINT
        defaultUserEventQueueShouldBeFound("eventPoint.in=" + DEFAULT_EVENT_POINT + "," + UPDATED_EVENT_POINT);

        // Get all the userEventQueueList where eventPoint equals to UPDATED_EVENT_POINT
        defaultUserEventQueueShouldNotBeFound("eventPoint.in=" + UPDATED_EVENT_POINT);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventPoint is not null
        defaultUserEventQueueShouldBeFound("eventPoint.specified=true");

        // Get all the userEventQueueList where eventPoint is null
        defaultUserEventQueueShouldNotBeFound("eventPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventCategory equals to DEFAULT_EVENT_CATEGORY
        defaultUserEventQueueShouldBeFound("eventCategory.equals=" + DEFAULT_EVENT_CATEGORY);

        // Get all the userEventQueueList where eventCategory equals to UPDATED_EVENT_CATEGORY
        defaultUserEventQueueShouldNotBeFound("eventCategory.equals=" + UPDATED_EVENT_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventCategoryIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventCategory in DEFAULT_EVENT_CATEGORY or UPDATED_EVENT_CATEGORY
        defaultUserEventQueueShouldBeFound("eventCategory.in=" + DEFAULT_EVENT_CATEGORY + "," + UPDATED_EVENT_CATEGORY);

        // Get all the userEventQueueList where eventCategory equals to UPDATED_EVENT_CATEGORY
        defaultUserEventQueueShouldNotBeFound("eventCategory.in=" + UPDATED_EVENT_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByEventCategoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where eventCategory is not null
        defaultUserEventQueueShouldBeFound("eventCategory.specified=true");

        // Get all the userEventQueueList where eventCategory is null
        defaultUserEventQueueShouldNotBeFound("eventCategory.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programUserId equals to DEFAULT_PROGRAM_USER_ID
        defaultUserEventQueueShouldBeFound("programUserId.equals=" + DEFAULT_PROGRAM_USER_ID);

        // Get all the userEventQueueList where programUserId equals to UPDATED_PROGRAM_USER_ID
        defaultUserEventQueueShouldNotBeFound("programUserId.equals=" + UPDATED_PROGRAM_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programUserId in DEFAULT_PROGRAM_USER_ID or UPDATED_PROGRAM_USER_ID
        defaultUserEventQueueShouldBeFound("programUserId.in=" + DEFAULT_PROGRAM_USER_ID + "," + UPDATED_PROGRAM_USER_ID);

        // Get all the userEventQueueList where programUserId equals to UPDATED_PROGRAM_USER_ID
        defaultUserEventQueueShouldNotBeFound("programUserId.in=" + UPDATED_PROGRAM_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programUserId is not null
        defaultUserEventQueueShouldBeFound("programUserId.specified=true");

        // Get all the userEventQueueList where programUserId is null
        defaultUserEventQueueShouldNotBeFound("programUserId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramUserIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programUserId greater than or equals to DEFAULT_PROGRAM_USER_ID
        defaultUserEventQueueShouldBeFound("programUserId.greaterOrEqualThan=" + DEFAULT_PROGRAM_USER_ID);

        // Get all the userEventQueueList where programUserId greater than or equals to UPDATED_PROGRAM_USER_ID
        defaultUserEventQueueShouldNotBeFound("programUserId.greaterOrEqualThan=" + UPDATED_PROGRAM_USER_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramUserIdIsLessThanSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programUserId less than or equals to DEFAULT_PROGRAM_USER_ID
        defaultUserEventQueueShouldNotBeFound("programUserId.lessThan=" + DEFAULT_PROGRAM_USER_ID);

        // Get all the userEventQueueList where programUserId less than or equals to UPDATED_PROGRAM_USER_ID
        defaultUserEventQueueShouldBeFound("programUserId.lessThan=" + UPDATED_PROGRAM_USER_ID);
    }


    @Test
    @Transactional
    public void getAllUserEventQueuesByExecuteStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where executeStatus equals to DEFAULT_EXECUTE_STATUS
        defaultUserEventQueueShouldBeFound("executeStatus.equals=" + DEFAULT_EXECUTE_STATUS);

        // Get all the userEventQueueList where executeStatus equals to UPDATED_EXECUTE_STATUS
        defaultUserEventQueueShouldNotBeFound("executeStatus.equals=" + UPDATED_EXECUTE_STATUS);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByExecuteStatusIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where executeStatus in DEFAULT_EXECUTE_STATUS or UPDATED_EXECUTE_STATUS
        defaultUserEventQueueShouldBeFound("executeStatus.in=" + DEFAULT_EXECUTE_STATUS + "," + UPDATED_EXECUTE_STATUS);

        // Get all the userEventQueueList where executeStatus equals to UPDATED_EXECUTE_STATUS
        defaultUserEventQueueShouldNotBeFound("executeStatus.in=" + UPDATED_EXECUTE_STATUS);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByExecuteStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where executeStatus is not null
        defaultUserEventQueueShouldBeFound("executeStatus.specified=true");

        // Get all the userEventQueueList where executeStatus is null
        defaultUserEventQueueShouldNotBeFound("executeStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programId equals to DEFAULT_PROGRAM_ID
        defaultUserEventQueueShouldBeFound("programId.equals=" + DEFAULT_PROGRAM_ID);

        // Get all the userEventQueueList where programId equals to UPDATED_PROGRAM_ID
        defaultUserEventQueueShouldNotBeFound("programId.equals=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramIdIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programId in DEFAULT_PROGRAM_ID or UPDATED_PROGRAM_ID
        defaultUserEventQueueShouldBeFound("programId.in=" + DEFAULT_PROGRAM_ID + "," + UPDATED_PROGRAM_ID);

        // Get all the userEventQueueList where programId equals to UPDATED_PROGRAM_ID
        defaultUserEventQueueShouldNotBeFound("programId.in=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programId is not null
        defaultUserEventQueueShouldBeFound("programId.specified=true");

        // Get all the userEventQueueList where programId is null
        defaultUserEventQueueShouldNotBeFound("programId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programId greater than or equals to DEFAULT_PROGRAM_ID
        defaultUserEventQueueShouldBeFound("programId.greaterOrEqualThan=" + DEFAULT_PROGRAM_ID);

        // Get all the userEventQueueList where programId greater than or equals to UPDATED_PROGRAM_ID
        defaultUserEventQueueShouldNotBeFound("programId.greaterOrEqualThan=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByProgramIdIsLessThanSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where programId less than or equals to DEFAULT_PROGRAM_ID
        defaultUserEventQueueShouldNotBeFound("programId.lessThan=" + DEFAULT_PROGRAM_ID);

        // Get all the userEventQueueList where programId less than or equals to UPDATED_PROGRAM_ID
        defaultUserEventQueueShouldBeFound("programId.lessThan=" + UPDATED_PROGRAM_ID);
    }


    @Test
    @Transactional
    public void getAllUserEventQueuesByReadAtIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where readAt equals to DEFAULT_READ_AT
        defaultUserEventQueueShouldBeFound("readAt.equals=" + DEFAULT_READ_AT);

        // Get all the userEventQueueList where readAt equals to UPDATED_READ_AT
        defaultUserEventQueueShouldNotBeFound("readAt.equals=" + UPDATED_READ_AT);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByReadAtIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where readAt in DEFAULT_READ_AT or UPDATED_READ_AT
        defaultUserEventQueueShouldBeFound("readAt.in=" + DEFAULT_READ_AT + "," + UPDATED_READ_AT);

        // Get all the userEventQueueList where readAt equals to UPDATED_READ_AT
        defaultUserEventQueueShouldNotBeFound("readAt.in=" + UPDATED_READ_AT);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByReadAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where readAt is not null
        defaultUserEventQueueShouldBeFound("readAt.specified=true");

        // Get all the userEventQueueList where readAt is null
        defaultUserEventQueueShouldNotBeFound("readAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByErrorMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where errorMessage equals to DEFAULT_ERROR_MESSAGE
        defaultUserEventQueueShouldBeFound("errorMessage.equals=" + DEFAULT_ERROR_MESSAGE);

        // Get all the userEventQueueList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultUserEventQueueShouldNotBeFound("errorMessage.equals=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByErrorMessageIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where errorMessage in DEFAULT_ERROR_MESSAGE or UPDATED_ERROR_MESSAGE
        defaultUserEventQueueShouldBeFound("errorMessage.in=" + DEFAULT_ERROR_MESSAGE + "," + UPDATED_ERROR_MESSAGE);

        // Get all the userEventQueueList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultUserEventQueueShouldNotBeFound("errorMessage.in=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByErrorMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where errorMessage is not null
        defaultUserEventQueueShouldBeFound("errorMessage.specified=true");

        // Get all the userEventQueueList where errorMessage is null
        defaultUserEventQueueShouldNotBeFound("errorMessage.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByCurrentStepIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where currentStep equals to DEFAULT_CURRENT_STEP
        defaultUserEventQueueShouldBeFound("currentStep.equals=" + DEFAULT_CURRENT_STEP);

        // Get all the userEventQueueList where currentStep equals to UPDATED_CURRENT_STEP
        defaultUserEventQueueShouldNotBeFound("currentStep.equals=" + UPDATED_CURRENT_STEP);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByCurrentStepIsInShouldWork() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where currentStep in DEFAULT_CURRENT_STEP or UPDATED_CURRENT_STEP
        defaultUserEventQueueShouldBeFound("currentStep.in=" + DEFAULT_CURRENT_STEP + "," + UPDATED_CURRENT_STEP);

        // Get all the userEventQueueList where currentStep equals to UPDATED_CURRENT_STEP
        defaultUserEventQueueShouldNotBeFound("currentStep.in=" + UPDATED_CURRENT_STEP);
    }

    @Test
    @Transactional
    public void getAllUserEventQueuesByCurrentStepIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        // Get all the userEventQueueList where currentStep is not null
        defaultUserEventQueueShouldBeFound("currentStep.specified=true");

        // Get all the userEventQueueList where currentStep is null
        defaultUserEventQueueShouldNotBeFound("currentStep.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserEventQueueShouldBeFound(String filter) throws Exception {
        restUserEventQueueMockMvc.perform(get("/api/user-event-queues?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userEventQueue.getId().intValue())))
            .andExpect(jsonPath("$.[*].eventCode").value(hasItem(DEFAULT_EVENT_CODE)))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID)))
            .andExpect(jsonPath("$.[*].eventDate").value(hasItem(DEFAULT_EVENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].eventPoint").value(hasItem(DEFAULT_EVENT_POINT.intValue())))
            .andExpect(jsonPath("$.[*].eventCategory").value(hasItem(DEFAULT_EVENT_CATEGORY)))
            .andExpect(jsonPath("$.[*].programUserId").value(hasItem(DEFAULT_PROGRAM_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].executeStatus").value(hasItem(DEFAULT_EXECUTE_STATUS)))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].readAt").value(hasItem(DEFAULT_READ_AT.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE)))
            .andExpect(jsonPath("$.[*].currentStep").value(hasItem(DEFAULT_CURRENT_STEP)));

        // Check, that the count call also returns 1
        restUserEventQueueMockMvc.perform(get("/api/user-event-queues/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserEventQueueShouldNotBeFound(String filter) throws Exception {
        restUserEventQueueMockMvc.perform(get("/api/user-event-queues?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserEventQueueMockMvc.perform(get("/api/user-event-queues/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUserEventQueue() throws Exception {
        // Get the userEventQueue
        restUserEventQueueMockMvc.perform(get("/api/user-event-queues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserEventQueue() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        int databaseSizeBeforeUpdate = userEventQueueRepository.findAll().size();

        // Update the userEventQueue
        UserEventQueue updatedUserEventQueue = userEventQueueRepository.findById(userEventQueue.getId()).get();
        // Disconnect from session so that the updates on updatedUserEventQueue are not directly saved in db
        em.detach(updatedUserEventQueue);
        updatedUserEventQueue
            .eventCode(UPDATED_EVENT_CODE)
            .eventId(UPDATED_EVENT_ID)
            .eventDate(UPDATED_EVENT_DATE)
            .eventPoint(UPDATED_EVENT_POINT)
            .eventCategory(UPDATED_EVENT_CATEGORY)
            .programUserId(UPDATED_PROGRAM_USER_ID)
            .executeStatus(UPDATED_EXECUTE_STATUS)
            .programId(UPDATED_PROGRAM_ID)
            .readAt(UPDATED_READ_AT)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .currentStep(UPDATED_CURRENT_STEP);
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(updatedUserEventQueue);

        restUserEventQueueMockMvc.perform(put("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isOk());

        // Validate the UserEventQueue in the database
        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeUpdate);
        UserEventQueue testUserEventQueue = userEventQueueList.get(userEventQueueList.size() - 1);
        assertThat(testUserEventQueue.getEventCode()).isEqualTo(UPDATED_EVENT_CODE);
        assertThat(testUserEventQueue.getEventId()).isEqualTo(UPDATED_EVENT_ID);
        assertThat(testUserEventQueue.getEventDate()).isEqualTo(UPDATED_EVENT_DATE);
        assertThat(testUserEventQueue.getEventPoint()).isEqualTo(UPDATED_EVENT_POINT);
        assertThat(testUserEventQueue.getEventCategory()).isEqualTo(UPDATED_EVENT_CATEGORY);
        assertThat(testUserEventQueue.getProgramUserId()).isEqualTo(UPDATED_PROGRAM_USER_ID);
        assertThat(testUserEventQueue.getExecuteStatus()).isEqualTo(UPDATED_EXECUTE_STATUS);
        assertThat(testUserEventQueue.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
        assertThat(testUserEventQueue.getReadAt()).isEqualTo(UPDATED_READ_AT);
        assertThat(testUserEventQueue.getErrorMessage()).isEqualTo(UPDATED_ERROR_MESSAGE);
        assertThat(testUserEventQueue.getCurrentStep()).isEqualTo(UPDATED_CURRENT_STEP);
    }

    @Test
    @Transactional
    public void updateNonExistingUserEventQueue() throws Exception {
        int databaseSizeBeforeUpdate = userEventQueueRepository.findAll().size();

        // Create the UserEventQueue
        UserEventQueueDTO userEventQueueDTO = userEventQueueMapper.toDto(userEventQueue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserEventQueueMockMvc.perform(put("/api/user-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventQueueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserEventQueue in the database
        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserEventQueue() throws Exception {
        // Initialize the database
        userEventQueueRepository.saveAndFlush(userEventQueue);

        int databaseSizeBeforeDelete = userEventQueueRepository.findAll().size();

        // Delete the userEventQueue
        restUserEventQueueMockMvc.perform(delete("/api/user-event-queues/{id}", userEventQueue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<UserEventQueue> userEventQueueList = userEventQueueRepository.findAll();
        assertThat(userEventQueueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserEventQueue.class);
        UserEventQueue userEventQueue1 = new UserEventQueue();
        userEventQueue1.setId(1L);
        UserEventQueue userEventQueue2 = new UserEventQueue();
        userEventQueue2.setId(userEventQueue1.getId());
        assertThat(userEventQueue1).isEqualTo(userEventQueue2);
        userEventQueue2.setId(2L);
        assertThat(userEventQueue1).isNotEqualTo(userEventQueue2);
        userEventQueue1.setId(null);
        assertThat(userEventQueue1).isNotEqualTo(userEventQueue2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserEventQueueDTO.class);
        UserEventQueueDTO userEventQueueDTO1 = new UserEventQueueDTO();
        userEventQueueDTO1.setId(1L);
        UserEventQueueDTO userEventQueueDTO2 = new UserEventQueueDTO();
        assertThat(userEventQueueDTO1).isNotEqualTo(userEventQueueDTO2);
        userEventQueueDTO2.setId(userEventQueueDTO1.getId());
        assertThat(userEventQueueDTO1).isEqualTo(userEventQueueDTO2);
        userEventQueueDTO2.setId(2L);
        assertThat(userEventQueueDTO1).isNotEqualTo(userEventQueueDTO2);
        userEventQueueDTO1.setId(null);
        assertThat(userEventQueueDTO1).isNotEqualTo(userEventQueueDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userEventQueueMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userEventQueueMapper.fromId(null)).isNull();
    }
}
