package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.Category;
import aduro.basic.programservice.domain.ProgramSubCategoryPoint;
import aduro.basic.programservice.domain.ProgramCategoryPoint;
import aduro.basic.programservice.domain.SubCategory;
import aduro.basic.programservice.repository.ProgramSubCategoryPointRepository;
import aduro.basic.programservice.service.ProgramSubCategoryPointService;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointDTO;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryPointMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramSubCategoryPointCriteria;
import aduro.basic.programservice.service.ProgramSubCategoryPointQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramSubCategoryPointResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramSubCategoryPointResourceIT {

    private static final String DEFAULT_CODE = "INFORMATIONAL";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_PERCENT_POINT = new BigDecimal(0.1);
    private static final BigDecimal UPDATED_PERCENT_POINT = new BigDecimal(0.2);

    private static final BigDecimal DEFAULT_VALUE_POINT = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE_POINT = new BigDecimal(2);

    private static final Integer DEFAULT_COMPLETIONS_CAP = 10;
    private static final Integer UPDATED_COMPLETIONS_CAP = 20;

    @Autowired
    private ProgramSubCategoryPointRepository programSubCategoryPointRepository;

    @Autowired
    private ProgramSubCategoryPointMapper programSubCategoryPointMapper;

    @Autowired
    private ProgramSubCategoryPointService programSubCategoryPointService;

    @Autowired
    private ProgramSubCategoryPointQueryService programSubCategoryPointQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramSubCategoryPointMockMvc;

    private ProgramSubCategoryPoint programSubCategoryPoint;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramSubCategoryPointResource programSubCategoryPointResource = new ProgramSubCategoryPointResource(programSubCategoryPointService, programSubCategoryPointQueryService);
        this.restProgramSubCategoryPointMockMvc = MockMvcBuilders.standaloneSetup(programSubCategoryPointResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramSubCategoryPoint createEntity(EntityManager em) {

        //Add Category and Sub-category
        if (TestUtil.findAll(em, SubCategory.class).isEmpty()) {

            SubCategory subCategory =  SubCategoryResourceIT.createEntity(em);
            em.persist(subCategory);
            em.flush();
        }
        ProgramSubCategoryPoint programSubCategoryPoint = new ProgramSubCategoryPoint()
            .code(DEFAULT_CODE)
            .percentPoint(DEFAULT_PERCENT_POINT)
            .valuePoint(DEFAULT_VALUE_POINT)
            .completionsCap(DEFAULT_COMPLETIONS_CAP);
        // Add required entity
        ProgramCategoryPoint programCategoryPoint;
        if (TestUtil.findAll(em, ProgramCategoryPoint.class).isEmpty()) {
            programCategoryPoint = ProgramCategoryPointResourceIT.createEntity(em);
            em.persist(programCategoryPoint);
            em.flush();
        } else {
            programCategoryPoint = TestUtil.findAll(em, ProgramCategoryPoint.class).get(0);
        }
        programSubCategoryPoint.setProgramCategoryPoint(programCategoryPoint);
        return programSubCategoryPoint;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramSubCategoryPoint createUpdatedEntity(EntityManager em) {
        ProgramSubCategoryPoint programSubCategoryPoint = new ProgramSubCategoryPoint()
            .code(UPDATED_CODE)
            .percentPoint(UPDATED_PERCENT_POINT)
            .valuePoint(UPDATED_VALUE_POINT)
            .completionsCap(UPDATED_COMPLETIONS_CAP);
        // Add required entity
        ProgramCategoryPoint programCategoryPoint;
        if (TestUtil.findAll(em, ProgramCategoryPoint.class).isEmpty()) {
            programCategoryPoint = ProgramCategoryPointResourceIT.createUpdatedEntity(em);
            em.persist(programCategoryPoint);
            em.flush();
        } else {
            programCategoryPoint = TestUtil.findAll(em, ProgramCategoryPoint.class).get(0);
        }
        programSubCategoryPoint.setProgramCategoryPoint(programCategoryPoint);
        return programSubCategoryPoint;
    }

    @BeforeEach
    public void initTest() {
        programSubCategoryPoint = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramSubCategoryPoint() throws Exception {
        int databaseSizeBeforeCreate = programSubCategoryPointRepository.findAll().size();

        // Create the ProgramSubCategoryPoint
        ProgramSubCategoryPointDTO programSubCategoryPointDTO = programSubCategoryPointMapper.toDto(programSubCategoryPoint);
        restProgramSubCategoryPointMockMvc.perform(post("/api/program-sub-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryPointDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramSubCategoryPoint in the database
        List<ProgramSubCategoryPoint> programSubCategoryPointList = programSubCategoryPointRepository.findAll();
        assertThat(programSubCategoryPointList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramSubCategoryPoint testProgramSubCategoryPoint = programSubCategoryPointList.get(programSubCategoryPointList.size() - 1);
        assertThat(testProgramSubCategoryPoint.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testProgramSubCategoryPoint.getPercentPoint()).isEqualTo(DEFAULT_PERCENT_POINT);
        assertThat(testProgramSubCategoryPoint.getValuePoint()).isEqualTo(DEFAULT_VALUE_POINT);
        assertThat(testProgramSubCategoryPoint.getCompletionsCap()).isEqualTo(DEFAULT_COMPLETIONS_CAP);
    }

    @Test
    @Transactional
    public void createProgramSubCategoryPointWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programSubCategoryPointRepository.findAll().size();

        // Create the ProgramSubCategoryPoint with an existing ID
        programSubCategoryPoint.setId(1L);
        ProgramSubCategoryPointDTO programSubCategoryPointDTO = programSubCategoryPointMapper.toDto(programSubCategoryPoint);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramSubCategoryPointMockMvc.perform(post("/api/program-sub-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryPointDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramSubCategoryPoint in the database
        List<ProgramSubCategoryPoint> programSubCategoryPointList = programSubCategoryPointRepository.findAll();
        assertThat(programSubCategoryPointList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programSubCategoryPointRepository.findAll().size();
        // set the field null
        programSubCategoryPoint.setCode(null);

        // Create the ProgramSubCategoryPoint, which fails.
        ProgramSubCategoryPointDTO programSubCategoryPointDTO = programSubCategoryPointMapper.toDto(programSubCategoryPoint);

        restProgramSubCategoryPointMockMvc.perform(post("/api/program-sub-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryPointDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramSubCategoryPoint> programSubCategoryPointList = programSubCategoryPointRepository.findAll();
        assertThat(programSubCategoryPointList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPoints() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList
        restProgramSubCategoryPointMockMvc.perform(get("/api/program-sub-category-points?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programSubCategoryPoint.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            //.andExpect(jsonPath("$.[*].percentPoint").value(hasItem(DEFAULT_PERCENT_POINT.intValue())))
            //.andExpect(jsonPath("$.[*].valuePoint").value(hasItem(DEFAULT_VALUE_POINT.intValue())))
            .andExpect(jsonPath("$.[*].completionsCap").value(hasItem(DEFAULT_COMPLETIONS_CAP)));
    }
    
    @Test
    @Transactional
    public void getProgramSubCategoryPoint() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get the programSubCategoryPoint
        restProgramSubCategoryPointMockMvc.perform(get("/api/program-sub-category-points/{id}", programSubCategoryPoint.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programSubCategoryPoint.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.percentPoint").value(DEFAULT_PERCENT_POINT.intValue()))
            .andExpect(jsonPath("$.valuePoint").value(DEFAULT_VALUE_POINT.intValue()))
            .andExpect(jsonPath("$.completionsCap").value(DEFAULT_COMPLETIONS_CAP));
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where code equals to DEFAULT_CODE
        defaultProgramSubCategoryPointShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the programSubCategoryPointList where code equals to UPDATED_CODE
        defaultProgramSubCategoryPointShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where code in DEFAULT_CODE or UPDATED_CODE
        defaultProgramSubCategoryPointShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the programSubCategoryPointList where code equals to UPDATED_CODE
        defaultProgramSubCategoryPointShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where code is not null
        defaultProgramSubCategoryPointShouldBeFound("code.specified=true");

        // Get all the programSubCategoryPointList where code is null
        defaultProgramSubCategoryPointShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByPercentPointIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where percentPoint equals to DEFAULT_PERCENT_POINT
       // defaultProgramSubCategoryPointShouldBeFound("percentPoint.equals=" + DEFAULT_PERCENT_POINT);

        // Get all the programSubCategoryPointList where percentPoint equals to UPDATED_PERCENT_POINT
        defaultProgramSubCategoryPointShouldNotBeFound("percentPoint.equals=" + UPDATED_PERCENT_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByPercentPointIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where percentPoint in DEFAULT_PERCENT_POINT or UPDATED_PERCENT_POINT
       // defaultProgramSubCategoryPointShouldBeFound("percentPoint.in=" + DEFAULT_PERCENT_POINT + "," + UPDATED_PERCENT_POINT);

        // Get all the programSubCategoryPointList where percentPoint equals to UPDATED_PERCENT_POINT
        defaultProgramSubCategoryPointShouldNotBeFound("percentPoint.in=" + UPDATED_PERCENT_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByPercentPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where percentPoint is not null
        defaultProgramSubCategoryPointShouldBeFound("percentPoint.specified=true");

        // Get all the programSubCategoryPointList where percentPoint is null
        defaultProgramSubCategoryPointShouldNotBeFound("percentPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByValuePointIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where valuePoint equals to DEFAULT_VALUE_POINT
        defaultProgramSubCategoryPointShouldBeFound("valuePoint.equals=" + DEFAULT_VALUE_POINT);

        // Get all the programSubCategoryPointList where valuePoint equals to UPDATED_VALUE_POINT
        defaultProgramSubCategoryPointShouldNotBeFound("valuePoint.equals=" + UPDATED_VALUE_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByValuePointIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where valuePoint in DEFAULT_VALUE_POINT or UPDATED_VALUE_POINT
        defaultProgramSubCategoryPointShouldBeFound("valuePoint.in=" + DEFAULT_VALUE_POINT + "," + UPDATED_VALUE_POINT);

        // Get all the programSubCategoryPointList where valuePoint equals to UPDATED_VALUE_POINT
        defaultProgramSubCategoryPointShouldNotBeFound("valuePoint.in=" + UPDATED_VALUE_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByValuePointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where valuePoint is not null
        defaultProgramSubCategoryPointShouldBeFound("valuePoint.specified=true");

        // Get all the programSubCategoryPointList where valuePoint is null
        defaultProgramSubCategoryPointShouldNotBeFound("valuePoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByCompletionsCapIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where completionsCap equals to DEFAULT_COMPLETIONS_CAP
        defaultProgramSubCategoryPointShouldBeFound("completionsCap.equals=" + DEFAULT_COMPLETIONS_CAP);

        // Get all the programSubCategoryPointList where completionsCap equals to UPDATED_COMPLETIONS_CAP
        defaultProgramSubCategoryPointShouldNotBeFound("completionsCap.equals=" + UPDATED_COMPLETIONS_CAP);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByCompletionsCapIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where completionsCap in DEFAULT_COMPLETIONS_CAP or UPDATED_COMPLETIONS_CAP
        defaultProgramSubCategoryPointShouldBeFound("completionsCap.in=" + DEFAULT_COMPLETIONS_CAP + "," + UPDATED_COMPLETIONS_CAP);

        // Get all the programSubCategoryPointList where completionsCap equals to UPDATED_COMPLETIONS_CAP
        defaultProgramSubCategoryPointShouldNotBeFound("completionsCap.in=" + UPDATED_COMPLETIONS_CAP);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByCompletionsCapIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where completionsCap is not null
        defaultProgramSubCategoryPointShouldBeFound("completionsCap.specified=true");

        // Get all the programSubCategoryPointList where completionsCap is null
        defaultProgramSubCategoryPointShouldNotBeFound("completionsCap.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByCompletionsCapIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where completionsCap greater than or equals to DEFAULT_COMPLETIONS_CAP
        defaultProgramSubCategoryPointShouldBeFound("completionsCap.greaterOrEqualThan=" + DEFAULT_COMPLETIONS_CAP);

        // Get all the programSubCategoryPointList where completionsCap greater than or equals to UPDATED_COMPLETIONS_CAP
        defaultProgramSubCategoryPointShouldNotBeFound("completionsCap.greaterOrEqualThan=" + UPDATED_COMPLETIONS_CAP);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByCompletionsCapIsLessThanSomething() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        // Get all the programSubCategoryPointList where completionsCap less than or equals to DEFAULT_COMPLETIONS_CAP
        defaultProgramSubCategoryPointShouldNotBeFound("completionsCap.lessThan=" + DEFAULT_COMPLETIONS_CAP);

        // Get all the programSubCategoryPointList where completionsCap less than or equals to UPDATED_COMPLETIONS_CAP
        defaultProgramSubCategoryPointShouldBeFound("completionsCap.lessThan=" + UPDATED_COMPLETIONS_CAP);
    }


    @Test
    @Transactional
    public void getAllProgramSubCategoryPointsByProgramCategoryPointIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramCategoryPoint programCategoryPoint = programSubCategoryPoint.getProgramCategoryPoint();
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);
        Long programCategoryPointId = programCategoryPoint.getId();

        // Get all the programSubCategoryPointList where programCategoryPoint equals to programCategoryPointId
        defaultProgramSubCategoryPointShouldBeFound("programCategoryPointId.equals=" + programCategoryPointId);

        // Get all the programSubCategoryPointList where programCategoryPoint equals to programCategoryPointId + 1
        defaultProgramSubCategoryPointShouldNotBeFound("programCategoryPointId.equals=" + (programCategoryPointId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramSubCategoryPointShouldBeFound(String filter) throws Exception {
        restProgramSubCategoryPointMockMvc.perform(get("/api/program-sub-category-points?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programSubCategoryPoint.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            //.andExpect(jsonPath("$.[*].percentPoint").value(hasItem(DEFAULT_PERCENT_POINT.intValue())))
           // .andExpect(jsonPath("$.[*].valuePoint").value(hasItem(DEFAULT_VALUE_POINT.intValue())))
            .andExpect(jsonPath("$.[*].completionsCap").value(hasItem(DEFAULT_COMPLETIONS_CAP)));

        // Check, that the count call also returns 1
        restProgramSubCategoryPointMockMvc.perform(get("/api/program-sub-category-points/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramSubCategoryPointShouldNotBeFound(String filter) throws Exception {
        restProgramSubCategoryPointMockMvc.perform(get("/api/program-sub-category-points?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramSubCategoryPointMockMvc.perform(get("/api/program-sub-category-points/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramSubCategoryPoint() throws Exception {
        // Get the programSubCategoryPoint
        restProgramSubCategoryPointMockMvc.perform(get("/api/program-sub-category-points/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramSubCategoryPoint() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        int databaseSizeBeforeUpdate = programSubCategoryPointRepository.findAll().size();

        // Update the programSubCategoryPoint
        ProgramSubCategoryPoint updatedProgramSubCategoryPoint = programSubCategoryPointRepository.findById(programSubCategoryPoint.getId()).get();
        // Disconnect from session so that the updates on updatedProgramSubCategoryPoint are not directly saved in db
        em.detach(updatedProgramSubCategoryPoint);
        updatedProgramSubCategoryPoint
            .code(DEFAULT_CODE)
            .percentPoint(UPDATED_PERCENT_POINT)
            .valuePoint(UPDATED_VALUE_POINT)
            .completionsCap(UPDATED_COMPLETIONS_CAP);
        ProgramSubCategoryPointDTO programSubCategoryPointDTO = programSubCategoryPointMapper.toDto(updatedProgramSubCategoryPoint);

        restProgramSubCategoryPointMockMvc.perform(put("/api/program-sub-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryPointDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramSubCategoryPoint in the database
        List<ProgramSubCategoryPoint> programSubCategoryPointList = programSubCategoryPointRepository.findAll();
        assertThat(programSubCategoryPointList).hasSize(databaseSizeBeforeUpdate);
        ProgramSubCategoryPoint testProgramSubCategoryPoint = programSubCategoryPointList.get(programSubCategoryPointList.size() - 1);
        assertThat(testProgramSubCategoryPoint.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testProgramSubCategoryPoint.getPercentPoint()).isEqualTo(UPDATED_PERCENT_POINT);
       // assertThat(testProgramSubCategoryPoint.getValuePoint()).isEqualTo(UPDATED_VALUE_POINT);
        assertThat(testProgramSubCategoryPoint.getCompletionsCap()).isEqualTo(UPDATED_COMPLETIONS_CAP);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramSubCategoryPoint() throws Exception {
        int databaseSizeBeforeUpdate = programSubCategoryPointRepository.findAll().size();

        // Create the ProgramSubCategoryPoint
        ProgramSubCategoryPointDTO programSubCategoryPointDTO = programSubCategoryPointMapper.toDto(programSubCategoryPoint);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramSubCategoryPointMockMvc.perform(put("/api/program-sub-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryPointDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramSubCategoryPoint in the database
        List<ProgramSubCategoryPoint> programSubCategoryPointList = programSubCategoryPointRepository.findAll();
        assertThat(programSubCategoryPointList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramSubCategoryPoint() throws Exception {
        // Initialize the database
        programSubCategoryPointRepository.saveAndFlush(programSubCategoryPoint);

        int databaseSizeBeforeDelete = programSubCategoryPointRepository.findAll().size();

        // Delete the programSubCategoryPoint
        restProgramSubCategoryPointMockMvc.perform(delete("/api/program-sub-category-points/{id}", programSubCategoryPoint.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramSubCategoryPoint> programSubCategoryPointList = programSubCategoryPointRepository.findAll();
        assertThat(programSubCategoryPointList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramSubCategoryPoint.class);
        ProgramSubCategoryPoint programSubCategoryPoint1 = new ProgramSubCategoryPoint();
        programSubCategoryPoint1.setId(1L);
        ProgramSubCategoryPoint programSubCategoryPoint2 = new ProgramSubCategoryPoint();
        programSubCategoryPoint2.setId(programSubCategoryPoint1.getId());
        assertThat(programSubCategoryPoint1).isEqualTo(programSubCategoryPoint2);
        programSubCategoryPoint2.setId(2L);
        assertThat(programSubCategoryPoint1).isNotEqualTo(programSubCategoryPoint2);
        programSubCategoryPoint1.setId(null);
        assertThat(programSubCategoryPoint1).isNotEqualTo(programSubCategoryPoint2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramSubCategoryPointDTO.class);
        ProgramSubCategoryPointDTO programSubCategoryPointDTO1 = new ProgramSubCategoryPointDTO();
        programSubCategoryPointDTO1.setId(1L);
        ProgramSubCategoryPointDTO programSubCategoryPointDTO2 = new ProgramSubCategoryPointDTO();
        assertThat(programSubCategoryPointDTO1).isNotEqualTo(programSubCategoryPointDTO2);
        programSubCategoryPointDTO2.setId(programSubCategoryPointDTO1.getId());
        assertThat(programSubCategoryPointDTO1).isEqualTo(programSubCategoryPointDTO2);
        programSubCategoryPointDTO2.setId(2L);
        assertThat(programSubCategoryPointDTO1).isNotEqualTo(programSubCategoryPointDTO2);
        programSubCategoryPointDTO1.setId(null);
        assertThat(programSubCategoryPointDTO1).isNotEqualTo(programSubCategoryPointDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programSubCategoryPointMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programSubCategoryPointMapper.fromId(null)).isNull();
    }
}
