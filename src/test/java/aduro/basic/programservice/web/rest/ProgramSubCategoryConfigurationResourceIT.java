package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramSubCategoryConfiguration;
import aduro.basic.programservice.repository.ProgramSubCategoryConfigurationRepository;
import aduro.basic.programservice.service.ProgramSubCategoryConfigurationService;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationDTO;
import aduro.basic.programservice.service.mapper.ProgramSubCategoryConfigurationMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramSubCategoryConfigurationCriteria;
import aduro.basic.programservice.service.ProgramSubCategoryConfigurationQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramSubCategoryConfigurationResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramSubCategoryConfigurationResourceIT {

    private static final String DEFAULT_SUB_CATEGORY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SUB_CATEGORY_CODE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST = false;
    private static final Boolean UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST = true;

    private static final Boolean DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST = false;
    private static final Boolean UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST = true;

    private static final Boolean DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE = false;
    private static final Boolean UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE = true;

    private static final Boolean DEFAULT_IS_BMI_AWARDED_IN_RANGE = false;
    private static final Boolean UPDATED_IS_BMI_AWARDED_IN_RANGE = true;

    private static final Boolean DEFAULT_IS_BMI_AWARDED_FASTING = false;
    private static final Boolean UPDATED_IS_BMI_AWARDED_FASTING = true;

    private static final Boolean DEFAULT_IS_BMI_AWARDED_NON_FASTING = false;
    private static final Boolean UPDATED_IS_BMI_AWARDED_NON_FASTING = true;

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    @Autowired
    private ProgramSubCategoryConfigurationRepository programSubCategoryConfigurationRepository;

    @Autowired
    private ProgramSubCategoryConfigurationMapper programSubCategoryConfigurationMapper;

    @Autowired
    private ProgramSubCategoryConfigurationService programSubCategoryConfigurationService;

    @Autowired
    private ProgramSubCategoryConfigurationQueryService programSubCategoryConfigurationQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramSubCategoryConfigurationMockMvc;

    private ProgramSubCategoryConfiguration programSubCategoryConfiguration;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramSubCategoryConfigurationResource programSubCategoryConfigurationResource = new ProgramSubCategoryConfigurationResource(programSubCategoryConfigurationService, programSubCategoryConfigurationQueryService);
        this.restProgramSubCategoryConfigurationMockMvc = MockMvcBuilders.standaloneSetup(programSubCategoryConfigurationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramSubCategoryConfiguration createEntity(EntityManager em) {
        ProgramSubCategoryConfiguration programSubCategoryConfiguration = new ProgramSubCategoryConfiguration()
            .subCategoryCode(DEFAULT_SUB_CATEGORY_CODE)
            .isBloodPressureSingleTest(DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST)
            .isBloodPressureIndividualTest(DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST)
            .isGlucoseAwardedInRange(DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE)
            .isBmiAwardedInRange(DEFAULT_IS_BMI_AWARDED_IN_RANGE)
            .isBmiAwardedFasting(DEFAULT_IS_BMI_AWARDED_FASTING)
            .isBmiAwardedNonFasting(DEFAULT_IS_BMI_AWARDED_NON_FASTING)
            .programId(DEFAULT_PROGRAM_ID);
        return programSubCategoryConfiguration;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramSubCategoryConfiguration createUpdatedEntity(EntityManager em) {
        ProgramSubCategoryConfiguration programSubCategoryConfiguration = new ProgramSubCategoryConfiguration()
            .subCategoryCode(UPDATED_SUB_CATEGORY_CODE)
            .isBloodPressureSingleTest(UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST)
            .isBloodPressureIndividualTest(UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST)
            .isGlucoseAwardedInRange(UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE)
            .isBmiAwardedInRange(UPDATED_IS_BMI_AWARDED_IN_RANGE)
            .isBmiAwardedFasting(UPDATED_IS_BMI_AWARDED_FASTING)
            .isBmiAwardedNonFasting(UPDATED_IS_BMI_AWARDED_NON_FASTING)
            .programId(UPDATED_PROGRAM_ID);
        return programSubCategoryConfiguration;
    }

    @BeforeEach
    public void initTest() {
        programSubCategoryConfiguration = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramSubCategoryConfiguration() throws Exception {
        int databaseSizeBeforeCreate = programSubCategoryConfigurationRepository.findAll().size();

        // Create the ProgramSubCategoryConfiguration
        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO = programSubCategoryConfigurationMapper.toDto(programSubCategoryConfiguration);
        restProgramSubCategoryConfigurationMockMvc.perform(post("/api/program-sub-category-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryConfigurationDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramSubCategoryConfiguration in the database
        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.findAll();
        assertThat(programSubCategoryConfigurationList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramSubCategoryConfiguration testProgramSubCategoryConfiguration = programSubCategoryConfigurationList.get(programSubCategoryConfigurationList.size() - 1);
        assertThat(testProgramSubCategoryConfiguration.getSubCategoryCode()).isEqualTo(DEFAULT_SUB_CATEGORY_CODE);
        assertThat(testProgramSubCategoryConfiguration.isIsBloodPressureSingleTest()).isEqualTo(DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST);
        assertThat(testProgramSubCategoryConfiguration.isIsBloodPressureIndividualTest()).isEqualTo(DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST);
        assertThat(testProgramSubCategoryConfiguration.isIsGlucoseAwardedInRange()).isEqualTo(DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE);
        assertThat(testProgramSubCategoryConfiguration.isIsBmiAwardedInRange()).isEqualTo(DEFAULT_IS_BMI_AWARDED_IN_RANGE);
        assertThat(testProgramSubCategoryConfiguration.isIsBmiAwardedFasting()).isEqualTo(DEFAULT_IS_BMI_AWARDED_FASTING);
        assertThat(testProgramSubCategoryConfiguration.isIsBmiAwardedNonFasting()).isEqualTo(DEFAULT_IS_BMI_AWARDED_NON_FASTING);
        assertThat(testProgramSubCategoryConfiguration.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void createProgramSubCategoryConfigurationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programSubCategoryConfigurationRepository.findAll().size();

        // Create the ProgramSubCategoryConfiguration with an existing ID
        programSubCategoryConfiguration.setId(1L);
        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO = programSubCategoryConfigurationMapper.toDto(programSubCategoryConfiguration);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramSubCategoryConfigurationMockMvc.perform(post("/api/program-sub-category-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryConfigurationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramSubCategoryConfiguration in the database
        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.findAll();
        assertThat(programSubCategoryConfigurationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSubCategoryCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programSubCategoryConfigurationRepository.findAll().size();
        // set the field null
        programSubCategoryConfiguration.setSubCategoryCode(null);

        // Create the ProgramSubCategoryConfiguration, which fails.
        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO = programSubCategoryConfigurationMapper.toDto(programSubCategoryConfiguration);

        restProgramSubCategoryConfigurationMockMvc.perform(post("/api/program-sub-category-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryConfigurationDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.findAll();
        assertThat(programSubCategoryConfigurationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programSubCategoryConfigurationRepository.findAll().size();
        // set the field null
        programSubCategoryConfiguration.setProgramId(null);

        // Create the ProgramSubCategoryConfiguration, which fails.
        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO = programSubCategoryConfigurationMapper.toDto(programSubCategoryConfiguration);

        restProgramSubCategoryConfigurationMockMvc.perform(post("/api/program-sub-category-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryConfigurationDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.findAll();
        assertThat(programSubCategoryConfigurationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurations() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList
        restProgramSubCategoryConfigurationMockMvc.perform(get("/api/program-sub-category-configurations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programSubCategoryConfiguration.getId().intValue())))
            .andExpect(jsonPath("$.[*].subCategoryCode").value(hasItem(DEFAULT_SUB_CATEGORY_CODE.toString())))
            .andExpect(jsonPath("$.[*].isBloodPressureSingleTest").value(hasItem(DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST.booleanValue())))
            .andExpect(jsonPath("$.[*].isBloodPressureIndividualTest").value(hasItem(DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST.booleanValue())))
            .andExpect(jsonPath("$.[*].isGlucoseAwardedInRange").value(hasItem(DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE.booleanValue())))
            .andExpect(jsonPath("$.[*].isBmiAwardedInRange").value(hasItem(DEFAULT_IS_BMI_AWARDED_IN_RANGE.booleanValue())))
            .andExpect(jsonPath("$.[*].isBmiAwardedFasting").value(hasItem(DEFAULT_IS_BMI_AWARDED_FASTING.booleanValue())))
            .andExpect(jsonPath("$.[*].isBmiAwardedNonFasting").value(hasItem(DEFAULT_IS_BMI_AWARDED_NON_FASTING.booleanValue())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getProgramSubCategoryConfiguration() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get the programSubCategoryConfiguration
        restProgramSubCategoryConfigurationMockMvc.perform(get("/api/program-sub-category-configurations/{id}", programSubCategoryConfiguration.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programSubCategoryConfiguration.getId().intValue()))
            .andExpect(jsonPath("$.subCategoryCode").value(DEFAULT_SUB_CATEGORY_CODE.toString()))
            .andExpect(jsonPath("$.isBloodPressureSingleTest").value(DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST.booleanValue()))
            .andExpect(jsonPath("$.isBloodPressureIndividualTest").value(DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST.booleanValue()))
            .andExpect(jsonPath("$.isGlucoseAwardedInRange").value(DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE.booleanValue()))
            .andExpect(jsonPath("$.isBmiAwardedInRange").value(DEFAULT_IS_BMI_AWARDED_IN_RANGE.booleanValue()))
            .andExpect(jsonPath("$.isBmiAwardedFasting").value(DEFAULT_IS_BMI_AWARDED_FASTING.booleanValue()))
            .andExpect(jsonPath("$.isBmiAwardedNonFasting").value(DEFAULT_IS_BMI_AWARDED_NON_FASTING.booleanValue()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()));
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsBySubCategoryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where subCategoryCode equals to DEFAULT_SUB_CATEGORY_CODE
        defaultProgramSubCategoryConfigurationShouldBeFound("subCategoryCode.equals=" + DEFAULT_SUB_CATEGORY_CODE);

        // Get all the programSubCategoryConfigurationList where subCategoryCode equals to UPDATED_SUB_CATEGORY_CODE
        defaultProgramSubCategoryConfigurationShouldNotBeFound("subCategoryCode.equals=" + UPDATED_SUB_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsBySubCategoryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where subCategoryCode in DEFAULT_SUB_CATEGORY_CODE or UPDATED_SUB_CATEGORY_CODE
        defaultProgramSubCategoryConfigurationShouldBeFound("subCategoryCode.in=" + DEFAULT_SUB_CATEGORY_CODE + "," + UPDATED_SUB_CATEGORY_CODE);

        // Get all the programSubCategoryConfigurationList where subCategoryCode equals to UPDATED_SUB_CATEGORY_CODE
        defaultProgramSubCategoryConfigurationShouldNotBeFound("subCategoryCode.in=" + UPDATED_SUB_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsBySubCategoryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where subCategoryCode is not null
        defaultProgramSubCategoryConfigurationShouldBeFound("subCategoryCode.specified=true");

        // Get all the programSubCategoryConfigurationList where subCategoryCode is null
        defaultProgramSubCategoryConfigurationShouldNotBeFound("subCategoryCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBloodPressureSingleTestIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBloodPressureSingleTest equals to DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST
        defaultProgramSubCategoryConfigurationShouldBeFound("isBloodPressureSingleTest.equals=" + DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST);

        // Get all the programSubCategoryConfigurationList where isBloodPressureSingleTest equals to UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBloodPressureSingleTest.equals=" + UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBloodPressureSingleTestIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBloodPressureSingleTest in DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST or UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST
        defaultProgramSubCategoryConfigurationShouldBeFound("isBloodPressureSingleTest.in=" + DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST + "," + UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST);

        // Get all the programSubCategoryConfigurationList where isBloodPressureSingleTest equals to UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBloodPressureSingleTest.in=" + UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBloodPressureSingleTestIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBloodPressureSingleTest is not null
        defaultProgramSubCategoryConfigurationShouldBeFound("isBloodPressureSingleTest.specified=true");

        // Get all the programSubCategoryConfigurationList where isBloodPressureSingleTest is null
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBloodPressureSingleTest.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBloodPressureIndividualTestIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBloodPressureIndividualTest equals to DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST
        defaultProgramSubCategoryConfigurationShouldBeFound("isBloodPressureIndividualTest.equals=" + DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST);

        // Get all the programSubCategoryConfigurationList where isBloodPressureIndividualTest equals to UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBloodPressureIndividualTest.equals=" + UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBloodPressureIndividualTestIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBloodPressureIndividualTest in DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST or UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST
        defaultProgramSubCategoryConfigurationShouldBeFound("isBloodPressureIndividualTest.in=" + DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST + "," + UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST);

        // Get all the programSubCategoryConfigurationList where isBloodPressureIndividualTest equals to UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBloodPressureIndividualTest.in=" + UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBloodPressureIndividualTestIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBloodPressureIndividualTest is not null
        defaultProgramSubCategoryConfigurationShouldBeFound("isBloodPressureIndividualTest.specified=true");

        // Get all the programSubCategoryConfigurationList where isBloodPressureIndividualTest is null
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBloodPressureIndividualTest.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsGlucoseAwardedInRangeIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isGlucoseAwardedInRange equals to DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE
        defaultProgramSubCategoryConfigurationShouldBeFound("isGlucoseAwardedInRange.equals=" + DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE);

        // Get all the programSubCategoryConfigurationList where isGlucoseAwardedInRange equals to UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isGlucoseAwardedInRange.equals=" + UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsGlucoseAwardedInRangeIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isGlucoseAwardedInRange in DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE or UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE
        defaultProgramSubCategoryConfigurationShouldBeFound("isGlucoseAwardedInRange.in=" + DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE + "," + UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE);

        // Get all the programSubCategoryConfigurationList where isGlucoseAwardedInRange equals to UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isGlucoseAwardedInRange.in=" + UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsGlucoseAwardedInRangeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isGlucoseAwardedInRange is not null
        defaultProgramSubCategoryConfigurationShouldBeFound("isGlucoseAwardedInRange.specified=true");

        // Get all the programSubCategoryConfigurationList where isGlucoseAwardedInRange is null
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isGlucoseAwardedInRange.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedInRangeIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedInRange equals to DEFAULT_IS_BMI_AWARDED_IN_RANGE
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedInRange.equals=" + DEFAULT_IS_BMI_AWARDED_IN_RANGE);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedInRange equals to UPDATED_IS_BMI_AWARDED_IN_RANGE
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedInRange.equals=" + UPDATED_IS_BMI_AWARDED_IN_RANGE);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedInRangeIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedInRange in DEFAULT_IS_BMI_AWARDED_IN_RANGE or UPDATED_IS_BMI_AWARDED_IN_RANGE
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedInRange.in=" + DEFAULT_IS_BMI_AWARDED_IN_RANGE + "," + UPDATED_IS_BMI_AWARDED_IN_RANGE);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedInRange equals to UPDATED_IS_BMI_AWARDED_IN_RANGE
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedInRange.in=" + UPDATED_IS_BMI_AWARDED_IN_RANGE);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedInRangeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedInRange is not null
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedInRange.specified=true");

        // Get all the programSubCategoryConfigurationList where isBmiAwardedInRange is null
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedInRange.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedFastingIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedFasting equals to DEFAULT_IS_BMI_AWARDED_FASTING
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedFasting.equals=" + DEFAULT_IS_BMI_AWARDED_FASTING);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedFasting equals to UPDATED_IS_BMI_AWARDED_FASTING
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedFasting.equals=" + UPDATED_IS_BMI_AWARDED_FASTING);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedFastingIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedFasting in DEFAULT_IS_BMI_AWARDED_FASTING or UPDATED_IS_BMI_AWARDED_FASTING
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedFasting.in=" + DEFAULT_IS_BMI_AWARDED_FASTING + "," + UPDATED_IS_BMI_AWARDED_FASTING);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedFasting equals to UPDATED_IS_BMI_AWARDED_FASTING
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedFasting.in=" + UPDATED_IS_BMI_AWARDED_FASTING);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedFastingIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedFasting is not null
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedFasting.specified=true");

        // Get all the programSubCategoryConfigurationList where isBmiAwardedFasting is null
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedFasting.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedNonFastingIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedNonFasting equals to DEFAULT_IS_BMI_AWARDED_NON_FASTING
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedNonFasting.equals=" + DEFAULT_IS_BMI_AWARDED_NON_FASTING);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedNonFasting equals to UPDATED_IS_BMI_AWARDED_NON_FASTING
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedNonFasting.equals=" + UPDATED_IS_BMI_AWARDED_NON_FASTING);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedNonFastingIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedNonFasting in DEFAULT_IS_BMI_AWARDED_NON_FASTING or UPDATED_IS_BMI_AWARDED_NON_FASTING
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedNonFasting.in=" + DEFAULT_IS_BMI_AWARDED_NON_FASTING + "," + UPDATED_IS_BMI_AWARDED_NON_FASTING);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedNonFasting equals to UPDATED_IS_BMI_AWARDED_NON_FASTING
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedNonFasting.in=" + UPDATED_IS_BMI_AWARDED_NON_FASTING);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByIsBmiAwardedNonFastingIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where isBmiAwardedNonFasting is not null
        defaultProgramSubCategoryConfigurationShouldBeFound("isBmiAwardedNonFasting.specified=true");

        // Get all the programSubCategoryConfigurationList where isBmiAwardedNonFasting is null
        defaultProgramSubCategoryConfigurationShouldNotBeFound("isBmiAwardedNonFasting.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByProgramIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where programId equals to DEFAULT_PROGRAM_ID
        defaultProgramSubCategoryConfigurationShouldBeFound("programId.equals=" + DEFAULT_PROGRAM_ID);

        // Get all the programSubCategoryConfigurationList where programId equals to UPDATED_PROGRAM_ID
        defaultProgramSubCategoryConfigurationShouldNotBeFound("programId.equals=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByProgramIdIsInShouldWork() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where programId in DEFAULT_PROGRAM_ID or UPDATED_PROGRAM_ID
        defaultProgramSubCategoryConfigurationShouldBeFound("programId.in=" + DEFAULT_PROGRAM_ID + "," + UPDATED_PROGRAM_ID);

        // Get all the programSubCategoryConfigurationList where programId equals to UPDATED_PROGRAM_ID
        defaultProgramSubCategoryConfigurationShouldNotBeFound("programId.in=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByProgramIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where programId is not null
        defaultProgramSubCategoryConfigurationShouldBeFound("programId.specified=true");

        // Get all the programSubCategoryConfigurationList where programId is null
        defaultProgramSubCategoryConfigurationShouldNotBeFound("programId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByProgramIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where programId greater than or equals to DEFAULT_PROGRAM_ID
        defaultProgramSubCategoryConfigurationShouldBeFound("programId.greaterOrEqualThan=" + DEFAULT_PROGRAM_ID);

        // Get all the programSubCategoryConfigurationList where programId greater than or equals to UPDATED_PROGRAM_ID
        defaultProgramSubCategoryConfigurationShouldNotBeFound("programId.greaterOrEqualThan=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramSubCategoryConfigurationsByProgramIdIsLessThanSomething() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        // Get all the programSubCategoryConfigurationList where programId less than or equals to DEFAULT_PROGRAM_ID
        defaultProgramSubCategoryConfigurationShouldNotBeFound("programId.lessThan=" + DEFAULT_PROGRAM_ID);

        // Get all the programSubCategoryConfigurationList where programId less than or equals to UPDATED_PROGRAM_ID
        defaultProgramSubCategoryConfigurationShouldBeFound("programId.lessThan=" + UPDATED_PROGRAM_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramSubCategoryConfigurationShouldBeFound(String filter) throws Exception {
        restProgramSubCategoryConfigurationMockMvc.perform(get("/api/program-sub-category-configurations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programSubCategoryConfiguration.getId().intValue())))
            .andExpect(jsonPath("$.[*].subCategoryCode").value(hasItem(DEFAULT_SUB_CATEGORY_CODE)))
            .andExpect(jsonPath("$.[*].isBloodPressureSingleTest").value(hasItem(DEFAULT_IS_BLOOD_PRESSURE_SINGLE_TEST.booleanValue())))
            .andExpect(jsonPath("$.[*].isBloodPressureIndividualTest").value(hasItem(DEFAULT_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST.booleanValue())))
            .andExpect(jsonPath("$.[*].isGlucoseAwardedInRange").value(hasItem(DEFAULT_IS_GLUCOSE_AWARDED_IN_RANGE.booleanValue())))
            .andExpect(jsonPath("$.[*].isBmiAwardedInRange").value(hasItem(DEFAULT_IS_BMI_AWARDED_IN_RANGE.booleanValue())))
            .andExpect(jsonPath("$.[*].isBmiAwardedFasting").value(hasItem(DEFAULT_IS_BMI_AWARDED_FASTING.booleanValue())))
            .andExpect(jsonPath("$.[*].isBmiAwardedNonFasting").value(hasItem(DEFAULT_IS_BMI_AWARDED_NON_FASTING.booleanValue())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())));

        // Check, that the count call also returns 1
        restProgramSubCategoryConfigurationMockMvc.perform(get("/api/program-sub-category-configurations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramSubCategoryConfigurationShouldNotBeFound(String filter) throws Exception {
        restProgramSubCategoryConfigurationMockMvc.perform(get("/api/program-sub-category-configurations?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramSubCategoryConfigurationMockMvc.perform(get("/api/program-sub-category-configurations/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramSubCategoryConfiguration() throws Exception {
        // Get the programSubCategoryConfiguration
        restProgramSubCategoryConfigurationMockMvc.perform(get("/api/program-sub-category-configurations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramSubCategoryConfiguration() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        int databaseSizeBeforeUpdate = programSubCategoryConfigurationRepository.findAll().size();

        // Update the programSubCategoryConfiguration
        ProgramSubCategoryConfiguration updatedProgramSubCategoryConfiguration = programSubCategoryConfigurationRepository.findById(programSubCategoryConfiguration.getId()).get();
        // Disconnect from session so that the updates on updatedProgramSubCategoryConfiguration are not directly saved in db
        em.detach(updatedProgramSubCategoryConfiguration);
        updatedProgramSubCategoryConfiguration
            .subCategoryCode(UPDATED_SUB_CATEGORY_CODE)
            .isBloodPressureSingleTest(UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST)
            .isBloodPressureIndividualTest(UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST)
            .isGlucoseAwardedInRange(UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE)
            .isBmiAwardedInRange(UPDATED_IS_BMI_AWARDED_IN_RANGE)
            .isBmiAwardedFasting(UPDATED_IS_BMI_AWARDED_FASTING)
            .isBmiAwardedNonFasting(UPDATED_IS_BMI_AWARDED_NON_FASTING)
            .programId(UPDATED_PROGRAM_ID);
        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO = programSubCategoryConfigurationMapper.toDto(updatedProgramSubCategoryConfiguration);

        restProgramSubCategoryConfigurationMockMvc.perform(put("/api/program-sub-category-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryConfigurationDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramSubCategoryConfiguration in the database
        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.findAll();
        assertThat(programSubCategoryConfigurationList).hasSize(databaseSizeBeforeUpdate);
        ProgramSubCategoryConfiguration testProgramSubCategoryConfiguration = programSubCategoryConfigurationList.get(programSubCategoryConfigurationList.size() - 1);
        assertThat(testProgramSubCategoryConfiguration.getSubCategoryCode()).isEqualTo(UPDATED_SUB_CATEGORY_CODE);
        assertThat(testProgramSubCategoryConfiguration.isIsBloodPressureSingleTest()).isEqualTo(UPDATED_IS_BLOOD_PRESSURE_SINGLE_TEST);
        assertThat(testProgramSubCategoryConfiguration.isIsBloodPressureIndividualTest()).isEqualTo(UPDATED_IS_BLOOD_PRESSURE_INDIVIDUAL_TEST);
        assertThat(testProgramSubCategoryConfiguration.isIsGlucoseAwardedInRange()).isEqualTo(UPDATED_IS_GLUCOSE_AWARDED_IN_RANGE);
        assertThat(testProgramSubCategoryConfiguration.isIsBmiAwardedInRange()).isEqualTo(UPDATED_IS_BMI_AWARDED_IN_RANGE);
        assertThat(testProgramSubCategoryConfiguration.isIsBmiAwardedFasting()).isEqualTo(UPDATED_IS_BMI_AWARDED_FASTING);
        assertThat(testProgramSubCategoryConfiguration.isIsBmiAwardedNonFasting()).isEqualTo(UPDATED_IS_BMI_AWARDED_NON_FASTING);
        assertThat(testProgramSubCategoryConfiguration.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramSubCategoryConfiguration() throws Exception {
        int databaseSizeBeforeUpdate = programSubCategoryConfigurationRepository.findAll().size();

        // Create the ProgramSubCategoryConfiguration
        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO = programSubCategoryConfigurationMapper.toDto(programSubCategoryConfiguration);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramSubCategoryConfigurationMockMvc.perform(put("/api/program-sub-category-configurations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubCategoryConfigurationDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramSubCategoryConfiguration in the database
        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.findAll();
        assertThat(programSubCategoryConfigurationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramSubCategoryConfiguration() throws Exception {
        // Initialize the database
        programSubCategoryConfigurationRepository.saveAndFlush(programSubCategoryConfiguration);

        int databaseSizeBeforeDelete = programSubCategoryConfigurationRepository.findAll().size();

        // Delete the programSubCategoryConfiguration
        restProgramSubCategoryConfigurationMockMvc.perform(delete("/api/program-sub-category-configurations/{id}", programSubCategoryConfiguration.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramSubCategoryConfiguration> programSubCategoryConfigurationList = programSubCategoryConfigurationRepository.findAll();
        assertThat(programSubCategoryConfigurationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramSubCategoryConfiguration.class);
        ProgramSubCategoryConfiguration programSubCategoryConfiguration1 = new ProgramSubCategoryConfiguration();
        programSubCategoryConfiguration1.setId(1L);
        ProgramSubCategoryConfiguration programSubCategoryConfiguration2 = new ProgramSubCategoryConfiguration();
        programSubCategoryConfiguration2.setId(programSubCategoryConfiguration1.getId());
        assertThat(programSubCategoryConfiguration1).isEqualTo(programSubCategoryConfiguration2);
        programSubCategoryConfiguration2.setId(2L);
        assertThat(programSubCategoryConfiguration1).isNotEqualTo(programSubCategoryConfiguration2);
        programSubCategoryConfiguration1.setId(null);
        assertThat(programSubCategoryConfiguration1).isNotEqualTo(programSubCategoryConfiguration2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramSubCategoryConfigurationDTO.class);
        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO1 = new ProgramSubCategoryConfigurationDTO();
        programSubCategoryConfigurationDTO1.setId(1L);
        ProgramSubCategoryConfigurationDTO programSubCategoryConfigurationDTO2 = new ProgramSubCategoryConfigurationDTO();
        assertThat(programSubCategoryConfigurationDTO1).isNotEqualTo(programSubCategoryConfigurationDTO2);
        programSubCategoryConfigurationDTO2.setId(programSubCategoryConfigurationDTO1.getId());
        assertThat(programSubCategoryConfigurationDTO1).isEqualTo(programSubCategoryConfigurationDTO2);
        programSubCategoryConfigurationDTO2.setId(2L);
        assertThat(programSubCategoryConfigurationDTO1).isNotEqualTo(programSubCategoryConfigurationDTO2);
        programSubCategoryConfigurationDTO1.setId(null);
        assertThat(programSubCategoryConfigurationDTO1).isNotEqualTo(programSubCategoryConfigurationDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programSubCategoryConfigurationMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programSubCategoryConfigurationMapper.fromId(null)).isNull();
    }
}
