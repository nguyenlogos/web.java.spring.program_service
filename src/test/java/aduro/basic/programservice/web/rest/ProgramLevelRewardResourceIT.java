package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramLevelReward;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.repository.ProgramLevelRewardRepository;
import aduro.basic.programservice.service.ProgramLevelRewardService;
import aduro.basic.programservice.service.dto.ProgramLevelRewardDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelRewardMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramLevelRewardCriteria;
import aduro.basic.programservice.service.ProgramLevelRewardQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PostMapping;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramLevelRewardResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramLevelRewardResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_REWARD_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_REWARD_AMOUNT = new BigDecimal(2);

    private static final String DEFAULT_REWARD_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_REWARD_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_CAMPAIGN_ID = "AAAAAAAAAA";
    private static final String UPDATED_CAMPAIGN_ID = "BBBBBBBBBB";

    @Autowired
    private ProgramLevelRewardRepository programLevelRewardRepository;

    @Autowired
    private ProgramLevelRewardMapper programLevelRewardMapper;

    @Autowired
    private ProgramLevelRewardService programLevelRewardService;

    @Autowired
    private ProgramLevelRewardQueryService programLevelRewardQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramLevelRewardMockMvc;

    private ProgramLevelReward programLevelReward;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramLevelRewardResource programLevelRewardResource = new ProgramLevelRewardResource(programLevelRewardService, programLevelRewardQueryService);
        this.restProgramLevelRewardMockMvc = MockMvcBuilders.standaloneSetup(programLevelRewardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevelReward createEntity(EntityManager em) {
        ProgramLevelReward programLevelReward = new ProgramLevelReward()
            .description(DEFAULT_DESCRIPTION)
            .quantity(DEFAULT_QUANTITY)
            .code(DEFAULT_CODE)
            .rewardAmount(DEFAULT_REWARD_AMOUNT)
            .rewardType(DEFAULT_REWARD_TYPE)
            .campaignId(DEFAULT_CAMPAIGN_ID);
        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }
        programLevelReward.setProgramLevel(programLevel);
        return programLevelReward;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevelReward createUpdatedEntity(EntityManager em) {
        ProgramLevelReward programLevelReward = new ProgramLevelReward()
            .description(UPDATED_DESCRIPTION)
            .quantity(UPDATED_QUANTITY)
            .code(UPDATED_CODE)
            .rewardAmount(UPDATED_REWARD_AMOUNT)
            .rewardType(UPDATED_REWARD_TYPE)
            .campaignId(UPDATED_CAMPAIGN_ID);
        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createUpdatedEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }
        programLevelReward.setProgramLevel(programLevel);
        return programLevelReward;
    }

    @BeforeEach
    public void initTest() {
        programLevelReward = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramLevelReward() throws Exception {
        int databaseSizeBeforeCreate = programLevelRewardRepository.findAll().size();

        // Create the ProgramLevelReward
        ProgramLevelRewardDTO programLevelRewardDTO = programLevelRewardMapper.toDto(programLevelReward);
        restProgramLevelRewardMockMvc.perform(post("/api/program-level-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramLevelReward in the database
        List<ProgramLevelReward> programLevelRewardList = programLevelRewardRepository.findAll();
        assertThat(programLevelRewardList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramLevelReward testProgramLevelReward = programLevelRewardList.get(programLevelRewardList.size() - 1);
        assertThat(testProgramLevelReward.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProgramLevelReward.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testProgramLevelReward.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testProgramLevelReward.getRewardAmount()).isEqualTo(DEFAULT_REWARD_AMOUNT);
        assertThat(testProgramLevelReward.getRewardType()).isEqualTo(DEFAULT_REWARD_TYPE);
        assertThat(testProgramLevelReward.getCampaignId()).isEqualTo(DEFAULT_CAMPAIGN_ID);
    }

    @Test
    @Transactional
    public void createProgramLevelRewardWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programLevelRewardRepository.findAll().size();

        // Create the ProgramLevelReward with an existing ID
        programLevelReward.setId(1L);
        ProgramLevelRewardDTO programLevelRewardDTO = programLevelRewardMapper.toDto(programLevelReward);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramLevelRewardMockMvc.perform(post("/api/program-level-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevelReward in the database
        List<ProgramLevelReward> programLevelRewardList = programLevelRewardRepository.findAll();
        assertThat(programLevelRewardList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programLevelRewardRepository.findAll().size();
        // set the field null
        programLevelReward.setCode(null);

        // Create the ProgramLevelReward, which fails.
        ProgramLevelRewardDTO programLevelRewardDTO = programLevelRewardMapper.toDto(programLevelReward);

        restProgramLevelRewardMockMvc.perform(post("/api/program-level-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramLevelReward> programLevelRewardList = programLevelRewardRepository.findAll();
        assertThat(programLevelRewardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRewardAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = programLevelRewardRepository.findAll().size();
        // set the field null
        programLevelReward.setRewardAmount(null);

        // Create the ProgramLevelReward, which fails.
        ProgramLevelRewardDTO programLevelRewardDTO = programLevelRewardMapper.toDto(programLevelReward);

        restProgramLevelRewardMockMvc.perform(post("/api/program-level-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramLevelReward> programLevelRewardList = programLevelRewardRepository.findAll();
        assertThat(programLevelRewardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRewardTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programLevelRewardRepository.findAll().size();
        // set the field null
        programLevelReward.setRewardType(null);

        // Create the ProgramLevelReward, which fails.
        ProgramLevelRewardDTO programLevelRewardDTO = programLevelRewardMapper.toDto(programLevelReward);

        restProgramLevelRewardMockMvc.perform(post("/api/program-level-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramLevelReward> programLevelRewardList = programLevelRewardRepository.findAll();
        assertThat(programLevelRewardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewards() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList
        restProgramLevelRewardMockMvc.perform(get("/api/program-level-rewards?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevelReward.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].rewardAmount").value(hasItem(DEFAULT_REWARD_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].rewardType").value(hasItem(DEFAULT_REWARD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].campaignId").value(hasItem(DEFAULT_CAMPAIGN_ID.toString())));
    }

    @Test
    @Transactional
    public void getProgramLevelReward() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get the programLevelReward
        restProgramLevelRewardMockMvc.perform(get("/api/program-level-rewards/{id}", programLevelReward.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programLevelReward.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.rewardAmount").value(DEFAULT_REWARD_AMOUNT.intValue()))
            .andExpect(jsonPath("$.rewardType").value(DEFAULT_REWARD_TYPE.toString()))
            .andExpect(jsonPath("$.campaignId").value(DEFAULT_CAMPAIGN_ID.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where description equals to DEFAULT_DESCRIPTION
        defaultProgramLevelRewardShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the programLevelRewardList where description equals to UPDATED_DESCRIPTION
        defaultProgramLevelRewardShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultProgramLevelRewardShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the programLevelRewardList where description equals to UPDATED_DESCRIPTION
        defaultProgramLevelRewardShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where description is not null
        defaultProgramLevelRewardShouldBeFound("description.specified=true");

        // Get all the programLevelRewardList where description is null
        defaultProgramLevelRewardShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByQuantityIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where quantity equals to DEFAULT_QUANTITY
        defaultProgramLevelRewardShouldBeFound("quantity.equals=" + DEFAULT_QUANTITY);

        // Get all the programLevelRewardList where quantity equals to UPDATED_QUANTITY
        defaultProgramLevelRewardShouldNotBeFound("quantity.equals=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByQuantityIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where quantity in DEFAULT_QUANTITY or UPDATED_QUANTITY
        defaultProgramLevelRewardShouldBeFound("quantity.in=" + DEFAULT_QUANTITY + "," + UPDATED_QUANTITY);

        // Get all the programLevelRewardList where quantity equals to UPDATED_QUANTITY
        defaultProgramLevelRewardShouldNotBeFound("quantity.in=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByQuantityIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where quantity is not null
        defaultProgramLevelRewardShouldBeFound("quantity.specified=true");

        // Get all the programLevelRewardList where quantity is null
        defaultProgramLevelRewardShouldNotBeFound("quantity.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByQuantityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where quantity greater than or equals to DEFAULT_QUANTITY
        defaultProgramLevelRewardShouldBeFound("quantity.greaterOrEqualThan=" + DEFAULT_QUANTITY);

        // Get all the programLevelRewardList where quantity greater than or equals to UPDATED_QUANTITY
        defaultProgramLevelRewardShouldNotBeFound("quantity.greaterOrEqualThan=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByQuantityIsLessThanSomething() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where quantity less than or equals to DEFAULT_QUANTITY
        defaultProgramLevelRewardShouldNotBeFound("quantity.lessThan=" + DEFAULT_QUANTITY);

        // Get all the programLevelRewardList where quantity less than or equals to UPDATED_QUANTITY
        defaultProgramLevelRewardShouldBeFound("quantity.lessThan=" + UPDATED_QUANTITY);
    }


    @Test
    @Transactional
    public void getAllProgramLevelRewardsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where code equals to DEFAULT_CODE
        defaultProgramLevelRewardShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the programLevelRewardList where code equals to UPDATED_CODE
        defaultProgramLevelRewardShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where code in DEFAULT_CODE or UPDATED_CODE
        defaultProgramLevelRewardShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the programLevelRewardList where code equals to UPDATED_CODE
        defaultProgramLevelRewardShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where code is not null
        defaultProgramLevelRewardShouldBeFound("code.specified=true");

        // Get all the programLevelRewardList where code is null
        defaultProgramLevelRewardShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByRewardAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where rewardAmount equals to DEFAULT_REWARD_AMOUNT
        defaultProgramLevelRewardShouldBeFound("rewardAmount.equals=" + DEFAULT_REWARD_AMOUNT);

        // Get all the programLevelRewardList where rewardAmount equals to UPDATED_REWARD_AMOUNT
        defaultProgramLevelRewardShouldNotBeFound("rewardAmount.equals=" + UPDATED_REWARD_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByRewardAmountIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where rewardAmount in DEFAULT_REWARD_AMOUNT or UPDATED_REWARD_AMOUNT
        defaultProgramLevelRewardShouldBeFound("rewardAmount.in=" + DEFAULT_REWARD_AMOUNT + "," + UPDATED_REWARD_AMOUNT);

        // Get all the programLevelRewardList where rewardAmount equals to UPDATED_REWARD_AMOUNT
        defaultProgramLevelRewardShouldNotBeFound("rewardAmount.in=" + UPDATED_REWARD_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByRewardAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where rewardAmount is not null
        defaultProgramLevelRewardShouldBeFound("rewardAmount.specified=true");

        // Get all the programLevelRewardList where rewardAmount is null
        defaultProgramLevelRewardShouldNotBeFound("rewardAmount.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByRewardTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where rewardType equals to DEFAULT_REWARD_TYPE
        defaultProgramLevelRewardShouldBeFound("rewardType.equals=" + DEFAULT_REWARD_TYPE);

        // Get all the programLevelRewardList where rewardType equals to UPDATED_REWARD_TYPE
        defaultProgramLevelRewardShouldNotBeFound("rewardType.equals=" + UPDATED_REWARD_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByRewardTypeIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where rewardType in DEFAULT_REWARD_TYPE or UPDATED_REWARD_TYPE
        defaultProgramLevelRewardShouldBeFound("rewardType.in=" + DEFAULT_REWARD_TYPE + "," + UPDATED_REWARD_TYPE);

        // Get all the programLevelRewardList where rewardType equals to UPDATED_REWARD_TYPE
        defaultProgramLevelRewardShouldNotBeFound("rewardType.in=" + UPDATED_REWARD_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByRewardTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where rewardType is not null
        defaultProgramLevelRewardShouldBeFound("rewardType.specified=true");

        // Get all the programLevelRewardList where rewardType is null
        defaultProgramLevelRewardShouldNotBeFound("rewardType.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByCampaignIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where campaignId equals to DEFAULT_CAMPAIGN_ID
        defaultProgramLevelRewardShouldBeFound("campaignId.equals=" + DEFAULT_CAMPAIGN_ID);

        // Get all the programLevelRewardList where campaignId equals to UPDATED_CAMPAIGN_ID
        defaultProgramLevelRewardShouldNotBeFound("campaignId.equals=" + UPDATED_CAMPAIGN_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByCampaignIdIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where campaignId in DEFAULT_CAMPAIGN_ID or UPDATED_CAMPAIGN_ID
        defaultProgramLevelRewardShouldBeFound("campaignId.in=" + DEFAULT_CAMPAIGN_ID + "," + UPDATED_CAMPAIGN_ID);

        // Get all the programLevelRewardList where campaignId equals to UPDATED_CAMPAIGN_ID
        defaultProgramLevelRewardShouldNotBeFound("campaignId.in=" + UPDATED_CAMPAIGN_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByCampaignIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        // Get all the programLevelRewardList where campaignId is not null
        defaultProgramLevelRewardShouldBeFound("campaignId.specified=true");

        // Get all the programLevelRewardList where campaignId is null
        defaultProgramLevelRewardShouldNotBeFound("campaignId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelRewardsByProgramLevelIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramLevel programLevel = programLevelReward.getProgramLevel();
        programLevelRewardRepository.saveAndFlush(programLevelReward);
        Long programLevelId = programLevel.getId();

        // Get all the programLevelRewardList where programLevel equals to programLevelId
        defaultProgramLevelRewardShouldBeFound("programLevelId.equals=" + programLevelId);

        // Get all the programLevelRewardList where programLevel equals to programLevelId + 1
        defaultProgramLevelRewardShouldNotBeFound("programLevelId.equals=" + (programLevelId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramLevelRewardShouldBeFound(String filter) throws Exception {
        restProgramLevelRewardMockMvc.perform(get("/api/program-level-rewards?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevelReward.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].rewardAmount").value(hasItem(DEFAULT_REWARD_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].rewardType").value(hasItem(DEFAULT_REWARD_TYPE)))
            .andExpect(jsonPath("$.[*].campaignId").value(hasItem(DEFAULT_CAMPAIGN_ID)));

        // Check, that the count call also returns 1
        restProgramLevelRewardMockMvc.perform(get("/api/program-level-rewards/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramLevelRewardShouldNotBeFound(String filter) throws Exception {
        restProgramLevelRewardMockMvc.perform(get("/api/program-level-rewards?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramLevelRewardMockMvc.perform(get("/api/program-level-rewards/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramLevelReward() throws Exception {
        // Get the programLevelReward
        restProgramLevelRewardMockMvc.perform(get("/api/program-level-rewards/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramLevelReward() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        int databaseSizeBeforeUpdate = programLevelRewardRepository.findAll().size();

        // Update the programLevelReward
        ProgramLevelReward updatedProgramLevelReward = programLevelRewardRepository.findById(programLevelReward.getId()).get();
        // Disconnect from session so that the updates on updatedProgramLevelReward are not directly saved in db
        em.detach(updatedProgramLevelReward);
        updatedProgramLevelReward
            .description(UPDATED_DESCRIPTION)
            .quantity(UPDATED_QUANTITY)
            .code(UPDATED_CODE)
            .rewardAmount(UPDATED_REWARD_AMOUNT)
            .rewardType(UPDATED_REWARD_TYPE)
            .campaignId(UPDATED_CAMPAIGN_ID);
        ProgramLevelRewardDTO programLevelRewardDTO = programLevelRewardMapper.toDto(updatedProgramLevelReward);

        restProgramLevelRewardMockMvc.perform(put("/api/program-level-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramLevelReward in the database
        List<ProgramLevelReward> programLevelRewardList = programLevelRewardRepository.findAll();
        assertThat(programLevelRewardList).hasSize(databaseSizeBeforeUpdate);
        ProgramLevelReward testProgramLevelReward = programLevelRewardList.get(programLevelRewardList.size() - 1);
        assertThat(testProgramLevelReward.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
//        assertThat(testProgramLevelReward.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testProgramLevelReward.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testProgramLevelReward.getRewardAmount()).isEqualTo(UPDATED_REWARD_AMOUNT);
        assertThat(testProgramLevelReward.getRewardType()).isEqualTo(UPDATED_REWARD_TYPE);
        assertThat(testProgramLevelReward.getCampaignId()).isEqualTo(UPDATED_CAMPAIGN_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramLevelReward() throws Exception {
        int databaseSizeBeforeUpdate = programLevelRewardRepository.findAll().size();

        // Create the ProgramLevelReward
        ProgramLevelRewardDTO programLevelRewardDTO = programLevelRewardMapper.toDto(programLevelReward);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramLevelRewardMockMvc.perform(put("/api/program-level-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevelReward in the database
        List<ProgramLevelReward> programLevelRewardList = programLevelRewardRepository.findAll();
        assertThat(programLevelRewardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramLevelReward() throws Exception {
        // Initialize the database
        programLevelRewardRepository.saveAndFlush(programLevelReward);

        int databaseSizeBeforeDelete = programLevelRewardRepository.findAll().size();

        // Delete the programLevelReward
        restProgramLevelRewardMockMvc.perform(delete("/api/program-level-rewards/{id}", programLevelReward.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramLevelReward> programLevelRewardList = programLevelRewardRepository.findAll();
        assertThat(programLevelRewardList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelReward.class);
        ProgramLevelReward programLevelReward1 = new ProgramLevelReward();
        programLevelReward1.setId(1L);
        ProgramLevelReward programLevelReward2 = new ProgramLevelReward();
        programLevelReward2.setId(programLevelReward1.getId());
        assertThat(programLevelReward1).isEqualTo(programLevelReward2);
        programLevelReward2.setId(2L);
        assertThat(programLevelReward1).isNotEqualTo(programLevelReward2);
        programLevelReward1.setId(null);
        assertThat(programLevelReward1).isNotEqualTo(programLevelReward2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelRewardDTO.class);
        ProgramLevelRewardDTO programLevelRewardDTO1 = new ProgramLevelRewardDTO();
        programLevelRewardDTO1.setId(1L);
        ProgramLevelRewardDTO programLevelRewardDTO2 = new ProgramLevelRewardDTO();
        assertThat(programLevelRewardDTO1).isNotEqualTo(programLevelRewardDTO2);
        programLevelRewardDTO2.setId(programLevelRewardDTO1.getId());
        assertThat(programLevelRewardDTO1).isEqualTo(programLevelRewardDTO2);
        programLevelRewardDTO2.setId(2L);
        assertThat(programLevelRewardDTO1).isNotEqualTo(programLevelRewardDTO2);
        programLevelRewardDTO1.setId(null);
        assertThat(programLevelRewardDTO1).isNotEqualTo(programLevelRewardDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programLevelRewardMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programLevelRewardMapper.fromId(null)).isNull();
    }



    @Test
    @Transactional
    public void testUpdateRewardsForAllLevel()throws Exception {

        ProgramLevelRewardDTO programLevelRewardDTO1 = programLevelRewardMapper.toDto(programLevelReward);
        ProgramLevelRewardDTO programLevelRewardDTO2 = programLevelRewardMapper.toDto(programLevelReward);

        List<ProgramLevelRewardDTO> programLevelRewardDTOList = new ArrayList<>();
        programLevelRewardDTOList.add(programLevelRewardDTO1);
        programLevelRewardDTOList.add(programLevelRewardDTO2);

        restProgramLevelRewardMockMvc.perform(post("/api/program-level-rewards/1/update-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTOList)));
           // .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void testUpdateRewardsForOneLevel()throws Exception {

        ProgramLevelRewardDTO programLevelRewardDTO1 = programLevelRewardMapper.toDto(programLevelReward);
        ProgramLevelRewardDTO programLevelRewardDTO2 = programLevelRewardMapper.toDto(programLevelReward);

        List<ProgramLevelRewardDTO> programLevelRewardDTOList = new ArrayList<>();
        programLevelRewardDTOList.add(programLevelRewardDTO1);
        programLevelRewardDTOList.add(programLevelRewardDTO2);

        restProgramLevelRewardMockMvc.perform(post("/api/program-level-rewards/1/update-level-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelRewardDTOList)))
            .andExpect(status().isCreated());
    }

}
