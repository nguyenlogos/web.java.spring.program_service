package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.UserEvent;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.repository.UserEventRepository;
import aduro.basic.programservice.service.UserEventService;
import aduro.basic.programservice.service.dto.*;
import aduro.basic.programservice.service.mapper.UserEventMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.UserEventQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link UserEventResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class UserEventResourceIT {

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String DEFAULT_CLIENT_NAME = "AAAAAAAAAA";



    private static final String DEFAULT_EVENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_EVENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_EVENT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_EVENT_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final BigDecimal DEFAULT_EVENT_POINT = new BigDecimal(1);
    private static final BigDecimal UPDATED_EVENT_POINT = new BigDecimal(2);

    private static final String DEFAULT_EVENT_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_CATEGORY = "BBBBBBBBBB";

    @Autowired
    private UserEventRepository userEventRepository;

    @Autowired
    private UserEventMapper userEventMapper;

    @Autowired
    private UserEventService userEventService;

    @Autowired
    private UserEventQueryService userEventQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserEventMockMvc;

    private UserEvent userEvent;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserEventResource userEventResource = new UserEventResource(userEventService, userEventQueryService);
        this.restUserEventMockMvc = MockMvcBuilders.standaloneSetup(userEventResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();

        ClientProgramResourceIT.createEntity(em);
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEvent createEntity(EntityManager em) {
        UserEvent userEvent = new UserEvent()
            .eventCode(DEFAULT_EVENT_CODE)
            .eventId(DEFAULT_EVENT_ID)
            .eventDate(DEFAULT_EVENT_DATE)
            .eventPoint(DEFAULT_EVENT_POINT)
            .eventCategory(DEFAULT_EVENT_CATEGORY);

        // Add required entity
        ProgramUser programUser;

        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            program.setStatus(ProgramStatus.Active.toString());
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }

        ClientProgram clientProgram;
        if (TestUtil.findAll(em, ClientProgram.class).isEmpty()) {
            clientProgram = ClientProgramResourceIT.createEntity(em);
            em.persist(clientProgram);
            em.flush();
        } else {
            clientProgram = TestUtil.findAll(em, ClientProgram.class).get(0);
        }



        if (TestUtil.findAll(em, ProgramUser.class).isEmpty()) {
            programUser = ProgramUserResourceIT.createEntity(em);
            programUser.setProgramId(program.getId());

            programUser.setClientId(clientProgram.getClientId());
            em.persist(programUser);
            em.flush();
        } else {
            programUser = TestUtil.findAll(em, ProgramUser.class).get(0);
        }



        userEvent.setProgramUser(programUser);
        return userEvent;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserEvent createUpdatedEntity(EntityManager em) {
        UserEvent userEvent = new UserEvent()
            .eventCode(UPDATED_EVENT_CODE)
            .eventId(UPDATED_EVENT_ID)
            .eventDate(UPDATED_EVENT_DATE)
            .eventPoint(UPDATED_EVENT_POINT)
            .eventCategory(UPDATED_EVENT_CATEGORY);
        // Add required entity
        ProgramUser programUser;
        if (TestUtil.findAll(em, ProgramUser.class).isEmpty()) {
            programUser = ProgramUserResourceIT.createUpdatedEntity(em);
            em.persist(programUser);
            em.flush();
        } else {
            programUser = TestUtil.findAll(em, ProgramUser.class).get(0);
        }
        userEvent.setProgramUser(programUser);
        return userEvent;
    }

    @BeforeEach
    public void initTest() {
        userEvent = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserEvent() throws Exception {
        int databaseSizeBeforeCreate = userEventRepository.findAll().size();

        // Create the UserEvent
        UserEventDTO userEventDTO = userEventMapper.toDto(userEvent);
        restUserEventMockMvc.perform(post("/api/user-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventDTO)))
            .andExpect(status().isCreated());

        // Validate the UserEvent in the database
        List<UserEvent> userEventList = userEventRepository.findAll();
        assertThat(userEventList).hasSize(databaseSizeBeforeCreate + 1);
        UserEvent testUserEvent = userEventList.get(userEventList.size() - 1);
        assertThat(testUserEvent.getEventCode()).isEqualTo(DEFAULT_EVENT_CODE);
        assertThat(testUserEvent.getEventId()).isEqualTo(DEFAULT_EVENT_ID);
        assertThat(testUserEvent.getEventDate()).isEqualTo(DEFAULT_EVENT_DATE);
        assertThat(testUserEvent.getEventPoint()).isEqualTo(DEFAULT_EVENT_POINT);
        assertThat(testUserEvent.getEventCategory()).isEqualTo(DEFAULT_EVENT_CATEGORY);
    }

    @Test
    @Transactional
    public void createUserEventWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userEventRepository.findAll().size();

        // Create the UserEvent with an existing ID
        userEvent.setId(1L);
        UserEventDTO userEventDTO = userEventMapper.toDto(userEvent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserEventMockMvc.perform(post("/api/user-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserEvent in the database
        List<UserEvent> userEventList = userEventRepository.findAll();
        assertThat(userEventList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkEventCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventRepository.findAll().size();
        // set the field null
        userEvent.setEventCode(null);

        // Create the UserEvent, which fails.
        UserEventDTO userEventDTO = userEventMapper.toDto(userEvent);

        restUserEventMockMvc.perform(post("/api/user-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventDTO)))
            .andExpect(status().isBadRequest());

        List<UserEvent> userEventList = userEventRepository.findAll();
        assertThat(userEventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventRepository.findAll().size();
        // set the field null
        userEvent.setEventId(null);

        // Create the UserEvent, which fails.
        UserEventDTO userEventDTO = userEventMapper.toDto(userEvent);

        restUserEventMockMvc.perform(post("/api/user-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventDTO)))
            .andExpect(status().isBadRequest());

        List<UserEvent> userEventList = userEventRepository.findAll();
        assertThat(userEventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = userEventRepository.findAll().size();
        // set the field null
        userEvent.setEventDate(null);

        // Create the UserEvent, which fails.
        UserEventDTO userEventDTO = userEventMapper.toDto(userEvent);

        restUserEventMockMvc.perform(post("/api/user-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventDTO)))
            .andExpect(status().isBadRequest());

        List<UserEvent> userEventList = userEventRepository.findAll();
        assertThat(userEventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserEvents() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList
        restUserEventMockMvc.perform(get("/api/user-events?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userEvent.getId().intValue())))
            .andExpect(jsonPath("$.[*].eventCode").value(hasItem(DEFAULT_EVENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID.toString())))
            .andExpect(jsonPath("$.[*].eventDate").value(hasItem(DEFAULT_EVENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].eventPoint").value(hasItem(DEFAULT_EVENT_POINT.intValue())))
            .andExpect(jsonPath("$.[*].eventCategory").value(hasItem(DEFAULT_EVENT_CATEGORY.toString())));
    }

    @Test
    @Transactional
    public void getUserEvent() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get the userEvent
        restUserEventMockMvc.perform(get("/api/user-events/{id}", userEvent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userEvent.getId().intValue()))
            .andExpect(jsonPath("$.eventCode").value(DEFAULT_EVENT_CODE.toString()))
            .andExpect(jsonPath("$.eventId").value(DEFAULT_EVENT_ID.toString()))
            .andExpect(jsonPath("$.eventDate").value(DEFAULT_EVENT_DATE.toString()))
            .andExpect(jsonPath("$.eventPoint").value(DEFAULT_EVENT_POINT.intValue()))
            .andExpect(jsonPath("$.eventCategory").value(DEFAULT_EVENT_CATEGORY.toString()));
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventCode equals to DEFAULT_EVENT_CODE
        defaultUserEventShouldBeFound("eventCode.equals=" + DEFAULT_EVENT_CODE);

        // Get all the userEventList where eventCode equals to UPDATED_EVENT_CODE
        defaultUserEventShouldNotBeFound("eventCode.equals=" + UPDATED_EVENT_CODE);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventCodeIsInShouldWork() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventCode in DEFAULT_EVENT_CODE or UPDATED_EVENT_CODE
        defaultUserEventShouldBeFound("eventCode.in=" + DEFAULT_EVENT_CODE + "," + UPDATED_EVENT_CODE);

        // Get all the userEventList where eventCode equals to UPDATED_EVENT_CODE
        defaultUserEventShouldNotBeFound("eventCode.in=" + UPDATED_EVENT_CODE);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventCode is not null
        defaultUserEventShouldBeFound("eventCode.specified=true");

        // Get all the userEventList where eventCode is null
        defaultUserEventShouldNotBeFound("eventCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventId equals to DEFAULT_EVENT_ID
        defaultUserEventShouldBeFound("eventId.equals=" + DEFAULT_EVENT_ID);

        // Get all the userEventList where eventId equals to UPDATED_EVENT_ID
        defaultUserEventShouldNotBeFound("eventId.equals=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventIdIsInShouldWork() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventId in DEFAULT_EVENT_ID or UPDATED_EVENT_ID
        defaultUserEventShouldBeFound("eventId.in=" + DEFAULT_EVENT_ID + "," + UPDATED_EVENT_ID);

        // Get all the userEventList where eventId equals to UPDATED_EVENT_ID
        defaultUserEventShouldNotBeFound("eventId.in=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventId is not null
        defaultUserEventShouldBeFound("eventId.specified=true");

        // Get all the userEventList where eventId is null
        defaultUserEventShouldNotBeFound("eventId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventDateIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventDate equals to DEFAULT_EVENT_DATE
        defaultUserEventShouldBeFound("eventDate.equals=" + DEFAULT_EVENT_DATE);

        // Get all the userEventList where eventDate equals to UPDATED_EVENT_DATE
        defaultUserEventShouldNotBeFound("eventDate.equals=" + UPDATED_EVENT_DATE);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventDateIsInShouldWork() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventDate in DEFAULT_EVENT_DATE or UPDATED_EVENT_DATE
        defaultUserEventShouldBeFound("eventDate.in=" + DEFAULT_EVENT_DATE + "," + UPDATED_EVENT_DATE);

        // Get all the userEventList where eventDate equals to UPDATED_EVENT_DATE
        defaultUserEventShouldNotBeFound("eventDate.in=" + UPDATED_EVENT_DATE);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventDate is not null
        defaultUserEventShouldBeFound("eventDate.specified=true");

        // Get all the userEventList where eventDate is null
        defaultUserEventShouldNotBeFound("eventDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventPointIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventPoint equals to DEFAULT_EVENT_POINT
        defaultUserEventShouldBeFound("eventPoint.equals=" + DEFAULT_EVENT_POINT);

        // Get all the userEventList where eventPoint equals to UPDATED_EVENT_POINT
        defaultUserEventShouldNotBeFound("eventPoint.equals=" + UPDATED_EVENT_POINT);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventPointIsInShouldWork() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventPoint in DEFAULT_EVENT_POINT or UPDATED_EVENT_POINT
        defaultUserEventShouldBeFound("eventPoint.in=" + DEFAULT_EVENT_POINT + "," + UPDATED_EVENT_POINT);

        // Get all the userEventList where eventPoint equals to UPDATED_EVENT_POINT
        defaultUserEventShouldNotBeFound("eventPoint.in=" + UPDATED_EVENT_POINT);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventPoint is not null
        defaultUserEventShouldBeFound("eventPoint.specified=true");

        // Get all the userEventList where eventPoint is null
        defaultUserEventShouldNotBeFound("eventPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventCategory equals to DEFAULT_EVENT_CATEGORY
        defaultUserEventShouldBeFound("eventCategory.equals=" + DEFAULT_EVENT_CATEGORY);

        // Get all the userEventList where eventCategory equals to UPDATED_EVENT_CATEGORY
        defaultUserEventShouldNotBeFound("eventCategory.equals=" + UPDATED_EVENT_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventCategoryIsInShouldWork() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventCategory in DEFAULT_EVENT_CATEGORY or UPDATED_EVENT_CATEGORY
        defaultUserEventShouldBeFound("eventCategory.in=" + DEFAULT_EVENT_CATEGORY + "," + UPDATED_EVENT_CATEGORY);

        // Get all the userEventList where eventCategory equals to UPDATED_EVENT_CATEGORY
        defaultUserEventShouldNotBeFound("eventCategory.in=" + UPDATED_EVENT_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllUserEventsByEventCategoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        // Get all the userEventList where eventCategory is not null
        defaultUserEventShouldBeFound("eventCategory.specified=true");

        // Get all the userEventList where eventCategory is null
        defaultUserEventShouldNotBeFound("eventCategory.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserEventsByProgramUserIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramUser programUser = userEvent.getProgramUser();
        userEventRepository.saveAndFlush(userEvent);
        Long programUserId = programUser.getId();

        // Get all the userEventList where programUser equals to programUserId
        defaultUserEventShouldBeFound("programUserId.equals=" + programUserId);

        // Get all the userEventList where programUser equals to programUserId + 1
        defaultUserEventShouldNotBeFound("programUserId.equals=" + (programUserId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserEventShouldBeFound(String filter) throws Exception {
        restUserEventMockMvc.perform(get("/api/user-events?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userEvent.getId().intValue())))
            .andExpect(jsonPath("$.[*].eventCode").value(hasItem(DEFAULT_EVENT_CODE)))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID)))
            .andExpect(jsonPath("$.[*].eventDate").value(hasItem(DEFAULT_EVENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].eventPoint").value(hasItem(DEFAULT_EVENT_POINT.intValue())))
            .andExpect(jsonPath("$.[*].eventCategory").value(hasItem(DEFAULT_EVENT_CATEGORY)));

        // Check, that the count call also returns 1
        restUserEventMockMvc.perform(get("/api/user-events/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserEventShouldNotBeFound(String filter) throws Exception {
        restUserEventMockMvc.perform(get("/api/user-events?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserEventMockMvc.perform(get("/api/user-events/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUserEvent() throws Exception {
        // Get the userEvent
        restUserEventMockMvc.perform(get("/api/user-events/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserEvent() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        int databaseSizeBeforeUpdate = userEventRepository.findAll().size();

        // Update the userEvent
        UserEvent updatedUserEvent = userEventRepository.findById(userEvent.getId()).get();
        // Disconnect from session so that the updates on updatedUserEvent are not directly saved in db
        em.detach(updatedUserEvent);
        updatedUserEvent
            .eventCode(UPDATED_EVENT_CODE)
            .eventId(UPDATED_EVENT_ID)
            .eventDate(UPDATED_EVENT_DATE)
            .eventPoint(UPDATED_EVENT_POINT)
            .eventCategory(UPDATED_EVENT_CATEGORY);
        UserEventDTO userEventDTO = userEventMapper.toDto(updatedUserEvent);

        restUserEventMockMvc.perform(put("/api/user-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventDTO)))
            .andExpect(status().isOk());

        // Validate the UserEvent in the database
        List<UserEvent> userEventList = userEventRepository.findAll();
        assertThat(userEventList).hasSize(databaseSizeBeforeUpdate);
        UserEvent testUserEvent = userEventList.get(userEventList.size() - 1);
        assertThat(testUserEvent.getEventCode()).isEqualTo(UPDATED_EVENT_CODE);
        assertThat(testUserEvent.getEventId()).isEqualTo(UPDATED_EVENT_ID);
        assertThat(testUserEvent.getEventDate()).isEqualTo(UPDATED_EVENT_DATE);
        assertThat(testUserEvent.getEventPoint()).isEqualTo(UPDATED_EVENT_POINT);
        assertThat(testUserEvent.getEventCategory()).isEqualTo(UPDATED_EVENT_CATEGORY);
    }

    @Test
    @Transactional
    public void updateNonExistingUserEvent() throws Exception {
        int databaseSizeBeforeUpdate = userEventRepository.findAll().size();

        // Create the UserEvent
        UserEventDTO userEventDTO = userEventMapper.toDto(userEvent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserEventMockMvc.perform(put("/api/user-events")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userEventDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserEvent in the database
        List<UserEvent> userEventList = userEventRepository.findAll();
        assertThat(userEventList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserEvent() throws Exception {
        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);

        int databaseSizeBeforeDelete = userEventRepository.findAll().size();

        // Delete the userEvent
        restUserEventMockMvc.perform(delete("/api/user-events/{id}", userEvent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<UserEvent> userEventList = userEventRepository.findAll();
        assertThat(userEventList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserEvent.class);
        UserEvent userEvent1 = new UserEvent();
        userEvent1.setId(1L);
        UserEvent userEvent2 = new UserEvent();
        userEvent2.setId(userEvent1.getId());
        assertThat(userEvent1).isEqualTo(userEvent2);
        userEvent2.setId(2L);
        assertThat(userEvent1).isNotEqualTo(userEvent2);
        userEvent1.setId(null);
        assertThat(userEvent1).isNotEqualTo(userEvent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserEventDTO.class);
        UserEventDTO userEventDTO1 = new UserEventDTO();
        userEventDTO1.setId(1L);
        UserEventDTO userEventDTO2 = new UserEventDTO();
        assertThat(userEventDTO1).isNotEqualTo(userEventDTO2);
        userEventDTO2.setId(userEventDTO1.getId());
        assertThat(userEventDTO1).isEqualTo(userEventDTO2);
        userEventDTO2.setId(2L);
        assertThat(userEventDTO1).isNotEqualTo(userEventDTO2);
        userEventDTO1.setId(null);
        assertThat(userEventDTO1).isNotEqualTo(userEventDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userEventMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userEventMapper.fromId(null)).isNull();
    }

    @Test
    @Transactional
    public void handleIncentiveEvent() throws Exception {

        ProgramUser programUser = userEvent.getProgramUser();

        // Initialize the database
        userEventRepository.saveAndFlush(userEvent);



        ActivityEventDTO dto = new ActivityEventDTO();
        dto.setClientId(programUser.getClientId());
        dto.setClientName(DEFAULT_CLIENT_NAME);
        dto.setEventCode("PASSIVE_TRACKING");
        dto.setEventId("activity1");
        dto.setEventDate(Instant.now());
        dto.setProgramId(programUser.getProgramId());

        List<ParticipantDTO> participantDTOList = new ArrayList<>();

        ParticipantDTO participantDTO = new ParticipantDTO();
        participantDTO.setEmail("aa");
        participantDTO.setFirstName("aa");
        participantDTO.setLastName("aa");
        participantDTO.setParticipantId("aa");
        participantDTOList.add(participantDTO);
        dto.setParticipants(participantDTOList);

        restUserEventMockMvc.perform(post("/api/user-events/incentives")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dto)));

    }
}
