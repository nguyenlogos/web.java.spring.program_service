package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramCategoryPoint;
import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.repository.ProgramRepository;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.ProgramDTO;
import aduro.basic.programservice.service.mapper.ProgramMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramCriteria;
import aduro.basic.programservice.service.ProgramQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_LAST_SENT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_LAST_SENT = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_IS_RETRIGGER_EMAIL = false;
    private static final Boolean UPDATED_IS_RETRIGGER_EMAIL = true;

    private static final Boolean DEFAULT_IS_ELIGIBLE = false;
    private static final Boolean UPDATED_IS_ELIGIBLE = true;

    private static final Boolean DEFAULT_IS_SENT_REGISTRATION_EMAIL = false;
    private static final Boolean UPDATED_IS_SENT_REGISTRATION_EMAIL = true;

    private static final Boolean DEFAULT_IS_REGISTERED_FOR_PLATFORM = false;
    private static final Boolean UPDATED_IS_REGISTERED_FOR_PLATFORM = true;

    private static final Boolean DEFAULT_IS_SCHEDULED_SCREENING = false;
    private static final Boolean UPDATED_IS_SCHEDULED_SCREENING = true;

    private static final Boolean DEFAULT_IS_FUNCTIONALLY = false;
    private static final Boolean UPDATED_IS_FUNCTIONALLY = true;

    private static final String DEFAULT_LOGO_URL = "AAAAAAAAAA";
    private static final String UPDATED_LOGO_URL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_USER_POINT = new BigDecimal(1);
    private static final BigDecimal UPDATED_USER_POINT = new BigDecimal(2);

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_USE_POINT = false;
    private static final Boolean UPDATED_IS_USE_POINT = true;

    private static final Boolean DEFAULT_IS_SCREEN = false;
    private static final Boolean UPDATED_IS_SCREEN = true;

    private static final Boolean DEFAULT_IS_COACHING = false;
    private static final Boolean UPDATED_IS_COACHING = true;

    private static final Boolean DEFAULT_IS_WELL_MATRIC = false;
    private static final Boolean UPDATED_IS_WELL_MATRIC = true;

    private static final Boolean DEFAULT_IS_HP = false;
    private static final Boolean UPDATED_IS_HP = true;

    private static final Boolean DEFAULT_IS_USE_LEVEL = false;
    private static final Boolean UPDATED_IS_USE_LEVEL = true;

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_RESET_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_RESET_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_TEMPLATE = false;
    private static final Boolean UPDATED_IS_TEMPLATE = true;

    private static final Boolean DEFAULT_IS_PREVIEW = false;
    private static final Boolean UPDATED_IS_PREVIEW = true;

    private static final Instant DEFAULT_PREVIEW_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PREVIEW_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_PREVIEW_TIME_ZONE = "AAAAAAAAAA";
    private static final String UPDATED_PREVIEW_TIME_ZONE = "BBBBBBBBBB";

    private static final String DEFAULT_START_TIME_ZONE = "AAAAAAAAAA";
    private static final String UPDATED_START_TIME_ZONE = "BBBBBBBBBB";

    private static final String DEFAULT_END_TIME_ZONE = "AAAAAAAAAA";
    private static final String UPDATED_END_TIME_ZONE = "BBBBBBBBBB";

    private static final String DEFAULT_LEVEL_STRUCTURE = "AAAAAAAAAA";
    private static final String UPDATED_LEVEL_STRUCTURE = "BBBBBBBBBB";

    private static final Integer DEFAULT_PROGRAM_LENGTH = 1;
    private static final Integer UPDATED_PROGRAM_LENGTH = 2;

    private static final Boolean DEFAULT_IS_HPSF = false;
    private static final Boolean UPDATED_IS_HPSF = true;

    private static final Boolean DEFAULT_IS_HTK = false;
    private static final Boolean UPDATED_IS_HTK = true;

    private static final Boolean DEFAULT_IS_LABCORP = false;
    private static final Boolean UPDATED_IS_LABCORP = true;

    private static final String DEFAULT_LABCORP_ACCOUNT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_LABCORP_ACCOUNT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_LABCORP_FILE_URL = "AAAAAAAAAA";
    private static final String UPDATED_LABCORP_FILE_URL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_APPLY_REWARD_ALL_SUBGROUP = false;
    private static final Boolean UPDATED_APPLY_REWARD_ALL_SUBGROUP = true;

    private static final Instant DEFAULT_BIOMETRIC_DEADLINE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BIOMETRIC_DEADLINE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_BIOMETRIC_LOOKBACK_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BIOMETRIC_LOOKBACK_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LANDING_BACKGROUND_IMAGE_URL = "AAAAAAAAAA";
    private static final String UPDATED_LANDING_BACKGROUND_IMAGE_URL = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT = false;
    private static final Boolean UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT = true;

    private static final Instant DEFAULT_TOBACCO_ATTESTATION_DEADLINE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TOBACCO_ATTESTATION_DEADLINE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_TOBACCO_PROGRAM_DEADLINE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TOBACCO_PROGRAM_DEADLINE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION = false;
    private static final Boolean UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION = true;

    private static final Boolean DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS = false;
    private static final Boolean UPDATED_IS_AWARDED_FOR_TOBACCO_RAS = true;

    @Autowired
    private ProgramRepository programRepository;

    @Autowired
    private ProgramMapper programMapper;

    @Autowired
    private ProgramService programService;

    @Autowired
    private ProgramQueryService programQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramMockMvc;

    private Program program;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramResource programResource = new ProgramResource(programService, programQueryService);
        this.restProgramMockMvc = MockMvcBuilders.standaloneSetup(programResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Program createEntity(EntityManager em) {
        Program program = new Program()
            .name(DEFAULT_NAME)
            .lastSent(DEFAULT_LAST_SENT)
            .isRetriggerEmail(DEFAULT_IS_RETRIGGER_EMAIL)
            .isEligible(DEFAULT_IS_ELIGIBLE)
            .isSentRegistrationEmail(DEFAULT_IS_SENT_REGISTRATION_EMAIL)
            .isRegisteredForPlatform(DEFAULT_IS_REGISTERED_FOR_PLATFORM)
            .isScheduledScreening(DEFAULT_IS_SCHEDULED_SCREENING)
            .isFunctionally(DEFAULT_IS_FUNCTIONALLY)
            .logoUrl(DEFAULT_LOGO_URL)
            .description(DEFAULT_DESCRIPTION)
            .userPoint(DEFAULT_USER_POINT)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .createdBy(DEFAULT_CREATED_BY)
            .status(DEFAULT_STATUS)
            .isUsePoint(DEFAULT_IS_USE_POINT)
            .isScreen(DEFAULT_IS_SCREEN)
            .isCoaching(DEFAULT_IS_COACHING)
            .isWellMatric(DEFAULT_IS_WELL_MATRIC)
            .isHP(DEFAULT_IS_HP)
            .isUseLevel(DEFAULT_IS_USE_LEVEL)
            .startDate(DEFAULT_START_DATE)
            .resetDate(DEFAULT_RESET_DATE)
            .isTemplate(DEFAULT_IS_TEMPLATE)
            .isPreview(DEFAULT_IS_PREVIEW)
            .previewDate(DEFAULT_PREVIEW_DATE)
            .previewTimeZone(DEFAULT_PREVIEW_TIME_ZONE)
            .startTimeZone(DEFAULT_START_TIME_ZONE)
            .endTimeZone(DEFAULT_END_TIME_ZONE)
            .levelStructure(DEFAULT_LEVEL_STRUCTURE)
            .programLength(DEFAULT_PROGRAM_LENGTH)
            .isHPSF(DEFAULT_IS_HPSF)
            .isHTK(DEFAULT_IS_HTK)
            .isLabcorp(DEFAULT_IS_LABCORP)
            .labcorpAccountNumber(DEFAULT_LABCORP_ACCOUNT_NUMBER)
            .labcorpFileUrl(DEFAULT_LABCORP_FILE_URL)
            .applyRewardAllSubgroup(DEFAULT_APPLY_REWARD_ALL_SUBGROUP)
            .biometricDeadlineDate(DEFAULT_BIOMETRIC_DEADLINE_DATE)
            .biometricLookbackDate(DEFAULT_BIOMETRIC_LOOKBACK_DATE)
            .landingBackgroundImageUrl(DEFAULT_LANDING_BACKGROUND_IMAGE_URL)
            .isTobaccoSurchargeManagement(DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT)
            .tobaccoAttestationDeadline(DEFAULT_TOBACCO_ATTESTATION_DEADLINE)
            .tobaccoProgramDeadline(DEFAULT_TOBACCO_PROGRAM_DEADLINE)
            .isAwardedForTobaccoAttestation(DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION)
            .isAwardedForTobaccoRas(DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS);
        return program;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Program createUpdatedEntity(EntityManager em) {
        Program program = new Program()
            .name(UPDATED_NAME)
            .lastSent(UPDATED_LAST_SENT)
            .isRetriggerEmail(UPDATED_IS_RETRIGGER_EMAIL)
            .isEligible(UPDATED_IS_ELIGIBLE)
            .isSentRegistrationEmail(UPDATED_IS_SENT_REGISTRATION_EMAIL)
            .isRegisteredForPlatform(UPDATED_IS_REGISTERED_FOR_PLATFORM)
            .isScheduledScreening(UPDATED_IS_SCHEDULED_SCREENING)
            .isFunctionally(UPDATED_IS_FUNCTIONALLY)
            .logoUrl(UPDATED_LOGO_URL)
            .description(UPDATED_DESCRIPTION)
            .userPoint(UPDATED_USER_POINT)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .createdBy(UPDATED_CREATED_BY)
            .status(UPDATED_STATUS)
            .isUsePoint(UPDATED_IS_USE_POINT)
            .isScreen(UPDATED_IS_SCREEN)
            .isCoaching(UPDATED_IS_COACHING)
            .isWellMatric(UPDATED_IS_WELL_MATRIC)
            .isHP(UPDATED_IS_HP)
            .isUseLevel(UPDATED_IS_USE_LEVEL)
            .startDate(UPDATED_START_DATE)
            .resetDate(UPDATED_RESET_DATE)
            .isTemplate(UPDATED_IS_TEMPLATE)
            .isPreview(UPDATED_IS_PREVIEW)
            .previewDate(UPDATED_PREVIEW_DATE)
            .previewTimeZone(UPDATED_PREVIEW_TIME_ZONE)
            .startTimeZone(UPDATED_START_TIME_ZONE)
            .endTimeZone(UPDATED_END_TIME_ZONE)
            .levelStructure(UPDATED_LEVEL_STRUCTURE)
            .programLength(UPDATED_PROGRAM_LENGTH)
            .isHPSF(UPDATED_IS_HPSF)
            .isHTK(UPDATED_IS_HTK)
            .isLabcorp(UPDATED_IS_LABCORP)
            .labcorpAccountNumber(UPDATED_LABCORP_ACCOUNT_NUMBER)
            .labcorpFileUrl(UPDATED_LABCORP_FILE_URL)
            .applyRewardAllSubgroup(UPDATED_APPLY_REWARD_ALL_SUBGROUP)
            .biometricDeadlineDate(UPDATED_BIOMETRIC_DEADLINE_DATE)
            .biometricLookbackDate(UPDATED_BIOMETRIC_LOOKBACK_DATE)
            .landingBackgroundImageUrl(UPDATED_LANDING_BACKGROUND_IMAGE_URL)
            .isTobaccoSurchargeManagement(UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT)
            .tobaccoAttestationDeadline(UPDATED_TOBACCO_ATTESTATION_DEADLINE)
            .tobaccoProgramDeadline(UPDATED_TOBACCO_PROGRAM_DEADLINE)
            .isAwardedForTobaccoAttestation(UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION)
            .isAwardedForTobaccoRas(UPDATED_IS_AWARDED_FOR_TOBACCO_RAS);
        return program;
    }

    @BeforeEach
    public void initTest() {
        program = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgram() throws Exception {
        int databaseSizeBeforeCreate = programRepository.findAll().size();

        // Create the Program
        ProgramDTO programDTO = programMapper.toDto(program);
        restProgramMockMvc.perform(post("/api/programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programDTO)))
            .andExpect(status().isCreated());

        // Validate the Program in the database
        List<Program> programList = programRepository.findAll();
        assertThat(programList).hasSize(databaseSizeBeforeCreate + 1);
        Program testProgram = programList.get(programList.size() - 1);
        assertThat(testProgram.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProgram.getLastSent()).isEqualTo(DEFAULT_LAST_SENT);
        assertThat(testProgram.isIsRetriggerEmail()).isEqualTo(DEFAULT_IS_RETRIGGER_EMAIL);
        assertThat(testProgram.isIsEligible()).isEqualTo(DEFAULT_IS_ELIGIBLE);
        assertThat(testProgram.isIsSentRegistrationEmail()).isEqualTo(DEFAULT_IS_SENT_REGISTRATION_EMAIL);
        assertThat(testProgram.isIsRegisteredForPlatform()).isEqualTo(DEFAULT_IS_REGISTERED_FOR_PLATFORM);
        assertThat(testProgram.isIsScheduledScreening()).isEqualTo(DEFAULT_IS_SCHEDULED_SCREENING);
        assertThat(testProgram.isIsFunctionally()).isEqualTo(DEFAULT_IS_FUNCTIONALLY);
        assertThat(testProgram.getLogoUrl()).isEqualTo(DEFAULT_LOGO_URL);
        assertThat(testProgram.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProgram.getUserPoint()).isEqualTo(DEFAULT_USER_POINT);
        assertThat(testProgram.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testProgram.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgram.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testProgram.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testProgram.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testProgram.isIsUsePoint()).isEqualTo(DEFAULT_IS_USE_POINT);
        assertThat(testProgram.isIsScreen()).isEqualTo(DEFAULT_IS_SCREEN);
        assertThat(testProgram.isIsCoaching()).isEqualTo(DEFAULT_IS_COACHING);
        assertThat(testProgram.isIsWellMatric()).isEqualTo(DEFAULT_IS_WELL_MATRIC);
        assertThat(testProgram.isIsHp()).isEqualTo(DEFAULT_IS_HP);
        assertThat(testProgram.isIsUseLevel()).isEqualTo(DEFAULT_IS_USE_LEVEL);
        assertThat(testProgram.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testProgram.getResetDate()).isEqualTo(DEFAULT_RESET_DATE);
        assertThat(testProgram.isIsTemplate()).isEqualTo(DEFAULT_IS_TEMPLATE);
        assertThat(testProgram.isIsPreview()).isEqualTo(DEFAULT_IS_PREVIEW);
        assertThat(testProgram.getPreviewDate()).isEqualTo(DEFAULT_PREVIEW_DATE);
        assertThat(testProgram.getPreviewTimeZone()).isEqualTo(DEFAULT_PREVIEW_TIME_ZONE);
        assertThat(testProgram.getStartTimeZone()).isEqualTo(DEFAULT_START_TIME_ZONE);
        assertThat(testProgram.getEndTimeZone()).isEqualTo(DEFAULT_END_TIME_ZONE);
        assertThat(testProgram.getLevelStructure()).isEqualTo(DEFAULT_LEVEL_STRUCTURE);
        assertThat(testProgram.getProgramLength()).isEqualTo(DEFAULT_PROGRAM_LENGTH);
        assertThat(testProgram.isIsHPSF()).isEqualTo(DEFAULT_IS_HPSF);
        assertThat(testProgram.isIsHTK()).isEqualTo(DEFAULT_IS_HTK);
        assertThat(testProgram.isIsLabcorp()).isEqualTo(DEFAULT_IS_LABCORP);
        assertThat(testProgram.getLabcorpAccountNumber()).isEqualTo(DEFAULT_LABCORP_ACCOUNT_NUMBER);
        assertThat(testProgram.getLabcorpFileUrl()).isEqualTo(DEFAULT_LABCORP_FILE_URL);
        assertThat(testProgram.isApplyRewardAllSubgroup()).isEqualTo(DEFAULT_APPLY_REWARD_ALL_SUBGROUP);
        assertThat(testProgram.getBiometricDeadlineDate()).isEqualTo(DEFAULT_BIOMETRIC_DEADLINE_DATE);
        assertThat(testProgram.getBiometricLookbackDate()).isEqualTo(DEFAULT_BIOMETRIC_LOOKBACK_DATE);
        assertThat(testProgram.getLandingBackgroundImageUrl()).isEqualTo(DEFAULT_LANDING_BACKGROUND_IMAGE_URL);
        assertThat(testProgram.isIsTobaccoSurchargeManagement()).isEqualTo(DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT);
        assertThat(testProgram.getTobaccoAttestationDeadline()).isEqualTo(DEFAULT_TOBACCO_ATTESTATION_DEADLINE);
        assertThat(testProgram.getTobaccoProgramDeadline()).isEqualTo(DEFAULT_TOBACCO_PROGRAM_DEADLINE);
        assertThat(testProgram.isIsAwardedForTobaccoAttestation()).isEqualTo(DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION);
        assertThat(testProgram.isIsAwardedForTobaccoRas()).isEqualTo(DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS);
    }

    @Test
    @Transactional
    public void createProgramWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programRepository.findAll().size();

        // Create the Program with an existing ID
        program.setId(1L);
        ProgramDTO programDTO = programMapper.toDto(program);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramMockMvc.perform(post("/api/programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Program in the database
        List<Program> programList = programRepository.findAll();
        assertThat(programList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = programRepository.findAll().size();
        // set the field null
        program.setName(null);

        // Create the Program, which fails.
        ProgramDTO programDTO = programMapper.toDto(program);

        restProgramMockMvc.perform(post("/api/programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programDTO)))
            .andExpect(status().isBadRequest());

        List<Program> programList = programRepository.findAll();
        assertThat(programList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIsTemplateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programRepository.findAll().size();
        // set the field null
        program.setIsTemplate(null);

        // Create the Program, which fails.
        ProgramDTO programDTO = programMapper.toDto(program);

        restProgramMockMvc.perform(post("/api/programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programDTO)))
            .andExpect(status().isBadRequest());

        List<Program> programList = programRepository.findAll();
        assertThat(programList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPrograms() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList
        restProgramMockMvc.perform(get("/api/programs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(program.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastSent").value(hasItem(DEFAULT_LAST_SENT.toString())))
            .andExpect(jsonPath("$.[*].isRetriggerEmail").value(hasItem(DEFAULT_IS_RETRIGGER_EMAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].isEligible").value(hasItem(DEFAULT_IS_ELIGIBLE.booleanValue())))
            .andExpect(jsonPath("$.[*].isSentRegistrationEmail").value(hasItem(DEFAULT_IS_SENT_REGISTRATION_EMAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].isRegisteredForPlatform").value(hasItem(DEFAULT_IS_REGISTERED_FOR_PLATFORM.booleanValue())))
            .andExpect(jsonPath("$.[*].isScheduledScreening").value(hasItem(DEFAULT_IS_SCHEDULED_SCREENING.booleanValue())))
            .andExpect(jsonPath("$.[*].isFunctionally").value(hasItem(DEFAULT_IS_FUNCTIONALLY.booleanValue())))
            .andExpect(jsonPath("$.[*].logoUrl").value(hasItem(DEFAULT_LOGO_URL.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].userPoint").value(hasItem(DEFAULT_USER_POINT.intValue())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].isUsePoint").value(hasItem(DEFAULT_IS_USE_POINT.booleanValue())))
            .andExpect(jsonPath("$.[*].isScreen").value(hasItem(DEFAULT_IS_SCREEN.booleanValue())))
            .andExpect(jsonPath("$.[*].isCoaching").value(hasItem(DEFAULT_IS_COACHING.booleanValue())))
            .andExpect(jsonPath("$.[*].isWellMatric").value(hasItem(DEFAULT_IS_WELL_MATRIC.booleanValue())))
            .andExpect(jsonPath("$.[*].isHP").value(hasItem(DEFAULT_IS_HP.booleanValue())))
            .andExpect(jsonPath("$.[*].isUseLevel").value(hasItem(DEFAULT_IS_USE_LEVEL.booleanValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].resetDate").value(hasItem(DEFAULT_RESET_DATE.toString())))
            .andExpect(jsonPath("$.[*].isTemplate").value(hasItem(DEFAULT_IS_TEMPLATE.booleanValue())))
            .andExpect(jsonPath("$.[*].isPreview").value(hasItem(DEFAULT_IS_PREVIEW.booleanValue())))
            .andExpect(jsonPath("$.[*].previewDate").value(hasItem(DEFAULT_PREVIEW_DATE.toString())))
            .andExpect(jsonPath("$.[*].previewTimeZone").value(hasItem(DEFAULT_PREVIEW_TIME_ZONE.toString())))
            .andExpect(jsonPath("$.[*].startTimeZone").value(hasItem(DEFAULT_START_TIME_ZONE.toString())))
            .andExpect(jsonPath("$.[*].endTimeZone").value(hasItem(DEFAULT_END_TIME_ZONE.toString())))
            .andExpect(jsonPath("$.[*].levelStructure").value(hasItem(DEFAULT_LEVEL_STRUCTURE.toString())))
            .andExpect(jsonPath("$.[*].programLength").value(hasItem(DEFAULT_PROGRAM_LENGTH)))
            .andExpect(jsonPath("$.[*].isHPSF").value(hasItem(DEFAULT_IS_HPSF.booleanValue())))
            .andExpect(jsonPath("$.[*].isHTK").value(hasItem(DEFAULT_IS_HTK.booleanValue())))
            .andExpect(jsonPath("$.[*].isLabcorp").value(hasItem(DEFAULT_IS_LABCORP.booleanValue())))
            .andExpect(jsonPath("$.[*].labcorpAccountNumber").value(hasItem(DEFAULT_LABCORP_ACCOUNT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].labcorpFileUrl").value(hasItem(DEFAULT_LABCORP_FILE_URL.toString())))
            .andExpect(jsonPath("$.[*].applyRewardAllSubgroup").value(hasItem(DEFAULT_APPLY_REWARD_ALL_SUBGROUP.booleanValue())))
            .andExpect(jsonPath("$.[*].biometricDeadlineDate").value(hasItem(DEFAULT_BIOMETRIC_DEADLINE_DATE.toString())))
            .andExpect(jsonPath("$.[*].biometricLookbackDate").value(hasItem(DEFAULT_BIOMETRIC_LOOKBACK_DATE.toString())))
            .andExpect(jsonPath("$.[*].landingBackgroundImageUrl").value(hasItem(DEFAULT_LANDING_BACKGROUND_IMAGE_URL.toString())))
            .andExpect(jsonPath("$.[*].isTobaccoSurchargeManagement").value(hasItem(DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT.booleanValue())))
            .andExpect(jsonPath("$.[*].tobaccoAttestationDeadline").value(hasItem(DEFAULT_TOBACCO_ATTESTATION_DEADLINE.toString())))
            .andExpect(jsonPath("$.[*].tobaccoProgramDeadline").value(hasItem(DEFAULT_TOBACCO_PROGRAM_DEADLINE.toString())))
            .andExpect(jsonPath("$.[*].isAwardedForTobaccoAttestation").value(hasItem(DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION.booleanValue())))
            .andExpect(jsonPath("$.[*].isAwardedForTobaccoRas").value(hasItem(DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS.booleanValue())));
    }

    @Test
    @Transactional
    public void getProgram() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get the program
        restProgramMockMvc.perform(get("/api/programs/{id}", program.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(program.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.lastSent").value(DEFAULT_LAST_SENT.toString()))
            .andExpect(jsonPath("$.isRetriggerEmail").value(DEFAULT_IS_RETRIGGER_EMAIL.booleanValue()))
            .andExpect(jsonPath("$.isEligible").value(DEFAULT_IS_ELIGIBLE.booleanValue()))
            .andExpect(jsonPath("$.isSentRegistrationEmail").value(DEFAULT_IS_SENT_REGISTRATION_EMAIL.booleanValue()))
            .andExpect(jsonPath("$.isRegisteredForPlatform").value(DEFAULT_IS_REGISTERED_FOR_PLATFORM.booleanValue()))
            .andExpect(jsonPath("$.isScheduledScreening").value(DEFAULT_IS_SCHEDULED_SCREENING.booleanValue()))
            .andExpect(jsonPath("$.isFunctionally").value(DEFAULT_IS_FUNCTIONALLY.booleanValue()))
            .andExpect(jsonPath("$.logoUrl").value(DEFAULT_LOGO_URL.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.userPoint").value(DEFAULT_USER_POINT.intValue()))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.isUsePoint").value(DEFAULT_IS_USE_POINT.booleanValue()))
            .andExpect(jsonPath("$.isScreen").value(DEFAULT_IS_SCREEN.booleanValue()))
            .andExpect(jsonPath("$.isCoaching").value(DEFAULT_IS_COACHING.booleanValue()))
            .andExpect(jsonPath("$.isWellMatric").value(DEFAULT_IS_WELL_MATRIC.booleanValue()))
            .andExpect(jsonPath("$.isHP").value(DEFAULT_IS_HP.booleanValue()))
            .andExpect(jsonPath("$.isUseLevel").value(DEFAULT_IS_USE_LEVEL.booleanValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.resetDate").value(DEFAULT_RESET_DATE.toString()))
            .andExpect(jsonPath("$.isTemplate").value(DEFAULT_IS_TEMPLATE.booleanValue()))
            .andExpect(jsonPath("$.isPreview").value(DEFAULT_IS_PREVIEW.booleanValue()))
            .andExpect(jsonPath("$.previewDate").value(DEFAULT_PREVIEW_DATE.toString()))
            .andExpect(jsonPath("$.previewTimeZone").value(DEFAULT_PREVIEW_TIME_ZONE.toString()))
            .andExpect(jsonPath("$.startTimeZone").value(DEFAULT_START_TIME_ZONE.toString()))
            .andExpect(jsonPath("$.endTimeZone").value(DEFAULT_END_TIME_ZONE.toString()))
            .andExpect(jsonPath("$.levelStructure").value(DEFAULT_LEVEL_STRUCTURE.toString()))
            .andExpect(jsonPath("$.programLength").value(DEFAULT_PROGRAM_LENGTH))
            .andExpect(jsonPath("$.isHPSF").value(DEFAULT_IS_HPSF.booleanValue()))
            .andExpect(jsonPath("$.isHTK").value(DEFAULT_IS_HTK.booleanValue()))
            .andExpect(jsonPath("$.isLabcorp").value(DEFAULT_IS_LABCORP.booleanValue()))
            .andExpect(jsonPath("$.labcorpAccountNumber").value(DEFAULT_LABCORP_ACCOUNT_NUMBER.toString()))
            .andExpect(jsonPath("$.labcorpFileUrl").value(DEFAULT_LABCORP_FILE_URL.toString()))
            .andExpect(jsonPath("$.applyRewardAllSubgroup").value(DEFAULT_APPLY_REWARD_ALL_SUBGROUP.booleanValue()))
            .andExpect(jsonPath("$.biometricDeadlineDate").value(DEFAULT_BIOMETRIC_DEADLINE_DATE.toString()))
            .andExpect(jsonPath("$.biometricLookbackDate").value(DEFAULT_BIOMETRIC_LOOKBACK_DATE.toString()))
            .andExpect(jsonPath("$.landingBackgroundImageUrl").value(DEFAULT_LANDING_BACKGROUND_IMAGE_URL.toString()))
            .andExpect(jsonPath("$.isTobaccoSurchargeManagement").value(DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT.booleanValue()))
            .andExpect(jsonPath("$.tobaccoAttestationDeadline").value(DEFAULT_TOBACCO_ATTESTATION_DEADLINE.toString()))
            .andExpect(jsonPath("$.tobaccoProgramDeadline").value(DEFAULT_TOBACCO_PROGRAM_DEADLINE.toString()))
            .andExpect(jsonPath("$.isAwardedForTobaccoAttestation").value(DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION.booleanValue()))
            .andExpect(jsonPath("$.isAwardedForTobaccoRas").value(DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllProgramsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where name equals to DEFAULT_NAME
        defaultProgramShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the programList where name equals to UPDATED_NAME
        defaultProgramShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where name in DEFAULT_NAME or UPDATED_NAME
        defaultProgramShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the programList where name equals to UPDATED_NAME
        defaultProgramShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where name is not null
        defaultProgramShouldBeFound("name.specified=true");

        // Get all the programList where name is null
        defaultProgramShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLastSentIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastSent equals to DEFAULT_LAST_SENT
        defaultProgramShouldBeFound("lastSent.equals=" + DEFAULT_LAST_SENT);

        // Get all the programList where lastSent equals to UPDATED_LAST_SENT
        defaultProgramShouldNotBeFound("lastSent.equals=" + UPDATED_LAST_SENT);
    }

    @Test
    @Transactional
    public void getAllProgramsByLastSentIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastSent in DEFAULT_LAST_SENT or UPDATED_LAST_SENT
        defaultProgramShouldBeFound("lastSent.in=" + DEFAULT_LAST_SENT + "," + UPDATED_LAST_SENT);

        // Get all the programList where lastSent equals to UPDATED_LAST_SENT
        defaultProgramShouldNotBeFound("lastSent.in=" + UPDATED_LAST_SENT);
    }

    @Test
    @Transactional
    public void getAllProgramsByLastSentIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastSent is not null
        defaultProgramShouldBeFound("lastSent.specified=true");

        // Get all the programList where lastSent is null
        defaultProgramShouldNotBeFound("lastSent.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLastSentIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastSent greater than or equals to DEFAULT_LAST_SENT
        defaultProgramShouldBeFound("lastSent.greaterOrEqualThan=" + DEFAULT_LAST_SENT);

        // Get all the programList where lastSent greater than or equals to UPDATED_LAST_SENT
        defaultProgramShouldNotBeFound("lastSent.greaterOrEqualThan=" + UPDATED_LAST_SENT);
    }

    @Test
    @Transactional
    public void getAllProgramsByLastSentIsLessThanSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastSent less than or equals to DEFAULT_LAST_SENT
        defaultProgramShouldNotBeFound("lastSent.lessThan=" + DEFAULT_LAST_SENT);

        // Get all the programList where lastSent less than or equals to UPDATED_LAST_SENT
        defaultProgramShouldBeFound("lastSent.lessThan=" + UPDATED_LAST_SENT);
    }


    @Test
    @Transactional
    public void getAllProgramsByIsRetriggerEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isRetriggerEmail equals to DEFAULT_IS_RETRIGGER_EMAIL
        defaultProgramShouldBeFound("isRetriggerEmail.equals=" + DEFAULT_IS_RETRIGGER_EMAIL);

        // Get all the programList where isRetriggerEmail equals to UPDATED_IS_RETRIGGER_EMAIL
        defaultProgramShouldNotBeFound("isRetriggerEmail.equals=" + UPDATED_IS_RETRIGGER_EMAIL);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsRetriggerEmailIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isRetriggerEmail in DEFAULT_IS_RETRIGGER_EMAIL or UPDATED_IS_RETRIGGER_EMAIL
        defaultProgramShouldBeFound("isRetriggerEmail.in=" + DEFAULT_IS_RETRIGGER_EMAIL + "," + UPDATED_IS_RETRIGGER_EMAIL);

        // Get all the programList where isRetriggerEmail equals to UPDATED_IS_RETRIGGER_EMAIL
        defaultProgramShouldNotBeFound("isRetriggerEmail.in=" + UPDATED_IS_RETRIGGER_EMAIL);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsRetriggerEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isRetriggerEmail is not null
        defaultProgramShouldBeFound("isRetriggerEmail.specified=true");

        // Get all the programList where isRetriggerEmail is null
        defaultProgramShouldNotBeFound("isRetriggerEmail.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsEligibleIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isEligible equals to DEFAULT_IS_ELIGIBLE
        defaultProgramShouldBeFound("isEligible.equals=" + DEFAULT_IS_ELIGIBLE);

        // Get all the programList where isEligible equals to UPDATED_IS_ELIGIBLE
        defaultProgramShouldNotBeFound("isEligible.equals=" + UPDATED_IS_ELIGIBLE);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsEligibleIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isEligible in DEFAULT_IS_ELIGIBLE or UPDATED_IS_ELIGIBLE
        defaultProgramShouldBeFound("isEligible.in=" + DEFAULT_IS_ELIGIBLE + "," + UPDATED_IS_ELIGIBLE);

        // Get all the programList where isEligible equals to UPDATED_IS_ELIGIBLE
        defaultProgramShouldNotBeFound("isEligible.in=" + UPDATED_IS_ELIGIBLE);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsEligibleIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isEligible is not null
        defaultProgramShouldBeFound("isEligible.specified=true");

        // Get all the programList where isEligible is null
        defaultProgramShouldNotBeFound("isEligible.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsSentRegistrationEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isSentRegistrationEmail equals to DEFAULT_IS_SENT_REGISTRATION_EMAIL
        defaultProgramShouldBeFound("isSentRegistrationEmail.equals=" + DEFAULT_IS_SENT_REGISTRATION_EMAIL);

        // Get all the programList where isSentRegistrationEmail equals to UPDATED_IS_SENT_REGISTRATION_EMAIL
        defaultProgramShouldNotBeFound("isSentRegistrationEmail.equals=" + UPDATED_IS_SENT_REGISTRATION_EMAIL);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsSentRegistrationEmailIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isSentRegistrationEmail in DEFAULT_IS_SENT_REGISTRATION_EMAIL or UPDATED_IS_SENT_REGISTRATION_EMAIL
        defaultProgramShouldBeFound("isSentRegistrationEmail.in=" + DEFAULT_IS_SENT_REGISTRATION_EMAIL + "," + UPDATED_IS_SENT_REGISTRATION_EMAIL);

        // Get all the programList where isSentRegistrationEmail equals to UPDATED_IS_SENT_REGISTRATION_EMAIL
        defaultProgramShouldNotBeFound("isSentRegistrationEmail.in=" + UPDATED_IS_SENT_REGISTRATION_EMAIL);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsSentRegistrationEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isSentRegistrationEmail is not null
        defaultProgramShouldBeFound("isSentRegistrationEmail.specified=true");

        // Get all the programList where isSentRegistrationEmail is null
        defaultProgramShouldNotBeFound("isSentRegistrationEmail.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsRegisteredForPlatformIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isRegisteredForPlatform equals to DEFAULT_IS_REGISTERED_FOR_PLATFORM
        defaultProgramShouldBeFound("isRegisteredForPlatform.equals=" + DEFAULT_IS_REGISTERED_FOR_PLATFORM);

        // Get all the programList where isRegisteredForPlatform equals to UPDATED_IS_REGISTERED_FOR_PLATFORM
        defaultProgramShouldNotBeFound("isRegisteredForPlatform.equals=" + UPDATED_IS_REGISTERED_FOR_PLATFORM);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsRegisteredForPlatformIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isRegisteredForPlatform in DEFAULT_IS_REGISTERED_FOR_PLATFORM or UPDATED_IS_REGISTERED_FOR_PLATFORM
        defaultProgramShouldBeFound("isRegisteredForPlatform.in=" + DEFAULT_IS_REGISTERED_FOR_PLATFORM + "," + UPDATED_IS_REGISTERED_FOR_PLATFORM);

        // Get all the programList where isRegisteredForPlatform equals to UPDATED_IS_REGISTERED_FOR_PLATFORM
        defaultProgramShouldNotBeFound("isRegisteredForPlatform.in=" + UPDATED_IS_REGISTERED_FOR_PLATFORM);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsRegisteredForPlatformIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isRegisteredForPlatform is not null
        defaultProgramShouldBeFound("isRegisteredForPlatform.specified=true");

        // Get all the programList where isRegisteredForPlatform is null
        defaultProgramShouldNotBeFound("isRegisteredForPlatform.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsScheduledScreeningIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isScheduledScreening equals to DEFAULT_IS_SCHEDULED_SCREENING
        defaultProgramShouldBeFound("isScheduledScreening.equals=" + DEFAULT_IS_SCHEDULED_SCREENING);

        // Get all the programList where isScheduledScreening equals to UPDATED_IS_SCHEDULED_SCREENING
        defaultProgramShouldNotBeFound("isScheduledScreening.equals=" + UPDATED_IS_SCHEDULED_SCREENING);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsScheduledScreeningIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isScheduledScreening in DEFAULT_IS_SCHEDULED_SCREENING or UPDATED_IS_SCHEDULED_SCREENING
        defaultProgramShouldBeFound("isScheduledScreening.in=" + DEFAULT_IS_SCHEDULED_SCREENING + "," + UPDATED_IS_SCHEDULED_SCREENING);

        // Get all the programList where isScheduledScreening equals to UPDATED_IS_SCHEDULED_SCREENING
        defaultProgramShouldNotBeFound("isScheduledScreening.in=" + UPDATED_IS_SCHEDULED_SCREENING);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsScheduledScreeningIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isScheduledScreening is not null
        defaultProgramShouldBeFound("isScheduledScreening.specified=true");

        // Get all the programList where isScheduledScreening is null
        defaultProgramShouldNotBeFound("isScheduledScreening.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsFunctionallyIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isFunctionally equals to DEFAULT_IS_FUNCTIONALLY
        defaultProgramShouldBeFound("isFunctionally.equals=" + DEFAULT_IS_FUNCTIONALLY);

        // Get all the programList where isFunctionally equals to UPDATED_IS_FUNCTIONALLY
        defaultProgramShouldNotBeFound("isFunctionally.equals=" + UPDATED_IS_FUNCTIONALLY);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsFunctionallyIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isFunctionally in DEFAULT_IS_FUNCTIONALLY or UPDATED_IS_FUNCTIONALLY
        defaultProgramShouldBeFound("isFunctionally.in=" + DEFAULT_IS_FUNCTIONALLY + "," + UPDATED_IS_FUNCTIONALLY);

        // Get all the programList where isFunctionally equals to UPDATED_IS_FUNCTIONALLY
        defaultProgramShouldNotBeFound("isFunctionally.in=" + UPDATED_IS_FUNCTIONALLY);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsFunctionallyIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isFunctionally is not null
        defaultProgramShouldBeFound("isFunctionally.specified=true");

        // Get all the programList where isFunctionally is null
        defaultProgramShouldNotBeFound("isFunctionally.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLogoUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where logoUrl equals to DEFAULT_LOGO_URL
        defaultProgramShouldBeFound("logoUrl.equals=" + DEFAULT_LOGO_URL);

        // Get all the programList where logoUrl equals to UPDATED_LOGO_URL
        defaultProgramShouldNotBeFound("logoUrl.equals=" + UPDATED_LOGO_URL);
    }

    @Test
    @Transactional
    public void getAllProgramsByLogoUrlIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where logoUrl in DEFAULT_LOGO_URL or UPDATED_LOGO_URL
        defaultProgramShouldBeFound("logoUrl.in=" + DEFAULT_LOGO_URL + "," + UPDATED_LOGO_URL);

        // Get all the programList where logoUrl equals to UPDATED_LOGO_URL
        defaultProgramShouldNotBeFound("logoUrl.in=" + UPDATED_LOGO_URL);
    }

    @Test
    @Transactional
    public void getAllProgramsByLogoUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where logoUrl is not null
        defaultProgramShouldBeFound("logoUrl.specified=true");

        // Get all the programList where logoUrl is null
        defaultProgramShouldNotBeFound("logoUrl.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where description equals to DEFAULT_DESCRIPTION
        defaultProgramShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the programList where description equals to UPDATED_DESCRIPTION
        defaultProgramShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProgramsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultProgramShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the programList where description equals to UPDATED_DESCRIPTION
        defaultProgramShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProgramsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where description is not null
        defaultProgramShouldBeFound("description.specified=true");

        // Get all the programList where description is null
        defaultProgramShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByUserPointIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where userPoint equals to DEFAULT_USER_POINT
        defaultProgramShouldBeFound("userPoint.equals=" + DEFAULT_USER_POINT);

        // Get all the programList where userPoint equals to UPDATED_USER_POINT
        defaultProgramShouldNotBeFound("userPoint.equals=" + UPDATED_USER_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramsByUserPointIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where userPoint in DEFAULT_USER_POINT or UPDATED_USER_POINT
        defaultProgramShouldBeFound("userPoint.in=" + DEFAULT_USER_POINT + "," + UPDATED_USER_POINT);

        // Get all the programList where userPoint equals to UPDATED_USER_POINT
        defaultProgramShouldNotBeFound("userPoint.in=" + UPDATED_USER_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramsByUserPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where userPoint is not null
        defaultProgramShouldBeFound("userPoint.specified=true");

        // Get all the programList where userPoint is null
        defaultProgramShouldNotBeFound("userPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultProgramShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the programList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultProgramShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultProgramShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the programList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultProgramShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastModifiedDate is not null
        defaultProgramShouldBeFound("lastModifiedDate.specified=true");

        // Get all the programList where lastModifiedDate is null
        defaultProgramShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where createdDate is not null
        defaultProgramShouldBeFound("createdDate.specified=true");

        // Get all the programList where createdDate is null
        defaultProgramShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultProgramShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the programList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultProgramShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllProgramsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultProgramShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the programList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultProgramShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    public void getAllProgramsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where lastModifiedBy is not null
        defaultProgramShouldBeFound("lastModifiedBy.specified=true");

        // Get all the programList where lastModifiedBy is null
        defaultProgramShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where createdBy equals to DEFAULT_CREATED_BY
        defaultProgramShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the programList where createdBy equals to UPDATED_CREATED_BY
        defaultProgramShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllProgramsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultProgramShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the programList where createdBy equals to UPDATED_CREATED_BY
        defaultProgramShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    public void getAllProgramsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where createdBy is not null
        defaultProgramShouldBeFound("createdBy.specified=true");

        // Get all the programList where createdBy is null
        defaultProgramShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where status equals to DEFAULT_STATUS
        defaultProgramShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the programList where status equals to UPDATED_STATUS
        defaultProgramShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultProgramShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the programList where status equals to UPDATED_STATUS
        defaultProgramShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where status is not null
        defaultProgramShouldBeFound("status.specified=true");

        // Get all the programList where status is null
        defaultProgramShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsUsePointIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isUsePoint equals to DEFAULT_IS_USE_POINT
        defaultProgramShouldBeFound("isUsePoint.equals=" + DEFAULT_IS_USE_POINT);

        // Get all the programList where isUsePoint equals to UPDATED_IS_USE_POINT
        defaultProgramShouldNotBeFound("isUsePoint.equals=" + UPDATED_IS_USE_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsUsePointIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isUsePoint in DEFAULT_IS_USE_POINT or UPDATED_IS_USE_POINT
        defaultProgramShouldBeFound("isUsePoint.in=" + DEFAULT_IS_USE_POINT + "," + UPDATED_IS_USE_POINT);

        // Get all the programList where isUsePoint equals to UPDATED_IS_USE_POINT
        defaultProgramShouldNotBeFound("isUsePoint.in=" + UPDATED_IS_USE_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsUsePointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isUsePoint is not null
        defaultProgramShouldBeFound("isUsePoint.specified=true");

        // Get all the programList where isUsePoint is null
        defaultProgramShouldNotBeFound("isUsePoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsScreenIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isScreen equals to DEFAULT_IS_SCREEN
        defaultProgramShouldBeFound("isScreen.equals=" + DEFAULT_IS_SCREEN);

        // Get all the programList where isScreen equals to UPDATED_IS_SCREEN
        defaultProgramShouldNotBeFound("isScreen.equals=" + UPDATED_IS_SCREEN);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsScreenIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isScreen in DEFAULT_IS_SCREEN or UPDATED_IS_SCREEN
        defaultProgramShouldBeFound("isScreen.in=" + DEFAULT_IS_SCREEN + "," + UPDATED_IS_SCREEN);

        // Get all the programList where isScreen equals to UPDATED_IS_SCREEN
        defaultProgramShouldNotBeFound("isScreen.in=" + UPDATED_IS_SCREEN);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsScreenIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isScreen is not null
        defaultProgramShouldBeFound("isScreen.specified=true");

        // Get all the programList where isScreen is null
        defaultProgramShouldNotBeFound("isScreen.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsCoachingIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isCoaching equals to DEFAULT_IS_COACHING
        defaultProgramShouldBeFound("isCoaching.equals=" + DEFAULT_IS_COACHING);

        // Get all the programList where isCoaching equals to UPDATED_IS_COACHING
        defaultProgramShouldNotBeFound("isCoaching.equals=" + UPDATED_IS_COACHING);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsCoachingIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isCoaching in DEFAULT_IS_COACHING or UPDATED_IS_COACHING
        defaultProgramShouldBeFound("isCoaching.in=" + DEFAULT_IS_COACHING + "," + UPDATED_IS_COACHING);

        // Get all the programList where isCoaching equals to UPDATED_IS_COACHING
        defaultProgramShouldNotBeFound("isCoaching.in=" + UPDATED_IS_COACHING);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsCoachingIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isCoaching is not null
        defaultProgramShouldBeFound("isCoaching.specified=true");

        // Get all the programList where isCoaching is null
        defaultProgramShouldNotBeFound("isCoaching.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsWellMatricIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isWellMatric equals to DEFAULT_IS_WELL_MATRIC
        defaultProgramShouldBeFound("isWellMatric.equals=" + DEFAULT_IS_WELL_MATRIC);

        // Get all the programList where isWellMatric equals to UPDATED_IS_WELL_MATRIC
        defaultProgramShouldNotBeFound("isWellMatric.equals=" + UPDATED_IS_WELL_MATRIC);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsWellMatricIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isWellMatric in DEFAULT_IS_WELL_MATRIC or UPDATED_IS_WELL_MATRIC
        defaultProgramShouldBeFound("isWellMatric.in=" + DEFAULT_IS_WELL_MATRIC + "," + UPDATED_IS_WELL_MATRIC);

        // Get all the programList where isWellMatric equals to UPDATED_IS_WELL_MATRIC
        defaultProgramShouldNotBeFound("isWellMatric.in=" + UPDATED_IS_WELL_MATRIC);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsWellMatricIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isWellMatric is not null
        defaultProgramShouldBeFound("isWellMatric.specified=true");

        // Get all the programList where isWellMatric is null
        defaultProgramShouldNotBeFound("isWellMatric.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsHPIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHP equals to DEFAULT_IS_HP
        defaultProgramShouldBeFound("isHP.equals=" + DEFAULT_IS_HP);

        // Get all the programList where isHP equals to UPDATED_IS_HP
        defaultProgramShouldNotBeFound("isHP.equals=" + UPDATED_IS_HP);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsHPIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHP in DEFAULT_IS_HP or UPDATED_IS_HP
        defaultProgramShouldBeFound("isHP.in=" + DEFAULT_IS_HP + "," + UPDATED_IS_HP);

        // Get all the programList where isHP equals to UPDATED_IS_HP
        defaultProgramShouldNotBeFound("isHP.in=" + UPDATED_IS_HP);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsHPIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHP is not null
        defaultProgramShouldBeFound("isHP.specified=true");

        // Get all the programList where isHP is null
        defaultProgramShouldNotBeFound("isHP.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsUseLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isUseLevel equals to DEFAULT_IS_USE_LEVEL
        defaultProgramShouldBeFound("isUseLevel.equals=" + DEFAULT_IS_USE_LEVEL);

        // Get all the programList where isUseLevel equals to UPDATED_IS_USE_LEVEL
        defaultProgramShouldNotBeFound("isUseLevel.equals=" + UPDATED_IS_USE_LEVEL);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsUseLevelIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isUseLevel in DEFAULT_IS_USE_LEVEL or UPDATED_IS_USE_LEVEL
        defaultProgramShouldBeFound("isUseLevel.in=" + DEFAULT_IS_USE_LEVEL + "," + UPDATED_IS_USE_LEVEL);

        // Get all the programList where isUseLevel equals to UPDATED_IS_USE_LEVEL
        defaultProgramShouldNotBeFound("isUseLevel.in=" + UPDATED_IS_USE_LEVEL);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsUseLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isUseLevel is not null
        defaultProgramShouldBeFound("isUseLevel.specified=true");

        // Get all the programList where isUseLevel is null
        defaultProgramShouldNotBeFound("isUseLevel.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByStartDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where startDate equals to DEFAULT_START_DATE
        defaultProgramShouldBeFound("startDate.equals=" + DEFAULT_START_DATE);

        // Get all the programList where startDate equals to UPDATED_START_DATE
        defaultProgramShouldNotBeFound("startDate.equals=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByStartDateIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where startDate in DEFAULT_START_DATE or UPDATED_START_DATE
        defaultProgramShouldBeFound("startDate.in=" + DEFAULT_START_DATE + "," + UPDATED_START_DATE);

        // Get all the programList where startDate equals to UPDATED_START_DATE
        defaultProgramShouldNotBeFound("startDate.in=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByStartDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where startDate is not null
        defaultProgramShouldBeFound("startDate.specified=true");

        // Get all the programList where startDate is null
        defaultProgramShouldNotBeFound("startDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByResetDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where resetDate equals to DEFAULT_RESET_DATE
        defaultProgramShouldBeFound("resetDate.equals=" + DEFAULT_RESET_DATE);

        // Get all the programList where resetDate equals to UPDATED_RESET_DATE
        defaultProgramShouldNotBeFound("resetDate.equals=" + UPDATED_RESET_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByResetDateIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where resetDate in DEFAULT_RESET_DATE or UPDATED_RESET_DATE
        defaultProgramShouldBeFound("resetDate.in=" + DEFAULT_RESET_DATE + "," + UPDATED_RESET_DATE);

        // Get all the programList where resetDate equals to UPDATED_RESET_DATE
        defaultProgramShouldNotBeFound("resetDate.in=" + UPDATED_RESET_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByResetDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where resetDate is not null
        defaultProgramShouldBeFound("resetDate.specified=true");

        // Get all the programList where resetDate is null
        defaultProgramShouldNotBeFound("resetDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsTemplateIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isTemplate equals to DEFAULT_IS_TEMPLATE
        defaultProgramShouldBeFound("isTemplate.equals=" + DEFAULT_IS_TEMPLATE);

        // Get all the programList where isTemplate equals to UPDATED_IS_TEMPLATE
        defaultProgramShouldNotBeFound("isTemplate.equals=" + UPDATED_IS_TEMPLATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsTemplateIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isTemplate in DEFAULT_IS_TEMPLATE or UPDATED_IS_TEMPLATE
        defaultProgramShouldBeFound("isTemplate.in=" + DEFAULT_IS_TEMPLATE + "," + UPDATED_IS_TEMPLATE);

        // Get all the programList where isTemplate equals to UPDATED_IS_TEMPLATE
        defaultProgramShouldNotBeFound("isTemplate.in=" + UPDATED_IS_TEMPLATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsTemplateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isTemplate is not null
        defaultProgramShouldBeFound("isTemplate.specified=true");

        // Get all the programList where isTemplate is null
        defaultProgramShouldNotBeFound("isTemplate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsPreviewIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isPreview equals to DEFAULT_IS_PREVIEW
        defaultProgramShouldBeFound("isPreview.equals=" + DEFAULT_IS_PREVIEW);

        // Get all the programList where isPreview equals to UPDATED_IS_PREVIEW
        defaultProgramShouldNotBeFound("isPreview.equals=" + UPDATED_IS_PREVIEW);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsPreviewIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isPreview in DEFAULT_IS_PREVIEW or UPDATED_IS_PREVIEW
        defaultProgramShouldBeFound("isPreview.in=" + DEFAULT_IS_PREVIEW + "," + UPDATED_IS_PREVIEW);

        // Get all the programList where isPreview equals to UPDATED_IS_PREVIEW
        defaultProgramShouldNotBeFound("isPreview.in=" + UPDATED_IS_PREVIEW);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsPreviewIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isPreview is not null
        defaultProgramShouldBeFound("isPreview.specified=true");

        // Get all the programList where isPreview is null
        defaultProgramShouldNotBeFound("isPreview.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByPreviewDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where previewDate equals to DEFAULT_PREVIEW_DATE
        defaultProgramShouldBeFound("previewDate.equals=" + DEFAULT_PREVIEW_DATE);

        // Get all the programList where previewDate equals to UPDATED_PREVIEW_DATE
        defaultProgramShouldNotBeFound("previewDate.equals=" + UPDATED_PREVIEW_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByPreviewDateIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where previewDate in DEFAULT_PREVIEW_DATE or UPDATED_PREVIEW_DATE
        defaultProgramShouldBeFound("previewDate.in=" + DEFAULT_PREVIEW_DATE + "," + UPDATED_PREVIEW_DATE);

        // Get all the programList where previewDate equals to UPDATED_PREVIEW_DATE
        defaultProgramShouldNotBeFound("previewDate.in=" + UPDATED_PREVIEW_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByPreviewDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where previewDate is not null
        defaultProgramShouldBeFound("previewDate.specified=true");

        // Get all the programList where previewDate is null
        defaultProgramShouldNotBeFound("previewDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByPreviewTimeZoneIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where previewTimeZone equals to DEFAULT_PREVIEW_TIME_ZONE
        defaultProgramShouldBeFound("previewTimeZone.equals=" + DEFAULT_PREVIEW_TIME_ZONE);

        // Get all the programList where previewTimeZone equals to UPDATED_PREVIEW_TIME_ZONE
        defaultProgramShouldNotBeFound("previewTimeZone.equals=" + UPDATED_PREVIEW_TIME_ZONE);
    }

    @Test
    @Transactional
    public void getAllProgramsByPreviewTimeZoneIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where previewTimeZone in DEFAULT_PREVIEW_TIME_ZONE or UPDATED_PREVIEW_TIME_ZONE
        defaultProgramShouldBeFound("previewTimeZone.in=" + DEFAULT_PREVIEW_TIME_ZONE + "," + UPDATED_PREVIEW_TIME_ZONE);

        // Get all the programList where previewTimeZone equals to UPDATED_PREVIEW_TIME_ZONE
        defaultProgramShouldNotBeFound("previewTimeZone.in=" + UPDATED_PREVIEW_TIME_ZONE);
    }

    @Test
    @Transactional
    public void getAllProgramsByPreviewTimeZoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where previewTimeZone is not null
        defaultProgramShouldBeFound("previewTimeZone.specified=true");

        // Get all the programList where previewTimeZone is null
        defaultProgramShouldNotBeFound("previewTimeZone.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByStartTimeZoneIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where startTimeZone equals to DEFAULT_START_TIME_ZONE
        defaultProgramShouldBeFound("startTimeZone.equals=" + DEFAULT_START_TIME_ZONE);

        // Get all the programList where startTimeZone equals to UPDATED_START_TIME_ZONE
        defaultProgramShouldNotBeFound("startTimeZone.equals=" + UPDATED_START_TIME_ZONE);
    }

    @Test
    @Transactional
    public void getAllProgramsByStartTimeZoneIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where startTimeZone in DEFAULT_START_TIME_ZONE or UPDATED_START_TIME_ZONE
        defaultProgramShouldBeFound("startTimeZone.in=" + DEFAULT_START_TIME_ZONE + "," + UPDATED_START_TIME_ZONE);

        // Get all the programList where startTimeZone equals to UPDATED_START_TIME_ZONE
        defaultProgramShouldNotBeFound("startTimeZone.in=" + UPDATED_START_TIME_ZONE);
    }

    @Test
    @Transactional
    public void getAllProgramsByStartTimeZoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where startTimeZone is not null
        defaultProgramShouldBeFound("startTimeZone.specified=true");

        // Get all the programList where startTimeZone is null
        defaultProgramShouldNotBeFound("startTimeZone.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByEndTimeZoneIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where endTimeZone equals to DEFAULT_END_TIME_ZONE
        defaultProgramShouldBeFound("endTimeZone.equals=" + DEFAULT_END_TIME_ZONE);

        // Get all the programList where endTimeZone equals to UPDATED_END_TIME_ZONE
        defaultProgramShouldNotBeFound("endTimeZone.equals=" + UPDATED_END_TIME_ZONE);
    }

    @Test
    @Transactional
    public void getAllProgramsByEndTimeZoneIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where endTimeZone in DEFAULT_END_TIME_ZONE or UPDATED_END_TIME_ZONE
        defaultProgramShouldBeFound("endTimeZone.in=" + DEFAULT_END_TIME_ZONE + "," + UPDATED_END_TIME_ZONE);

        // Get all the programList where endTimeZone equals to UPDATED_END_TIME_ZONE
        defaultProgramShouldNotBeFound("endTimeZone.in=" + UPDATED_END_TIME_ZONE);
    }

    @Test
    @Transactional
    public void getAllProgramsByEndTimeZoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where endTimeZone is not null
        defaultProgramShouldBeFound("endTimeZone.specified=true");

        // Get all the programList where endTimeZone is null
        defaultProgramShouldNotBeFound("endTimeZone.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLevelStructureIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where levelStructure equals to DEFAULT_LEVEL_STRUCTURE
        defaultProgramShouldBeFound("levelStructure.equals=" + DEFAULT_LEVEL_STRUCTURE);

        // Get all the programList where levelStructure equals to UPDATED_LEVEL_STRUCTURE
        defaultProgramShouldNotBeFound("levelStructure.equals=" + UPDATED_LEVEL_STRUCTURE);
    }

    @Test
    @Transactional
    public void getAllProgramsByLevelStructureIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where levelStructure in DEFAULT_LEVEL_STRUCTURE or UPDATED_LEVEL_STRUCTURE
        defaultProgramShouldBeFound("levelStructure.in=" + DEFAULT_LEVEL_STRUCTURE + "," + UPDATED_LEVEL_STRUCTURE);

        // Get all the programList where levelStructure equals to UPDATED_LEVEL_STRUCTURE
        defaultProgramShouldNotBeFound("levelStructure.in=" + UPDATED_LEVEL_STRUCTURE);
    }

    @Test
    @Transactional
    public void getAllProgramsByLevelStructureIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where levelStructure is not null
        defaultProgramShouldBeFound("levelStructure.specified=true");

        // Get all the programList where levelStructure is null
        defaultProgramShouldNotBeFound("levelStructure.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByProgramLengthIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where programLength equals to DEFAULT_PROGRAM_LENGTH
        defaultProgramShouldBeFound("programLength.equals=" + DEFAULT_PROGRAM_LENGTH);

        // Get all the programList where programLength equals to UPDATED_PROGRAM_LENGTH
        defaultProgramShouldNotBeFound("programLength.equals=" + UPDATED_PROGRAM_LENGTH);
    }

    @Test
    @Transactional
    public void getAllProgramsByProgramLengthIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where programLength in DEFAULT_PROGRAM_LENGTH or UPDATED_PROGRAM_LENGTH
        defaultProgramShouldBeFound("programLength.in=" + DEFAULT_PROGRAM_LENGTH + "," + UPDATED_PROGRAM_LENGTH);

        // Get all the programList where programLength equals to UPDATED_PROGRAM_LENGTH
        defaultProgramShouldNotBeFound("programLength.in=" + UPDATED_PROGRAM_LENGTH);
    }

    @Test
    @Transactional
    public void getAllProgramsByProgramLengthIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where programLength is not null
        defaultProgramShouldBeFound("programLength.specified=true");

        // Get all the programList where programLength is null
        defaultProgramShouldNotBeFound("programLength.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByProgramLengthIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where programLength greater than or equals to DEFAULT_PROGRAM_LENGTH
        defaultProgramShouldBeFound("programLength.greaterOrEqualThan=" + DEFAULT_PROGRAM_LENGTH);

        // Get all the programList where programLength greater than or equals to UPDATED_PROGRAM_LENGTH
        defaultProgramShouldNotBeFound("programLength.greaterOrEqualThan=" + UPDATED_PROGRAM_LENGTH);
    }

    @Test
    @Transactional
    public void getAllProgramsByProgramLengthIsLessThanSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where programLength less than or equals to DEFAULT_PROGRAM_LENGTH
        defaultProgramShouldNotBeFound("programLength.lessThan=" + DEFAULT_PROGRAM_LENGTH);

        // Get all the programList where programLength less than or equals to UPDATED_PROGRAM_LENGTH
        defaultProgramShouldBeFound("programLength.lessThan=" + UPDATED_PROGRAM_LENGTH);
    }


    @Test
    @Transactional
    public void getAllProgramsByIsHPSFIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHPSF equals to DEFAULT_IS_HPSF
        defaultProgramShouldBeFound("isHPSF.equals=" + DEFAULT_IS_HPSF);

        // Get all the programList where isHPSF equals to UPDATED_IS_HPSF
        defaultProgramShouldNotBeFound("isHPSF.equals=" + UPDATED_IS_HPSF);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsHPSFIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHPSF in DEFAULT_IS_HPSF or UPDATED_IS_HPSF
        defaultProgramShouldBeFound("isHPSF.in=" + DEFAULT_IS_HPSF + "," + UPDATED_IS_HPSF);

        // Get all the programList where isHPSF equals to UPDATED_IS_HPSF
        defaultProgramShouldNotBeFound("isHPSF.in=" + UPDATED_IS_HPSF);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsHPSFIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHPSF is not null
        defaultProgramShouldBeFound("isHPSF.specified=true");

        // Get all the programList where isHPSF is null
        defaultProgramShouldNotBeFound("isHPSF.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsHTKIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHTK equals to DEFAULT_IS_HTK
        defaultProgramShouldBeFound("isHTK.equals=" + DEFAULT_IS_HTK);

        // Get all the programList where isHTK equals to UPDATED_IS_HTK
        defaultProgramShouldNotBeFound("isHTK.equals=" + UPDATED_IS_HTK);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsHTKIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHTK in DEFAULT_IS_HTK or UPDATED_IS_HTK
        defaultProgramShouldBeFound("isHTK.in=" + DEFAULT_IS_HTK + "," + UPDATED_IS_HTK);

        // Get all the programList where isHTK equals to UPDATED_IS_HTK
        defaultProgramShouldNotBeFound("isHTK.in=" + UPDATED_IS_HTK);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsHTKIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isHTK is not null
        defaultProgramShouldBeFound("isHTK.specified=true");

        // Get all the programList where isHTK is null
        defaultProgramShouldNotBeFound("isHTK.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsLabcorpIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isLabcorp equals to DEFAULT_IS_LABCORP
        defaultProgramShouldBeFound("isLabcorp.equals=" + DEFAULT_IS_LABCORP);

        // Get all the programList where isLabcorp equals to UPDATED_IS_LABCORP
        defaultProgramShouldNotBeFound("isLabcorp.equals=" + UPDATED_IS_LABCORP);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsLabcorpIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isLabcorp in DEFAULT_IS_LABCORP or UPDATED_IS_LABCORP
        defaultProgramShouldBeFound("isLabcorp.in=" + DEFAULT_IS_LABCORP + "," + UPDATED_IS_LABCORP);

        // Get all the programList where isLabcorp equals to UPDATED_IS_LABCORP
        defaultProgramShouldNotBeFound("isLabcorp.in=" + UPDATED_IS_LABCORP);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsLabcorpIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isLabcorp is not null
        defaultProgramShouldBeFound("isLabcorp.specified=true");

        // Get all the programList where isLabcorp is null
        defaultProgramShouldNotBeFound("isLabcorp.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLabcorpAccountNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where labcorpAccountNumber equals to DEFAULT_LABCORP_ACCOUNT_NUMBER
        defaultProgramShouldBeFound("labcorpAccountNumber.equals=" + DEFAULT_LABCORP_ACCOUNT_NUMBER);

        // Get all the programList where labcorpAccountNumber equals to UPDATED_LABCORP_ACCOUNT_NUMBER
        defaultProgramShouldNotBeFound("labcorpAccountNumber.equals=" + UPDATED_LABCORP_ACCOUNT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllProgramsByLabcorpAccountNumberIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where labcorpAccountNumber in DEFAULT_LABCORP_ACCOUNT_NUMBER or UPDATED_LABCORP_ACCOUNT_NUMBER
        defaultProgramShouldBeFound("labcorpAccountNumber.in=" + DEFAULT_LABCORP_ACCOUNT_NUMBER + "," + UPDATED_LABCORP_ACCOUNT_NUMBER);

        // Get all the programList where labcorpAccountNumber equals to UPDATED_LABCORP_ACCOUNT_NUMBER
        defaultProgramShouldNotBeFound("labcorpAccountNumber.in=" + UPDATED_LABCORP_ACCOUNT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllProgramsByLabcorpAccountNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where labcorpAccountNumber is not null
        defaultProgramShouldBeFound("labcorpAccountNumber.specified=true");

        // Get all the programList where labcorpAccountNumber is null
        defaultProgramShouldNotBeFound("labcorpAccountNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLabcorpFileUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where labcorpFileUrl equals to DEFAULT_LABCORP_FILE_URL
        defaultProgramShouldBeFound("labcorpFileUrl.equals=" + DEFAULT_LABCORP_FILE_URL);

        // Get all the programList where labcorpFileUrl equals to UPDATED_LABCORP_FILE_URL
        defaultProgramShouldNotBeFound("labcorpFileUrl.equals=" + UPDATED_LABCORP_FILE_URL);
    }

    @Test
    @Transactional
    public void getAllProgramsByLabcorpFileUrlIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where labcorpFileUrl in DEFAULT_LABCORP_FILE_URL or UPDATED_LABCORP_FILE_URL
        defaultProgramShouldBeFound("labcorpFileUrl.in=" + DEFAULT_LABCORP_FILE_URL + "," + UPDATED_LABCORP_FILE_URL);

        // Get all the programList where labcorpFileUrl equals to UPDATED_LABCORP_FILE_URL
        defaultProgramShouldNotBeFound("labcorpFileUrl.in=" + UPDATED_LABCORP_FILE_URL);
    }

    @Test
    @Transactional
    public void getAllProgramsByLabcorpFileUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where labcorpFileUrl is not null
        defaultProgramShouldBeFound("labcorpFileUrl.specified=true");

        // Get all the programList where labcorpFileUrl is null
        defaultProgramShouldNotBeFound("labcorpFileUrl.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByApplyRewardAllSubgroupIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where applyRewardAllSubgroup equals to DEFAULT_APPLY_REWARD_ALL_SUBGROUP
        defaultProgramShouldBeFound("applyRewardAllSubgroup.equals=" + DEFAULT_APPLY_REWARD_ALL_SUBGROUP);

        // Get all the programList where applyRewardAllSubgroup equals to UPDATED_APPLY_REWARD_ALL_SUBGROUP
        defaultProgramShouldNotBeFound("applyRewardAllSubgroup.equals=" + UPDATED_APPLY_REWARD_ALL_SUBGROUP);
    }

    @Test
    @Transactional
    public void getAllProgramsByApplyRewardAllSubgroupIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where applyRewardAllSubgroup in DEFAULT_APPLY_REWARD_ALL_SUBGROUP or UPDATED_APPLY_REWARD_ALL_SUBGROUP
        defaultProgramShouldBeFound("applyRewardAllSubgroup.in=" + DEFAULT_APPLY_REWARD_ALL_SUBGROUP + "," + UPDATED_APPLY_REWARD_ALL_SUBGROUP);

        // Get all the programList where applyRewardAllSubgroup equals to UPDATED_APPLY_REWARD_ALL_SUBGROUP
        defaultProgramShouldNotBeFound("applyRewardAllSubgroup.in=" + UPDATED_APPLY_REWARD_ALL_SUBGROUP);
    }

    @Test
    @Transactional
    public void getAllProgramsByApplyRewardAllSubgroupIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where applyRewardAllSubgroup is not null
        defaultProgramShouldBeFound("applyRewardAllSubgroup.specified=true");

        // Get all the programList where applyRewardAllSubgroup is null
        defaultProgramShouldNotBeFound("applyRewardAllSubgroup.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByBiometricDeadlineDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where biometricDeadlineDate equals to DEFAULT_BIOMETRIC_DEADLINE_DATE
        defaultProgramShouldBeFound("biometricDeadlineDate.equals=" + DEFAULT_BIOMETRIC_DEADLINE_DATE);

        // Get all the programList where biometricDeadlineDate equals to UPDATED_BIOMETRIC_DEADLINE_DATE
        defaultProgramShouldNotBeFound("biometricDeadlineDate.equals=" + UPDATED_BIOMETRIC_DEADLINE_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByBiometricDeadlineDateIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where biometricDeadlineDate in DEFAULT_BIOMETRIC_DEADLINE_DATE or UPDATED_BIOMETRIC_DEADLINE_DATE
        defaultProgramShouldBeFound("biometricDeadlineDate.in=" + DEFAULT_BIOMETRIC_DEADLINE_DATE + "," + UPDATED_BIOMETRIC_DEADLINE_DATE);

        // Get all the programList where biometricDeadlineDate equals to UPDATED_BIOMETRIC_DEADLINE_DATE
        defaultProgramShouldNotBeFound("biometricDeadlineDate.in=" + UPDATED_BIOMETRIC_DEADLINE_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByBiometricDeadlineDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where biometricDeadlineDate is not null
        defaultProgramShouldBeFound("biometricDeadlineDate.specified=true");

        // Get all the programList where biometricDeadlineDate is null
        defaultProgramShouldNotBeFound("biometricDeadlineDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByBiometricLookbackDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where biometricLookbackDate equals to DEFAULT_BIOMETRIC_LOOKBACK_DATE
        defaultProgramShouldBeFound("biometricLookbackDate.equals=" + DEFAULT_BIOMETRIC_LOOKBACK_DATE);

        // Get all the programList where biometricLookbackDate equals to UPDATED_BIOMETRIC_LOOKBACK_DATE
        defaultProgramShouldNotBeFound("biometricLookbackDate.equals=" + UPDATED_BIOMETRIC_LOOKBACK_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByBiometricLookbackDateIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where biometricLookbackDate in DEFAULT_BIOMETRIC_LOOKBACK_DATE or UPDATED_BIOMETRIC_LOOKBACK_DATE
        defaultProgramShouldBeFound("biometricLookbackDate.in=" + DEFAULT_BIOMETRIC_LOOKBACK_DATE + "," + UPDATED_BIOMETRIC_LOOKBACK_DATE);

        // Get all the programList where biometricLookbackDate equals to UPDATED_BIOMETRIC_LOOKBACK_DATE
        defaultProgramShouldNotBeFound("biometricLookbackDate.in=" + UPDATED_BIOMETRIC_LOOKBACK_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramsByBiometricLookbackDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where biometricLookbackDate is not null
        defaultProgramShouldBeFound("biometricLookbackDate.specified=true");

        // Get all the programList where biometricLookbackDate is null
        defaultProgramShouldNotBeFound("biometricLookbackDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByLandingBackgroundImageUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where landingBackgroundImageUrl equals to DEFAULT_LANDING_BACKGROUND_IMAGE_URL
        defaultProgramShouldBeFound("landingBackgroundImageUrl.equals=" + DEFAULT_LANDING_BACKGROUND_IMAGE_URL);

        // Get all the programList where landingBackgroundImageUrl equals to UPDATED_LANDING_BACKGROUND_IMAGE_URL
        defaultProgramShouldNotBeFound("landingBackgroundImageUrl.equals=" + UPDATED_LANDING_BACKGROUND_IMAGE_URL);
    }

    @Test
    @Transactional
    public void getAllProgramsByLandingBackgroundImageUrlIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where landingBackgroundImageUrl in DEFAULT_LANDING_BACKGROUND_IMAGE_URL or UPDATED_LANDING_BACKGROUND_IMAGE_URL
        defaultProgramShouldBeFound("landingBackgroundImageUrl.in=" + DEFAULT_LANDING_BACKGROUND_IMAGE_URL + "," + UPDATED_LANDING_BACKGROUND_IMAGE_URL);

        // Get all the programList where landingBackgroundImageUrl equals to UPDATED_LANDING_BACKGROUND_IMAGE_URL
        defaultProgramShouldNotBeFound("landingBackgroundImageUrl.in=" + UPDATED_LANDING_BACKGROUND_IMAGE_URL);
    }

    @Test
    @Transactional
    public void getAllProgramsByLandingBackgroundImageUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where landingBackgroundImageUrl is not null
        defaultProgramShouldBeFound("landingBackgroundImageUrl.specified=true");

        // Get all the programList where landingBackgroundImageUrl is null
        defaultProgramShouldNotBeFound("landingBackgroundImageUrl.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsTobaccoSurchargeManagementIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isTobaccoSurchargeManagement equals to DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT
        defaultProgramShouldBeFound("isTobaccoSurchargeManagement.equals=" + DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT);

        // Get all the programList where isTobaccoSurchargeManagement equals to UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT
        defaultProgramShouldNotBeFound("isTobaccoSurchargeManagement.equals=" + UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsTobaccoSurchargeManagementIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isTobaccoSurchargeManagement in DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT or UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT
        defaultProgramShouldBeFound("isTobaccoSurchargeManagement.in=" + DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT + "," + UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT);

        // Get all the programList where isTobaccoSurchargeManagement equals to UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT
        defaultProgramShouldNotBeFound("isTobaccoSurchargeManagement.in=" + UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsTobaccoSurchargeManagementIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isTobaccoSurchargeManagement is not null
        defaultProgramShouldBeFound("isTobaccoSurchargeManagement.specified=true");

        // Get all the programList where isTobaccoSurchargeManagement is null
        defaultProgramShouldNotBeFound("isTobaccoSurchargeManagement.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByTobaccoAttestationDeadlineIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where tobaccoAttestationDeadline equals to DEFAULT_TOBACCO_ATTESTATION_DEADLINE
        defaultProgramShouldBeFound("tobaccoAttestationDeadline.equals=" + DEFAULT_TOBACCO_ATTESTATION_DEADLINE);

        // Get all the programList where tobaccoAttestationDeadline equals to UPDATED_TOBACCO_ATTESTATION_DEADLINE
        defaultProgramShouldNotBeFound("tobaccoAttestationDeadline.equals=" + UPDATED_TOBACCO_ATTESTATION_DEADLINE);
    }

    @Test
    @Transactional
    public void getAllProgramsByTobaccoAttestationDeadlineIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where tobaccoAttestationDeadline in DEFAULT_TOBACCO_ATTESTATION_DEADLINE or UPDATED_TOBACCO_ATTESTATION_DEADLINE
        defaultProgramShouldBeFound("tobaccoAttestationDeadline.in=" + DEFAULT_TOBACCO_ATTESTATION_DEADLINE + "," + UPDATED_TOBACCO_ATTESTATION_DEADLINE);

        // Get all the programList where tobaccoAttestationDeadline equals to UPDATED_TOBACCO_ATTESTATION_DEADLINE
        defaultProgramShouldNotBeFound("tobaccoAttestationDeadline.in=" + UPDATED_TOBACCO_ATTESTATION_DEADLINE);
    }

    @Test
    @Transactional
    public void getAllProgramsByTobaccoAttestationDeadlineIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where tobaccoAttestationDeadline is not null
        defaultProgramShouldBeFound("tobaccoAttestationDeadline.specified=true");

        // Get all the programList where tobaccoAttestationDeadline is null
        defaultProgramShouldNotBeFound("tobaccoAttestationDeadline.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByTobaccoProgramDeadlineIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where tobaccoProgramDeadline equals to DEFAULT_TOBACCO_PROGRAM_DEADLINE
        defaultProgramShouldBeFound("tobaccoProgramDeadline.equals=" + DEFAULT_TOBACCO_PROGRAM_DEADLINE);

        // Get all the programList where tobaccoProgramDeadline equals to UPDATED_TOBACCO_PROGRAM_DEADLINE
        defaultProgramShouldNotBeFound("tobaccoProgramDeadline.equals=" + UPDATED_TOBACCO_PROGRAM_DEADLINE);
    }

    @Test
    @Transactional
    public void getAllProgramsByTobaccoProgramDeadlineIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where tobaccoProgramDeadline in DEFAULT_TOBACCO_PROGRAM_DEADLINE or UPDATED_TOBACCO_PROGRAM_DEADLINE
        defaultProgramShouldBeFound("tobaccoProgramDeadline.in=" + DEFAULT_TOBACCO_PROGRAM_DEADLINE + "," + UPDATED_TOBACCO_PROGRAM_DEADLINE);

        // Get all the programList where tobaccoProgramDeadline equals to UPDATED_TOBACCO_PROGRAM_DEADLINE
        defaultProgramShouldNotBeFound("tobaccoProgramDeadline.in=" + UPDATED_TOBACCO_PROGRAM_DEADLINE);
    }

    @Test
    @Transactional
    public void getAllProgramsByTobaccoProgramDeadlineIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where tobaccoProgramDeadline is not null
        defaultProgramShouldBeFound("tobaccoProgramDeadline.specified=true");

        // Get all the programList where tobaccoProgramDeadline is null
        defaultProgramShouldNotBeFound("tobaccoProgramDeadline.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsAwardedForTobaccoAttestationIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isAwardedForTobaccoAttestation equals to DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION
        defaultProgramShouldBeFound("isAwardedForTobaccoAttestation.equals=" + DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION);

        // Get all the programList where isAwardedForTobaccoAttestation equals to UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION
        defaultProgramShouldNotBeFound("isAwardedForTobaccoAttestation.equals=" + UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsAwardedForTobaccoAttestationIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isAwardedForTobaccoAttestation in DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION or UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION
        defaultProgramShouldBeFound("isAwardedForTobaccoAttestation.in=" + DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION + "," + UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION);

        // Get all the programList where isAwardedForTobaccoAttestation equals to UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION
        defaultProgramShouldNotBeFound("isAwardedForTobaccoAttestation.in=" + UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsAwardedForTobaccoAttestationIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isAwardedForTobaccoAttestation is not null
        defaultProgramShouldBeFound("isAwardedForTobaccoAttestation.specified=true");

        // Get all the programList where isAwardedForTobaccoAttestation is null
        defaultProgramShouldNotBeFound("isAwardedForTobaccoAttestation.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByIsAwardedForTobaccoRasIsEqualToSomething() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isAwardedForTobaccoRas equals to DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS
        defaultProgramShouldBeFound("isAwardedForTobaccoRas.equals=" + DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS);

        // Get all the programList where isAwardedForTobaccoRas equals to UPDATED_IS_AWARDED_FOR_TOBACCO_RAS
        defaultProgramShouldNotBeFound("isAwardedForTobaccoRas.equals=" + UPDATED_IS_AWARDED_FOR_TOBACCO_RAS);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsAwardedForTobaccoRasIsInShouldWork() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isAwardedForTobaccoRas in DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS or UPDATED_IS_AWARDED_FOR_TOBACCO_RAS
        defaultProgramShouldBeFound("isAwardedForTobaccoRas.in=" + DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS + "," + UPDATED_IS_AWARDED_FOR_TOBACCO_RAS);

        // Get all the programList where isAwardedForTobaccoRas equals to UPDATED_IS_AWARDED_FOR_TOBACCO_RAS
        defaultProgramShouldNotBeFound("isAwardedForTobaccoRas.in=" + UPDATED_IS_AWARDED_FOR_TOBACCO_RAS);
    }

    @Test
    @Transactional
    public void getAllProgramsByIsAwardedForTobaccoRasIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        // Get all the programList where isAwardedForTobaccoRas is not null
        defaultProgramShouldBeFound("isAwardedForTobaccoRas.specified=true");

        // Get all the programList where isAwardedForTobaccoRas is null
        defaultProgramShouldNotBeFound("isAwardedForTobaccoRas.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramsByProgramCategoryPointIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCategoryPoint programCategoryPoint = ProgramCategoryPointResourceIT.createEntity(em);
        em.persist(programCategoryPoint);
        em.flush();
        program.addProgramCategoryPoint(programCategoryPoint);
        programRepository.saveAndFlush(program);
        Long programCategoryPointId = programCategoryPoint.getId();

        // Get all the programList where programCategoryPoint equals to programCategoryPointId
        defaultProgramShouldBeFound("programCategoryPointId.equals=" + programCategoryPointId);

        // Get all the programList where programCategoryPoint equals to programCategoryPointId + 1
        defaultProgramShouldNotBeFound("programCategoryPointId.equals=" + (programCategoryPointId + 1));
    }


    @Test
    @Transactional
    public void getAllProgramsByProgramHealthyRangeIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramHealthyRange programHealthyRange = ProgramHealthyRangeResourceIT.createEntity(em);
        em.persist(programHealthyRange);
        em.flush();
        program.addProgramHealthyRange(programHealthyRange);
        programRepository.saveAndFlush(program);
        Long programHealthyRangeId = programHealthyRange.getId();

        // Get all the programList where programHealthyRange equals to programHealthyRangeId
        defaultProgramShouldBeFound("programHealthyRangeId.equals=" + programHealthyRangeId);

        // Get all the programList where programHealthyRange equals to programHealthyRangeId + 1
        defaultProgramShouldNotBeFound("programHealthyRangeId.equals=" + (programHealthyRangeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramShouldBeFound(String filter) throws Exception {
        restProgramMockMvc.perform(get("/api/programs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(program.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].lastSent").value(hasItem(DEFAULT_LAST_SENT.toString())))
            .andExpect(jsonPath("$.[*].isRetriggerEmail").value(hasItem(DEFAULT_IS_RETRIGGER_EMAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].isEligible").value(hasItem(DEFAULT_IS_ELIGIBLE.booleanValue())))
            .andExpect(jsonPath("$.[*].isSentRegistrationEmail").value(hasItem(DEFAULT_IS_SENT_REGISTRATION_EMAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].isRegisteredForPlatform").value(hasItem(DEFAULT_IS_REGISTERED_FOR_PLATFORM.booleanValue())))
            .andExpect(jsonPath("$.[*].isScheduledScreening").value(hasItem(DEFAULT_IS_SCHEDULED_SCREENING.booleanValue())))
            .andExpect(jsonPath("$.[*].isFunctionally").value(hasItem(DEFAULT_IS_FUNCTIONALLY.booleanValue())))
            .andExpect(jsonPath("$.[*].logoUrl").value(hasItem(DEFAULT_LOGO_URL)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].userPoint").value(hasItem(DEFAULT_USER_POINT.intValue())))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].isUsePoint").value(hasItem(DEFAULT_IS_USE_POINT.booleanValue())))
            .andExpect(jsonPath("$.[*].isScreen").value(hasItem(DEFAULT_IS_SCREEN.booleanValue())))
            .andExpect(jsonPath("$.[*].isCoaching").value(hasItem(DEFAULT_IS_COACHING.booleanValue())))
            .andExpect(jsonPath("$.[*].isWellMatric").value(hasItem(DEFAULT_IS_WELL_MATRIC.booleanValue())))
            .andExpect(jsonPath("$.[*].isHP").value(hasItem(DEFAULT_IS_HP.booleanValue())))
            .andExpect(jsonPath("$.[*].isUseLevel").value(hasItem(DEFAULT_IS_USE_LEVEL.booleanValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].resetDate").value(hasItem(DEFAULT_RESET_DATE.toString())))
            .andExpect(jsonPath("$.[*].isTemplate").value(hasItem(DEFAULT_IS_TEMPLATE.booleanValue())))
            .andExpect(jsonPath("$.[*].isPreview").value(hasItem(DEFAULT_IS_PREVIEW.booleanValue())))
            .andExpect(jsonPath("$.[*].previewDate").value(hasItem(DEFAULT_PREVIEW_DATE.toString())))
            .andExpect(jsonPath("$.[*].previewTimeZone").value(hasItem(DEFAULT_PREVIEW_TIME_ZONE)))
            .andExpect(jsonPath("$.[*].startTimeZone").value(hasItem(DEFAULT_START_TIME_ZONE)))
            .andExpect(jsonPath("$.[*].endTimeZone").value(hasItem(DEFAULT_END_TIME_ZONE)))
            .andExpect(jsonPath("$.[*].levelStructure").value(hasItem(DEFAULT_LEVEL_STRUCTURE)))
            .andExpect(jsonPath("$.[*].programLength").value(hasItem(DEFAULT_PROGRAM_LENGTH)))
            .andExpect(jsonPath("$.[*].isHPSF").value(hasItem(DEFAULT_IS_HPSF.booleanValue())))
            .andExpect(jsonPath("$.[*].isHTK").value(hasItem(DEFAULT_IS_HTK.booleanValue())))
            .andExpect(jsonPath("$.[*].isLabcorp").value(hasItem(DEFAULT_IS_LABCORP.booleanValue())))
            .andExpect(jsonPath("$.[*].labcorpAccountNumber").value(hasItem(DEFAULT_LABCORP_ACCOUNT_NUMBER)))
            .andExpect(jsonPath("$.[*].labcorpFileUrl").value(hasItem(DEFAULT_LABCORP_FILE_URL)))
            .andExpect(jsonPath("$.[*].applyRewardAllSubgroup").value(hasItem(DEFAULT_APPLY_REWARD_ALL_SUBGROUP.booleanValue())))
            .andExpect(jsonPath("$.[*].biometricDeadlineDate").value(hasItem(DEFAULT_BIOMETRIC_DEADLINE_DATE.toString())))
            .andExpect(jsonPath("$.[*].biometricLookbackDate").value(hasItem(DEFAULT_BIOMETRIC_LOOKBACK_DATE.toString())))
            .andExpect(jsonPath("$.[*].landingBackgroundImageUrl").value(hasItem(DEFAULT_LANDING_BACKGROUND_IMAGE_URL)))
            .andExpect(jsonPath("$.[*].isTobaccoSurchargeManagement").value(hasItem(DEFAULT_IS_TOBACCO_SURCHARGE_MANAGEMENT.booleanValue())))
            .andExpect(jsonPath("$.[*].tobaccoAttestationDeadline").value(hasItem(DEFAULT_TOBACCO_ATTESTATION_DEADLINE.toString())))
            .andExpect(jsonPath("$.[*].tobaccoProgramDeadline").value(hasItem(DEFAULT_TOBACCO_PROGRAM_DEADLINE.toString())))
            .andExpect(jsonPath("$.[*].isAwardedForTobaccoAttestation").value(hasItem(DEFAULT_IS_AWARDED_FOR_TOBACCO_ATTESTATION.booleanValue())))
            .andExpect(jsonPath("$.[*].isAwardedForTobaccoRas").value(hasItem(DEFAULT_IS_AWARDED_FOR_TOBACCO_RAS.booleanValue())));

        // Check, that the count call also returns 1
        restProgramMockMvc.perform(get("/api/programs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramShouldNotBeFound(String filter) throws Exception {
        restProgramMockMvc.perform(get("/api/programs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramMockMvc.perform(get("/api/programs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgram() throws Exception {
        // Get the program
        restProgramMockMvc.perform(get("/api/programs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgram() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        int databaseSizeBeforeUpdate = programRepository.findAll().size();

        // Update the program
        Program updatedProgram = programRepository.findById(program.getId()).get();
        // Disconnect from session so that the updates on updatedProgram are not directly saved in db
        em.detach(updatedProgram);
        updatedProgram
            .name(UPDATED_NAME)
            .lastSent(UPDATED_LAST_SENT)
            .isRetriggerEmail(UPDATED_IS_RETRIGGER_EMAIL)
            .isEligible(UPDATED_IS_ELIGIBLE)
            .isSentRegistrationEmail(UPDATED_IS_SENT_REGISTRATION_EMAIL)
            .isRegisteredForPlatform(UPDATED_IS_REGISTERED_FOR_PLATFORM)
            .isScheduledScreening(UPDATED_IS_SCHEDULED_SCREENING)
            .isFunctionally(UPDATED_IS_FUNCTIONALLY)
            .logoUrl(UPDATED_LOGO_URL)
            .description(UPDATED_DESCRIPTION)
            .userPoint(UPDATED_USER_POINT)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .createdBy(UPDATED_CREATED_BY)
            .status(UPDATED_STATUS)
            .isUsePoint(UPDATED_IS_USE_POINT)
            .isScreen(UPDATED_IS_SCREEN)
            .isCoaching(UPDATED_IS_COACHING)
            .isWellMatric(UPDATED_IS_WELL_MATRIC)
            .isHP(UPDATED_IS_HP)
            .isUseLevel(UPDATED_IS_USE_LEVEL)
            .startDate(UPDATED_START_DATE)
            .resetDate(UPDATED_RESET_DATE)
            .isTemplate(UPDATED_IS_TEMPLATE)
            .isPreview(UPDATED_IS_PREVIEW)
            .previewDate(UPDATED_PREVIEW_DATE)
            .previewTimeZone(UPDATED_PREVIEW_TIME_ZONE)
            .startTimeZone(UPDATED_START_TIME_ZONE)
            .endTimeZone(UPDATED_END_TIME_ZONE)
            .levelStructure(UPDATED_LEVEL_STRUCTURE)
            .programLength(UPDATED_PROGRAM_LENGTH)
            .isHPSF(UPDATED_IS_HPSF)
            .isHTK(UPDATED_IS_HTK)
            .isLabcorp(UPDATED_IS_LABCORP)
            .labcorpAccountNumber(UPDATED_LABCORP_ACCOUNT_NUMBER)
            .labcorpFileUrl(UPDATED_LABCORP_FILE_URL)
            .applyRewardAllSubgroup(UPDATED_APPLY_REWARD_ALL_SUBGROUP)
            .biometricDeadlineDate(UPDATED_BIOMETRIC_DEADLINE_DATE)
            .biometricLookbackDate(UPDATED_BIOMETRIC_LOOKBACK_DATE)
            .landingBackgroundImageUrl(UPDATED_LANDING_BACKGROUND_IMAGE_URL)
            .isTobaccoSurchargeManagement(UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT)
            .tobaccoAttestationDeadline(UPDATED_TOBACCO_ATTESTATION_DEADLINE)
            .tobaccoProgramDeadline(UPDATED_TOBACCO_PROGRAM_DEADLINE)
            .isAwardedForTobaccoAttestation(UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION)
            .isAwardedForTobaccoRas(UPDATED_IS_AWARDED_FOR_TOBACCO_RAS);
        ProgramDTO programDTO = programMapper.toDto(updatedProgram);

        restProgramMockMvc.perform(put("/api/programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programDTO)))
            .andExpect(status().isOk());

        // Validate the Program in the database
        List<Program> programList = programRepository.findAll();
        assertThat(programList).hasSize(databaseSizeBeforeUpdate);
        Program testProgram = programList.get(programList.size() - 1);
        assertThat(testProgram.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProgram.getLastSent()).isEqualTo(UPDATED_LAST_SENT);
        assertThat(testProgram.isIsRetriggerEmail()).isEqualTo(UPDATED_IS_RETRIGGER_EMAIL);
        assertThat(testProgram.isIsEligible()).isEqualTo(UPDATED_IS_ELIGIBLE);
        assertThat(testProgram.isIsSentRegistrationEmail()).isEqualTo(UPDATED_IS_SENT_REGISTRATION_EMAIL);
        assertThat(testProgram.isIsRegisteredForPlatform()).isEqualTo(UPDATED_IS_REGISTERED_FOR_PLATFORM);
        assertThat(testProgram.isIsScheduledScreening()).isEqualTo(UPDATED_IS_SCHEDULED_SCREENING);
        assertThat(testProgram.isIsFunctionally()).isEqualTo(UPDATED_IS_FUNCTIONALLY);
        assertThat(testProgram.getLogoUrl()).isEqualTo(UPDATED_LOGO_URL);
        assertThat(testProgram.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProgram.getUserPoint()).isEqualTo(UPDATED_USER_POINT);
        assertThat(testProgram.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testProgram.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgram.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testProgram.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testProgram.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testProgram.isIsUsePoint()).isEqualTo(UPDATED_IS_USE_POINT);
        assertThat(testProgram.isIsScreen()).isEqualTo(UPDATED_IS_SCREEN);
        assertThat(testProgram.isIsCoaching()).isEqualTo(UPDATED_IS_COACHING);
        assertThat(testProgram.isIsWellMatric()).isEqualTo(UPDATED_IS_WELL_MATRIC);
        assertThat(testProgram.isIsHp()).isEqualTo(UPDATED_IS_HP);
        assertThat(testProgram.isIsUseLevel()).isEqualTo(UPDATED_IS_USE_LEVEL);
        assertThat(testProgram.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testProgram.getResetDate()).isEqualTo(UPDATED_RESET_DATE);
        assertThat(testProgram.isIsTemplate()).isEqualTo(UPDATED_IS_TEMPLATE);
        assertThat(testProgram.isIsPreview()).isEqualTo(UPDATED_IS_PREVIEW);
        assertThat(testProgram.getPreviewDate()).isEqualTo(UPDATED_PREVIEW_DATE);
        assertThat(testProgram.getPreviewTimeZone()).isEqualTo(UPDATED_PREVIEW_TIME_ZONE);
        assertThat(testProgram.getStartTimeZone()).isEqualTo(UPDATED_START_TIME_ZONE);
        assertThat(testProgram.getEndTimeZone()).isEqualTo(UPDATED_END_TIME_ZONE);
        assertThat(testProgram.getLevelStructure()).isEqualTo(UPDATED_LEVEL_STRUCTURE);
        assertThat(testProgram.getProgramLength()).isEqualTo(UPDATED_PROGRAM_LENGTH);
        assertThat(testProgram.isIsHPSF()).isEqualTo(UPDATED_IS_HPSF);
        assertThat(testProgram.isIsHTK()).isEqualTo(UPDATED_IS_HTK);
        assertThat(testProgram.isIsLabcorp()).isEqualTo(UPDATED_IS_LABCORP);
        assertThat(testProgram.getLabcorpAccountNumber()).isEqualTo(UPDATED_LABCORP_ACCOUNT_NUMBER);
        assertThat(testProgram.getLabcorpFileUrl()).isEqualTo(UPDATED_LABCORP_FILE_URL);
        assertThat(testProgram.isApplyRewardAllSubgroup()).isEqualTo(UPDATED_APPLY_REWARD_ALL_SUBGROUP);
        assertThat(testProgram.getBiometricDeadlineDate()).isEqualTo(UPDATED_BIOMETRIC_DEADLINE_DATE);
        assertThat(testProgram.getBiometricLookbackDate()).isEqualTo(UPDATED_BIOMETRIC_LOOKBACK_DATE);
        assertThat(testProgram.getLandingBackgroundImageUrl()).isEqualTo(UPDATED_LANDING_BACKGROUND_IMAGE_URL);
        assertThat(testProgram.isIsTobaccoSurchargeManagement()).isEqualTo(UPDATED_IS_TOBACCO_SURCHARGE_MANAGEMENT);
        assertThat(testProgram.getTobaccoAttestationDeadline()).isEqualTo(UPDATED_TOBACCO_ATTESTATION_DEADLINE);
        assertThat(testProgram.getTobaccoProgramDeadline()).isEqualTo(UPDATED_TOBACCO_PROGRAM_DEADLINE);
        assertThat(testProgram.isIsAwardedForTobaccoAttestation()).isEqualTo(UPDATED_IS_AWARDED_FOR_TOBACCO_ATTESTATION);
        assertThat(testProgram.isIsAwardedForTobaccoRas()).isEqualTo(UPDATED_IS_AWARDED_FOR_TOBACCO_RAS);
    }

    @Test
    @Transactional
    public void updateNonExistingProgram() throws Exception {
        int databaseSizeBeforeUpdate = programRepository.findAll().size();

        // Create the Program
        ProgramDTO programDTO = programMapper.toDto(program);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramMockMvc.perform(put("/api/programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Program in the database
        List<Program> programList = programRepository.findAll();
        assertThat(programList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgram() throws Exception {
        // Initialize the database
        programRepository.saveAndFlush(program);

        int databaseSizeBeforeDelete = programRepository.findAll().size();

        // Delete the program
        restProgramMockMvc.perform(delete("/api/programs/{id}", program.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Program> programList = programRepository.findAll();
        assertThat(programList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Program.class);
        Program program1 = new Program();
        program1.setId(1L);
        Program program2 = new Program();
        program2.setId(program1.getId());
        assertThat(program1).isEqualTo(program2);
        program2.setId(2L);
        assertThat(program1).isNotEqualTo(program2);
        program1.setId(null);
        assertThat(program1).isNotEqualTo(program2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramDTO.class);
        ProgramDTO programDTO1 = new ProgramDTO();
        programDTO1.setId(1L);
        ProgramDTO programDTO2 = new ProgramDTO();
        assertThat(programDTO1).isNotEqualTo(programDTO2);
        programDTO2.setId(programDTO1.getId());
        assertThat(programDTO1).isEqualTo(programDTO2);
        programDTO2.setId(2L);
        assertThat(programDTO1).isNotEqualTo(programDTO2);
        programDTO1.setId(null);
        assertThat(programDTO1).isNotEqualTo(programDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programMapper.fromId(null)).isNull();
    }
}
