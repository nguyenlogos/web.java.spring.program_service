package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramCohortCollection;
import aduro.basic.programservice.domain.ProgramCollection;
import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.repository.ProgramCohortCollectionRepository;
import aduro.basic.programservice.service.ProgramCohortCollectionService;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortCollectionMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramCohortCollectionCriteria;
import aduro.basic.programservice.service.ProgramCohortCollectionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramCohortCollectionResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramCohortCollectionResourceIT {

    private static final Integer DEFAULT_REQUIRED_COMPLETION = 1;
    private static final Integer UPDATED_REQUIRED_COMPLETION = 2;

    private static final Integer DEFAULT_REQUIRED_LEVEL = 1;
    private static final Integer UPDATED_REQUIRED_LEVEL = 2;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramCohortCollectionRepository programCohortCollectionRepository;

    @Autowired
    private ProgramCohortCollectionMapper programCohortCollectionMapper;

    @Autowired
    private ProgramCohortCollectionService programCohortCollectionService;

    @Autowired
    private ProgramCohortCollectionQueryService programCohortCollectionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramCohortCollectionMockMvc;

    private ProgramCohortCollection programCohortCollection;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramCohortCollectionResource programCohortCollectionResource = new ProgramCohortCollectionResource(programCohortCollectionService, programCohortCollectionQueryService);
        this.restProgramCohortCollectionMockMvc = MockMvcBuilders.standaloneSetup(programCohortCollectionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCohortCollection createEntity(EntityManager em) {
        ProgramCohortCollection programCohortCollection = new ProgramCohortCollection()
            .requiredCompletion(DEFAULT_REQUIRED_COMPLETION)
            .requiredLevel(DEFAULT_REQUIRED_LEVEL)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        return programCohortCollection;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCohortCollection createUpdatedEntity(EntityManager em) {
        ProgramCohortCollection programCohortCollection = new ProgramCohortCollection()
            .requiredCompletion(UPDATED_REQUIRED_COMPLETION)
            .requiredLevel(UPDATED_REQUIRED_LEVEL)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        return programCohortCollection;
    }

    @BeforeEach
    public void initTest() {
        programCohortCollection = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramCohortCollection() throws Exception {
        int databaseSizeBeforeCreate = programCohortCollectionRepository.findAll().size();

        // Create the ProgramCohortCollection
        ProgramCohortCollectionDTO programCohortCollectionDTO = programCohortCollectionMapper.toDto(programCohortCollection);
        restProgramCohortCollectionMockMvc.perform(post("/api/program-cohort-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortCollectionDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramCohortCollection in the database
        List<ProgramCohortCollection> programCohortCollectionList = programCohortCollectionRepository.findAll();
        assertThat(programCohortCollectionList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramCohortCollection testProgramCohortCollection = programCohortCollectionList.get(programCohortCollectionList.size() - 1);
        assertThat(testProgramCohortCollection.getRequiredCompletion()).isEqualTo(DEFAULT_REQUIRED_COMPLETION);
        assertThat(testProgramCohortCollection.getRequiredLevel()).isEqualTo(DEFAULT_REQUIRED_LEVEL);
        assertThat(testProgramCohortCollection.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramCohortCollection.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramCohortCollectionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programCohortCollectionRepository.findAll().size();

        // Create the ProgramCohortCollection with an existing ID
        programCohortCollection.setId(1L);
        ProgramCohortCollectionDTO programCohortCollectionDTO = programCohortCollectionMapper.toDto(programCohortCollection);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramCohortCollectionMockMvc.perform(post("/api/program-cohort-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortCollectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCohortCollection in the database
        List<ProgramCohortCollection> programCohortCollectionList = programCohortCollectionRepository.findAll();
        assertThat(programCohortCollectionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProgramCohortCollections() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList
        restProgramCohortCollectionMockMvc.perform(get("/api/program-cohort-collections?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCohortCollection.getId().intValue())))
            .andExpect(jsonPath("$.[*].requiredCompletion").value(hasItem(DEFAULT_REQUIRED_COMPLETION)))
            .andExpect(jsonPath("$.[*].requiredLevel").value(hasItem(DEFAULT_REQUIRED_LEVEL)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramCohortCollection() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get the programCohortCollection
        restProgramCohortCollectionMockMvc.perform(get("/api/program-cohort-collections/{id}", programCohortCollection.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programCohortCollection.getId().intValue()))
            .andExpect(jsonPath("$.requiredCompletion").value(DEFAULT_REQUIRED_COMPLETION))
            .andExpect(jsonPath("$.requiredLevel").value(DEFAULT_REQUIRED_LEVEL))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredCompletionIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredCompletion equals to DEFAULT_REQUIRED_COMPLETION
        defaultProgramCohortCollectionShouldBeFound("requiredCompletion.equals=" + DEFAULT_REQUIRED_COMPLETION);

        // Get all the programCohortCollectionList where requiredCompletion equals to UPDATED_REQUIRED_COMPLETION
        defaultProgramCohortCollectionShouldNotBeFound("requiredCompletion.equals=" + UPDATED_REQUIRED_COMPLETION);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredCompletionIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredCompletion in DEFAULT_REQUIRED_COMPLETION or UPDATED_REQUIRED_COMPLETION
        defaultProgramCohortCollectionShouldBeFound("requiredCompletion.in=" + DEFAULT_REQUIRED_COMPLETION + "," + UPDATED_REQUIRED_COMPLETION);

        // Get all the programCohortCollectionList where requiredCompletion equals to UPDATED_REQUIRED_COMPLETION
        defaultProgramCohortCollectionShouldNotBeFound("requiredCompletion.in=" + UPDATED_REQUIRED_COMPLETION);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredCompletionIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredCompletion is not null
        defaultProgramCohortCollectionShouldBeFound("requiredCompletion.specified=true");

        // Get all the programCohortCollectionList where requiredCompletion is null
        defaultProgramCohortCollectionShouldNotBeFound("requiredCompletion.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredCompletionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredCompletion greater than or equals to DEFAULT_REQUIRED_COMPLETION
        defaultProgramCohortCollectionShouldBeFound("requiredCompletion.greaterOrEqualThan=" + DEFAULT_REQUIRED_COMPLETION);

        // Get all the programCohortCollectionList where requiredCompletion greater than or equals to UPDATED_REQUIRED_COMPLETION
        defaultProgramCohortCollectionShouldNotBeFound("requiredCompletion.greaterOrEqualThan=" + UPDATED_REQUIRED_COMPLETION);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredCompletionIsLessThanSomething() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredCompletion less than or equals to DEFAULT_REQUIRED_COMPLETION
        defaultProgramCohortCollectionShouldNotBeFound("requiredCompletion.lessThan=" + DEFAULT_REQUIRED_COMPLETION);

        // Get all the programCohortCollectionList where requiredCompletion less than or equals to UPDATED_REQUIRED_COMPLETION
        defaultProgramCohortCollectionShouldBeFound("requiredCompletion.lessThan=" + UPDATED_REQUIRED_COMPLETION);
    }


    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredLevel equals to DEFAULT_REQUIRED_LEVEL
        defaultProgramCohortCollectionShouldBeFound("requiredLevel.equals=" + DEFAULT_REQUIRED_LEVEL);

        // Get all the programCohortCollectionList where requiredLevel equals to UPDATED_REQUIRED_LEVEL
        defaultProgramCohortCollectionShouldNotBeFound("requiredLevel.equals=" + UPDATED_REQUIRED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredLevelIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredLevel in DEFAULT_REQUIRED_LEVEL or UPDATED_REQUIRED_LEVEL
        defaultProgramCohortCollectionShouldBeFound("requiredLevel.in=" + DEFAULT_REQUIRED_LEVEL + "," + UPDATED_REQUIRED_LEVEL);

        // Get all the programCohortCollectionList where requiredLevel equals to UPDATED_REQUIRED_LEVEL
        defaultProgramCohortCollectionShouldNotBeFound("requiredLevel.in=" + UPDATED_REQUIRED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredLevel is not null
        defaultProgramCohortCollectionShouldBeFound("requiredLevel.specified=true");

        // Get all the programCohortCollectionList where requiredLevel is null
        defaultProgramCohortCollectionShouldNotBeFound("requiredLevel.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredLevel greater than or equals to DEFAULT_REQUIRED_LEVEL
        defaultProgramCohortCollectionShouldBeFound("requiredLevel.greaterOrEqualThan=" + DEFAULT_REQUIRED_LEVEL);

        // Get all the programCohortCollectionList where requiredLevel greater than or equals to UPDATED_REQUIRED_LEVEL
        defaultProgramCohortCollectionShouldNotBeFound("requiredLevel.greaterOrEqualThan=" + UPDATED_REQUIRED_LEVEL);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByRequiredLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where requiredLevel less than or equals to DEFAULT_REQUIRED_LEVEL
        defaultProgramCohortCollectionShouldNotBeFound("requiredLevel.lessThan=" + DEFAULT_REQUIRED_LEVEL);

        // Get all the programCohortCollectionList where requiredLevel less than or equals to UPDATED_REQUIRED_LEVEL
        defaultProgramCohortCollectionShouldBeFound("requiredLevel.lessThan=" + UPDATED_REQUIRED_LEVEL);
    }


    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramCohortCollectionShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programCohortCollectionList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCohortCollectionShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramCohortCollectionShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programCohortCollectionList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCohortCollectionShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where createdDate is not null
        defaultProgramCohortCollectionShouldBeFound("createdDate.specified=true");

        // Get all the programCohortCollectionList where createdDate is null
        defaultProgramCohortCollectionShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramCohortCollectionShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programCohortCollectionList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCohortCollectionShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramCohortCollectionShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programCohortCollectionList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCohortCollectionShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        // Get all the programCohortCollectionList where modifiedDate is not null
        defaultProgramCohortCollectionShouldBeFound("modifiedDate.specified=true");

        // Get all the programCohortCollectionList where modifiedDate is null
        defaultProgramCohortCollectionShouldNotBeFound("modifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByProgramCollectionIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCollection programCollection = ProgramCollectionResourceIT.createEntity(em);
        em.persist(programCollection);
        em.flush();
        programCohortCollection.setProgramCollection(programCollection);
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);
        Long programCollectionId = programCollection.getId();

        // Get all the programCohortCollectionList where programCollection equals to programCollectionId
        defaultProgramCohortCollectionShouldBeFound("programCollectionId.equals=" + programCollectionId);

        // Get all the programCohortCollectionList where programCollection equals to programCollectionId + 1
        defaultProgramCohortCollectionShouldNotBeFound("programCollectionId.equals=" + (programCollectionId + 1));
    }


    @Test
    @Transactional
    public void getAllProgramCohortCollectionsByProgramCohortIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCohort programCohort = ProgramCohortResourceIT.createEntity(em);
        em.persist(programCohort);
        em.flush();
        programCohortCollection.setProgramCohort(programCohort);
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);
        Long programCohortId = programCohort.getId();

        // Get all the programCohortCollectionList where programCohort equals to programCohortId
        defaultProgramCohortCollectionShouldBeFound("programCohortId.equals=" + programCohortId);

        // Get all the programCohortCollectionList where programCohort equals to programCohortId + 1
        defaultProgramCohortCollectionShouldNotBeFound("programCohortId.equals=" + (programCohortId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramCohortCollectionShouldBeFound(String filter) throws Exception {
        restProgramCohortCollectionMockMvc.perform(get("/api/program-cohort-collections?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCohortCollection.getId().intValue())))
            .andExpect(jsonPath("$.[*].requiredCompletion").value(hasItem(DEFAULT_REQUIRED_COMPLETION)))
            .andExpect(jsonPath("$.[*].requiredLevel").value(hasItem(DEFAULT_REQUIRED_LEVEL)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramCohortCollectionMockMvc.perform(get("/api/program-cohort-collections/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramCohortCollectionShouldNotBeFound(String filter) throws Exception {
        restProgramCohortCollectionMockMvc.perform(get("/api/program-cohort-collections?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramCohortCollectionMockMvc.perform(get("/api/program-cohort-collections/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramCohortCollection() throws Exception {
        // Get the programCohortCollection
        restProgramCohortCollectionMockMvc.perform(get("/api/program-cohort-collections/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramCohortCollection() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        int databaseSizeBeforeUpdate = programCohortCollectionRepository.findAll().size();

        // Update the programCohortCollection
        ProgramCohortCollection updatedProgramCohortCollection = programCohortCollectionRepository.findById(programCohortCollection.getId()).get();
        // Disconnect from session so that the updates on updatedProgramCohortCollection are not directly saved in db
        em.detach(updatedProgramCohortCollection);
        updatedProgramCohortCollection
            .requiredCompletion(UPDATED_REQUIRED_COMPLETION)
            .requiredLevel(UPDATED_REQUIRED_LEVEL)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        ProgramCohortCollectionDTO programCohortCollectionDTO = programCohortCollectionMapper.toDto(updatedProgramCohortCollection);

        restProgramCohortCollectionMockMvc.perform(put("/api/program-cohort-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortCollectionDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramCohortCollection in the database
        List<ProgramCohortCollection> programCohortCollectionList = programCohortCollectionRepository.findAll();
        assertThat(programCohortCollectionList).hasSize(databaseSizeBeforeUpdate);
        ProgramCohortCollection testProgramCohortCollection = programCohortCollectionList.get(programCohortCollectionList.size() - 1);
        assertThat(testProgramCohortCollection.getRequiredCompletion()).isEqualTo(UPDATED_REQUIRED_COMPLETION);
        assertThat(testProgramCohortCollection.getRequiredLevel()).isEqualTo(UPDATED_REQUIRED_LEVEL);
        assertThat(testProgramCohortCollection.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramCohortCollection.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramCohortCollection() throws Exception {
        int databaseSizeBeforeUpdate = programCohortCollectionRepository.findAll().size();

        // Create the ProgramCohortCollection
        ProgramCohortCollectionDTO programCohortCollectionDTO = programCohortCollectionMapper.toDto(programCohortCollection);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramCohortCollectionMockMvc.perform(put("/api/program-cohort-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortCollectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCohortCollection in the database
        List<ProgramCohortCollection> programCohortCollectionList = programCohortCollectionRepository.findAll();
        assertThat(programCohortCollectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramCohortCollection() throws Exception {
        // Initialize the database
        programCohortCollectionRepository.saveAndFlush(programCohortCollection);

        int databaseSizeBeforeDelete = programCohortCollectionRepository.findAll().size();

        // Delete the programCohortCollection
        restProgramCohortCollectionMockMvc.perform(delete("/api/program-cohort-collections/{id}", programCohortCollection.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramCohortCollection> programCohortCollectionList = programCohortCollectionRepository.findAll();
        assertThat(programCohortCollectionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCohortCollection.class);
        ProgramCohortCollection programCohortCollection1 = new ProgramCohortCollection();
        programCohortCollection1.setId(1L);
        ProgramCohortCollection programCohortCollection2 = new ProgramCohortCollection();
        programCohortCollection2.setId(programCohortCollection1.getId());
        assertThat(programCohortCollection1).isEqualTo(programCohortCollection2);
        programCohortCollection2.setId(2L);
        assertThat(programCohortCollection1).isNotEqualTo(programCohortCollection2);
        programCohortCollection1.setId(null);
        assertThat(programCohortCollection1).isNotEqualTo(programCohortCollection2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCohortCollectionDTO.class);
        ProgramCohortCollectionDTO programCohortCollectionDTO1 = new ProgramCohortCollectionDTO();
        programCohortCollectionDTO1.setId(1L);
        ProgramCohortCollectionDTO programCohortCollectionDTO2 = new ProgramCohortCollectionDTO();
        assertThat(programCohortCollectionDTO1).isNotEqualTo(programCohortCollectionDTO2);
        programCohortCollectionDTO2.setId(programCohortCollectionDTO1.getId());
        assertThat(programCohortCollectionDTO1).isEqualTo(programCohortCollectionDTO2);
        programCohortCollectionDTO2.setId(2L);
        assertThat(programCohortCollectionDTO1).isNotEqualTo(programCohortCollectionDTO2);
        programCohortCollectionDTO1.setId(null);
        assertThat(programCohortCollectionDTO1).isNotEqualTo(programCohortCollectionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programCohortCollectionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programCohortCollectionMapper.fromId(null)).isNull();
    }
}
