package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramUserCohort;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.repository.ProgramUserCohortRepository;
import aduro.basic.programservice.service.ProgramUserCohortService;
import aduro.basic.programservice.service.dto.ProgramUserCohortDTO;
import aduro.basic.programservice.service.mapper.ProgramUserCohortMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramUserCohortCriteria;
import aduro.basic.programservice.service.ProgramUserCohortQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import aduro.basic.programservice.domain.enumeration.CohortStatus;
/**
 * Integration tests for the {@Link ProgramUserCohortResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramUserCohortResourceIT {

    private static final Long DEFAULT_PROGRESS = 1L;
    private static final Long UPDATED_PROGRESS = 2L;

    private static final Integer DEFAULT_NUMBER_OF_PASS = 1;
    private static final Integer UPDATED_NUMBER_OF_PASS = 2;

    private static final Integer DEFAULT_NUMBER_OF_FAILURE = 1;
    private static final Integer UPDATED_NUMBER_OF_FAILURE = 2;

    private static final CohortStatus DEFAULT_STATUS = CohortStatus.INPROGRESS;
    private static final CohortStatus UPDATED_STATUS = CohortStatus.PENDING;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramUserCohortRepository programUserCohortRepository;

    @Autowired
    private ProgramUserCohortMapper programUserCohortMapper;

    @Autowired
    private ProgramUserCohortService programUserCohortService;

    @Autowired
    private ProgramUserCohortQueryService programUserCohortQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramUserCohortMockMvc;

    private ProgramUserCohort programUserCohort;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramUserCohortResource programUserCohortResource = new ProgramUserCohortResource(programUserCohortService, programUserCohortQueryService);
        this.restProgramUserCohortMockMvc = MockMvcBuilders.standaloneSetup(programUserCohortResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUserCohort createEntity(EntityManager em) {
        ProgramUserCohort programUserCohort = new ProgramUserCohort()
            .progress(DEFAULT_PROGRESS)
            .numberOfPass(DEFAULT_NUMBER_OF_PASS)
            .numberOfFailure(DEFAULT_NUMBER_OF_FAILURE)
            .status(DEFAULT_STATUS)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        return programUserCohort;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUserCohort createUpdatedEntity(EntityManager em) {
        ProgramUserCohort programUserCohort = new ProgramUserCohort()
            .progress(UPDATED_PROGRESS)
            .numberOfPass(UPDATED_NUMBER_OF_PASS)
            .numberOfFailure(UPDATED_NUMBER_OF_FAILURE)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        return programUserCohort;
    }

    @BeforeEach
    public void initTest() {
        programUserCohort = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramUserCohort() throws Exception {
        int databaseSizeBeforeCreate = programUserCohortRepository.findAll().size();

        // Create the ProgramUserCohort
        ProgramUserCohortDTO programUserCohortDTO = programUserCohortMapper.toDto(programUserCohort);
        restProgramUserCohortMockMvc.perform(post("/api/program-user-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCohortDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramUserCohort in the database
        List<ProgramUserCohort> programUserCohortList = programUserCohortRepository.findAll();
        assertThat(programUserCohortList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramUserCohort testProgramUserCohort = programUserCohortList.get(programUserCohortList.size() - 1);
        assertThat(testProgramUserCohort.getProgress()).isEqualTo(DEFAULT_PROGRESS);
        assertThat(testProgramUserCohort.getNumberOfPass()).isEqualTo(DEFAULT_NUMBER_OF_PASS);
        assertThat(testProgramUserCohort.getNumberOfFailure()).isEqualTo(DEFAULT_NUMBER_OF_FAILURE);
        assertThat(testProgramUserCohort.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testProgramUserCohort.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramUserCohort.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramUserCohortWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programUserCohortRepository.findAll().size();

        // Create the ProgramUserCohort with an existing ID
        programUserCohort.setId(1L);
        ProgramUserCohortDTO programUserCohortDTO = programUserCohortMapper.toDto(programUserCohort);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramUserCohortMockMvc.perform(post("/api/program-user-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCohortDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUserCohort in the database
        List<ProgramUserCohort> programUserCohortList = programUserCohortRepository.findAll();
        assertThat(programUserCohortList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserCohortRepository.findAll().size();
        // set the field null
        programUserCohort.setCreatedDate(null);

        // Create the ProgramUserCohort, which fails.
        ProgramUserCohortDTO programUserCohortDTO = programUserCohortMapper.toDto(programUserCohort);

        restProgramUserCohortMockMvc.perform(post("/api/program-user-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCohortDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserCohort> programUserCohortList = programUserCohortRepository.findAll();
        assertThat(programUserCohortList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModifiedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserCohortRepository.findAll().size();
        // set the field null
        programUserCohort.setModifiedDate(null);

        // Create the ProgramUserCohort, which fails.
        ProgramUserCohortDTO programUserCohortDTO = programUserCohortMapper.toDto(programUserCohort);

        restProgramUserCohortMockMvc.perform(post("/api/program-user-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCohortDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserCohort> programUserCohortList = programUserCohortRepository.findAll();
        assertThat(programUserCohortList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohorts() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList
        restProgramUserCohortMockMvc.perform(get("/api/program-user-cohorts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUserCohort.getId().intValue())))
            .andExpect(jsonPath("$.[*].progress").value(hasItem(DEFAULT_PROGRESS.intValue())))
            .andExpect(jsonPath("$.[*].numberOfPass").value(hasItem(DEFAULT_NUMBER_OF_PASS)))
            .andExpect(jsonPath("$.[*].numberOfFailure").value(hasItem(DEFAULT_NUMBER_OF_FAILURE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramUserCohort() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get the programUserCohort
        restProgramUserCohortMockMvc.perform(get("/api/program-user-cohorts/{id}", programUserCohort.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programUserCohort.getId().intValue()))
            .andExpect(jsonPath("$.progress").value(DEFAULT_PROGRESS.intValue()))
            .andExpect(jsonPath("$.numberOfPass").value(DEFAULT_NUMBER_OF_PASS))
            .andExpect(jsonPath("$.numberOfFailure").value(DEFAULT_NUMBER_OF_FAILURE))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByProgressIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where progress equals to DEFAULT_PROGRESS
        defaultProgramUserCohortShouldBeFound("progress.equals=" + DEFAULT_PROGRESS);

        // Get all the programUserCohortList where progress equals to UPDATED_PROGRESS
        defaultProgramUserCohortShouldNotBeFound("progress.equals=" + UPDATED_PROGRESS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByProgressIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where progress in DEFAULT_PROGRESS or UPDATED_PROGRESS
        defaultProgramUserCohortShouldBeFound("progress.in=" + DEFAULT_PROGRESS + "," + UPDATED_PROGRESS);

        // Get all the programUserCohortList where progress equals to UPDATED_PROGRESS
        defaultProgramUserCohortShouldNotBeFound("progress.in=" + UPDATED_PROGRESS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByProgressIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where progress is not null
        defaultProgramUserCohortShouldBeFound("progress.specified=true");

        // Get all the programUserCohortList where progress is null
        defaultProgramUserCohortShouldNotBeFound("progress.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByProgressIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where progress greater than or equals to DEFAULT_PROGRESS
        defaultProgramUserCohortShouldBeFound("progress.greaterOrEqualThan=" + DEFAULT_PROGRESS);

        // Get all the programUserCohortList where progress greater than or equals to UPDATED_PROGRESS
        defaultProgramUserCohortShouldNotBeFound("progress.greaterOrEqualThan=" + UPDATED_PROGRESS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByProgressIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where progress less than or equals to DEFAULT_PROGRESS
        defaultProgramUserCohortShouldNotBeFound("progress.lessThan=" + DEFAULT_PROGRESS);

        // Get all the programUserCohortList where progress less than or equals to UPDATED_PROGRESS
        defaultProgramUserCohortShouldBeFound("progress.lessThan=" + UPDATED_PROGRESS);
    }


    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfPassIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfPass equals to DEFAULT_NUMBER_OF_PASS
        defaultProgramUserCohortShouldBeFound("numberOfPass.equals=" + DEFAULT_NUMBER_OF_PASS);

        // Get all the programUserCohortList where numberOfPass equals to UPDATED_NUMBER_OF_PASS
        defaultProgramUserCohortShouldNotBeFound("numberOfPass.equals=" + UPDATED_NUMBER_OF_PASS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfPassIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfPass in DEFAULT_NUMBER_OF_PASS or UPDATED_NUMBER_OF_PASS
        defaultProgramUserCohortShouldBeFound("numberOfPass.in=" + DEFAULT_NUMBER_OF_PASS + "," + UPDATED_NUMBER_OF_PASS);

        // Get all the programUserCohortList where numberOfPass equals to UPDATED_NUMBER_OF_PASS
        defaultProgramUserCohortShouldNotBeFound("numberOfPass.in=" + UPDATED_NUMBER_OF_PASS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfPassIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfPass is not null
        defaultProgramUserCohortShouldBeFound("numberOfPass.specified=true");

        // Get all the programUserCohortList where numberOfPass is null
        defaultProgramUserCohortShouldNotBeFound("numberOfPass.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfPassIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfPass greater than or equals to DEFAULT_NUMBER_OF_PASS
        defaultProgramUserCohortShouldBeFound("numberOfPass.greaterOrEqualThan=" + DEFAULT_NUMBER_OF_PASS);

        // Get all the programUserCohortList where numberOfPass greater than or equals to UPDATED_NUMBER_OF_PASS
        defaultProgramUserCohortShouldNotBeFound("numberOfPass.greaterOrEqualThan=" + UPDATED_NUMBER_OF_PASS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfPassIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfPass less than or equals to DEFAULT_NUMBER_OF_PASS
        defaultProgramUserCohortShouldNotBeFound("numberOfPass.lessThan=" + DEFAULT_NUMBER_OF_PASS);

        // Get all the programUserCohortList where numberOfPass less than or equals to UPDATED_NUMBER_OF_PASS
        defaultProgramUserCohortShouldBeFound("numberOfPass.lessThan=" + UPDATED_NUMBER_OF_PASS);
    }


    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfFailureIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfFailure equals to DEFAULT_NUMBER_OF_FAILURE
        defaultProgramUserCohortShouldBeFound("numberOfFailure.equals=" + DEFAULT_NUMBER_OF_FAILURE);

        // Get all the programUserCohortList where numberOfFailure equals to UPDATED_NUMBER_OF_FAILURE
        defaultProgramUserCohortShouldNotBeFound("numberOfFailure.equals=" + UPDATED_NUMBER_OF_FAILURE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfFailureIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfFailure in DEFAULT_NUMBER_OF_FAILURE or UPDATED_NUMBER_OF_FAILURE
        defaultProgramUserCohortShouldBeFound("numberOfFailure.in=" + DEFAULT_NUMBER_OF_FAILURE + "," + UPDATED_NUMBER_OF_FAILURE);

        // Get all the programUserCohortList where numberOfFailure equals to UPDATED_NUMBER_OF_FAILURE
        defaultProgramUserCohortShouldNotBeFound("numberOfFailure.in=" + UPDATED_NUMBER_OF_FAILURE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfFailureIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfFailure is not null
        defaultProgramUserCohortShouldBeFound("numberOfFailure.specified=true");

        // Get all the programUserCohortList where numberOfFailure is null
        defaultProgramUserCohortShouldNotBeFound("numberOfFailure.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfFailureIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfFailure greater than or equals to DEFAULT_NUMBER_OF_FAILURE
        defaultProgramUserCohortShouldBeFound("numberOfFailure.greaterOrEqualThan=" + DEFAULT_NUMBER_OF_FAILURE);

        // Get all the programUserCohortList where numberOfFailure greater than or equals to UPDATED_NUMBER_OF_FAILURE
        defaultProgramUserCohortShouldNotBeFound("numberOfFailure.greaterOrEqualThan=" + UPDATED_NUMBER_OF_FAILURE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByNumberOfFailureIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where numberOfFailure less than or equals to DEFAULT_NUMBER_OF_FAILURE
        defaultProgramUserCohortShouldNotBeFound("numberOfFailure.lessThan=" + DEFAULT_NUMBER_OF_FAILURE);

        // Get all the programUserCohortList where numberOfFailure less than or equals to UPDATED_NUMBER_OF_FAILURE
        defaultProgramUserCohortShouldBeFound("numberOfFailure.lessThan=" + UPDATED_NUMBER_OF_FAILURE);
    }


    @Test
    @Transactional
    public void getAllProgramUserCohortsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where status equals to DEFAULT_STATUS
        defaultProgramUserCohortShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the programUserCohortList where status equals to UPDATED_STATUS
        defaultProgramUserCohortShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultProgramUserCohortShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the programUserCohortList where status equals to UPDATED_STATUS
        defaultProgramUserCohortShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where status is not null
        defaultProgramUserCohortShouldBeFound("status.specified=true");

        // Get all the programUserCohortList where status is null
        defaultProgramUserCohortShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramUserCohortShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programUserCohortList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramUserCohortShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramUserCohortShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programUserCohortList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramUserCohortShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where createdDate is not null
        defaultProgramUserCohortShouldBeFound("createdDate.specified=true");

        // Get all the programUserCohortList where createdDate is null
        defaultProgramUserCohortShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramUserCohortShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programUserCohortList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramUserCohortShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramUserCohortShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programUserCohortList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramUserCohortShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        // Get all the programUserCohortList where modifiedDate is not null
        defaultProgramUserCohortShouldBeFound("modifiedDate.specified=true");

        // Get all the programUserCohortList where modifiedDate is null
        defaultProgramUserCohortShouldNotBeFound("modifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCohortsByProgramUserIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramUser programUser = ProgramUserResourceIT.createEntity(em);
        em.persist(programUser);
        em.flush();
        programUserCohort.setProgramUser(programUser);
        programUserCohortRepository.saveAndFlush(programUserCohort);
        Long programUserId = programUser.getId();

        // Get all the programUserCohortList where programUser equals to programUserId
        defaultProgramUserCohortShouldBeFound("programUserId.equals=" + programUserId);

        // Get all the programUserCohortList where programUser equals to programUserId + 1
        defaultProgramUserCohortShouldNotBeFound("programUserId.equals=" + (programUserId + 1));
    }


    @Test
    @Transactional
    public void getAllProgramUserCohortsByProgramCohortIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCohort programCohort = ProgramCohortResourceIT.createEntity(em);
        em.persist(programCohort);
        em.flush();
        programUserCohort.setProgramCohort(programCohort);
        programUserCohortRepository.saveAndFlush(programUserCohort);
        Long programCohortId = programCohort.getId();

        // Get all the programUserCohortList where programCohort equals to programCohortId
        defaultProgramUserCohortShouldBeFound("programCohortId.equals=" + programCohortId);

        // Get all the programUserCohortList where programCohort equals to programCohortId + 1
        defaultProgramUserCohortShouldNotBeFound("programCohortId.equals=" + (programCohortId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramUserCohortShouldBeFound(String filter) throws Exception {
        restProgramUserCohortMockMvc.perform(get("/api/program-user-cohorts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUserCohort.getId().intValue())))
            .andExpect(jsonPath("$.[*].progress").value(hasItem(DEFAULT_PROGRESS.intValue())))
            .andExpect(jsonPath("$.[*].numberOfPass").value(hasItem(DEFAULT_NUMBER_OF_PASS)))
            .andExpect(jsonPath("$.[*].numberOfFailure").value(hasItem(DEFAULT_NUMBER_OF_FAILURE)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramUserCohortMockMvc.perform(get("/api/program-user-cohorts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramUserCohortShouldNotBeFound(String filter) throws Exception {
        restProgramUserCohortMockMvc.perform(get("/api/program-user-cohorts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramUserCohortMockMvc.perform(get("/api/program-user-cohorts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramUserCohort() throws Exception {
        // Get the programUserCohort
        restProgramUserCohortMockMvc.perform(get("/api/program-user-cohorts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramUserCohort() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        int databaseSizeBeforeUpdate = programUserCohortRepository.findAll().size();

        // Update the programUserCohort
        ProgramUserCohort updatedProgramUserCohort = programUserCohortRepository.findById(programUserCohort.getId()).get();
        // Disconnect from session so that the updates on updatedProgramUserCohort are not directly saved in db
        em.detach(updatedProgramUserCohort);
        updatedProgramUserCohort
            .progress(UPDATED_PROGRESS)
            .numberOfPass(UPDATED_NUMBER_OF_PASS)
            .numberOfFailure(UPDATED_NUMBER_OF_FAILURE)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        ProgramUserCohortDTO programUserCohortDTO = programUserCohortMapper.toDto(updatedProgramUserCohort);

        restProgramUserCohortMockMvc.perform(put("/api/program-user-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCohortDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramUserCohort in the database
        List<ProgramUserCohort> programUserCohortList = programUserCohortRepository.findAll();
        assertThat(programUserCohortList).hasSize(databaseSizeBeforeUpdate);
        ProgramUserCohort testProgramUserCohort = programUserCohortList.get(programUserCohortList.size() - 1);
        assertThat(testProgramUserCohort.getProgress()).isEqualTo(UPDATED_PROGRESS);
        assertThat(testProgramUserCohort.getNumberOfPass()).isEqualTo(UPDATED_NUMBER_OF_PASS);
        assertThat(testProgramUserCohort.getNumberOfFailure()).isEqualTo(UPDATED_NUMBER_OF_FAILURE);
        assertThat(testProgramUserCohort.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testProgramUserCohort.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramUserCohort.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramUserCohort() throws Exception {
        int databaseSizeBeforeUpdate = programUserCohortRepository.findAll().size();

        // Create the ProgramUserCohort
        ProgramUserCohortDTO programUserCohortDTO = programUserCohortMapper.toDto(programUserCohort);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramUserCohortMockMvc.perform(put("/api/program-user-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCohortDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUserCohort in the database
        List<ProgramUserCohort> programUserCohortList = programUserCohortRepository.findAll();
        assertThat(programUserCohortList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramUserCohort() throws Exception {
        // Initialize the database
        programUserCohortRepository.saveAndFlush(programUserCohort);

        int databaseSizeBeforeDelete = programUserCohortRepository.findAll().size();

        // Delete the programUserCohort
        restProgramUserCohortMockMvc.perform(delete("/api/program-user-cohorts/{id}", programUserCohort.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramUserCohort> programUserCohortList = programUserCohortRepository.findAll();
        assertThat(programUserCohortList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserCohort.class);
        ProgramUserCohort programUserCohort1 = new ProgramUserCohort();
        programUserCohort1.setId(1L);
        ProgramUserCohort programUserCohort2 = new ProgramUserCohort();
        programUserCohort2.setId(programUserCohort1.getId());
        assertThat(programUserCohort1).isEqualTo(programUserCohort2);
        programUserCohort2.setId(2L);
        assertThat(programUserCohort1).isNotEqualTo(programUserCohort2);
        programUserCohort1.setId(null);
        assertThat(programUserCohort1).isNotEqualTo(programUserCohort2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserCohortDTO.class);
        ProgramUserCohortDTO programUserCohortDTO1 = new ProgramUserCohortDTO();
        programUserCohortDTO1.setId(1L);
        ProgramUserCohortDTO programUserCohortDTO2 = new ProgramUserCohortDTO();
        assertThat(programUserCohortDTO1).isNotEqualTo(programUserCohortDTO2);
        programUserCohortDTO2.setId(programUserCohortDTO1.getId());
        assertThat(programUserCohortDTO1).isEqualTo(programUserCohortDTO2);
        programUserCohortDTO2.setId(2L);
        assertThat(programUserCohortDTO1).isNotEqualTo(programUserCohortDTO2);
        programUserCohortDTO1.setId(null);
        assertThat(programUserCohortDTO1).isNotEqualTo(programUserCohortDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programUserCohortMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programUserCohortMapper.fromId(null)).isNull();
    }
}
