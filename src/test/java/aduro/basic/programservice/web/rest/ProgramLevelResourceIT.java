package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramRequirementItems;
import aduro.basic.programservice.repository.ProgramLevelRepository;
import aduro.basic.programservice.service.ProgramLevelService;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.ProgramLevelDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramLevelCriteria;
import aduro.basic.programservice.service.ProgramLevelQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramLevelResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramLevelResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_START_POINT = 1;
    private static final Integer UPDATED_START_POINT = 2;

    private static final Integer DEFAULT_END_POINT = 1;
    private static final Integer UPDATED_END_POINT = 2;

    private static final Integer DEFAULT_LEVEL_ORDER = 1;
    private static final Integer UPDATED_LEVEL_ORDER = 2;

    private static final String DEFAULT_ICON_PATH = "AAAAAAAAAA";
    private static final String UPDATED_ICON_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private ProgramLevelRepository programLevelRepository;

    @Autowired
    private ProgramLevelMapper programLevelMapper;

    @Autowired
    private ProgramLevelService programLevelService;

    @Autowired
    private ProgramLevelQueryService programLevelQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramLevelMockMvc;

    private ProgramLevel programLevel;

    @Autowired
    private ProgramService programService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramLevelResource programLevelResource = new ProgramLevelResource(programService, programLevelService, programLevelQueryService );
        this.restProgramLevelMockMvc = MockMvcBuilders.standaloneSetup(programLevelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevel createEntity(EntityManager em) {
        ProgramLevel programLevel = new ProgramLevel()
            .description(DEFAULT_DESCRIPTION)
            .startPoint(DEFAULT_START_POINT)
            .endPoint(DEFAULT_END_POINT)
            .levelOrder(DEFAULT_LEVEL_ORDER)
            .iconPath(DEFAULT_ICON_PATH)
            .name(DEFAULT_NAME)
            .endDate(DEFAULT_END_DATE);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        programLevel.setProgram(program);
        return programLevel;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevel createUpdatedEntity(EntityManager em) {
        ProgramLevel programLevel = new ProgramLevel()
            .description(UPDATED_DESCRIPTION)
            .startPoint(UPDATED_START_POINT)
            .endPoint(UPDATED_END_POINT)
            .levelOrder(UPDATED_LEVEL_ORDER)
            .iconPath(UPDATED_ICON_PATH)
            .name(UPDATED_NAME)
            .endDate(UPDATED_END_DATE);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createUpdatedEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        programLevel.setProgram(program);
        return programLevel;
    }

    @BeforeEach
    public void initTest() {
        programLevel = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramLevel() throws Exception {
        int databaseSizeBeforeCreate = programLevelRepository.findAll().size();

        // Create the ProgramLevel
        ProgramLevelDTO programLevelDTO = programLevelMapper.toDto(programLevel);
        restProgramLevelMockMvc.perform(post("/api/program-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramLevel in the database
        List<ProgramLevel> programLevelList = programLevelRepository.findAll();
        assertThat(programLevelList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramLevel testProgramLevel = programLevelList.get(programLevelList.size() - 1);
        assertThat(testProgramLevel.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProgramLevel.getStartPoint()).isEqualTo(DEFAULT_START_POINT);
        assertThat(testProgramLevel.getEndPoint()).isEqualTo(DEFAULT_END_POINT);
        assertThat(testProgramLevel.getLevelOrder()).isEqualTo(DEFAULT_LEVEL_ORDER);
        assertThat(testProgramLevel.getIconPath()).isEqualTo(DEFAULT_ICON_PATH);
        assertThat(testProgramLevel.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProgramLevel.getEndDate()).isEqualTo(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    public void createProgramLevelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programLevelRepository.findAll().size();

        // Create the ProgramLevel with an existing ID
        programLevel.setId(1L);
        ProgramLevelDTO programLevelDTO = programLevelMapper.toDto(programLevel);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramLevelMockMvc.perform(post("/api/program-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevel in the database
        List<ProgramLevel> programLevelList = programLevelRepository.findAll();
        assertThat(programLevelList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProgramLevels() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList
        restProgramLevelMockMvc.perform(get("/api/program-levels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevel.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].startPoint").value(hasItem(DEFAULT_START_POINT)))
            .andExpect(jsonPath("$.[*].endPoint").value(hasItem(DEFAULT_END_POINT)))
            .andExpect(jsonPath("$.[*].levelOrder").value(hasItem(DEFAULT_LEVEL_ORDER)))
            .andExpect(jsonPath("$.[*].iconPath").value(hasItem(DEFAULT_ICON_PATH.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));
    }

    @Test
    @Transactional
    public void getProgramLevel() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get the programLevel
        restProgramLevelMockMvc.perform(get("/api/program-levels/{id}", programLevel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programLevel.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.startPoint").value(DEFAULT_START_POINT))
            .andExpect(jsonPath("$.endPoint").value(DEFAULT_END_POINT))
            .andExpect(jsonPath("$.levelOrder").value(DEFAULT_LEVEL_ORDER))
            .andExpect(jsonPath("$.iconPath").value(DEFAULT_ICON_PATH.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where description equals to DEFAULT_DESCRIPTION
        defaultProgramLevelShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the programLevelList where description equals to UPDATED_DESCRIPTION
        defaultProgramLevelShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultProgramLevelShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the programLevelList where description equals to UPDATED_DESCRIPTION
        defaultProgramLevelShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where description is not null
        defaultProgramLevelShouldBeFound("description.specified=true");

        // Get all the programLevelList where description is null
        defaultProgramLevelShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByStartPointIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where startPoint equals to DEFAULT_START_POINT
        defaultProgramLevelShouldBeFound("startPoint.equals=" + DEFAULT_START_POINT);

        // Get all the programLevelList where startPoint equals to UPDATED_START_POINT
        defaultProgramLevelShouldNotBeFound("startPoint.equals=" + UPDATED_START_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByStartPointIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where startPoint in DEFAULT_START_POINT or UPDATED_START_POINT
        defaultProgramLevelShouldBeFound("startPoint.in=" + DEFAULT_START_POINT + "," + UPDATED_START_POINT);

        // Get all the programLevelList where startPoint equals to UPDATED_START_POINT
        defaultProgramLevelShouldNotBeFound("startPoint.in=" + UPDATED_START_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByStartPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where startPoint is not null
        defaultProgramLevelShouldBeFound("startPoint.specified=true");

        // Get all the programLevelList where startPoint is null
        defaultProgramLevelShouldNotBeFound("startPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByStartPointIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where startPoint greater than or equals to DEFAULT_START_POINT
        defaultProgramLevelShouldBeFound("startPoint.greaterOrEqualThan=" + DEFAULT_START_POINT);

        // Get all the programLevelList where startPoint greater than or equals to UPDATED_START_POINT
        defaultProgramLevelShouldNotBeFound("startPoint.greaterOrEqualThan=" + UPDATED_START_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByStartPointIsLessThanSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where startPoint less than or equals to DEFAULT_START_POINT
        defaultProgramLevelShouldNotBeFound("startPoint.lessThan=" + DEFAULT_START_POINT);

        // Get all the programLevelList where startPoint less than or equals to UPDATED_START_POINT
        defaultProgramLevelShouldBeFound("startPoint.lessThan=" + UPDATED_START_POINT);
    }


    @Test
    @Transactional
    public void getAllProgramLevelsByEndPointIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endPoint equals to DEFAULT_END_POINT
        defaultProgramLevelShouldBeFound("endPoint.equals=" + DEFAULT_END_POINT);

        // Get all the programLevelList where endPoint equals to UPDATED_END_POINT
        defaultProgramLevelShouldNotBeFound("endPoint.equals=" + UPDATED_END_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndPointIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endPoint in DEFAULT_END_POINT or UPDATED_END_POINT
        defaultProgramLevelShouldBeFound("endPoint.in=" + DEFAULT_END_POINT + "," + UPDATED_END_POINT);

        // Get all the programLevelList where endPoint equals to UPDATED_END_POINT
        defaultProgramLevelShouldNotBeFound("endPoint.in=" + UPDATED_END_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endPoint is not null
        defaultProgramLevelShouldBeFound("endPoint.specified=true");

        // Get all the programLevelList where endPoint is null
        defaultProgramLevelShouldNotBeFound("endPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndPointIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endPoint greater than or equals to DEFAULT_END_POINT
        defaultProgramLevelShouldBeFound("endPoint.greaterOrEqualThan=" + DEFAULT_END_POINT);

        // Get all the programLevelList where endPoint greater than or equals to UPDATED_END_POINT
        defaultProgramLevelShouldNotBeFound("endPoint.greaterOrEqualThan=" + UPDATED_END_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndPointIsLessThanSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endPoint less than or equals to DEFAULT_END_POINT
        defaultProgramLevelShouldNotBeFound("endPoint.lessThan=" + DEFAULT_END_POINT);

        // Get all the programLevelList where endPoint less than or equals to UPDATED_END_POINT
        defaultProgramLevelShouldBeFound("endPoint.lessThan=" + UPDATED_END_POINT);
    }


    @Test
    @Transactional
    public void getAllProgramLevelsByLevelOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where levelOrder equals to DEFAULT_LEVEL_ORDER
        defaultProgramLevelShouldBeFound("levelOrder.equals=" + DEFAULT_LEVEL_ORDER);

        // Get all the programLevelList where levelOrder equals to UPDATED_LEVEL_ORDER
        defaultProgramLevelShouldNotBeFound("levelOrder.equals=" + UPDATED_LEVEL_ORDER);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByLevelOrderIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where levelOrder in DEFAULT_LEVEL_ORDER or UPDATED_LEVEL_ORDER
        defaultProgramLevelShouldBeFound("levelOrder.in=" + DEFAULT_LEVEL_ORDER + "," + UPDATED_LEVEL_ORDER);

        // Get all the programLevelList where levelOrder equals to UPDATED_LEVEL_ORDER
        defaultProgramLevelShouldNotBeFound("levelOrder.in=" + UPDATED_LEVEL_ORDER);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByLevelOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where levelOrder is not null
        defaultProgramLevelShouldBeFound("levelOrder.specified=true");

        // Get all the programLevelList where levelOrder is null
        defaultProgramLevelShouldNotBeFound("levelOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByLevelOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where levelOrder greater than or equals to DEFAULT_LEVEL_ORDER
        defaultProgramLevelShouldBeFound("levelOrder.greaterOrEqualThan=" + DEFAULT_LEVEL_ORDER);

        // Get all the programLevelList where levelOrder greater than or equals to UPDATED_LEVEL_ORDER
        defaultProgramLevelShouldNotBeFound("levelOrder.greaterOrEqualThan=" + UPDATED_LEVEL_ORDER);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByLevelOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where levelOrder less than or equals to DEFAULT_LEVEL_ORDER
        defaultProgramLevelShouldNotBeFound("levelOrder.lessThan=" + DEFAULT_LEVEL_ORDER);

        // Get all the programLevelList where levelOrder less than or equals to UPDATED_LEVEL_ORDER
        defaultProgramLevelShouldBeFound("levelOrder.lessThan=" + UPDATED_LEVEL_ORDER);
    }


    @Test
    @Transactional
    public void getAllProgramLevelsByIconPathIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where iconPath equals to DEFAULT_ICON_PATH
        defaultProgramLevelShouldBeFound("iconPath.equals=" + DEFAULT_ICON_PATH);

        // Get all the programLevelList where iconPath equals to UPDATED_ICON_PATH
        defaultProgramLevelShouldNotBeFound("iconPath.equals=" + UPDATED_ICON_PATH);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByIconPathIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where iconPath in DEFAULT_ICON_PATH or UPDATED_ICON_PATH
        defaultProgramLevelShouldBeFound("iconPath.in=" + DEFAULT_ICON_PATH + "," + UPDATED_ICON_PATH);

        // Get all the programLevelList where iconPath equals to UPDATED_ICON_PATH
        defaultProgramLevelShouldNotBeFound("iconPath.in=" + UPDATED_ICON_PATH);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByIconPathIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where iconPath is not null
        defaultProgramLevelShouldBeFound("iconPath.specified=true");

        // Get all the programLevelList where iconPath is null
        defaultProgramLevelShouldNotBeFound("iconPath.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where name equals to DEFAULT_NAME
        defaultProgramLevelShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the programLevelList where name equals to UPDATED_NAME
        defaultProgramLevelShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where name in DEFAULT_NAME or UPDATED_NAME
        defaultProgramLevelShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the programLevelList where name equals to UPDATED_NAME
        defaultProgramLevelShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where name is not null
        defaultProgramLevelShouldBeFound("name.specified=true");

        // Get all the programLevelList where name is null
        defaultProgramLevelShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endDate equals to DEFAULT_END_DATE
        defaultProgramLevelShouldBeFound("endDate.equals=" + DEFAULT_END_DATE);

        // Get all the programLevelList where endDate equals to UPDATED_END_DATE
        defaultProgramLevelShouldNotBeFound("endDate.equals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endDate in DEFAULT_END_DATE or UPDATED_END_DATE
        defaultProgramLevelShouldBeFound("endDate.in=" + DEFAULT_END_DATE + "," + UPDATED_END_DATE);

        // Get all the programLevelList where endDate equals to UPDATED_END_DATE
        defaultProgramLevelShouldNotBeFound("endDate.in=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endDate is not null
        defaultProgramLevelShouldBeFound("endDate.specified=true");

        // Get all the programLevelList where endDate is null
        defaultProgramLevelShouldNotBeFound("endDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endDate greater than or equals to DEFAULT_END_DATE
        defaultProgramLevelShouldBeFound("endDate.greaterOrEqualThan=" + DEFAULT_END_DATE);

        // Get all the programLevelList where endDate greater than or equals to UPDATED_END_DATE
        defaultProgramLevelShouldNotBeFound("endDate.greaterOrEqualThan=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelsByEndDateIsLessThanSomething() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        // Get all the programLevelList where endDate less than or equals to DEFAULT_END_DATE
        defaultProgramLevelShouldNotBeFound("endDate.lessThan=" + DEFAULT_END_DATE);

        // Get all the programLevelList where endDate less than or equals to UPDATED_END_DATE
        defaultProgramLevelShouldBeFound("endDate.lessThan=" + UPDATED_END_DATE);
    }


    @Test
    @Transactional
    public void getAllProgramLevelsByProgramIsEqualToSomething() throws Exception {
        // Get already existing entity
        Program program = programLevel.getProgram();
        programLevelRepository.saveAndFlush(programLevel);
        Long programId = program.getId();

        // Get all the programLevelList where program equals to programId
        defaultProgramLevelShouldBeFound("programId.equals=" + programId);

        // Get all the programLevelList where program equals to programId + 1
        defaultProgramLevelShouldNotBeFound("programId.equals=" + (programId + 1));
    }


    @Test
    @Transactional
    public void getAllProgramLevelsByProgramRequirementItemsIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramRequirementItems programRequirementItems = ProgramRequirementItemsResourceIT.createEntity(em);
        em.persist(programRequirementItems);
        em.flush();
        programLevel.addProgramRequirementItems(programRequirementItems);
        programLevelRepository.saveAndFlush(programLevel);
        Long programRequirementItemsId = programRequirementItems.getId();

        // Get all the programLevelList where programRequirementItems equals to programRequirementItemsId
        defaultProgramLevelShouldBeFound("programRequirementItemsId.equals=" + programRequirementItemsId);

        // Get all the programLevelList where programRequirementItems equals to programRequirementItemsId + 1
        defaultProgramLevelShouldNotBeFound("programRequirementItemsId.equals=" + (programRequirementItemsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramLevelShouldBeFound(String filter) throws Exception {
        restProgramLevelMockMvc.perform(get("/api/program-levels?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevel.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].startPoint").value(hasItem(DEFAULT_START_POINT)))
            .andExpect(jsonPath("$.[*].endPoint").value(hasItem(DEFAULT_END_POINT)))
            .andExpect(jsonPath("$.[*].levelOrder").value(hasItem(DEFAULT_LEVEL_ORDER)))
            .andExpect(jsonPath("$.[*].iconPath").value(hasItem(DEFAULT_ICON_PATH)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramLevelMockMvc.perform(get("/api/program-levels/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramLevelShouldNotBeFound(String filter) throws Exception {
        restProgramLevelMockMvc.perform(get("/api/program-levels?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramLevelMockMvc.perform(get("/api/program-levels/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramLevel() throws Exception {
        // Get the programLevel
        restProgramLevelMockMvc.perform(get("/api/program-levels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramLevel() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        int databaseSizeBeforeUpdate = programLevelRepository.findAll().size();

        // Update the programLevel
        ProgramLevel updatedProgramLevel = programLevelRepository.findById(programLevel.getId()).get();
        // Disconnect from session so that the updates on updatedProgramLevel are not directly saved in db
        em.detach(updatedProgramLevel);
        updatedProgramLevel
            .description(UPDATED_DESCRIPTION)
            .startPoint(UPDATED_START_POINT)
            .endPoint(UPDATED_END_POINT)
            .levelOrder(UPDATED_LEVEL_ORDER)
            .iconPath(UPDATED_ICON_PATH)
            .name(UPDATED_NAME)
            .endDate(UPDATED_END_DATE);
        ProgramLevelDTO programLevelDTO = programLevelMapper.toDto(updatedProgramLevel);

        restProgramLevelMockMvc.perform(put("/api/program-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramLevel in the database
        List<ProgramLevel> programLevelList = programLevelRepository.findAll();
        assertThat(programLevelList).hasSize(databaseSizeBeforeUpdate);
        ProgramLevel testProgramLevel = programLevelList.get(programLevelList.size() - 1);
        assertThat(testProgramLevel.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProgramLevel.getStartPoint()).isEqualTo(UPDATED_START_POINT);
        assertThat(testProgramLevel.getEndPoint()).isEqualTo(UPDATED_END_POINT);
        assertThat(testProgramLevel.getLevelOrder()).isEqualTo(UPDATED_LEVEL_ORDER);
        assertThat(testProgramLevel.getIconPath()).isEqualTo(UPDATED_ICON_PATH);
        assertThat(testProgramLevel.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProgramLevel.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramLevel() throws Exception {
        int databaseSizeBeforeUpdate = programLevelRepository.findAll().size();

        // Create the ProgramLevel
        ProgramLevelDTO programLevelDTO = programLevelMapper.toDto(programLevel);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramLevelMockMvc.perform(put("/api/program-levels")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevel in the database
        List<ProgramLevel> programLevelList = programLevelRepository.findAll();
        assertThat(programLevelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramLevel() throws Exception {
        // Initialize the database
        programLevelRepository.saveAndFlush(programLevel);

        int databaseSizeBeforeDelete = programLevelRepository.findAll().size();

        // Delete the programLevel
        restProgramLevelMockMvc.perform(delete("/api/program-levels/{id}", programLevel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramLevel> programLevelList = programLevelRepository.findAll();
        assertThat(programLevelList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevel.class);
        ProgramLevel programLevel1 = new ProgramLevel();
        programLevel1.setId(1L);
        ProgramLevel programLevel2 = new ProgramLevel();
        programLevel2.setId(programLevel1.getId());
        assertThat(programLevel1).isEqualTo(programLevel2);
        programLevel2.setId(2L);
        assertThat(programLevel1).isNotEqualTo(programLevel2);
        programLevel1.setId(null);
        assertThat(programLevel1).isNotEqualTo(programLevel2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelDTO.class);
        ProgramLevelDTO programLevelDTO1 = new ProgramLevelDTO();
        programLevelDTO1.setId(1L);
        ProgramLevelDTO programLevelDTO2 = new ProgramLevelDTO();
        assertThat(programLevelDTO1).isNotEqualTo(programLevelDTO2);
        programLevelDTO2.setId(programLevelDTO1.getId());
        assertThat(programLevelDTO1).isEqualTo(programLevelDTO2);
        programLevelDTO2.setId(2L);
        assertThat(programLevelDTO1).isNotEqualTo(programLevelDTO2);
        programLevelDTO1.setId(null);
        assertThat(programLevelDTO1).isNotEqualTo(programLevelDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programLevelMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programLevelMapper.fromId(null)).isNull();
    }
}
