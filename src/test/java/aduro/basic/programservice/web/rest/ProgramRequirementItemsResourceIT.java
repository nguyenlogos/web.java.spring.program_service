package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramRequirementItems;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.repository.ProgramRequirementItemsRepository;
import aduro.basic.programservice.service.ProgramRequirementItemsService;
import aduro.basic.programservice.service.dto.ProgramRequirementItemsDTO;
import aduro.basic.programservice.service.mapper.ProgramRequirementItemsMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramRequirementItemsCriteria;
import aduro.basic.programservice.service.ProgramRequirementItemsQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramRequirementItemsResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramRequirementItemsResourceIT {

    private static final String DEFAULT_ITEM_ID = "AAAAAAAAAA";
    private static final String UPDATED_ITEM_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME_OF_ITEM = "AAAAAAAAAA";
    private static final String UPDATED_NAME_OF_ITEM = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_OF_ITEM = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_OF_ITEM = "BBBBBBBBBB";

    private static final String DEFAULT_ITEM_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ITEM_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramRequirementItemsRepository programRequirementItemsRepository;

    @Autowired
    private ProgramRequirementItemsMapper programRequirementItemsMapper;

    @Autowired
    private ProgramRequirementItemsService programRequirementItemsService;

    @Autowired
    private ProgramRequirementItemsQueryService programRequirementItemsQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramRequirementItemsMockMvc;

    private ProgramRequirementItems programRequirementItems;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramRequirementItemsResource programRequirementItemsResource = new ProgramRequirementItemsResource(programRequirementItemsService, programRequirementItemsQueryService);
        this.restProgramRequirementItemsMockMvc = MockMvcBuilders.standaloneSetup(programRequirementItemsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramRequirementItems createEntity(EntityManager em) {
        ProgramRequirementItems programRequirementItems = new ProgramRequirementItems()
            .itemId(DEFAULT_ITEM_ID)
            .nameOfItem(DEFAULT_NAME_OF_ITEM)
            .typeOfItem(DEFAULT_TYPE_OF_ITEM)
            .itemCode(DEFAULT_ITEM_CODE)
            .subgroupId(DEFAULT_SUBGROUP_ID)
            .subgroupName(DEFAULT_SUBGROUP_NAME)
            .createdAt(DEFAULT_CREATED_AT);
        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }
        programRequirementItems.setProgramLevel(programLevel);
        return programRequirementItems;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramRequirementItems createUpdatedEntity(EntityManager em) {
        ProgramRequirementItems programRequirementItems = new ProgramRequirementItems()
            .itemId(UPDATED_ITEM_ID)
            .nameOfItem(UPDATED_NAME_OF_ITEM)
            .typeOfItem(UPDATED_TYPE_OF_ITEM)
            .itemCode(UPDATED_ITEM_CODE)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME)
            .createdAt(UPDATED_CREATED_AT);
        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createUpdatedEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }
        programRequirementItems.setProgramLevel(programLevel);
        return programRequirementItems;
    }

    @BeforeEach
    public void initTest() {
        programRequirementItems = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramRequirementItems() throws Exception {
        int databaseSizeBeforeCreate = programRequirementItemsRepository.findAll().size();

        // Create the ProgramRequirementItems
        ProgramRequirementItemsDTO programRequirementItemsDTO = programRequirementItemsMapper.toDto(programRequirementItems);
        restProgramRequirementItemsMockMvc.perform(post("/api/program-requirement-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programRequirementItemsDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramRequirementItems in the database
        List<ProgramRequirementItems> programRequirementItemsList = programRequirementItemsRepository.findAll();
        assertThat(programRequirementItemsList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramRequirementItems testProgramRequirementItems = programRequirementItemsList.get(programRequirementItemsList.size() - 1);
        assertThat(testProgramRequirementItems.getItemId()).isEqualTo(DEFAULT_ITEM_ID);
        assertThat(testProgramRequirementItems.getNameOfItem()).isEqualTo(DEFAULT_NAME_OF_ITEM);
        assertThat(testProgramRequirementItems.getTypeOfItem()).isEqualTo(DEFAULT_TYPE_OF_ITEM);
        assertThat(testProgramRequirementItems.getItemCode()).isEqualTo(DEFAULT_ITEM_CODE);
        assertThat(testProgramRequirementItems.getSubgroupId()).isEqualTo(DEFAULT_SUBGROUP_ID);
        assertThat(testProgramRequirementItems.getSubgroupName()).isEqualTo(DEFAULT_SUBGROUP_NAME);
        assertThat(testProgramRequirementItems.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
    }

    @Test
    @Transactional
    public void createProgramRequirementItemsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programRequirementItemsRepository.findAll().size();

        // Create the ProgramRequirementItems with an existing ID
        programRequirementItems.setId(1L);
        ProgramRequirementItemsDTO programRequirementItemsDTO = programRequirementItemsMapper.toDto(programRequirementItems);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramRequirementItemsMockMvc.perform(post("/api/program-requirement-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programRequirementItemsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramRequirementItems in the database
        List<ProgramRequirementItems> programRequirementItemsList = programRequirementItemsRepository.findAll();
        assertThat(programRequirementItemsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkItemIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programRequirementItemsRepository.findAll().size();
        // set the field null
        programRequirementItems.setItemId(null);

        // Create the ProgramRequirementItems, which fails.
        ProgramRequirementItemsDTO programRequirementItemsDTO = programRequirementItemsMapper.toDto(programRequirementItems);

        restProgramRequirementItemsMockMvc.perform(post("/api/program-requirement-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programRequirementItemsDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramRequirementItems> programRequirementItemsList = programRequirementItemsRepository.findAll();
        assertThat(programRequirementItemsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeOfItemIsRequired() throws Exception {
        int databaseSizeBeforeTest = programRequirementItemsRepository.findAll().size();
        // set the field null
        programRequirementItems.setTypeOfItem(null);

        // Create the ProgramRequirementItems, which fails.
        ProgramRequirementItemsDTO programRequirementItemsDTO = programRequirementItemsMapper.toDto(programRequirementItems);

        restProgramRequirementItemsMockMvc.perform(post("/api/program-requirement-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programRequirementItemsDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramRequirementItems> programRequirementItemsList = programRequirementItemsRepository.findAll();
        assertThat(programRequirementItemsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkItemCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programRequirementItemsRepository.findAll().size();
        // set the field null
        programRequirementItems.setItemCode(null);

        // Create the ProgramRequirementItems, which fails.
        ProgramRequirementItemsDTO programRequirementItemsDTO = programRequirementItemsMapper.toDto(programRequirementItems);

        restProgramRequirementItemsMockMvc.perform(post("/api/program-requirement-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programRequirementItemsDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramRequirementItems> programRequirementItemsList = programRequirementItemsRepository.findAll();
        assertThat(programRequirementItemsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItems() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList
        restProgramRequirementItemsMockMvc.perform(get("/api/program-requirement-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programRequirementItems.getId().intValue())))
            .andExpect(jsonPath("$.[*].itemId").value(hasItem(DEFAULT_ITEM_ID.toString())))
            .andExpect(jsonPath("$.[*].nameOfItem").value(hasItem(DEFAULT_NAME_OF_ITEM.toString())))
            .andExpect(jsonPath("$.[*].typeOfItem").value(hasItem(DEFAULT_TYPE_OF_ITEM.toString())))
            .andExpect(jsonPath("$.[*].itemCode").value(hasItem(DEFAULT_ITEM_CODE.toString())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID.toString())))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramRequirementItems() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get the programRequirementItems
        restProgramRequirementItemsMockMvc.perform(get("/api/program-requirement-items/{id}", programRequirementItems.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programRequirementItems.getId().intValue()))
            .andExpect(jsonPath("$.itemId").value(DEFAULT_ITEM_ID.toString()))
            .andExpect(jsonPath("$.nameOfItem").value(DEFAULT_NAME_OF_ITEM.toString()))
            .andExpect(jsonPath("$.typeOfItem").value(DEFAULT_TYPE_OF_ITEM.toString()))
            .andExpect(jsonPath("$.itemCode").value(DEFAULT_ITEM_CODE.toString()))
            .andExpect(jsonPath("$.subgroupId").value(DEFAULT_SUBGROUP_ID.toString()))
            .andExpect(jsonPath("$.subgroupName").value(DEFAULT_SUBGROUP_NAME.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByItemIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where itemId equals to DEFAULT_ITEM_ID
        defaultProgramRequirementItemsShouldBeFound("itemId.equals=" + DEFAULT_ITEM_ID);

        // Get all the programRequirementItemsList where itemId equals to UPDATED_ITEM_ID
        defaultProgramRequirementItemsShouldNotBeFound("itemId.equals=" + UPDATED_ITEM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByItemIdIsInShouldWork() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where itemId in DEFAULT_ITEM_ID or UPDATED_ITEM_ID
        defaultProgramRequirementItemsShouldBeFound("itemId.in=" + DEFAULT_ITEM_ID + "," + UPDATED_ITEM_ID);

        // Get all the programRequirementItemsList where itemId equals to UPDATED_ITEM_ID
        defaultProgramRequirementItemsShouldNotBeFound("itemId.in=" + UPDATED_ITEM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByItemIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where itemId is not null
        defaultProgramRequirementItemsShouldBeFound("itemId.specified=true");

        // Get all the programRequirementItemsList where itemId is null
        defaultProgramRequirementItemsShouldNotBeFound("itemId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByNameOfItemIsEqualToSomething() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where nameOfItem equals to DEFAULT_NAME_OF_ITEM
        defaultProgramRequirementItemsShouldBeFound("nameOfItem.equals=" + DEFAULT_NAME_OF_ITEM);

        // Get all the programRequirementItemsList where nameOfItem equals to UPDATED_NAME_OF_ITEM
        defaultProgramRequirementItemsShouldNotBeFound("nameOfItem.equals=" + UPDATED_NAME_OF_ITEM);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByNameOfItemIsInShouldWork() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where nameOfItem in DEFAULT_NAME_OF_ITEM or UPDATED_NAME_OF_ITEM
        defaultProgramRequirementItemsShouldBeFound("nameOfItem.in=" + DEFAULT_NAME_OF_ITEM + "," + UPDATED_NAME_OF_ITEM);

        // Get all the programRequirementItemsList where nameOfItem equals to UPDATED_NAME_OF_ITEM
        defaultProgramRequirementItemsShouldNotBeFound("nameOfItem.in=" + UPDATED_NAME_OF_ITEM);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByNameOfItemIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where nameOfItem is not null
        defaultProgramRequirementItemsShouldBeFound("nameOfItem.specified=true");

        // Get all the programRequirementItemsList where nameOfItem is null
        defaultProgramRequirementItemsShouldNotBeFound("nameOfItem.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByTypeOfItemIsEqualToSomething() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where typeOfItem equals to DEFAULT_TYPE_OF_ITEM
        defaultProgramRequirementItemsShouldBeFound("typeOfItem.equals=" + DEFAULT_TYPE_OF_ITEM);

        // Get all the programRequirementItemsList where typeOfItem equals to UPDATED_TYPE_OF_ITEM
        defaultProgramRequirementItemsShouldNotBeFound("typeOfItem.equals=" + UPDATED_TYPE_OF_ITEM);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByTypeOfItemIsInShouldWork() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where typeOfItem in DEFAULT_TYPE_OF_ITEM or UPDATED_TYPE_OF_ITEM
        defaultProgramRequirementItemsShouldBeFound("typeOfItem.in=" + DEFAULT_TYPE_OF_ITEM + "," + UPDATED_TYPE_OF_ITEM);

        // Get all the programRequirementItemsList where typeOfItem equals to UPDATED_TYPE_OF_ITEM
        defaultProgramRequirementItemsShouldNotBeFound("typeOfItem.in=" + UPDATED_TYPE_OF_ITEM);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByTypeOfItemIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where typeOfItem is not null
        defaultProgramRequirementItemsShouldBeFound("typeOfItem.specified=true");

        // Get all the programRequirementItemsList where typeOfItem is null
        defaultProgramRequirementItemsShouldNotBeFound("typeOfItem.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByItemCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where itemCode equals to DEFAULT_ITEM_CODE
        defaultProgramRequirementItemsShouldBeFound("itemCode.equals=" + DEFAULT_ITEM_CODE);

        // Get all the programRequirementItemsList where itemCode equals to UPDATED_ITEM_CODE
        defaultProgramRequirementItemsShouldNotBeFound("itemCode.equals=" + UPDATED_ITEM_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByItemCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where itemCode in DEFAULT_ITEM_CODE or UPDATED_ITEM_CODE
        defaultProgramRequirementItemsShouldBeFound("itemCode.in=" + DEFAULT_ITEM_CODE + "," + UPDATED_ITEM_CODE);

        // Get all the programRequirementItemsList where itemCode equals to UPDATED_ITEM_CODE
        defaultProgramRequirementItemsShouldNotBeFound("itemCode.in=" + UPDATED_ITEM_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByItemCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where itemCode is not null
        defaultProgramRequirementItemsShouldBeFound("itemCode.specified=true");

        // Get all the programRequirementItemsList where itemCode is null
        defaultProgramRequirementItemsShouldNotBeFound("itemCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsBySubgroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where subgroupId equals to DEFAULT_SUBGROUP_ID
        defaultProgramRequirementItemsShouldBeFound("subgroupId.equals=" + DEFAULT_SUBGROUP_ID);

        // Get all the programRequirementItemsList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramRequirementItemsShouldNotBeFound("subgroupId.equals=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsBySubgroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where subgroupId in DEFAULT_SUBGROUP_ID or UPDATED_SUBGROUP_ID
        defaultProgramRequirementItemsShouldBeFound("subgroupId.in=" + DEFAULT_SUBGROUP_ID + "," + UPDATED_SUBGROUP_ID);

        // Get all the programRequirementItemsList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramRequirementItemsShouldNotBeFound("subgroupId.in=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsBySubgroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where subgroupId is not null
        defaultProgramRequirementItemsShouldBeFound("subgroupId.specified=true");

        // Get all the programRequirementItemsList where subgroupId is null
        defaultProgramRequirementItemsShouldNotBeFound("subgroupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsBySubgroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where subgroupName equals to DEFAULT_SUBGROUP_NAME
        defaultProgramRequirementItemsShouldBeFound("subgroupName.equals=" + DEFAULT_SUBGROUP_NAME);

        // Get all the programRequirementItemsList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramRequirementItemsShouldNotBeFound("subgroupName.equals=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsBySubgroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where subgroupName in DEFAULT_SUBGROUP_NAME or UPDATED_SUBGROUP_NAME
        defaultProgramRequirementItemsShouldBeFound("subgroupName.in=" + DEFAULT_SUBGROUP_NAME + "," + UPDATED_SUBGROUP_NAME);

        // Get all the programRequirementItemsList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramRequirementItemsShouldNotBeFound("subgroupName.in=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsBySubgroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where subgroupName is not null
        defaultProgramRequirementItemsShouldBeFound("subgroupName.specified=true");

        // Get all the programRequirementItemsList where subgroupName is null
        defaultProgramRequirementItemsShouldNotBeFound("subgroupName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where createdAt equals to DEFAULT_CREATED_AT
        defaultProgramRequirementItemsShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the programRequirementItemsList where createdAt equals to UPDATED_CREATED_AT
        defaultProgramRequirementItemsShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultProgramRequirementItemsShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the programRequirementItemsList where createdAt equals to UPDATED_CREATED_AT
        defaultProgramRequirementItemsShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        // Get all the programRequirementItemsList where createdAt is not null
        defaultProgramRequirementItemsShouldBeFound("createdAt.specified=true");

        // Get all the programRequirementItemsList where createdAt is null
        defaultProgramRequirementItemsShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramRequirementItemsByProgramLevelIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramLevel programLevel = programRequirementItems.getProgramLevel();
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);
        Long programLevelId = programLevel.getId();

        // Get all the programRequirementItemsList where programLevel equals to programLevelId
        defaultProgramRequirementItemsShouldBeFound("programLevelId.equals=" + programLevelId);

        // Get all the programRequirementItemsList where programLevel equals to programLevelId + 1
        defaultProgramRequirementItemsShouldNotBeFound("programLevelId.equals=" + (programLevelId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramRequirementItemsShouldBeFound(String filter) throws Exception {
        restProgramRequirementItemsMockMvc.perform(get("/api/program-requirement-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programRequirementItems.getId().intValue())))
            .andExpect(jsonPath("$.[*].itemId").value(hasItem(DEFAULT_ITEM_ID)))
            .andExpect(jsonPath("$.[*].nameOfItem").value(hasItem(DEFAULT_NAME_OF_ITEM)))
            .andExpect(jsonPath("$.[*].typeOfItem").value(hasItem(DEFAULT_TYPE_OF_ITEM)))
            .andExpect(jsonPath("$.[*].itemCode").value(hasItem(DEFAULT_ITEM_CODE)))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID)))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())));

        // Check, that the count call also returns 1
        restProgramRequirementItemsMockMvc.perform(get("/api/program-requirement-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramRequirementItemsShouldNotBeFound(String filter) throws Exception {
        restProgramRequirementItemsMockMvc.perform(get("/api/program-requirement-items?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramRequirementItemsMockMvc.perform(get("/api/program-requirement-items/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramRequirementItems() throws Exception {
        // Get the programRequirementItems
        restProgramRequirementItemsMockMvc.perform(get("/api/program-requirement-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramRequirementItems() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        int databaseSizeBeforeUpdate = programRequirementItemsRepository.findAll().size();

        // Update the programRequirementItems
        ProgramRequirementItems updatedProgramRequirementItems = programRequirementItemsRepository.findById(programRequirementItems.getId()).get();
        // Disconnect from session so that the updates on updatedProgramRequirementItems are not directly saved in db
        em.detach(updatedProgramRequirementItems);
        updatedProgramRequirementItems
            .itemId(UPDATED_ITEM_ID)
            .nameOfItem(UPDATED_NAME_OF_ITEM)
            .typeOfItem(UPDATED_TYPE_OF_ITEM)
            .itemCode(UPDATED_ITEM_CODE)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME)
            .createdAt(UPDATED_CREATED_AT);
        ProgramRequirementItemsDTO programRequirementItemsDTO = programRequirementItemsMapper.toDto(updatedProgramRequirementItems);

        restProgramRequirementItemsMockMvc.perform(put("/api/program-requirement-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programRequirementItemsDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramRequirementItems in the database
        List<ProgramRequirementItems> programRequirementItemsList = programRequirementItemsRepository.findAll();
        assertThat(programRequirementItemsList).hasSize(databaseSizeBeforeUpdate);
        ProgramRequirementItems testProgramRequirementItems = programRequirementItemsList.get(programRequirementItemsList.size() - 1);
        assertThat(testProgramRequirementItems.getItemId()).isEqualTo(UPDATED_ITEM_ID);
        assertThat(testProgramRequirementItems.getNameOfItem()).isEqualTo(UPDATED_NAME_OF_ITEM);
        assertThat(testProgramRequirementItems.getTypeOfItem()).isEqualTo(UPDATED_TYPE_OF_ITEM);
        assertThat(testProgramRequirementItems.getItemCode()).isEqualTo(UPDATED_ITEM_CODE);
        assertThat(testProgramRequirementItems.getSubgroupId()).isEqualTo(UPDATED_SUBGROUP_ID);
        assertThat(testProgramRequirementItems.getSubgroupName()).isEqualTo(UPDATED_SUBGROUP_NAME);
        assertThat(testProgramRequirementItems.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramRequirementItems() throws Exception {
        int databaseSizeBeforeUpdate = programRequirementItemsRepository.findAll().size();

        // Create the ProgramRequirementItems
        ProgramRequirementItemsDTO programRequirementItemsDTO = programRequirementItemsMapper.toDto(programRequirementItems);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramRequirementItemsMockMvc.perform(put("/api/program-requirement-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programRequirementItemsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramRequirementItems in the database
        List<ProgramRequirementItems> programRequirementItemsList = programRequirementItemsRepository.findAll();
        assertThat(programRequirementItemsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramRequirementItems() throws Exception {
        // Initialize the database
        programRequirementItemsRepository.saveAndFlush(programRequirementItems);

        int databaseSizeBeforeDelete = programRequirementItemsRepository.findAll().size();

        // Delete the programRequirementItems
        restProgramRequirementItemsMockMvc.perform(delete("/api/program-requirement-items/{id}", programRequirementItems.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramRequirementItems> programRequirementItemsList = programRequirementItemsRepository.findAll();
        assertThat(programRequirementItemsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramRequirementItems.class);
        ProgramRequirementItems programRequirementItems1 = new ProgramRequirementItems();
        programRequirementItems1.setId(1L);
        ProgramRequirementItems programRequirementItems2 = new ProgramRequirementItems();
        programRequirementItems2.setId(programRequirementItems1.getId());
        assertThat(programRequirementItems1).isEqualTo(programRequirementItems2);
        programRequirementItems2.setId(2L);
        assertThat(programRequirementItems1).isNotEqualTo(programRequirementItems2);
        programRequirementItems1.setId(null);
        assertThat(programRequirementItems1).isNotEqualTo(programRequirementItems2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramRequirementItemsDTO.class);
        ProgramRequirementItemsDTO programRequirementItemsDTO1 = new ProgramRequirementItemsDTO();
        programRequirementItemsDTO1.setId(1L);
        ProgramRequirementItemsDTO programRequirementItemsDTO2 = new ProgramRequirementItemsDTO();
        assertThat(programRequirementItemsDTO1).isNotEqualTo(programRequirementItemsDTO2);
        programRequirementItemsDTO2.setId(programRequirementItemsDTO1.getId());
        assertThat(programRequirementItemsDTO1).isEqualTo(programRequirementItemsDTO2);
        programRequirementItemsDTO2.setId(2L);
        assertThat(programRequirementItemsDTO1).isNotEqualTo(programRequirementItemsDTO2);
        programRequirementItemsDTO1.setId(null);
        assertThat(programRequirementItemsDTO1).isNotEqualTo(programRequirementItemsDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programRequirementItemsMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programRequirementItemsMapper.fromId(null)).isNull();
    }
}
