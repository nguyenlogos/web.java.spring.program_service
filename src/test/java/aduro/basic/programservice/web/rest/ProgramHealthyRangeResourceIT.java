package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramHealthyRange;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ProgramHealthyRangeRepository;
import aduro.basic.programservice.service.ProgramHealthyRangeService;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeDTO;
import aduro.basic.programservice.service.mapper.ProgramHealthyRangeMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramHealthyRangeCriteria;
import aduro.basic.programservice.service.ProgramHealthyRangeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramHealthyRangeResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramHealthyRangeResourceIT {

    private static final Float DEFAULT_MALE_MIN = 1F;
    private static final Float UPDATED_MALE_MIN = 2F;

    private static final Float DEFAULT_MALE_MAX = 1F;
    private static final Float UPDATED_MALE_MAX = 2F;

    private static final Float DEFAULT_FEMALE_MIN = 1F;
    private static final Float UPDATED_FEMALE_MIN = 2F;

    private static final Float DEFAULT_FEMALE_MAX = 1F;
    private static final Float UPDATED_FEMALE_MAX = 2F;

    private static final Float DEFAULT_UNIDENTIFIED_MIN = 1F;
    private static final Float UPDATED_UNIDENTIFIED_MIN = 2F;

    private static final Float DEFAULT_UNIDENTIFIED_MAX = 1F;
    private static final Float UPDATED_UNIDENTIFIED_MAX = 2F;

    private static final Boolean DEFAULT_IS_APPLY_POINT = false;
    private static final Boolean UPDATED_IS_APPLY_POINT = true;

    private static final String DEFAULT_BIOMETRIC_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BIOMETRIC_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_CATEGORY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SUB_CATEGORY_CODE = "BBBBBBBBBB";

    private static final Long DEFAULT_SUB_CATEGORY_ID = 1L;
    private static final Long UPDATED_SUB_CATEGORY_ID = 2L;

    @Autowired
    private ProgramHealthyRangeRepository programHealthyRangeRepository;

    @Autowired
    private ProgramHealthyRangeMapper programHealthyRangeMapper;

    @Autowired
    private ProgramHealthyRangeService programHealthyRangeService;

    @Autowired
    private ProgramHealthyRangeQueryService programHealthyRangeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramHealthyRangeMockMvc;

    private ProgramHealthyRange programHealthyRange;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramHealthyRangeResource programHealthyRangeResource = new ProgramHealthyRangeResource(programHealthyRangeService, programHealthyRangeQueryService);
        this.restProgramHealthyRangeMockMvc = MockMvcBuilders.standaloneSetup(programHealthyRangeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramHealthyRange createEntity(EntityManager em) {
        ProgramHealthyRange programHealthyRange = new ProgramHealthyRange()
            .maleMin(DEFAULT_MALE_MIN)
            .maleMax(DEFAULT_MALE_MAX)
            .femaleMin(DEFAULT_FEMALE_MIN)
            .femaleMax(DEFAULT_FEMALE_MAX)
            .unidentifiedMin(DEFAULT_UNIDENTIFIED_MIN)
            .unidentifiedMax(DEFAULT_UNIDENTIFIED_MAX)
            .isApplyPoint(DEFAULT_IS_APPLY_POINT)
            .biometricCode(DEFAULT_BIOMETRIC_CODE)
            .subCategoryCode(DEFAULT_SUB_CATEGORY_CODE)
            .subCategoryId(DEFAULT_SUB_CATEGORY_ID);
        return programHealthyRange;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramHealthyRange createUpdatedEntity(EntityManager em) {
        ProgramHealthyRange programHealthyRange = new ProgramHealthyRange()
            .maleMin(UPDATED_MALE_MIN)
            .maleMax(UPDATED_MALE_MAX)
            .femaleMin(UPDATED_FEMALE_MIN)
            .femaleMax(UPDATED_FEMALE_MAX)
            .unidentifiedMin(UPDATED_UNIDENTIFIED_MIN)
            .unidentifiedMax(UPDATED_UNIDENTIFIED_MAX)
            .isApplyPoint(UPDATED_IS_APPLY_POINT)
            .biometricCode(UPDATED_BIOMETRIC_CODE)
            .subCategoryCode(UPDATED_SUB_CATEGORY_CODE)
            .subCategoryId(UPDATED_SUB_CATEGORY_ID);
        return programHealthyRange;
    }

    @BeforeEach
    public void initTest() {
        programHealthyRange = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramHealthyRange() throws Exception {
        int databaseSizeBeforeCreate = programHealthyRangeRepository.findAll().size();

        // Create the ProgramHealthyRange
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);
        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramHealthyRange in the database
        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramHealthyRange testProgramHealthyRange = programHealthyRangeList.get(programHealthyRangeList.size() - 1);
        assertThat(testProgramHealthyRange.getMaleMin()).isEqualTo(DEFAULT_MALE_MIN);
        assertThat(testProgramHealthyRange.getMaleMax()).isEqualTo(DEFAULT_MALE_MAX);
        assertThat(testProgramHealthyRange.getFemaleMin()).isEqualTo(DEFAULT_FEMALE_MIN);
        assertThat(testProgramHealthyRange.getFemaleMax()).isEqualTo(DEFAULT_FEMALE_MAX);
        assertThat(testProgramHealthyRange.getUnidentifiedMin()).isEqualTo(DEFAULT_UNIDENTIFIED_MIN);
        assertThat(testProgramHealthyRange.getUnidentifiedMax()).isEqualTo(DEFAULT_UNIDENTIFIED_MAX);
        assertThat(testProgramHealthyRange.isIsApplyPoint()).isEqualTo(DEFAULT_IS_APPLY_POINT);
        assertThat(testProgramHealthyRange.getBiometricCode()).isEqualTo(DEFAULT_BIOMETRIC_CODE);
        assertThat(testProgramHealthyRange.getSubCategoryCode()).isEqualTo(DEFAULT_SUB_CATEGORY_CODE);
        assertThat(testProgramHealthyRange.getSubCategoryId()).isEqualTo(DEFAULT_SUB_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void createProgramHealthyRangeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programHealthyRangeRepository.findAll().size();

        // Create the ProgramHealthyRange with an existing ID
        programHealthyRange.setId(1L);
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramHealthyRange in the database
        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkMaleMinIsRequired() throws Exception {
        int databaseSizeBeforeTest = programHealthyRangeRepository.findAll().size();
        // set the field null
        programHealthyRange.setMaleMin(null);

        // Create the ProgramHealthyRange, which fails.
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkMaleMaxIsRequired() throws Exception {
        int databaseSizeBeforeTest = programHealthyRangeRepository.findAll().size();
        // set the field null
        programHealthyRange.setMaleMax(null);

        // Create the ProgramHealthyRange, which fails.
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFemaleMinIsRequired() throws Exception {
        int databaseSizeBeforeTest = programHealthyRangeRepository.findAll().size();
        // set the field null
        programHealthyRange.setFemaleMin(null);

        // Create the ProgramHealthyRange, which fails.
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFemaleMaxIsRequired() throws Exception {
        int databaseSizeBeforeTest = programHealthyRangeRepository.findAll().size();
        // set the field null
        programHealthyRange.setFemaleMax(null);

        // Create the ProgramHealthyRange, which fails.
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUnidentifiedMinIsRequired() throws Exception {
        int databaseSizeBeforeTest = programHealthyRangeRepository.findAll().size();
        // set the field null
        programHealthyRange.setUnidentifiedMin(null);

        // Create the ProgramHealthyRange, which fails.
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUnidentifiedMaxIsRequired() throws Exception {
        int databaseSizeBeforeTest = programHealthyRangeRepository.findAll().size();
        // set the field null
        programHealthyRange.setUnidentifiedMax(null);

        // Create the ProgramHealthyRange, which fails.
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBiometricCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programHealthyRangeRepository.findAll().size();
        // set the field null
        programHealthyRange.setBiometricCode(null);

        // Create the ProgramHealthyRange, which fails.
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubCategoryIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programHealthyRangeRepository.findAll().size();
        // set the field null
        programHealthyRange.setSubCategoryId(null);

        // Create the ProgramHealthyRange, which fails.
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        restProgramHealthyRangeMockMvc.perform(post("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRanges() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList
        restProgramHealthyRangeMockMvc.perform(get("/api/program-healthy-ranges?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programHealthyRange.getId().intValue())))
            .andExpect(jsonPath("$.[*].maleMin").value(hasItem(DEFAULT_MALE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].maleMax").value(hasItem(DEFAULT_MALE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].femaleMin").value(hasItem(DEFAULT_FEMALE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].femaleMax").value(hasItem(DEFAULT_FEMALE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].unidentifiedMin").value(hasItem(DEFAULT_UNIDENTIFIED_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].unidentifiedMax").value(hasItem(DEFAULT_UNIDENTIFIED_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].isApplyPoint").value(hasItem(DEFAULT_IS_APPLY_POINT.booleanValue())))
            .andExpect(jsonPath("$.[*].biometricCode").value(hasItem(DEFAULT_BIOMETRIC_CODE.toString())))
            .andExpect(jsonPath("$.[*].subCategoryCode").value(hasItem(DEFAULT_SUB_CATEGORY_CODE.toString())))
            .andExpect(jsonPath("$.[*].subCategoryId").value(hasItem(DEFAULT_SUB_CATEGORY_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getProgramHealthyRange() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get the programHealthyRange
        restProgramHealthyRangeMockMvc.perform(get("/api/program-healthy-ranges/{id}", programHealthyRange.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programHealthyRange.getId().intValue()))
            .andExpect(jsonPath("$.maleMin").value(DEFAULT_MALE_MIN.doubleValue()))
            .andExpect(jsonPath("$.maleMax").value(DEFAULT_MALE_MAX.doubleValue()))
            .andExpect(jsonPath("$.femaleMin").value(DEFAULT_FEMALE_MIN.doubleValue()))
            .andExpect(jsonPath("$.femaleMax").value(DEFAULT_FEMALE_MAX.doubleValue()))
            .andExpect(jsonPath("$.unidentifiedMin").value(DEFAULT_UNIDENTIFIED_MIN.doubleValue()))
            .andExpect(jsonPath("$.unidentifiedMax").value(DEFAULT_UNIDENTIFIED_MAX.doubleValue()))
            .andExpect(jsonPath("$.isApplyPoint").value(DEFAULT_IS_APPLY_POINT.booleanValue()))
            .andExpect(jsonPath("$.biometricCode").value(DEFAULT_BIOMETRIC_CODE.toString()))
            .andExpect(jsonPath("$.subCategoryCode").value(DEFAULT_SUB_CATEGORY_CODE.toString()))
            .andExpect(jsonPath("$.subCategoryId").value(DEFAULT_SUB_CATEGORY_ID.intValue()));
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByMaleMinIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where maleMin equals to DEFAULT_MALE_MIN
        defaultProgramHealthyRangeShouldBeFound("maleMin.equals=" + DEFAULT_MALE_MIN);

        // Get all the programHealthyRangeList where maleMin equals to UPDATED_MALE_MIN
        defaultProgramHealthyRangeShouldNotBeFound("maleMin.equals=" + UPDATED_MALE_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByMaleMinIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where maleMin in DEFAULT_MALE_MIN or UPDATED_MALE_MIN
        defaultProgramHealthyRangeShouldBeFound("maleMin.in=" + DEFAULT_MALE_MIN + "," + UPDATED_MALE_MIN);

        // Get all the programHealthyRangeList where maleMin equals to UPDATED_MALE_MIN
        defaultProgramHealthyRangeShouldNotBeFound("maleMin.in=" + UPDATED_MALE_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByMaleMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where maleMin is not null
        defaultProgramHealthyRangeShouldBeFound("maleMin.specified=true");

        // Get all the programHealthyRangeList where maleMin is null
        defaultProgramHealthyRangeShouldNotBeFound("maleMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByMaleMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where maleMax equals to DEFAULT_MALE_MAX
        defaultProgramHealthyRangeShouldBeFound("maleMax.equals=" + DEFAULT_MALE_MAX);

        // Get all the programHealthyRangeList where maleMax equals to UPDATED_MALE_MAX
        defaultProgramHealthyRangeShouldNotBeFound("maleMax.equals=" + UPDATED_MALE_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByMaleMaxIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where maleMax in DEFAULT_MALE_MAX or UPDATED_MALE_MAX
        defaultProgramHealthyRangeShouldBeFound("maleMax.in=" + DEFAULT_MALE_MAX + "," + UPDATED_MALE_MAX);

        // Get all the programHealthyRangeList where maleMax equals to UPDATED_MALE_MAX
        defaultProgramHealthyRangeShouldNotBeFound("maleMax.in=" + UPDATED_MALE_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByMaleMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where maleMax is not null
        defaultProgramHealthyRangeShouldBeFound("maleMax.specified=true");

        // Get all the programHealthyRangeList where maleMax is null
        defaultProgramHealthyRangeShouldNotBeFound("maleMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByFemaleMinIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where femaleMin equals to DEFAULT_FEMALE_MIN
        defaultProgramHealthyRangeShouldBeFound("femaleMin.equals=" + DEFAULT_FEMALE_MIN);

        // Get all the programHealthyRangeList where femaleMin equals to UPDATED_FEMALE_MIN
        defaultProgramHealthyRangeShouldNotBeFound("femaleMin.equals=" + UPDATED_FEMALE_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByFemaleMinIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where femaleMin in DEFAULT_FEMALE_MIN or UPDATED_FEMALE_MIN
        defaultProgramHealthyRangeShouldBeFound("femaleMin.in=" + DEFAULT_FEMALE_MIN + "," + UPDATED_FEMALE_MIN);

        // Get all the programHealthyRangeList where femaleMin equals to UPDATED_FEMALE_MIN
        defaultProgramHealthyRangeShouldNotBeFound("femaleMin.in=" + UPDATED_FEMALE_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByFemaleMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where femaleMin is not null
        defaultProgramHealthyRangeShouldBeFound("femaleMin.specified=true");

        // Get all the programHealthyRangeList where femaleMin is null
        defaultProgramHealthyRangeShouldNotBeFound("femaleMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByFemaleMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where femaleMax equals to DEFAULT_FEMALE_MAX
        defaultProgramHealthyRangeShouldBeFound("femaleMax.equals=" + DEFAULT_FEMALE_MAX);

        // Get all the programHealthyRangeList where femaleMax equals to UPDATED_FEMALE_MAX
        defaultProgramHealthyRangeShouldNotBeFound("femaleMax.equals=" + UPDATED_FEMALE_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByFemaleMaxIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where femaleMax in DEFAULT_FEMALE_MAX or UPDATED_FEMALE_MAX
        defaultProgramHealthyRangeShouldBeFound("femaleMax.in=" + DEFAULT_FEMALE_MAX + "," + UPDATED_FEMALE_MAX);

        // Get all the programHealthyRangeList where femaleMax equals to UPDATED_FEMALE_MAX
        defaultProgramHealthyRangeShouldNotBeFound("femaleMax.in=" + UPDATED_FEMALE_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByFemaleMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where femaleMax is not null
        defaultProgramHealthyRangeShouldBeFound("femaleMax.specified=true");

        // Get all the programHealthyRangeList where femaleMax is null
        defaultProgramHealthyRangeShouldNotBeFound("femaleMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByUnidentifiedMinIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where unidentifiedMin equals to DEFAULT_UNIDENTIFIED_MIN
        defaultProgramHealthyRangeShouldBeFound("unidentifiedMin.equals=" + DEFAULT_UNIDENTIFIED_MIN);

        // Get all the programHealthyRangeList where unidentifiedMin equals to UPDATED_UNIDENTIFIED_MIN
        defaultProgramHealthyRangeShouldNotBeFound("unidentifiedMin.equals=" + UPDATED_UNIDENTIFIED_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByUnidentifiedMinIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where unidentifiedMin in DEFAULT_UNIDENTIFIED_MIN or UPDATED_UNIDENTIFIED_MIN
        defaultProgramHealthyRangeShouldBeFound("unidentifiedMin.in=" + DEFAULT_UNIDENTIFIED_MIN + "," + UPDATED_UNIDENTIFIED_MIN);

        // Get all the programHealthyRangeList where unidentifiedMin equals to UPDATED_UNIDENTIFIED_MIN
        defaultProgramHealthyRangeShouldNotBeFound("unidentifiedMin.in=" + UPDATED_UNIDENTIFIED_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByUnidentifiedMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where unidentifiedMin is not null
        defaultProgramHealthyRangeShouldBeFound("unidentifiedMin.specified=true");

        // Get all the programHealthyRangeList where unidentifiedMin is null
        defaultProgramHealthyRangeShouldNotBeFound("unidentifiedMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByUnidentifiedMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where unidentifiedMax equals to DEFAULT_UNIDENTIFIED_MAX
        defaultProgramHealthyRangeShouldBeFound("unidentifiedMax.equals=" + DEFAULT_UNIDENTIFIED_MAX);

        // Get all the programHealthyRangeList where unidentifiedMax equals to UPDATED_UNIDENTIFIED_MAX
        defaultProgramHealthyRangeShouldNotBeFound("unidentifiedMax.equals=" + UPDATED_UNIDENTIFIED_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByUnidentifiedMaxIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where unidentifiedMax in DEFAULT_UNIDENTIFIED_MAX or UPDATED_UNIDENTIFIED_MAX
        defaultProgramHealthyRangeShouldBeFound("unidentifiedMax.in=" + DEFAULT_UNIDENTIFIED_MAX + "," + UPDATED_UNIDENTIFIED_MAX);

        // Get all the programHealthyRangeList where unidentifiedMax equals to UPDATED_UNIDENTIFIED_MAX
        defaultProgramHealthyRangeShouldNotBeFound("unidentifiedMax.in=" + UPDATED_UNIDENTIFIED_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByUnidentifiedMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where unidentifiedMax is not null
        defaultProgramHealthyRangeShouldBeFound("unidentifiedMax.specified=true");

        // Get all the programHealthyRangeList where unidentifiedMax is null
        defaultProgramHealthyRangeShouldNotBeFound("unidentifiedMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByIsApplyPointIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where isApplyPoint equals to DEFAULT_IS_APPLY_POINT
        defaultProgramHealthyRangeShouldBeFound("isApplyPoint.equals=" + DEFAULT_IS_APPLY_POINT);

        // Get all the programHealthyRangeList where isApplyPoint equals to UPDATED_IS_APPLY_POINT
        defaultProgramHealthyRangeShouldNotBeFound("isApplyPoint.equals=" + UPDATED_IS_APPLY_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByIsApplyPointIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where isApplyPoint in DEFAULT_IS_APPLY_POINT or UPDATED_IS_APPLY_POINT
        defaultProgramHealthyRangeShouldBeFound("isApplyPoint.in=" + DEFAULT_IS_APPLY_POINT + "," + UPDATED_IS_APPLY_POINT);

        // Get all the programHealthyRangeList where isApplyPoint equals to UPDATED_IS_APPLY_POINT
        defaultProgramHealthyRangeShouldNotBeFound("isApplyPoint.in=" + UPDATED_IS_APPLY_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByIsApplyPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where isApplyPoint is not null
        defaultProgramHealthyRangeShouldBeFound("isApplyPoint.specified=true");

        // Get all the programHealthyRangeList where isApplyPoint is null
        defaultProgramHealthyRangeShouldNotBeFound("isApplyPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByBiometricCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where biometricCode equals to DEFAULT_BIOMETRIC_CODE
        defaultProgramHealthyRangeShouldBeFound("biometricCode.equals=" + DEFAULT_BIOMETRIC_CODE);

        // Get all the programHealthyRangeList where biometricCode equals to UPDATED_BIOMETRIC_CODE
        defaultProgramHealthyRangeShouldNotBeFound("biometricCode.equals=" + UPDATED_BIOMETRIC_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByBiometricCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where biometricCode in DEFAULT_BIOMETRIC_CODE or UPDATED_BIOMETRIC_CODE
        defaultProgramHealthyRangeShouldBeFound("biometricCode.in=" + DEFAULT_BIOMETRIC_CODE + "," + UPDATED_BIOMETRIC_CODE);

        // Get all the programHealthyRangeList where biometricCode equals to UPDATED_BIOMETRIC_CODE
        defaultProgramHealthyRangeShouldNotBeFound("biometricCode.in=" + UPDATED_BIOMETRIC_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesByBiometricCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where biometricCode is not null
        defaultProgramHealthyRangeShouldBeFound("biometricCode.specified=true");

        // Get all the programHealthyRangeList where biometricCode is null
        defaultProgramHealthyRangeShouldNotBeFound("biometricCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesBySubCategoryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where subCategoryCode equals to DEFAULT_SUB_CATEGORY_CODE
        defaultProgramHealthyRangeShouldBeFound("subCategoryCode.equals=" + DEFAULT_SUB_CATEGORY_CODE);

        // Get all the programHealthyRangeList where subCategoryCode equals to UPDATED_SUB_CATEGORY_CODE
        defaultProgramHealthyRangeShouldNotBeFound("subCategoryCode.equals=" + UPDATED_SUB_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesBySubCategoryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where subCategoryCode in DEFAULT_SUB_CATEGORY_CODE or UPDATED_SUB_CATEGORY_CODE
        defaultProgramHealthyRangeShouldBeFound("subCategoryCode.in=" + DEFAULT_SUB_CATEGORY_CODE + "," + UPDATED_SUB_CATEGORY_CODE);

        // Get all the programHealthyRangeList where subCategoryCode equals to UPDATED_SUB_CATEGORY_CODE
        defaultProgramHealthyRangeShouldNotBeFound("subCategoryCode.in=" + UPDATED_SUB_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesBySubCategoryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where subCategoryCode is not null
        defaultProgramHealthyRangeShouldBeFound("subCategoryCode.specified=true");

        // Get all the programHealthyRangeList where subCategoryCode is null
        defaultProgramHealthyRangeShouldNotBeFound("subCategoryCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesBySubCategoryIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where subCategoryId equals to DEFAULT_SUB_CATEGORY_ID
        defaultProgramHealthyRangeShouldBeFound("subCategoryId.equals=" + DEFAULT_SUB_CATEGORY_ID);

        // Get all the programHealthyRangeList where subCategoryId equals to UPDATED_SUB_CATEGORY_ID
        defaultProgramHealthyRangeShouldNotBeFound("subCategoryId.equals=" + UPDATED_SUB_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesBySubCategoryIdIsInShouldWork() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where subCategoryId in DEFAULT_SUB_CATEGORY_ID or UPDATED_SUB_CATEGORY_ID
        defaultProgramHealthyRangeShouldBeFound("subCategoryId.in=" + DEFAULT_SUB_CATEGORY_ID + "," + UPDATED_SUB_CATEGORY_ID);

        // Get all the programHealthyRangeList where subCategoryId equals to UPDATED_SUB_CATEGORY_ID
        defaultProgramHealthyRangeShouldNotBeFound("subCategoryId.in=" + UPDATED_SUB_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesBySubCategoryIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where subCategoryId is not null
        defaultProgramHealthyRangeShouldBeFound("subCategoryId.specified=true");

        // Get all the programHealthyRangeList where subCategoryId is null
        defaultProgramHealthyRangeShouldNotBeFound("subCategoryId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesBySubCategoryIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where subCategoryId greater than or equals to DEFAULT_SUB_CATEGORY_ID
        defaultProgramHealthyRangeShouldBeFound("subCategoryId.greaterOrEqualThan=" + DEFAULT_SUB_CATEGORY_ID);

        // Get all the programHealthyRangeList where subCategoryId greater than or equals to UPDATED_SUB_CATEGORY_ID
        defaultProgramHealthyRangeShouldNotBeFound("subCategoryId.greaterOrEqualThan=" + UPDATED_SUB_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void getAllProgramHealthyRangesBySubCategoryIdIsLessThanSomething() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        // Get all the programHealthyRangeList where subCategoryId less than or equals to DEFAULT_SUB_CATEGORY_ID
        defaultProgramHealthyRangeShouldNotBeFound("subCategoryId.lessThan=" + DEFAULT_SUB_CATEGORY_ID);

        // Get all the programHealthyRangeList where subCategoryId less than or equals to UPDATED_SUB_CATEGORY_ID
        defaultProgramHealthyRangeShouldBeFound("subCategoryId.lessThan=" + UPDATED_SUB_CATEGORY_ID);
    }


    @Test
    @Transactional
    public void getAllProgramHealthyRangesByProgramIsEqualToSomething() throws Exception {
        // Initialize the database
        Program program = ProgramResourceIT.createEntity(em);
        em.persist(program);
        em.flush();
        programHealthyRange.setProgram(program);
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);
        Long programId = program.getId();

        // Get all the programHealthyRangeList where program equals to programId
        defaultProgramHealthyRangeShouldBeFound("programId.equals=" + programId);

        // Get all the programHealthyRangeList where program equals to programId + 1
        defaultProgramHealthyRangeShouldNotBeFound("programId.equals=" + (programId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramHealthyRangeShouldBeFound(String filter) throws Exception {
        restProgramHealthyRangeMockMvc.perform(get("/api/program-healthy-ranges?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programHealthyRange.getId().intValue())))
            .andExpect(jsonPath("$.[*].maleMin").value(hasItem(DEFAULT_MALE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].maleMax").value(hasItem(DEFAULT_MALE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].femaleMin").value(hasItem(DEFAULT_FEMALE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].femaleMax").value(hasItem(DEFAULT_FEMALE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].unidentifiedMin").value(hasItem(DEFAULT_UNIDENTIFIED_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].unidentifiedMax").value(hasItem(DEFAULT_UNIDENTIFIED_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].isApplyPoint").value(hasItem(DEFAULT_IS_APPLY_POINT.booleanValue())))
            .andExpect(jsonPath("$.[*].biometricCode").value(hasItem(DEFAULT_BIOMETRIC_CODE)))
            .andExpect(jsonPath("$.[*].subCategoryCode").value(hasItem(DEFAULT_SUB_CATEGORY_CODE)))
            .andExpect(jsonPath("$.[*].subCategoryId").value(hasItem(DEFAULT_SUB_CATEGORY_ID.intValue())));

        // Check, that the count call also returns 1
        restProgramHealthyRangeMockMvc.perform(get("/api/program-healthy-ranges/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramHealthyRangeShouldNotBeFound(String filter) throws Exception {
        restProgramHealthyRangeMockMvc.perform(get("/api/program-healthy-ranges?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramHealthyRangeMockMvc.perform(get("/api/program-healthy-ranges/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramHealthyRange() throws Exception {
        // Get the programHealthyRange
        restProgramHealthyRangeMockMvc.perform(get("/api/program-healthy-ranges/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramHealthyRange() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        int databaseSizeBeforeUpdate = programHealthyRangeRepository.findAll().size();

        // Update the programHealthyRange
        ProgramHealthyRange updatedProgramHealthyRange = programHealthyRangeRepository.findById(programHealthyRange.getId()).get();
        // Disconnect from session so that the updates on updatedProgramHealthyRange are not directly saved in db
        em.detach(updatedProgramHealthyRange);
        updatedProgramHealthyRange
            .maleMin(UPDATED_MALE_MIN)
            .maleMax(UPDATED_MALE_MAX)
            .femaleMin(UPDATED_FEMALE_MIN)
            .femaleMax(UPDATED_FEMALE_MAX)
            .unidentifiedMin(UPDATED_UNIDENTIFIED_MIN)
            .unidentifiedMax(UPDATED_UNIDENTIFIED_MAX)
            .isApplyPoint(UPDATED_IS_APPLY_POINT)
            .biometricCode(UPDATED_BIOMETRIC_CODE)
            .subCategoryCode(UPDATED_SUB_CATEGORY_CODE)
            .subCategoryId(UPDATED_SUB_CATEGORY_ID);
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(updatedProgramHealthyRange);

        restProgramHealthyRangeMockMvc.perform(put("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramHealthyRange in the database
        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeUpdate);
        ProgramHealthyRange testProgramHealthyRange = programHealthyRangeList.get(programHealthyRangeList.size() - 1);
        assertThat(testProgramHealthyRange.getMaleMin()).isEqualTo(UPDATED_MALE_MIN);
        assertThat(testProgramHealthyRange.getMaleMax()).isEqualTo(UPDATED_MALE_MAX);
        assertThat(testProgramHealthyRange.getFemaleMin()).isEqualTo(UPDATED_FEMALE_MIN);
        assertThat(testProgramHealthyRange.getFemaleMax()).isEqualTo(UPDATED_FEMALE_MAX);
        assertThat(testProgramHealthyRange.getUnidentifiedMin()).isEqualTo(UPDATED_UNIDENTIFIED_MIN);
        assertThat(testProgramHealthyRange.getUnidentifiedMax()).isEqualTo(UPDATED_UNIDENTIFIED_MAX);
        assertThat(testProgramHealthyRange.isIsApplyPoint()).isEqualTo(UPDATED_IS_APPLY_POINT);
        assertThat(testProgramHealthyRange.getBiometricCode()).isEqualTo(UPDATED_BIOMETRIC_CODE);
        assertThat(testProgramHealthyRange.getSubCategoryCode()).isEqualTo(UPDATED_SUB_CATEGORY_CODE);
        assertThat(testProgramHealthyRange.getSubCategoryId()).isEqualTo(UPDATED_SUB_CATEGORY_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramHealthyRange() throws Exception {
        int databaseSizeBeforeUpdate = programHealthyRangeRepository.findAll().size();

        // Create the ProgramHealthyRange
        ProgramHealthyRangeDTO programHealthyRangeDTO = programHealthyRangeMapper.toDto(programHealthyRange);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramHealthyRangeMockMvc.perform(put("/api/program-healthy-ranges")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programHealthyRangeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramHealthyRange in the database
        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramHealthyRange() throws Exception {
        // Initialize the database
        programHealthyRangeRepository.saveAndFlush(programHealthyRange);

        int databaseSizeBeforeDelete = programHealthyRangeRepository.findAll().size();

        // Delete the programHealthyRange
        restProgramHealthyRangeMockMvc.perform(delete("/api/program-healthy-ranges/{id}", programHealthyRange.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramHealthyRange> programHealthyRangeList = programHealthyRangeRepository.findAll();
        assertThat(programHealthyRangeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramHealthyRange.class);
        ProgramHealthyRange programHealthyRange1 = new ProgramHealthyRange();
        programHealthyRange1.setId(1L);
        ProgramHealthyRange programHealthyRange2 = new ProgramHealthyRange();
        programHealthyRange2.setId(programHealthyRange1.getId());
        assertThat(programHealthyRange1).isEqualTo(programHealthyRange2);
        programHealthyRange2.setId(2L);
        assertThat(programHealthyRange1).isNotEqualTo(programHealthyRange2);
        programHealthyRange1.setId(null);
        assertThat(programHealthyRange1).isNotEqualTo(programHealthyRange2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramHealthyRangeDTO.class);
        ProgramHealthyRangeDTO programHealthyRangeDTO1 = new ProgramHealthyRangeDTO();
        programHealthyRangeDTO1.setId(1L);
        ProgramHealthyRangeDTO programHealthyRangeDTO2 = new ProgramHealthyRangeDTO();
        assertThat(programHealthyRangeDTO1).isNotEqualTo(programHealthyRangeDTO2);
        programHealthyRangeDTO2.setId(programHealthyRangeDTO1.getId());
        assertThat(programHealthyRangeDTO1).isEqualTo(programHealthyRangeDTO2);
        programHealthyRangeDTO2.setId(2L);
        assertThat(programHealthyRangeDTO1).isNotEqualTo(programHealthyRangeDTO2);
        programHealthyRangeDTO1.setId(null);
        assertThat(programHealthyRangeDTO1).isNotEqualTo(programHealthyRangeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programHealthyRangeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programHealthyRangeMapper.fromId(null)).isNull();
    }
}
