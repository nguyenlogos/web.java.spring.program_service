package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.UserReward;
import aduro.basic.programservice.domain.ProgramUser;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.UserRewardRepository;
import aduro.basic.programservice.service.UserRewardService;
import aduro.basic.programservice.service.dto.UserRewardDTO;
import aduro.basic.programservice.service.mapper.UserRewardMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.UserRewardCriteria;
import aduro.basic.programservice.service.UserRewardQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link UserRewardResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class UserRewardResourceIT {

    private static final Integer DEFAULT_PROGRAM_LEVEL = 1;
    private static final Integer UPDATED_PROGRAM_LEVEL = 2;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_ORDER_ID = "AAAAAAAAAA";
    private static final String UPDATED_ORDER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_ORDER_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ORDER_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_GIFT_ID = "AAAAAAAAAA";
    private static final String UPDATED_GIFT_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_LEVEL_COMPLETED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LEVEL_COMPLETED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_PARTICIPANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_REWARD_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_REWARD_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_REWARD_CODE = "AAAAAAAAAA";
    private static final String UPDATED_REWARD_CODE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_REWARD_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_REWARD_AMOUNT = new BigDecimal(2);

    private static final String DEFAULT_CAMPAIGN_ID = "AAAAAAAAAA";
    private static final String UPDATED_CAMPAIGN_ID = "BBBBBBBBBB";

    private static final String DEFAULT_REWARD_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_REWARD_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_EVENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_ID = "BBBBBBBBBB";

    @Autowired
    private UserRewardRepository userRewardRepository;

    @Autowired
    private UserRewardMapper userRewardMapper;

    @Autowired
    private UserRewardService userRewardService;

    @Autowired
    private UserRewardQueryService userRewardQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restUserRewardMockMvc;

    private UserReward userReward;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserRewardResource userRewardResource = new UserRewardResource(userRewardService, userRewardQueryService);
        this.restUserRewardMockMvc = MockMvcBuilders.standaloneSetup(userRewardResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserReward createEntity(EntityManager em) {
        UserReward userReward = new UserReward()
            .programLevel(DEFAULT_PROGRAM_LEVEL)
            .status(DEFAULT_STATUS)
            .orderId(DEFAULT_ORDER_ID)
            .orderDate(DEFAULT_ORDER_DATE)
            .giftId(DEFAULT_GIFT_ID)
            .levelCompletedDate(DEFAULT_LEVEL_COMPLETED_DATE)
            .participantId(DEFAULT_PARTICIPANT_ID)
            .email(DEFAULT_EMAIL)
            .clientId(DEFAULT_CLIENT_ID)
            .rewardType(DEFAULT_REWARD_TYPE)
            .rewardCode(DEFAULT_REWARD_CODE)
            .rewardAmount(DEFAULT_REWARD_AMOUNT)
            .campaignId(DEFAULT_CAMPAIGN_ID)
            .rewardMessage(DEFAULT_REWARD_MESSAGE)
            .eventId(DEFAULT_EVENT_ID);
        // Add required entity
        ProgramUser programUser;
        if (TestUtil.findAll(em, ProgramUser.class).isEmpty()) {
            programUser = ProgramUserResourceIT.createEntity(em);
            em.persist(programUser);
            em.flush();
        } else {
            programUser = TestUtil.findAll(em, ProgramUser.class).get(0);
        }
        userReward.setProgramUser(programUser);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        userReward.setProgram(program);
        return userReward;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserReward createUpdatedEntity(EntityManager em) {
        UserReward userReward = new UserReward()
            .programLevel(UPDATED_PROGRAM_LEVEL)
            .status(UPDATED_STATUS)
            .orderId(UPDATED_ORDER_ID)
            .orderDate(UPDATED_ORDER_DATE)
            .giftId(UPDATED_GIFT_ID)
            .levelCompletedDate(UPDATED_LEVEL_COMPLETED_DATE)
            .participantId(UPDATED_PARTICIPANT_ID)
            .email(UPDATED_EMAIL)
            .clientId(UPDATED_CLIENT_ID)
            .rewardType(UPDATED_REWARD_TYPE)
            .rewardCode(UPDATED_REWARD_CODE)
            .rewardAmount(UPDATED_REWARD_AMOUNT)
            .campaignId(UPDATED_CAMPAIGN_ID)
            .rewardMessage(UPDATED_REWARD_MESSAGE)
            .eventId(UPDATED_EVENT_ID);
        // Add required entity
        ProgramUser programUser;
        if (TestUtil.findAll(em, ProgramUser.class).isEmpty()) {
            programUser = ProgramUserResourceIT.createUpdatedEntity(em);
            em.persist(programUser);
            em.flush();
        } else {
            programUser = TestUtil.findAll(em, ProgramUser.class).get(0);
        }
        userReward.setProgramUser(programUser);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createUpdatedEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        userReward.setProgram(program);
        return userReward;
    }

    @BeforeEach
    public void initTest() {
        userReward = createEntity(em);
    }

    @Test
    @Transactional
    public void createUserReward() throws Exception {
        int databaseSizeBeforeCreate = userRewardRepository.findAll().size();

        // Create the UserReward
        UserRewardDTO userRewardDTO = userRewardMapper.toDto(userReward);
        restUserRewardMockMvc.perform(post("/api/user-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userRewardDTO)))
            .andExpect(status().isCreated());

        // Validate the UserReward in the database
        List<UserReward> userRewardList = userRewardRepository.findAll();
        assertThat(userRewardList).hasSize(databaseSizeBeforeCreate + 1);
        UserReward testUserReward = userRewardList.get(userRewardList.size() - 1);
        assertThat(testUserReward.getProgramLevel()).isEqualTo(DEFAULT_PROGRAM_LEVEL);
        assertThat(testUserReward.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testUserReward.getOrderId()).isEqualTo(DEFAULT_ORDER_ID);
        assertThat(testUserReward.getOrderDate()).isEqualTo(DEFAULT_ORDER_DATE);
        assertThat(testUserReward.getGiftId()).isEqualTo(DEFAULT_GIFT_ID);
        assertThat(testUserReward.getLevelCompletedDate()).isEqualTo(DEFAULT_LEVEL_COMPLETED_DATE);
        assertThat(testUserReward.getParticipantId()).isEqualTo(DEFAULT_PARTICIPANT_ID);
        assertThat(testUserReward.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testUserReward.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testUserReward.getRewardType()).isEqualTo(DEFAULT_REWARD_TYPE);
        assertThat(testUserReward.getRewardCode()).isEqualTo(DEFAULT_REWARD_CODE);
        assertThat(testUserReward.getRewardAmount()).isEqualTo(DEFAULT_REWARD_AMOUNT);
        assertThat(testUserReward.getCampaignId()).isEqualTo(DEFAULT_CAMPAIGN_ID);
        assertThat(testUserReward.getRewardMessage()).isEqualTo(DEFAULT_REWARD_MESSAGE);
        assertThat(testUserReward.getEventId()).isEqualTo(DEFAULT_EVENT_ID);
    }

    @Test
    @Transactional
    public void createUserRewardWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userRewardRepository.findAll().size();

        // Create the UserReward with an existing ID
        userReward.setId(1L);
        UserRewardDTO userRewardDTO = userRewardMapper.toDto(userReward);

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserRewardMockMvc.perform(post("/api/user-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userRewardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserReward in the database
        List<UserReward> userRewardList = userRewardRepository.findAll();
        assertThat(userRewardList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkParticipantIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = userRewardRepository.findAll().size();
        // set the field null
        userReward.setParticipantId(null);

        // Create the UserReward, which fails.
        UserRewardDTO userRewardDTO = userRewardMapper.toDto(userReward);

        restUserRewardMockMvc.perform(post("/api/user-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userRewardDTO)))
            .andExpect(status().isBadRequest());

        List<UserReward> userRewardList = userRewardRepository.findAll();
        assertThat(userRewardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = userRewardRepository.findAll().size();
        // set the field null
        userReward.setEmail(null);

        // Create the UserReward, which fails.
        UserRewardDTO userRewardDTO = userRewardMapper.toDto(userReward);

        restUserRewardMockMvc.perform(post("/api/user-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userRewardDTO)))
            .andExpect(status().isBadRequest());

        List<UserReward> userRewardList = userRewardRepository.findAll();
        assertThat(userRewardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = userRewardRepository.findAll().size();
        // set the field null
        userReward.setClientId(null);

        // Create the UserReward, which fails.
        UserRewardDTO userRewardDTO = userRewardMapper.toDto(userReward);

        restUserRewardMockMvc.perform(post("/api/user-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userRewardDTO)))
            .andExpect(status().isBadRequest());

        List<UserReward> userRewardList = userRewardRepository.findAll();
        assertThat(userRewardList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllUserRewards() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList
        restUserRewardMockMvc.perform(get("/api/user-rewards?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userReward.getId().intValue())))
            .andExpect(jsonPath("$.[*].programLevel").value(hasItem(DEFAULT_PROGRAM_LEVEL)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].orderId").value(hasItem(DEFAULT_ORDER_ID.toString())))
            .andExpect(jsonPath("$.[*].orderDate").value(hasItem(DEFAULT_ORDER_DATE.toString())))
            .andExpect(jsonPath("$.[*].giftId").value(hasItem(DEFAULT_GIFT_ID.toString())))
            .andExpect(jsonPath("$.[*].levelCompletedDate").value(hasItem(DEFAULT_LEVEL_COMPLETED_DATE.toString())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].rewardType").value(hasItem(DEFAULT_REWARD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].rewardCode").value(hasItem(DEFAULT_REWARD_CODE.toString())))
            .andExpect(jsonPath("$.[*].rewardAmount").value(hasItem(DEFAULT_REWARD_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].campaignId").value(hasItem(DEFAULT_CAMPAIGN_ID.toString())))
            .andExpect(jsonPath("$.[*].rewardMessage").value(hasItem(DEFAULT_REWARD_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID.toString())));
    }
    
    @Test
    @Transactional
    public void getUserReward() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get the userReward
        restUserRewardMockMvc.perform(get("/api/user-rewards/{id}", userReward.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userReward.getId().intValue()))
            .andExpect(jsonPath("$.programLevel").value(DEFAULT_PROGRAM_LEVEL))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.orderId").value(DEFAULT_ORDER_ID.toString()))
            .andExpect(jsonPath("$.orderDate").value(DEFAULT_ORDER_DATE.toString()))
            .andExpect(jsonPath("$.giftId").value(DEFAULT_GIFT_ID.toString()))
            .andExpect(jsonPath("$.levelCompletedDate").value(DEFAULT_LEVEL_COMPLETED_DATE.toString()))
            .andExpect(jsonPath("$.participantId").value(DEFAULT_PARTICIPANT_ID.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.rewardType").value(DEFAULT_REWARD_TYPE.toString()))
            .andExpect(jsonPath("$.rewardCode").value(DEFAULT_REWARD_CODE.toString()))
            .andExpect(jsonPath("$.rewardAmount").value(DEFAULT_REWARD_AMOUNT.intValue()))
            .andExpect(jsonPath("$.campaignId").value(DEFAULT_CAMPAIGN_ID.toString()))
            .andExpect(jsonPath("$.rewardMessage").value(DEFAULT_REWARD_MESSAGE.toString()))
            .andExpect(jsonPath("$.eventId").value(DEFAULT_EVENT_ID.toString()));
    }

    @Test
    @Transactional
    public void getAllUserRewardsByProgramLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where programLevel equals to DEFAULT_PROGRAM_LEVEL
        defaultUserRewardShouldBeFound("programLevel.equals=" + DEFAULT_PROGRAM_LEVEL);

        // Get all the userRewardList where programLevel equals to UPDATED_PROGRAM_LEVEL
        defaultUserRewardShouldNotBeFound("programLevel.equals=" + UPDATED_PROGRAM_LEVEL);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByProgramLevelIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where programLevel in DEFAULT_PROGRAM_LEVEL or UPDATED_PROGRAM_LEVEL
        defaultUserRewardShouldBeFound("programLevel.in=" + DEFAULT_PROGRAM_LEVEL + "," + UPDATED_PROGRAM_LEVEL);

        // Get all the userRewardList where programLevel equals to UPDATED_PROGRAM_LEVEL
        defaultUserRewardShouldNotBeFound("programLevel.in=" + UPDATED_PROGRAM_LEVEL);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByProgramLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where programLevel is not null
        defaultUserRewardShouldBeFound("programLevel.specified=true");

        // Get all the userRewardList where programLevel is null
        defaultUserRewardShouldNotBeFound("programLevel.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByProgramLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where programLevel greater than or equals to DEFAULT_PROGRAM_LEVEL
        defaultUserRewardShouldBeFound("programLevel.greaterOrEqualThan=" + DEFAULT_PROGRAM_LEVEL);

        // Get all the userRewardList where programLevel greater than or equals to UPDATED_PROGRAM_LEVEL
        defaultUserRewardShouldNotBeFound("programLevel.greaterOrEqualThan=" + UPDATED_PROGRAM_LEVEL);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByProgramLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where programLevel less than or equals to DEFAULT_PROGRAM_LEVEL
        defaultUserRewardShouldNotBeFound("programLevel.lessThan=" + DEFAULT_PROGRAM_LEVEL);

        // Get all the userRewardList where programLevel less than or equals to UPDATED_PROGRAM_LEVEL
        defaultUserRewardShouldBeFound("programLevel.lessThan=" + UPDATED_PROGRAM_LEVEL);
    }


    @Test
    @Transactional
    public void getAllUserRewardsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where status equals to DEFAULT_STATUS
        defaultUserRewardShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the userRewardList where status equals to UPDATED_STATUS
        defaultUserRewardShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultUserRewardShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the userRewardList where status equals to UPDATED_STATUS
        defaultUserRewardShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where status is not null
        defaultUserRewardShouldBeFound("status.specified=true");

        // Get all the userRewardList where status is null
        defaultUserRewardShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByOrderIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where orderId equals to DEFAULT_ORDER_ID
        defaultUserRewardShouldBeFound("orderId.equals=" + DEFAULT_ORDER_ID);

        // Get all the userRewardList where orderId equals to UPDATED_ORDER_ID
        defaultUserRewardShouldNotBeFound("orderId.equals=" + UPDATED_ORDER_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByOrderIdIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where orderId in DEFAULT_ORDER_ID or UPDATED_ORDER_ID
        defaultUserRewardShouldBeFound("orderId.in=" + DEFAULT_ORDER_ID + "," + UPDATED_ORDER_ID);

        // Get all the userRewardList where orderId equals to UPDATED_ORDER_ID
        defaultUserRewardShouldNotBeFound("orderId.in=" + UPDATED_ORDER_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByOrderIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where orderId is not null
        defaultUserRewardShouldBeFound("orderId.specified=true");

        // Get all the userRewardList where orderId is null
        defaultUserRewardShouldNotBeFound("orderId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByOrderDateIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where orderDate equals to DEFAULT_ORDER_DATE
        defaultUserRewardShouldBeFound("orderDate.equals=" + DEFAULT_ORDER_DATE);

        // Get all the userRewardList where orderDate equals to UPDATED_ORDER_DATE
        defaultUserRewardShouldNotBeFound("orderDate.equals=" + UPDATED_ORDER_DATE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByOrderDateIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where orderDate in DEFAULT_ORDER_DATE or UPDATED_ORDER_DATE
        defaultUserRewardShouldBeFound("orderDate.in=" + DEFAULT_ORDER_DATE + "," + UPDATED_ORDER_DATE);

        // Get all the userRewardList where orderDate equals to UPDATED_ORDER_DATE
        defaultUserRewardShouldNotBeFound("orderDate.in=" + UPDATED_ORDER_DATE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByOrderDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where orderDate is not null
        defaultUserRewardShouldBeFound("orderDate.specified=true");

        // Get all the userRewardList where orderDate is null
        defaultUserRewardShouldNotBeFound("orderDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByGiftIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where giftId equals to DEFAULT_GIFT_ID
        defaultUserRewardShouldBeFound("giftId.equals=" + DEFAULT_GIFT_ID);

        // Get all the userRewardList where giftId equals to UPDATED_GIFT_ID
        defaultUserRewardShouldNotBeFound("giftId.equals=" + UPDATED_GIFT_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByGiftIdIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where giftId in DEFAULT_GIFT_ID or UPDATED_GIFT_ID
        defaultUserRewardShouldBeFound("giftId.in=" + DEFAULT_GIFT_ID + "," + UPDATED_GIFT_ID);

        // Get all the userRewardList where giftId equals to UPDATED_GIFT_ID
        defaultUserRewardShouldNotBeFound("giftId.in=" + UPDATED_GIFT_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByGiftIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where giftId is not null
        defaultUserRewardShouldBeFound("giftId.specified=true");

        // Get all the userRewardList where giftId is null
        defaultUserRewardShouldNotBeFound("giftId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByLevelCompletedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where levelCompletedDate equals to DEFAULT_LEVEL_COMPLETED_DATE
        defaultUserRewardShouldBeFound("levelCompletedDate.equals=" + DEFAULT_LEVEL_COMPLETED_DATE);

        // Get all the userRewardList where levelCompletedDate equals to UPDATED_LEVEL_COMPLETED_DATE
        defaultUserRewardShouldNotBeFound("levelCompletedDate.equals=" + UPDATED_LEVEL_COMPLETED_DATE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByLevelCompletedDateIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where levelCompletedDate in DEFAULT_LEVEL_COMPLETED_DATE or UPDATED_LEVEL_COMPLETED_DATE
        defaultUserRewardShouldBeFound("levelCompletedDate.in=" + DEFAULT_LEVEL_COMPLETED_DATE + "," + UPDATED_LEVEL_COMPLETED_DATE);

        // Get all the userRewardList where levelCompletedDate equals to UPDATED_LEVEL_COMPLETED_DATE
        defaultUserRewardShouldNotBeFound("levelCompletedDate.in=" + UPDATED_LEVEL_COMPLETED_DATE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByLevelCompletedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where levelCompletedDate is not null
        defaultUserRewardShouldBeFound("levelCompletedDate.specified=true");

        // Get all the userRewardList where levelCompletedDate is null
        defaultUserRewardShouldNotBeFound("levelCompletedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByParticipantIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where participantId equals to DEFAULT_PARTICIPANT_ID
        defaultUserRewardShouldBeFound("participantId.equals=" + DEFAULT_PARTICIPANT_ID);

        // Get all the userRewardList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultUserRewardShouldNotBeFound("participantId.equals=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByParticipantIdIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where participantId in DEFAULT_PARTICIPANT_ID or UPDATED_PARTICIPANT_ID
        defaultUserRewardShouldBeFound("participantId.in=" + DEFAULT_PARTICIPANT_ID + "," + UPDATED_PARTICIPANT_ID);

        // Get all the userRewardList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultUserRewardShouldNotBeFound("participantId.in=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByParticipantIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where participantId is not null
        defaultUserRewardShouldBeFound("participantId.specified=true");

        // Get all the userRewardList where participantId is null
        defaultUserRewardShouldNotBeFound("participantId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where email equals to DEFAULT_EMAIL
        defaultUserRewardShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the userRewardList where email equals to UPDATED_EMAIL
        defaultUserRewardShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultUserRewardShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the userRewardList where email equals to UPDATED_EMAIL
        defaultUserRewardShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where email is not null
        defaultUserRewardShouldBeFound("email.specified=true");

        // Get all the userRewardList where email is null
        defaultUserRewardShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where clientId equals to DEFAULT_CLIENT_ID
        defaultUserRewardShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the userRewardList where clientId equals to UPDATED_CLIENT_ID
        defaultUserRewardShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultUserRewardShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the userRewardList where clientId equals to UPDATED_CLIENT_ID
        defaultUserRewardShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where clientId is not null
        defaultUserRewardShouldBeFound("clientId.specified=true");

        // Get all the userRewardList where clientId is null
        defaultUserRewardShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardType equals to DEFAULT_REWARD_TYPE
        defaultUserRewardShouldBeFound("rewardType.equals=" + DEFAULT_REWARD_TYPE);

        // Get all the userRewardList where rewardType equals to UPDATED_REWARD_TYPE
        defaultUserRewardShouldNotBeFound("rewardType.equals=" + UPDATED_REWARD_TYPE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardTypeIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardType in DEFAULT_REWARD_TYPE or UPDATED_REWARD_TYPE
        defaultUserRewardShouldBeFound("rewardType.in=" + DEFAULT_REWARD_TYPE + "," + UPDATED_REWARD_TYPE);

        // Get all the userRewardList where rewardType equals to UPDATED_REWARD_TYPE
        defaultUserRewardShouldNotBeFound("rewardType.in=" + UPDATED_REWARD_TYPE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardType is not null
        defaultUserRewardShouldBeFound("rewardType.specified=true");

        // Get all the userRewardList where rewardType is null
        defaultUserRewardShouldNotBeFound("rewardType.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardCode equals to DEFAULT_REWARD_CODE
        defaultUserRewardShouldBeFound("rewardCode.equals=" + DEFAULT_REWARD_CODE);

        // Get all the userRewardList where rewardCode equals to UPDATED_REWARD_CODE
        defaultUserRewardShouldNotBeFound("rewardCode.equals=" + UPDATED_REWARD_CODE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardCodeIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardCode in DEFAULT_REWARD_CODE or UPDATED_REWARD_CODE
        defaultUserRewardShouldBeFound("rewardCode.in=" + DEFAULT_REWARD_CODE + "," + UPDATED_REWARD_CODE);

        // Get all the userRewardList where rewardCode equals to UPDATED_REWARD_CODE
        defaultUserRewardShouldNotBeFound("rewardCode.in=" + UPDATED_REWARD_CODE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardCode is not null
        defaultUserRewardShouldBeFound("rewardCode.specified=true");

        // Get all the userRewardList where rewardCode is null
        defaultUserRewardShouldNotBeFound("rewardCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardAmount equals to DEFAULT_REWARD_AMOUNT
        defaultUserRewardShouldBeFound("rewardAmount.equals=" + DEFAULT_REWARD_AMOUNT);

        // Get all the userRewardList where rewardAmount equals to UPDATED_REWARD_AMOUNT
        defaultUserRewardShouldNotBeFound("rewardAmount.equals=" + UPDATED_REWARD_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardAmountIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardAmount in DEFAULT_REWARD_AMOUNT or UPDATED_REWARD_AMOUNT
        defaultUserRewardShouldBeFound("rewardAmount.in=" + DEFAULT_REWARD_AMOUNT + "," + UPDATED_REWARD_AMOUNT);

        // Get all the userRewardList where rewardAmount equals to UPDATED_REWARD_AMOUNT
        defaultUserRewardShouldNotBeFound("rewardAmount.in=" + UPDATED_REWARD_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardAmount is not null
        defaultUserRewardShouldBeFound("rewardAmount.specified=true");

        // Get all the userRewardList where rewardAmount is null
        defaultUserRewardShouldNotBeFound("rewardAmount.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByCampaignIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where campaignId equals to DEFAULT_CAMPAIGN_ID
        defaultUserRewardShouldBeFound("campaignId.equals=" + DEFAULT_CAMPAIGN_ID);

        // Get all the userRewardList where campaignId equals to UPDATED_CAMPAIGN_ID
        defaultUserRewardShouldNotBeFound("campaignId.equals=" + UPDATED_CAMPAIGN_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByCampaignIdIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where campaignId in DEFAULT_CAMPAIGN_ID or UPDATED_CAMPAIGN_ID
        defaultUserRewardShouldBeFound("campaignId.in=" + DEFAULT_CAMPAIGN_ID + "," + UPDATED_CAMPAIGN_ID);

        // Get all the userRewardList where campaignId equals to UPDATED_CAMPAIGN_ID
        defaultUserRewardShouldNotBeFound("campaignId.in=" + UPDATED_CAMPAIGN_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByCampaignIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where campaignId is not null
        defaultUserRewardShouldBeFound("campaignId.specified=true");

        // Get all the userRewardList where campaignId is null
        defaultUserRewardShouldNotBeFound("campaignId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardMessage equals to DEFAULT_REWARD_MESSAGE
        defaultUserRewardShouldBeFound("rewardMessage.equals=" + DEFAULT_REWARD_MESSAGE);

        // Get all the userRewardList where rewardMessage equals to UPDATED_REWARD_MESSAGE
        defaultUserRewardShouldNotBeFound("rewardMessage.equals=" + UPDATED_REWARD_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardMessageIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardMessage in DEFAULT_REWARD_MESSAGE or UPDATED_REWARD_MESSAGE
        defaultUserRewardShouldBeFound("rewardMessage.in=" + DEFAULT_REWARD_MESSAGE + "," + UPDATED_REWARD_MESSAGE);

        // Get all the userRewardList where rewardMessage equals to UPDATED_REWARD_MESSAGE
        defaultUserRewardShouldNotBeFound("rewardMessage.in=" + UPDATED_REWARD_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByRewardMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where rewardMessage is not null
        defaultUserRewardShouldBeFound("rewardMessage.specified=true");

        // Get all the userRewardList where rewardMessage is null
        defaultUserRewardShouldNotBeFound("rewardMessage.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByEventIdIsEqualToSomething() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where eventId equals to DEFAULT_EVENT_ID
        defaultUserRewardShouldBeFound("eventId.equals=" + DEFAULT_EVENT_ID);

        // Get all the userRewardList where eventId equals to UPDATED_EVENT_ID
        defaultUserRewardShouldNotBeFound("eventId.equals=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByEventIdIsInShouldWork() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where eventId in DEFAULT_EVENT_ID or UPDATED_EVENT_ID
        defaultUserRewardShouldBeFound("eventId.in=" + DEFAULT_EVENT_ID + "," + UPDATED_EVENT_ID);

        // Get all the userRewardList where eventId equals to UPDATED_EVENT_ID
        defaultUserRewardShouldNotBeFound("eventId.in=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllUserRewardsByEventIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        // Get all the userRewardList where eventId is not null
        defaultUserRewardShouldBeFound("eventId.specified=true");

        // Get all the userRewardList where eventId is null
        defaultUserRewardShouldNotBeFound("eventId.specified=false");
    }

    @Test
    @Transactional
    public void getAllUserRewardsByProgramUserIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramUser programUser = userReward.getProgramUser();
        userRewardRepository.saveAndFlush(userReward);
        Long programUserId = programUser.getId();

        // Get all the userRewardList where programUser equals to programUserId
        defaultUserRewardShouldBeFound("programUserId.equals=" + programUserId);

        // Get all the userRewardList where programUser equals to programUserId + 1
        defaultUserRewardShouldNotBeFound("programUserId.equals=" + (programUserId + 1));
    }


    @Test
    @Transactional
    public void getAllUserRewardsByProgramIsEqualToSomething() throws Exception {
        // Get already existing entity
        Program program = userReward.getProgram();
        userRewardRepository.saveAndFlush(userReward);
        Long programId = program.getId();

        // Get all the userRewardList where program equals to programId
        defaultUserRewardShouldBeFound("programId.equals=" + programId);

        // Get all the userRewardList where program equals to programId + 1
        defaultUserRewardShouldNotBeFound("programId.equals=" + (programId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultUserRewardShouldBeFound(String filter) throws Exception {
        restUserRewardMockMvc.perform(get("/api/user-rewards?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userReward.getId().intValue())))
            .andExpect(jsonPath("$.[*].programLevel").value(hasItem(DEFAULT_PROGRAM_LEVEL)))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].orderId").value(hasItem(DEFAULT_ORDER_ID)))
            .andExpect(jsonPath("$.[*].orderDate").value(hasItem(DEFAULT_ORDER_DATE.toString())))
            .andExpect(jsonPath("$.[*].giftId").value(hasItem(DEFAULT_GIFT_ID)))
            .andExpect(jsonPath("$.[*].levelCompletedDate").value(hasItem(DEFAULT_LEVEL_COMPLETED_DATE.toString())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].rewardType").value(hasItem(DEFAULT_REWARD_TYPE)))
            .andExpect(jsonPath("$.[*].rewardCode").value(hasItem(DEFAULT_REWARD_CODE)))
            .andExpect(jsonPath("$.[*].rewardAmount").value(hasItem(DEFAULT_REWARD_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].campaignId").value(hasItem(DEFAULT_CAMPAIGN_ID)))
            .andExpect(jsonPath("$.[*].rewardMessage").value(hasItem(DEFAULT_REWARD_MESSAGE)))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID)));

        // Check, that the count call also returns 1
        restUserRewardMockMvc.perform(get("/api/user-rewards/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultUserRewardShouldNotBeFound(String filter) throws Exception {
        restUserRewardMockMvc.perform(get("/api/user-rewards?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restUserRewardMockMvc.perform(get("/api/user-rewards/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingUserReward() throws Exception {
        // Get the userReward
        restUserRewardMockMvc.perform(get("/api/user-rewards/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateUserReward() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        int databaseSizeBeforeUpdate = userRewardRepository.findAll().size();

        // Update the userReward
        UserReward updatedUserReward = userRewardRepository.findById(userReward.getId()).get();
        // Disconnect from session so that the updates on updatedUserReward are not directly saved in db
        em.detach(updatedUserReward);
        updatedUserReward
            .programLevel(UPDATED_PROGRAM_LEVEL)
            .status(UPDATED_STATUS)
            .orderId(UPDATED_ORDER_ID)
            .orderDate(UPDATED_ORDER_DATE)
            .giftId(UPDATED_GIFT_ID)
            .levelCompletedDate(UPDATED_LEVEL_COMPLETED_DATE)
            .participantId(UPDATED_PARTICIPANT_ID)
            .email(UPDATED_EMAIL)
            .clientId(UPDATED_CLIENT_ID)
            .rewardType(UPDATED_REWARD_TYPE)
            .rewardCode(UPDATED_REWARD_CODE)
            .rewardAmount(UPDATED_REWARD_AMOUNT)
            .campaignId(UPDATED_CAMPAIGN_ID)
            .rewardMessage(UPDATED_REWARD_MESSAGE)
            .eventId(UPDATED_EVENT_ID);
        UserRewardDTO userRewardDTO = userRewardMapper.toDto(updatedUserReward);

        restUserRewardMockMvc.perform(put("/api/user-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userRewardDTO)))
            .andExpect(status().isOk());

        // Validate the UserReward in the database
        List<UserReward> userRewardList = userRewardRepository.findAll();
        assertThat(userRewardList).hasSize(databaseSizeBeforeUpdate);
        UserReward testUserReward = userRewardList.get(userRewardList.size() - 1);
        assertThat(testUserReward.getProgramLevel()).isEqualTo(UPDATED_PROGRAM_LEVEL);
        assertThat(testUserReward.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testUserReward.getOrderId()).isEqualTo(UPDATED_ORDER_ID);
        assertThat(testUserReward.getOrderDate()).isEqualTo(UPDATED_ORDER_DATE);
        assertThat(testUserReward.getGiftId()).isEqualTo(UPDATED_GIFT_ID);
        assertThat(testUserReward.getLevelCompletedDate()).isEqualTo(UPDATED_LEVEL_COMPLETED_DATE);
        assertThat(testUserReward.getParticipantId()).isEqualTo(UPDATED_PARTICIPANT_ID);
        assertThat(testUserReward.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testUserReward.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testUserReward.getRewardType()).isEqualTo(UPDATED_REWARD_TYPE);
        assertThat(testUserReward.getRewardCode()).isEqualTo(UPDATED_REWARD_CODE);
        assertThat(testUserReward.getRewardAmount()).isEqualTo(UPDATED_REWARD_AMOUNT);
        assertThat(testUserReward.getCampaignId()).isEqualTo(UPDATED_CAMPAIGN_ID);
        assertThat(testUserReward.getRewardMessage()).isEqualTo(UPDATED_REWARD_MESSAGE);
        assertThat(testUserReward.getEventId()).isEqualTo(UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingUserReward() throws Exception {
        int databaseSizeBeforeUpdate = userRewardRepository.findAll().size();

        // Create the UserReward
        UserRewardDTO userRewardDTO = userRewardMapper.toDto(userReward);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserRewardMockMvc.perform(put("/api/user-rewards")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userRewardDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserReward in the database
        List<UserReward> userRewardList = userRewardRepository.findAll();
        assertThat(userRewardList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteUserReward() throws Exception {
        // Initialize the database
        userRewardRepository.saveAndFlush(userReward);

        int databaseSizeBeforeDelete = userRewardRepository.findAll().size();

        // Delete the userReward
        restUserRewardMockMvc.perform(delete("/api/user-rewards/{id}", userReward.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<UserReward> userRewardList = userRewardRepository.findAll();
        assertThat(userRewardList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserReward.class);
        UserReward userReward1 = new UserReward();
        userReward1.setId(1L);
        UserReward userReward2 = new UserReward();
        userReward2.setId(userReward1.getId());
        assertThat(userReward1).isEqualTo(userReward2);
        userReward2.setId(2L);
        assertThat(userReward1).isNotEqualTo(userReward2);
        userReward1.setId(null);
        assertThat(userReward1).isNotEqualTo(userReward2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserRewardDTO.class);
        UserRewardDTO userRewardDTO1 = new UserRewardDTO();
        userRewardDTO1.setId(1L);
        UserRewardDTO userRewardDTO2 = new UserRewardDTO();
        assertThat(userRewardDTO1).isNotEqualTo(userRewardDTO2);
        userRewardDTO2.setId(userRewardDTO1.getId());
        assertThat(userRewardDTO1).isEqualTo(userRewardDTO2);
        userRewardDTO2.setId(2L);
        assertThat(userRewardDTO1).isNotEqualTo(userRewardDTO2);
        userRewardDTO1.setId(null);
        assertThat(userRewardDTO1).isNotEqualTo(userRewardDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(userRewardMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(userRewardMapper.fromId(null)).isNull();
    }
}
