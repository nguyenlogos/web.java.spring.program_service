package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramLevelActivity;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.repository.ProgramLevelActivityRepository;
import aduro.basic.programservice.service.ProgramLevelActivityService;
import aduro.basic.programservice.service.dto.ProgramLevelActivityDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelActivityMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramLevelActivityCriteria;
import aduro.basic.programservice.service.ProgramLevelActivityQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramLevelActivityResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramLevelActivityResourceIT {

    private static final String DEFAULT_ACTIVITY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVITY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIVITY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVITY_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_ALLOWED_COMPLETIONS = 1;
    private static final Integer UPDATED_ALLOWED_COMPLETIONS = 2;

    private static final Integer DEFAULT_REWARD_POINTS = 1;
    private static final Integer UPDATED_REWARD_POINTS = 2;

    private static final Boolean DEFAULT_REQUIRED = false;
    private static final Boolean UPDATED_REQUIRED = true;

    private static final String DEFAULT_ACTIVITY_ID = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVITY_ID = "BBBBBBBBBB";

    @Autowired
    private ProgramLevelActivityRepository programLevelActivityRepository;

    @Autowired
    private ProgramLevelActivityMapper programLevelActivityMapper;

    @Autowired
    private ProgramLevelActivityService programLevelActivityService;

    @Autowired
    private ProgramLevelActivityQueryService programLevelActivityQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramLevelActivityMockMvc;

    private ProgramLevelActivity programLevelActivity;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramLevelActivityResource programLevelActivityResource = new ProgramLevelActivityResource(programLevelActivityService, programLevelActivityQueryService);
        this.restProgramLevelActivityMockMvc = MockMvcBuilders.standaloneSetup(programLevelActivityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevelActivity createEntity(EntityManager em) {



        ProgramLevelActivity programLevelActivity = new ProgramLevelActivity()
            .activityCode(DEFAULT_ACTIVITY_CODE)
            .activityId(DEFAULT_ACTIVITY_ID);

        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }

        programLevelActivity.setProgramLevel(programLevel);


        return programLevelActivity;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevelActivity createUpdatedEntity(EntityManager em) {
        ProgramLevelActivity programLevelActivity = new ProgramLevelActivity()
            .activityCode(UPDATED_ACTIVITY_CODE)
            .activityId(UPDATED_ACTIVITY_ID);

        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }

        programLevelActivity.setProgramLevel(programLevel);


        return programLevelActivity;
    }

    @BeforeEach
    public void initTest() {
        programLevelActivity = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramLevelActivity() throws Exception {
        int databaseSizeBeforeCreate = programLevelActivityRepository.findAll().size();

        // Create the ProgramLevelActivity
        ProgramLevelActivityDTO programLevelActivityDTO = programLevelActivityMapper.toDto(programLevelActivity);
        restProgramLevelActivityMockMvc.perform(post("/api/program-level-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelActivityDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramLevelActivity in the database
        List<ProgramLevelActivity> programLevelActivityList = programLevelActivityRepository.findAll();
        assertThat(programLevelActivityList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramLevelActivity testProgramLevelActivity = programLevelActivityList.get(programLevelActivityList.size() - 1);
        assertThat(testProgramLevelActivity.getActivityCode()).isEqualTo(DEFAULT_ACTIVITY_CODE);
        assertThat(testProgramLevelActivity.getActivityId()).isEqualTo(DEFAULT_ACTIVITY_ID);
    }

    @Test
    @Transactional
    public void createProgramLevelActivityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programLevelActivityRepository.findAll().size();

        // Create the ProgramLevelActivity with an existing ID
        programLevelActivity.setId(1L);
        ProgramLevelActivityDTO programLevelActivityDTO = programLevelActivityMapper.toDto(programLevelActivity);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramLevelActivityMockMvc.perform(post("/api/program-level-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelActivityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevelActivity in the database
        List<ProgramLevelActivity> programLevelActivityList = programLevelActivityRepository.findAll();
        assertThat(programLevelActivityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkActivityIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programLevelActivityRepository.findAll().size();
        // set the field null
        programLevelActivity.setActivityId(null);

        // Create the ProgramLevelActivity, which fails.
        ProgramLevelActivityDTO programLevelActivityDTO = programLevelActivityMapper.toDto(programLevelActivity);

        restProgramLevelActivityMockMvc.perform(post("/api/program-level-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelActivityDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramLevelActivity> programLevelActivityList = programLevelActivityRepository.findAll();
        assertThat(programLevelActivityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramLevelActivities() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        // Get all the programLevelActivityList
        restProgramLevelActivityMockMvc.perform(get("/api/program-level-activities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevelActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].activityCode").value(hasItem(DEFAULT_ACTIVITY_CODE.toString())))
            .andExpect(jsonPath("$.[*].activityId").value(hasItem(DEFAULT_ACTIVITY_ID.toString())));
    }

    @Test
    @Transactional
    public void getProgramLevelActivity() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        // Get the programLevelActivity
        restProgramLevelActivityMockMvc.perform(get("/api/program-level-activities/{id}", programLevelActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programLevelActivity.getId().intValue()))
            .andExpect(jsonPath("$.activityCode").value(DEFAULT_ACTIVITY_CODE.toString()))
            .andExpect(jsonPath("$.activityId").value(DEFAULT_ACTIVITY_ID.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramLevelActivitiesByActivityCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        // Get all the programLevelActivityList where activityCode equals to DEFAULT_ACTIVITY_CODE
        defaultProgramLevelActivityShouldBeFound("activityCode.equals=" + DEFAULT_ACTIVITY_CODE);

        // Get all the programLevelActivityList where activityCode equals to UPDATED_ACTIVITY_CODE
        defaultProgramLevelActivityShouldNotBeFound("activityCode.equals=" + UPDATED_ACTIVITY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelActivitiesByActivityCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        // Get all the programLevelActivityList where activityCode in DEFAULT_ACTIVITY_CODE or UPDATED_ACTIVITY_CODE
        defaultProgramLevelActivityShouldBeFound("activityCode.in=" + DEFAULT_ACTIVITY_CODE + "," + UPDATED_ACTIVITY_CODE);

        // Get all the programLevelActivityList where activityCode equals to UPDATED_ACTIVITY_CODE
        defaultProgramLevelActivityShouldNotBeFound("activityCode.in=" + UPDATED_ACTIVITY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelActivitiesByActivityCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        // Get all the programLevelActivityList where activityCode is not null
        defaultProgramLevelActivityShouldBeFound("activityCode.specified=true");

        // Get all the programLevelActivityList where activityCode is null
        defaultProgramLevelActivityShouldNotBeFound("activityCode.specified=false");
    }



    @Test
    @Transactional
    public void getAllProgramLevelActivitiesByActivityIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        // Get all the programLevelActivityList where activityId equals to DEFAULT_ACTIVITY_ID
        defaultProgramLevelActivityShouldBeFound("activityId.equals=" + DEFAULT_ACTIVITY_ID);

        // Get all the programLevelActivityList where activityId equals to UPDATED_ACTIVITY_ID
        defaultProgramLevelActivityShouldNotBeFound("activityId.equals=" + UPDATED_ACTIVITY_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelActivitiesByActivityIdIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        // Get all the programLevelActivityList where activityId in DEFAULT_ACTIVITY_ID or UPDATED_ACTIVITY_ID
        defaultProgramLevelActivityShouldBeFound("activityId.in=" + DEFAULT_ACTIVITY_ID + "," + UPDATED_ACTIVITY_ID);

        // Get all the programLevelActivityList where activityId equals to UPDATED_ACTIVITY_ID
        defaultProgramLevelActivityShouldNotBeFound("activityId.in=" + UPDATED_ACTIVITY_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelActivitiesByActivityIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        // Get all the programLevelActivityList where activityId is not null
        defaultProgramLevelActivityShouldBeFound("activityId.specified=true");

        // Get all the programLevelActivityList where activityId is null
        defaultProgramLevelActivityShouldNotBeFound("activityId.specified=false");
    }


    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramLevelActivityShouldBeFound(String filter) throws Exception {
        restProgramLevelActivityMockMvc.perform(get("/api/program-level-activities?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevelActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].activityCode").value(hasItem(DEFAULT_ACTIVITY_CODE)))
            .andExpect(jsonPath("$.[*].activityId").value(hasItem(DEFAULT_ACTIVITY_ID)));

        // Check, that the count call also returns 1
        restProgramLevelActivityMockMvc.perform(get("/api/program-level-activities/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramLevelActivityShouldNotBeFound(String filter) throws Exception {
        restProgramLevelActivityMockMvc.perform(get("/api/program-level-activities?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramLevelActivityMockMvc.perform(get("/api/program-level-activities/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramLevelActivity() throws Exception {
        // Get the programLevelActivity
        restProgramLevelActivityMockMvc.perform(get("/api/program-level-activities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramLevelActivity() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        int databaseSizeBeforeUpdate = programLevelActivityRepository.findAll().size();

        // Update the programLevelActivity
        ProgramLevelActivity updatedProgramLevelActivity = programLevelActivityRepository.findById(programLevelActivity.getId()).get();
        // Disconnect from session so that the updates on updatedProgramLevelActivity are not directly saved in db
        em.detach(updatedProgramLevelActivity);
        updatedProgramLevelActivity
            .activityCode(UPDATED_ACTIVITY_CODE)
            .activityId(UPDATED_ACTIVITY_ID);
        ProgramLevelActivityDTO programLevelActivityDTO = programLevelActivityMapper.toDto(updatedProgramLevelActivity);

        restProgramLevelActivityMockMvc.perform(put("/api/program-level-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelActivityDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramLevelActivity in the database
        List<ProgramLevelActivity> programLevelActivityList = programLevelActivityRepository.findAll();
        assertThat(programLevelActivityList).hasSize(databaseSizeBeforeUpdate);
        ProgramLevelActivity testProgramLevelActivity = programLevelActivityList.get(programLevelActivityList.size() - 1);
        assertThat(testProgramLevelActivity.getActivityCode()).isEqualTo(UPDATED_ACTIVITY_CODE);
        assertThat(testProgramLevelActivity.getActivityId()).isEqualTo(UPDATED_ACTIVITY_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramLevelActivity() throws Exception {
        int databaseSizeBeforeUpdate = programLevelActivityRepository.findAll().size();

        // Create the ProgramLevelActivity
        ProgramLevelActivityDTO programLevelActivityDTO = programLevelActivityMapper.toDto(programLevelActivity);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramLevelActivityMockMvc.perform(put("/api/program-level-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelActivityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevelActivity in the database
        List<ProgramLevelActivity> programLevelActivityList = programLevelActivityRepository.findAll();
        assertThat(programLevelActivityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramLevelActivity() throws Exception {
        // Initialize the database
        programLevelActivityRepository.saveAndFlush(programLevelActivity);

        int databaseSizeBeforeDelete = programLevelActivityRepository.findAll().size();

        // Delete the programLevelActivity
        restProgramLevelActivityMockMvc.perform(delete("/api/program-level-activities/{id}", programLevelActivity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramLevelActivity> programLevelActivityList = programLevelActivityRepository.findAll();
        assertThat(programLevelActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelActivity.class);
        ProgramLevelActivity programLevelActivity1 = new ProgramLevelActivity();
        programLevelActivity1.setId(1L);
        ProgramLevelActivity programLevelActivity2 = new ProgramLevelActivity();
        programLevelActivity2.setId(programLevelActivity1.getId());
        assertThat(programLevelActivity1).isEqualTo(programLevelActivity2);
        programLevelActivity2.setId(2L);
        assertThat(programLevelActivity1).isNotEqualTo(programLevelActivity2);
        programLevelActivity1.setId(null);
        assertThat(programLevelActivity1).isNotEqualTo(programLevelActivity2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelActivityDTO.class);
        ProgramLevelActivityDTO programLevelActivityDTO1 = new ProgramLevelActivityDTO();
        programLevelActivityDTO1.setId(1L);
        ProgramLevelActivityDTO programLevelActivityDTO2 = new ProgramLevelActivityDTO();
        assertThat(programLevelActivityDTO1).isNotEqualTo(programLevelActivityDTO2);
        programLevelActivityDTO2.setId(programLevelActivityDTO1.getId());
        assertThat(programLevelActivityDTO1).isEqualTo(programLevelActivityDTO2);
        programLevelActivityDTO2.setId(2L);
        assertThat(programLevelActivityDTO1).isNotEqualTo(programLevelActivityDTO2);
        programLevelActivityDTO1.setId(null);
        assertThat(programLevelActivityDTO1).isNotEqualTo(programLevelActivityDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programLevelActivityMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programLevelActivityMapper.fromId(null)).isNull();
    }

    @Test
    @Transactional
    public void updateLevelActivities() throws Exception {
        // Create the ProgramLevelActivity

        ProgramLevelActivityDTO programLevelActivityDTO1 = programLevelActivityMapper.toDto(programLevelActivity);
        ProgramLevelActivityDTO programLevelActivityDTO2 = programLevelActivityMapper.toDto(programLevelActivity);

        List<ProgramLevelActivityDTO> programLevelActivityDTOList = new ArrayList<>();
        programLevelActivityDTOList.add(programLevelActivityDTO1);
        programLevelActivityDTOList.add(programLevelActivityDTO2);


        restProgramLevelActivityMockMvc.perform(post("/api//program-level-activities/1/update-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelActivityDTOList)))
            .andExpect(status().isCreated());

    }

}
