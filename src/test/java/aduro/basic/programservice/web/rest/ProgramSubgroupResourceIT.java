package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramSubgroup;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ProgramSubgroupRepository;
import aduro.basic.programservice.service.ProgramSubgroupService;
import aduro.basic.programservice.service.dto.ProgramSubgroupDTO;
import aduro.basic.programservice.service.mapper.ProgramSubgroupMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramSubgroupCriteria;
import aduro.basic.programservice.service.ProgramSubgroupQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramSubgroupResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramSubgroupResourceIT {

    private static final String DEFAULT_SUBGROUP_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_NAME = "BBBBBBBBBB";

    @Autowired
    private ProgramSubgroupRepository programSubgroupRepository;

    @Autowired
    private ProgramSubgroupMapper programSubgroupMapper;

    @Autowired
    private ProgramSubgroupService programSubgroupService;

    @Autowired
    private ProgramSubgroupQueryService programSubgroupQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramSubgroupMockMvc;

    private ProgramSubgroup programSubgroup;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramSubgroupResource programSubgroupResource = new ProgramSubgroupResource(programSubgroupService, programSubgroupQueryService);
        this.restProgramSubgroupMockMvc = MockMvcBuilders.standaloneSetup(programSubgroupResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramSubgroup createEntity(EntityManager em) {
        ProgramSubgroup programSubgroup = new ProgramSubgroup()
            .subgroupId(DEFAULT_SUBGROUP_ID)
            .subgroupName(DEFAULT_SUBGROUP_NAME);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        programSubgroup.setProgram(program);
        return programSubgroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramSubgroup createUpdatedEntity(EntityManager em) {
        ProgramSubgroup programSubgroup = new ProgramSubgroup()
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createUpdatedEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        programSubgroup.setProgram(program);
        return programSubgroup;
    }

    @BeforeEach
    public void initTest() {
        programSubgroup = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramSubgroup() throws Exception {
        int databaseSizeBeforeCreate = programSubgroupRepository.findAll().size();

        // Create the ProgramSubgroup
        ProgramSubgroupDTO programSubgroupDTO = programSubgroupMapper.toDto(programSubgroup);
        restProgramSubgroupMockMvc.perform(post("/api/program-subgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubgroupDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramSubgroup in the database
        List<ProgramSubgroup> programSubgroupList = programSubgroupRepository.findAll();
        assertThat(programSubgroupList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramSubgroup testProgramSubgroup = programSubgroupList.get(programSubgroupList.size() - 1);
        assertThat(testProgramSubgroup.getSubgroupId()).isEqualTo(DEFAULT_SUBGROUP_ID);
        assertThat(testProgramSubgroup.getSubgroupName()).isEqualTo(DEFAULT_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void createProgramSubgroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programSubgroupRepository.findAll().size();

        // Create the ProgramSubgroup with an existing ID
        programSubgroup.setId(1L);
        ProgramSubgroupDTO programSubgroupDTO = programSubgroupMapper.toDto(programSubgroup);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramSubgroupMockMvc.perform(post("/api/program-subgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubgroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramSubgroup in the database
        List<ProgramSubgroup> programSubgroupList = programSubgroupRepository.findAll();
        assertThat(programSubgroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkSubgroupIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programSubgroupRepository.findAll().size();
        // set the field null
        programSubgroup.setSubgroupId(null);

        // Create the ProgramSubgroup, which fails.
        ProgramSubgroupDTO programSubgroupDTO = programSubgroupMapper.toDto(programSubgroup);

        restProgramSubgroupMockMvc.perform(post("/api/program-subgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubgroupDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramSubgroup> programSubgroupList = programSubgroupRepository.findAll();
        assertThat(programSubgroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubgroupNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = programSubgroupRepository.findAll().size();
        // set the field null
        programSubgroup.setSubgroupName(null);

        // Create the ProgramSubgroup, which fails.
        ProgramSubgroupDTO programSubgroupDTO = programSubgroupMapper.toDto(programSubgroup);

        restProgramSubgroupMockMvc.perform(post("/api/program-subgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubgroupDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramSubgroup> programSubgroupList = programSubgroupRepository.findAll();
        assertThat(programSubgroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramSubgroups() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        // Get all the programSubgroupList
        restProgramSubgroupMockMvc.perform(get("/api/program-subgroups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programSubgroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID.toString())))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramSubgroup() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        // Get the programSubgroup
        restProgramSubgroupMockMvc.perform(get("/api/program-subgroups/{id}", programSubgroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programSubgroup.getId().intValue()))
            .andExpect(jsonPath("$.subgroupId").value(DEFAULT_SUBGROUP_ID.toString()))
            .andExpect(jsonPath("$.subgroupName").value(DEFAULT_SUBGROUP_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramSubgroupsBySubgroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        // Get all the programSubgroupList where subgroupId equals to DEFAULT_SUBGROUP_ID
        defaultProgramSubgroupShouldBeFound("subgroupId.equals=" + DEFAULT_SUBGROUP_ID);

        // Get all the programSubgroupList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramSubgroupShouldNotBeFound("subgroupId.equals=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramSubgroupsBySubgroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        // Get all the programSubgroupList where subgroupId in DEFAULT_SUBGROUP_ID or UPDATED_SUBGROUP_ID
        defaultProgramSubgroupShouldBeFound("subgroupId.in=" + DEFAULT_SUBGROUP_ID + "," + UPDATED_SUBGROUP_ID);

        // Get all the programSubgroupList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramSubgroupShouldNotBeFound("subgroupId.in=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramSubgroupsBySubgroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        // Get all the programSubgroupList where subgroupId is not null
        defaultProgramSubgroupShouldBeFound("subgroupId.specified=true");

        // Get all the programSubgroupList where subgroupId is null
        defaultProgramSubgroupShouldNotBeFound("subgroupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubgroupsBySubgroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        // Get all the programSubgroupList where subgroupName equals to DEFAULT_SUBGROUP_NAME
        defaultProgramSubgroupShouldBeFound("subgroupName.equals=" + DEFAULT_SUBGROUP_NAME);

        // Get all the programSubgroupList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramSubgroupShouldNotBeFound("subgroupName.equals=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramSubgroupsBySubgroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        // Get all the programSubgroupList where subgroupName in DEFAULT_SUBGROUP_NAME or UPDATED_SUBGROUP_NAME
        defaultProgramSubgroupShouldBeFound("subgroupName.in=" + DEFAULT_SUBGROUP_NAME + "," + UPDATED_SUBGROUP_NAME);

        // Get all the programSubgroupList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramSubgroupShouldNotBeFound("subgroupName.in=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramSubgroupsBySubgroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        // Get all the programSubgroupList where subgroupName is not null
        defaultProgramSubgroupShouldBeFound("subgroupName.specified=true");

        // Get all the programSubgroupList where subgroupName is null
        defaultProgramSubgroupShouldNotBeFound("subgroupName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramSubgroupsByProgramIsEqualToSomething() throws Exception {
        // Get already existing entity
        Program program = programSubgroup.getProgram();
        programSubgroupRepository.saveAndFlush(programSubgroup);
        Long programId = program.getId();

        // Get all the programSubgroupList where program equals to programId
        defaultProgramSubgroupShouldBeFound("programId.equals=" + programId);

        // Get all the programSubgroupList where program equals to programId + 1
        defaultProgramSubgroupShouldNotBeFound("programId.equals=" + (programId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramSubgroupShouldBeFound(String filter) throws Exception {
        restProgramSubgroupMockMvc.perform(get("/api/program-subgroups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programSubgroup.getId().intValue())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID)))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME)));

        // Check, that the count call also returns 1
        restProgramSubgroupMockMvc.perform(get("/api/program-subgroups/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramSubgroupShouldNotBeFound(String filter) throws Exception {
        restProgramSubgroupMockMvc.perform(get("/api/program-subgroups?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramSubgroupMockMvc.perform(get("/api/program-subgroups/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramSubgroup() throws Exception {
        // Get the programSubgroup
        restProgramSubgroupMockMvc.perform(get("/api/program-subgroups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramSubgroup() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        int databaseSizeBeforeUpdate = programSubgroupRepository.findAll().size();

        // Update the programSubgroup
        ProgramSubgroup updatedProgramSubgroup = programSubgroupRepository.findById(programSubgroup.getId()).get();
        // Disconnect from session so that the updates on updatedProgramSubgroup are not directly saved in db
        em.detach(updatedProgramSubgroup);
        updatedProgramSubgroup
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME);
        ProgramSubgroupDTO programSubgroupDTO = programSubgroupMapper.toDto(updatedProgramSubgroup);

        restProgramSubgroupMockMvc.perform(put("/api/program-subgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubgroupDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramSubgroup in the database
        List<ProgramSubgroup> programSubgroupList = programSubgroupRepository.findAll();
        assertThat(programSubgroupList).hasSize(databaseSizeBeforeUpdate);
        ProgramSubgroup testProgramSubgroup = programSubgroupList.get(programSubgroupList.size() - 1);
        assertThat(testProgramSubgroup.getSubgroupId()).isEqualTo(UPDATED_SUBGROUP_ID);
        assertThat(testProgramSubgroup.getSubgroupName()).isEqualTo(UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramSubgroup() throws Exception {
        int databaseSizeBeforeUpdate = programSubgroupRepository.findAll().size();

        // Create the ProgramSubgroup
        ProgramSubgroupDTO programSubgroupDTO = programSubgroupMapper.toDto(programSubgroup);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramSubgroupMockMvc.perform(put("/api/program-subgroups")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programSubgroupDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramSubgroup in the database
        List<ProgramSubgroup> programSubgroupList = programSubgroupRepository.findAll();
        assertThat(programSubgroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramSubgroup() throws Exception {
        // Initialize the database
        programSubgroupRepository.saveAndFlush(programSubgroup);

        int databaseSizeBeforeDelete = programSubgroupRepository.findAll().size();

        // Delete the programSubgroup
        restProgramSubgroupMockMvc.perform(delete("/api/program-subgroups/{id}", programSubgroup.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramSubgroup> programSubgroupList = programSubgroupRepository.findAll();
        assertThat(programSubgroupList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramSubgroup.class);
        ProgramSubgroup programSubgroup1 = new ProgramSubgroup();
        programSubgroup1.setId(1L);
        ProgramSubgroup programSubgroup2 = new ProgramSubgroup();
        programSubgroup2.setId(programSubgroup1.getId());
        assertThat(programSubgroup1).isEqualTo(programSubgroup2);
        programSubgroup2.setId(2L);
        assertThat(programSubgroup1).isNotEqualTo(programSubgroup2);
        programSubgroup1.setId(null);
        assertThat(programSubgroup1).isNotEqualTo(programSubgroup2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramSubgroupDTO.class);
        ProgramSubgroupDTO programSubgroupDTO1 = new ProgramSubgroupDTO();
        programSubgroupDTO1.setId(1L);
        ProgramSubgroupDTO programSubgroupDTO2 = new ProgramSubgroupDTO();
        assertThat(programSubgroupDTO1).isNotEqualTo(programSubgroupDTO2);
        programSubgroupDTO2.setId(programSubgroupDTO1.getId());
        assertThat(programSubgroupDTO1).isEqualTo(programSubgroupDTO2);
        programSubgroupDTO2.setId(2L);
        assertThat(programSubgroupDTO1).isNotEqualTo(programSubgroupDTO2);
        programSubgroupDTO1.setId(null);
        assertThat(programSubgroupDTO1).isNotEqualTo(programSubgroupDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programSubgroupMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programSubgroupMapper.fromId(null)).isNull();
    }
}
