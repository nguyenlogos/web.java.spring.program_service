package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramBiometricData;
import aduro.basic.programservice.repository.ProgramBiometricDataRepository;
import aduro.basic.programservice.service.ProgramBiometricDataService;
import aduro.basic.programservice.service.dto.ProgramBiometricDataDTO;
import aduro.basic.programservice.service.mapper.ProgramBiometricDataMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramBiometricDataCriteria;
import aduro.basic.programservice.service.ProgramBiometricDataQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramBiometricDataResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramBiometricDataResourceIT {

    private static final String DEFAULT_BIOMETRIC_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BIOMETRIC_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_BIOMETRIC_NAME = "AAAAAAAAAA";
    private static final String UPDATED_BIOMETRIC_NAME = "BBBBBBBBBB";

    private static final Float DEFAULT_MALE_MIN = 1F;
    private static final Float UPDATED_MALE_MIN = 2F;

    private static final Float DEFAULT_MALE_MAX = 1F;
    private static final Float UPDATED_MALE_MAX = 2F;

    private static final Float DEFAULT_FEMALE_MIN = 1F;
    private static final Float UPDATED_FEMALE_MIN = 2F;

    private static final Float DEFAULT_FEMALE_MAX = 1F;
    private static final Float UPDATED_FEMALE_MAX = 2F;

    private static final Float DEFAULT_UNIDENTIFIED_MIN = 1F;
    private static final Float UPDATED_UNIDENTIFIED_MIN = 2F;

    private static final Float DEFAULT_UNIDENTIFIED_MAX = 1F;
    private static final Float UPDATED_UNIDENTIFIED_MAX = 2F;

    @Autowired
    private ProgramBiometricDataRepository programBiometricDataRepository;

    @Autowired
    private ProgramBiometricDataMapper programBiometricDataMapper;

    @Autowired
    private ProgramBiometricDataService programBiometricDataService;

    @Autowired
    private ProgramBiometricDataQueryService programBiometricDataQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramBiometricDataMockMvc;

    private ProgramBiometricData programBiometricData;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramBiometricDataResource programBiometricDataResource = new ProgramBiometricDataResource(programBiometricDataService, programBiometricDataQueryService);
        this.restProgramBiometricDataMockMvc = MockMvcBuilders.standaloneSetup(programBiometricDataResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramBiometricData createEntity(EntityManager em) {
        ProgramBiometricData programBiometricData = new ProgramBiometricData()
            .biometricCode(DEFAULT_BIOMETRIC_CODE)
            .biometricName(DEFAULT_BIOMETRIC_NAME)
            .maleMin(DEFAULT_MALE_MIN)
            .maleMax(DEFAULT_MALE_MAX)
            .femaleMin(DEFAULT_FEMALE_MIN)
            .femaleMax(DEFAULT_FEMALE_MAX)
            .unidentifiedMin(DEFAULT_UNIDENTIFIED_MIN)
            .unidentifiedMax(DEFAULT_UNIDENTIFIED_MAX);
        return programBiometricData;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramBiometricData createUpdatedEntity(EntityManager em) {
        ProgramBiometricData programBiometricData = new ProgramBiometricData()
            .biometricCode(UPDATED_BIOMETRIC_CODE)
            .biometricName(UPDATED_BIOMETRIC_NAME)
            .maleMin(UPDATED_MALE_MIN)
            .maleMax(UPDATED_MALE_MAX)
            .femaleMin(UPDATED_FEMALE_MIN)
            .femaleMax(UPDATED_FEMALE_MAX)
            .unidentifiedMin(UPDATED_UNIDENTIFIED_MIN)
            .unidentifiedMax(UPDATED_UNIDENTIFIED_MAX);
        return programBiometricData;
    }

    @BeforeEach
    public void initTest() {
        programBiometricData = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramBiometricData() throws Exception {
        int databaseSizeBeforeCreate = programBiometricDataRepository.findAll().size();

        // Create the ProgramBiometricData
        ProgramBiometricDataDTO programBiometricDataDTO = programBiometricDataMapper.toDto(programBiometricData);
        restProgramBiometricDataMockMvc.perform(post("/api/program-biometric-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programBiometricDataDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramBiometricData in the database
        List<ProgramBiometricData> programBiometricDataList = programBiometricDataRepository.findAll();
        assertThat(programBiometricDataList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramBiometricData testProgramBiometricData = programBiometricDataList.get(programBiometricDataList.size() - 1);
        assertThat(testProgramBiometricData.getBiometricCode()).isEqualTo(DEFAULT_BIOMETRIC_CODE);
        assertThat(testProgramBiometricData.getBiometricName()).isEqualTo(DEFAULT_BIOMETRIC_NAME);
        assertThat(testProgramBiometricData.getMaleMin()).isEqualTo(DEFAULT_MALE_MIN);
        assertThat(testProgramBiometricData.getMaleMax()).isEqualTo(DEFAULT_MALE_MAX);
        assertThat(testProgramBiometricData.getFemaleMin()).isEqualTo(DEFAULT_FEMALE_MIN);
        assertThat(testProgramBiometricData.getFemaleMax()).isEqualTo(DEFAULT_FEMALE_MAX);
        assertThat(testProgramBiometricData.getUnidentifiedMin()).isEqualTo(DEFAULT_UNIDENTIFIED_MIN);
        assertThat(testProgramBiometricData.getUnidentifiedMax()).isEqualTo(DEFAULT_UNIDENTIFIED_MAX);
    }

    @Test
    @Transactional
    public void createProgramBiometricDataWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programBiometricDataRepository.findAll().size();

        // Create the ProgramBiometricData with an existing ID
        programBiometricData.setId(1L);
        ProgramBiometricDataDTO programBiometricDataDTO = programBiometricDataMapper.toDto(programBiometricData);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramBiometricDataMockMvc.perform(post("/api/program-biometric-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programBiometricDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramBiometricData in the database
        List<ProgramBiometricData> programBiometricDataList = programBiometricDataRepository.findAll();
        assertThat(programBiometricDataList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProgramBiometricData() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList
        restProgramBiometricDataMockMvc.perform(get("/api/program-biometric-data?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programBiometricData.getId().intValue())))
            .andExpect(jsonPath("$.[*].biometricCode").value(hasItem(DEFAULT_BIOMETRIC_CODE.toString())))
            .andExpect(jsonPath("$.[*].biometricName").value(hasItem(DEFAULT_BIOMETRIC_NAME.toString())))
            .andExpect(jsonPath("$.[*].maleMin").value(hasItem(DEFAULT_MALE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].maleMax").value(hasItem(DEFAULT_MALE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].femaleMin").value(hasItem(DEFAULT_FEMALE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].femaleMax").value(hasItem(DEFAULT_FEMALE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].unidentifiedMin").value(hasItem(DEFAULT_UNIDENTIFIED_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].unidentifiedMax").value(hasItem(DEFAULT_UNIDENTIFIED_MAX.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getProgramBiometricData() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get the programBiometricData
        restProgramBiometricDataMockMvc.perform(get("/api/program-biometric-data/{id}", programBiometricData.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programBiometricData.getId().intValue()))
            .andExpect(jsonPath("$.biometricCode").value(DEFAULT_BIOMETRIC_CODE.toString()))
            .andExpect(jsonPath("$.biometricName").value(DEFAULT_BIOMETRIC_NAME.toString()))
            .andExpect(jsonPath("$.maleMin").value(DEFAULT_MALE_MIN.doubleValue()))
            .andExpect(jsonPath("$.maleMax").value(DEFAULT_MALE_MAX.doubleValue()))
            .andExpect(jsonPath("$.femaleMin").value(DEFAULT_FEMALE_MIN.doubleValue()))
            .andExpect(jsonPath("$.femaleMax").value(DEFAULT_FEMALE_MAX.doubleValue()))
            .andExpect(jsonPath("$.unidentifiedMin").value(DEFAULT_UNIDENTIFIED_MIN.doubleValue()))
            .andExpect(jsonPath("$.unidentifiedMax").value(DEFAULT_UNIDENTIFIED_MAX.doubleValue()));
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByBiometricCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where biometricCode equals to DEFAULT_BIOMETRIC_CODE
        defaultProgramBiometricDataShouldBeFound("biometricCode.equals=" + DEFAULT_BIOMETRIC_CODE);

        // Get all the programBiometricDataList where biometricCode equals to UPDATED_BIOMETRIC_CODE
        defaultProgramBiometricDataShouldNotBeFound("biometricCode.equals=" + UPDATED_BIOMETRIC_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByBiometricCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where biometricCode in DEFAULT_BIOMETRIC_CODE or UPDATED_BIOMETRIC_CODE
        defaultProgramBiometricDataShouldBeFound("biometricCode.in=" + DEFAULT_BIOMETRIC_CODE + "," + UPDATED_BIOMETRIC_CODE);

        // Get all the programBiometricDataList where biometricCode equals to UPDATED_BIOMETRIC_CODE
        defaultProgramBiometricDataShouldNotBeFound("biometricCode.in=" + UPDATED_BIOMETRIC_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByBiometricCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where biometricCode is not null
        defaultProgramBiometricDataShouldBeFound("biometricCode.specified=true");

        // Get all the programBiometricDataList where biometricCode is null
        defaultProgramBiometricDataShouldNotBeFound("biometricCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByBiometricNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where biometricName equals to DEFAULT_BIOMETRIC_NAME
        defaultProgramBiometricDataShouldBeFound("biometricName.equals=" + DEFAULT_BIOMETRIC_NAME);

        // Get all the programBiometricDataList where biometricName equals to UPDATED_BIOMETRIC_NAME
        defaultProgramBiometricDataShouldNotBeFound("biometricName.equals=" + UPDATED_BIOMETRIC_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByBiometricNameIsInShouldWork() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where biometricName in DEFAULT_BIOMETRIC_NAME or UPDATED_BIOMETRIC_NAME
        defaultProgramBiometricDataShouldBeFound("biometricName.in=" + DEFAULT_BIOMETRIC_NAME + "," + UPDATED_BIOMETRIC_NAME);

        // Get all the programBiometricDataList where biometricName equals to UPDATED_BIOMETRIC_NAME
        defaultProgramBiometricDataShouldNotBeFound("biometricName.in=" + UPDATED_BIOMETRIC_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByBiometricNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where biometricName is not null
        defaultProgramBiometricDataShouldBeFound("biometricName.specified=true");

        // Get all the programBiometricDataList where biometricName is null
        defaultProgramBiometricDataShouldNotBeFound("biometricName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByMaleMinIsEqualToSomething() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where maleMin equals to DEFAULT_MALE_MIN
        defaultProgramBiometricDataShouldBeFound("maleMin.equals=" + DEFAULT_MALE_MIN);

        // Get all the programBiometricDataList where maleMin equals to UPDATED_MALE_MIN
        defaultProgramBiometricDataShouldNotBeFound("maleMin.equals=" + UPDATED_MALE_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByMaleMinIsInShouldWork() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where maleMin in DEFAULT_MALE_MIN or UPDATED_MALE_MIN
        defaultProgramBiometricDataShouldBeFound("maleMin.in=" + DEFAULT_MALE_MIN + "," + UPDATED_MALE_MIN);

        // Get all the programBiometricDataList where maleMin equals to UPDATED_MALE_MIN
        defaultProgramBiometricDataShouldNotBeFound("maleMin.in=" + UPDATED_MALE_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByMaleMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where maleMin is not null
        defaultProgramBiometricDataShouldBeFound("maleMin.specified=true");

        // Get all the programBiometricDataList where maleMin is null
        defaultProgramBiometricDataShouldNotBeFound("maleMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByMaleMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where maleMax equals to DEFAULT_MALE_MAX
        defaultProgramBiometricDataShouldBeFound("maleMax.equals=" + DEFAULT_MALE_MAX);

        // Get all the programBiometricDataList where maleMax equals to UPDATED_MALE_MAX
        defaultProgramBiometricDataShouldNotBeFound("maleMax.equals=" + UPDATED_MALE_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByMaleMaxIsInShouldWork() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where maleMax in DEFAULT_MALE_MAX or UPDATED_MALE_MAX
        defaultProgramBiometricDataShouldBeFound("maleMax.in=" + DEFAULT_MALE_MAX + "," + UPDATED_MALE_MAX);

        // Get all the programBiometricDataList where maleMax equals to UPDATED_MALE_MAX
        defaultProgramBiometricDataShouldNotBeFound("maleMax.in=" + UPDATED_MALE_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByMaleMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where maleMax is not null
        defaultProgramBiometricDataShouldBeFound("maleMax.specified=true");

        // Get all the programBiometricDataList where maleMax is null
        defaultProgramBiometricDataShouldNotBeFound("maleMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByFemaleMinIsEqualToSomething() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where femaleMin equals to DEFAULT_FEMALE_MIN
        defaultProgramBiometricDataShouldBeFound("femaleMin.equals=" + DEFAULT_FEMALE_MIN);

        // Get all the programBiometricDataList where femaleMin equals to UPDATED_FEMALE_MIN
        defaultProgramBiometricDataShouldNotBeFound("femaleMin.equals=" + UPDATED_FEMALE_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByFemaleMinIsInShouldWork() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where femaleMin in DEFAULT_FEMALE_MIN or UPDATED_FEMALE_MIN
        defaultProgramBiometricDataShouldBeFound("femaleMin.in=" + DEFAULT_FEMALE_MIN + "," + UPDATED_FEMALE_MIN);

        // Get all the programBiometricDataList where femaleMin equals to UPDATED_FEMALE_MIN
        defaultProgramBiometricDataShouldNotBeFound("femaleMin.in=" + UPDATED_FEMALE_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByFemaleMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where femaleMin is not null
        defaultProgramBiometricDataShouldBeFound("femaleMin.specified=true");

        // Get all the programBiometricDataList where femaleMin is null
        defaultProgramBiometricDataShouldNotBeFound("femaleMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByFemaleMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where femaleMax equals to DEFAULT_FEMALE_MAX
        defaultProgramBiometricDataShouldBeFound("femaleMax.equals=" + DEFAULT_FEMALE_MAX);

        // Get all the programBiometricDataList where femaleMax equals to UPDATED_FEMALE_MAX
        defaultProgramBiometricDataShouldNotBeFound("femaleMax.equals=" + UPDATED_FEMALE_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByFemaleMaxIsInShouldWork() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where femaleMax in DEFAULT_FEMALE_MAX or UPDATED_FEMALE_MAX
        defaultProgramBiometricDataShouldBeFound("femaleMax.in=" + DEFAULT_FEMALE_MAX + "," + UPDATED_FEMALE_MAX);

        // Get all the programBiometricDataList where femaleMax equals to UPDATED_FEMALE_MAX
        defaultProgramBiometricDataShouldNotBeFound("femaleMax.in=" + UPDATED_FEMALE_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByFemaleMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where femaleMax is not null
        defaultProgramBiometricDataShouldBeFound("femaleMax.specified=true");

        // Get all the programBiometricDataList where femaleMax is null
        defaultProgramBiometricDataShouldNotBeFound("femaleMax.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByUnidentifiedMinIsEqualToSomething() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where unidentifiedMin equals to DEFAULT_UNIDENTIFIED_MIN
        defaultProgramBiometricDataShouldBeFound("unidentifiedMin.equals=" + DEFAULT_UNIDENTIFIED_MIN);

        // Get all the programBiometricDataList where unidentifiedMin equals to UPDATED_UNIDENTIFIED_MIN
        defaultProgramBiometricDataShouldNotBeFound("unidentifiedMin.equals=" + UPDATED_UNIDENTIFIED_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByUnidentifiedMinIsInShouldWork() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where unidentifiedMin in DEFAULT_UNIDENTIFIED_MIN or UPDATED_UNIDENTIFIED_MIN
        defaultProgramBiometricDataShouldBeFound("unidentifiedMin.in=" + DEFAULT_UNIDENTIFIED_MIN + "," + UPDATED_UNIDENTIFIED_MIN);

        // Get all the programBiometricDataList where unidentifiedMin equals to UPDATED_UNIDENTIFIED_MIN
        defaultProgramBiometricDataShouldNotBeFound("unidentifiedMin.in=" + UPDATED_UNIDENTIFIED_MIN);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByUnidentifiedMinIsNullOrNotNull() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where unidentifiedMin is not null
        defaultProgramBiometricDataShouldBeFound("unidentifiedMin.specified=true");

        // Get all the programBiometricDataList where unidentifiedMin is null
        defaultProgramBiometricDataShouldNotBeFound("unidentifiedMin.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByUnidentifiedMaxIsEqualToSomething() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where unidentifiedMax equals to DEFAULT_UNIDENTIFIED_MAX
        defaultProgramBiometricDataShouldBeFound("unidentifiedMax.equals=" + DEFAULT_UNIDENTIFIED_MAX);

        // Get all the programBiometricDataList where unidentifiedMax equals to UPDATED_UNIDENTIFIED_MAX
        defaultProgramBiometricDataShouldNotBeFound("unidentifiedMax.equals=" + UPDATED_UNIDENTIFIED_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByUnidentifiedMaxIsInShouldWork() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where unidentifiedMax in DEFAULT_UNIDENTIFIED_MAX or UPDATED_UNIDENTIFIED_MAX
        defaultProgramBiometricDataShouldBeFound("unidentifiedMax.in=" + DEFAULT_UNIDENTIFIED_MAX + "," + UPDATED_UNIDENTIFIED_MAX);

        // Get all the programBiometricDataList where unidentifiedMax equals to UPDATED_UNIDENTIFIED_MAX
        defaultProgramBiometricDataShouldNotBeFound("unidentifiedMax.in=" + UPDATED_UNIDENTIFIED_MAX);
    }

    @Test
    @Transactional
    public void getAllProgramBiometricDataByUnidentifiedMaxIsNullOrNotNull() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        // Get all the programBiometricDataList where unidentifiedMax is not null
        defaultProgramBiometricDataShouldBeFound("unidentifiedMax.specified=true");

        // Get all the programBiometricDataList where unidentifiedMax is null
        defaultProgramBiometricDataShouldNotBeFound("unidentifiedMax.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramBiometricDataShouldBeFound(String filter) throws Exception {
        restProgramBiometricDataMockMvc.perform(get("/api/program-biometric-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programBiometricData.getId().intValue())))
            .andExpect(jsonPath("$.[*].biometricCode").value(hasItem(DEFAULT_BIOMETRIC_CODE)))
            .andExpect(jsonPath("$.[*].biometricName").value(hasItem(DEFAULT_BIOMETRIC_NAME)))
            .andExpect(jsonPath("$.[*].maleMin").value(hasItem(DEFAULT_MALE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].maleMax").value(hasItem(DEFAULT_MALE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].femaleMin").value(hasItem(DEFAULT_FEMALE_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].femaleMax").value(hasItem(DEFAULT_FEMALE_MAX.doubleValue())))
            .andExpect(jsonPath("$.[*].unidentifiedMin").value(hasItem(DEFAULT_UNIDENTIFIED_MIN.doubleValue())))
            .andExpect(jsonPath("$.[*].unidentifiedMax").value(hasItem(DEFAULT_UNIDENTIFIED_MAX.doubleValue())));

        // Check, that the count call also returns 1
        restProgramBiometricDataMockMvc.perform(get("/api/program-biometric-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramBiometricDataShouldNotBeFound(String filter) throws Exception {
        restProgramBiometricDataMockMvc.perform(get("/api/program-biometric-data?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramBiometricDataMockMvc.perform(get("/api/program-biometric-data/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramBiometricData() throws Exception {
        // Get the programBiometricData
        restProgramBiometricDataMockMvc.perform(get("/api/program-biometric-data/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramBiometricData() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        int databaseSizeBeforeUpdate = programBiometricDataRepository.findAll().size();

        // Update the programBiometricData
        ProgramBiometricData updatedProgramBiometricData = programBiometricDataRepository.findById(programBiometricData.getId()).get();
        // Disconnect from session so that the updates on updatedProgramBiometricData are not directly saved in db
        em.detach(updatedProgramBiometricData);
        updatedProgramBiometricData
            .biometricCode(UPDATED_BIOMETRIC_CODE)
            .biometricName(UPDATED_BIOMETRIC_NAME)
            .maleMin(UPDATED_MALE_MIN)
            .maleMax(UPDATED_MALE_MAX)
            .femaleMin(UPDATED_FEMALE_MIN)
            .femaleMax(UPDATED_FEMALE_MAX)
            .unidentifiedMin(UPDATED_UNIDENTIFIED_MIN)
            .unidentifiedMax(UPDATED_UNIDENTIFIED_MAX);
        ProgramBiometricDataDTO programBiometricDataDTO = programBiometricDataMapper.toDto(updatedProgramBiometricData);

        restProgramBiometricDataMockMvc.perform(put("/api/program-biometric-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programBiometricDataDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramBiometricData in the database
        List<ProgramBiometricData> programBiometricDataList = programBiometricDataRepository.findAll();
        assertThat(programBiometricDataList).hasSize(databaseSizeBeforeUpdate);
        ProgramBiometricData testProgramBiometricData = programBiometricDataList.get(programBiometricDataList.size() - 1);
        assertThat(testProgramBiometricData.getBiometricCode()).isEqualTo(UPDATED_BIOMETRIC_CODE);
        assertThat(testProgramBiometricData.getBiometricName()).isEqualTo(UPDATED_BIOMETRIC_NAME);
        assertThat(testProgramBiometricData.getMaleMin()).isEqualTo(UPDATED_MALE_MIN);
        assertThat(testProgramBiometricData.getMaleMax()).isEqualTo(UPDATED_MALE_MAX);
        assertThat(testProgramBiometricData.getFemaleMin()).isEqualTo(UPDATED_FEMALE_MIN);
        assertThat(testProgramBiometricData.getFemaleMax()).isEqualTo(UPDATED_FEMALE_MAX);
        assertThat(testProgramBiometricData.getUnidentifiedMin()).isEqualTo(UPDATED_UNIDENTIFIED_MIN);
        assertThat(testProgramBiometricData.getUnidentifiedMax()).isEqualTo(UPDATED_UNIDENTIFIED_MAX);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramBiometricData() throws Exception {
        int databaseSizeBeforeUpdate = programBiometricDataRepository.findAll().size();

        // Create the ProgramBiometricData
        ProgramBiometricDataDTO programBiometricDataDTO = programBiometricDataMapper.toDto(programBiometricData);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramBiometricDataMockMvc.perform(put("/api/program-biometric-data")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programBiometricDataDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramBiometricData in the database
        List<ProgramBiometricData> programBiometricDataList = programBiometricDataRepository.findAll();
        assertThat(programBiometricDataList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramBiometricData() throws Exception {
        // Initialize the database
        programBiometricDataRepository.saveAndFlush(programBiometricData);

        int databaseSizeBeforeDelete = programBiometricDataRepository.findAll().size();

        // Delete the programBiometricData
        restProgramBiometricDataMockMvc.perform(delete("/api/program-biometric-data/{id}", programBiometricData.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramBiometricData> programBiometricDataList = programBiometricDataRepository.findAll();
        assertThat(programBiometricDataList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramBiometricData.class);
        ProgramBiometricData programBiometricData1 = new ProgramBiometricData();
        programBiometricData1.setId(1L);
        ProgramBiometricData programBiometricData2 = new ProgramBiometricData();
        programBiometricData2.setId(programBiometricData1.getId());
        assertThat(programBiometricData1).isEqualTo(programBiometricData2);
        programBiometricData2.setId(2L);
        assertThat(programBiometricData1).isNotEqualTo(programBiometricData2);
        programBiometricData1.setId(null);
        assertThat(programBiometricData1).isNotEqualTo(programBiometricData2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramBiometricDataDTO.class);
        ProgramBiometricDataDTO programBiometricDataDTO1 = new ProgramBiometricDataDTO();
        programBiometricDataDTO1.setId(1L);
        ProgramBiometricDataDTO programBiometricDataDTO2 = new ProgramBiometricDataDTO();
        assertThat(programBiometricDataDTO1).isNotEqualTo(programBiometricDataDTO2);
        programBiometricDataDTO2.setId(programBiometricDataDTO1.getId());
        assertThat(programBiometricDataDTO1).isEqualTo(programBiometricDataDTO2);
        programBiometricDataDTO2.setId(2L);
        assertThat(programBiometricDataDTO1).isNotEqualTo(programBiometricDataDTO2);
        programBiometricDataDTO1.setId(null);
        assertThat(programBiometricDataDTO1).isNotEqualTo(programBiometricDataDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programBiometricDataMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programBiometricDataMapper.fromId(null)).isNull();
    }
}
