package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.Category;
import aduro.basic.programservice.domain.ProgramCategoryPoint;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ProgramCategoryPointRepository;
import aduro.basic.programservice.service.ProgramCategoryPointService;
import aduro.basic.programservice.service.dto.ProgramCategoryPointDTO;
import aduro.basic.programservice.service.mapper.ProgramCategoryPointMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramCategoryPointCriteria;
import aduro.basic.programservice.service.ProgramCategoryPointQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramCategoryPointResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramCategoryPointResourceIT {

    private static final String DEFAULT_CATEGORY_CODE = "ACTIVITIES";
    private static final String UPDATED_CATEGORY_CODE = "COACH";

    private static final String DEFAULT_CATEGORY_NAME = "ACTIVITIES";
    private static final String UPDATED_CATEGORY_NAME = "COACH";

    private static final BigDecimal DEFAULT_PERCENT_POINT = new BigDecimal(0.1);
    private static final BigDecimal UPDATED_PERCENT_POINT = new BigDecimal(0.2);

    private static final BigDecimal DEFAULT_VALUE_POINT = new BigDecimal(1);
    private static final BigDecimal UPDATED_VALUE_POINT = new BigDecimal(2);

    private static final Boolean DEFAULT_LOCKED = false;
    private static final Boolean UPDATED_LOCKED = true;

    @Autowired
    private ProgramCategoryPointRepository programCategoryPointRepository;

    @Autowired
    private ProgramCategoryPointMapper programCategoryPointMapper;

    @Autowired
    private ProgramCategoryPointService programCategoryPointService;

    @Autowired
    private ProgramCategoryPointQueryService programCategoryPointQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramCategoryPointMockMvc;

    private ProgramCategoryPoint programCategoryPoint;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramCategoryPointResource programCategoryPointResource = new ProgramCategoryPointResource(programCategoryPointService, programCategoryPointQueryService);
        this.restProgramCategoryPointMockMvc = MockMvcBuilders.standaloneSetup(programCategoryPointResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCategoryPoint createEntity(EntityManager em) {

        //Add Category and Sub-category
        if (TestUtil.findAll(em, Category.class).isEmpty()) {

            Category category =  CategoryResourceIT.createEntity(em);
            em.persist(category);
            em.flush();
        }


        ProgramCategoryPoint programCategoryPoint = new ProgramCategoryPoint()
            .categoryCode(DEFAULT_CATEGORY_CODE)
            .categoryName(DEFAULT_CATEGORY_NAME)
            .percentPoint(DEFAULT_PERCENT_POINT)
            .valuePoint(DEFAULT_VALUE_POINT)
            .locked(DEFAULT_LOCKED);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        programCategoryPoint.setProgram(program);
        return programCategoryPoint;



    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCategoryPoint createUpdatedEntity(EntityManager em) {
        ProgramCategoryPoint programCategoryPoint = new ProgramCategoryPoint()
            .categoryCode(UPDATED_CATEGORY_CODE)
            .categoryName(UPDATED_CATEGORY_NAME)
            .percentPoint(UPDATED_PERCENT_POINT)
            .valuePoint(UPDATED_VALUE_POINT)
            .locked(UPDATED_LOCKED);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createUpdatedEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        programCategoryPoint.setProgram(program);
        return programCategoryPoint;
    }

    @BeforeEach
    public void initTest() {

        programCategoryPoint = createEntity(em);


    }

    @Test
    @Transactional
    public void createProgramCategoryPoint() throws Exception {
        int databaseSizeBeforeCreate = programCategoryPointRepository.findAll().size();

        // Create the ProgramCategoryPoint
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointMapper.toDto(programCategoryPoint);
        programCategoryPointDTO.setProgramId(programCategoryPoint.getProgram().getId());

        restProgramCategoryPointMockMvc.perform(post("/api/program-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCategoryPointDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramCategoryPoint in the database
        List<ProgramCategoryPoint> programCategoryPointList = programCategoryPointRepository.findAll();
        assertThat(programCategoryPointList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramCategoryPoint testProgramCategoryPoint = programCategoryPointList.get(programCategoryPointList.size() - 1);
        assertThat(testProgramCategoryPoint.getCategoryCode()).isEqualTo(DEFAULT_CATEGORY_CODE);
        assertThat(testProgramCategoryPoint.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
        assertThat(testProgramCategoryPoint.getPercentPoint()).isEqualTo(DEFAULT_PERCENT_POINT);
        //assertThat(testProgramCategoryPoint.getValuePoint()).isEqualTo(DEFAULT_VALUE_POINT);
    }

    @Test
    @Transactional
    public void createProgramCategoryPointWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programCategoryPointRepository.findAll().size();

        // Create the ProgramCategoryPoint with an existing ID
        programCategoryPoint.setId(1L);
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointMapper.toDto(programCategoryPoint);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramCategoryPointMockMvc.perform(post("/api/program-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCategoryPointDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCategoryPoint in the database
        List<ProgramCategoryPoint> programCategoryPointList = programCategoryPointRepository.findAll();
        assertThat(programCategoryPointList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCategoryCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCategoryPointRepository.findAll().size();
        // set the field null
        programCategoryPoint.setCategoryCode(null);

        // Create the ProgramCategoryPoint, which fails.
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointMapper.toDto(programCategoryPoint);

        restProgramCategoryPointMockMvc.perform(post("/api/program-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCategoryPointDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCategoryPoint> programCategoryPointList = programCategoryPointRepository.findAll();
        assertThat(programCategoryPointList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPoints() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList
        restProgramCategoryPointMockMvc.perform(get("/api/program-category-points?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCategoryPoint.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryCode").value(hasItem(DEFAULT_CATEGORY_CODE.toString())))
            .andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME.toString())))
          //  .andExpect(jsonPath("$.[*].percentPoint").value(hasItem(DEFAULT_PERCENT_POINT.intValue())))
          //  .andExpect(jsonPath("$.[*].valuePoint").value(hasItem(DEFAULT_VALUE_POINT.intValue())))
            .andExpect(jsonPath("$.[*].locked").value(hasItem(DEFAULT_LOCKED.booleanValue())));
    }

    @Test
    @Transactional
    public void getProgramCategoryPoint() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get the programCategoryPoint
        restProgramCategoryPointMockMvc.perform(get("/api/program-category-points/{id}", programCategoryPoint.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programCategoryPoint.getId().intValue()))
            .andExpect(jsonPath("$.categoryCode").value(DEFAULT_CATEGORY_CODE.toString()))
            .andExpect(jsonPath("$.categoryName").value(DEFAULT_CATEGORY_NAME.toString()))
            .andExpect(jsonPath("$.percentPoint").value(DEFAULT_PERCENT_POINT.intValue()))
            .andExpect(jsonPath("$.valuePoint").value(DEFAULT_VALUE_POINT.intValue()))
            .andExpect(jsonPath("$.locked").value(DEFAULT_LOCKED.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByCategoryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where categoryCode equals to DEFAULT_CATEGORY_CODE
        defaultProgramCategoryPointShouldBeFound("categoryCode.equals=" + DEFAULT_CATEGORY_CODE);

        // Get all the programCategoryPointList where categoryCode equals to UPDATED_CATEGORY_CODE
        defaultProgramCategoryPointShouldNotBeFound("categoryCode.equals=" + UPDATED_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByCategoryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where categoryCode in DEFAULT_CATEGORY_CODE or UPDATED_CATEGORY_CODE
        defaultProgramCategoryPointShouldBeFound("categoryCode.in=" + DEFAULT_CATEGORY_CODE + "," + UPDATED_CATEGORY_CODE);

        // Get all the programCategoryPointList where categoryCode equals to UPDATED_CATEGORY_CODE
        defaultProgramCategoryPointShouldNotBeFound("categoryCode.in=" + UPDATED_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByCategoryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where categoryCode is not null
        defaultProgramCategoryPointShouldBeFound("categoryCode.specified=true");

        // Get all the programCategoryPointList where categoryCode is null
        defaultProgramCategoryPointShouldNotBeFound("categoryCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByCategoryNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where categoryName equals to DEFAULT_CATEGORY_NAME
        defaultProgramCategoryPointShouldBeFound("categoryName.equals=" + DEFAULT_CATEGORY_NAME);

        // Get all the programCategoryPointList where categoryName equals to UPDATED_CATEGORY_NAME
        defaultProgramCategoryPointShouldNotBeFound("categoryName.equals=" + UPDATED_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByCategoryNameIsInShouldWork() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where categoryName in DEFAULT_CATEGORY_NAME or UPDATED_CATEGORY_NAME
        defaultProgramCategoryPointShouldBeFound("categoryName.in=" + DEFAULT_CATEGORY_NAME + "," + UPDATED_CATEGORY_NAME);

        // Get all the programCategoryPointList where categoryName equals to UPDATED_CATEGORY_NAME
        defaultProgramCategoryPointShouldNotBeFound("categoryName.in=" + UPDATED_CATEGORY_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByCategoryNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where categoryName is not null
        defaultProgramCategoryPointShouldBeFound("categoryName.specified=true");

        // Get all the programCategoryPointList where categoryName is null
        defaultProgramCategoryPointShouldNotBeFound("categoryName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByPercentPointIsEqualToSomething() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where percentPoint equals to DEFAULT_PERCENT_POINT
//        defaultProgramCategoryPointShouldBeFound("percentPoint.equals=" + DEFAULT_PERCENT_POINT);

        // Get all the programCategoryPointList where percentPoint equals to UPDATED_PERCENT_POINT
   //     defaultProgramCategoryPointShouldNotBeFound("percentPoint.equals=" + UPDATED_PERCENT_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByPercentPointIsInShouldWork() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where percentPoint in DEFAULT_PERCENT_POINT or UPDATED_PERCENT_POINT
    //    defaultProgramCategoryPointShouldBeFound("percentPoint.in=" + DEFAULT_PERCENT_POINT + "," + UPDATED_PERCENT_POINT);

        // Get all the programCategoryPointList where percentPoint equals to UPDATED_PERCENT_POINT
      //  defaultProgramCategoryPointShouldNotBeFound("percentPoint.in=" + UPDATED_PERCENT_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByPercentPointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where percentPoint is not null
        defaultProgramCategoryPointShouldBeFound("percentPoint.specified=true");

        // Get all the programCategoryPointList where percentPoint is null
        defaultProgramCategoryPointShouldNotBeFound("percentPoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByValuePointIsEqualToSomething() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where valuePoint equals to DEFAULT_VALUE_POINT
        defaultProgramCategoryPointShouldBeFound("valuePoint.equals=" + DEFAULT_VALUE_POINT);

        // Get all the programCategoryPointList where valuePoint equals to UPDATED_VALUE_POINT
        defaultProgramCategoryPointShouldNotBeFound("valuePoint.equals=" + UPDATED_VALUE_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByValuePointIsInShouldWork() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where valuePoint in DEFAULT_VALUE_POINT or UPDATED_VALUE_POINT
        defaultProgramCategoryPointShouldBeFound("valuePoint.in=" + DEFAULT_VALUE_POINT + "," + UPDATED_VALUE_POINT);

        // Get all the programCategoryPointList where valuePoint equals to UPDATED_VALUE_POINT
        defaultProgramCategoryPointShouldNotBeFound("valuePoint.in=" + UPDATED_VALUE_POINT);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByValuePointIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where valuePoint is not null
        defaultProgramCategoryPointShouldBeFound("valuePoint.specified=true");

        // Get all the programCategoryPointList where valuePoint is null
        defaultProgramCategoryPointShouldNotBeFound("valuePoint.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByLockedIsEqualToSomething() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where locked equals to DEFAULT_LOCKED
        defaultProgramCategoryPointShouldBeFound("locked.equals=" + DEFAULT_LOCKED);

        // Get all the programCategoryPointList where locked equals to UPDATED_LOCKED
        //defaultProgramCategoryPointShouldNotBeFound("locked.equals=" + UPDATED_LOCKED);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByLockedIsInShouldWork() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where locked in DEFAULT_LOCKED or UPDATED_LOCKED
        defaultProgramCategoryPointShouldBeFound("locked.in=" + DEFAULT_LOCKED + "," + UPDATED_LOCKED);

        // Get all the programCategoryPointList where locked equals to UPDATED_LOCKED
        //defaultProgramCategoryPointShouldNotBeFound("locked.in=" + UPDATED_LOCKED);
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByLockedIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        // Get all the programCategoryPointList where locked is not null
        defaultProgramCategoryPointShouldBeFound("locked.specified=true");

        // Get all the programCategoryPointList where locked is null
       // defaultProgramCategoryPointShouldNotBeFound("locked.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCategoryPointsByProgramIsEqualToSomething() throws Exception {
        // Get already existing entity
        Program program = programCategoryPoint.getProgram();
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);
        Long programId = program.getId();

        // Get all the programCategoryPointList where program equals to programId
        defaultProgramCategoryPointShouldBeFound("programId.equals=" + programId);

        // Get all the programCategoryPointList where program equals to programId + 1
        defaultProgramCategoryPointShouldNotBeFound("programId.equals=" + (programId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramCategoryPointShouldBeFound(String filter) throws Exception {
        restProgramCategoryPointMockMvc.perform(get("/api/program-category-points?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCategoryPoint.getId().intValue())))
            .andExpect(jsonPath("$.[*].categoryCode").value(hasItem(DEFAULT_CATEGORY_CODE)))
            .andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME)))
          //  .andExpect(jsonPath("$.[*].percentPoint").value(hasItem(DEFAULT_PERCENT_POINT.intValue())))
           // .andExpect(jsonPath("$.[*].valuePoint").value(hasItem(DEFAULT_VALUE_POINT.intValue())))
            .andExpect(jsonPath("$.[*].locked").value(hasItem(DEFAULT_LOCKED.booleanValue())));

        // Check, that the count call also returns 1
        restProgramCategoryPointMockMvc.perform(get("/api/program-category-points/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramCategoryPointShouldNotBeFound(String filter) throws Exception {
        restProgramCategoryPointMockMvc.perform(get("/api/program-category-points?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramCategoryPointMockMvc.perform(get("/api/program-category-points/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramCategoryPoint() throws Exception {
        // Get the programCategoryPoint
        restProgramCategoryPointMockMvc.perform(get("/api/program-category-points/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramCategoryPoint() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        int databaseSizeBeforeUpdate = programCategoryPointRepository.findAll().size();

        // Update the programCategoryPoint
        ProgramCategoryPoint updatedProgramCategoryPoint = programCategoryPointRepository.findById(programCategoryPoint.getId()).get();
        // Disconnect from session so that the updates on updatedProgramCategoryPoint are not directly saved in db
        em.detach(updatedProgramCategoryPoint);
        updatedProgramCategoryPoint
            .categoryCode(DEFAULT_CATEGORY_CODE)
            .categoryName(DEFAULT_CATEGORY_CODE)
            .percentPoint(UPDATED_PERCENT_POINT)
            .valuePoint(UPDATED_VALUE_POINT)
            .locked(UPDATED_LOCKED);
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointMapper.toDto(updatedProgramCategoryPoint);

        restProgramCategoryPointMockMvc.perform(put("/api/program-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCategoryPointDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramCategoryPoint in the database
        List<ProgramCategoryPoint> programCategoryPointList = programCategoryPointRepository.findAll();
        assertThat(programCategoryPointList).hasSize(databaseSizeBeforeUpdate);
        ProgramCategoryPoint testProgramCategoryPoint = programCategoryPointList.get(programCategoryPointList.size() - 1);
        assertThat(testProgramCategoryPoint.getCategoryCode()).isEqualTo(DEFAULT_CATEGORY_CODE);
        assertThat(testProgramCategoryPoint.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_CODE);
        assertThat(testProgramCategoryPoint.getPercentPoint()).isEqualTo(UPDATED_PERCENT_POINT);
      //  assertThat(testProgramCategoryPoint.getValuePoint()).isEqualTo(UPDATED_VALUE_POINT);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramCategoryPoint() throws Exception {
        int databaseSizeBeforeUpdate = programCategoryPointRepository.findAll().size();

        // Create the ProgramCategoryPoint
        ProgramCategoryPointDTO programCategoryPointDTO = programCategoryPointMapper.toDto(programCategoryPoint);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramCategoryPointMockMvc.perform(put("/api/program-category-points")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCategoryPointDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCategoryPoint in the database
        List<ProgramCategoryPoint> programCategoryPointList = programCategoryPointRepository.findAll();
        assertThat(programCategoryPointList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramCategoryPoint() throws Exception {
        // Initialize the database
        programCategoryPointRepository.saveAndFlush(programCategoryPoint);

        int databaseSizeBeforeDelete = programCategoryPointRepository.findAll().size();

        // Delete the programCategoryPoint
        restProgramCategoryPointMockMvc.perform(delete("/api/program-category-points/{id}", programCategoryPoint.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramCategoryPoint> programCategoryPointList = programCategoryPointRepository.findAll();
        assertThat(programCategoryPointList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCategoryPoint.class);
        ProgramCategoryPoint programCategoryPoint1 = new ProgramCategoryPoint();
        programCategoryPoint1.setId(1L);
        ProgramCategoryPoint programCategoryPoint2 = new ProgramCategoryPoint();
        programCategoryPoint2.setId(programCategoryPoint1.getId());
        assertThat(programCategoryPoint1).isEqualTo(programCategoryPoint2);
        programCategoryPoint2.setId(2L);
        assertThat(programCategoryPoint1).isNotEqualTo(programCategoryPoint2);
        programCategoryPoint1.setId(null);
        assertThat(programCategoryPoint1).isNotEqualTo(programCategoryPoint2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCategoryPointDTO.class);
        ProgramCategoryPointDTO programCategoryPointDTO1 = new ProgramCategoryPointDTO();
        programCategoryPointDTO1.setId(1L);
        ProgramCategoryPointDTO programCategoryPointDTO2 = new ProgramCategoryPointDTO();
        assertThat(programCategoryPointDTO1).isNotEqualTo(programCategoryPointDTO2);
        programCategoryPointDTO2.setId(programCategoryPointDTO1.getId());
        assertThat(programCategoryPointDTO1).isEqualTo(programCategoryPointDTO2);
        programCategoryPointDTO2.setId(2L);
        assertThat(programCategoryPointDTO1).isNotEqualTo(programCategoryPointDTO2);
        programCategoryPointDTO1.setId(null);
        assertThat(programCategoryPointDTO1).isNotEqualTo(programCategoryPointDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programCategoryPointMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programCategoryPointMapper.fromId(null)).isNull();
    }

    @Test
    @Transactional
    public void testAddProgramCategoryPoints() throws Exception {

        // Create the ProgramCategoryPoint
        List<ProgramCategoryPointDTO> programCategoryPointDTOs =new ArrayList<>();
        ProgramCategoryPointDTO programCategoryPointDTO1 = programCategoryPointMapper.toDto(programCategoryPoint);
        programCategoryPointDTO1.setProgramId(programCategoryPoint.getProgram().getId());

        ProgramCategoryPointDTO programCategoryPointDTO2 = programCategoryPointMapper.toDto(programCategoryPoint);
        programCategoryPointDTO2.setProgramId(programCategoryPoint.getProgram().getId());

        programCategoryPointDTOs.add(programCategoryPointDTO1);
        programCategoryPointDTOs.add(programCategoryPointDTO2);

        restProgramCategoryPointMockMvc.perform(post("/api//program-category-points/1/add-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCategoryPointDTOs)));

    }
}
