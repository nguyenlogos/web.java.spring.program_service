package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ClientProgramRewardSetting;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ClientProgramRewardSettingRepository;
import aduro.basic.programservice.service.ClientProgramRewardSettingService;
import aduro.basic.programservice.service.dto.ClientProgramRewardSettingDTO;
import aduro.basic.programservice.service.mapper.ClientProgramRewardSettingMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ClientProgramRewardSettingCriteria;
import aduro.basic.programservice.service.ClientProgramRewardSettingQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ClientProgramRewardSettingResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ClientProgramRewardSettingResourceIT {

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_IDENTIFIER = "BBBBBBBBBB";

    private static final String DEFAULT_ACCOUNT_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_ACCOUNT_IDENTIFIER = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_ACCOUNT_THRESHOLD = new BigDecimal(1);
    private static final BigDecimal UPDATED_ACCOUNT_THRESHOLD = new BigDecimal(2);

    private static final BigDecimal DEFAULT_CURRENT_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CURRENT_BALANCE = new BigDecimal(2);

    private static final String DEFAULT_CLIENT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_EMAIL = "BBBBBBBBBB";

    @Autowired
    private ClientProgramRewardSettingRepository clientProgramRewardSettingRepository;

    @Autowired
    private ClientProgramRewardSettingMapper clientProgramRewardSettingMapper;

    @Autowired
    private ClientProgramRewardSettingService clientProgramRewardSettingService;

    @Autowired
    private ClientProgramRewardSettingQueryService clientProgramRewardSettingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClientProgramRewardSettingMockMvc;

    private ClientProgramRewardSetting clientProgramRewardSetting;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientProgramRewardSettingResource clientProgramRewardSettingResource = new ClientProgramRewardSettingResource(clientProgramRewardSettingService, clientProgramRewardSettingQueryService);
        this.restClientProgramRewardSettingMockMvc = MockMvcBuilders.standaloneSetup(clientProgramRewardSettingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientProgramRewardSetting createEntity(EntityManager em) {
        ClientProgramRewardSetting clientProgramRewardSetting = new ClientProgramRewardSetting()
            .clientId(DEFAULT_CLIENT_ID)
            .clientName(DEFAULT_CLIENT_NAME)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY)
            .customerIdentifier(DEFAULT_CUSTOMER_IDENTIFIER)
            .accountIdentifier(DEFAULT_ACCOUNT_IDENTIFIER)
            .accountThreshold(DEFAULT_ACCOUNT_THRESHOLD)
            .currentBalance(DEFAULT_CURRENT_BALANCE)
            .clientEmail(DEFAULT_CLIENT_EMAIL);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        clientProgramRewardSetting.setProgram(program);
        return clientProgramRewardSetting;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientProgramRewardSetting createUpdatedEntity(EntityManager em) {
        ClientProgramRewardSetting clientProgramRewardSetting = new ClientProgramRewardSetting()
            .clientId(UPDATED_CLIENT_ID)
            .clientName(UPDATED_CLIENT_NAME)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .customerIdentifier(UPDATED_CUSTOMER_IDENTIFIER)
            .accountIdentifier(UPDATED_ACCOUNT_IDENTIFIER)
            .accountThreshold(UPDATED_ACCOUNT_THRESHOLD)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .clientEmail(UPDATED_CLIENT_EMAIL);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createUpdatedEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        clientProgramRewardSetting.setProgram(program);
        return clientProgramRewardSetting;
    }

    @BeforeEach
    public void initTest() {
        clientProgramRewardSetting = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientProgramRewardSetting() throws Exception {
        int databaseSizeBeforeCreate = clientProgramRewardSettingRepository.findAll().size();

        // Create the ClientProgramRewardSetting
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);
        restClientProgramRewardSettingMockMvc.perform(post("/api/client-program-reward-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramRewardSettingDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientProgramRewardSetting in the database
        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeCreate + 1);
        ClientProgramRewardSetting testClientProgramRewardSetting = clientProgramRewardSettingList.get(clientProgramRewardSettingList.size() - 1);
        assertThat(testClientProgramRewardSetting.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testClientProgramRewardSetting.getClientName()).isEqualTo(DEFAULT_CLIENT_NAME);
        assertThat(testClientProgramRewardSetting.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);
        assertThat(testClientProgramRewardSetting.getCustomerIdentifier()).isEqualTo(DEFAULT_CUSTOMER_IDENTIFIER);
        assertThat(testClientProgramRewardSetting.getAccountIdentifier()).isEqualTo(DEFAULT_ACCOUNT_IDENTIFIER);
        assertThat(testClientProgramRewardSetting.getAccountThreshold()).isEqualTo(DEFAULT_ACCOUNT_THRESHOLD);
        assertThat(testClientProgramRewardSetting.getCurrentBalance()).isEqualTo(DEFAULT_CURRENT_BALANCE);
        assertThat(testClientProgramRewardSetting.getClientEmail()).isEqualTo(DEFAULT_CLIENT_EMAIL);
    }

    @Test
    @Transactional
    public void createClientProgramRewardSettingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientProgramRewardSettingRepository.findAll().size();

        // Create the ClientProgramRewardSetting with an existing ID
        clientProgramRewardSetting.setId(1L);
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientProgramRewardSettingMockMvc.perform(post("/api/client-program-reward-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramRewardSettingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientProgramRewardSetting in the database
        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientProgramRewardSettingRepository.findAll().size();
        // set the field null
        clientProgramRewardSetting.setClientId(null);

        // Create the ClientProgramRewardSetting, which fails.
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);

        restClientProgramRewardSettingMockMvc.perform(post("/api/client-program-reward-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramRewardSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClientNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientProgramRewardSettingRepository.findAll().size();
        // set the field null
        clientProgramRewardSetting.setClientName(null);

        // Create the ClientProgramRewardSetting, which fails.
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);

        restClientProgramRewardSettingMockMvc.perform(post("/api/client-program-reward-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramRewardSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientProgramRewardSettingRepository.findAll().size();
        // set the field null
        clientProgramRewardSetting.setUpdatedDate(null);

        // Create the ClientProgramRewardSetting, which fails.
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);

        restClientProgramRewardSettingMockMvc.perform(post("/api/client-program-reward-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramRewardSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientProgramRewardSettingRepository.findAll().size();
        // set the field null
        clientProgramRewardSetting.setUpdatedBy(null);

        // Create the ClientProgramRewardSetting, which fails.
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);

        restClientProgramRewardSettingMockMvc.perform(post("/api/client-program-reward-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramRewardSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettings() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList
        restClientProgramRewardSettingMockMvc.perform(get("/api/client-program-reward-settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientProgramRewardSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())))
            .andExpect(jsonPath("$.[*].customerIdentifier").value(hasItem(DEFAULT_CUSTOMER_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].accountIdentifier").value(hasItem(DEFAULT_ACCOUNT_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].accountThreshold").value(hasItem(DEFAULT_ACCOUNT_THRESHOLD.intValue())))
            .andExpect(jsonPath("$.[*].currentBalance").value(hasItem(DEFAULT_CURRENT_BALANCE.intValue())))
            .andExpect(jsonPath("$.[*].clientEmail").value(hasItem(DEFAULT_CLIENT_EMAIL.toString())));
    }

    @Test
    @Transactional
    public void getClientProgramRewardSetting() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get the clientProgramRewardSetting
        restClientProgramRewardSettingMockMvc.perform(get("/api/client-program-reward-settings/{id}", clientProgramRewardSetting.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientProgramRewardSetting.getId().intValue()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.clientName").value(DEFAULT_CLIENT_NAME.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()))
            .andExpect(jsonPath("$.customerIdentifier").value(DEFAULT_CUSTOMER_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.accountIdentifier").value(DEFAULT_ACCOUNT_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.accountThreshold").value(DEFAULT_ACCOUNT_THRESHOLD.intValue()))
            .andExpect(jsonPath("$.currentBalance").value(DEFAULT_CURRENT_BALANCE.intValue()))
            .andExpect(jsonPath("$.clientEmail").value(DEFAULT_CLIENT_EMAIL.toString()));
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientId equals to DEFAULT_CLIENT_ID
        defaultClientProgramRewardSettingShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the clientProgramRewardSettingList where clientId equals to UPDATED_CLIENT_ID
        defaultClientProgramRewardSettingShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultClientProgramRewardSettingShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the clientProgramRewardSettingList where clientId equals to UPDATED_CLIENT_ID
        defaultClientProgramRewardSettingShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientId is not null
        defaultClientProgramRewardSettingShouldBeFound("clientId.specified=true");

        // Get all the clientProgramRewardSettingList where clientId is null
        defaultClientProgramRewardSettingShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientNameIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientName equals to DEFAULT_CLIENT_NAME
        defaultClientProgramRewardSettingShouldBeFound("clientName.equals=" + DEFAULT_CLIENT_NAME);

        // Get all the clientProgramRewardSettingList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientProgramRewardSettingShouldNotBeFound("clientName.equals=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientNameIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientName in DEFAULT_CLIENT_NAME or UPDATED_CLIENT_NAME
        defaultClientProgramRewardSettingShouldBeFound("clientName.in=" + DEFAULT_CLIENT_NAME + "," + UPDATED_CLIENT_NAME);

        // Get all the clientProgramRewardSettingList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientProgramRewardSettingShouldNotBeFound("clientName.in=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientName is not null
        defaultClientProgramRewardSettingShouldBeFound("clientName.specified=true");

        // Get all the clientProgramRewardSettingList where clientName is null
        defaultClientProgramRewardSettingShouldNotBeFound("clientName.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultClientProgramRewardSettingShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the clientProgramRewardSettingList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultClientProgramRewardSettingShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultClientProgramRewardSettingShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the clientProgramRewardSettingList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultClientProgramRewardSettingShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where updatedDate is not null
        defaultClientProgramRewardSettingShouldBeFound("updatedDate.specified=true");

        // Get all the clientProgramRewardSettingList where updatedDate is null
        defaultClientProgramRewardSettingShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultClientProgramRewardSettingShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the clientProgramRewardSettingList where updatedBy equals to UPDATED_UPDATED_BY
        defaultClientProgramRewardSettingShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultClientProgramRewardSettingShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the clientProgramRewardSettingList where updatedBy equals to UPDATED_UPDATED_BY
        defaultClientProgramRewardSettingShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where updatedBy is not null
        defaultClientProgramRewardSettingShouldBeFound("updatedBy.specified=true");

        // Get all the clientProgramRewardSettingList where updatedBy is null
        defaultClientProgramRewardSettingShouldNotBeFound("updatedBy.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByCustomerIdentifierIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where customerIdentifier equals to DEFAULT_CUSTOMER_IDENTIFIER
        defaultClientProgramRewardSettingShouldBeFound("customerIdentifier.equals=" + DEFAULT_CUSTOMER_IDENTIFIER);

        // Get all the clientProgramRewardSettingList where customerIdentifier equals to UPDATED_CUSTOMER_IDENTIFIER
        defaultClientProgramRewardSettingShouldNotBeFound("customerIdentifier.equals=" + UPDATED_CUSTOMER_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByCustomerIdentifierIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where customerIdentifier in DEFAULT_CUSTOMER_IDENTIFIER or UPDATED_CUSTOMER_IDENTIFIER
        defaultClientProgramRewardSettingShouldBeFound("customerIdentifier.in=" + DEFAULT_CUSTOMER_IDENTIFIER + "," + UPDATED_CUSTOMER_IDENTIFIER);

        // Get all the clientProgramRewardSettingList where customerIdentifier equals to UPDATED_CUSTOMER_IDENTIFIER
        defaultClientProgramRewardSettingShouldNotBeFound("customerIdentifier.in=" + UPDATED_CUSTOMER_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByCustomerIdentifierIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where customerIdentifier is not null
        defaultClientProgramRewardSettingShouldBeFound("customerIdentifier.specified=true");

        // Get all the clientProgramRewardSettingList where customerIdentifier is null
        defaultClientProgramRewardSettingShouldNotBeFound("customerIdentifier.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByAccountIdentifierIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where accountIdentifier equals to DEFAULT_ACCOUNT_IDENTIFIER
        defaultClientProgramRewardSettingShouldBeFound("accountIdentifier.equals=" + DEFAULT_ACCOUNT_IDENTIFIER);

        // Get all the clientProgramRewardSettingList where accountIdentifier equals to UPDATED_ACCOUNT_IDENTIFIER
        defaultClientProgramRewardSettingShouldNotBeFound("accountIdentifier.equals=" + UPDATED_ACCOUNT_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByAccountIdentifierIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where accountIdentifier in DEFAULT_ACCOUNT_IDENTIFIER or UPDATED_ACCOUNT_IDENTIFIER
        defaultClientProgramRewardSettingShouldBeFound("accountIdentifier.in=" + DEFAULT_ACCOUNT_IDENTIFIER + "," + UPDATED_ACCOUNT_IDENTIFIER);

        // Get all the clientProgramRewardSettingList where accountIdentifier equals to UPDATED_ACCOUNT_IDENTIFIER
        defaultClientProgramRewardSettingShouldNotBeFound("accountIdentifier.in=" + UPDATED_ACCOUNT_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByAccountIdentifierIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where accountIdentifier is not null
        defaultClientProgramRewardSettingShouldBeFound("accountIdentifier.specified=true");

        // Get all the clientProgramRewardSettingList where accountIdentifier is null
        defaultClientProgramRewardSettingShouldNotBeFound("accountIdentifier.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByAccountThresholdIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where accountThreshold equals to DEFAULT_ACCOUNT_THRESHOLD
        defaultClientProgramRewardSettingShouldBeFound("accountThreshold.equals=" + DEFAULT_ACCOUNT_THRESHOLD);

        // Get all the clientProgramRewardSettingList where accountThreshold equals to UPDATED_ACCOUNT_THRESHOLD
        defaultClientProgramRewardSettingShouldNotBeFound("accountThreshold.equals=" + UPDATED_ACCOUNT_THRESHOLD);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByAccountThresholdIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where accountThreshold in DEFAULT_ACCOUNT_THRESHOLD or UPDATED_ACCOUNT_THRESHOLD
        defaultClientProgramRewardSettingShouldBeFound("accountThreshold.in=" + DEFAULT_ACCOUNT_THRESHOLD + "," + UPDATED_ACCOUNT_THRESHOLD);

        // Get all the clientProgramRewardSettingList where accountThreshold equals to UPDATED_ACCOUNT_THRESHOLD
        defaultClientProgramRewardSettingShouldNotBeFound("accountThreshold.in=" + UPDATED_ACCOUNT_THRESHOLD);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByAccountThresholdIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where accountThreshold is not null
        defaultClientProgramRewardSettingShouldBeFound("accountThreshold.specified=true");

        // Get all the clientProgramRewardSettingList where accountThreshold is null
        defaultClientProgramRewardSettingShouldNotBeFound("accountThreshold.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByCurrentBalanceIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where currentBalance equals to DEFAULT_CURRENT_BALANCE
        defaultClientProgramRewardSettingShouldBeFound("currentBalance.equals=" + DEFAULT_CURRENT_BALANCE);

        // Get all the clientProgramRewardSettingList where currentBalance equals to UPDATED_CURRENT_BALANCE
        defaultClientProgramRewardSettingShouldNotBeFound("currentBalance.equals=" + UPDATED_CURRENT_BALANCE);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByCurrentBalanceIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where currentBalance in DEFAULT_CURRENT_BALANCE or UPDATED_CURRENT_BALANCE
        defaultClientProgramRewardSettingShouldBeFound("currentBalance.in=" + DEFAULT_CURRENT_BALANCE + "," + UPDATED_CURRENT_BALANCE);

        // Get all the clientProgramRewardSettingList where currentBalance equals to UPDATED_CURRENT_BALANCE
        defaultClientProgramRewardSettingShouldNotBeFound("currentBalance.in=" + UPDATED_CURRENT_BALANCE);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByCurrentBalanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where currentBalance is not null
        defaultClientProgramRewardSettingShouldBeFound("currentBalance.specified=true");

        // Get all the clientProgramRewardSettingList where currentBalance is null
        defaultClientProgramRewardSettingShouldNotBeFound("currentBalance.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientEmail equals to DEFAULT_CLIENT_EMAIL
        defaultClientProgramRewardSettingShouldBeFound("clientEmail.equals=" + DEFAULT_CLIENT_EMAIL);

        // Get all the clientProgramRewardSettingList where clientEmail equals to UPDATED_CLIENT_EMAIL
        defaultClientProgramRewardSettingShouldNotBeFound("clientEmail.equals=" + UPDATED_CLIENT_EMAIL);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientEmailIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientEmail in DEFAULT_CLIENT_EMAIL or UPDATED_CLIENT_EMAIL
        defaultClientProgramRewardSettingShouldBeFound("clientEmail.in=" + DEFAULT_CLIENT_EMAIL + "," + UPDATED_CLIENT_EMAIL);

        // Get all the clientProgramRewardSettingList where clientEmail equals to UPDATED_CLIENT_EMAIL
        defaultClientProgramRewardSettingShouldNotBeFound("clientEmail.in=" + UPDATED_CLIENT_EMAIL);
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByClientEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        // Get all the clientProgramRewardSettingList where clientEmail is not null
        defaultClientProgramRewardSettingShouldBeFound("clientEmail.specified=true");

        // Get all the clientProgramRewardSettingList where clientEmail is null
        defaultClientProgramRewardSettingShouldNotBeFound("clientEmail.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramRewardSettingsByProgramIsEqualToSomething() throws Exception {
        // Get already existing entity
        Program program = clientProgramRewardSetting.getProgram();
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);
        Long programId = program.getId();

        // Get all the clientProgramRewardSettingList where program equals to programId
        defaultClientProgramRewardSettingShouldBeFound("programId.equals=" + programId);

        // Get all the clientProgramRewardSettingList where program equals to programId + 1
        defaultClientProgramRewardSettingShouldNotBeFound("programId.equals=" + (programId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClientProgramRewardSettingShouldBeFound(String filter) throws Exception {
        restClientProgramRewardSettingMockMvc.perform(get("/api/client-program-reward-settings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientProgramRewardSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)))
            .andExpect(jsonPath("$.[*].customerIdentifier").value(hasItem(DEFAULT_CUSTOMER_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].accountIdentifier").value(hasItem(DEFAULT_ACCOUNT_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].accountThreshold").value(hasItem(DEFAULT_ACCOUNT_THRESHOLD.intValue())))
            .andExpect(jsonPath("$.[*].currentBalance").value(hasItem(DEFAULT_CURRENT_BALANCE.intValue())))
            .andExpect(jsonPath("$.[*].clientEmail").value(hasItem(DEFAULT_CLIENT_EMAIL)));

        // Check, that the count call also returns 1
        restClientProgramRewardSettingMockMvc.perform(get("/api/client-program-reward-settings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClientProgramRewardSettingShouldNotBeFound(String filter) throws Exception {
        restClientProgramRewardSettingMockMvc.perform(get("/api/client-program-reward-settings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClientProgramRewardSettingMockMvc.perform(get("/api/client-program-reward-settings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingClientProgramRewardSetting() throws Exception {
        // Get the clientProgramRewardSetting
        restClientProgramRewardSettingMockMvc.perform(get("/api/client-program-reward-settings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientProgramRewardSetting() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        int databaseSizeBeforeUpdate = clientProgramRewardSettingRepository.findAll().size();

        // Update the clientProgramRewardSetting
        ClientProgramRewardSetting updatedClientProgramRewardSetting = clientProgramRewardSettingRepository.findById(clientProgramRewardSetting.getId()).get();
        // Disconnect from session so that the updates on updatedClientProgramRewardSetting are not directly saved in db
        em.detach(updatedClientProgramRewardSetting);
        updatedClientProgramRewardSetting
            .clientId(UPDATED_CLIENT_ID)
            .clientName(UPDATED_CLIENT_NAME)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY)
            .customerIdentifier(UPDATED_CUSTOMER_IDENTIFIER)
            .accountIdentifier(UPDATED_ACCOUNT_IDENTIFIER)
            .accountThreshold(UPDATED_ACCOUNT_THRESHOLD)
            .currentBalance(UPDATED_CURRENT_BALANCE)
            .clientEmail(UPDATED_CLIENT_EMAIL);
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = clientProgramRewardSettingMapper.toDto(updatedClientProgramRewardSetting);

        restClientProgramRewardSettingMockMvc.perform(put("/api/client-program-reward-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramRewardSettingDTO)))
            .andExpect(status().isOk());

        // Validate the ClientProgramRewardSetting in the database
        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeUpdate);
        ClientProgramRewardSetting testClientProgramRewardSetting = clientProgramRewardSettingList.get(clientProgramRewardSettingList.size() - 1);
        assertThat(testClientProgramRewardSetting.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testClientProgramRewardSetting.getClientName()).isEqualTo(UPDATED_CLIENT_NAME);
        assertThat(testClientProgramRewardSetting.getUpdatedBy()).isEqualTo(UPDATED_UPDATED_BY);
        assertThat(testClientProgramRewardSetting.getCustomerIdentifier()).isEqualTo(UPDATED_CUSTOMER_IDENTIFIER);
        assertThat(testClientProgramRewardSetting.getAccountIdentifier()).isEqualTo(UPDATED_ACCOUNT_IDENTIFIER);
        assertThat(testClientProgramRewardSetting.getAccountThreshold()).isEqualTo(UPDATED_ACCOUNT_THRESHOLD);
        assertThat(testClientProgramRewardSetting.getCurrentBalance()).isEqualTo(UPDATED_CURRENT_BALANCE);
        assertThat(testClientProgramRewardSetting.getClientEmail()).isEqualTo(UPDATED_CLIENT_EMAIL);
    }

    @Test
    @Transactional
    public void updateNonExistingClientProgramRewardSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientProgramRewardSettingRepository.findAll().size();

        // Create the ClientProgramRewardSetting
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO = clientProgramRewardSettingMapper.toDto(clientProgramRewardSetting);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientProgramRewardSettingMockMvc.perform(put("/api/client-program-reward-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramRewardSettingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientProgramRewardSetting in the database
        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClientProgramRewardSetting() throws Exception {
        // Initialize the database
        clientProgramRewardSettingRepository.saveAndFlush(clientProgramRewardSetting);

        int databaseSizeBeforeDelete = clientProgramRewardSettingRepository.findAll().size();

        // Delete the clientProgramRewardSetting
        restClientProgramRewardSettingMockMvc.perform(delete("/api/client-program-reward-settings/{id}", clientProgramRewardSetting.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ClientProgramRewardSetting> clientProgramRewardSettingList = clientProgramRewardSettingRepository.findAll();
        assertThat(clientProgramRewardSettingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientProgramRewardSetting.class);
        ClientProgramRewardSetting clientProgramRewardSetting1 = new ClientProgramRewardSetting();
        clientProgramRewardSetting1.setId(1L);
        ClientProgramRewardSetting clientProgramRewardSetting2 = new ClientProgramRewardSetting();
        clientProgramRewardSetting2.setId(clientProgramRewardSetting1.getId());
        assertThat(clientProgramRewardSetting1).isEqualTo(clientProgramRewardSetting2);
        clientProgramRewardSetting2.setId(2L);
        assertThat(clientProgramRewardSetting1).isNotEqualTo(clientProgramRewardSetting2);
        clientProgramRewardSetting1.setId(null);
        assertThat(clientProgramRewardSetting1).isNotEqualTo(clientProgramRewardSetting2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientProgramRewardSettingDTO.class);
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO1 = new ClientProgramRewardSettingDTO();
        clientProgramRewardSettingDTO1.setId(1L);
        ClientProgramRewardSettingDTO clientProgramRewardSettingDTO2 = new ClientProgramRewardSettingDTO();
        assertThat(clientProgramRewardSettingDTO1).isNotEqualTo(clientProgramRewardSettingDTO2);
        clientProgramRewardSettingDTO2.setId(clientProgramRewardSettingDTO1.getId());
        assertThat(clientProgramRewardSettingDTO1).isEqualTo(clientProgramRewardSettingDTO2);
        clientProgramRewardSettingDTO2.setId(2L);
        assertThat(clientProgramRewardSettingDTO1).isNotEqualTo(clientProgramRewardSettingDTO2);
        clientProgramRewardSettingDTO1.setId(null);
        assertThat(clientProgramRewardSettingDTO1).isNotEqualTo(clientProgramRewardSettingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientProgramRewardSettingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientProgramRewardSettingMapper.fromId(null)).isNull();
    }
}
