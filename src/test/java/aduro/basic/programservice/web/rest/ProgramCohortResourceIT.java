package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramCohortRule;
import aduro.basic.programservice.domain.ProgramCohortCollection;
import aduro.basic.programservice.repository.ProgramCohortRepository;
import aduro.basic.programservice.service.ProgramCohortService;
import aduro.basic.programservice.service.dto.ProgramCohortDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramCohortCriteria;
import aduro.basic.programservice.service.ProgramCohortQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramCohortResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramCohortResourceIT {

    private static final String DEFAULT_COHORT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_COHORT_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_DEFAULT = false;
    private static final Boolean UPDATED_IS_DEFAULT = true;

    @Autowired
    private ProgramCohortRepository programCohortRepository;

    @Autowired
    private ProgramCohortMapper programCohortMapper;

    @Autowired
    private ProgramCohortService programCohortService;

    @Autowired
    private ProgramCohortQueryService programCohortQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramCohortMockMvc;

    private ProgramCohort programCohort;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramCohortResource programCohortResource = new ProgramCohortResource(programCohortService, programCohortQueryService);
        this.restProgramCohortMockMvc = MockMvcBuilders.standaloneSetup(programCohortResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCohort createEntity(EntityManager em) {
        ProgramCohort programCohort = new ProgramCohort()
            .cohortName(DEFAULT_COHORT_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE)
            .isDefault(DEFAULT_IS_DEFAULT);
        return programCohort;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCohort createUpdatedEntity(EntityManager em) {
        ProgramCohort programCohort = new ProgramCohort()
            .cohortName(UPDATED_COHORT_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .isDefault(UPDATED_IS_DEFAULT);
        return programCohort;
    }

    @BeforeEach
    public void initTest() {
        programCohort = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramCohort() throws Exception {
        int databaseSizeBeforeCreate = programCohortRepository.findAll().size();

        // Create the ProgramCohort
        ProgramCohortDTO programCohortDTO = programCohortMapper.toDto(programCohort);
        restProgramCohortMockMvc.perform(post("/api/program-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramCohort in the database
        List<ProgramCohort> programCohortList = programCohortRepository.findAll();
        assertThat(programCohortList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramCohort testProgramCohort = programCohortList.get(programCohortList.size() - 1);
        assertThat(testProgramCohort.getCohortName()).isEqualTo(DEFAULT_COHORT_NAME);
        assertThat(testProgramCohort.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramCohort.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
        assertThat(testProgramCohort.isIsDefault()).isEqualTo(DEFAULT_IS_DEFAULT);
    }

    @Test
    @Transactional
    public void createProgramCohortWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programCohortRepository.findAll().size();

        // Create the ProgramCohort with an existing ID
        programCohort.setId(1L);
        ProgramCohortDTO programCohortDTO = programCohortMapper.toDto(programCohort);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramCohortMockMvc.perform(post("/api/program-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCohort in the database
        List<ProgramCohort> programCohortList = programCohortRepository.findAll();
        assertThat(programCohortList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCohortNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortRepository.findAll().size();
        // set the field null
        programCohort.setCohortName(null);

        // Create the ProgramCohort, which fails.
        ProgramCohortDTO programCohortDTO = programCohortMapper.toDto(programCohort);

        restProgramCohortMockMvc.perform(post("/api/program-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohort> programCohortList = programCohortRepository.findAll();
        assertThat(programCohortList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortRepository.findAll().size();
        // set the field null
        programCohort.setCreatedDate(null);

        // Create the ProgramCohort, which fails.
        ProgramCohortDTO programCohortDTO = programCohortMapper.toDto(programCohort);

        restProgramCohortMockMvc.perform(post("/api/program-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohort> programCohortList = programCohortRepository.findAll();
        assertThat(programCohortList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModifiedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortRepository.findAll().size();
        // set the field null
        programCohort.setModifiedDate(null);

        // Create the ProgramCohort, which fails.
        ProgramCohortDTO programCohortDTO = programCohortMapper.toDto(programCohort);

        restProgramCohortMockMvc.perform(post("/api/program-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohort> programCohortList = programCohortRepository.findAll();
        assertThat(programCohortList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramCohorts() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList
        restProgramCohortMockMvc.perform(get("/api/program-cohorts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCohort.getId().intValue())))
            .andExpect(jsonPath("$.[*].cohortName").value(hasItem(DEFAULT_COHORT_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDefault").value(hasItem(DEFAULT_IS_DEFAULT.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getProgramCohort() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get the programCohort
        restProgramCohortMockMvc.perform(get("/api/program-cohorts/{id}", programCohort.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programCohort.getId().intValue()))
            .andExpect(jsonPath("$.cohortName").value(DEFAULT_COHORT_NAME.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.isDefault").value(DEFAULT_IS_DEFAULT.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByCohortNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where cohortName equals to DEFAULT_COHORT_NAME
        defaultProgramCohortShouldBeFound("cohortName.equals=" + DEFAULT_COHORT_NAME);

        // Get all the programCohortList where cohortName equals to UPDATED_COHORT_NAME
        defaultProgramCohortShouldNotBeFound("cohortName.equals=" + UPDATED_COHORT_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByCohortNameIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where cohortName in DEFAULT_COHORT_NAME or UPDATED_COHORT_NAME
        defaultProgramCohortShouldBeFound("cohortName.in=" + DEFAULT_COHORT_NAME + "," + UPDATED_COHORT_NAME);

        // Get all the programCohortList where cohortName equals to UPDATED_COHORT_NAME
        defaultProgramCohortShouldNotBeFound("cohortName.in=" + UPDATED_COHORT_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByCohortNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where cohortName is not null
        defaultProgramCohortShouldBeFound("cohortName.specified=true");

        // Get all the programCohortList where cohortName is null
        defaultProgramCohortShouldNotBeFound("cohortName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramCohortShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programCohortList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCohortShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramCohortShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programCohortList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCohortShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where createdDate is not null
        defaultProgramCohortShouldBeFound("createdDate.specified=true");

        // Get all the programCohortList where createdDate is null
        defaultProgramCohortShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramCohortShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programCohortList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCohortShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramCohortShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programCohortList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCohortShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where modifiedDate is not null
        defaultProgramCohortShouldBeFound("modifiedDate.specified=true");

        // Get all the programCohortList where modifiedDate is null
        defaultProgramCohortShouldNotBeFound("modifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByIsDefaultIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where isDefault equals to DEFAULT_IS_DEFAULT
        defaultProgramCohortShouldBeFound("isDefault.equals=" + DEFAULT_IS_DEFAULT);

        // Get all the programCohortList where isDefault equals to UPDATED_IS_DEFAULT
        defaultProgramCohortShouldNotBeFound("isDefault.equals=" + UPDATED_IS_DEFAULT);
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByIsDefaultIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where isDefault in DEFAULT_IS_DEFAULT or UPDATED_IS_DEFAULT
        defaultProgramCohortShouldBeFound("isDefault.in=" + DEFAULT_IS_DEFAULT + "," + UPDATED_IS_DEFAULT);

        // Get all the programCohortList where isDefault equals to UPDATED_IS_DEFAULT
        defaultProgramCohortShouldNotBeFound("isDefault.in=" + UPDATED_IS_DEFAULT);
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByIsDefaultIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        // Get all the programCohortList where isDefault is not null
        defaultProgramCohortShouldBeFound("isDefault.specified=true");

        // Get all the programCohortList where isDefault is null
        defaultProgramCohortShouldNotBeFound("isDefault.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortsByProgramIsEqualToSomething() throws Exception {
        // Initialize the database
        Program program = ProgramResourceIT.createEntity(em);
        em.persist(program);
        em.flush();
        programCohort.setProgram(program);
        programCohortRepository.saveAndFlush(programCohort);
        Long programId = program.getId();

        // Get all the programCohortList where program equals to programId
        defaultProgramCohortShouldBeFound("programId.equals=" + programId);

        // Get all the programCohortList where program equals to programId + 1
        defaultProgramCohortShouldNotBeFound("programId.equals=" + (programId + 1));
    }


    @Test
    @Transactional
    public void getAllProgramCohortsByProgramCohortRuleIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCohortRule programCohortRule = ProgramCohortRuleResourceIT.createEntity(em);
        em.persist(programCohortRule);
        em.flush();
        programCohort.addProgramCohortRule(programCohortRule);
        programCohortRepository.saveAndFlush(programCohort);
        Long programCohortRuleId = programCohortRule.getId();

        // Get all the programCohortList where programCohortRule equals to programCohortRuleId
        defaultProgramCohortShouldBeFound("programCohortRuleId.equals=" + programCohortRuleId);

        // Get all the programCohortList where programCohortRule equals to programCohortRuleId + 1
        defaultProgramCohortShouldNotBeFound("programCohortRuleId.equals=" + (programCohortRuleId + 1));
    }


    @Test
    @Transactional
    public void getAllProgramCohortsByProgramCohortCollectionIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCohortCollection programCohortCollection = ProgramCohortCollectionResourceIT.createEntity(em);
        em.persist(programCohortCollection);
        em.flush();
        programCohort.addProgramCohortCollection(programCohortCollection);
        programCohortRepository.saveAndFlush(programCohort);
        Long programCohortCollectionId = programCohortCollection.getId();

        // Get all the programCohortList where programCohortCollection equals to programCohortCollectionId
        defaultProgramCohortShouldBeFound("programCohortCollectionId.equals=" + programCohortCollectionId);

        // Get all the programCohortList where programCohortCollection equals to programCohortCollectionId + 1
        defaultProgramCohortShouldNotBeFound("programCohortCollectionId.equals=" + (programCohortCollectionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramCohortShouldBeFound(String filter) throws Exception {
        restProgramCohortMockMvc.perform(get("/api/program-cohorts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCohort.getId().intValue())))
            .andExpect(jsonPath("$.[*].cohortName").value(hasItem(DEFAULT_COHORT_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].isDefault").value(hasItem(DEFAULT_IS_DEFAULT.booleanValue())));

        // Check, that the count call also returns 1
        restProgramCohortMockMvc.perform(get("/api/program-cohorts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramCohortShouldNotBeFound(String filter) throws Exception {
        restProgramCohortMockMvc.perform(get("/api/program-cohorts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramCohortMockMvc.perform(get("/api/program-cohorts/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramCohort() throws Exception {
        // Get the programCohort
        restProgramCohortMockMvc.perform(get("/api/program-cohorts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramCohort() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        int databaseSizeBeforeUpdate = programCohortRepository.findAll().size();

        // Update the programCohort
        ProgramCohort updatedProgramCohort = programCohortRepository.findById(programCohort.getId()).get();
        // Disconnect from session so that the updates on updatedProgramCohort are not directly saved in db
        em.detach(updatedProgramCohort);
        updatedProgramCohort
            .cohortName(UPDATED_COHORT_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE)
            .isDefault(UPDATED_IS_DEFAULT);
        ProgramCohortDTO programCohortDTO = programCohortMapper.toDto(updatedProgramCohort);

        restProgramCohortMockMvc.perform(put("/api/program-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramCohort in the database
        List<ProgramCohort> programCohortList = programCohortRepository.findAll();
        assertThat(programCohortList).hasSize(databaseSizeBeforeUpdate);
        ProgramCohort testProgramCohort = programCohortList.get(programCohortList.size() - 1);
        assertThat(testProgramCohort.getCohortName()).isEqualTo(UPDATED_COHORT_NAME);
        assertThat(testProgramCohort.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramCohort.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
        assertThat(testProgramCohort.isIsDefault()).isEqualTo(UPDATED_IS_DEFAULT);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramCohort() throws Exception {
        int databaseSizeBeforeUpdate = programCohortRepository.findAll().size();

        // Create the ProgramCohort
        ProgramCohortDTO programCohortDTO = programCohortMapper.toDto(programCohort);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramCohortMockMvc.perform(put("/api/program-cohorts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCohort in the database
        List<ProgramCohort> programCohortList = programCohortRepository.findAll();
        assertThat(programCohortList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramCohort() throws Exception {
        // Initialize the database
        programCohortRepository.saveAndFlush(programCohort);

        int databaseSizeBeforeDelete = programCohortRepository.findAll().size();

        // Delete the programCohort
        restProgramCohortMockMvc.perform(delete("/api/program-cohorts/{id}", programCohort.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramCohort> programCohortList = programCohortRepository.findAll();
        assertThat(programCohortList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCohort.class);
        ProgramCohort programCohort1 = new ProgramCohort();
        programCohort1.setId(1L);
        ProgramCohort programCohort2 = new ProgramCohort();
        programCohort2.setId(programCohort1.getId());
        assertThat(programCohort1).isEqualTo(programCohort2);
        programCohort2.setId(2L);
        assertThat(programCohort1).isNotEqualTo(programCohort2);
        programCohort1.setId(null);
        assertThat(programCohort1).isNotEqualTo(programCohort2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCohortDTO.class);
        ProgramCohortDTO programCohortDTO1 = new ProgramCohortDTO();
        programCohortDTO1.setId(1L);
        ProgramCohortDTO programCohortDTO2 = new ProgramCohortDTO();
        assertThat(programCohortDTO1).isNotEqualTo(programCohortDTO2);
        programCohortDTO2.setId(programCohortDTO1.getId());
        assertThat(programCohortDTO1).isEqualTo(programCohortDTO2);
        programCohortDTO2.setId(2L);
        assertThat(programCohortDTO1).isNotEqualTo(programCohortDTO2);
        programCohortDTO1.setId(null);
        assertThat(programCohortDTO1).isNotEqualTo(programCohortDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programCohortMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programCohortMapper.fromId(null)).isNull();
    }
}
