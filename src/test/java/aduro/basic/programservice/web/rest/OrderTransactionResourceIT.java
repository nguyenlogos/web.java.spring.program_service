package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.OrderTransaction;
import aduro.basic.programservice.repository.OrderTransactionRepository;
import aduro.basic.programservice.service.OrderTransactionService;
import aduro.basic.programservice.service.dto.OrderTransactionDTO;
import aduro.basic.programservice.service.mapper.OrderTransactionMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.OrderTransactionCriteria;
import aduro.basic.programservice.service.OrderTransactionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link OrderTransactionResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class OrderTransactionResourceIT {

    private static final String DEFAULT_PROGRAM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PROGRAM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PARTICIPANT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANT_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_JSON_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_JSON_CONTENT = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_EXTERNAL_ORDER_ID = "AAAAAAAAAA";
    private static final String UPDATED_EXTERNAL_ORDER_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    private static final Long DEFAULT_USER_REWARD_ID = 1L;
    private static final Long UPDATED_USER_REWARD_ID = 2L;

    @Autowired
    private OrderTransactionRepository orderTransactionRepository;

    @Autowired
    private OrderTransactionMapper orderTransactionMapper;

    @Autowired
    private OrderTransactionService orderTransactionService;

    @Autowired
    private OrderTransactionQueryService orderTransactionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrderTransactionMockMvc;

    private OrderTransaction orderTransaction;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrderTransactionResource orderTransactionResource = new OrderTransactionResource(orderTransactionService, orderTransactionQueryService);
        this.restOrderTransactionMockMvc = MockMvcBuilders.standaloneSetup(orderTransactionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderTransaction createEntity(EntityManager em) {
        OrderTransaction orderTransaction = new OrderTransaction()
            .programName(DEFAULT_PROGRAM_NAME)
            .clientName(DEFAULT_CLIENT_NAME)
            .participantName(DEFAULT_PARTICIPANT_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .message(DEFAULT_MESSAGE)
            .jsonContent(DEFAULT_JSON_CONTENT)
            .email(DEFAULT_EMAIL)
            .externalOrderId(DEFAULT_EXTERNAL_ORDER_ID)
            .programId(DEFAULT_PROGRAM_ID)
            .userRewardId(DEFAULT_USER_REWARD_ID);
        return orderTransaction;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderTransaction createUpdatedEntity(EntityManager em) {
        OrderTransaction orderTransaction = new OrderTransaction()
            .programName(UPDATED_PROGRAM_NAME)
            .clientName(UPDATED_CLIENT_NAME)
            .participantName(UPDATED_PARTICIPANT_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .message(UPDATED_MESSAGE)
            .jsonContent(UPDATED_JSON_CONTENT)
            .email(UPDATED_EMAIL)
            .externalOrderId(UPDATED_EXTERNAL_ORDER_ID)
            .programId(UPDATED_PROGRAM_ID)
            .userRewardId(UPDATED_USER_REWARD_ID);
        return orderTransaction;
    }

    @BeforeEach
    public void initTest() {
        orderTransaction = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderTransaction() throws Exception {
        int databaseSizeBeforeCreate = orderTransactionRepository.findAll().size();

        // Create the OrderTransaction
        OrderTransactionDTO orderTransactionDTO = orderTransactionMapper.toDto(orderTransaction);
        restOrderTransactionMockMvc.perform(post("/api/order-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderTransactionDTO)))
            .andExpect(status().isCreated());

        // Validate the OrderTransaction in the database
        List<OrderTransaction> orderTransactionList = orderTransactionRepository.findAll();
        assertThat(orderTransactionList).hasSize(databaseSizeBeforeCreate + 1);
        OrderTransaction testOrderTransaction = orderTransactionList.get(orderTransactionList.size() - 1);
        assertThat(testOrderTransaction.getProgramName()).isEqualTo(DEFAULT_PROGRAM_NAME);
        assertThat(testOrderTransaction.getClientName()).isEqualTo(DEFAULT_CLIENT_NAME);
        assertThat(testOrderTransaction.getParticipantName()).isEqualTo(DEFAULT_PARTICIPANT_NAME);
        assertThat(testOrderTransaction.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testOrderTransaction.getMessage()).isEqualTo(DEFAULT_MESSAGE);
        assertThat(testOrderTransaction.getJsonContent()).isEqualTo(DEFAULT_JSON_CONTENT);
        assertThat(testOrderTransaction.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testOrderTransaction.getExternalOrderId()).isEqualTo(DEFAULT_EXTERNAL_ORDER_ID);
        assertThat(testOrderTransaction.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
        assertThat(testOrderTransaction.getUserRewardId()).isEqualTo(DEFAULT_USER_REWARD_ID);
    }

    @Test
    @Transactional
    public void createOrderTransactionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderTransactionRepository.findAll().size();

        // Create the OrderTransaction with an existing ID
        orderTransaction.setId(1L);
        OrderTransactionDTO orderTransactionDTO = orderTransactionMapper.toDto(orderTransaction);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderTransactionMockMvc.perform(post("/api/order-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrderTransaction in the database
        List<OrderTransaction> orderTransactionList = orderTransactionRepository.findAll();
        assertThat(orderTransactionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrderTransactions() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList
        restOrderTransactionMockMvc.perform(get("/api/order-transactions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].programName").value(hasItem(DEFAULT_PROGRAM_NAME.toString())))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].participantName").value(hasItem(DEFAULT_PARTICIPANT_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].jsonContent").value(hasItem(DEFAULT_JSON_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].externalOrderId").value(hasItem(DEFAULT_EXTERNAL_ORDER_ID.toString())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].userRewardId").value(hasItem(DEFAULT_USER_REWARD_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getOrderTransaction() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get the orderTransaction
        restOrderTransactionMockMvc.perform(get("/api/order-transactions/{id}", orderTransaction.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(orderTransaction.getId().intValue()))
            .andExpect(jsonPath("$.programName").value(DEFAULT_PROGRAM_NAME.toString()))
            .andExpect(jsonPath("$.clientName").value(DEFAULT_CLIENT_NAME.toString()))
            .andExpect(jsonPath("$.participantName").value(DEFAULT_PARTICIPANT_NAME.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.message").value(DEFAULT_MESSAGE.toString()))
            .andExpect(jsonPath("$.jsonContent").value(DEFAULT_JSON_CONTENT.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.externalOrderId").value(DEFAULT_EXTERNAL_ORDER_ID.toString()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()))
            .andExpect(jsonPath("$.userRewardId").value(DEFAULT_USER_REWARD_ID.intValue()));
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByProgramNameIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where programName equals to DEFAULT_PROGRAM_NAME
        defaultOrderTransactionShouldBeFound("programName.equals=" + DEFAULT_PROGRAM_NAME);

        // Get all the orderTransactionList where programName equals to UPDATED_PROGRAM_NAME
        defaultOrderTransactionShouldNotBeFound("programName.equals=" + UPDATED_PROGRAM_NAME);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByProgramNameIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where programName in DEFAULT_PROGRAM_NAME or UPDATED_PROGRAM_NAME
        defaultOrderTransactionShouldBeFound("programName.in=" + DEFAULT_PROGRAM_NAME + "," + UPDATED_PROGRAM_NAME);

        // Get all the orderTransactionList where programName equals to UPDATED_PROGRAM_NAME
        defaultOrderTransactionShouldNotBeFound("programName.in=" + UPDATED_PROGRAM_NAME);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByProgramNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where programName is not null
        defaultOrderTransactionShouldBeFound("programName.specified=true");

        // Get all the orderTransactionList where programName is null
        defaultOrderTransactionShouldNotBeFound("programName.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByClientNameIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where clientName equals to DEFAULT_CLIENT_NAME
        defaultOrderTransactionShouldBeFound("clientName.equals=" + DEFAULT_CLIENT_NAME);

        // Get all the orderTransactionList where clientName equals to UPDATED_CLIENT_NAME
        defaultOrderTransactionShouldNotBeFound("clientName.equals=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByClientNameIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where clientName in DEFAULT_CLIENT_NAME or UPDATED_CLIENT_NAME
        defaultOrderTransactionShouldBeFound("clientName.in=" + DEFAULT_CLIENT_NAME + "," + UPDATED_CLIENT_NAME);

        // Get all the orderTransactionList where clientName equals to UPDATED_CLIENT_NAME
        defaultOrderTransactionShouldNotBeFound("clientName.in=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByClientNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where clientName is not null
        defaultOrderTransactionShouldBeFound("clientName.specified=true");

        // Get all the orderTransactionList where clientName is null
        defaultOrderTransactionShouldNotBeFound("clientName.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByParticipantNameIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where participantName equals to DEFAULT_PARTICIPANT_NAME
        defaultOrderTransactionShouldBeFound("participantName.equals=" + DEFAULT_PARTICIPANT_NAME);

        // Get all the orderTransactionList where participantName equals to UPDATED_PARTICIPANT_NAME
        defaultOrderTransactionShouldNotBeFound("participantName.equals=" + UPDATED_PARTICIPANT_NAME);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByParticipantNameIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where participantName in DEFAULT_PARTICIPANT_NAME or UPDATED_PARTICIPANT_NAME
        defaultOrderTransactionShouldBeFound("participantName.in=" + DEFAULT_PARTICIPANT_NAME + "," + UPDATED_PARTICIPANT_NAME);

        // Get all the orderTransactionList where participantName equals to UPDATED_PARTICIPANT_NAME
        defaultOrderTransactionShouldNotBeFound("participantName.in=" + UPDATED_PARTICIPANT_NAME);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByParticipantNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where participantName is not null
        defaultOrderTransactionShouldBeFound("participantName.specified=true");

        // Get all the orderTransactionList where participantName is null
        defaultOrderTransactionShouldNotBeFound("participantName.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where createdDate equals to DEFAULT_CREATED_DATE
        defaultOrderTransactionShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the orderTransactionList where createdDate equals to UPDATED_CREATED_DATE
        defaultOrderTransactionShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultOrderTransactionShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the orderTransactionList where createdDate equals to UPDATED_CREATED_DATE
        defaultOrderTransactionShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where createdDate is not null
        defaultOrderTransactionShouldBeFound("createdDate.specified=true");

        // Get all the orderTransactionList where createdDate is null
        defaultOrderTransactionShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where message equals to DEFAULT_MESSAGE
        defaultOrderTransactionShouldBeFound("message.equals=" + DEFAULT_MESSAGE);

        // Get all the orderTransactionList where message equals to UPDATED_MESSAGE
        defaultOrderTransactionShouldNotBeFound("message.equals=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByMessageIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where message in DEFAULT_MESSAGE or UPDATED_MESSAGE
        defaultOrderTransactionShouldBeFound("message.in=" + DEFAULT_MESSAGE + "," + UPDATED_MESSAGE);

        // Get all the orderTransactionList where message equals to UPDATED_MESSAGE
        defaultOrderTransactionShouldNotBeFound("message.in=" + UPDATED_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where message is not null
        defaultOrderTransactionShouldBeFound("message.specified=true");

        // Get all the orderTransactionList where message is null
        defaultOrderTransactionShouldNotBeFound("message.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByJsonContentIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where jsonContent equals to DEFAULT_JSON_CONTENT
        defaultOrderTransactionShouldBeFound("jsonContent.equals=" + DEFAULT_JSON_CONTENT);

        // Get all the orderTransactionList where jsonContent equals to UPDATED_JSON_CONTENT
        defaultOrderTransactionShouldNotBeFound("jsonContent.equals=" + UPDATED_JSON_CONTENT);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByJsonContentIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where jsonContent in DEFAULT_JSON_CONTENT or UPDATED_JSON_CONTENT
        defaultOrderTransactionShouldBeFound("jsonContent.in=" + DEFAULT_JSON_CONTENT + "," + UPDATED_JSON_CONTENT);

        // Get all the orderTransactionList where jsonContent equals to UPDATED_JSON_CONTENT
        defaultOrderTransactionShouldNotBeFound("jsonContent.in=" + UPDATED_JSON_CONTENT);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByJsonContentIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where jsonContent is not null
        defaultOrderTransactionShouldBeFound("jsonContent.specified=true");

        // Get all the orderTransactionList where jsonContent is null
        defaultOrderTransactionShouldNotBeFound("jsonContent.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where email equals to DEFAULT_EMAIL
        defaultOrderTransactionShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the orderTransactionList where email equals to UPDATED_EMAIL
        defaultOrderTransactionShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultOrderTransactionShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the orderTransactionList where email equals to UPDATED_EMAIL
        defaultOrderTransactionShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where email is not null
        defaultOrderTransactionShouldBeFound("email.specified=true");

        // Get all the orderTransactionList where email is null
        defaultOrderTransactionShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByExternalOrderIdIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where externalOrderId equals to DEFAULT_EXTERNAL_ORDER_ID
        defaultOrderTransactionShouldBeFound("externalOrderId.equals=" + DEFAULT_EXTERNAL_ORDER_ID);

        // Get all the orderTransactionList where externalOrderId equals to UPDATED_EXTERNAL_ORDER_ID
        defaultOrderTransactionShouldNotBeFound("externalOrderId.equals=" + UPDATED_EXTERNAL_ORDER_ID);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByExternalOrderIdIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where externalOrderId in DEFAULT_EXTERNAL_ORDER_ID or UPDATED_EXTERNAL_ORDER_ID
        defaultOrderTransactionShouldBeFound("externalOrderId.in=" + DEFAULT_EXTERNAL_ORDER_ID + "," + UPDATED_EXTERNAL_ORDER_ID);

        // Get all the orderTransactionList where externalOrderId equals to UPDATED_EXTERNAL_ORDER_ID
        defaultOrderTransactionShouldNotBeFound("externalOrderId.in=" + UPDATED_EXTERNAL_ORDER_ID);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByExternalOrderIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where externalOrderId is not null
        defaultOrderTransactionShouldBeFound("externalOrderId.specified=true");

        // Get all the orderTransactionList where externalOrderId is null
        defaultOrderTransactionShouldNotBeFound("externalOrderId.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByProgramIdIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where programId equals to DEFAULT_PROGRAM_ID
        defaultOrderTransactionShouldBeFound("programId.equals=" + DEFAULT_PROGRAM_ID);

        // Get all the orderTransactionList where programId equals to UPDATED_PROGRAM_ID
        defaultOrderTransactionShouldNotBeFound("programId.equals=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByProgramIdIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where programId in DEFAULT_PROGRAM_ID or UPDATED_PROGRAM_ID
        defaultOrderTransactionShouldBeFound("programId.in=" + DEFAULT_PROGRAM_ID + "," + UPDATED_PROGRAM_ID);

        // Get all the orderTransactionList where programId equals to UPDATED_PROGRAM_ID
        defaultOrderTransactionShouldNotBeFound("programId.in=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByProgramIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where programId is not null
        defaultOrderTransactionShouldBeFound("programId.specified=true");

        // Get all the orderTransactionList where programId is null
        defaultOrderTransactionShouldNotBeFound("programId.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByProgramIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where programId greater than or equals to DEFAULT_PROGRAM_ID
        defaultOrderTransactionShouldBeFound("programId.greaterOrEqualThan=" + DEFAULT_PROGRAM_ID);

        // Get all the orderTransactionList where programId greater than or equals to UPDATED_PROGRAM_ID
        defaultOrderTransactionShouldNotBeFound("programId.greaterOrEqualThan=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByProgramIdIsLessThanSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where programId less than or equals to DEFAULT_PROGRAM_ID
        defaultOrderTransactionShouldNotBeFound("programId.lessThan=" + DEFAULT_PROGRAM_ID);

        // Get all the orderTransactionList where programId less than or equals to UPDATED_PROGRAM_ID
        defaultOrderTransactionShouldBeFound("programId.lessThan=" + UPDATED_PROGRAM_ID);
    }


    @Test
    @Transactional
    public void getAllOrderTransactionsByUserRewardIdIsEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where userRewardId equals to DEFAULT_USER_REWARD_ID
        defaultOrderTransactionShouldBeFound("userRewardId.equals=" + DEFAULT_USER_REWARD_ID);

        // Get all the orderTransactionList where userRewardId equals to UPDATED_USER_REWARD_ID
        defaultOrderTransactionShouldNotBeFound("userRewardId.equals=" + UPDATED_USER_REWARD_ID);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByUserRewardIdIsInShouldWork() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where userRewardId in DEFAULT_USER_REWARD_ID or UPDATED_USER_REWARD_ID
        defaultOrderTransactionShouldBeFound("userRewardId.in=" + DEFAULT_USER_REWARD_ID + "," + UPDATED_USER_REWARD_ID);

        // Get all the orderTransactionList where userRewardId equals to UPDATED_USER_REWARD_ID
        defaultOrderTransactionShouldNotBeFound("userRewardId.in=" + UPDATED_USER_REWARD_ID);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByUserRewardIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where userRewardId is not null
        defaultOrderTransactionShouldBeFound("userRewardId.specified=true");

        // Get all the orderTransactionList where userRewardId is null
        defaultOrderTransactionShouldNotBeFound("userRewardId.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByUserRewardIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where userRewardId greater than or equals to DEFAULT_USER_REWARD_ID
        defaultOrderTransactionShouldBeFound("userRewardId.greaterOrEqualThan=" + DEFAULT_USER_REWARD_ID);

        // Get all the orderTransactionList where userRewardId greater than or equals to UPDATED_USER_REWARD_ID
        defaultOrderTransactionShouldNotBeFound("userRewardId.greaterOrEqualThan=" + UPDATED_USER_REWARD_ID);
    }

    @Test
    @Transactional
    public void getAllOrderTransactionsByUserRewardIdIsLessThanSomething() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        // Get all the orderTransactionList where userRewardId less than or equals to DEFAULT_USER_REWARD_ID
        defaultOrderTransactionShouldNotBeFound("userRewardId.lessThan=" + DEFAULT_USER_REWARD_ID);

        // Get all the orderTransactionList where userRewardId less than or equals to UPDATED_USER_REWARD_ID
        defaultOrderTransactionShouldBeFound("userRewardId.lessThan=" + UPDATED_USER_REWARD_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOrderTransactionShouldBeFound(String filter) throws Exception {
        restOrderTransactionMockMvc.perform(get("/api/order-transactions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderTransaction.getId().intValue())))
            .andExpect(jsonPath("$.[*].programName").value(hasItem(DEFAULT_PROGRAM_NAME)))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME)))
            .andExpect(jsonPath("$.[*].participantName").value(hasItem(DEFAULT_PARTICIPANT_NAME)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].message").value(hasItem(DEFAULT_MESSAGE)))
            .andExpect(jsonPath("$.[*].jsonContent").value(hasItem(DEFAULT_JSON_CONTENT)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].externalOrderId").value(hasItem(DEFAULT_EXTERNAL_ORDER_ID)))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].userRewardId").value(hasItem(DEFAULT_USER_REWARD_ID.intValue())));

        // Check, that the count call also returns 1
        restOrderTransactionMockMvc.perform(get("/api/order-transactions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOrderTransactionShouldNotBeFound(String filter) throws Exception {
        restOrderTransactionMockMvc.perform(get("/api/order-transactions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOrderTransactionMockMvc.perform(get("/api/order-transactions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingOrderTransaction() throws Exception {
        // Get the orderTransaction
        restOrderTransactionMockMvc.perform(get("/api/order-transactions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderTransaction() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        int databaseSizeBeforeUpdate = orderTransactionRepository.findAll().size();

        // Update the orderTransaction
        OrderTransaction updatedOrderTransaction = orderTransactionRepository.findById(orderTransaction.getId()).get();
        // Disconnect from session so that the updates on updatedOrderTransaction are not directly saved in db
        em.detach(updatedOrderTransaction);
        updatedOrderTransaction
            .programName(UPDATED_PROGRAM_NAME)
            .clientName(UPDATED_CLIENT_NAME)
            .participantName(UPDATED_PARTICIPANT_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .message(UPDATED_MESSAGE)
            .jsonContent(UPDATED_JSON_CONTENT)
            .email(UPDATED_EMAIL)
            .externalOrderId(UPDATED_EXTERNAL_ORDER_ID)
            .programId(UPDATED_PROGRAM_ID)
            .userRewardId(UPDATED_USER_REWARD_ID);
        OrderTransactionDTO orderTransactionDTO = orderTransactionMapper.toDto(updatedOrderTransaction);

        restOrderTransactionMockMvc.perform(put("/api/order-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderTransactionDTO)))
            .andExpect(status().isOk());

        // Validate the OrderTransaction in the database
        List<OrderTransaction> orderTransactionList = orderTransactionRepository.findAll();
        assertThat(orderTransactionList).hasSize(databaseSizeBeforeUpdate);
        OrderTransaction testOrderTransaction = orderTransactionList.get(orderTransactionList.size() - 1);
        assertThat(testOrderTransaction.getProgramName()).isEqualTo(UPDATED_PROGRAM_NAME);
        assertThat(testOrderTransaction.getClientName()).isEqualTo(UPDATED_CLIENT_NAME);
        assertThat(testOrderTransaction.getParticipantName()).isEqualTo(UPDATED_PARTICIPANT_NAME);
        assertThat(testOrderTransaction.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOrderTransaction.getMessage()).isEqualTo(UPDATED_MESSAGE);
        assertThat(testOrderTransaction.getJsonContent()).isEqualTo(UPDATED_JSON_CONTENT);
        assertThat(testOrderTransaction.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testOrderTransaction.getExternalOrderId()).isEqualTo(UPDATED_EXTERNAL_ORDER_ID);
        assertThat(testOrderTransaction.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
        assertThat(testOrderTransaction.getUserRewardId()).isEqualTo(UPDATED_USER_REWARD_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderTransaction() throws Exception {
        int databaseSizeBeforeUpdate = orderTransactionRepository.findAll().size();

        // Create the OrderTransaction
        OrderTransactionDTO orderTransactionDTO = orderTransactionMapper.toDto(orderTransaction);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderTransactionMockMvc.perform(put("/api/order-transactions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderTransactionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrderTransaction in the database
        List<OrderTransaction> orderTransactionList = orderTransactionRepository.findAll();
        assertThat(orderTransactionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrderTransaction() throws Exception {
        // Initialize the database
        orderTransactionRepository.saveAndFlush(orderTransaction);

        int databaseSizeBeforeDelete = orderTransactionRepository.findAll().size();

        // Delete the orderTransaction
        restOrderTransactionMockMvc.perform(delete("/api/order-transactions/{id}", orderTransaction.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<OrderTransaction> orderTransactionList = orderTransactionRepository.findAll();
        assertThat(orderTransactionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderTransaction.class);
        OrderTransaction orderTransaction1 = new OrderTransaction();
        orderTransaction1.setId(1L);
        OrderTransaction orderTransaction2 = new OrderTransaction();
        orderTransaction2.setId(orderTransaction1.getId());
        assertThat(orderTransaction1).isEqualTo(orderTransaction2);
        orderTransaction2.setId(2L);
        assertThat(orderTransaction1).isNotEqualTo(orderTransaction2);
        orderTransaction1.setId(null);
        assertThat(orderTransaction1).isNotEqualTo(orderTransaction2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderTransactionDTO.class);
        OrderTransactionDTO orderTransactionDTO1 = new OrderTransactionDTO();
        orderTransactionDTO1.setId(1L);
        OrderTransactionDTO orderTransactionDTO2 = new OrderTransactionDTO();
        assertThat(orderTransactionDTO1).isNotEqualTo(orderTransactionDTO2);
        orderTransactionDTO2.setId(orderTransactionDTO1.getId());
        assertThat(orderTransactionDTO1).isEqualTo(orderTransactionDTO2);
        orderTransactionDTO2.setId(2L);
        assertThat(orderTransactionDTO1).isNotEqualTo(orderTransactionDTO2);
        orderTransactionDTO1.setId(null);
        assertThat(orderTransactionDTO1).isNotEqualTo(orderTransactionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(orderTransactionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(orderTransactionMapper.fromId(null)).isNull();
    }
}
