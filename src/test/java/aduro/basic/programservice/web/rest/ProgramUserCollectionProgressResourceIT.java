package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramUserCollectionProgress;
import aduro.basic.programservice.domain.ProgramUserCohort;
import aduro.basic.programservice.repository.ProgramUserCollectionProgressRepository;
import aduro.basic.programservice.service.ProgramUserCollectionProgressService;
import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressDTO;
import aduro.basic.programservice.service.mapper.ProgramUserCollectionProgressMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramUserCollectionProgressCriteria;
import aduro.basic.programservice.service.ProgramUserCollectionProgressQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import aduro.basic.programservice.domain.enumeration.CollectionStatus;
/**
 * Integration tests for the {@Link ProgramUserCollectionProgressResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramUserCollectionProgressResourceIT {

    private static final Long DEFAULT_COLLECTION_ID = 1L;
    private static final Long UPDATED_COLLECTION_ID = 2L;

    private static final Integer DEFAULT_TOTAL_REQUIRED_ITEMS = 1;
    private static final Integer UPDATED_TOTAL_REQUIRED_ITEMS = 2;

    private static final Long DEFAULT_CURRENT_PROGRESS = 1L;
    private static final Long UPDATED_CURRENT_PROGRESS = 2L;

    private static final CollectionStatus DEFAULT_STATUS = CollectionStatus.DOING;
    private static final CollectionStatus UPDATED_STATUS = CollectionStatus.DONE;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramUserCollectionProgressRepository programUserCollectionProgressRepository;

    @Autowired
    private ProgramUserCollectionProgressMapper programUserCollectionProgressMapper;

    @Autowired
    private ProgramUserCollectionProgressService programUserCollectionProgressService;

    @Autowired
    private ProgramUserCollectionProgressQueryService programUserCollectionProgressQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramUserCollectionProgressMockMvc;

    private ProgramUserCollectionProgress programUserCollectionProgress;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramUserCollectionProgressResource programUserCollectionProgressResource = new ProgramUserCollectionProgressResource(programUserCollectionProgressService, programUserCollectionProgressQueryService);
        this.restProgramUserCollectionProgressMockMvc = MockMvcBuilders.standaloneSetup(programUserCollectionProgressResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUserCollectionProgress createEntity(EntityManager em) {
        ProgramUserCollectionProgress programUserCollectionProgress = new ProgramUserCollectionProgress()
            .collectionId(DEFAULT_COLLECTION_ID)
            .totalRequiredItems(DEFAULT_TOTAL_REQUIRED_ITEMS)
            .currentProgress(DEFAULT_CURRENT_PROGRESS)
            .status(DEFAULT_STATUS)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        // Add required entity
        ProgramUserCohort programUserCohort;
        if (TestUtil.findAll(em, ProgramUserCohort.class).isEmpty()) {
            programUserCohort = ProgramUserCohortResourceIT.createEntity(em);
            em.persist(programUserCohort);
            em.flush();
        } else {
            programUserCohort = TestUtil.findAll(em, ProgramUserCohort.class).get(0);
        }
        programUserCollectionProgress.setProgramUserCohort(programUserCohort);
        return programUserCollectionProgress;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramUserCollectionProgress createUpdatedEntity(EntityManager em) {
        ProgramUserCollectionProgress programUserCollectionProgress = new ProgramUserCollectionProgress()
            .collectionId(UPDATED_COLLECTION_ID)
            .totalRequiredItems(UPDATED_TOTAL_REQUIRED_ITEMS)
            .currentProgress(UPDATED_CURRENT_PROGRESS)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        // Add required entity
        ProgramUserCohort programUserCohort;
        if (TestUtil.findAll(em, ProgramUserCohort.class).isEmpty()) {
            programUserCohort = ProgramUserCohortResourceIT.createUpdatedEntity(em);
            em.persist(programUserCohort);
            em.flush();
        } else {
            programUserCohort = TestUtil.findAll(em, ProgramUserCohort.class).get(0);
        }
        programUserCollectionProgress.setProgramUserCohort(programUserCohort);
        return programUserCollectionProgress;
    }

    @BeforeEach
    public void initTest() {
        programUserCollectionProgress = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramUserCollectionProgress() throws Exception {
        int databaseSizeBeforeCreate = programUserCollectionProgressRepository.findAll().size();

        // Create the ProgramUserCollectionProgress
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = programUserCollectionProgressMapper.toDto(programUserCollectionProgress);
        restProgramUserCollectionProgressMockMvc.perform(post("/api/program-user-collection-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCollectionProgressDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramUserCollectionProgress in the database
        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramUserCollectionProgress testProgramUserCollectionProgress = programUserCollectionProgressList.get(programUserCollectionProgressList.size() - 1);
        assertThat(testProgramUserCollectionProgress.getCollectionId()).isEqualTo(DEFAULT_COLLECTION_ID);
        assertThat(testProgramUserCollectionProgress.getTotalRequiredItems()).isEqualTo(DEFAULT_TOTAL_REQUIRED_ITEMS);
        assertThat(testProgramUserCollectionProgress.getCurrentProgress()).isEqualTo(DEFAULT_CURRENT_PROGRESS);
        assertThat(testProgramUserCollectionProgress.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testProgramUserCollectionProgress.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramUserCollectionProgress.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramUserCollectionProgressWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programUserCollectionProgressRepository.findAll().size();

        // Create the ProgramUserCollectionProgress with an existing ID
        programUserCollectionProgress.setId(1L);
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = programUserCollectionProgressMapper.toDto(programUserCollectionProgress);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramUserCollectionProgressMockMvc.perform(post("/api/program-user-collection-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCollectionProgressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUserCollectionProgress in the database
        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCollectionIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserCollectionProgressRepository.findAll().size();
        // set the field null
        programUserCollectionProgress.setCollectionId(null);

        // Create the ProgramUserCollectionProgress, which fails.
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = programUserCollectionProgressMapper.toDto(programUserCollectionProgress);

        restProgramUserCollectionProgressMockMvc.perform(post("/api/program-user-collection-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCollectionProgressDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTotalRequiredItemsIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserCollectionProgressRepository.findAll().size();
        // set the field null
        programUserCollectionProgress.setTotalRequiredItems(null);

        // Create the ProgramUserCollectionProgress, which fails.
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = programUserCollectionProgressMapper.toDto(programUserCollectionProgress);

        restProgramUserCollectionProgressMockMvc.perform(post("/api/program-user-collection-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCollectionProgressDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserCollectionProgressRepository.findAll().size();
        // set the field null
        programUserCollectionProgress.setCreatedDate(null);

        // Create the ProgramUserCollectionProgress, which fails.
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = programUserCollectionProgressMapper.toDto(programUserCollectionProgress);

        restProgramUserCollectionProgressMockMvc.perform(post("/api/program-user-collection-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCollectionProgressDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModifiedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programUserCollectionProgressRepository.findAll().size();
        // set the field null
        programUserCollectionProgress.setModifiedDate(null);

        // Create the ProgramUserCollectionProgress, which fails.
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = programUserCollectionProgressMapper.toDto(programUserCollectionProgress);

        restProgramUserCollectionProgressMockMvc.perform(post("/api/program-user-collection-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCollectionProgressDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgresses() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList
        restProgramUserCollectionProgressMockMvc.perform(get("/api/program-user-collection-progresses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUserCollectionProgress.getId().intValue())))
            .andExpect(jsonPath("$.[*].collectionId").value(hasItem(DEFAULT_COLLECTION_ID.intValue())))
            .andExpect(jsonPath("$.[*].totalRequiredItems").value(hasItem(DEFAULT_TOTAL_REQUIRED_ITEMS)))
            .andExpect(jsonPath("$.[*].currentProgress").value(hasItem(DEFAULT_CURRENT_PROGRESS.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramUserCollectionProgress() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get the programUserCollectionProgress
        restProgramUserCollectionProgressMockMvc.perform(get("/api/program-user-collection-progresses/{id}", programUserCollectionProgress.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programUserCollectionProgress.getId().intValue()))
            .andExpect(jsonPath("$.collectionId").value(DEFAULT_COLLECTION_ID.intValue()))
            .andExpect(jsonPath("$.totalRequiredItems").value(DEFAULT_TOTAL_REQUIRED_ITEMS))
            .andExpect(jsonPath("$.currentProgress").value(DEFAULT_CURRENT_PROGRESS.intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCollectionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where collectionId equals to DEFAULT_COLLECTION_ID
        defaultProgramUserCollectionProgressShouldBeFound("collectionId.equals=" + DEFAULT_COLLECTION_ID);

        // Get all the programUserCollectionProgressList where collectionId equals to UPDATED_COLLECTION_ID
        defaultProgramUserCollectionProgressShouldNotBeFound("collectionId.equals=" + UPDATED_COLLECTION_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCollectionIdIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where collectionId in DEFAULT_COLLECTION_ID or UPDATED_COLLECTION_ID
        defaultProgramUserCollectionProgressShouldBeFound("collectionId.in=" + DEFAULT_COLLECTION_ID + "," + UPDATED_COLLECTION_ID);

        // Get all the programUserCollectionProgressList where collectionId equals to UPDATED_COLLECTION_ID
        defaultProgramUserCollectionProgressShouldNotBeFound("collectionId.in=" + UPDATED_COLLECTION_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCollectionIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where collectionId is not null
        defaultProgramUserCollectionProgressShouldBeFound("collectionId.specified=true");

        // Get all the programUserCollectionProgressList where collectionId is null
        defaultProgramUserCollectionProgressShouldNotBeFound("collectionId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCollectionIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where collectionId greater than or equals to DEFAULT_COLLECTION_ID
        defaultProgramUserCollectionProgressShouldBeFound("collectionId.greaterOrEqualThan=" + DEFAULT_COLLECTION_ID);

        // Get all the programUserCollectionProgressList where collectionId greater than or equals to UPDATED_COLLECTION_ID
        defaultProgramUserCollectionProgressShouldNotBeFound("collectionId.greaterOrEqualThan=" + UPDATED_COLLECTION_ID);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCollectionIdIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where collectionId less than or equals to DEFAULT_COLLECTION_ID
        defaultProgramUserCollectionProgressShouldNotBeFound("collectionId.lessThan=" + DEFAULT_COLLECTION_ID);

        // Get all the programUserCollectionProgressList where collectionId less than or equals to UPDATED_COLLECTION_ID
        defaultProgramUserCollectionProgressShouldBeFound("collectionId.lessThan=" + UPDATED_COLLECTION_ID);
    }


    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByTotalRequiredItemsIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where totalRequiredItems equals to DEFAULT_TOTAL_REQUIRED_ITEMS
        defaultProgramUserCollectionProgressShouldBeFound("totalRequiredItems.equals=" + DEFAULT_TOTAL_REQUIRED_ITEMS);

        // Get all the programUserCollectionProgressList where totalRequiredItems equals to UPDATED_TOTAL_REQUIRED_ITEMS
        defaultProgramUserCollectionProgressShouldNotBeFound("totalRequiredItems.equals=" + UPDATED_TOTAL_REQUIRED_ITEMS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByTotalRequiredItemsIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where totalRequiredItems in DEFAULT_TOTAL_REQUIRED_ITEMS or UPDATED_TOTAL_REQUIRED_ITEMS
        defaultProgramUserCollectionProgressShouldBeFound("totalRequiredItems.in=" + DEFAULT_TOTAL_REQUIRED_ITEMS + "," + UPDATED_TOTAL_REQUIRED_ITEMS);

        // Get all the programUserCollectionProgressList where totalRequiredItems equals to UPDATED_TOTAL_REQUIRED_ITEMS
        defaultProgramUserCollectionProgressShouldNotBeFound("totalRequiredItems.in=" + UPDATED_TOTAL_REQUIRED_ITEMS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByTotalRequiredItemsIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where totalRequiredItems is not null
        defaultProgramUserCollectionProgressShouldBeFound("totalRequiredItems.specified=true");

        // Get all the programUserCollectionProgressList where totalRequiredItems is null
        defaultProgramUserCollectionProgressShouldNotBeFound("totalRequiredItems.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByTotalRequiredItemsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where totalRequiredItems greater than or equals to DEFAULT_TOTAL_REQUIRED_ITEMS
        defaultProgramUserCollectionProgressShouldBeFound("totalRequiredItems.greaterOrEqualThan=" + DEFAULT_TOTAL_REQUIRED_ITEMS);

        // Get all the programUserCollectionProgressList where totalRequiredItems greater than or equals to UPDATED_TOTAL_REQUIRED_ITEMS
        defaultProgramUserCollectionProgressShouldNotBeFound("totalRequiredItems.greaterOrEqualThan=" + UPDATED_TOTAL_REQUIRED_ITEMS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByTotalRequiredItemsIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where totalRequiredItems less than or equals to DEFAULT_TOTAL_REQUIRED_ITEMS
        defaultProgramUserCollectionProgressShouldNotBeFound("totalRequiredItems.lessThan=" + DEFAULT_TOTAL_REQUIRED_ITEMS);

        // Get all the programUserCollectionProgressList where totalRequiredItems less than or equals to UPDATED_TOTAL_REQUIRED_ITEMS
        defaultProgramUserCollectionProgressShouldBeFound("totalRequiredItems.lessThan=" + UPDATED_TOTAL_REQUIRED_ITEMS);
    }


    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCurrentProgressIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where currentProgress equals to DEFAULT_CURRENT_PROGRESS
        defaultProgramUserCollectionProgressShouldBeFound("currentProgress.equals=" + DEFAULT_CURRENT_PROGRESS);

        // Get all the programUserCollectionProgressList where currentProgress equals to UPDATED_CURRENT_PROGRESS
        defaultProgramUserCollectionProgressShouldNotBeFound("currentProgress.equals=" + UPDATED_CURRENT_PROGRESS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCurrentProgressIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where currentProgress in DEFAULT_CURRENT_PROGRESS or UPDATED_CURRENT_PROGRESS
        defaultProgramUserCollectionProgressShouldBeFound("currentProgress.in=" + DEFAULT_CURRENT_PROGRESS + "," + UPDATED_CURRENT_PROGRESS);

        // Get all the programUserCollectionProgressList where currentProgress equals to UPDATED_CURRENT_PROGRESS
        defaultProgramUserCollectionProgressShouldNotBeFound("currentProgress.in=" + UPDATED_CURRENT_PROGRESS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCurrentProgressIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where currentProgress is not null
        defaultProgramUserCollectionProgressShouldBeFound("currentProgress.specified=true");

        // Get all the programUserCollectionProgressList where currentProgress is null
        defaultProgramUserCollectionProgressShouldNotBeFound("currentProgress.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCurrentProgressIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where currentProgress greater than or equals to DEFAULT_CURRENT_PROGRESS
        defaultProgramUserCollectionProgressShouldBeFound("currentProgress.greaterOrEqualThan=" + DEFAULT_CURRENT_PROGRESS);

        // Get all the programUserCollectionProgressList where currentProgress greater than or equals to UPDATED_CURRENT_PROGRESS
        defaultProgramUserCollectionProgressShouldNotBeFound("currentProgress.greaterOrEqualThan=" + UPDATED_CURRENT_PROGRESS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCurrentProgressIsLessThanSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where currentProgress less than or equals to DEFAULT_CURRENT_PROGRESS
        defaultProgramUserCollectionProgressShouldNotBeFound("currentProgress.lessThan=" + DEFAULT_CURRENT_PROGRESS);

        // Get all the programUserCollectionProgressList where currentProgress less than or equals to UPDATED_CURRENT_PROGRESS
        defaultProgramUserCollectionProgressShouldBeFound("currentProgress.lessThan=" + UPDATED_CURRENT_PROGRESS);
    }


    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where status equals to DEFAULT_STATUS
        defaultProgramUserCollectionProgressShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the programUserCollectionProgressList where status equals to UPDATED_STATUS
        defaultProgramUserCollectionProgressShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultProgramUserCollectionProgressShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the programUserCollectionProgressList where status equals to UPDATED_STATUS
        defaultProgramUserCollectionProgressShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where status is not null
        defaultProgramUserCollectionProgressShouldBeFound("status.specified=true");

        // Get all the programUserCollectionProgressList where status is null
        defaultProgramUserCollectionProgressShouldNotBeFound("status.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramUserCollectionProgressShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programUserCollectionProgressList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramUserCollectionProgressShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramUserCollectionProgressShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programUserCollectionProgressList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramUserCollectionProgressShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where createdDate is not null
        defaultProgramUserCollectionProgressShouldBeFound("createdDate.specified=true");

        // Get all the programUserCollectionProgressList where createdDate is null
        defaultProgramUserCollectionProgressShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramUserCollectionProgressShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programUserCollectionProgressList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramUserCollectionProgressShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramUserCollectionProgressShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programUserCollectionProgressList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramUserCollectionProgressShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        // Get all the programUserCollectionProgressList where modifiedDate is not null
        defaultProgramUserCollectionProgressShouldBeFound("modifiedDate.specified=true");

        // Get all the programUserCollectionProgressList where modifiedDate is null
        defaultProgramUserCollectionProgressShouldNotBeFound("modifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramUserCollectionProgressesByProgramUserCohortIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramUserCohort programUserCohort = programUserCollectionProgress.getProgramUserCohort();
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);
        Long programUserCohortId = programUserCohort.getId();

        // Get all the programUserCollectionProgressList where programUserCohort equals to programUserCohortId
        defaultProgramUserCollectionProgressShouldBeFound("programUserCohortId.equals=" + programUserCohortId);

        // Get all the programUserCollectionProgressList where programUserCohort equals to programUserCohortId + 1
        defaultProgramUserCollectionProgressShouldNotBeFound("programUserCohortId.equals=" + (programUserCohortId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramUserCollectionProgressShouldBeFound(String filter) throws Exception {
        restProgramUserCollectionProgressMockMvc.perform(get("/api/program-user-collection-progresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programUserCollectionProgress.getId().intValue())))
            .andExpect(jsonPath("$.[*].collectionId").value(hasItem(DEFAULT_COLLECTION_ID.intValue())))
            .andExpect(jsonPath("$.[*].totalRequiredItems").value(hasItem(DEFAULT_TOTAL_REQUIRED_ITEMS)))
            .andExpect(jsonPath("$.[*].currentProgress").value(hasItem(DEFAULT_CURRENT_PROGRESS.intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramUserCollectionProgressMockMvc.perform(get("/api/program-user-collection-progresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramUserCollectionProgressShouldNotBeFound(String filter) throws Exception {
        restProgramUserCollectionProgressMockMvc.perform(get("/api/program-user-collection-progresses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramUserCollectionProgressMockMvc.perform(get("/api/program-user-collection-progresses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramUserCollectionProgress() throws Exception {
        // Get the programUserCollectionProgress
        restProgramUserCollectionProgressMockMvc.perform(get("/api/program-user-collection-progresses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramUserCollectionProgress() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        int databaseSizeBeforeUpdate = programUserCollectionProgressRepository.findAll().size();

        // Update the programUserCollectionProgress
        ProgramUserCollectionProgress updatedProgramUserCollectionProgress = programUserCollectionProgressRepository.findById(programUserCollectionProgress.getId()).get();
        // Disconnect from session so that the updates on updatedProgramUserCollectionProgress are not directly saved in db
        em.detach(updatedProgramUserCollectionProgress);
        updatedProgramUserCollectionProgress
            .collectionId(UPDATED_COLLECTION_ID)
            .totalRequiredItems(UPDATED_TOTAL_REQUIRED_ITEMS)
            .currentProgress(UPDATED_CURRENT_PROGRESS)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = programUserCollectionProgressMapper.toDto(updatedProgramUserCollectionProgress);

        restProgramUserCollectionProgressMockMvc.perform(put("/api/program-user-collection-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCollectionProgressDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramUserCollectionProgress in the database
        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeUpdate);
        ProgramUserCollectionProgress testProgramUserCollectionProgress = programUserCollectionProgressList.get(programUserCollectionProgressList.size() - 1);
        assertThat(testProgramUserCollectionProgress.getCollectionId()).isEqualTo(UPDATED_COLLECTION_ID);
        assertThat(testProgramUserCollectionProgress.getTotalRequiredItems()).isEqualTo(UPDATED_TOTAL_REQUIRED_ITEMS);
        assertThat(testProgramUserCollectionProgress.getCurrentProgress()).isEqualTo(UPDATED_CURRENT_PROGRESS);
        assertThat(testProgramUserCollectionProgress.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testProgramUserCollectionProgress.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramUserCollectionProgress.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramUserCollectionProgress() throws Exception {
        int databaseSizeBeforeUpdate = programUserCollectionProgressRepository.findAll().size();

        // Create the ProgramUserCollectionProgress
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO = programUserCollectionProgressMapper.toDto(programUserCollectionProgress);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramUserCollectionProgressMockMvc.perform(put("/api/program-user-collection-progresses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programUserCollectionProgressDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramUserCollectionProgress in the database
        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramUserCollectionProgress() throws Exception {
        // Initialize the database
        programUserCollectionProgressRepository.saveAndFlush(programUserCollectionProgress);

        int databaseSizeBeforeDelete = programUserCollectionProgressRepository.findAll().size();

        // Delete the programUserCollectionProgress
        restProgramUserCollectionProgressMockMvc.perform(delete("/api/program-user-collection-progresses/{id}", programUserCollectionProgress.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramUserCollectionProgress> programUserCollectionProgressList = programUserCollectionProgressRepository.findAll();
        assertThat(programUserCollectionProgressList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserCollectionProgress.class);
        ProgramUserCollectionProgress programUserCollectionProgress1 = new ProgramUserCollectionProgress();
        programUserCollectionProgress1.setId(1L);
        ProgramUserCollectionProgress programUserCollectionProgress2 = new ProgramUserCollectionProgress();
        programUserCollectionProgress2.setId(programUserCollectionProgress1.getId());
        assertThat(programUserCollectionProgress1).isEqualTo(programUserCollectionProgress2);
        programUserCollectionProgress2.setId(2L);
        assertThat(programUserCollectionProgress1).isNotEqualTo(programUserCollectionProgress2);
        programUserCollectionProgress1.setId(null);
        assertThat(programUserCollectionProgress1).isNotEqualTo(programUserCollectionProgress2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserCollectionProgressDTO.class);
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO1 = new ProgramUserCollectionProgressDTO();
        programUserCollectionProgressDTO1.setId(1L);
        ProgramUserCollectionProgressDTO programUserCollectionProgressDTO2 = new ProgramUserCollectionProgressDTO();
        assertThat(programUserCollectionProgressDTO1).isNotEqualTo(programUserCollectionProgressDTO2);
        programUserCollectionProgressDTO2.setId(programUserCollectionProgressDTO1.getId());
        assertThat(programUserCollectionProgressDTO1).isEqualTo(programUserCollectionProgressDTO2);
        programUserCollectionProgressDTO2.setId(2L);
        assertThat(programUserCollectionProgressDTO1).isNotEqualTo(programUserCollectionProgressDTO2);
        programUserCollectionProgressDTO1.setId(null);
        assertThat(programUserCollectionProgressDTO1).isNotEqualTo(programUserCollectionProgressDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programUserCollectionProgressMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programUserCollectionProgressMapper.fromId(null)).isNull();
    }
}
