package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramLevelPath;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.repository.ProgramLevelPathRepository;
import aduro.basic.programservice.service.ProgramLevelPathService;
import aduro.basic.programservice.service.dto.ProgramLevelPathDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelPathMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramLevelPathCriteria;
import aduro.basic.programservice.service.ProgramLevelPathQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramLevelPathResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramLevelPathResourceIT {

    private static final String DEFAULT_PATH_ID = "AAAAAAAAAA";
    private static final String UPDATED_PATH_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_PATH_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_PATH_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_PATH_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_PATH_CATEGORY = "BBBBBBBBBB";

    @Autowired
    private ProgramLevelPathRepository programLevelPathRepository;

    @Autowired
    private ProgramLevelPathMapper programLevelPathMapper;

    @Autowired
    private ProgramLevelPathService programLevelPathService;

    @Autowired
    private ProgramLevelPathQueryService programLevelPathQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramLevelPathMockMvc;

    private ProgramLevelPath programLevelPath;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramLevelPathResource programLevelPathResource = new ProgramLevelPathResource(programLevelPathService, programLevelPathQueryService);
        this.restProgramLevelPathMockMvc = MockMvcBuilders.standaloneSetup(programLevelPathResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevelPath createEntity(EntityManager em) {
        ProgramLevelPath programLevelPath = new ProgramLevelPath()
            .pathId(DEFAULT_PATH_ID)
            .name(DEFAULT_NAME)
            .subgroupId(DEFAULT_SUBGROUP_ID)
            .subgroupName(DEFAULT_SUBGROUP_NAME)
            .pathType(DEFAULT_PATH_TYPE)
            .pathCategory(DEFAULT_PATH_CATEGORY);
        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }
        programLevelPath.setProgramLevel(programLevel);
        return programLevelPath;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevelPath createUpdatedEntity(EntityManager em) {
        ProgramLevelPath programLevelPath = new ProgramLevelPath()
            .pathId(UPDATED_PATH_ID)
            .name(UPDATED_NAME)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME)
            .pathType(UPDATED_PATH_TYPE)
            .pathCategory(UPDATED_PATH_CATEGORY);
        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createUpdatedEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }
        programLevelPath.setProgramLevel(programLevel);
        return programLevelPath;
    }

    @BeforeEach
    public void initTest() {
        programLevelPath = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramLevelPath() throws Exception {
        int databaseSizeBeforeCreate = programLevelPathRepository.findAll().size();

        // Create the ProgramLevelPath
        ProgramLevelPathDTO programLevelPathDTO = programLevelPathMapper.toDto(programLevelPath);
        restProgramLevelPathMockMvc.perform(post("/api/program-level-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPathDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramLevelPath in the database
        List<ProgramLevelPath> programLevelPathList = programLevelPathRepository.findAll();
        assertThat(programLevelPathList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramLevelPath testProgramLevelPath = programLevelPathList.get(programLevelPathList.size() - 1);
        assertThat(testProgramLevelPath.getPathId()).isEqualTo(DEFAULT_PATH_ID);
        assertThat(testProgramLevelPath.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProgramLevelPath.getSubgroupId()).isEqualTo(DEFAULT_SUBGROUP_ID);
        assertThat(testProgramLevelPath.getSubgroupName()).isEqualTo(DEFAULT_SUBGROUP_NAME);
        assertThat(testProgramLevelPath.getPathType()).isEqualTo(DEFAULT_PATH_TYPE);
        assertThat(testProgramLevelPath.getPathCategory()).isEqualTo(DEFAULT_PATH_CATEGORY);
    }

    @Test
    @Transactional
    public void createProgramLevelPathWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programLevelPathRepository.findAll().size();

        // Create the ProgramLevelPath with an existing ID
        programLevelPath.setId(1L);
        ProgramLevelPathDTO programLevelPathDTO = programLevelPathMapper.toDto(programLevelPath);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramLevelPathMockMvc.perform(post("/api/program-level-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPathDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevelPath in the database
        List<ProgramLevelPath> programLevelPathList = programLevelPathRepository.findAll();
        assertThat(programLevelPathList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkPathIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programLevelPathRepository.findAll().size();
        // set the field null
        programLevelPath.setPathId(null);

        // Create the ProgramLevelPath, which fails.
        ProgramLevelPathDTO programLevelPathDTO = programLevelPathMapper.toDto(programLevelPath);

        restProgramLevelPathMockMvc.perform(post("/api/program-level-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPathDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramLevelPath> programLevelPathList = programLevelPathRepository.findAll();
        assertThat(programLevelPathList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = programLevelPathRepository.findAll().size();
        // set the field null
        programLevelPath.setName(null);

        // Create the ProgramLevelPath, which fails.
        ProgramLevelPathDTO programLevelPathDTO = programLevelPathMapper.toDto(programLevelPath);

        restProgramLevelPathMockMvc.perform(post("/api/program-level-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPathDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramLevelPath> programLevelPathList = programLevelPathRepository.findAll();
        assertThat(programLevelPathList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPaths() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList
        restProgramLevelPathMockMvc.perform(get("/api/program-level-paths?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevelPath.getId().intValue())))
            .andExpect(jsonPath("$.[*].pathId").value(hasItem(DEFAULT_PATH_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID.toString())))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME.toString())))
            .andExpect(jsonPath("$.[*].pathType").value(hasItem(DEFAULT_PATH_TYPE.toString())))
            .andExpect(jsonPath("$.[*].pathCategory").value(hasItem(DEFAULT_PATH_CATEGORY.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramLevelPath() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get the programLevelPath
        restProgramLevelPathMockMvc.perform(get("/api/program-level-paths/{id}", programLevelPath.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programLevelPath.getId().intValue()))
            .andExpect(jsonPath("$.pathId").value(DEFAULT_PATH_ID.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.subgroupId").value(DEFAULT_SUBGROUP_ID.toString()))
            .andExpect(jsonPath("$.subgroupName").value(DEFAULT_SUBGROUP_NAME.toString()))
            .andExpect(jsonPath("$.pathType").value(DEFAULT_PATH_TYPE.toString()))
            .andExpect(jsonPath("$.pathCategory").value(DEFAULT_PATH_CATEGORY.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathId equals to DEFAULT_PATH_ID
        defaultProgramLevelPathShouldBeFound("pathId.equals=" + DEFAULT_PATH_ID);

        // Get all the programLevelPathList where pathId equals to UPDATED_PATH_ID
        defaultProgramLevelPathShouldNotBeFound("pathId.equals=" + UPDATED_PATH_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathIdIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathId in DEFAULT_PATH_ID or UPDATED_PATH_ID
        defaultProgramLevelPathShouldBeFound("pathId.in=" + DEFAULT_PATH_ID + "," + UPDATED_PATH_ID);

        // Get all the programLevelPathList where pathId equals to UPDATED_PATH_ID
        defaultProgramLevelPathShouldNotBeFound("pathId.in=" + UPDATED_PATH_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathId is not null
        defaultProgramLevelPathShouldBeFound("pathId.specified=true");

        // Get all the programLevelPathList where pathId is null
        defaultProgramLevelPathShouldNotBeFound("pathId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where name equals to DEFAULT_NAME
        defaultProgramLevelPathShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the programLevelPathList where name equals to UPDATED_NAME
        defaultProgramLevelPathShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where name in DEFAULT_NAME or UPDATED_NAME
        defaultProgramLevelPathShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the programLevelPathList where name equals to UPDATED_NAME
        defaultProgramLevelPathShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where name is not null
        defaultProgramLevelPathShouldBeFound("name.specified=true");

        // Get all the programLevelPathList where name is null
        defaultProgramLevelPathShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsBySubgroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where subgroupId equals to DEFAULT_SUBGROUP_ID
        defaultProgramLevelPathShouldBeFound("subgroupId.equals=" + DEFAULT_SUBGROUP_ID);

        // Get all the programLevelPathList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramLevelPathShouldNotBeFound("subgroupId.equals=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsBySubgroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where subgroupId in DEFAULT_SUBGROUP_ID or UPDATED_SUBGROUP_ID
        defaultProgramLevelPathShouldBeFound("subgroupId.in=" + DEFAULT_SUBGROUP_ID + "," + UPDATED_SUBGROUP_ID);

        // Get all the programLevelPathList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramLevelPathShouldNotBeFound("subgroupId.in=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsBySubgroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where subgroupId is not null
        defaultProgramLevelPathShouldBeFound("subgroupId.specified=true");

        // Get all the programLevelPathList where subgroupId is null
        defaultProgramLevelPathShouldNotBeFound("subgroupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsBySubgroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where subgroupName equals to DEFAULT_SUBGROUP_NAME
        defaultProgramLevelPathShouldBeFound("subgroupName.equals=" + DEFAULT_SUBGROUP_NAME);

        // Get all the programLevelPathList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramLevelPathShouldNotBeFound("subgroupName.equals=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsBySubgroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where subgroupName in DEFAULT_SUBGROUP_NAME or UPDATED_SUBGROUP_NAME
        defaultProgramLevelPathShouldBeFound("subgroupName.in=" + DEFAULT_SUBGROUP_NAME + "," + UPDATED_SUBGROUP_NAME);

        // Get all the programLevelPathList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramLevelPathShouldNotBeFound("subgroupName.in=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsBySubgroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where subgroupName is not null
        defaultProgramLevelPathShouldBeFound("subgroupName.specified=true");

        // Get all the programLevelPathList where subgroupName is null
        defaultProgramLevelPathShouldNotBeFound("subgroupName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathType equals to DEFAULT_PATH_TYPE
        defaultProgramLevelPathShouldBeFound("pathType.equals=" + DEFAULT_PATH_TYPE);

        // Get all the programLevelPathList where pathType equals to UPDATED_PATH_TYPE
        defaultProgramLevelPathShouldNotBeFound("pathType.equals=" + UPDATED_PATH_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathTypeIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathType in DEFAULT_PATH_TYPE or UPDATED_PATH_TYPE
        defaultProgramLevelPathShouldBeFound("pathType.in=" + DEFAULT_PATH_TYPE + "," + UPDATED_PATH_TYPE);

        // Get all the programLevelPathList where pathType equals to UPDATED_PATH_TYPE
        defaultProgramLevelPathShouldNotBeFound("pathType.in=" + UPDATED_PATH_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathType is not null
        defaultProgramLevelPathShouldBeFound("pathType.specified=true");

        // Get all the programLevelPathList where pathType is null
        defaultProgramLevelPathShouldNotBeFound("pathType.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathCategoryIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathCategory equals to DEFAULT_PATH_CATEGORY
        defaultProgramLevelPathShouldBeFound("pathCategory.equals=" + DEFAULT_PATH_CATEGORY);

        // Get all the programLevelPathList where pathCategory equals to UPDATED_PATH_CATEGORY
        defaultProgramLevelPathShouldNotBeFound("pathCategory.equals=" + UPDATED_PATH_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathCategoryIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathCategory in DEFAULT_PATH_CATEGORY or UPDATED_PATH_CATEGORY
        defaultProgramLevelPathShouldBeFound("pathCategory.in=" + DEFAULT_PATH_CATEGORY + "," + UPDATED_PATH_CATEGORY);

        // Get all the programLevelPathList where pathCategory equals to UPDATED_PATH_CATEGORY
        defaultProgramLevelPathShouldNotBeFound("pathCategory.in=" + UPDATED_PATH_CATEGORY);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByPathCategoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        // Get all the programLevelPathList where pathCategory is not null
        defaultProgramLevelPathShouldBeFound("pathCategory.specified=true");

        // Get all the programLevelPathList where pathCategory is null
        defaultProgramLevelPathShouldNotBeFound("pathCategory.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPathsByProgramLevelIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramLevel programLevel = programLevelPath.getProgramLevel();
        programLevelPathRepository.saveAndFlush(programLevelPath);
        Long programLevelId = programLevel.getId();

        // Get all the programLevelPathList where programLevel equals to programLevelId
        defaultProgramLevelPathShouldBeFound("programLevelId.equals=" + programLevelId);

        // Get all the programLevelPathList where programLevel equals to programLevelId + 1
        defaultProgramLevelPathShouldNotBeFound("programLevelId.equals=" + (programLevelId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramLevelPathShouldBeFound(String filter) throws Exception {
        restProgramLevelPathMockMvc.perform(get("/api/program-level-paths?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevelPath.getId().intValue())))
            .andExpect(jsonPath("$.[*].pathId").value(hasItem(DEFAULT_PATH_ID)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID)))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME)))
            .andExpect(jsonPath("$.[*].pathType").value(hasItem(DEFAULT_PATH_TYPE)))
            .andExpect(jsonPath("$.[*].pathCategory").value(hasItem(DEFAULT_PATH_CATEGORY)));

        // Check, that the count call also returns 1
        restProgramLevelPathMockMvc.perform(get("/api/program-level-paths/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramLevelPathShouldNotBeFound(String filter) throws Exception {
        restProgramLevelPathMockMvc.perform(get("/api/program-level-paths?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramLevelPathMockMvc.perform(get("/api/program-level-paths/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramLevelPath() throws Exception {
        // Get the programLevelPath
        restProgramLevelPathMockMvc.perform(get("/api/program-level-paths/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramLevelPath() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        int databaseSizeBeforeUpdate = programLevelPathRepository.findAll().size();

        // Update the programLevelPath
        ProgramLevelPath updatedProgramLevelPath = programLevelPathRepository.findById(programLevelPath.getId()).get();
        // Disconnect from session so that the updates on updatedProgramLevelPath are not directly saved in db
        em.detach(updatedProgramLevelPath);
        updatedProgramLevelPath
            .pathId(UPDATED_PATH_ID)
            .name(UPDATED_NAME)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME)
            .pathType(UPDATED_PATH_TYPE)
            .pathCategory(UPDATED_PATH_CATEGORY);
        ProgramLevelPathDTO programLevelPathDTO = programLevelPathMapper.toDto(updatedProgramLevelPath);

        restProgramLevelPathMockMvc.perform(put("/api/program-level-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPathDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramLevelPath in the database
        List<ProgramLevelPath> programLevelPathList = programLevelPathRepository.findAll();
        assertThat(programLevelPathList).hasSize(databaseSizeBeforeUpdate);
        ProgramLevelPath testProgramLevelPath = programLevelPathList.get(programLevelPathList.size() - 1);
        assertThat(testProgramLevelPath.getPathId()).isEqualTo(UPDATED_PATH_ID);
        assertThat(testProgramLevelPath.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProgramLevelPath.getSubgroupId()).isEqualTo(UPDATED_SUBGROUP_ID);
        assertThat(testProgramLevelPath.getSubgroupName()).isEqualTo(UPDATED_SUBGROUP_NAME);
        assertThat(testProgramLevelPath.getPathType()).isEqualTo(UPDATED_PATH_TYPE);
        assertThat(testProgramLevelPath.getPathCategory()).isEqualTo(UPDATED_PATH_CATEGORY);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramLevelPath() throws Exception {
        int databaseSizeBeforeUpdate = programLevelPathRepository.findAll().size();

        // Create the ProgramLevelPath
        ProgramLevelPathDTO programLevelPathDTO = programLevelPathMapper.toDto(programLevelPath);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramLevelPathMockMvc.perform(put("/api/program-level-paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPathDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevelPath in the database
        List<ProgramLevelPath> programLevelPathList = programLevelPathRepository.findAll();
        assertThat(programLevelPathList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramLevelPath() throws Exception {
        // Initialize the database
        programLevelPathRepository.saveAndFlush(programLevelPath);

        int databaseSizeBeforeDelete = programLevelPathRepository.findAll().size();

        // Delete the programLevelPath
        restProgramLevelPathMockMvc.perform(delete("/api/program-level-paths/{id}", programLevelPath.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramLevelPath> programLevelPathList = programLevelPathRepository.findAll();
        assertThat(programLevelPathList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelPath.class);
        ProgramLevelPath programLevelPath1 = new ProgramLevelPath();
        programLevelPath1.setId(1L);
        ProgramLevelPath programLevelPath2 = new ProgramLevelPath();
        programLevelPath2.setId(programLevelPath1.getId());
        assertThat(programLevelPath1).isEqualTo(programLevelPath2);
        programLevelPath2.setId(2L);
        assertThat(programLevelPath1).isNotEqualTo(programLevelPath2);
        programLevelPath1.setId(null);
        assertThat(programLevelPath1).isNotEqualTo(programLevelPath2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelPathDTO.class);
        ProgramLevelPathDTO programLevelPathDTO1 = new ProgramLevelPathDTO();
        programLevelPathDTO1.setId(1L);
        ProgramLevelPathDTO programLevelPathDTO2 = new ProgramLevelPathDTO();
        assertThat(programLevelPathDTO1).isNotEqualTo(programLevelPathDTO2);
        programLevelPathDTO2.setId(programLevelPathDTO1.getId());
        assertThat(programLevelPathDTO1).isEqualTo(programLevelPathDTO2);
        programLevelPathDTO2.setId(2L);
        assertThat(programLevelPathDTO1).isNotEqualTo(programLevelPathDTO2);
        programLevelPathDTO1.setId(null);
        assertThat(programLevelPathDTO1).isNotEqualTo(programLevelPathDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programLevelPathMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programLevelPathMapper.fromId(null)).isNull();
    }
}
