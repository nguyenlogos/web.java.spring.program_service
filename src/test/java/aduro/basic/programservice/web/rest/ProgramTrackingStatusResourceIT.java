package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramTrackingStatus;
import aduro.basic.programservice.repository.ProgramTrackingStatusRepository;
import aduro.basic.programservice.service.ProgramTrackingStatusService;
import aduro.basic.programservice.service.dto.ProgramTrackingStatusDTO;
import aduro.basic.programservice.service.mapper.ProgramTrackingStatusMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramTrackingStatusCriteria;
import aduro.basic.programservice.service.ProgramTrackingStatusQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramTrackingStatusResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramTrackingStatusResourceIT {

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    private static final String DEFAULT_PROGRAM_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_PROGRAM_STATUS = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramTrackingStatusRepository programTrackingStatusRepository;

    @Autowired
    private ProgramTrackingStatusMapper programTrackingStatusMapper;

    @Autowired
    private ProgramTrackingStatusService programTrackingStatusService;

    @Autowired
    private ProgramTrackingStatusQueryService programTrackingStatusQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramTrackingStatusMockMvc;

    private ProgramTrackingStatus programTrackingStatus;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramTrackingStatusResource programTrackingStatusResource = new ProgramTrackingStatusResource(programTrackingStatusService, programTrackingStatusQueryService);
        this.restProgramTrackingStatusMockMvc = MockMvcBuilders.standaloneSetup(programTrackingStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramTrackingStatus createEntity(EntityManager em) {
        ProgramTrackingStatus programTrackingStatus = new ProgramTrackingStatus()
            .programId(DEFAULT_PROGRAM_ID)
            .programStatus(DEFAULT_PROGRAM_STATUS)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        return programTrackingStatus;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramTrackingStatus createUpdatedEntity(EntityManager em) {
        ProgramTrackingStatus programTrackingStatus = new ProgramTrackingStatus()
            .programId(UPDATED_PROGRAM_ID)
            .programStatus(UPDATED_PROGRAM_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        return programTrackingStatus;
    }

    @BeforeEach
    public void initTest() {
        programTrackingStatus = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramTrackingStatus() throws Exception {
        int databaseSizeBeforeCreate = programTrackingStatusRepository.findAll().size();

        // Create the ProgramTrackingStatus
        ProgramTrackingStatusDTO programTrackingStatusDTO = programTrackingStatusMapper.toDto(programTrackingStatus);
        restProgramTrackingStatusMockMvc.perform(post("/api/program-tracking-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programTrackingStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramTrackingStatus in the database
        List<ProgramTrackingStatus> programTrackingStatusList = programTrackingStatusRepository.findAll();
        assertThat(programTrackingStatusList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramTrackingStatus testProgramTrackingStatus = programTrackingStatusList.get(programTrackingStatusList.size() - 1);
        assertThat(testProgramTrackingStatus.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
        assertThat(testProgramTrackingStatus.getProgramStatus()).isEqualTo(DEFAULT_PROGRAM_STATUS);
        assertThat(testProgramTrackingStatus.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramTrackingStatus.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramTrackingStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programTrackingStatusRepository.findAll().size();

        // Create the ProgramTrackingStatus with an existing ID
        programTrackingStatus.setId(1L);
        ProgramTrackingStatusDTO programTrackingStatusDTO = programTrackingStatusMapper.toDto(programTrackingStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramTrackingStatusMockMvc.perform(post("/api/program-tracking-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programTrackingStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramTrackingStatus in the database
        List<ProgramTrackingStatus> programTrackingStatusList = programTrackingStatusRepository.findAll();
        assertThat(programTrackingStatusList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkProgramIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programTrackingStatusRepository.findAll().size();
        // set the field null
        programTrackingStatus.setProgramId(null);

        // Create the ProgramTrackingStatus, which fails.
        ProgramTrackingStatusDTO programTrackingStatusDTO = programTrackingStatusMapper.toDto(programTrackingStatus);

        restProgramTrackingStatusMockMvc.perform(post("/api/program-tracking-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programTrackingStatusDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramTrackingStatus> programTrackingStatusList = programTrackingStatusRepository.findAll();
        assertThat(programTrackingStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatuses() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList
        restProgramTrackingStatusMockMvc.perform(get("/api/program-tracking-statuses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programTrackingStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].programStatus").value(hasItem(DEFAULT_PROGRAM_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramTrackingStatus() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get the programTrackingStatus
        restProgramTrackingStatusMockMvc.perform(get("/api/program-tracking-statuses/{id}", programTrackingStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programTrackingStatus.getId().intValue()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()))
            .andExpect(jsonPath("$.programStatus").value(DEFAULT_PROGRAM_STATUS.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByProgramIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where programId equals to DEFAULT_PROGRAM_ID
        defaultProgramTrackingStatusShouldBeFound("programId.equals=" + DEFAULT_PROGRAM_ID);

        // Get all the programTrackingStatusList where programId equals to UPDATED_PROGRAM_ID
        defaultProgramTrackingStatusShouldNotBeFound("programId.equals=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByProgramIdIsInShouldWork() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where programId in DEFAULT_PROGRAM_ID or UPDATED_PROGRAM_ID
        defaultProgramTrackingStatusShouldBeFound("programId.in=" + DEFAULT_PROGRAM_ID + "," + UPDATED_PROGRAM_ID);

        // Get all the programTrackingStatusList where programId equals to UPDATED_PROGRAM_ID
        defaultProgramTrackingStatusShouldNotBeFound("programId.in=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByProgramIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where programId is not null
        defaultProgramTrackingStatusShouldBeFound("programId.specified=true");

        // Get all the programTrackingStatusList where programId is null
        defaultProgramTrackingStatusShouldNotBeFound("programId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByProgramIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where programId greater than or equals to DEFAULT_PROGRAM_ID
        defaultProgramTrackingStatusShouldBeFound("programId.greaterOrEqualThan=" + DEFAULT_PROGRAM_ID);

        // Get all the programTrackingStatusList where programId greater than or equals to UPDATED_PROGRAM_ID
        defaultProgramTrackingStatusShouldNotBeFound("programId.greaterOrEqualThan=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByProgramIdIsLessThanSomething() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where programId less than or equals to DEFAULT_PROGRAM_ID
        defaultProgramTrackingStatusShouldNotBeFound("programId.lessThan=" + DEFAULT_PROGRAM_ID);

        // Get all the programTrackingStatusList where programId less than or equals to UPDATED_PROGRAM_ID
        defaultProgramTrackingStatusShouldBeFound("programId.lessThan=" + UPDATED_PROGRAM_ID);
    }


    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByProgramStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where programStatus equals to DEFAULT_PROGRAM_STATUS
        defaultProgramTrackingStatusShouldBeFound("programStatus.equals=" + DEFAULT_PROGRAM_STATUS);

        // Get all the programTrackingStatusList where programStatus equals to UPDATED_PROGRAM_STATUS
        defaultProgramTrackingStatusShouldNotBeFound("programStatus.equals=" + UPDATED_PROGRAM_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByProgramStatusIsInShouldWork() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where programStatus in DEFAULT_PROGRAM_STATUS or UPDATED_PROGRAM_STATUS
        defaultProgramTrackingStatusShouldBeFound("programStatus.in=" + DEFAULT_PROGRAM_STATUS + "," + UPDATED_PROGRAM_STATUS);

        // Get all the programTrackingStatusList where programStatus equals to UPDATED_PROGRAM_STATUS
        defaultProgramTrackingStatusShouldNotBeFound("programStatus.in=" + UPDATED_PROGRAM_STATUS);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByProgramStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where programStatus is not null
        defaultProgramTrackingStatusShouldBeFound("programStatus.specified=true");

        // Get all the programTrackingStatusList where programStatus is null
        defaultProgramTrackingStatusShouldNotBeFound("programStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramTrackingStatusShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programTrackingStatusList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramTrackingStatusShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramTrackingStatusShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programTrackingStatusList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramTrackingStatusShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where createdDate is not null
        defaultProgramTrackingStatusShouldBeFound("createdDate.specified=true");

        // Get all the programTrackingStatusList where createdDate is null
        defaultProgramTrackingStatusShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramTrackingStatusShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programTrackingStatusList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramTrackingStatusShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramTrackingStatusShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programTrackingStatusList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramTrackingStatusShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramTrackingStatusesByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        // Get all the programTrackingStatusList where modifiedDate is not null
        defaultProgramTrackingStatusShouldBeFound("modifiedDate.specified=true");

        // Get all the programTrackingStatusList where modifiedDate is null
        defaultProgramTrackingStatusShouldNotBeFound("modifiedDate.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramTrackingStatusShouldBeFound(String filter) throws Exception {
        restProgramTrackingStatusMockMvc.perform(get("/api/program-tracking-statuses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programTrackingStatus.getId().intValue())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].programStatus").value(hasItem(DEFAULT_PROGRAM_STATUS)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramTrackingStatusMockMvc.perform(get("/api/program-tracking-statuses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramTrackingStatusShouldNotBeFound(String filter) throws Exception {
        restProgramTrackingStatusMockMvc.perform(get("/api/program-tracking-statuses?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramTrackingStatusMockMvc.perform(get("/api/program-tracking-statuses/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramTrackingStatus() throws Exception {
        // Get the programTrackingStatus
        restProgramTrackingStatusMockMvc.perform(get("/api/program-tracking-statuses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramTrackingStatus() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        int databaseSizeBeforeUpdate = programTrackingStatusRepository.findAll().size();

        // Update the programTrackingStatus
        ProgramTrackingStatus updatedProgramTrackingStatus = programTrackingStatusRepository.findById(programTrackingStatus.getId()).get();
        // Disconnect from session so that the updates on updatedProgramTrackingStatus are not directly saved in db
        em.detach(updatedProgramTrackingStatus);
        updatedProgramTrackingStatus
            .programId(UPDATED_PROGRAM_ID)
            .programStatus(UPDATED_PROGRAM_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        ProgramTrackingStatusDTO programTrackingStatusDTO = programTrackingStatusMapper.toDto(updatedProgramTrackingStatus);

        restProgramTrackingStatusMockMvc.perform(put("/api/program-tracking-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programTrackingStatusDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramTrackingStatus in the database
        List<ProgramTrackingStatus> programTrackingStatusList = programTrackingStatusRepository.findAll();
        assertThat(programTrackingStatusList).hasSize(databaseSizeBeforeUpdate);
        ProgramTrackingStatus testProgramTrackingStatus = programTrackingStatusList.get(programTrackingStatusList.size() - 1);
        assertThat(testProgramTrackingStatus.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
        assertThat(testProgramTrackingStatus.getProgramStatus()).isEqualTo(UPDATED_PROGRAM_STATUS);
        assertThat(testProgramTrackingStatus.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramTrackingStatus.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramTrackingStatus() throws Exception {
        int databaseSizeBeforeUpdate = programTrackingStatusRepository.findAll().size();

        // Create the ProgramTrackingStatus
        ProgramTrackingStatusDTO programTrackingStatusDTO = programTrackingStatusMapper.toDto(programTrackingStatus);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramTrackingStatusMockMvc.perform(put("/api/program-tracking-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programTrackingStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramTrackingStatus in the database
        List<ProgramTrackingStatus> programTrackingStatusList = programTrackingStatusRepository.findAll();
        assertThat(programTrackingStatusList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramTrackingStatus() throws Exception {
        // Initialize the database
        programTrackingStatusRepository.saveAndFlush(programTrackingStatus);

        int databaseSizeBeforeDelete = programTrackingStatusRepository.findAll().size();

        // Delete the programTrackingStatus
        restProgramTrackingStatusMockMvc.perform(delete("/api/program-tracking-statuses/{id}", programTrackingStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramTrackingStatus> programTrackingStatusList = programTrackingStatusRepository.findAll();
        assertThat(programTrackingStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramTrackingStatus.class);
        ProgramTrackingStatus programTrackingStatus1 = new ProgramTrackingStatus();
        programTrackingStatus1.setId(1L);
        ProgramTrackingStatus programTrackingStatus2 = new ProgramTrackingStatus();
        programTrackingStatus2.setId(programTrackingStatus1.getId());
        assertThat(programTrackingStatus1).isEqualTo(programTrackingStatus2);
        programTrackingStatus2.setId(2L);
        assertThat(programTrackingStatus1).isNotEqualTo(programTrackingStatus2);
        programTrackingStatus1.setId(null);
        assertThat(programTrackingStatus1).isNotEqualTo(programTrackingStatus2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramTrackingStatusDTO.class);
        ProgramTrackingStatusDTO programTrackingStatusDTO1 = new ProgramTrackingStatusDTO();
        programTrackingStatusDTO1.setId(1L);
        ProgramTrackingStatusDTO programTrackingStatusDTO2 = new ProgramTrackingStatusDTO();
        assertThat(programTrackingStatusDTO1).isNotEqualTo(programTrackingStatusDTO2);
        programTrackingStatusDTO2.setId(programTrackingStatusDTO1.getId());
        assertThat(programTrackingStatusDTO1).isEqualTo(programTrackingStatusDTO2);
        programTrackingStatusDTO2.setId(2L);
        assertThat(programTrackingStatusDTO1).isNotEqualTo(programTrackingStatusDTO2);
        programTrackingStatusDTO1.setId(null);
        assertThat(programTrackingStatusDTO1).isNotEqualTo(programTrackingStatusDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programTrackingStatusMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programTrackingStatusMapper.fromId(null)).isNull();
    }
}
