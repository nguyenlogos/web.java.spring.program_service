package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.WellmetricEventQueue;
import aduro.basic.programservice.repository.WellmetricEventQueueRepository;
import aduro.basic.programservice.service.WellmetricEventQueueService;
import aduro.basic.programservice.service.dto.WellmetricEventQueueDTO;
import aduro.basic.programservice.service.mapper.WellmetricEventQueueMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.WellmetricEventQueueCriteria;
import aduro.basic.programservice.service.WellmetricEventQueueQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import aduro.basic.programservice.domain.enumeration.ResultStatus;
import aduro.basic.programservice.domain.enumeration.Gender;
/**
 * Integration tests for the {@Link WellmetricEventQueueResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class WellmetricEventQueueResourceIT {

    private static final String DEFAULT_PARTICIPANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_ERROR_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_MESSAGE = "BBBBBBBBBB";

    private static final ResultStatus DEFAULT_RESULT = ResultStatus.PASS;
    private static final ResultStatus UPDATED_RESULT = ResultStatus.FAIL;

    private static final String DEFAULT_EVENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_CODE = "BBBBBBBBBB";

    private static final Gender DEFAULT_GENDER = Gender.MALE;
    private static final Gender UPDATED_GENDER = Gender.FEMALE;

    private static final Integer DEFAULT_ATTEMPT_COUNT = 1;
    private static final Integer UPDATED_ATTEMPT_COUNT = 2;

    private static final Instant DEFAULT_LAST_ATTEMPT_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_ATTEMPT_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_SUBGROUP_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_ID = "BBBBBBBBBB";

    private static final Float DEFAULT_HEALTHY_VALUE = 1F;
    private static final Float UPDATED_HEALTHY_VALUE = 2F;

    private static final String DEFAULT_EXECUTE_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_EXECUTE_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_EVENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_ID = "BBBBBBBBBB";

    private static final Boolean DEFAULT_HAS_INCENTIVE = false;
    private static final Boolean UPDATED_HAS_INCENTIVE = true;

    private static final Boolean DEFAULT_IS_WAITING_PUSH = false;
    private static final Boolean UPDATED_IS_WAITING_PUSH = true;

    private static final Instant DEFAULT_ATTEMPTED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ATTEMPTED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    private static final String DEFAULT_EVENT_DATE = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_DATE = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_CATEGORY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SUB_CATEGORY_CODE = "BBBBBBBBBB";

    @Autowired
    private WellmetricEventQueueRepository wellmetricEventQueueRepository;

    @Autowired
    private WellmetricEventQueueMapper wellmetricEventQueueMapper;

    @Autowired
    private WellmetricEventQueueService wellmetricEventQueueService;

    @Autowired
    private WellmetricEventQueueQueryService wellmetricEventQueueQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restWellmetricEventQueueMockMvc;

    private WellmetricEventQueue wellmetricEventQueue;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final WellmetricEventQueueResource wellmetricEventQueueResource = new WellmetricEventQueueResource(wellmetricEventQueueService, wellmetricEventQueueQueryService);
        this.restWellmetricEventQueueMockMvc = MockMvcBuilders.standaloneSetup(wellmetricEventQueueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WellmetricEventQueue createEntity(EntityManager em) {
        WellmetricEventQueue wellmetricEventQueue = new WellmetricEventQueue()
            .participantId(DEFAULT_PARTICIPANT_ID)
            .clientId(DEFAULT_CLIENT_ID)
            .email(DEFAULT_EMAIL)
            .errorMessage(DEFAULT_ERROR_MESSAGE)
            .result(DEFAULT_RESULT)
            .eventCode(DEFAULT_EVENT_CODE)
            .gender(DEFAULT_GENDER)
            .attemptCount(DEFAULT_ATTEMPT_COUNT)
            .lastAttemptTime(DEFAULT_LAST_ATTEMPT_TIME)
            .createdAt(DEFAULT_CREATED_AT)
            .subgroupId(DEFAULT_SUBGROUP_ID)
            .healthyValue(DEFAULT_HEALTHY_VALUE)
            .executeStatus(DEFAULT_EXECUTE_STATUS)
            .eventId(DEFAULT_EVENT_ID)
            .hasIncentive(DEFAULT_HAS_INCENTIVE)
            .isWaitingPush(DEFAULT_IS_WAITING_PUSH)
            .attemptedAt(DEFAULT_ATTEMPTED_AT)
            .programId(DEFAULT_PROGRAM_ID)
            .eventDate(DEFAULT_EVENT_DATE)
            .subCategoryCode(DEFAULT_SUB_CATEGORY_CODE);
        return wellmetricEventQueue;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WellmetricEventQueue createUpdatedEntity(EntityManager em) {
        WellmetricEventQueue wellmetricEventQueue = new WellmetricEventQueue()
            .participantId(UPDATED_PARTICIPANT_ID)
            .clientId(UPDATED_CLIENT_ID)
            .email(UPDATED_EMAIL)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .result(UPDATED_RESULT)
            .eventCode(UPDATED_EVENT_CODE)
            .gender(UPDATED_GENDER)
            .attemptCount(UPDATED_ATTEMPT_COUNT)
            .lastAttemptTime(UPDATED_LAST_ATTEMPT_TIME)
            .createdAt(UPDATED_CREATED_AT)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .healthyValue(UPDATED_HEALTHY_VALUE)
            .executeStatus(UPDATED_EXECUTE_STATUS)
            .eventId(UPDATED_EVENT_ID)
            .hasIncentive(UPDATED_HAS_INCENTIVE)
            .isWaitingPush(UPDATED_IS_WAITING_PUSH)
            .attemptedAt(UPDATED_ATTEMPTED_AT)
            .programId(UPDATED_PROGRAM_ID)
            .eventDate(UPDATED_EVENT_DATE)
            .subCategoryCode(UPDATED_SUB_CATEGORY_CODE);
        return wellmetricEventQueue;
    }

    @BeforeEach
    public void initTest() {
        wellmetricEventQueue = createEntity(em);
    }

    @Test
    @Transactional
    public void createWellmetricEventQueue() throws Exception {
        int databaseSizeBeforeCreate = wellmetricEventQueueRepository.findAll().size();

        // Create the WellmetricEventQueue
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);
        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isCreated());

        // Validate the WellmetricEventQueue in the database
        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeCreate + 1);
        WellmetricEventQueue testWellmetricEventQueue = wellmetricEventQueueList.get(wellmetricEventQueueList.size() - 1);
        assertThat(testWellmetricEventQueue.getParticipantId()).isEqualTo(DEFAULT_PARTICIPANT_ID);
        assertThat(testWellmetricEventQueue.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testWellmetricEventQueue.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testWellmetricEventQueue.getErrorMessage()).isEqualTo(DEFAULT_ERROR_MESSAGE);
        assertThat(testWellmetricEventQueue.getResult()).isEqualTo(DEFAULT_RESULT);
        assertThat(testWellmetricEventQueue.getEventCode()).isEqualTo(DEFAULT_EVENT_CODE);
        assertThat(testWellmetricEventQueue.getGender()).isEqualTo(DEFAULT_GENDER);
        assertThat(testWellmetricEventQueue.getAttemptCount()).isEqualTo(DEFAULT_ATTEMPT_COUNT);
        assertThat(testWellmetricEventQueue.getLastAttemptTime()).isEqualTo(DEFAULT_LAST_ATTEMPT_TIME);
        assertThat(testWellmetricEventQueue.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testWellmetricEventQueue.getSubgroupId()).isEqualTo(DEFAULT_SUBGROUP_ID);
        assertThat(testWellmetricEventQueue.getHealthyValue()).isEqualTo(DEFAULT_HEALTHY_VALUE);
        assertThat(testWellmetricEventQueue.getExecuteStatus()).isEqualTo(DEFAULT_EXECUTE_STATUS);
        assertThat(testWellmetricEventQueue.getEventId()).isEqualTo(DEFAULT_EVENT_ID);
        assertThat(testWellmetricEventQueue.isHasIncentive()).isEqualTo(DEFAULT_HAS_INCENTIVE);
        assertThat(testWellmetricEventQueue.isIsWaitingPush()).isEqualTo(DEFAULT_IS_WAITING_PUSH);
        assertThat(testWellmetricEventQueue.getAttemptedAt()).isEqualTo(DEFAULT_ATTEMPTED_AT);
        assertThat(testWellmetricEventQueue.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
        assertThat(testWellmetricEventQueue.getEventDate()).isEqualTo(DEFAULT_EVENT_DATE);
        assertThat(testWellmetricEventQueue.getSubCategoryCode()).isEqualTo(DEFAULT_SUB_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void createWellmetricEventQueueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = wellmetricEventQueueRepository.findAll().size();

        // Create the WellmetricEventQueue with an existing ID
        wellmetricEventQueue.setId(1L);
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the WellmetricEventQueue in the database
        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkParticipantIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setParticipantId(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setClientId(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setEmail(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkResultIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setResult(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setEventCode(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkGenderIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setGender(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastAttemptTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setLastAttemptTime(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setCreatedAt(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkHealthyValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setHealthyValue(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkExecuteStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setExecuteStatus(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setEventId(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAttemptedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setAttemptedAt(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkProgramIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setProgramId(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = wellmetricEventQueueRepository.findAll().size();
        // set the field null
        wellmetricEventQueue.setEventDate(null);

        // Create the WellmetricEventQueue, which fails.
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(post("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueues() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList
        restWellmetricEventQueueMockMvc.perform(get("/api/wellmetric-event-queues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(wellmetricEventQueue.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID.toString())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())))
            .andExpect(jsonPath("$.[*].eventCode").value(hasItem(DEFAULT_EVENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].attemptCount").value(hasItem(DEFAULT_ATTEMPT_COUNT)))
            .andExpect(jsonPath("$.[*].lastAttemptTime").value(hasItem(DEFAULT_LAST_ATTEMPT_TIME.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID.toString())))
            .andExpect(jsonPath("$.[*].healthyValue").value(hasItem(DEFAULT_HEALTHY_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].executeStatus").value(hasItem(DEFAULT_EXECUTE_STATUS.toString())))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID.toString())))
            .andExpect(jsonPath("$.[*].hasIncentive").value(hasItem(DEFAULT_HAS_INCENTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].isWaitingPush").value(hasItem(DEFAULT_IS_WAITING_PUSH.booleanValue())))
            .andExpect(jsonPath("$.[*].attemptedAt").value(hasItem(DEFAULT_ATTEMPTED_AT.toString())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].eventDate").value(hasItem(DEFAULT_EVENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].subCategoryCode").value(hasItem(DEFAULT_SUB_CATEGORY_CODE.toString())));
    }
    
    @Test
    @Transactional
    public void getWellmetricEventQueue() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get the wellmetricEventQueue
        restWellmetricEventQueueMockMvc.perform(get("/api/wellmetric-event-queues/{id}", wellmetricEventQueue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(wellmetricEventQueue.getId().intValue()))
            .andExpect(jsonPath("$.participantId").value(DEFAULT_PARTICIPANT_ID.toString()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.errorMessage").value(DEFAULT_ERROR_MESSAGE.toString()))
            .andExpect(jsonPath("$.result").value(DEFAULT_RESULT.toString()))
            .andExpect(jsonPath("$.eventCode").value(DEFAULT_EVENT_CODE.toString()))
            .andExpect(jsonPath("$.gender").value(DEFAULT_GENDER.toString()))
            .andExpect(jsonPath("$.attemptCount").value(DEFAULT_ATTEMPT_COUNT))
            .andExpect(jsonPath("$.lastAttemptTime").value(DEFAULT_LAST_ATTEMPT_TIME.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.subgroupId").value(DEFAULT_SUBGROUP_ID.toString()))
            .andExpect(jsonPath("$.healthyValue").value(DEFAULT_HEALTHY_VALUE.doubleValue()))
            .andExpect(jsonPath("$.executeStatus").value(DEFAULT_EXECUTE_STATUS.toString()))
            .andExpect(jsonPath("$.eventId").value(DEFAULT_EVENT_ID.toString()))
            .andExpect(jsonPath("$.hasIncentive").value(DEFAULT_HAS_INCENTIVE.booleanValue()))
            .andExpect(jsonPath("$.isWaitingPush").value(DEFAULT_IS_WAITING_PUSH.booleanValue()))
            .andExpect(jsonPath("$.attemptedAt").value(DEFAULT_ATTEMPTED_AT.toString()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()))
            .andExpect(jsonPath("$.eventDate").value(DEFAULT_EVENT_DATE.toString()))
            .andExpect(jsonPath("$.subCategoryCode").value(DEFAULT_SUB_CATEGORY_CODE.toString()));
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByParticipantIdIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where participantId equals to DEFAULT_PARTICIPANT_ID
        defaultWellmetricEventQueueShouldBeFound("participantId.equals=" + DEFAULT_PARTICIPANT_ID);

        // Get all the wellmetricEventQueueList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultWellmetricEventQueueShouldNotBeFound("participantId.equals=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByParticipantIdIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where participantId in DEFAULT_PARTICIPANT_ID or UPDATED_PARTICIPANT_ID
        defaultWellmetricEventQueueShouldBeFound("participantId.in=" + DEFAULT_PARTICIPANT_ID + "," + UPDATED_PARTICIPANT_ID);

        // Get all the wellmetricEventQueueList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultWellmetricEventQueueShouldNotBeFound("participantId.in=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByParticipantIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where participantId is not null
        defaultWellmetricEventQueueShouldBeFound("participantId.specified=true");

        // Get all the wellmetricEventQueueList where participantId is null
        defaultWellmetricEventQueueShouldNotBeFound("participantId.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where clientId equals to DEFAULT_CLIENT_ID
        defaultWellmetricEventQueueShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the wellmetricEventQueueList where clientId equals to UPDATED_CLIENT_ID
        defaultWellmetricEventQueueShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultWellmetricEventQueueShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the wellmetricEventQueueList where clientId equals to UPDATED_CLIENT_ID
        defaultWellmetricEventQueueShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where clientId is not null
        defaultWellmetricEventQueueShouldBeFound("clientId.specified=true");

        // Get all the wellmetricEventQueueList where clientId is null
        defaultWellmetricEventQueueShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where email equals to DEFAULT_EMAIL
        defaultWellmetricEventQueueShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the wellmetricEventQueueList where email equals to UPDATED_EMAIL
        defaultWellmetricEventQueueShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultWellmetricEventQueueShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the wellmetricEventQueueList where email equals to UPDATED_EMAIL
        defaultWellmetricEventQueueShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where email is not null
        defaultWellmetricEventQueueShouldBeFound("email.specified=true");

        // Get all the wellmetricEventQueueList where email is null
        defaultWellmetricEventQueueShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByErrorMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where errorMessage equals to DEFAULT_ERROR_MESSAGE
        defaultWellmetricEventQueueShouldBeFound("errorMessage.equals=" + DEFAULT_ERROR_MESSAGE);

        // Get all the wellmetricEventQueueList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultWellmetricEventQueueShouldNotBeFound("errorMessage.equals=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByErrorMessageIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where errorMessage in DEFAULT_ERROR_MESSAGE or UPDATED_ERROR_MESSAGE
        defaultWellmetricEventQueueShouldBeFound("errorMessage.in=" + DEFAULT_ERROR_MESSAGE + "," + UPDATED_ERROR_MESSAGE);

        // Get all the wellmetricEventQueueList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultWellmetricEventQueueShouldNotBeFound("errorMessage.in=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByErrorMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where errorMessage is not null
        defaultWellmetricEventQueueShouldBeFound("errorMessage.specified=true");

        // Get all the wellmetricEventQueueList where errorMessage is null
        defaultWellmetricEventQueueShouldNotBeFound("errorMessage.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByResultIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where result equals to DEFAULT_RESULT
        defaultWellmetricEventQueueShouldBeFound("result.equals=" + DEFAULT_RESULT);

        // Get all the wellmetricEventQueueList where result equals to UPDATED_RESULT
        defaultWellmetricEventQueueShouldNotBeFound("result.equals=" + UPDATED_RESULT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByResultIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where result in DEFAULT_RESULT or UPDATED_RESULT
        defaultWellmetricEventQueueShouldBeFound("result.in=" + DEFAULT_RESULT + "," + UPDATED_RESULT);

        // Get all the wellmetricEventQueueList where result equals to UPDATED_RESULT
        defaultWellmetricEventQueueShouldNotBeFound("result.in=" + UPDATED_RESULT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByResultIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where result is not null
        defaultWellmetricEventQueueShouldBeFound("result.specified=true");

        // Get all the wellmetricEventQueueList where result is null
        defaultWellmetricEventQueueShouldNotBeFound("result.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventCode equals to DEFAULT_EVENT_CODE
        defaultWellmetricEventQueueShouldBeFound("eventCode.equals=" + DEFAULT_EVENT_CODE);

        // Get all the wellmetricEventQueueList where eventCode equals to UPDATED_EVENT_CODE
        defaultWellmetricEventQueueShouldNotBeFound("eventCode.equals=" + UPDATED_EVENT_CODE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventCodeIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventCode in DEFAULT_EVENT_CODE or UPDATED_EVENT_CODE
        defaultWellmetricEventQueueShouldBeFound("eventCode.in=" + DEFAULT_EVENT_CODE + "," + UPDATED_EVENT_CODE);

        // Get all the wellmetricEventQueueList where eventCode equals to UPDATED_EVENT_CODE
        defaultWellmetricEventQueueShouldNotBeFound("eventCode.in=" + UPDATED_EVENT_CODE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventCode is not null
        defaultWellmetricEventQueueShouldBeFound("eventCode.specified=true");

        // Get all the wellmetricEventQueueList where eventCode is null
        defaultWellmetricEventQueueShouldNotBeFound("eventCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByGenderIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where gender equals to DEFAULT_GENDER
        defaultWellmetricEventQueueShouldBeFound("gender.equals=" + DEFAULT_GENDER);

        // Get all the wellmetricEventQueueList where gender equals to UPDATED_GENDER
        defaultWellmetricEventQueueShouldNotBeFound("gender.equals=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByGenderIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where gender in DEFAULT_GENDER or UPDATED_GENDER
        defaultWellmetricEventQueueShouldBeFound("gender.in=" + DEFAULT_GENDER + "," + UPDATED_GENDER);

        // Get all the wellmetricEventQueueList where gender equals to UPDATED_GENDER
        defaultWellmetricEventQueueShouldNotBeFound("gender.in=" + UPDATED_GENDER);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByGenderIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where gender is not null
        defaultWellmetricEventQueueShouldBeFound("gender.specified=true");

        // Get all the wellmetricEventQueueList where gender is null
        defaultWellmetricEventQueueShouldNotBeFound("gender.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByAttemptCountIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where attemptCount equals to DEFAULT_ATTEMPT_COUNT
        defaultWellmetricEventQueueShouldBeFound("attemptCount.equals=" + DEFAULT_ATTEMPT_COUNT);

        // Get all the wellmetricEventQueueList where attemptCount equals to UPDATED_ATTEMPT_COUNT
        defaultWellmetricEventQueueShouldNotBeFound("attemptCount.equals=" + UPDATED_ATTEMPT_COUNT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByAttemptCountIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where attemptCount in DEFAULT_ATTEMPT_COUNT or UPDATED_ATTEMPT_COUNT
        defaultWellmetricEventQueueShouldBeFound("attemptCount.in=" + DEFAULT_ATTEMPT_COUNT + "," + UPDATED_ATTEMPT_COUNT);

        // Get all the wellmetricEventQueueList where attemptCount equals to UPDATED_ATTEMPT_COUNT
        defaultWellmetricEventQueueShouldNotBeFound("attemptCount.in=" + UPDATED_ATTEMPT_COUNT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByAttemptCountIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where attemptCount is not null
        defaultWellmetricEventQueueShouldBeFound("attemptCount.specified=true");

        // Get all the wellmetricEventQueueList where attemptCount is null
        defaultWellmetricEventQueueShouldNotBeFound("attemptCount.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByAttemptCountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where attemptCount greater than or equals to DEFAULT_ATTEMPT_COUNT
        defaultWellmetricEventQueueShouldBeFound("attemptCount.greaterOrEqualThan=" + DEFAULT_ATTEMPT_COUNT);

        // Get all the wellmetricEventQueueList where attemptCount greater than or equals to UPDATED_ATTEMPT_COUNT
        defaultWellmetricEventQueueShouldNotBeFound("attemptCount.greaterOrEqualThan=" + UPDATED_ATTEMPT_COUNT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByAttemptCountIsLessThanSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where attemptCount less than or equals to DEFAULT_ATTEMPT_COUNT
        defaultWellmetricEventQueueShouldNotBeFound("attemptCount.lessThan=" + DEFAULT_ATTEMPT_COUNT);

        // Get all the wellmetricEventQueueList where attemptCount less than or equals to UPDATED_ATTEMPT_COUNT
        defaultWellmetricEventQueueShouldBeFound("attemptCount.lessThan=" + UPDATED_ATTEMPT_COUNT);
    }


    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByLastAttemptTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where lastAttemptTime equals to DEFAULT_LAST_ATTEMPT_TIME
        defaultWellmetricEventQueueShouldBeFound("lastAttemptTime.equals=" + DEFAULT_LAST_ATTEMPT_TIME);

        // Get all the wellmetricEventQueueList where lastAttemptTime equals to UPDATED_LAST_ATTEMPT_TIME
        defaultWellmetricEventQueueShouldNotBeFound("lastAttemptTime.equals=" + UPDATED_LAST_ATTEMPT_TIME);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByLastAttemptTimeIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where lastAttemptTime in DEFAULT_LAST_ATTEMPT_TIME or UPDATED_LAST_ATTEMPT_TIME
        defaultWellmetricEventQueueShouldBeFound("lastAttemptTime.in=" + DEFAULT_LAST_ATTEMPT_TIME + "," + UPDATED_LAST_ATTEMPT_TIME);

        // Get all the wellmetricEventQueueList where lastAttemptTime equals to UPDATED_LAST_ATTEMPT_TIME
        defaultWellmetricEventQueueShouldNotBeFound("lastAttemptTime.in=" + UPDATED_LAST_ATTEMPT_TIME);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByLastAttemptTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where lastAttemptTime is not null
        defaultWellmetricEventQueueShouldBeFound("lastAttemptTime.specified=true");

        // Get all the wellmetricEventQueueList where lastAttemptTime is null
        defaultWellmetricEventQueueShouldNotBeFound("lastAttemptTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where createdAt equals to DEFAULT_CREATED_AT
        defaultWellmetricEventQueueShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the wellmetricEventQueueList where createdAt equals to UPDATED_CREATED_AT
        defaultWellmetricEventQueueShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultWellmetricEventQueueShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the wellmetricEventQueueList where createdAt equals to UPDATED_CREATED_AT
        defaultWellmetricEventQueueShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where createdAt is not null
        defaultWellmetricEventQueueShouldBeFound("createdAt.specified=true");

        // Get all the wellmetricEventQueueList where createdAt is null
        defaultWellmetricEventQueueShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesBySubgroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where subgroupId equals to DEFAULT_SUBGROUP_ID
        defaultWellmetricEventQueueShouldBeFound("subgroupId.equals=" + DEFAULT_SUBGROUP_ID);

        // Get all the wellmetricEventQueueList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultWellmetricEventQueueShouldNotBeFound("subgroupId.equals=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesBySubgroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where subgroupId in DEFAULT_SUBGROUP_ID or UPDATED_SUBGROUP_ID
        defaultWellmetricEventQueueShouldBeFound("subgroupId.in=" + DEFAULT_SUBGROUP_ID + "," + UPDATED_SUBGROUP_ID);

        // Get all the wellmetricEventQueueList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultWellmetricEventQueueShouldNotBeFound("subgroupId.in=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesBySubgroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where subgroupId is not null
        defaultWellmetricEventQueueShouldBeFound("subgroupId.specified=true");

        // Get all the wellmetricEventQueueList where subgroupId is null
        defaultWellmetricEventQueueShouldNotBeFound("subgroupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByHealthyValueIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where healthyValue equals to DEFAULT_HEALTHY_VALUE
        defaultWellmetricEventQueueShouldBeFound("healthyValue.equals=" + DEFAULT_HEALTHY_VALUE);

        // Get all the wellmetricEventQueueList where healthyValue equals to UPDATED_HEALTHY_VALUE
        defaultWellmetricEventQueueShouldNotBeFound("healthyValue.equals=" + UPDATED_HEALTHY_VALUE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByHealthyValueIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where healthyValue in DEFAULT_HEALTHY_VALUE or UPDATED_HEALTHY_VALUE
        defaultWellmetricEventQueueShouldBeFound("healthyValue.in=" + DEFAULT_HEALTHY_VALUE + "," + UPDATED_HEALTHY_VALUE);

        // Get all the wellmetricEventQueueList where healthyValue equals to UPDATED_HEALTHY_VALUE
        defaultWellmetricEventQueueShouldNotBeFound("healthyValue.in=" + UPDATED_HEALTHY_VALUE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByHealthyValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where healthyValue is not null
        defaultWellmetricEventQueueShouldBeFound("healthyValue.specified=true");

        // Get all the wellmetricEventQueueList where healthyValue is null
        defaultWellmetricEventQueueShouldNotBeFound("healthyValue.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByExecuteStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where executeStatus equals to DEFAULT_EXECUTE_STATUS
        defaultWellmetricEventQueueShouldBeFound("executeStatus.equals=" + DEFAULT_EXECUTE_STATUS);

        // Get all the wellmetricEventQueueList where executeStatus equals to UPDATED_EXECUTE_STATUS
        defaultWellmetricEventQueueShouldNotBeFound("executeStatus.equals=" + UPDATED_EXECUTE_STATUS);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByExecuteStatusIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where executeStatus in DEFAULT_EXECUTE_STATUS or UPDATED_EXECUTE_STATUS
        defaultWellmetricEventQueueShouldBeFound("executeStatus.in=" + DEFAULT_EXECUTE_STATUS + "," + UPDATED_EXECUTE_STATUS);

        // Get all the wellmetricEventQueueList where executeStatus equals to UPDATED_EXECUTE_STATUS
        defaultWellmetricEventQueueShouldNotBeFound("executeStatus.in=" + UPDATED_EXECUTE_STATUS);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByExecuteStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where executeStatus is not null
        defaultWellmetricEventQueueShouldBeFound("executeStatus.specified=true");

        // Get all the wellmetricEventQueueList where executeStatus is null
        defaultWellmetricEventQueueShouldNotBeFound("executeStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventIdIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventId equals to DEFAULT_EVENT_ID
        defaultWellmetricEventQueueShouldBeFound("eventId.equals=" + DEFAULT_EVENT_ID);

        // Get all the wellmetricEventQueueList where eventId equals to UPDATED_EVENT_ID
        defaultWellmetricEventQueueShouldNotBeFound("eventId.equals=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventIdIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventId in DEFAULT_EVENT_ID or UPDATED_EVENT_ID
        defaultWellmetricEventQueueShouldBeFound("eventId.in=" + DEFAULT_EVENT_ID + "," + UPDATED_EVENT_ID);

        // Get all the wellmetricEventQueueList where eventId equals to UPDATED_EVENT_ID
        defaultWellmetricEventQueueShouldNotBeFound("eventId.in=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventId is not null
        defaultWellmetricEventQueueShouldBeFound("eventId.specified=true");

        // Get all the wellmetricEventQueueList where eventId is null
        defaultWellmetricEventQueueShouldNotBeFound("eventId.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByHasIncentiveIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where hasIncentive equals to DEFAULT_HAS_INCENTIVE
        defaultWellmetricEventQueueShouldBeFound("hasIncentive.equals=" + DEFAULT_HAS_INCENTIVE);

        // Get all the wellmetricEventQueueList where hasIncentive equals to UPDATED_HAS_INCENTIVE
        defaultWellmetricEventQueueShouldNotBeFound("hasIncentive.equals=" + UPDATED_HAS_INCENTIVE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByHasIncentiveIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where hasIncentive in DEFAULT_HAS_INCENTIVE or UPDATED_HAS_INCENTIVE
        defaultWellmetricEventQueueShouldBeFound("hasIncentive.in=" + DEFAULT_HAS_INCENTIVE + "," + UPDATED_HAS_INCENTIVE);

        // Get all the wellmetricEventQueueList where hasIncentive equals to UPDATED_HAS_INCENTIVE
        defaultWellmetricEventQueueShouldNotBeFound("hasIncentive.in=" + UPDATED_HAS_INCENTIVE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByHasIncentiveIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where hasIncentive is not null
        defaultWellmetricEventQueueShouldBeFound("hasIncentive.specified=true");

        // Get all the wellmetricEventQueueList where hasIncentive is null
        defaultWellmetricEventQueueShouldNotBeFound("hasIncentive.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByIsWaitingPushIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where isWaitingPush equals to DEFAULT_IS_WAITING_PUSH
        defaultWellmetricEventQueueShouldBeFound("isWaitingPush.equals=" + DEFAULT_IS_WAITING_PUSH);

        // Get all the wellmetricEventQueueList where isWaitingPush equals to UPDATED_IS_WAITING_PUSH
        defaultWellmetricEventQueueShouldNotBeFound("isWaitingPush.equals=" + UPDATED_IS_WAITING_PUSH);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByIsWaitingPushIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where isWaitingPush in DEFAULT_IS_WAITING_PUSH or UPDATED_IS_WAITING_PUSH
        defaultWellmetricEventQueueShouldBeFound("isWaitingPush.in=" + DEFAULT_IS_WAITING_PUSH + "," + UPDATED_IS_WAITING_PUSH);

        // Get all the wellmetricEventQueueList where isWaitingPush equals to UPDATED_IS_WAITING_PUSH
        defaultWellmetricEventQueueShouldNotBeFound("isWaitingPush.in=" + UPDATED_IS_WAITING_PUSH);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByIsWaitingPushIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where isWaitingPush is not null
        defaultWellmetricEventQueueShouldBeFound("isWaitingPush.specified=true");

        // Get all the wellmetricEventQueueList where isWaitingPush is null
        defaultWellmetricEventQueueShouldNotBeFound("isWaitingPush.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByAttemptedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where attemptedAt equals to DEFAULT_ATTEMPTED_AT
        defaultWellmetricEventQueueShouldBeFound("attemptedAt.equals=" + DEFAULT_ATTEMPTED_AT);

        // Get all the wellmetricEventQueueList where attemptedAt equals to UPDATED_ATTEMPTED_AT
        defaultWellmetricEventQueueShouldNotBeFound("attemptedAt.equals=" + UPDATED_ATTEMPTED_AT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByAttemptedAtIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where attemptedAt in DEFAULT_ATTEMPTED_AT or UPDATED_ATTEMPTED_AT
        defaultWellmetricEventQueueShouldBeFound("attemptedAt.in=" + DEFAULT_ATTEMPTED_AT + "," + UPDATED_ATTEMPTED_AT);

        // Get all the wellmetricEventQueueList where attemptedAt equals to UPDATED_ATTEMPTED_AT
        defaultWellmetricEventQueueShouldNotBeFound("attemptedAt.in=" + UPDATED_ATTEMPTED_AT);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByAttemptedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where attemptedAt is not null
        defaultWellmetricEventQueueShouldBeFound("attemptedAt.specified=true");

        // Get all the wellmetricEventQueueList where attemptedAt is null
        defaultWellmetricEventQueueShouldNotBeFound("attemptedAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByProgramIdIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where programId equals to DEFAULT_PROGRAM_ID
        defaultWellmetricEventQueueShouldBeFound("programId.equals=" + DEFAULT_PROGRAM_ID);

        // Get all the wellmetricEventQueueList where programId equals to UPDATED_PROGRAM_ID
        defaultWellmetricEventQueueShouldNotBeFound("programId.equals=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByProgramIdIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where programId in DEFAULT_PROGRAM_ID or UPDATED_PROGRAM_ID
        defaultWellmetricEventQueueShouldBeFound("programId.in=" + DEFAULT_PROGRAM_ID + "," + UPDATED_PROGRAM_ID);

        // Get all the wellmetricEventQueueList where programId equals to UPDATED_PROGRAM_ID
        defaultWellmetricEventQueueShouldNotBeFound("programId.in=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByProgramIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where programId is not null
        defaultWellmetricEventQueueShouldBeFound("programId.specified=true");

        // Get all the wellmetricEventQueueList where programId is null
        defaultWellmetricEventQueueShouldNotBeFound("programId.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByProgramIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where programId greater than or equals to DEFAULT_PROGRAM_ID
        defaultWellmetricEventQueueShouldBeFound("programId.greaterOrEqualThan=" + DEFAULT_PROGRAM_ID);

        // Get all the wellmetricEventQueueList where programId greater than or equals to UPDATED_PROGRAM_ID
        defaultWellmetricEventQueueShouldNotBeFound("programId.greaterOrEqualThan=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByProgramIdIsLessThanSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where programId less than or equals to DEFAULT_PROGRAM_ID
        defaultWellmetricEventQueueShouldNotBeFound("programId.lessThan=" + DEFAULT_PROGRAM_ID);

        // Get all the wellmetricEventQueueList where programId less than or equals to UPDATED_PROGRAM_ID
        defaultWellmetricEventQueueShouldBeFound("programId.lessThan=" + UPDATED_PROGRAM_ID);
    }


    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventDateIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventDate equals to DEFAULT_EVENT_DATE
        defaultWellmetricEventQueueShouldBeFound("eventDate.equals=" + DEFAULT_EVENT_DATE);

        // Get all the wellmetricEventQueueList where eventDate equals to UPDATED_EVENT_DATE
        defaultWellmetricEventQueueShouldNotBeFound("eventDate.equals=" + UPDATED_EVENT_DATE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventDateIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventDate in DEFAULT_EVENT_DATE or UPDATED_EVENT_DATE
        defaultWellmetricEventQueueShouldBeFound("eventDate.in=" + DEFAULT_EVENT_DATE + "," + UPDATED_EVENT_DATE);

        // Get all the wellmetricEventQueueList where eventDate equals to UPDATED_EVENT_DATE
        defaultWellmetricEventQueueShouldNotBeFound("eventDate.in=" + UPDATED_EVENT_DATE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesByEventDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where eventDate is not null
        defaultWellmetricEventQueueShouldBeFound("eventDate.specified=true");

        // Get all the wellmetricEventQueueList where eventDate is null
        defaultWellmetricEventQueueShouldNotBeFound("eventDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesBySubCategoryCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where subCategoryCode equals to DEFAULT_SUB_CATEGORY_CODE
        defaultWellmetricEventQueueShouldBeFound("subCategoryCode.equals=" + DEFAULT_SUB_CATEGORY_CODE);

        // Get all the wellmetricEventQueueList where subCategoryCode equals to UPDATED_SUB_CATEGORY_CODE
        defaultWellmetricEventQueueShouldNotBeFound("subCategoryCode.equals=" + UPDATED_SUB_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesBySubCategoryCodeIsInShouldWork() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where subCategoryCode in DEFAULT_SUB_CATEGORY_CODE or UPDATED_SUB_CATEGORY_CODE
        defaultWellmetricEventQueueShouldBeFound("subCategoryCode.in=" + DEFAULT_SUB_CATEGORY_CODE + "," + UPDATED_SUB_CATEGORY_CODE);

        // Get all the wellmetricEventQueueList where subCategoryCode equals to UPDATED_SUB_CATEGORY_CODE
        defaultWellmetricEventQueueShouldNotBeFound("subCategoryCode.in=" + UPDATED_SUB_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void getAllWellmetricEventQueuesBySubCategoryCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        // Get all the wellmetricEventQueueList where subCategoryCode is not null
        defaultWellmetricEventQueueShouldBeFound("subCategoryCode.specified=true");

        // Get all the wellmetricEventQueueList where subCategoryCode is null
        defaultWellmetricEventQueueShouldNotBeFound("subCategoryCode.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultWellmetricEventQueueShouldBeFound(String filter) throws Exception {
        restWellmetricEventQueueMockMvc.perform(get("/api/wellmetric-event-queues?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(wellmetricEventQueue.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID)))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE)))
            .andExpect(jsonPath("$.[*].result").value(hasItem(DEFAULT_RESULT.toString())))
            .andExpect(jsonPath("$.[*].eventCode").value(hasItem(DEFAULT_EVENT_CODE)))
            .andExpect(jsonPath("$.[*].gender").value(hasItem(DEFAULT_GENDER.toString())))
            .andExpect(jsonPath("$.[*].attemptCount").value(hasItem(DEFAULT_ATTEMPT_COUNT)))
            .andExpect(jsonPath("$.[*].lastAttemptTime").value(hasItem(DEFAULT_LAST_ATTEMPT_TIME.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID)))
            .andExpect(jsonPath("$.[*].healthyValue").value(hasItem(DEFAULT_HEALTHY_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].executeStatus").value(hasItem(DEFAULT_EXECUTE_STATUS)))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID)))
            .andExpect(jsonPath("$.[*].hasIncentive").value(hasItem(DEFAULT_HAS_INCENTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].isWaitingPush").value(hasItem(DEFAULT_IS_WAITING_PUSH.booleanValue())))
            .andExpect(jsonPath("$.[*].attemptedAt").value(hasItem(DEFAULT_ATTEMPTED_AT.toString())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].eventDate").value(hasItem(DEFAULT_EVENT_DATE)))
            .andExpect(jsonPath("$.[*].subCategoryCode").value(hasItem(DEFAULT_SUB_CATEGORY_CODE)));

        // Check, that the count call also returns 1
        restWellmetricEventQueueMockMvc.perform(get("/api/wellmetric-event-queues/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultWellmetricEventQueueShouldNotBeFound(String filter) throws Exception {
        restWellmetricEventQueueMockMvc.perform(get("/api/wellmetric-event-queues?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restWellmetricEventQueueMockMvc.perform(get("/api/wellmetric-event-queues/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingWellmetricEventQueue() throws Exception {
        // Get the wellmetricEventQueue
        restWellmetricEventQueueMockMvc.perform(get("/api/wellmetric-event-queues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateWellmetricEventQueue() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        int databaseSizeBeforeUpdate = wellmetricEventQueueRepository.findAll().size();

        // Update the wellmetricEventQueue
        WellmetricEventQueue updatedWellmetricEventQueue = wellmetricEventQueueRepository.findById(wellmetricEventQueue.getId()).get();
        // Disconnect from session so that the updates on updatedWellmetricEventQueue are not directly saved in db
        em.detach(updatedWellmetricEventQueue);
        updatedWellmetricEventQueue
            .participantId(UPDATED_PARTICIPANT_ID)
            .clientId(UPDATED_CLIENT_ID)
            .email(UPDATED_EMAIL)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .result(UPDATED_RESULT)
            .eventCode(UPDATED_EVENT_CODE)
            .gender(UPDATED_GENDER)
            .attemptCount(UPDATED_ATTEMPT_COUNT)
            .lastAttemptTime(UPDATED_LAST_ATTEMPT_TIME)
            .createdAt(UPDATED_CREATED_AT)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .healthyValue(UPDATED_HEALTHY_VALUE)
            .executeStatus(UPDATED_EXECUTE_STATUS)
            .eventId(UPDATED_EVENT_ID)
            .hasIncentive(UPDATED_HAS_INCENTIVE)
            .isWaitingPush(UPDATED_IS_WAITING_PUSH)
            .attemptedAt(UPDATED_ATTEMPTED_AT)
            .programId(UPDATED_PROGRAM_ID)
            .eventDate(UPDATED_EVENT_DATE)
            .subCategoryCode(UPDATED_SUB_CATEGORY_CODE);
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(updatedWellmetricEventQueue);

        restWellmetricEventQueueMockMvc.perform(put("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isOk());

        // Validate the WellmetricEventQueue in the database
        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeUpdate);
        WellmetricEventQueue testWellmetricEventQueue = wellmetricEventQueueList.get(wellmetricEventQueueList.size() - 1);
        assertThat(testWellmetricEventQueue.getParticipantId()).isEqualTo(UPDATED_PARTICIPANT_ID);
        assertThat(testWellmetricEventQueue.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testWellmetricEventQueue.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testWellmetricEventQueue.getErrorMessage()).isEqualTo(UPDATED_ERROR_MESSAGE);
        assertThat(testWellmetricEventQueue.getResult()).isEqualTo(UPDATED_RESULT);
        assertThat(testWellmetricEventQueue.getEventCode()).isEqualTo(UPDATED_EVENT_CODE);
        assertThat(testWellmetricEventQueue.getGender()).isEqualTo(UPDATED_GENDER);
        assertThat(testWellmetricEventQueue.getAttemptCount()).isEqualTo(UPDATED_ATTEMPT_COUNT);
        assertThat(testWellmetricEventQueue.getLastAttemptTime()).isEqualTo(UPDATED_LAST_ATTEMPT_TIME);
        assertThat(testWellmetricEventQueue.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testWellmetricEventQueue.getSubgroupId()).isEqualTo(UPDATED_SUBGROUP_ID);
        assertThat(testWellmetricEventQueue.getHealthyValue()).isEqualTo(UPDATED_HEALTHY_VALUE);
        assertThat(testWellmetricEventQueue.getExecuteStatus()).isEqualTo(UPDATED_EXECUTE_STATUS);
        assertThat(testWellmetricEventQueue.getEventId()).isEqualTo(UPDATED_EVENT_ID);
        assertThat(testWellmetricEventQueue.isHasIncentive()).isEqualTo(UPDATED_HAS_INCENTIVE);
        assertThat(testWellmetricEventQueue.isIsWaitingPush()).isEqualTo(UPDATED_IS_WAITING_PUSH);
        assertThat(testWellmetricEventQueue.getAttemptedAt()).isEqualTo(UPDATED_ATTEMPTED_AT);
        assertThat(testWellmetricEventQueue.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
        assertThat(testWellmetricEventQueue.getEventDate()).isEqualTo(UPDATED_EVENT_DATE);
        assertThat(testWellmetricEventQueue.getSubCategoryCode()).isEqualTo(UPDATED_SUB_CATEGORY_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingWellmetricEventQueue() throws Exception {
        int databaseSizeBeforeUpdate = wellmetricEventQueueRepository.findAll().size();

        // Create the WellmetricEventQueue
        WellmetricEventQueueDTO wellmetricEventQueueDTO = wellmetricEventQueueMapper.toDto(wellmetricEventQueue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWellmetricEventQueueMockMvc.perform(put("/api/wellmetric-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(wellmetricEventQueueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the WellmetricEventQueue in the database
        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteWellmetricEventQueue() throws Exception {
        // Initialize the database
        wellmetricEventQueueRepository.saveAndFlush(wellmetricEventQueue);

        int databaseSizeBeforeDelete = wellmetricEventQueueRepository.findAll().size();

        // Delete the wellmetricEventQueue
        restWellmetricEventQueueMockMvc.perform(delete("/api/wellmetric-event-queues/{id}", wellmetricEventQueue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<WellmetricEventQueue> wellmetricEventQueueList = wellmetricEventQueueRepository.findAll();
        assertThat(wellmetricEventQueueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WellmetricEventQueue.class);
        WellmetricEventQueue wellmetricEventQueue1 = new WellmetricEventQueue();
        wellmetricEventQueue1.setId(1L);
        WellmetricEventQueue wellmetricEventQueue2 = new WellmetricEventQueue();
        wellmetricEventQueue2.setId(wellmetricEventQueue1.getId());
        assertThat(wellmetricEventQueue1).isEqualTo(wellmetricEventQueue2);
        wellmetricEventQueue2.setId(2L);
        assertThat(wellmetricEventQueue1).isNotEqualTo(wellmetricEventQueue2);
        wellmetricEventQueue1.setId(null);
        assertThat(wellmetricEventQueue1).isNotEqualTo(wellmetricEventQueue2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(WellmetricEventQueueDTO.class);
        WellmetricEventQueueDTO wellmetricEventQueueDTO1 = new WellmetricEventQueueDTO();
        wellmetricEventQueueDTO1.setId(1L);
        WellmetricEventQueueDTO wellmetricEventQueueDTO2 = new WellmetricEventQueueDTO();
        assertThat(wellmetricEventQueueDTO1).isNotEqualTo(wellmetricEventQueueDTO2);
        wellmetricEventQueueDTO2.setId(wellmetricEventQueueDTO1.getId());
        assertThat(wellmetricEventQueueDTO1).isEqualTo(wellmetricEventQueueDTO2);
        wellmetricEventQueueDTO2.setId(2L);
        assertThat(wellmetricEventQueueDTO1).isNotEqualTo(wellmetricEventQueueDTO2);
        wellmetricEventQueueDTO1.setId(null);
        assertThat(wellmetricEventQueueDTO1).isNotEqualTo(wellmetricEventQueueDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(wellmetricEventQueueMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(wellmetricEventQueueMapper.fromId(null)).isNull();
    }
}
