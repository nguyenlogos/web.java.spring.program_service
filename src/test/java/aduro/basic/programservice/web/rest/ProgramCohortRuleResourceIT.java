package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramCohortRule;
import aduro.basic.programservice.domain.ProgramCohort;
import aduro.basic.programservice.repository.ProgramCohortRuleRepository;
import aduro.basic.programservice.service.ProgramCohortRuleService;
import aduro.basic.programservice.service.dto.ProgramCohortRuleDTO;
import aduro.basic.programservice.service.mapper.ProgramCohortRuleMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramCohortRuleCriteria;
import aduro.basic.programservice.service.ProgramCohortRuleQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramCohortRuleResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramCohortRuleResourceIT {

    private static final String DEFAULT_DATA_INPUT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_DATA_INPUT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_RULES = "AAAAAAAAAA";
    private static final String UPDATED_RULES = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramCohortRuleRepository programCohortRuleRepository;

    @Autowired
    private ProgramCohortRuleMapper programCohortRuleMapper;

    @Autowired
    private ProgramCohortRuleService programCohortRuleService;

    @Autowired
    private ProgramCohortRuleQueryService programCohortRuleQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramCohortRuleMockMvc;

    private ProgramCohortRule programCohortRule;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramCohortRuleResource programCohortRuleResource = new ProgramCohortRuleResource(programCohortRuleService, programCohortRuleQueryService);
        this.restProgramCohortRuleMockMvc = MockMvcBuilders.standaloneSetup(programCohortRuleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCohortRule createEntity(EntityManager em) {
        ProgramCohortRule programCohortRule = new ProgramCohortRule()
            .dataInputType(DEFAULT_DATA_INPUT_TYPE)
            .rules(DEFAULT_RULES)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        return programCohortRule;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCohortRule createUpdatedEntity(EntityManager em) {
        ProgramCohortRule programCohortRule = new ProgramCohortRule()
            .dataInputType(UPDATED_DATA_INPUT_TYPE)
            .rules(UPDATED_RULES)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        return programCohortRule;
    }

    @BeforeEach
    public void initTest() {
        programCohortRule = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramCohortRule() throws Exception {
        int databaseSizeBeforeCreate = programCohortRuleRepository.findAll().size();

        // Create the ProgramCohortRule
        ProgramCohortRuleDTO programCohortRuleDTO = programCohortRuleMapper.toDto(programCohortRule);
        restProgramCohortRuleMockMvc.perform(post("/api/program-cohort-rules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortRuleDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramCohortRule in the database
        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramCohortRule testProgramCohortRule = programCohortRuleList.get(programCohortRuleList.size() - 1);
        assertThat(testProgramCohortRule.getDataInputType()).isEqualTo(DEFAULT_DATA_INPUT_TYPE);
        assertThat(testProgramCohortRule.getRules()).isEqualTo(DEFAULT_RULES);
        assertThat(testProgramCohortRule.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramCohortRule.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramCohortRuleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programCohortRuleRepository.findAll().size();

        // Create the ProgramCohortRule with an existing ID
        programCohortRule.setId(1L);
        ProgramCohortRuleDTO programCohortRuleDTO = programCohortRuleMapper.toDto(programCohortRule);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramCohortRuleMockMvc.perform(post("/api/program-cohort-rules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortRuleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCohortRule in the database
        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDataInputTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortRuleRepository.findAll().size();
        // set the field null
        programCohortRule.setDataInputType(null);

        // Create the ProgramCohortRule, which fails.
        ProgramCohortRuleDTO programCohortRuleDTO = programCohortRuleMapper.toDto(programCohortRule);

        restProgramCohortRuleMockMvc.perform(post("/api/program-cohort-rules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortRuleDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRulesIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortRuleRepository.findAll().size();
        // set the field null
        programCohortRule.setRules(null);

        // Create the ProgramCohortRule, which fails.
        ProgramCohortRuleDTO programCohortRuleDTO = programCohortRuleMapper.toDto(programCohortRule);

        restProgramCohortRuleMockMvc.perform(post("/api/program-cohort-rules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortRuleDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortRuleRepository.findAll().size();
        // set the field null
        programCohortRule.setCreatedDate(null);

        // Create the ProgramCohortRule, which fails.
        ProgramCohortRuleDTO programCohortRuleDTO = programCohortRuleMapper.toDto(programCohortRule);

        restProgramCohortRuleMockMvc.perform(post("/api/program-cohort-rules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortRuleDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkModifiedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCohortRuleRepository.findAll().size();
        // set the field null
        programCohortRule.setModifiedDate(null);

        // Create the ProgramCohortRule, which fails.
        ProgramCohortRuleDTO programCohortRuleDTO = programCohortRuleMapper.toDto(programCohortRule);

        restProgramCohortRuleMockMvc.perform(post("/api/program-cohort-rules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortRuleDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRules() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList
        restProgramCohortRuleMockMvc.perform(get("/api/program-cohort-rules?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCohortRule.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataInputType").value(hasItem(DEFAULT_DATA_INPUT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].rules").value(hasItem(DEFAULT_RULES.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramCohortRule() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get the programCohortRule
        restProgramCohortRuleMockMvc.perform(get("/api/program-cohort-rules/{id}", programCohortRule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programCohortRule.getId().intValue()))
            .andExpect(jsonPath("$.dataInputType").value(DEFAULT_DATA_INPUT_TYPE.toString()))
            .andExpect(jsonPath("$.rules").value(DEFAULT_RULES.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByDataInputTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where dataInputType equals to DEFAULT_DATA_INPUT_TYPE
        defaultProgramCohortRuleShouldBeFound("dataInputType.equals=" + DEFAULT_DATA_INPUT_TYPE);

        // Get all the programCohortRuleList where dataInputType equals to UPDATED_DATA_INPUT_TYPE
        defaultProgramCohortRuleShouldNotBeFound("dataInputType.equals=" + UPDATED_DATA_INPUT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByDataInputTypeIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where dataInputType in DEFAULT_DATA_INPUT_TYPE or UPDATED_DATA_INPUT_TYPE
        defaultProgramCohortRuleShouldBeFound("dataInputType.in=" + DEFAULT_DATA_INPUT_TYPE + "," + UPDATED_DATA_INPUT_TYPE);

        // Get all the programCohortRuleList where dataInputType equals to UPDATED_DATA_INPUT_TYPE
        defaultProgramCohortRuleShouldNotBeFound("dataInputType.in=" + UPDATED_DATA_INPUT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByDataInputTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where dataInputType is not null
        defaultProgramCohortRuleShouldBeFound("dataInputType.specified=true");

        // Get all the programCohortRuleList where dataInputType is null
        defaultProgramCohortRuleShouldNotBeFound("dataInputType.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByRulesIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where rules equals to DEFAULT_RULES
        defaultProgramCohortRuleShouldBeFound("rules.equals=" + DEFAULT_RULES);

        // Get all the programCohortRuleList where rules equals to UPDATED_RULES
        defaultProgramCohortRuleShouldNotBeFound("rules.equals=" + UPDATED_RULES);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByRulesIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where rules in DEFAULT_RULES or UPDATED_RULES
        defaultProgramCohortRuleShouldBeFound("rules.in=" + DEFAULT_RULES + "," + UPDATED_RULES);

        // Get all the programCohortRuleList where rules equals to UPDATED_RULES
        defaultProgramCohortRuleShouldNotBeFound("rules.in=" + UPDATED_RULES);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByRulesIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where rules is not null
        defaultProgramCohortRuleShouldBeFound("rules.specified=true");

        // Get all the programCohortRuleList where rules is null
        defaultProgramCohortRuleShouldNotBeFound("rules.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramCohortRuleShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programCohortRuleList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCohortRuleShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramCohortRuleShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programCohortRuleList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCohortRuleShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where createdDate is not null
        defaultProgramCohortRuleShouldBeFound("createdDate.specified=true");

        // Get all the programCohortRuleList where createdDate is null
        defaultProgramCohortRuleShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramCohortRuleShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programCohortRuleList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCohortRuleShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramCohortRuleShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programCohortRuleList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCohortRuleShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        // Get all the programCohortRuleList where modifiedDate is not null
        defaultProgramCohortRuleShouldBeFound("modifiedDate.specified=true");

        // Get all the programCohortRuleList where modifiedDate is null
        defaultProgramCohortRuleShouldNotBeFound("modifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCohortRulesByProgramCohortIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCohort programCohort = ProgramCohortResourceIT.createEntity(em);
        em.persist(programCohort);
        em.flush();
        programCohortRule.setProgramCohort(programCohort);
        programCohortRuleRepository.saveAndFlush(programCohortRule);
        Long programCohortId = programCohort.getId();

        // Get all the programCohortRuleList where programCohort equals to programCohortId
        defaultProgramCohortRuleShouldBeFound("programCohortId.equals=" + programCohortId);

        // Get all the programCohortRuleList where programCohort equals to programCohortId + 1
        defaultProgramCohortRuleShouldNotBeFound("programCohortId.equals=" + (programCohortId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramCohortRuleShouldBeFound(String filter) throws Exception {
        restProgramCohortRuleMockMvc.perform(get("/api/program-cohort-rules?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCohortRule.getId().intValue())))
            .andExpect(jsonPath("$.[*].dataInputType").value(hasItem(DEFAULT_DATA_INPUT_TYPE)))
            .andExpect(jsonPath("$.[*].rules").value(hasItem(DEFAULT_RULES)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramCohortRuleMockMvc.perform(get("/api/program-cohort-rules/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramCohortRuleShouldNotBeFound(String filter) throws Exception {
        restProgramCohortRuleMockMvc.perform(get("/api/program-cohort-rules?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramCohortRuleMockMvc.perform(get("/api/program-cohort-rules/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramCohortRule() throws Exception {
        // Get the programCohortRule
        restProgramCohortRuleMockMvc.perform(get("/api/program-cohort-rules/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramCohortRule() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        int databaseSizeBeforeUpdate = programCohortRuleRepository.findAll().size();

        // Update the programCohortRule
        ProgramCohortRule updatedProgramCohortRule = programCohortRuleRepository.findById(programCohortRule.getId()).get();
        // Disconnect from session so that the updates on updatedProgramCohortRule are not directly saved in db
        em.detach(updatedProgramCohortRule);
        updatedProgramCohortRule
            .dataInputType(UPDATED_DATA_INPUT_TYPE)
            .rules(UPDATED_RULES)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        ProgramCohortRuleDTO programCohortRuleDTO = programCohortRuleMapper.toDto(updatedProgramCohortRule);

        restProgramCohortRuleMockMvc.perform(put("/api/program-cohort-rules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortRuleDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramCohortRule in the database
        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeUpdate);
        ProgramCohortRule testProgramCohortRule = programCohortRuleList.get(programCohortRuleList.size() - 1);
        assertThat(testProgramCohortRule.getDataInputType()).isEqualTo(UPDATED_DATA_INPUT_TYPE);
        assertThat(testProgramCohortRule.getRules()).isEqualTo(UPDATED_RULES);
        assertThat(testProgramCohortRule.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramCohortRule.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramCohortRule() throws Exception {
        int databaseSizeBeforeUpdate = programCohortRuleRepository.findAll().size();

        // Create the ProgramCohortRule
        ProgramCohortRuleDTO programCohortRuleDTO = programCohortRuleMapper.toDto(programCohortRule);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramCohortRuleMockMvc.perform(put("/api/program-cohort-rules")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCohortRuleDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCohortRule in the database
        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramCohortRule() throws Exception {
        // Initialize the database
        programCohortRuleRepository.saveAndFlush(programCohortRule);

        int databaseSizeBeforeDelete = programCohortRuleRepository.findAll().size();

        // Delete the programCohortRule
        restProgramCohortRuleMockMvc.perform(delete("/api/program-cohort-rules/{id}", programCohortRule.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramCohortRule> programCohortRuleList = programCohortRuleRepository.findAll();
        assertThat(programCohortRuleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCohortRule.class);
        ProgramCohortRule programCohortRule1 = new ProgramCohortRule();
        programCohortRule1.setId(1L);
        ProgramCohortRule programCohortRule2 = new ProgramCohortRule();
        programCohortRule2.setId(programCohortRule1.getId());
        assertThat(programCohortRule1).isEqualTo(programCohortRule2);
        programCohortRule2.setId(2L);
        assertThat(programCohortRule1).isNotEqualTo(programCohortRule2);
        programCohortRule1.setId(null);
        assertThat(programCohortRule1).isNotEqualTo(programCohortRule2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCohortRuleDTO.class);
        ProgramCohortRuleDTO programCohortRuleDTO1 = new ProgramCohortRuleDTO();
        programCohortRuleDTO1.setId(1L);
        ProgramCohortRuleDTO programCohortRuleDTO2 = new ProgramCohortRuleDTO();
        assertThat(programCohortRuleDTO1).isNotEqualTo(programCohortRuleDTO2);
        programCohortRuleDTO2.setId(programCohortRuleDTO1.getId());
        assertThat(programCohortRuleDTO1).isEqualTo(programCohortRuleDTO2);
        programCohortRuleDTO2.setId(2L);
        assertThat(programCohortRuleDTO1).isNotEqualTo(programCohortRuleDTO2);
        programCohortRuleDTO1.setId(null);
        assertThat(programCohortRuleDTO1).isNotEqualTo(programCohortRuleDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programCohortRuleMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programCohortRuleMapper.fromId(null)).isNull();
    }
}
