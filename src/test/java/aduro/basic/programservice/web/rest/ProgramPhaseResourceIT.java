package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramPhase;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ProgramPhaseRepository;
import aduro.basic.programservice.service.ProgramPhaseService;
import aduro.basic.programservice.service.dto.ProgramPhaseDTO;
import aduro.basic.programservice.service.mapper.ProgramPhaseMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramPhaseCriteria;
import aduro.basic.programservice.service.ProgramPhaseQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramPhaseResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramPhaseResourceIT {

    private static final Integer DEFAULT_PHASE_ORDER = 1;
    private static final Integer UPDATED_PHASE_ORDER = 2;

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramPhaseRepository programPhaseRepository;

    @Autowired
    private ProgramPhaseMapper programPhaseMapper;

    @Autowired
    private ProgramPhaseService programPhaseService;

    @Autowired
    private ProgramPhaseQueryService programPhaseQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramPhaseMockMvc;

    private ProgramPhase programPhase;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramPhaseResource programPhaseResource = new ProgramPhaseResource(programPhaseService, programPhaseQueryService);
        this.restProgramPhaseMockMvc = MockMvcBuilders.standaloneSetup(programPhaseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramPhase createEntity(EntityManager em) {
        ProgramPhase programPhase = new ProgramPhase()
            .phaseOrder(DEFAULT_PHASE_ORDER)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        programPhase.setProgram(program);
        return programPhase;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramPhase createUpdatedEntity(EntityManager em) {
        ProgramPhase programPhase = new ProgramPhase()
            .phaseOrder(UPDATED_PHASE_ORDER)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createUpdatedEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        programPhase.setProgram(program);
        return programPhase;
    }

    @BeforeEach
    public void initTest() {
        programPhase = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramPhase() throws Exception {
        int databaseSizeBeforeCreate = programPhaseRepository.findAll().size();

        // Create the ProgramPhase
        ProgramPhaseDTO programPhaseDTO = programPhaseMapper.toDto(programPhase);
        restProgramPhaseMockMvc.perform(post("/api/program-phases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programPhaseDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramPhase in the database
        List<ProgramPhase> programPhaseList = programPhaseRepository.findAll();
        assertThat(programPhaseList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramPhase testProgramPhase = programPhaseList.get(programPhaseList.size() - 1);
        assertThat(testProgramPhase.getPhaseOrder()).isEqualTo(DEFAULT_PHASE_ORDER);
        assertThat(testProgramPhase.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testProgramPhase.getEndDate()).isEqualTo(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    public void createProgramPhaseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programPhaseRepository.findAll().size();

        // Create the ProgramPhase with an existing ID
        programPhase.setId(1L);
        ProgramPhaseDTO programPhaseDTO = programPhaseMapper.toDto(programPhase);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramPhaseMockMvc.perform(post("/api/program-phases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programPhaseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramPhase in the database
        List<ProgramPhase> programPhaseList = programPhaseRepository.findAll();
        assertThat(programPhaseList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkPhaseOrderIsRequired() throws Exception {
        int databaseSizeBeforeTest = programPhaseRepository.findAll().size();
        // set the field null
        programPhase.setPhaseOrder(null);

        // Create the ProgramPhase, which fails.
        ProgramPhaseDTO programPhaseDTO = programPhaseMapper.toDto(programPhase);

        restProgramPhaseMockMvc.perform(post("/api/program-phases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programPhaseDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramPhase> programPhaseList = programPhaseRepository.findAll();
        assertThat(programPhaseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramPhases() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList
        restProgramPhaseMockMvc.perform(get("/api/program-phases?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programPhase.getId().intValue())))
            .andExpect(jsonPath("$.[*].phaseOrder").value(hasItem(DEFAULT_PHASE_ORDER)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramPhase() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get the programPhase
        restProgramPhaseMockMvc.perform(get("/api/program-phases/{id}", programPhase.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programPhase.getId().intValue()))
            .andExpect(jsonPath("$.phaseOrder").value(DEFAULT_PHASE_ORDER))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByPhaseOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where phaseOrder equals to DEFAULT_PHASE_ORDER
        defaultProgramPhaseShouldBeFound("phaseOrder.equals=" + DEFAULT_PHASE_ORDER);

        // Get all the programPhaseList where phaseOrder equals to UPDATED_PHASE_ORDER
        defaultProgramPhaseShouldNotBeFound("phaseOrder.equals=" + UPDATED_PHASE_ORDER);
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByPhaseOrderIsInShouldWork() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where phaseOrder in DEFAULT_PHASE_ORDER or UPDATED_PHASE_ORDER
        defaultProgramPhaseShouldBeFound("phaseOrder.in=" + DEFAULT_PHASE_ORDER + "," + UPDATED_PHASE_ORDER);

        // Get all the programPhaseList where phaseOrder equals to UPDATED_PHASE_ORDER
        defaultProgramPhaseShouldNotBeFound("phaseOrder.in=" + UPDATED_PHASE_ORDER);
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByPhaseOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where phaseOrder is not null
        defaultProgramPhaseShouldBeFound("phaseOrder.specified=true");

        // Get all the programPhaseList where phaseOrder is null
        defaultProgramPhaseShouldNotBeFound("phaseOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByPhaseOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where phaseOrder greater than or equals to DEFAULT_PHASE_ORDER
        defaultProgramPhaseShouldBeFound("phaseOrder.greaterOrEqualThan=" + DEFAULT_PHASE_ORDER);

        // Get all the programPhaseList where phaseOrder greater than or equals to UPDATED_PHASE_ORDER
        defaultProgramPhaseShouldNotBeFound("phaseOrder.greaterOrEqualThan=" + UPDATED_PHASE_ORDER);
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByPhaseOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where phaseOrder less than or equals to DEFAULT_PHASE_ORDER
        defaultProgramPhaseShouldNotBeFound("phaseOrder.lessThan=" + DEFAULT_PHASE_ORDER);

        // Get all the programPhaseList where phaseOrder less than or equals to UPDATED_PHASE_ORDER
        defaultProgramPhaseShouldBeFound("phaseOrder.lessThan=" + UPDATED_PHASE_ORDER);
    }


    @Test
    @Transactional
    public void getAllProgramPhasesByStartDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where startDate equals to DEFAULT_START_DATE
        defaultProgramPhaseShouldBeFound("startDate.equals=" + DEFAULT_START_DATE);

        // Get all the programPhaseList where startDate equals to UPDATED_START_DATE
        defaultProgramPhaseShouldNotBeFound("startDate.equals=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByStartDateIsInShouldWork() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where startDate in DEFAULT_START_DATE or UPDATED_START_DATE
        defaultProgramPhaseShouldBeFound("startDate.in=" + DEFAULT_START_DATE + "," + UPDATED_START_DATE);

        // Get all the programPhaseList where startDate equals to UPDATED_START_DATE
        defaultProgramPhaseShouldNotBeFound("startDate.in=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByStartDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where startDate is not null
        defaultProgramPhaseShouldBeFound("startDate.specified=true");

        // Get all the programPhaseList where startDate is null
        defaultProgramPhaseShouldNotBeFound("startDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where endDate equals to DEFAULT_END_DATE
        defaultProgramPhaseShouldBeFound("endDate.equals=" + DEFAULT_END_DATE);

        // Get all the programPhaseList where endDate equals to UPDATED_END_DATE
        defaultProgramPhaseShouldNotBeFound("endDate.equals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where endDate in DEFAULT_END_DATE or UPDATED_END_DATE
        defaultProgramPhaseShouldBeFound("endDate.in=" + DEFAULT_END_DATE + "," + UPDATED_END_DATE);

        // Get all the programPhaseList where endDate equals to UPDATED_END_DATE
        defaultProgramPhaseShouldNotBeFound("endDate.in=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        // Get all the programPhaseList where endDate is not null
        defaultProgramPhaseShouldBeFound("endDate.specified=true");

        // Get all the programPhaseList where endDate is null
        defaultProgramPhaseShouldNotBeFound("endDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramPhasesByProgramIsEqualToSomething() throws Exception {
        // Get already existing entity
        Program program = programPhase.getProgram();
        programPhaseRepository.saveAndFlush(programPhase);
        Long programId = program.getId();

        // Get all the programPhaseList where program equals to programId
        defaultProgramPhaseShouldBeFound("programId.equals=" + programId);

        // Get all the programPhaseList where program equals to programId + 1
        defaultProgramPhaseShouldNotBeFound("programId.equals=" + (programId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramPhaseShouldBeFound(String filter) throws Exception {
        restProgramPhaseMockMvc.perform(get("/api/program-phases?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programPhase.getId().intValue())))
            .andExpect(jsonPath("$.[*].phaseOrder").value(hasItem(DEFAULT_PHASE_ORDER)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramPhaseMockMvc.perform(get("/api/program-phases/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramPhaseShouldNotBeFound(String filter) throws Exception {
        restProgramPhaseMockMvc.perform(get("/api/program-phases?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramPhaseMockMvc.perform(get("/api/program-phases/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramPhase() throws Exception {
        // Get the programPhase
        restProgramPhaseMockMvc.perform(get("/api/program-phases/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramPhase() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        int databaseSizeBeforeUpdate = programPhaseRepository.findAll().size();

        // Update the programPhase
        ProgramPhase updatedProgramPhase = programPhaseRepository.findById(programPhase.getId()).get();
        // Disconnect from session so that the updates on updatedProgramPhase are not directly saved in db
        em.detach(updatedProgramPhase);
        updatedProgramPhase
            .phaseOrder(UPDATED_PHASE_ORDER)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE);
        ProgramPhaseDTO programPhaseDTO = programPhaseMapper.toDto(updatedProgramPhase);

        restProgramPhaseMockMvc.perform(put("/api/program-phases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programPhaseDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramPhase in the database
        List<ProgramPhase> programPhaseList = programPhaseRepository.findAll();
        assertThat(programPhaseList).hasSize(databaseSizeBeforeUpdate);
        ProgramPhase testProgramPhase = programPhaseList.get(programPhaseList.size() - 1);
        assertThat(testProgramPhase.getPhaseOrder()).isEqualTo(UPDATED_PHASE_ORDER);
        assertThat(testProgramPhase.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testProgramPhase.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramPhase() throws Exception {
        int databaseSizeBeforeUpdate = programPhaseRepository.findAll().size();

        // Create the ProgramPhase
        ProgramPhaseDTO programPhaseDTO = programPhaseMapper.toDto(programPhase);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramPhaseMockMvc.perform(put("/api/program-phases")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programPhaseDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramPhase in the database
        List<ProgramPhase> programPhaseList = programPhaseRepository.findAll();
        assertThat(programPhaseList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramPhase() throws Exception {
        // Initialize the database
        programPhaseRepository.saveAndFlush(programPhase);

        int databaseSizeBeforeDelete = programPhaseRepository.findAll().size();

        // Delete the programPhase
        restProgramPhaseMockMvc.perform(delete("/api/program-phases/{id}", programPhase.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramPhase> programPhaseList = programPhaseRepository.findAll();
        assertThat(programPhaseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramPhase.class);
        ProgramPhase programPhase1 = new ProgramPhase();
        programPhase1.setId(1L);
        ProgramPhase programPhase2 = new ProgramPhase();
        programPhase2.setId(programPhase1.getId());
        assertThat(programPhase1).isEqualTo(programPhase2);
        programPhase2.setId(2L);
        assertThat(programPhase1).isNotEqualTo(programPhase2);
        programPhase1.setId(null);
        assertThat(programPhase1).isNotEqualTo(programPhase2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramPhaseDTO.class);
        ProgramPhaseDTO programPhaseDTO1 = new ProgramPhaseDTO();
        programPhaseDTO1.setId(1L);
        ProgramPhaseDTO programPhaseDTO2 = new ProgramPhaseDTO();
        assertThat(programPhaseDTO1).isNotEqualTo(programPhaseDTO2);
        programPhaseDTO2.setId(programPhaseDTO1.getId());
        assertThat(programPhaseDTO1).isEqualTo(programPhaseDTO2);
        programPhaseDTO2.setId(2L);
        assertThat(programPhaseDTO1).isNotEqualTo(programPhaseDTO2);
        programPhaseDTO1.setId(null);
        assertThat(programPhaseDTO1).isNotEqualTo(programPhaseDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programPhaseMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programPhaseMapper.fromId(null)).isNull();
    }
}
