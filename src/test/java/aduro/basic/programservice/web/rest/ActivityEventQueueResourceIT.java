package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ActivityEventQueue;
import aduro.basic.programservice.repository.ActivityEventQueueRepository;
import aduro.basic.programservice.service.ActivityEventQueueService;
import aduro.basic.programservice.service.dto.ActivityEventQueueDTO;
import aduro.basic.programservice.service.mapper.ActivityEventQueueMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ActivityEventQueueCriteria;
import aduro.basic.programservice.service.ActivityEventQueueQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ActivityEventQueueResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ActivityEventQueueResourceIT {

    private static final String DEFAULT_PARTICIPANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ACTIVITY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_ACTIVITY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TRANSACTION_ID = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EXECUTE_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_EXECUTE_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_ERROR_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_ID = "BBBBBBBBBB";

    @Autowired
    private ActivityEventQueueRepository activityEventQueueRepository;

    @Autowired
    private ActivityEventQueueMapper activityEventQueueMapper;

    @Autowired
    private ActivityEventQueueService activityEventQueueService;

    @Autowired
    private ActivityEventQueueQueryService activityEventQueueQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restActivityEventQueueMockMvc;

    private ActivityEventQueue activityEventQueue;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ActivityEventQueueResource activityEventQueueResource = new ActivityEventQueueResource(activityEventQueueService, activityEventQueueQueryService);
        this.restActivityEventQueueMockMvc = MockMvcBuilders.standaloneSetup(activityEventQueueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActivityEventQueue createEntity(EntityManager em) {
        ActivityEventQueue activityEventQueue = new ActivityEventQueue()
            .participantId(DEFAULT_PARTICIPANT_ID)
            .clientId(DEFAULT_CLIENT_ID)
            .activityCode(DEFAULT_ACTIVITY_CODE)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .createdDate(DEFAULT_CREATED_DATE)
            .transactionId(DEFAULT_TRANSACTION_ID)
            .executeStatus(DEFAULT_EXECUTE_STATUS)
            .errorMessage(DEFAULT_ERROR_MESSAGE)
            .subgroupId(DEFAULT_SUBGROUP_ID);
        return activityEventQueue;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ActivityEventQueue createUpdatedEntity(EntityManager em) {
        ActivityEventQueue activityEventQueue = new ActivityEventQueue()
            .participantId(UPDATED_PARTICIPANT_ID)
            .clientId(UPDATED_CLIENT_ID)
            .activityCode(UPDATED_ACTIVITY_CODE)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .createdDate(UPDATED_CREATED_DATE)
            .transactionId(UPDATED_TRANSACTION_ID)
            .executeStatus(UPDATED_EXECUTE_STATUS)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .subgroupId(UPDATED_SUBGROUP_ID);
        return activityEventQueue;
    }

    @BeforeEach
    public void initTest() {
        activityEventQueue = createEntity(em);
    }

    @Test
    @Transactional
    public void createActivityEventQueue() throws Exception {
        int databaseSizeBeforeCreate = activityEventQueueRepository.findAll().size();

        // Create the ActivityEventQueue
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);
        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isCreated());

        // Validate the ActivityEventQueue in the database
        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeCreate + 1);
        ActivityEventQueue testActivityEventQueue = activityEventQueueList.get(activityEventQueueList.size() - 1);
        assertThat(testActivityEventQueue.getParticipantId()).isEqualTo(DEFAULT_PARTICIPANT_ID);
        assertThat(testActivityEventQueue.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testActivityEventQueue.getActivityCode()).isEqualTo(DEFAULT_ACTIVITY_CODE);
        assertThat(testActivityEventQueue.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testActivityEventQueue.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testActivityEventQueue.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testActivityEventQueue.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testActivityEventQueue.getTransactionId()).isEqualTo(DEFAULT_TRANSACTION_ID);
        assertThat(testActivityEventQueue.getExecuteStatus()).isEqualTo(DEFAULT_EXECUTE_STATUS);
        assertThat(testActivityEventQueue.getErrorMessage()).isEqualTo(DEFAULT_ERROR_MESSAGE);
        assertThat(testActivityEventQueue.getSubgroupId()).isEqualTo(DEFAULT_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void createActivityEventQueueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = activityEventQueueRepository.findAll().size();

        // Create the ActivityEventQueue with an existing ID
        activityEventQueue.setId(1L);
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        // An entity with an existing ID cannot be created, so this API call must fail
        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ActivityEventQueue in the database
        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkParticipantIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityEventQueueRepository.findAll().size();
        // set the field null
        activityEventQueue.setParticipantId(null);

        // Create the ActivityEventQueue, which fails.
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityEventQueueRepository.findAll().size();
        // set the field null
        activityEventQueue.setClientId(null);

        // Create the ActivityEventQueue, which fails.
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActivityCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityEventQueueRepository.findAll().size();
        // set the field null
        activityEventQueue.setActivityCode(null);

        // Create the ActivityEventQueue, which fails.
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFirstNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityEventQueueRepository.findAll().size();
        // set the field null
        activityEventQueue.setFirstName(null);

        // Create the ActivityEventQueue, which fails.
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityEventQueueRepository.findAll().size();
        // set the field null
        activityEventQueue.setLastName(null);

        // Create the ActivityEventQueue, which fails.
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityEventQueueRepository.findAll().size();
        // set the field null
        activityEventQueue.setEmail(null);

        // Create the ActivityEventQueue, which fails.
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = activityEventQueueRepository.findAll().size();
        // set the field null
        activityEventQueue.setCreatedDate(null);

        // Create the ActivityEventQueue, which fails.
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        restActivityEventQueueMockMvc.perform(post("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueues() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList
        restActivityEventQueueMockMvc.perform(get("/api/activity-event-queues?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(activityEventQueue.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID.toString())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].activityCode").value(hasItem(DEFAULT_ACTIVITY_CODE.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID.toString())))
            .andExpect(jsonPath("$.[*].executeStatus").value(hasItem(DEFAULT_EXECUTE_STATUS.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID.toString())));
    }
    
    @Test
    @Transactional
    public void getActivityEventQueue() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get the activityEventQueue
        restActivityEventQueueMockMvc.perform(get("/api/activity-event-queues/{id}", activityEventQueue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(activityEventQueue.getId().intValue()))
            .andExpect(jsonPath("$.participantId").value(DEFAULT_PARTICIPANT_ID.toString()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.activityCode").value(DEFAULT_ACTIVITY_CODE.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.transactionId").value(DEFAULT_TRANSACTION_ID.toString()))
            .andExpect(jsonPath("$.executeStatus").value(DEFAULT_EXECUTE_STATUS.toString()))
            .andExpect(jsonPath("$.errorMessage").value(DEFAULT_ERROR_MESSAGE.toString()))
            .andExpect(jsonPath("$.subgroupId").value(DEFAULT_SUBGROUP_ID.toString()));
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByParticipantIdIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where participantId equals to DEFAULT_PARTICIPANT_ID
        defaultActivityEventQueueShouldBeFound("participantId.equals=" + DEFAULT_PARTICIPANT_ID);

        // Get all the activityEventQueueList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultActivityEventQueueShouldNotBeFound("participantId.equals=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByParticipantIdIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where participantId in DEFAULT_PARTICIPANT_ID or UPDATED_PARTICIPANT_ID
        defaultActivityEventQueueShouldBeFound("participantId.in=" + DEFAULT_PARTICIPANT_ID + "," + UPDATED_PARTICIPANT_ID);

        // Get all the activityEventQueueList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultActivityEventQueueShouldNotBeFound("participantId.in=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByParticipantIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where participantId is not null
        defaultActivityEventQueueShouldBeFound("participantId.specified=true");

        // Get all the activityEventQueueList where participantId is null
        defaultActivityEventQueueShouldNotBeFound("participantId.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where clientId equals to DEFAULT_CLIENT_ID
        defaultActivityEventQueueShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the activityEventQueueList where clientId equals to UPDATED_CLIENT_ID
        defaultActivityEventQueueShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultActivityEventQueueShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the activityEventQueueList where clientId equals to UPDATED_CLIENT_ID
        defaultActivityEventQueueShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where clientId is not null
        defaultActivityEventQueueShouldBeFound("clientId.specified=true");

        // Get all the activityEventQueueList where clientId is null
        defaultActivityEventQueueShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByActivityCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where activityCode equals to DEFAULT_ACTIVITY_CODE
        defaultActivityEventQueueShouldBeFound("activityCode.equals=" + DEFAULT_ACTIVITY_CODE);

        // Get all the activityEventQueueList where activityCode equals to UPDATED_ACTIVITY_CODE
        defaultActivityEventQueueShouldNotBeFound("activityCode.equals=" + UPDATED_ACTIVITY_CODE);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByActivityCodeIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where activityCode in DEFAULT_ACTIVITY_CODE or UPDATED_ACTIVITY_CODE
        defaultActivityEventQueueShouldBeFound("activityCode.in=" + DEFAULT_ACTIVITY_CODE + "," + UPDATED_ACTIVITY_CODE);

        // Get all the activityEventQueueList where activityCode equals to UPDATED_ACTIVITY_CODE
        defaultActivityEventQueueShouldNotBeFound("activityCode.in=" + UPDATED_ACTIVITY_CODE);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByActivityCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where activityCode is not null
        defaultActivityEventQueueShouldBeFound("activityCode.specified=true");

        // Get all the activityEventQueueList where activityCode is null
        defaultActivityEventQueueShouldNotBeFound("activityCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByFirstNameIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where firstName equals to DEFAULT_FIRST_NAME
        defaultActivityEventQueueShouldBeFound("firstName.equals=" + DEFAULT_FIRST_NAME);

        // Get all the activityEventQueueList where firstName equals to UPDATED_FIRST_NAME
        defaultActivityEventQueueShouldNotBeFound("firstName.equals=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByFirstNameIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where firstName in DEFAULT_FIRST_NAME or UPDATED_FIRST_NAME
        defaultActivityEventQueueShouldBeFound("firstName.in=" + DEFAULT_FIRST_NAME + "," + UPDATED_FIRST_NAME);

        // Get all the activityEventQueueList where firstName equals to UPDATED_FIRST_NAME
        defaultActivityEventQueueShouldNotBeFound("firstName.in=" + UPDATED_FIRST_NAME);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByFirstNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where firstName is not null
        defaultActivityEventQueueShouldBeFound("firstName.specified=true");

        // Get all the activityEventQueueList where firstName is null
        defaultActivityEventQueueShouldNotBeFound("firstName.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByLastNameIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where lastName equals to DEFAULT_LAST_NAME
        defaultActivityEventQueueShouldBeFound("lastName.equals=" + DEFAULT_LAST_NAME);

        // Get all the activityEventQueueList where lastName equals to UPDATED_LAST_NAME
        defaultActivityEventQueueShouldNotBeFound("lastName.equals=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByLastNameIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where lastName in DEFAULT_LAST_NAME or UPDATED_LAST_NAME
        defaultActivityEventQueueShouldBeFound("lastName.in=" + DEFAULT_LAST_NAME + "," + UPDATED_LAST_NAME);

        // Get all the activityEventQueueList where lastName equals to UPDATED_LAST_NAME
        defaultActivityEventQueueShouldNotBeFound("lastName.in=" + UPDATED_LAST_NAME);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByLastNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where lastName is not null
        defaultActivityEventQueueShouldBeFound("lastName.specified=true");

        // Get all the activityEventQueueList where lastName is null
        defaultActivityEventQueueShouldNotBeFound("lastName.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where email equals to DEFAULT_EMAIL
        defaultActivityEventQueueShouldBeFound("email.equals=" + DEFAULT_EMAIL);

        // Get all the activityEventQueueList where email equals to UPDATED_EMAIL
        defaultActivityEventQueueShouldNotBeFound("email.equals=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByEmailIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where email in DEFAULT_EMAIL or UPDATED_EMAIL
        defaultActivityEventQueueShouldBeFound("email.in=" + DEFAULT_EMAIL + "," + UPDATED_EMAIL);

        // Get all the activityEventQueueList where email equals to UPDATED_EMAIL
        defaultActivityEventQueueShouldNotBeFound("email.in=" + UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where email is not null
        defaultActivityEventQueueShouldBeFound("email.specified=true");

        // Get all the activityEventQueueList where email is null
        defaultActivityEventQueueShouldNotBeFound("email.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where createdDate equals to DEFAULT_CREATED_DATE
        defaultActivityEventQueueShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the activityEventQueueList where createdDate equals to UPDATED_CREATED_DATE
        defaultActivityEventQueueShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultActivityEventQueueShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the activityEventQueueList where createdDate equals to UPDATED_CREATED_DATE
        defaultActivityEventQueueShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where createdDate is not null
        defaultActivityEventQueueShouldBeFound("createdDate.specified=true");

        // Get all the activityEventQueueList where createdDate is null
        defaultActivityEventQueueShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByTransactionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where transactionId equals to DEFAULT_TRANSACTION_ID
        defaultActivityEventQueueShouldBeFound("transactionId.equals=" + DEFAULT_TRANSACTION_ID);

        // Get all the activityEventQueueList where transactionId equals to UPDATED_TRANSACTION_ID
        defaultActivityEventQueueShouldNotBeFound("transactionId.equals=" + UPDATED_TRANSACTION_ID);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByTransactionIdIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where transactionId in DEFAULT_TRANSACTION_ID or UPDATED_TRANSACTION_ID
        defaultActivityEventQueueShouldBeFound("transactionId.in=" + DEFAULT_TRANSACTION_ID + "," + UPDATED_TRANSACTION_ID);

        // Get all the activityEventQueueList where transactionId equals to UPDATED_TRANSACTION_ID
        defaultActivityEventQueueShouldNotBeFound("transactionId.in=" + UPDATED_TRANSACTION_ID);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByTransactionIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where transactionId is not null
        defaultActivityEventQueueShouldBeFound("transactionId.specified=true");

        // Get all the activityEventQueueList where transactionId is null
        defaultActivityEventQueueShouldNotBeFound("transactionId.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByExecuteStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where executeStatus equals to DEFAULT_EXECUTE_STATUS
        defaultActivityEventQueueShouldBeFound("executeStatus.equals=" + DEFAULT_EXECUTE_STATUS);

        // Get all the activityEventQueueList where executeStatus equals to UPDATED_EXECUTE_STATUS
        defaultActivityEventQueueShouldNotBeFound("executeStatus.equals=" + UPDATED_EXECUTE_STATUS);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByExecuteStatusIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where executeStatus in DEFAULT_EXECUTE_STATUS or UPDATED_EXECUTE_STATUS
        defaultActivityEventQueueShouldBeFound("executeStatus.in=" + DEFAULT_EXECUTE_STATUS + "," + UPDATED_EXECUTE_STATUS);

        // Get all the activityEventQueueList where executeStatus equals to UPDATED_EXECUTE_STATUS
        defaultActivityEventQueueShouldNotBeFound("executeStatus.in=" + UPDATED_EXECUTE_STATUS);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByExecuteStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where executeStatus is not null
        defaultActivityEventQueueShouldBeFound("executeStatus.specified=true");

        // Get all the activityEventQueueList where executeStatus is null
        defaultActivityEventQueueShouldNotBeFound("executeStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByErrorMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where errorMessage equals to DEFAULT_ERROR_MESSAGE
        defaultActivityEventQueueShouldBeFound("errorMessage.equals=" + DEFAULT_ERROR_MESSAGE);

        // Get all the activityEventQueueList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultActivityEventQueueShouldNotBeFound("errorMessage.equals=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByErrorMessageIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where errorMessage in DEFAULT_ERROR_MESSAGE or UPDATED_ERROR_MESSAGE
        defaultActivityEventQueueShouldBeFound("errorMessage.in=" + DEFAULT_ERROR_MESSAGE + "," + UPDATED_ERROR_MESSAGE);

        // Get all the activityEventQueueList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultActivityEventQueueShouldNotBeFound("errorMessage.in=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesByErrorMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where errorMessage is not null
        defaultActivityEventQueueShouldBeFound("errorMessage.specified=true");

        // Get all the activityEventQueueList where errorMessage is null
        defaultActivityEventQueueShouldNotBeFound("errorMessage.specified=false");
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesBySubgroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where subgroupId equals to DEFAULT_SUBGROUP_ID
        defaultActivityEventQueueShouldBeFound("subgroupId.equals=" + DEFAULT_SUBGROUP_ID);

        // Get all the activityEventQueueList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultActivityEventQueueShouldNotBeFound("subgroupId.equals=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesBySubgroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where subgroupId in DEFAULT_SUBGROUP_ID or UPDATED_SUBGROUP_ID
        defaultActivityEventQueueShouldBeFound("subgroupId.in=" + DEFAULT_SUBGROUP_ID + "," + UPDATED_SUBGROUP_ID);

        // Get all the activityEventQueueList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultActivityEventQueueShouldNotBeFound("subgroupId.in=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllActivityEventQueuesBySubgroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        // Get all the activityEventQueueList where subgroupId is not null
        defaultActivityEventQueueShouldBeFound("subgroupId.specified=true");

        // Get all the activityEventQueueList where subgroupId is null
        defaultActivityEventQueueShouldNotBeFound("subgroupId.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultActivityEventQueueShouldBeFound(String filter) throws Exception {
        restActivityEventQueueMockMvc.perform(get("/api/activity-event-queues?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(activityEventQueue.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID)))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].activityCode").value(hasItem(DEFAULT_ACTIVITY_CODE)))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].transactionId").value(hasItem(DEFAULT_TRANSACTION_ID)))
            .andExpect(jsonPath("$.[*].executeStatus").value(hasItem(DEFAULT_EXECUTE_STATUS)))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE)))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID)));

        // Check, that the count call also returns 1
        restActivityEventQueueMockMvc.perform(get("/api/activity-event-queues/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultActivityEventQueueShouldNotBeFound(String filter) throws Exception {
        restActivityEventQueueMockMvc.perform(get("/api/activity-event-queues?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restActivityEventQueueMockMvc.perform(get("/api/activity-event-queues/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingActivityEventQueue() throws Exception {
        // Get the activityEventQueue
        restActivityEventQueueMockMvc.perform(get("/api/activity-event-queues/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateActivityEventQueue() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        int databaseSizeBeforeUpdate = activityEventQueueRepository.findAll().size();

        // Update the activityEventQueue
        ActivityEventQueue updatedActivityEventQueue = activityEventQueueRepository.findById(activityEventQueue.getId()).get();
        // Disconnect from session so that the updates on updatedActivityEventQueue are not directly saved in db
        em.detach(updatedActivityEventQueue);
        updatedActivityEventQueue
            .participantId(UPDATED_PARTICIPANT_ID)
            .clientId(UPDATED_CLIENT_ID)
            .activityCode(UPDATED_ACTIVITY_CODE)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .createdDate(UPDATED_CREATED_DATE)
            .transactionId(UPDATED_TRANSACTION_ID)
            .executeStatus(UPDATED_EXECUTE_STATUS)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .subgroupId(UPDATED_SUBGROUP_ID);
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(updatedActivityEventQueue);

        restActivityEventQueueMockMvc.perform(put("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isOk());

        // Validate the ActivityEventQueue in the database
        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeUpdate);
        ActivityEventQueue testActivityEventQueue = activityEventQueueList.get(activityEventQueueList.size() - 1);
        assertThat(testActivityEventQueue.getParticipantId()).isEqualTo(UPDATED_PARTICIPANT_ID);
        assertThat(testActivityEventQueue.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testActivityEventQueue.getActivityCode()).isEqualTo(UPDATED_ACTIVITY_CODE);
        assertThat(testActivityEventQueue.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testActivityEventQueue.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testActivityEventQueue.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testActivityEventQueue.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testActivityEventQueue.getTransactionId()).isEqualTo(UPDATED_TRANSACTION_ID);
        assertThat(testActivityEventQueue.getExecuteStatus()).isEqualTo(UPDATED_EXECUTE_STATUS);
        assertThat(testActivityEventQueue.getErrorMessage()).isEqualTo(UPDATED_ERROR_MESSAGE);
        assertThat(testActivityEventQueue.getSubgroupId()).isEqualTo(UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingActivityEventQueue() throws Exception {
        int databaseSizeBeforeUpdate = activityEventQueueRepository.findAll().size();

        // Create the ActivityEventQueue
        ActivityEventQueueDTO activityEventQueueDTO = activityEventQueueMapper.toDto(activityEventQueue);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restActivityEventQueueMockMvc.perform(put("/api/activity-event-queues")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(activityEventQueueDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ActivityEventQueue in the database
        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteActivityEventQueue() throws Exception {
        // Initialize the database
        activityEventQueueRepository.saveAndFlush(activityEventQueue);

        int databaseSizeBeforeDelete = activityEventQueueRepository.findAll().size();

        // Delete the activityEventQueue
        restActivityEventQueueMockMvc.perform(delete("/api/activity-event-queues/{id}", activityEventQueue.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ActivityEventQueue> activityEventQueueList = activityEventQueueRepository.findAll();
        assertThat(activityEventQueueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActivityEventQueue.class);
        ActivityEventQueue activityEventQueue1 = new ActivityEventQueue();
        activityEventQueue1.setId(1L);
        ActivityEventQueue activityEventQueue2 = new ActivityEventQueue();
        activityEventQueue2.setId(activityEventQueue1.getId());
        assertThat(activityEventQueue1).isEqualTo(activityEventQueue2);
        activityEventQueue2.setId(2L);
        assertThat(activityEventQueue1).isNotEqualTo(activityEventQueue2);
        activityEventQueue1.setId(null);
        assertThat(activityEventQueue1).isNotEqualTo(activityEventQueue2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ActivityEventQueueDTO.class);
        ActivityEventQueueDTO activityEventQueueDTO1 = new ActivityEventQueueDTO();
        activityEventQueueDTO1.setId(1L);
        ActivityEventQueueDTO activityEventQueueDTO2 = new ActivityEventQueueDTO();
        assertThat(activityEventQueueDTO1).isNotEqualTo(activityEventQueueDTO2);
        activityEventQueueDTO2.setId(activityEventQueueDTO1.getId());
        assertThat(activityEventQueueDTO1).isEqualTo(activityEventQueueDTO2);
        activityEventQueueDTO2.setId(2L);
        assertThat(activityEventQueueDTO1).isNotEqualTo(activityEventQueueDTO2);
        activityEventQueueDTO1.setId(null);
        assertThat(activityEventQueueDTO1).isNotEqualTo(activityEventQueueDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(activityEventQueueMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(activityEventQueueMapper.fromId(null)).isNull();
    }
}
