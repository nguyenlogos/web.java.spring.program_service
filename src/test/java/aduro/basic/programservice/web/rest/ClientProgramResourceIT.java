package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ClientProgram;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.repository.ClientProgramRepository;
import aduro.basic.programservice.service.ClientProgramService;
import aduro.basic.programservice.service.ProgramService;
import aduro.basic.programservice.service.dto.ClientProgramDTO;
import aduro.basic.programservice.service.mapper.ClientProgramMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ClientProgramCriteria;
import aduro.basic.programservice.service.ClientProgramQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ClientProgramResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ClientProgramResourceIT {

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_NAME = "BBBBBBBBBB";

    @Autowired
    private ClientProgramRepository clientProgramRepository;

    @Autowired
    private ClientProgramMapper clientProgramMapper;

    @Autowired
    private ClientProgramService clientProgramService;

    @Autowired
    private ProgramService programService;

    @Autowired
    private ClientProgramQueryService clientProgramQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClientProgramMockMvc;

    private ClientProgram clientProgram;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientProgramResource clientProgramResource = new ClientProgramResource(clientProgramService, clientProgramQueryService, programService);
        this.restClientProgramMockMvc = MockMvcBuilders.standaloneSetup(clientProgramResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientProgram createEntity(EntityManager em) {

        ClientProgram clientProgram = new ClientProgram()
            .clientId(DEFAULT_CLIENT_ID)
            .clientName(DEFAULT_CLIENT_NAME);

        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        clientProgram.setProgram(program);
        return clientProgram;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientProgram createUpdatedEntity(EntityManager em) {
        ClientProgram clientProgram = new ClientProgram()
            .clientId(UPDATED_CLIENT_ID)
            .clientName(UPDATED_CLIENT_NAME);
        // Add required entity
        Program program;
        if (TestUtil.findAll(em, Program.class).isEmpty()) {
            program = ProgramResourceIT.createUpdatedEntity(em);
            em.persist(program);
            em.flush();
        } else {
            program = TestUtil.findAll(em, Program.class).get(0);
        }
        clientProgram.setProgram(program);
        return clientProgram;
    }

    @BeforeEach
    public void initTest() {
        clientProgram = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientProgram() throws Exception {
        int databaseSizeBeforeCreate = clientProgramRepository.findAll().size();

        // Create the ClientProgram
        ClientProgramDTO clientProgramDTO = clientProgramMapper.toDto(clientProgram);
        clientProgramDTO.setProgramId(clientProgram.getProgram().getId());

        restClientProgramMockMvc.perform(post("/api/client-programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramDTO)));
           // .andExpect(status().isCreated());


    }

    @Test
    @Transactional
    public void getProgramDetail() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);
        // Get the clientProgram
        restClientProgramMockMvc.perform(get("/api/client-programs/AAAAAAAAAA/current"))
            .andExpect(status().isOk());
           /* .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientProgram.getId().intValue()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.clientName").value(DEFAULT_CLIENT_NAME.toString()));*/
    }



    @Test
    @Transactional
    public void assignClientProgram() throws Exception {
        int databaseSizeBeforeCreate = clientProgramRepository.findAll().size();
        // Create the ClientProgram
        ClientProgramDTO clientProgramDTO = clientProgramMapper.toDto(clientProgram);
        clientProgramDTO.setProgramId(clientProgram.getProgram().getId());
        List<ClientProgramDTO> clientProgramDTOList = new ArrayList<>();
        clientProgramDTOList.add(clientProgramDTO);
        restClientProgramMockMvc.perform(post("api/client-programs/1/assign-clients-program")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramDTOList)));
        // .andExpect(status().isCreated());

    }

    @Test
    @Transactional
    public void createClientProgramWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientProgramRepository.findAll().size();

        // Create the ClientProgram with an existing ID
        clientProgram.setId(1L);
        ClientProgramDTO clientProgramDTO = clientProgramMapper.toDto(clientProgram);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientProgramMockMvc.perform(post("/api/client-programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientProgram in the database
        List<ClientProgram> clientProgramList = clientProgramRepository.findAll();
        assertThat(clientProgramList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientProgramRepository.findAll().size();
        // set the field null
        clientProgram.setClientId(null);

        // Create the ClientProgram, which fails.
        ClientProgramDTO clientProgramDTO = clientProgramMapper.toDto(clientProgram);

        restClientProgramMockMvc.perform(post("/api/client-programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramDTO)))
            .andExpect(status().isBadRequest());

        List<ClientProgram> clientProgramList = clientProgramRepository.findAll();
        assertThat(clientProgramList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientPrograms() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        // Get all the clientProgramList
        restClientProgramMockMvc.perform(get("/api/client-programs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientProgram.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getClientProgram() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        // Get the clientProgram
        restClientProgramMockMvc.perform(get("/api/client-programs/{id}", clientProgram.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientProgram.getId().intValue()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.clientName").value(DEFAULT_CLIENT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllClientProgramsByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        // Get all the clientProgramList where clientId equals to DEFAULT_CLIENT_ID
        defaultClientProgramShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the clientProgramList where clientId equals to UPDATED_CLIENT_ID
        defaultClientProgramShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientProgramsByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        // Get all the clientProgramList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultClientProgramShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the clientProgramList where clientId equals to UPDATED_CLIENT_ID
        defaultClientProgramShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientProgramsByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        // Get all the clientProgramList where clientId is not null
        defaultClientProgramShouldBeFound("clientId.specified=true");

        // Get all the clientProgramList where clientId is null
        defaultClientProgramShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramsByClientNameIsEqualToSomething() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        // Get all the clientProgramList where clientName equals to DEFAULT_CLIENT_NAME
        defaultClientProgramShouldBeFound("clientName.equals=" + DEFAULT_CLIENT_NAME);

        // Get all the clientProgramList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientProgramShouldNotBeFound("clientName.equals=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientProgramsByClientNameIsInShouldWork() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        // Get all the clientProgramList where clientName in DEFAULT_CLIENT_NAME or UPDATED_CLIENT_NAME
        defaultClientProgramShouldBeFound("clientName.in=" + DEFAULT_CLIENT_NAME + "," + UPDATED_CLIENT_NAME);

        // Get all the clientProgramList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientProgramShouldNotBeFound("clientName.in=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientProgramsByClientNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        // Get all the clientProgramList where clientName is not null
        defaultClientProgramShouldBeFound("clientName.specified=true");

        // Get all the clientProgramList where clientName is null
        defaultClientProgramShouldNotBeFound("clientName.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientProgramsByProgramIsEqualToSomething() throws Exception {
        // Get already existing entity
        Program program = clientProgram.getProgram();
        clientProgramRepository.saveAndFlush(clientProgram);
        Long programId = program.getId();

        // Get all the clientProgramList where program equals to programId
        defaultClientProgramShouldBeFound("programId.equals=" + programId);

        // Get all the clientProgramList where program equals to programId + 1
        defaultClientProgramShouldNotBeFound("programId.equals=" + (programId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClientProgramShouldBeFound(String filter) throws Exception {
        restClientProgramMockMvc.perform(get("/api/client-programs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientProgram.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME)));

        // Check, that the count call also returns 1
        restClientProgramMockMvc.perform(get("/api/client-programs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClientProgramShouldNotBeFound(String filter) throws Exception {
        restClientProgramMockMvc.perform(get("/api/client-programs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray());
            //.andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClientProgramMockMvc.perform(get("/api/client-programs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
          //  .andExpect(content().string("1"));
    }


    @Test
    @Transactional
    public void getNonExistingClientProgram() throws Exception {
        // Get the clientProgram
        restClientProgramMockMvc.perform(get("/api/client-programs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientProgram() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        int databaseSizeBeforeUpdate = clientProgramRepository.findAll().size();

        // Update the clientProgram
        ClientProgram updatedClientProgram = clientProgramRepository.findById(clientProgram.getId()).get();
        // Disconnect from session so that the updates on updatedClientProgram are not directly saved in db
        em.detach(updatedClientProgram);
        updatedClientProgram
            .clientId(UPDATED_CLIENT_ID)
            .clientName(UPDATED_CLIENT_NAME);
        ClientProgramDTO clientProgramDTO = clientProgramMapper.toDto(updatedClientProgram);
        clientProgramDTO.setProgramId(updatedClientProgram.getProgram().getId());

        restClientProgramMockMvc.perform(put("/api/client-programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramDTO)))
            .andExpect(status().isOk());

        // Validate the ClientProgram in the database
        List<ClientProgram> clientProgramList = clientProgramRepository.findAll();
        assertThat(clientProgramList).hasSize(databaseSizeBeforeUpdate);
        ClientProgram testClientProgram = clientProgramList.get(clientProgramList.size() - 1);
        assertThat(testClientProgram.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testClientProgram.getClientName()).isEqualTo(UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingClientProgram() throws Exception {
        int databaseSizeBeforeUpdate = clientProgramRepository.findAll().size();

        // Create the ClientProgram
        ClientProgramDTO clientProgramDTO = clientProgramMapper.toDto(clientProgram);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientProgramMockMvc.perform(put("/api/client-programs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientProgramDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientProgram in the database
        List<ClientProgram> clientProgramList = clientProgramRepository.findAll();
        assertThat(clientProgramList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClientProgram() throws Exception {
        // Initialize the database
        clientProgramRepository.saveAndFlush(clientProgram);

        int databaseSizeBeforeDelete = clientProgramRepository.findAll().size();

        // Delete the clientProgram
        restClientProgramMockMvc.perform(delete("/api/client-programs/{id}", clientProgram.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ClientProgram> clientProgramList = clientProgramRepository.findAll();
        assertThat(clientProgramList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientProgram.class);
        ClientProgram clientProgram1 = new ClientProgram();
        clientProgram1.setId(1L);
        ClientProgram clientProgram2 = new ClientProgram();
        clientProgram2.setId(clientProgram1.getId());
        assertThat(clientProgram1).isEqualTo(clientProgram2);
        clientProgram2.setId(2L);
        assertThat(clientProgram1).isNotEqualTo(clientProgram2);
        clientProgram1.setId(null);
        assertThat(clientProgram1).isNotEqualTo(clientProgram2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientProgramDTO.class);
        ClientProgramDTO clientProgramDTO1 = new ClientProgramDTO();
        clientProgramDTO1.setId(1L);
        ClientProgramDTO clientProgramDTO2 = new ClientProgramDTO();
        assertThat(clientProgramDTO1).isNotEqualTo(clientProgramDTO2);
        clientProgramDTO2.setId(clientProgramDTO1.getId());
        assertThat(clientProgramDTO1).isEqualTo(clientProgramDTO2);
        clientProgramDTO2.setId(2L);
        assertThat(clientProgramDTO1).isNotEqualTo(clientProgramDTO2);
        clientProgramDTO1.setId(null);
        assertThat(clientProgramDTO1).isNotEqualTo(clientProgramDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientProgramMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientProgramMapper.fromId(null)).isNull();
    }
}
