package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramCollectionContent;
import aduro.basic.programservice.domain.ProgramCollection;
import aduro.basic.programservice.repository.ProgramCollectionContentRepository;
import aduro.basic.programservice.service.ProgramCollectionContentService;
import aduro.basic.programservice.service.dto.ProgramCollectionContentDTO;
import aduro.basic.programservice.service.mapper.ProgramCollectionContentMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramCollectionContentCriteria;
import aduro.basic.programservice.service.ProgramCollectionContentQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramCollectionContentResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramCollectionContentResourceIT {

    private static final String DEFAULT_ITEM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_ITEM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ITEM_ID = "AAAAAAAAAA";
    private static final String UPDATED_ITEM_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ITEM_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_ITEM_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_ITEM_ICON = "AAAAAAAAAA";
    private static final String UPDATED_ITEM_ICON = "BBBBBBBBBB";

    private static final Boolean DEFAULT_HAS_REQUIRED = false;
    private static final Boolean UPDATED_HAS_REQUIRED = true;

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramCollectionContentRepository programCollectionContentRepository;

    @Autowired
    private ProgramCollectionContentMapper programCollectionContentMapper;

    @Autowired
    private ProgramCollectionContentService programCollectionContentService;

    @Autowired
    private ProgramCollectionContentQueryService programCollectionContentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramCollectionContentMockMvc;

    private ProgramCollectionContent programCollectionContent;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramCollectionContentResource programCollectionContentResource = new ProgramCollectionContentResource(programCollectionContentService, programCollectionContentQueryService);
        this.restProgramCollectionContentMockMvc = MockMvcBuilders.standaloneSetup(programCollectionContentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCollectionContent createEntity(EntityManager em) {
        ProgramCollectionContent programCollectionContent = new ProgramCollectionContent()
            .itemName(DEFAULT_ITEM_NAME)
            .itemId(DEFAULT_ITEM_ID)
            .itemType(DEFAULT_ITEM_TYPE)
            .contentType(DEFAULT_CONTENT_TYPE)
            .itemIcon(DEFAULT_ITEM_ICON)
            .hasRequired(DEFAULT_HAS_REQUIRED)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        return programCollectionContent;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCollectionContent createUpdatedEntity(EntityManager em) {
        ProgramCollectionContent programCollectionContent = new ProgramCollectionContent()
            .itemName(UPDATED_ITEM_NAME)
            .itemId(UPDATED_ITEM_ID)
            .itemType(UPDATED_ITEM_TYPE)
            .contentType(UPDATED_CONTENT_TYPE)
            .itemIcon(UPDATED_ITEM_ICON)
            .hasRequired(UPDATED_HAS_REQUIRED)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        return programCollectionContent;
    }

    @BeforeEach
    public void initTest() {
        programCollectionContent = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramCollectionContent() throws Exception {
        int databaseSizeBeforeCreate = programCollectionContentRepository.findAll().size();

        // Create the ProgramCollectionContent
        ProgramCollectionContentDTO programCollectionContentDTO = programCollectionContentMapper.toDto(programCollectionContent);
        restProgramCollectionContentMockMvc.perform(post("/api/program-collection-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionContentDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramCollectionContent in the database
        List<ProgramCollectionContent> programCollectionContentList = programCollectionContentRepository.findAll();
        assertThat(programCollectionContentList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramCollectionContent testProgramCollectionContent = programCollectionContentList.get(programCollectionContentList.size() - 1);
        assertThat(testProgramCollectionContent.getItemName()).isEqualTo(DEFAULT_ITEM_NAME);
        assertThat(testProgramCollectionContent.getItemId()).isEqualTo(DEFAULT_ITEM_ID);
        assertThat(testProgramCollectionContent.getItemType()).isEqualTo(DEFAULT_ITEM_TYPE);
        assertThat(testProgramCollectionContent.getContentType()).isEqualTo(DEFAULT_CONTENT_TYPE);
        assertThat(testProgramCollectionContent.getItemIcon()).isEqualTo(DEFAULT_ITEM_ICON);
        assertThat(testProgramCollectionContent.isHasRequired()).isEqualTo(DEFAULT_HAS_REQUIRED);
        assertThat(testProgramCollectionContent.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramCollectionContent.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramCollectionContentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programCollectionContentRepository.findAll().size();

        // Create the ProgramCollectionContent with an existing ID
        programCollectionContent.setId(1L);
        ProgramCollectionContentDTO programCollectionContentDTO = programCollectionContentMapper.toDto(programCollectionContent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramCollectionContentMockMvc.perform(post("/api/program-collection-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionContentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCollectionContent in the database
        List<ProgramCollectionContent> programCollectionContentList = programCollectionContentRepository.findAll();
        assertThat(programCollectionContentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkItemTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCollectionContentRepository.findAll().size();
        // set the field null
        programCollectionContent.setItemType(null);

        // Create the ProgramCollectionContent, which fails.
        ProgramCollectionContentDTO programCollectionContentDTO = programCollectionContentMapper.toDto(programCollectionContent);

        restProgramCollectionContentMockMvc.perform(post("/api/program-collection-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionContentDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCollectionContent> programCollectionContentList = programCollectionContentRepository.findAll();
        assertThat(programCollectionContentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContentTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCollectionContentRepository.findAll().size();
        // set the field null
        programCollectionContent.setContentType(null);

        // Create the ProgramCollectionContent, which fails.
        ProgramCollectionContentDTO programCollectionContentDTO = programCollectionContentMapper.toDto(programCollectionContent);

        restProgramCollectionContentMockMvc.perform(post("/api/program-collection-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionContentDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCollectionContent> programCollectionContentList = programCollectionContentRepository.findAll();
        assertThat(programCollectionContentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContents() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList
        restProgramCollectionContentMockMvc.perform(get("/api/program-collection-contents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCollectionContent.getId().intValue())))
            .andExpect(jsonPath("$.[*].itemName").value(hasItem(DEFAULT_ITEM_NAME.toString())))
            .andExpect(jsonPath("$.[*].itemId").value(hasItem(DEFAULT_ITEM_ID.toString())))
            .andExpect(jsonPath("$.[*].itemType").value(hasItem(DEFAULT_ITEM_TYPE.toString())))
            .andExpect(jsonPath("$.[*].contentType").value(hasItem(DEFAULT_CONTENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].itemIcon").value(hasItem(DEFAULT_ITEM_ICON.toString())))
            .andExpect(jsonPath("$.[*].hasRequired").value(hasItem(DEFAULT_HAS_REQUIRED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramCollectionContent() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get the programCollectionContent
        restProgramCollectionContentMockMvc.perform(get("/api/program-collection-contents/{id}", programCollectionContent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programCollectionContent.getId().intValue()))
            .andExpect(jsonPath("$.itemName").value(DEFAULT_ITEM_NAME.toString()))
            .andExpect(jsonPath("$.itemId").value(DEFAULT_ITEM_ID.toString()))
            .andExpect(jsonPath("$.itemType").value(DEFAULT_ITEM_TYPE.toString()))
            .andExpect(jsonPath("$.contentType").value(DEFAULT_CONTENT_TYPE.toString()))
            .andExpect(jsonPath("$.itemIcon").value(DEFAULT_ITEM_ICON.toString()))
            .andExpect(jsonPath("$.hasRequired").value(DEFAULT_HAS_REQUIRED.booleanValue()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemName equals to DEFAULT_ITEM_NAME
        defaultProgramCollectionContentShouldBeFound("itemName.equals=" + DEFAULT_ITEM_NAME);

        // Get all the programCollectionContentList where itemName equals to UPDATED_ITEM_NAME
        defaultProgramCollectionContentShouldNotBeFound("itemName.equals=" + UPDATED_ITEM_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemNameIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemName in DEFAULT_ITEM_NAME or UPDATED_ITEM_NAME
        defaultProgramCollectionContentShouldBeFound("itemName.in=" + DEFAULT_ITEM_NAME + "," + UPDATED_ITEM_NAME);

        // Get all the programCollectionContentList where itemName equals to UPDATED_ITEM_NAME
        defaultProgramCollectionContentShouldNotBeFound("itemName.in=" + UPDATED_ITEM_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemName is not null
        defaultProgramCollectionContentShouldBeFound("itemName.specified=true");

        // Get all the programCollectionContentList where itemName is null
        defaultProgramCollectionContentShouldNotBeFound("itemName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemId equals to DEFAULT_ITEM_ID
        defaultProgramCollectionContentShouldBeFound("itemId.equals=" + DEFAULT_ITEM_ID);

        // Get all the programCollectionContentList where itemId equals to UPDATED_ITEM_ID
        defaultProgramCollectionContentShouldNotBeFound("itemId.equals=" + UPDATED_ITEM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemIdIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemId in DEFAULT_ITEM_ID or UPDATED_ITEM_ID
        defaultProgramCollectionContentShouldBeFound("itemId.in=" + DEFAULT_ITEM_ID + "," + UPDATED_ITEM_ID);

        // Get all the programCollectionContentList where itemId equals to UPDATED_ITEM_ID
        defaultProgramCollectionContentShouldNotBeFound("itemId.in=" + UPDATED_ITEM_ID);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemId is not null
        defaultProgramCollectionContentShouldBeFound("itemId.specified=true");

        // Get all the programCollectionContentList where itemId is null
        defaultProgramCollectionContentShouldNotBeFound("itemId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemType equals to DEFAULT_ITEM_TYPE
        defaultProgramCollectionContentShouldBeFound("itemType.equals=" + DEFAULT_ITEM_TYPE);

        // Get all the programCollectionContentList where itemType equals to UPDATED_ITEM_TYPE
        defaultProgramCollectionContentShouldNotBeFound("itemType.equals=" + UPDATED_ITEM_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemTypeIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemType in DEFAULT_ITEM_TYPE or UPDATED_ITEM_TYPE
        defaultProgramCollectionContentShouldBeFound("itemType.in=" + DEFAULT_ITEM_TYPE + "," + UPDATED_ITEM_TYPE);

        // Get all the programCollectionContentList where itemType equals to UPDATED_ITEM_TYPE
        defaultProgramCollectionContentShouldNotBeFound("itemType.in=" + UPDATED_ITEM_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemType is not null
        defaultProgramCollectionContentShouldBeFound("itemType.specified=true");

        // Get all the programCollectionContentList where itemType is null
        defaultProgramCollectionContentShouldNotBeFound("itemType.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByContentTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where contentType equals to DEFAULT_CONTENT_TYPE
        defaultProgramCollectionContentShouldBeFound("contentType.equals=" + DEFAULT_CONTENT_TYPE);

        // Get all the programCollectionContentList where contentType equals to UPDATED_CONTENT_TYPE
        defaultProgramCollectionContentShouldNotBeFound("contentType.equals=" + UPDATED_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByContentTypeIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where contentType in DEFAULT_CONTENT_TYPE or UPDATED_CONTENT_TYPE
        defaultProgramCollectionContentShouldBeFound("contentType.in=" + DEFAULT_CONTENT_TYPE + "," + UPDATED_CONTENT_TYPE);

        // Get all the programCollectionContentList where contentType equals to UPDATED_CONTENT_TYPE
        defaultProgramCollectionContentShouldNotBeFound("contentType.in=" + UPDATED_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByContentTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where contentType is not null
        defaultProgramCollectionContentShouldBeFound("contentType.specified=true");

        // Get all the programCollectionContentList where contentType is null
        defaultProgramCollectionContentShouldNotBeFound("contentType.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemIconIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemIcon equals to DEFAULT_ITEM_ICON
        defaultProgramCollectionContentShouldBeFound("itemIcon.equals=" + DEFAULT_ITEM_ICON);

        // Get all the programCollectionContentList where itemIcon equals to UPDATED_ITEM_ICON
        defaultProgramCollectionContentShouldNotBeFound("itemIcon.equals=" + UPDATED_ITEM_ICON);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemIconIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemIcon in DEFAULT_ITEM_ICON or UPDATED_ITEM_ICON
        defaultProgramCollectionContentShouldBeFound("itemIcon.in=" + DEFAULT_ITEM_ICON + "," + UPDATED_ITEM_ICON);

        // Get all the programCollectionContentList where itemIcon equals to UPDATED_ITEM_ICON
        defaultProgramCollectionContentShouldNotBeFound("itemIcon.in=" + UPDATED_ITEM_ICON);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByItemIconIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where itemIcon is not null
        defaultProgramCollectionContentShouldBeFound("itemIcon.specified=true");

        // Get all the programCollectionContentList where itemIcon is null
        defaultProgramCollectionContentShouldNotBeFound("itemIcon.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByHasRequiredIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where hasRequired equals to DEFAULT_HAS_REQUIRED
        defaultProgramCollectionContentShouldBeFound("hasRequired.equals=" + DEFAULT_HAS_REQUIRED);

        // Get all the programCollectionContentList where hasRequired equals to UPDATED_HAS_REQUIRED
        defaultProgramCollectionContentShouldNotBeFound("hasRequired.equals=" + UPDATED_HAS_REQUIRED);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByHasRequiredIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where hasRequired in DEFAULT_HAS_REQUIRED or UPDATED_HAS_REQUIRED
        defaultProgramCollectionContentShouldBeFound("hasRequired.in=" + DEFAULT_HAS_REQUIRED + "," + UPDATED_HAS_REQUIRED);

        // Get all the programCollectionContentList where hasRequired equals to UPDATED_HAS_REQUIRED
        defaultProgramCollectionContentShouldNotBeFound("hasRequired.in=" + UPDATED_HAS_REQUIRED);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByHasRequiredIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where hasRequired is not null
        defaultProgramCollectionContentShouldBeFound("hasRequired.specified=true");

        // Get all the programCollectionContentList where hasRequired is null
        defaultProgramCollectionContentShouldNotBeFound("hasRequired.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramCollectionContentShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programCollectionContentList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCollectionContentShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramCollectionContentShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programCollectionContentList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCollectionContentShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where createdDate is not null
        defaultProgramCollectionContentShouldBeFound("createdDate.specified=true");

        // Get all the programCollectionContentList where createdDate is null
        defaultProgramCollectionContentShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramCollectionContentShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programCollectionContentList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCollectionContentShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramCollectionContentShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programCollectionContentList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCollectionContentShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        // Get all the programCollectionContentList where modifiedDate is not null
        defaultProgramCollectionContentShouldBeFound("modifiedDate.specified=true");

        // Get all the programCollectionContentList where modifiedDate is null
        defaultProgramCollectionContentShouldNotBeFound("modifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionContentsByProgramCollectionIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCollection programCollection = ProgramCollectionResourceIT.createEntity(em);
        em.persist(programCollection);
        em.flush();
        programCollectionContent.setProgramCollection(programCollection);
        programCollectionContentRepository.saveAndFlush(programCollectionContent);
        Long programCollectionId = programCollection.getId();

        // Get all the programCollectionContentList where programCollection equals to programCollectionId
        defaultProgramCollectionContentShouldBeFound("programCollectionId.equals=" + programCollectionId);

        // Get all the programCollectionContentList where programCollection equals to programCollectionId + 1
        defaultProgramCollectionContentShouldNotBeFound("programCollectionId.equals=" + (programCollectionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramCollectionContentShouldBeFound(String filter) throws Exception {
        restProgramCollectionContentMockMvc.perform(get("/api/program-collection-contents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCollectionContent.getId().intValue())))
            .andExpect(jsonPath("$.[*].itemName").value(hasItem(DEFAULT_ITEM_NAME)))
            .andExpect(jsonPath("$.[*].itemId").value(hasItem(DEFAULT_ITEM_ID)))
            .andExpect(jsonPath("$.[*].itemType").value(hasItem(DEFAULT_ITEM_TYPE)))
            .andExpect(jsonPath("$.[*].contentType").value(hasItem(DEFAULT_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].itemIcon").value(hasItem(DEFAULT_ITEM_ICON)))
            .andExpect(jsonPath("$.[*].hasRequired").value(hasItem(DEFAULT_HAS_REQUIRED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramCollectionContentMockMvc.perform(get("/api/program-collection-contents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramCollectionContentShouldNotBeFound(String filter) throws Exception {
        restProgramCollectionContentMockMvc.perform(get("/api/program-collection-contents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramCollectionContentMockMvc.perform(get("/api/program-collection-contents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramCollectionContent() throws Exception {
        // Get the programCollectionContent
        restProgramCollectionContentMockMvc.perform(get("/api/program-collection-contents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramCollectionContent() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        int databaseSizeBeforeUpdate = programCollectionContentRepository.findAll().size();

        // Update the programCollectionContent
        ProgramCollectionContent updatedProgramCollectionContent = programCollectionContentRepository.findById(programCollectionContent.getId()).get();
        // Disconnect from session so that the updates on updatedProgramCollectionContent are not directly saved in db
        em.detach(updatedProgramCollectionContent);
        updatedProgramCollectionContent
            .itemName(UPDATED_ITEM_NAME)
            .itemId(UPDATED_ITEM_ID)
            .itemType(UPDATED_ITEM_TYPE)
            .contentType(UPDATED_CONTENT_TYPE)
            .itemIcon(UPDATED_ITEM_ICON)
            .hasRequired(UPDATED_HAS_REQUIRED)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        ProgramCollectionContentDTO programCollectionContentDTO = programCollectionContentMapper.toDto(updatedProgramCollectionContent);

        restProgramCollectionContentMockMvc.perform(put("/api/program-collection-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionContentDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramCollectionContent in the database
        List<ProgramCollectionContent> programCollectionContentList = programCollectionContentRepository.findAll();
        assertThat(programCollectionContentList).hasSize(databaseSizeBeforeUpdate);
        ProgramCollectionContent testProgramCollectionContent = programCollectionContentList.get(programCollectionContentList.size() - 1);
        assertThat(testProgramCollectionContent.getItemName()).isEqualTo(UPDATED_ITEM_NAME);
        assertThat(testProgramCollectionContent.getItemId()).isEqualTo(UPDATED_ITEM_ID);
        assertThat(testProgramCollectionContent.getItemType()).isEqualTo(UPDATED_ITEM_TYPE);
        assertThat(testProgramCollectionContent.getContentType()).isEqualTo(UPDATED_CONTENT_TYPE);
        assertThat(testProgramCollectionContent.getItemIcon()).isEqualTo(UPDATED_ITEM_ICON);
        assertThat(testProgramCollectionContent.isHasRequired()).isEqualTo(UPDATED_HAS_REQUIRED);
        assertThat(testProgramCollectionContent.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramCollectionContent.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramCollectionContent() throws Exception {
        int databaseSizeBeforeUpdate = programCollectionContentRepository.findAll().size();

        // Create the ProgramCollectionContent
        ProgramCollectionContentDTO programCollectionContentDTO = programCollectionContentMapper.toDto(programCollectionContent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramCollectionContentMockMvc.perform(put("/api/program-collection-contents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionContentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCollectionContent in the database
        List<ProgramCollectionContent> programCollectionContentList = programCollectionContentRepository.findAll();
        assertThat(programCollectionContentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramCollectionContent() throws Exception {
        // Initialize the database
        programCollectionContentRepository.saveAndFlush(programCollectionContent);

        int databaseSizeBeforeDelete = programCollectionContentRepository.findAll().size();

        // Delete the programCollectionContent
        restProgramCollectionContentMockMvc.perform(delete("/api/program-collection-contents/{id}", programCollectionContent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramCollectionContent> programCollectionContentList = programCollectionContentRepository.findAll();
        assertThat(programCollectionContentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCollectionContent.class);
        ProgramCollectionContent programCollectionContent1 = new ProgramCollectionContent();
        programCollectionContent1.setId(1L);
        ProgramCollectionContent programCollectionContent2 = new ProgramCollectionContent();
        programCollectionContent2.setId(programCollectionContent1.getId());
        assertThat(programCollectionContent1).isEqualTo(programCollectionContent2);
        programCollectionContent2.setId(2L);
        assertThat(programCollectionContent1).isNotEqualTo(programCollectionContent2);
        programCollectionContent1.setId(null);
        assertThat(programCollectionContent1).isNotEqualTo(programCollectionContent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCollectionContentDTO.class);
        ProgramCollectionContentDTO programCollectionContentDTO1 = new ProgramCollectionContentDTO();
        programCollectionContentDTO1.setId(1L);
        ProgramCollectionContentDTO programCollectionContentDTO2 = new ProgramCollectionContentDTO();
        assertThat(programCollectionContentDTO1).isNotEqualTo(programCollectionContentDTO2);
        programCollectionContentDTO2.setId(programCollectionContentDTO1.getId());
        assertThat(programCollectionContentDTO1).isEqualTo(programCollectionContentDTO2);
        programCollectionContentDTO2.setId(2L);
        assertThat(programCollectionContentDTO1).isNotEqualTo(programCollectionContentDTO2);
        programCollectionContentDTO1.setId(null);
        assertThat(programCollectionContentDTO1).isNotEqualTo(programCollectionContentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programCollectionContentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programCollectionContentMapper.fromId(null)).isNull();
    }
}
