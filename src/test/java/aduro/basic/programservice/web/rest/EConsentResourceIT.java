package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.EConsent;
import aduro.basic.programservice.repository.EConsentRepository;
import aduro.basic.programservice.service.EConsentService;
import aduro.basic.programservice.service.dto.EConsentDTO;
import aduro.basic.programservice.service.mapper.EConsentMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.EConsentCriteria;
import aduro.basic.programservice.service.EConsentQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link EConsentResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class EConsentResourceIT {

    private static final String DEFAULT_PARTICIPANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANT_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_PROGRAM_ID = 1L;
    private static final Long UPDATED_PROGRAM_ID = 2L;

    private static final Boolean DEFAULT_HAS_CONFIRMED = false;
    private static final Boolean UPDATED_HAS_CONFIRMED = true;

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_TERM_AND_CONDITION_ID = 1L;
    private static final Long UPDATED_TERM_AND_CONDITION_ID = 2L;

    @Autowired
    private EConsentRepository eConsentRepository;

    @Autowired
    private EConsentMapper eConsentMapper;

    @Autowired
    private EConsentService eConsentService;

    @Autowired
    private EConsentQueryService eConsentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restEConsentMockMvc;

    private EConsent eConsent;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EConsentResource eConsentResource = new EConsentResource(eConsentService, eConsentQueryService);
        this.restEConsentMockMvc = MockMvcBuilders.standaloneSetup(eConsentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EConsent createEntity(EntityManager em) {
        EConsent eConsent = new EConsent()
            .participantId(DEFAULT_PARTICIPANT_ID)
            .programId(DEFAULT_PROGRAM_ID)
            .hasConfirmed(DEFAULT_HAS_CONFIRMED)
            .createdAt(DEFAULT_CREATED_AT)
            .termAndConditionId(DEFAULT_TERM_AND_CONDITION_ID);
        return eConsent;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EConsent createUpdatedEntity(EntityManager em) {
        EConsent eConsent = new EConsent()
            .participantId(UPDATED_PARTICIPANT_ID)
            .programId(UPDATED_PROGRAM_ID)
            .hasConfirmed(UPDATED_HAS_CONFIRMED)
            .createdAt(UPDATED_CREATED_AT)
            .termAndConditionId(UPDATED_TERM_AND_CONDITION_ID);
        return eConsent;
    }

    @BeforeEach
    public void initTest() {
        eConsent = createEntity(em);
    }

    @Test
    @Transactional
    public void createEConsent() throws Exception {
        int databaseSizeBeforeCreate = eConsentRepository.findAll().size();

        // Create the EConsent
        EConsentDTO eConsentDTO = eConsentMapper.toDto(eConsent);
        restEConsentMockMvc.perform(post("/api/e-consents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eConsentDTO)))
            .andExpect(status().isCreated());

        // Validate the EConsent in the database
        List<EConsent> eConsentList = eConsentRepository.findAll();
        assertThat(eConsentList).hasSize(databaseSizeBeforeCreate + 1);
        EConsent testEConsent = eConsentList.get(eConsentList.size() - 1);
        assertThat(testEConsent.getParticipantId()).isEqualTo(DEFAULT_PARTICIPANT_ID);
        assertThat(testEConsent.getProgramId()).isEqualTo(DEFAULT_PROGRAM_ID);
        assertThat(testEConsent.isHasConfirmed()).isEqualTo(DEFAULT_HAS_CONFIRMED);
        assertThat(testEConsent.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testEConsent.getTermAndConditionId()).isEqualTo(DEFAULT_TERM_AND_CONDITION_ID);
    }

    @Test
    @Transactional
    public void createEConsentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = eConsentRepository.findAll().size();

        // Create the EConsent with an existing ID
        eConsent.setId(1L);
        EConsentDTO eConsentDTO = eConsentMapper.toDto(eConsent);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEConsentMockMvc.perform(post("/api/e-consents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eConsentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EConsent in the database
        List<EConsent> eConsentList = eConsentRepository.findAll();
        assertThat(eConsentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkProgramIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = eConsentRepository.findAll().size();
        // set the field null
        eConsent.setProgramId(null);

        // Create the EConsent, which fails.
        EConsentDTO eConsentDTO = eConsentMapper.toDto(eConsent);

        restEConsentMockMvc.perform(post("/api/e-consents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eConsentDTO)))
            .andExpect(status().isBadRequest());

        List<EConsent> eConsentList = eConsentRepository.findAll();
        assertThat(eConsentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTermAndConditionIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = eConsentRepository.findAll().size();
        // set the field null
        eConsent.setTermAndConditionId(null);

        // Create the EConsent, which fails.
        EConsentDTO eConsentDTO = eConsentMapper.toDto(eConsent);

        restEConsentMockMvc.perform(post("/api/e-consents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eConsentDTO)))
            .andExpect(status().isBadRequest());

        List<EConsent> eConsentList = eConsentRepository.findAll();
        assertThat(eConsentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllEConsents() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList
        restEConsentMockMvc.perform(get("/api/e-consents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eConsent.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID.toString())))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].hasConfirmed").value(hasItem(DEFAULT_HAS_CONFIRMED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].termAndConditionId").value(hasItem(DEFAULT_TERM_AND_CONDITION_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getEConsent() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get the eConsent
        restEConsentMockMvc.perform(get("/api/e-consents/{id}", eConsent.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(eConsent.getId().intValue()))
            .andExpect(jsonPath("$.participantId").value(DEFAULT_PARTICIPANT_ID.toString()))
            .andExpect(jsonPath("$.programId").value(DEFAULT_PROGRAM_ID.intValue()))
            .andExpect(jsonPath("$.hasConfirmed").value(DEFAULT_HAS_CONFIRMED.booleanValue()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.termAndConditionId").value(DEFAULT_TERM_AND_CONDITION_ID.intValue()));
    }

    @Test
    @Transactional
    public void getAllEConsentsByParticipantIdIsEqualToSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where participantId equals to DEFAULT_PARTICIPANT_ID
        defaultEConsentShouldBeFound("participantId.equals=" + DEFAULT_PARTICIPANT_ID);

        // Get all the eConsentList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultEConsentShouldNotBeFound("participantId.equals=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllEConsentsByParticipantIdIsInShouldWork() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where participantId in DEFAULT_PARTICIPANT_ID or UPDATED_PARTICIPANT_ID
        defaultEConsentShouldBeFound("participantId.in=" + DEFAULT_PARTICIPANT_ID + "," + UPDATED_PARTICIPANT_ID);

        // Get all the eConsentList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultEConsentShouldNotBeFound("participantId.in=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllEConsentsByParticipantIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where participantId is not null
        defaultEConsentShouldBeFound("participantId.specified=true");

        // Get all the eConsentList where participantId is null
        defaultEConsentShouldNotBeFound("participantId.specified=false");
    }

    @Test
    @Transactional
    public void getAllEConsentsByProgramIdIsEqualToSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where programId equals to DEFAULT_PROGRAM_ID
        defaultEConsentShouldBeFound("programId.equals=" + DEFAULT_PROGRAM_ID);

        // Get all the eConsentList where programId equals to UPDATED_PROGRAM_ID
        defaultEConsentShouldNotBeFound("programId.equals=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllEConsentsByProgramIdIsInShouldWork() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where programId in DEFAULT_PROGRAM_ID or UPDATED_PROGRAM_ID
        defaultEConsentShouldBeFound("programId.in=" + DEFAULT_PROGRAM_ID + "," + UPDATED_PROGRAM_ID);

        // Get all the eConsentList where programId equals to UPDATED_PROGRAM_ID
        defaultEConsentShouldNotBeFound("programId.in=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllEConsentsByProgramIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where programId is not null
        defaultEConsentShouldBeFound("programId.specified=true");

        // Get all the eConsentList where programId is null
        defaultEConsentShouldNotBeFound("programId.specified=false");
    }

    @Test
    @Transactional
    public void getAllEConsentsByProgramIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where programId greater than or equals to DEFAULT_PROGRAM_ID
        defaultEConsentShouldBeFound("programId.greaterOrEqualThan=" + DEFAULT_PROGRAM_ID);

        // Get all the eConsentList where programId greater than or equals to UPDATED_PROGRAM_ID
        defaultEConsentShouldNotBeFound("programId.greaterOrEqualThan=" + UPDATED_PROGRAM_ID);
    }

    @Test
    @Transactional
    public void getAllEConsentsByProgramIdIsLessThanSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where programId less than or equals to DEFAULT_PROGRAM_ID
        defaultEConsentShouldNotBeFound("programId.lessThan=" + DEFAULT_PROGRAM_ID);

        // Get all the eConsentList where programId less than or equals to UPDATED_PROGRAM_ID
        defaultEConsentShouldBeFound("programId.lessThan=" + UPDATED_PROGRAM_ID);
    }


    @Test
    @Transactional
    public void getAllEConsentsByHasConfirmedIsEqualToSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where hasConfirmed equals to DEFAULT_HAS_CONFIRMED
        defaultEConsentShouldBeFound("hasConfirmed.equals=" + DEFAULT_HAS_CONFIRMED);

        // Get all the eConsentList where hasConfirmed equals to UPDATED_HAS_CONFIRMED
        defaultEConsentShouldNotBeFound("hasConfirmed.equals=" + UPDATED_HAS_CONFIRMED);
    }

    @Test
    @Transactional
    public void getAllEConsentsByHasConfirmedIsInShouldWork() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where hasConfirmed in DEFAULT_HAS_CONFIRMED or UPDATED_HAS_CONFIRMED
        defaultEConsentShouldBeFound("hasConfirmed.in=" + DEFAULT_HAS_CONFIRMED + "," + UPDATED_HAS_CONFIRMED);

        // Get all the eConsentList where hasConfirmed equals to UPDATED_HAS_CONFIRMED
        defaultEConsentShouldNotBeFound("hasConfirmed.in=" + UPDATED_HAS_CONFIRMED);
    }

    @Test
    @Transactional
    public void getAllEConsentsByHasConfirmedIsNullOrNotNull() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where hasConfirmed is not null
        defaultEConsentShouldBeFound("hasConfirmed.specified=true");

        // Get all the eConsentList where hasConfirmed is null
        defaultEConsentShouldNotBeFound("hasConfirmed.specified=false");
    }

    @Test
    @Transactional
    public void getAllEConsentsByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where createdAt equals to DEFAULT_CREATED_AT
        defaultEConsentShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the eConsentList where createdAt equals to UPDATED_CREATED_AT
        defaultEConsentShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllEConsentsByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultEConsentShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the eConsentList where createdAt equals to UPDATED_CREATED_AT
        defaultEConsentShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllEConsentsByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where createdAt is not null
        defaultEConsentShouldBeFound("createdAt.specified=true");

        // Get all the eConsentList where createdAt is null
        defaultEConsentShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllEConsentsByTermAndConditionIdIsEqualToSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where termAndConditionId equals to DEFAULT_TERM_AND_CONDITION_ID
        defaultEConsentShouldBeFound("termAndConditionId.equals=" + DEFAULT_TERM_AND_CONDITION_ID);

        // Get all the eConsentList where termAndConditionId equals to UPDATED_TERM_AND_CONDITION_ID
        defaultEConsentShouldNotBeFound("termAndConditionId.equals=" + UPDATED_TERM_AND_CONDITION_ID);
    }

    @Test
    @Transactional
    public void getAllEConsentsByTermAndConditionIdIsInShouldWork() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where termAndConditionId in DEFAULT_TERM_AND_CONDITION_ID or UPDATED_TERM_AND_CONDITION_ID
        defaultEConsentShouldBeFound("termAndConditionId.in=" + DEFAULT_TERM_AND_CONDITION_ID + "," + UPDATED_TERM_AND_CONDITION_ID);

        // Get all the eConsentList where termAndConditionId equals to UPDATED_TERM_AND_CONDITION_ID
        defaultEConsentShouldNotBeFound("termAndConditionId.in=" + UPDATED_TERM_AND_CONDITION_ID);
    }

    @Test
    @Transactional
    public void getAllEConsentsByTermAndConditionIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where termAndConditionId is not null
        defaultEConsentShouldBeFound("termAndConditionId.specified=true");

        // Get all the eConsentList where termAndConditionId is null
        defaultEConsentShouldNotBeFound("termAndConditionId.specified=false");
    }

    @Test
    @Transactional
    public void getAllEConsentsByTermAndConditionIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where termAndConditionId greater than or equals to DEFAULT_TERM_AND_CONDITION_ID
        defaultEConsentShouldBeFound("termAndConditionId.greaterOrEqualThan=" + DEFAULT_TERM_AND_CONDITION_ID);

        // Get all the eConsentList where termAndConditionId greater than or equals to UPDATED_TERM_AND_CONDITION_ID
        defaultEConsentShouldNotBeFound("termAndConditionId.greaterOrEqualThan=" + UPDATED_TERM_AND_CONDITION_ID);
    }

    @Test
    @Transactional
    public void getAllEConsentsByTermAndConditionIdIsLessThanSomething() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        // Get all the eConsentList where termAndConditionId less than or equals to DEFAULT_TERM_AND_CONDITION_ID
        defaultEConsentShouldNotBeFound("termAndConditionId.lessThan=" + DEFAULT_TERM_AND_CONDITION_ID);

        // Get all the eConsentList where termAndConditionId less than or equals to UPDATED_TERM_AND_CONDITION_ID
        defaultEConsentShouldBeFound("termAndConditionId.lessThan=" + UPDATED_TERM_AND_CONDITION_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultEConsentShouldBeFound(String filter) throws Exception {
        restEConsentMockMvc.perform(get("/api/e-consents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eConsent.getId().intValue())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID)))
            .andExpect(jsonPath("$.[*].programId").value(hasItem(DEFAULT_PROGRAM_ID.intValue())))
            .andExpect(jsonPath("$.[*].hasConfirmed").value(hasItem(DEFAULT_HAS_CONFIRMED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].termAndConditionId").value(hasItem(DEFAULT_TERM_AND_CONDITION_ID.intValue())));

        // Check, that the count call also returns 1
        restEConsentMockMvc.perform(get("/api/e-consents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultEConsentShouldNotBeFound(String filter) throws Exception {
        restEConsentMockMvc.perform(get("/api/e-consents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restEConsentMockMvc.perform(get("/api/e-consents/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingEConsent() throws Exception {
        // Get the eConsent
        restEConsentMockMvc.perform(get("/api/e-consents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEConsent() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        int databaseSizeBeforeUpdate = eConsentRepository.findAll().size();

        // Update the eConsent
        EConsent updatedEConsent = eConsentRepository.findById(eConsent.getId()).get();
        // Disconnect from session so that the updates on updatedEConsent are not directly saved in db
        em.detach(updatedEConsent);
        updatedEConsent
            .participantId(UPDATED_PARTICIPANT_ID)
            .programId(UPDATED_PROGRAM_ID)
            .hasConfirmed(UPDATED_HAS_CONFIRMED)
            .createdAt(UPDATED_CREATED_AT)
            .termAndConditionId(UPDATED_TERM_AND_CONDITION_ID);
        EConsentDTO eConsentDTO = eConsentMapper.toDto(updatedEConsent);

        restEConsentMockMvc.perform(put("/api/e-consents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eConsentDTO)))
            .andExpect(status().isOk());

        // Validate the EConsent in the database
        List<EConsent> eConsentList = eConsentRepository.findAll();
        assertThat(eConsentList).hasSize(databaseSizeBeforeUpdate);
        EConsent testEConsent = eConsentList.get(eConsentList.size() - 1);
        assertThat(testEConsent.getParticipantId()).isEqualTo(UPDATED_PARTICIPANT_ID);
        assertThat(testEConsent.getProgramId()).isEqualTo(UPDATED_PROGRAM_ID);
        assertThat(testEConsent.isHasConfirmed()).isEqualTo(UPDATED_HAS_CONFIRMED);
        assertThat(testEConsent.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testEConsent.getTermAndConditionId()).isEqualTo(UPDATED_TERM_AND_CONDITION_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingEConsent() throws Exception {
        int databaseSizeBeforeUpdate = eConsentRepository.findAll().size();

        // Create the EConsent
        EConsentDTO eConsentDTO = eConsentMapper.toDto(eConsent);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEConsentMockMvc.perform(put("/api/e-consents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eConsentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the EConsent in the database
        List<EConsent> eConsentList = eConsentRepository.findAll();
        assertThat(eConsentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteEConsent() throws Exception {
        // Initialize the database
        eConsentRepository.saveAndFlush(eConsent);

        int databaseSizeBeforeDelete = eConsentRepository.findAll().size();

        // Delete the eConsent
        restEConsentMockMvc.perform(delete("/api/e-consents/{id}", eConsent.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<EConsent> eConsentList = eConsentRepository.findAll();
        assertThat(eConsentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EConsent.class);
        EConsent eConsent1 = new EConsent();
        eConsent1.setId(1L);
        EConsent eConsent2 = new EConsent();
        eConsent2.setId(eConsent1.getId());
        assertThat(eConsent1).isEqualTo(eConsent2);
        eConsent2.setId(2L);
        assertThat(eConsent1).isNotEqualTo(eConsent2);
        eConsent1.setId(null);
        assertThat(eConsent1).isNotEqualTo(eConsent2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EConsentDTO.class);
        EConsentDTO eConsentDTO1 = new EConsentDTO();
        eConsentDTO1.setId(1L);
        EConsentDTO eConsentDTO2 = new EConsentDTO();
        assertThat(eConsentDTO1).isNotEqualTo(eConsentDTO2);
        eConsentDTO2.setId(eConsentDTO1.getId());
        assertThat(eConsentDTO1).isEqualTo(eConsentDTO2);
        eConsentDTO2.setId(2L);
        assertThat(eConsentDTO1).isNotEqualTo(eConsentDTO2);
        eConsentDTO1.setId(null);
        assertThat(eConsentDTO1).isNotEqualTo(eConsentDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(eConsentMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(eConsentMapper.fromId(null)).isNull();
    }
}
