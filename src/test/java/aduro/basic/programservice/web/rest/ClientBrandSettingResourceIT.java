package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ClientBrandSetting;
import aduro.basic.programservice.repository.ClientBrandSettingRepository;
import aduro.basic.programservice.service.ClientBrandSettingService;
import aduro.basic.programservice.service.dto.ClientBrandSettingDTO;
import aduro.basic.programservice.service.mapper.ClientBrandSettingMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ClientBrandSettingCriteria;
import aduro.basic.programservice.service.ClientBrandSettingQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ClientBrandSettingResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ClientBrandSettingResourceIT {

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_PATH_URL = "AAAAAAAAAA";
    private static final String UPDATED_SUB_PATH_URL = "BBBBBBBBBB";

    private static final String DEFAULT_WEB_LOGO_URL = "AAAAAAAAAA";
    private static final String UPDATED_WEB_LOGO_URL = "BBBBBBBBBB";

    private static final String DEFAULT_PRIMARY_COLOR_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_PRIMARY_COLOR_VALUE = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_URL = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_URL = "BBBBBBBBBB";

    @Autowired
    private ClientBrandSettingRepository clientBrandSettingRepository;

    @Autowired
    private ClientBrandSettingMapper clientBrandSettingMapper;

    @Autowired
    private ClientBrandSettingService clientBrandSettingService;

    @Autowired
    private ClientBrandSettingQueryService clientBrandSettingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClientBrandSettingMockMvc;

    private ClientBrandSetting clientBrandSetting;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientBrandSettingResource clientBrandSettingResource = new ClientBrandSettingResource(clientBrandSettingService, clientBrandSettingQueryService);
        this.restClientBrandSettingMockMvc = MockMvcBuilders.standaloneSetup(clientBrandSettingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientBrandSetting createEntity(EntityManager em) {
        ClientBrandSetting clientBrandSetting = new ClientBrandSetting()
            .clientId(DEFAULT_CLIENT_ID)
            .subPathUrl(DEFAULT_SUB_PATH_URL)
            .webLogoUrl(DEFAULT_WEB_LOGO_URL)
            .primaryColorValue(DEFAULT_PRIMARY_COLOR_VALUE)
            .clientName(DEFAULT_CLIENT_NAME)
            .clientUrl(DEFAULT_CLIENT_URL);
        return clientBrandSetting;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientBrandSetting createUpdatedEntity(EntityManager em) {
        ClientBrandSetting clientBrandSetting = new ClientBrandSetting()
            .clientId(UPDATED_CLIENT_ID)
            .subPathUrl(UPDATED_SUB_PATH_URL)
            .webLogoUrl(UPDATED_WEB_LOGO_URL)
            .primaryColorValue(UPDATED_PRIMARY_COLOR_VALUE)
            .clientName(UPDATED_CLIENT_NAME)
            .clientUrl(UPDATED_CLIENT_URL);
        return clientBrandSetting;
    }

    @BeforeEach
    public void initTest() {
        clientBrandSetting = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientBrandSetting() throws Exception {
        int databaseSizeBeforeCreate = clientBrandSettingRepository.findAll().size();

        // Create the ClientBrandSetting
        ClientBrandSettingDTO clientBrandSettingDTO = clientBrandSettingMapper.toDto(clientBrandSetting);
        restClientBrandSettingMockMvc.perform(post("/api/client-brand-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientBrandSettingDTO)))
            .andExpect(status().isCreated());

        // Validate the ClientBrandSetting in the database
        List<ClientBrandSetting> clientBrandSettingList = clientBrandSettingRepository.findAll();
        assertThat(clientBrandSettingList).hasSize(databaseSizeBeforeCreate + 1);
        ClientBrandSetting testClientBrandSetting = clientBrandSettingList.get(clientBrandSettingList.size() - 1);
        assertThat(testClientBrandSetting.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testClientBrandSetting.getSubPathUrl()).isEqualTo(DEFAULT_SUB_PATH_URL);
        assertThat(testClientBrandSetting.getWebLogoUrl()).isEqualTo(DEFAULT_WEB_LOGO_URL);
        assertThat(testClientBrandSetting.getPrimaryColorValue()).isEqualTo(DEFAULT_PRIMARY_COLOR_VALUE);
        assertThat(testClientBrandSetting.getClientName()).isEqualTo(DEFAULT_CLIENT_NAME);
        assertThat(testClientBrandSetting.getClientUrl()).isEqualTo(DEFAULT_CLIENT_URL);
    }

    @Test
    @Transactional
    public void createClientBrandSettingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientBrandSettingRepository.findAll().size();

        // Create the ClientBrandSetting with an existing ID
        clientBrandSetting.setId(1L);
        ClientBrandSettingDTO clientBrandSettingDTO = clientBrandSettingMapper.toDto(clientBrandSetting);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientBrandSettingMockMvc.perform(post("/api/client-brand-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientBrandSettingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientBrandSetting in the database
        List<ClientBrandSetting> clientBrandSettingList = clientBrandSettingRepository.findAll();
        assertThat(clientBrandSettingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientBrandSettingRepository.findAll().size();
        // set the field null
        clientBrandSetting.setClientId(null);

        // Create the ClientBrandSetting, which fails.
        ClientBrandSettingDTO clientBrandSettingDTO = clientBrandSettingMapper.toDto(clientBrandSetting);

        restClientBrandSettingMockMvc.perform(post("/api/client-brand-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientBrandSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientBrandSetting> clientBrandSettingList = clientBrandSettingRepository.findAll();
        assertThat(clientBrandSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSubPathUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientBrandSettingRepository.findAll().size();
        // set the field null
        clientBrandSetting.setSubPathUrl(null);

        // Create the ClientBrandSetting, which fails.
        ClientBrandSettingDTO clientBrandSettingDTO = clientBrandSettingMapper.toDto(clientBrandSetting);

        restClientBrandSettingMockMvc.perform(post("/api/client-brand-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientBrandSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientBrandSetting> clientBrandSettingList = clientBrandSettingRepository.findAll();
        assertThat(clientBrandSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettings() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList
        restClientBrandSettingMockMvc.perform(get("/api/client-brand-settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientBrandSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].subPathUrl").value(hasItem(DEFAULT_SUB_PATH_URL.toString())))
            .andExpect(jsonPath("$.[*].webLogoUrl").value(hasItem(DEFAULT_WEB_LOGO_URL.toString())))
            .andExpect(jsonPath("$.[*].primaryColorValue").value(hasItem(DEFAULT_PRIMARY_COLOR_VALUE.toString())))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].clientUrl").value(hasItem(DEFAULT_CLIENT_URL.toString())));
    }
    
    @Test
    @Transactional
    public void getClientBrandSetting() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get the clientBrandSetting
        restClientBrandSettingMockMvc.perform(get("/api/client-brand-settings/{id}", clientBrandSetting.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientBrandSetting.getId().intValue()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.subPathUrl").value(DEFAULT_SUB_PATH_URL.toString()))
            .andExpect(jsonPath("$.webLogoUrl").value(DEFAULT_WEB_LOGO_URL.toString()))
            .andExpect(jsonPath("$.primaryColorValue").value(DEFAULT_PRIMARY_COLOR_VALUE.toString()))
            .andExpect(jsonPath("$.clientName").value(DEFAULT_CLIENT_NAME.toString()))
            .andExpect(jsonPath("$.clientUrl").value(DEFAULT_CLIENT_URL.toString()));
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientId equals to DEFAULT_CLIENT_ID
        defaultClientBrandSettingShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the clientBrandSettingList where clientId equals to UPDATED_CLIENT_ID
        defaultClientBrandSettingShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultClientBrandSettingShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the clientBrandSettingList where clientId equals to UPDATED_CLIENT_ID
        defaultClientBrandSettingShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientId is not null
        defaultClientBrandSettingShouldBeFound("clientId.specified=true");

        // Get all the clientBrandSettingList where clientId is null
        defaultClientBrandSettingShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsBySubPathUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where subPathUrl equals to DEFAULT_SUB_PATH_URL
        defaultClientBrandSettingShouldBeFound("subPathUrl.equals=" + DEFAULT_SUB_PATH_URL);

        // Get all the clientBrandSettingList where subPathUrl equals to UPDATED_SUB_PATH_URL
        defaultClientBrandSettingShouldNotBeFound("subPathUrl.equals=" + UPDATED_SUB_PATH_URL);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsBySubPathUrlIsInShouldWork() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where subPathUrl in DEFAULT_SUB_PATH_URL or UPDATED_SUB_PATH_URL
        defaultClientBrandSettingShouldBeFound("subPathUrl.in=" + DEFAULT_SUB_PATH_URL + "," + UPDATED_SUB_PATH_URL);

        // Get all the clientBrandSettingList where subPathUrl equals to UPDATED_SUB_PATH_URL
        defaultClientBrandSettingShouldNotBeFound("subPathUrl.in=" + UPDATED_SUB_PATH_URL);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsBySubPathUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where subPathUrl is not null
        defaultClientBrandSettingShouldBeFound("subPathUrl.specified=true");

        // Get all the clientBrandSettingList where subPathUrl is null
        defaultClientBrandSettingShouldNotBeFound("subPathUrl.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByWebLogoUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where webLogoUrl equals to DEFAULT_WEB_LOGO_URL
        defaultClientBrandSettingShouldBeFound("webLogoUrl.equals=" + DEFAULT_WEB_LOGO_URL);

        // Get all the clientBrandSettingList where webLogoUrl equals to UPDATED_WEB_LOGO_URL
        defaultClientBrandSettingShouldNotBeFound("webLogoUrl.equals=" + UPDATED_WEB_LOGO_URL);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByWebLogoUrlIsInShouldWork() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where webLogoUrl in DEFAULT_WEB_LOGO_URL or UPDATED_WEB_LOGO_URL
        defaultClientBrandSettingShouldBeFound("webLogoUrl.in=" + DEFAULT_WEB_LOGO_URL + "," + UPDATED_WEB_LOGO_URL);

        // Get all the clientBrandSettingList where webLogoUrl equals to UPDATED_WEB_LOGO_URL
        defaultClientBrandSettingShouldNotBeFound("webLogoUrl.in=" + UPDATED_WEB_LOGO_URL);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByWebLogoUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where webLogoUrl is not null
        defaultClientBrandSettingShouldBeFound("webLogoUrl.specified=true");

        // Get all the clientBrandSettingList where webLogoUrl is null
        defaultClientBrandSettingShouldNotBeFound("webLogoUrl.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByPrimaryColorValueIsEqualToSomething() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where primaryColorValue equals to DEFAULT_PRIMARY_COLOR_VALUE
        defaultClientBrandSettingShouldBeFound("primaryColorValue.equals=" + DEFAULT_PRIMARY_COLOR_VALUE);

        // Get all the clientBrandSettingList where primaryColorValue equals to UPDATED_PRIMARY_COLOR_VALUE
        defaultClientBrandSettingShouldNotBeFound("primaryColorValue.equals=" + UPDATED_PRIMARY_COLOR_VALUE);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByPrimaryColorValueIsInShouldWork() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where primaryColorValue in DEFAULT_PRIMARY_COLOR_VALUE or UPDATED_PRIMARY_COLOR_VALUE
        defaultClientBrandSettingShouldBeFound("primaryColorValue.in=" + DEFAULT_PRIMARY_COLOR_VALUE + "," + UPDATED_PRIMARY_COLOR_VALUE);

        // Get all the clientBrandSettingList where primaryColorValue equals to UPDATED_PRIMARY_COLOR_VALUE
        defaultClientBrandSettingShouldNotBeFound("primaryColorValue.in=" + UPDATED_PRIMARY_COLOR_VALUE);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByPrimaryColorValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where primaryColorValue is not null
        defaultClientBrandSettingShouldBeFound("primaryColorValue.specified=true");

        // Get all the clientBrandSettingList where primaryColorValue is null
        defaultClientBrandSettingShouldNotBeFound("primaryColorValue.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientNameIsEqualToSomething() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientName equals to DEFAULT_CLIENT_NAME
        defaultClientBrandSettingShouldBeFound("clientName.equals=" + DEFAULT_CLIENT_NAME);

        // Get all the clientBrandSettingList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientBrandSettingShouldNotBeFound("clientName.equals=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientNameIsInShouldWork() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientName in DEFAULT_CLIENT_NAME or UPDATED_CLIENT_NAME
        defaultClientBrandSettingShouldBeFound("clientName.in=" + DEFAULT_CLIENT_NAME + "," + UPDATED_CLIENT_NAME);

        // Get all the clientBrandSettingList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientBrandSettingShouldNotBeFound("clientName.in=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientName is not null
        defaultClientBrandSettingShouldBeFound("clientName.specified=true");

        // Get all the clientBrandSettingList where clientName is null
        defaultClientBrandSettingShouldNotBeFound("clientName.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientUrl equals to DEFAULT_CLIENT_URL
        defaultClientBrandSettingShouldBeFound("clientUrl.equals=" + DEFAULT_CLIENT_URL);

        // Get all the clientBrandSettingList where clientUrl equals to UPDATED_CLIENT_URL
        defaultClientBrandSettingShouldNotBeFound("clientUrl.equals=" + UPDATED_CLIENT_URL);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientUrlIsInShouldWork() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientUrl in DEFAULT_CLIENT_URL or UPDATED_CLIENT_URL
        defaultClientBrandSettingShouldBeFound("clientUrl.in=" + DEFAULT_CLIENT_URL + "," + UPDATED_CLIENT_URL);

        // Get all the clientBrandSettingList where clientUrl equals to UPDATED_CLIENT_URL
        defaultClientBrandSettingShouldNotBeFound("clientUrl.in=" + UPDATED_CLIENT_URL);
    }

    @Test
    @Transactional
    public void getAllClientBrandSettingsByClientUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        // Get all the clientBrandSettingList where clientUrl is not null
        defaultClientBrandSettingShouldBeFound("clientUrl.specified=true");

        // Get all the clientBrandSettingList where clientUrl is null
        defaultClientBrandSettingShouldNotBeFound("clientUrl.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClientBrandSettingShouldBeFound(String filter) throws Exception {
        restClientBrandSettingMockMvc.perform(get("/api/client-brand-settings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientBrandSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].subPathUrl").value(hasItem(DEFAULT_SUB_PATH_URL)))
            .andExpect(jsonPath("$.[*].webLogoUrl").value(hasItem(DEFAULT_WEB_LOGO_URL)))
            .andExpect(jsonPath("$.[*].primaryColorValue").value(hasItem(DEFAULT_PRIMARY_COLOR_VALUE)))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME)))
            .andExpect(jsonPath("$.[*].clientUrl").value(hasItem(DEFAULT_CLIENT_URL)));

        // Check, that the count call also returns 1
        restClientBrandSettingMockMvc.perform(get("/api/client-brand-settings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClientBrandSettingShouldNotBeFound(String filter) throws Exception {
        restClientBrandSettingMockMvc.perform(get("/api/client-brand-settings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClientBrandSettingMockMvc.perform(get("/api/client-brand-settings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingClientBrandSetting() throws Exception {
        // Get the clientBrandSetting
        restClientBrandSettingMockMvc.perform(get("/api/client-brand-settings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientBrandSetting() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        int databaseSizeBeforeUpdate = clientBrandSettingRepository.findAll().size();

        // Update the clientBrandSetting
        ClientBrandSetting updatedClientBrandSetting = clientBrandSettingRepository.findById(clientBrandSetting.getId()).get();
        // Disconnect from session so that the updates on updatedClientBrandSetting are not directly saved in db
        em.detach(updatedClientBrandSetting);
        updatedClientBrandSetting
            .clientId(UPDATED_CLIENT_ID)
            .subPathUrl(UPDATED_SUB_PATH_URL)
            .webLogoUrl(UPDATED_WEB_LOGO_URL)
            .primaryColorValue(UPDATED_PRIMARY_COLOR_VALUE)
            .clientName(UPDATED_CLIENT_NAME)
            .clientUrl(UPDATED_CLIENT_URL);
        ClientBrandSettingDTO clientBrandSettingDTO = clientBrandSettingMapper.toDto(updatedClientBrandSetting);

        restClientBrandSettingMockMvc.perform(put("/api/client-brand-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientBrandSettingDTO)))
            .andExpect(status().isOk());

        // Validate the ClientBrandSetting in the database
        List<ClientBrandSetting> clientBrandSettingList = clientBrandSettingRepository.findAll();
        assertThat(clientBrandSettingList).hasSize(databaseSizeBeforeUpdate);
        ClientBrandSetting testClientBrandSetting = clientBrandSettingList.get(clientBrandSettingList.size() - 1);
        assertThat(testClientBrandSetting.getClientId()).isEqualTo(UPDATED_CLIENT_ID);
        assertThat(testClientBrandSetting.getSubPathUrl()).isEqualTo(UPDATED_SUB_PATH_URL);
        assertThat(testClientBrandSetting.getWebLogoUrl()).isEqualTo(UPDATED_WEB_LOGO_URL);
        assertThat(testClientBrandSetting.getPrimaryColorValue()).isEqualTo(UPDATED_PRIMARY_COLOR_VALUE);
        assertThat(testClientBrandSetting.getClientName()).isEqualTo(UPDATED_CLIENT_NAME);
        assertThat(testClientBrandSetting.getClientUrl()).isEqualTo(UPDATED_CLIENT_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingClientBrandSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientBrandSettingRepository.findAll().size();

        // Create the ClientBrandSetting
        ClientBrandSettingDTO clientBrandSettingDTO = clientBrandSettingMapper.toDto(clientBrandSetting);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientBrandSettingMockMvc.perform(put("/api/client-brand-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientBrandSettingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientBrandSetting in the database
        List<ClientBrandSetting> clientBrandSettingList = clientBrandSettingRepository.findAll();
        assertThat(clientBrandSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClientBrandSetting() throws Exception {
        // Initialize the database
        clientBrandSettingRepository.saveAndFlush(clientBrandSetting);

        int databaseSizeBeforeDelete = clientBrandSettingRepository.findAll().size();

        // Delete the clientBrandSetting
        restClientBrandSettingMockMvc.perform(delete("/api/client-brand-settings/{id}", clientBrandSetting.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ClientBrandSetting> clientBrandSettingList = clientBrandSettingRepository.findAll();
        assertThat(clientBrandSettingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientBrandSetting.class);
        ClientBrandSetting clientBrandSetting1 = new ClientBrandSetting();
        clientBrandSetting1.setId(1L);
        ClientBrandSetting clientBrandSetting2 = new ClientBrandSetting();
        clientBrandSetting2.setId(clientBrandSetting1.getId());
        assertThat(clientBrandSetting1).isEqualTo(clientBrandSetting2);
        clientBrandSetting2.setId(2L);
        assertThat(clientBrandSetting1).isNotEqualTo(clientBrandSetting2);
        clientBrandSetting1.setId(null);
        assertThat(clientBrandSetting1).isNotEqualTo(clientBrandSetting2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientBrandSettingDTO.class);
        ClientBrandSettingDTO clientBrandSettingDTO1 = new ClientBrandSettingDTO();
        clientBrandSettingDTO1.setId(1L);
        ClientBrandSettingDTO clientBrandSettingDTO2 = new ClientBrandSettingDTO();
        assertThat(clientBrandSettingDTO1).isNotEqualTo(clientBrandSettingDTO2);
        clientBrandSettingDTO2.setId(clientBrandSettingDTO1.getId());
        assertThat(clientBrandSettingDTO1).isEqualTo(clientBrandSettingDTO2);
        clientBrandSettingDTO2.setId(2L);
        assertThat(clientBrandSettingDTO1).isNotEqualTo(clientBrandSettingDTO2);
        clientBrandSettingDTO1.setId(null);
        assertThat(clientBrandSettingDTO1).isNotEqualTo(clientBrandSettingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientBrandSettingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientBrandSettingMapper.fromId(null)).isNull();
    }
}
