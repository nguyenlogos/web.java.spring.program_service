package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramLevelPractice;
import aduro.basic.programservice.domain.ProgramLevel;
import aduro.basic.programservice.repository.ProgramLevelPracticeRepository;
import aduro.basic.programservice.service.ProgramLevelPracticeService;
import aduro.basic.programservice.service.dto.ProgramLevelPracticeDTO;
import aduro.basic.programservice.service.mapper.ProgramLevelPracticeMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramLevelPracticeCriteria;
import aduro.basic.programservice.service.ProgramLevelPracticeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramLevelPracticeResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramLevelPracticeResourceIT {

    private static final String DEFAULT_PRACTICE_ID = "AAAAAAAAAA";
    private static final String UPDATED_PRACTICE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_ID = "BBBBBBBBBB";

    private static final String DEFAULT_SUBGROUP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_SUBGROUP_NAME = "BBBBBBBBBB";

    @Autowired
    private ProgramLevelPracticeRepository programLevelPracticeRepository;

    @Autowired
    private ProgramLevelPracticeMapper programLevelPracticeMapper;

    @Autowired
    private ProgramLevelPracticeService programLevelPracticeService;

    @Autowired
    private ProgramLevelPracticeQueryService programLevelPracticeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramLevelPracticeMockMvc;

    private ProgramLevelPractice programLevelPractice;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramLevelPracticeResource programLevelPracticeResource = new ProgramLevelPracticeResource(programLevelPracticeService, programLevelPracticeQueryService);
        this.restProgramLevelPracticeMockMvc = MockMvcBuilders.standaloneSetup(programLevelPracticeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevelPractice createEntity(EntityManager em) {
        ProgramLevelPractice programLevelPractice = new ProgramLevelPractice()
            .practiceId(DEFAULT_PRACTICE_ID)
            .name(DEFAULT_NAME)
            .subgroupId(DEFAULT_SUBGROUP_ID)
            .subgroupName(DEFAULT_SUBGROUP_NAME);
        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }
        programLevelPractice.setProgramLevel(programLevel);
        return programLevelPractice;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramLevelPractice createUpdatedEntity(EntityManager em) {
        ProgramLevelPractice programLevelPractice = new ProgramLevelPractice()
            .practiceId(UPDATED_PRACTICE_ID)
            .name(UPDATED_NAME)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME);
        // Add required entity
        ProgramLevel programLevel;
        if (TestUtil.findAll(em, ProgramLevel.class).isEmpty()) {
            programLevel = ProgramLevelResourceIT.createUpdatedEntity(em);
            em.persist(programLevel);
            em.flush();
        } else {
            programLevel = TestUtil.findAll(em, ProgramLevel.class).get(0);
        }
        programLevelPractice.setProgramLevel(programLevel);
        return programLevelPractice;
    }

    @BeforeEach
    public void initTest() {
        programLevelPractice = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramLevelPractice() throws Exception {
        int databaseSizeBeforeCreate = programLevelPracticeRepository.findAll().size();

        // Create the ProgramLevelPractice
        ProgramLevelPracticeDTO programLevelPracticeDTO = programLevelPracticeMapper.toDto(programLevelPractice);
        restProgramLevelPracticeMockMvc.perform(post("/api/program-level-practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPracticeDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramLevelPractice in the database
        List<ProgramLevelPractice> programLevelPracticeList = programLevelPracticeRepository.findAll();
        assertThat(programLevelPracticeList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramLevelPractice testProgramLevelPractice = programLevelPracticeList.get(programLevelPracticeList.size() - 1);
        assertThat(testProgramLevelPractice.getPracticeId()).isEqualTo(DEFAULT_PRACTICE_ID);
        assertThat(testProgramLevelPractice.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProgramLevelPractice.getSubgroupId()).isEqualTo(DEFAULT_SUBGROUP_ID);
        assertThat(testProgramLevelPractice.getSubgroupName()).isEqualTo(DEFAULT_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void createProgramLevelPracticeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programLevelPracticeRepository.findAll().size();

        // Create the ProgramLevelPractice with an existing ID
        programLevelPractice.setId(1L);
        ProgramLevelPracticeDTO programLevelPracticeDTO = programLevelPracticeMapper.toDto(programLevelPractice);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramLevelPracticeMockMvc.perform(post("/api/program-level-practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPracticeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevelPractice in the database
        List<ProgramLevelPractice> programLevelPracticeList = programLevelPracticeRepository.findAll();
        assertThat(programLevelPracticeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkPracticeIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = programLevelPracticeRepository.findAll().size();
        // set the field null
        programLevelPractice.setPracticeId(null);

        // Create the ProgramLevelPractice, which fails.
        ProgramLevelPracticeDTO programLevelPracticeDTO = programLevelPracticeMapper.toDto(programLevelPractice);

        restProgramLevelPracticeMockMvc.perform(post("/api/program-level-practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPracticeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramLevelPractice> programLevelPracticeList = programLevelPracticeRepository.findAll();
        assertThat(programLevelPracticeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = programLevelPracticeRepository.findAll().size();
        // set the field null
        programLevelPractice.setName(null);

        // Create the ProgramLevelPractice, which fails.
        ProgramLevelPracticeDTO programLevelPracticeDTO = programLevelPracticeMapper.toDto(programLevelPractice);

        restProgramLevelPracticeMockMvc.perform(post("/api/program-level-practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPracticeDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramLevelPractice> programLevelPracticeList = programLevelPracticeRepository.findAll();
        assertThat(programLevelPracticeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPractices() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList
        restProgramLevelPracticeMockMvc.perform(get("/api/program-level-practices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevelPractice.getId().intValue())))
            .andExpect(jsonPath("$.[*].practiceId").value(hasItem(DEFAULT_PRACTICE_ID.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID.toString())))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramLevelPractice() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get the programLevelPractice
        restProgramLevelPracticeMockMvc.perform(get("/api/program-level-practices/{id}", programLevelPractice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programLevelPractice.getId().intValue()))
            .andExpect(jsonPath("$.practiceId").value(DEFAULT_PRACTICE_ID.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.subgroupId").value(DEFAULT_SUBGROUP_ID.toString()))
            .andExpect(jsonPath("$.subgroupName").value(DEFAULT_SUBGROUP_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesByPracticeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where practiceId equals to DEFAULT_PRACTICE_ID
        defaultProgramLevelPracticeShouldBeFound("practiceId.equals=" + DEFAULT_PRACTICE_ID);

        // Get all the programLevelPracticeList where practiceId equals to UPDATED_PRACTICE_ID
        defaultProgramLevelPracticeShouldNotBeFound("practiceId.equals=" + UPDATED_PRACTICE_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesByPracticeIdIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where practiceId in DEFAULT_PRACTICE_ID or UPDATED_PRACTICE_ID
        defaultProgramLevelPracticeShouldBeFound("practiceId.in=" + DEFAULT_PRACTICE_ID + "," + UPDATED_PRACTICE_ID);

        // Get all the programLevelPracticeList where practiceId equals to UPDATED_PRACTICE_ID
        defaultProgramLevelPracticeShouldNotBeFound("practiceId.in=" + UPDATED_PRACTICE_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesByPracticeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where practiceId is not null
        defaultProgramLevelPracticeShouldBeFound("practiceId.specified=true");

        // Get all the programLevelPracticeList where practiceId is null
        defaultProgramLevelPracticeShouldNotBeFound("practiceId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where name equals to DEFAULT_NAME
        defaultProgramLevelPracticeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the programLevelPracticeList where name equals to UPDATED_NAME
        defaultProgramLevelPracticeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultProgramLevelPracticeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the programLevelPracticeList where name equals to UPDATED_NAME
        defaultProgramLevelPracticeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where name is not null
        defaultProgramLevelPracticeShouldBeFound("name.specified=true");

        // Get all the programLevelPracticeList where name is null
        defaultProgramLevelPracticeShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesBySubgroupIdIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where subgroupId equals to DEFAULT_SUBGROUP_ID
        defaultProgramLevelPracticeShouldBeFound("subgroupId.equals=" + DEFAULT_SUBGROUP_ID);

        // Get all the programLevelPracticeList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramLevelPracticeShouldNotBeFound("subgroupId.equals=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesBySubgroupIdIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where subgroupId in DEFAULT_SUBGROUP_ID or UPDATED_SUBGROUP_ID
        defaultProgramLevelPracticeShouldBeFound("subgroupId.in=" + DEFAULT_SUBGROUP_ID + "," + UPDATED_SUBGROUP_ID);

        // Get all the programLevelPracticeList where subgroupId equals to UPDATED_SUBGROUP_ID
        defaultProgramLevelPracticeShouldNotBeFound("subgroupId.in=" + UPDATED_SUBGROUP_ID);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesBySubgroupIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where subgroupId is not null
        defaultProgramLevelPracticeShouldBeFound("subgroupId.specified=true");

        // Get all the programLevelPracticeList where subgroupId is null
        defaultProgramLevelPracticeShouldNotBeFound("subgroupId.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesBySubgroupNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where subgroupName equals to DEFAULT_SUBGROUP_NAME
        defaultProgramLevelPracticeShouldBeFound("subgroupName.equals=" + DEFAULT_SUBGROUP_NAME);

        // Get all the programLevelPracticeList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramLevelPracticeShouldNotBeFound("subgroupName.equals=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesBySubgroupNameIsInShouldWork() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where subgroupName in DEFAULT_SUBGROUP_NAME or UPDATED_SUBGROUP_NAME
        defaultProgramLevelPracticeShouldBeFound("subgroupName.in=" + DEFAULT_SUBGROUP_NAME + "," + UPDATED_SUBGROUP_NAME);

        // Get all the programLevelPracticeList where subgroupName equals to UPDATED_SUBGROUP_NAME
        defaultProgramLevelPracticeShouldNotBeFound("subgroupName.in=" + UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesBySubgroupNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        // Get all the programLevelPracticeList where subgroupName is not null
        defaultProgramLevelPracticeShouldBeFound("subgroupName.specified=true");

        // Get all the programLevelPracticeList where subgroupName is null
        defaultProgramLevelPracticeShouldNotBeFound("subgroupName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramLevelPracticesByProgramLevelIsEqualToSomething() throws Exception {
        // Get already existing entity
        ProgramLevel programLevel = programLevelPractice.getProgramLevel();
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);
        Long programLevelId = programLevel.getId();

        // Get all the programLevelPracticeList where programLevel equals to programLevelId
        defaultProgramLevelPracticeShouldBeFound("programLevelId.equals=" + programLevelId);

        // Get all the programLevelPracticeList where programLevel equals to programLevelId + 1
        defaultProgramLevelPracticeShouldNotBeFound("programLevelId.equals=" + (programLevelId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramLevelPracticeShouldBeFound(String filter) throws Exception {
        restProgramLevelPracticeMockMvc.perform(get("/api/program-level-practices?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programLevelPractice.getId().intValue())))
            .andExpect(jsonPath("$.[*].practiceId").value(hasItem(DEFAULT_PRACTICE_ID)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].subgroupId").value(hasItem(DEFAULT_SUBGROUP_ID)))
            .andExpect(jsonPath("$.[*].subgroupName").value(hasItem(DEFAULT_SUBGROUP_NAME)));

        // Check, that the count call also returns 1
        restProgramLevelPracticeMockMvc.perform(get("/api/program-level-practices/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramLevelPracticeShouldNotBeFound(String filter) throws Exception {
        restProgramLevelPracticeMockMvc.perform(get("/api/program-level-practices?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramLevelPracticeMockMvc.perform(get("/api/program-level-practices/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramLevelPractice() throws Exception {
        // Get the programLevelPractice
        restProgramLevelPracticeMockMvc.perform(get("/api/program-level-practices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramLevelPractice() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        int databaseSizeBeforeUpdate = programLevelPracticeRepository.findAll().size();

        // Update the programLevelPractice
        ProgramLevelPractice updatedProgramLevelPractice = programLevelPracticeRepository.findById(programLevelPractice.getId()).get();
        // Disconnect from session so that the updates on updatedProgramLevelPractice are not directly saved in db
        em.detach(updatedProgramLevelPractice);
        updatedProgramLevelPractice
            .practiceId(UPDATED_PRACTICE_ID)
            .name(UPDATED_NAME)
            .subgroupId(UPDATED_SUBGROUP_ID)
            .subgroupName(UPDATED_SUBGROUP_NAME);
        ProgramLevelPracticeDTO programLevelPracticeDTO = programLevelPracticeMapper.toDto(updatedProgramLevelPractice);

        restProgramLevelPracticeMockMvc.perform(put("/api/program-level-practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPracticeDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramLevelPractice in the database
        List<ProgramLevelPractice> programLevelPracticeList = programLevelPracticeRepository.findAll();
        assertThat(programLevelPracticeList).hasSize(databaseSizeBeforeUpdate);
        ProgramLevelPractice testProgramLevelPractice = programLevelPracticeList.get(programLevelPracticeList.size() - 1);
        assertThat(testProgramLevelPractice.getPracticeId()).isEqualTo(UPDATED_PRACTICE_ID);
        assertThat(testProgramLevelPractice.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProgramLevelPractice.getSubgroupId()).isEqualTo(UPDATED_SUBGROUP_ID);
        assertThat(testProgramLevelPractice.getSubgroupName()).isEqualTo(UPDATED_SUBGROUP_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramLevelPractice() throws Exception {
        int databaseSizeBeforeUpdate = programLevelPracticeRepository.findAll().size();

        // Create the ProgramLevelPractice
        ProgramLevelPracticeDTO programLevelPracticeDTO = programLevelPracticeMapper.toDto(programLevelPractice);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramLevelPracticeMockMvc.perform(put("/api/program-level-practices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programLevelPracticeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramLevelPractice in the database
        List<ProgramLevelPractice> programLevelPracticeList = programLevelPracticeRepository.findAll();
        assertThat(programLevelPracticeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramLevelPractice() throws Exception {
        // Initialize the database
        programLevelPracticeRepository.saveAndFlush(programLevelPractice);

        int databaseSizeBeforeDelete = programLevelPracticeRepository.findAll().size();

        // Delete the programLevelPractice
        restProgramLevelPracticeMockMvc.perform(delete("/api/program-level-practices/{id}", programLevelPractice.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramLevelPractice> programLevelPracticeList = programLevelPracticeRepository.findAll();
        assertThat(programLevelPracticeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelPractice.class);
        ProgramLevelPractice programLevelPractice1 = new ProgramLevelPractice();
        programLevelPractice1.setId(1L);
        ProgramLevelPractice programLevelPractice2 = new ProgramLevelPractice();
        programLevelPractice2.setId(programLevelPractice1.getId());
        assertThat(programLevelPractice1).isEqualTo(programLevelPractice2);
        programLevelPractice2.setId(2L);
        assertThat(programLevelPractice1).isNotEqualTo(programLevelPractice2);
        programLevelPractice1.setId(null);
        assertThat(programLevelPractice1).isNotEqualTo(programLevelPractice2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramLevelPracticeDTO.class);
        ProgramLevelPracticeDTO programLevelPracticeDTO1 = new ProgramLevelPracticeDTO();
        programLevelPracticeDTO1.setId(1L);
        ProgramLevelPracticeDTO programLevelPracticeDTO2 = new ProgramLevelPracticeDTO();
        assertThat(programLevelPracticeDTO1).isNotEqualTo(programLevelPracticeDTO2);
        programLevelPracticeDTO2.setId(programLevelPracticeDTO1.getId());
        assertThat(programLevelPracticeDTO1).isEqualTo(programLevelPracticeDTO2);
        programLevelPracticeDTO2.setId(2L);
        assertThat(programLevelPracticeDTO1).isNotEqualTo(programLevelPracticeDTO2);
        programLevelPracticeDTO1.setId(null);
        assertThat(programLevelPracticeDTO1).isNotEqualTo(programLevelPracticeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programLevelPracticeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programLevelPracticeMapper.fromId(null)).isNull();
    }
}
