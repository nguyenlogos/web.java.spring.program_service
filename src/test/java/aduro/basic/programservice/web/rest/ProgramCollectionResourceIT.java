package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ProgramCollection;
import aduro.basic.programservice.domain.Program;
import aduro.basic.programservice.domain.ProgramCollectionContent;
import aduro.basic.programservice.repository.ProgramCollectionRepository;
import aduro.basic.programservice.service.ProgramCollectionService;
import aduro.basic.programservice.service.dto.ProgramCollectionDTO;
import aduro.basic.programservice.service.mapper.ProgramCollectionMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ProgramCollectionCriteria;
import aduro.basic.programservice.service.ProgramCollectionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ProgramCollectionResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ProgramCollectionResourceIT {

    private static final String DEFAULT_DISPLAY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DISPLAY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LONG_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_LONG_DESCRIPTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ProgramCollectionRepository programCollectionRepository;

    @Autowired
    private ProgramCollectionMapper programCollectionMapper;

    @Autowired
    private ProgramCollectionService programCollectionService;

    @Autowired
    private ProgramCollectionQueryService programCollectionQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProgramCollectionMockMvc;

    private ProgramCollection programCollection;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProgramCollectionResource programCollectionResource = new ProgramCollectionResource(programCollectionService, programCollectionQueryService);
        this.restProgramCollectionMockMvc = MockMvcBuilders.standaloneSetup(programCollectionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCollection createEntity(EntityManager em) {
        ProgramCollection programCollection = new ProgramCollection()
            .displayName(DEFAULT_DISPLAY_NAME)
            .longDescription(DEFAULT_LONG_DESCRIPTION)
            .createdDate(DEFAULT_CREATED_DATE)
            .modifiedDate(DEFAULT_MODIFIED_DATE);
        return programCollection;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProgramCollection createUpdatedEntity(EntityManager em) {
        ProgramCollection programCollection = new ProgramCollection()
            .displayName(UPDATED_DISPLAY_NAME)
            .longDescription(UPDATED_LONG_DESCRIPTION)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        return programCollection;
    }

    @BeforeEach
    public void initTest() {
        programCollection = createEntity(em);
    }

    @Test
    @Transactional
    public void createProgramCollection() throws Exception {
        int databaseSizeBeforeCreate = programCollectionRepository.findAll().size();

        // Create the ProgramCollection
        ProgramCollectionDTO programCollectionDTO = programCollectionMapper.toDto(programCollection);
        restProgramCollectionMockMvc.perform(post("/api/program-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionDTO)))
            .andExpect(status().isCreated());

        // Validate the ProgramCollection in the database
        List<ProgramCollection> programCollectionList = programCollectionRepository.findAll();
        assertThat(programCollectionList).hasSize(databaseSizeBeforeCreate + 1);
        ProgramCollection testProgramCollection = programCollectionList.get(programCollectionList.size() - 1);
        assertThat(testProgramCollection.getDisplayName()).isEqualTo(DEFAULT_DISPLAY_NAME);
        assertThat(testProgramCollection.getLongDescription()).isEqualTo(DEFAULT_LONG_DESCRIPTION);
        assertThat(testProgramCollection.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProgramCollection.getModifiedDate()).isEqualTo(DEFAULT_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void createProgramCollectionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = programCollectionRepository.findAll().size();

        // Create the ProgramCollection with an existing ID
        programCollection.setId(1L);
        ProgramCollectionDTO programCollectionDTO = programCollectionMapper.toDto(programCollection);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProgramCollectionMockMvc.perform(post("/api/program-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCollection in the database
        List<ProgramCollection> programCollectionList = programCollectionRepository.findAll();
        assertThat(programCollectionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDisplayNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = programCollectionRepository.findAll().size();
        // set the field null
        programCollection.setDisplayName(null);

        // Create the ProgramCollection, which fails.
        ProgramCollectionDTO programCollectionDTO = programCollectionMapper.toDto(programCollection);

        restProgramCollectionMockMvc.perform(post("/api/program-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionDTO)))
            .andExpect(status().isBadRequest());

        List<ProgramCollection> programCollectionList = programCollectionRepository.findAll();
        assertThat(programCollectionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllProgramCollections() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList
        restProgramCollectionMockMvc.perform(get("/api/program-collections?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCollection.getId().intValue())))
            .andExpect(jsonPath("$.[*].displayName").value(hasItem(DEFAULT_DISPLAY_NAME.toString())))
            .andExpect(jsonPath("$.[*].longDescription").value(hasItem(DEFAULT_LONG_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));
    }
    
    @Test
    @Transactional
    public void getProgramCollection() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get the programCollection
        restProgramCollectionMockMvc.perform(get("/api/program-collections/{id}", programCollection.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(programCollection.getId().intValue()))
            .andExpect(jsonPath("$.displayName").value(DEFAULT_DISPLAY_NAME.toString()))
            .andExpect(jsonPath("$.longDescription").value(DEFAULT_LONG_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.modifiedDate").value(DEFAULT_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByDisplayNameIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where displayName equals to DEFAULT_DISPLAY_NAME
        defaultProgramCollectionShouldBeFound("displayName.equals=" + DEFAULT_DISPLAY_NAME);

        // Get all the programCollectionList where displayName equals to UPDATED_DISPLAY_NAME
        defaultProgramCollectionShouldNotBeFound("displayName.equals=" + UPDATED_DISPLAY_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByDisplayNameIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where displayName in DEFAULT_DISPLAY_NAME or UPDATED_DISPLAY_NAME
        defaultProgramCollectionShouldBeFound("displayName.in=" + DEFAULT_DISPLAY_NAME + "," + UPDATED_DISPLAY_NAME);

        // Get all the programCollectionList where displayName equals to UPDATED_DISPLAY_NAME
        defaultProgramCollectionShouldNotBeFound("displayName.in=" + UPDATED_DISPLAY_NAME);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByDisplayNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where displayName is not null
        defaultProgramCollectionShouldBeFound("displayName.specified=true");

        // Get all the programCollectionList where displayName is null
        defaultProgramCollectionShouldNotBeFound("displayName.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByLongDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where longDescription equals to DEFAULT_LONG_DESCRIPTION
        defaultProgramCollectionShouldBeFound("longDescription.equals=" + DEFAULT_LONG_DESCRIPTION);

        // Get all the programCollectionList where longDescription equals to UPDATED_LONG_DESCRIPTION
        defaultProgramCollectionShouldNotBeFound("longDescription.equals=" + UPDATED_LONG_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByLongDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where longDescription in DEFAULT_LONG_DESCRIPTION or UPDATED_LONG_DESCRIPTION
        defaultProgramCollectionShouldBeFound("longDescription.in=" + DEFAULT_LONG_DESCRIPTION + "," + UPDATED_LONG_DESCRIPTION);

        // Get all the programCollectionList where longDescription equals to UPDATED_LONG_DESCRIPTION
        defaultProgramCollectionShouldNotBeFound("longDescription.in=" + UPDATED_LONG_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByLongDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where longDescription is not null
        defaultProgramCollectionShouldBeFound("longDescription.specified=true");

        // Get all the programCollectionList where longDescription is null
        defaultProgramCollectionShouldNotBeFound("longDescription.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where createdDate equals to DEFAULT_CREATED_DATE
        defaultProgramCollectionShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the programCollectionList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCollectionShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultProgramCollectionShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the programCollectionList where createdDate equals to UPDATED_CREATED_DATE
        defaultProgramCollectionShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where createdDate is not null
        defaultProgramCollectionShouldBeFound("createdDate.specified=true");

        // Get all the programCollectionList where createdDate is null
        defaultProgramCollectionShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where modifiedDate equals to DEFAULT_MODIFIED_DATE
        defaultProgramCollectionShouldBeFound("modifiedDate.equals=" + DEFAULT_MODIFIED_DATE);

        // Get all the programCollectionList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCollectionShouldNotBeFound("modifiedDate.equals=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where modifiedDate in DEFAULT_MODIFIED_DATE or UPDATED_MODIFIED_DATE
        defaultProgramCollectionShouldBeFound("modifiedDate.in=" + DEFAULT_MODIFIED_DATE + "," + UPDATED_MODIFIED_DATE);

        // Get all the programCollectionList where modifiedDate equals to UPDATED_MODIFIED_DATE
        defaultProgramCollectionShouldNotBeFound("modifiedDate.in=" + UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        // Get all the programCollectionList where modifiedDate is not null
        defaultProgramCollectionShouldBeFound("modifiedDate.specified=true");

        // Get all the programCollectionList where modifiedDate is null
        defaultProgramCollectionShouldNotBeFound("modifiedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllProgramCollectionsByProgramIsEqualToSomething() throws Exception {
        // Initialize the database
        Program program = ProgramResourceIT.createEntity(em);
        em.persist(program);
        em.flush();
        programCollection.setProgram(program);
        programCollectionRepository.saveAndFlush(programCollection);
        Long programId = program.getId();

        // Get all the programCollectionList where program equals to programId
        defaultProgramCollectionShouldBeFound("programId.equals=" + programId);

        // Get all the programCollectionList where program equals to programId + 1
        defaultProgramCollectionShouldNotBeFound("programId.equals=" + (programId + 1));
    }


    @Test
    @Transactional
    public void getAllProgramCollectionsByProgramCollectionContentIsEqualToSomething() throws Exception {
        // Initialize the database
        ProgramCollectionContent programCollectionContent = ProgramCollectionContentResourceIT.createEntity(em);
        em.persist(programCollectionContent);
        em.flush();
        programCollection.addProgramCollectionContent(programCollectionContent);
        programCollectionRepository.saveAndFlush(programCollection);
        Long programCollectionContentId = programCollectionContent.getId();

        // Get all the programCollectionList where programCollectionContent equals to programCollectionContentId
        defaultProgramCollectionShouldBeFound("programCollectionContentId.equals=" + programCollectionContentId);

        // Get all the programCollectionList where programCollectionContent equals to programCollectionContentId + 1
        defaultProgramCollectionShouldNotBeFound("programCollectionContentId.equals=" + (programCollectionContentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProgramCollectionShouldBeFound(String filter) throws Exception {
        restProgramCollectionMockMvc.perform(get("/api/program-collections?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(programCollection.getId().intValue())))
            .andExpect(jsonPath("$.[*].displayName").value(hasItem(DEFAULT_DISPLAY_NAME)))
            .andExpect(jsonPath("$.[*].longDescription").value(hasItem(DEFAULT_LONG_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].modifiedDate").value(hasItem(DEFAULT_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restProgramCollectionMockMvc.perform(get("/api/program-collections/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProgramCollectionShouldNotBeFound(String filter) throws Exception {
        restProgramCollectionMockMvc.perform(get("/api/program-collections?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProgramCollectionMockMvc.perform(get("/api/program-collections/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingProgramCollection() throws Exception {
        // Get the programCollection
        restProgramCollectionMockMvc.perform(get("/api/program-collections/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProgramCollection() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        int databaseSizeBeforeUpdate = programCollectionRepository.findAll().size();

        // Update the programCollection
        ProgramCollection updatedProgramCollection = programCollectionRepository.findById(programCollection.getId()).get();
        // Disconnect from session so that the updates on updatedProgramCollection are not directly saved in db
        em.detach(updatedProgramCollection);
        updatedProgramCollection
            .displayName(UPDATED_DISPLAY_NAME)
            .longDescription(UPDATED_LONG_DESCRIPTION)
            .createdDate(UPDATED_CREATED_DATE)
            .modifiedDate(UPDATED_MODIFIED_DATE);
        ProgramCollectionDTO programCollectionDTO = programCollectionMapper.toDto(updatedProgramCollection);

        restProgramCollectionMockMvc.perform(put("/api/program-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionDTO)))
            .andExpect(status().isOk());

        // Validate the ProgramCollection in the database
        List<ProgramCollection> programCollectionList = programCollectionRepository.findAll();
        assertThat(programCollectionList).hasSize(databaseSizeBeforeUpdate);
        ProgramCollection testProgramCollection = programCollectionList.get(programCollectionList.size() - 1);
        assertThat(testProgramCollection.getDisplayName()).isEqualTo(UPDATED_DISPLAY_NAME);
        assertThat(testProgramCollection.getLongDescription()).isEqualTo(UPDATED_LONG_DESCRIPTION);
        assertThat(testProgramCollection.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProgramCollection.getModifiedDate()).isEqualTo(UPDATED_MODIFIED_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingProgramCollection() throws Exception {
        int databaseSizeBeforeUpdate = programCollectionRepository.findAll().size();

        // Create the ProgramCollection
        ProgramCollectionDTO programCollectionDTO = programCollectionMapper.toDto(programCollection);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProgramCollectionMockMvc.perform(put("/api/program-collections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(programCollectionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProgramCollection in the database
        List<ProgramCollection> programCollectionList = programCollectionRepository.findAll();
        assertThat(programCollectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProgramCollection() throws Exception {
        // Initialize the database
        programCollectionRepository.saveAndFlush(programCollection);

        int databaseSizeBeforeDelete = programCollectionRepository.findAll().size();

        // Delete the programCollection
        restProgramCollectionMockMvc.perform(delete("/api/program-collections/{id}", programCollection.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ProgramCollection> programCollectionList = programCollectionRepository.findAll();
        assertThat(programCollectionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCollection.class);
        ProgramCollection programCollection1 = new ProgramCollection();
        programCollection1.setId(1L);
        ProgramCollection programCollection2 = new ProgramCollection();
        programCollection2.setId(programCollection1.getId());
        assertThat(programCollection1).isEqualTo(programCollection2);
        programCollection2.setId(2L);
        assertThat(programCollection1).isNotEqualTo(programCollection2);
        programCollection1.setId(null);
        assertThat(programCollection1).isNotEqualTo(programCollection2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramCollectionDTO.class);
        ProgramCollectionDTO programCollectionDTO1 = new ProgramCollectionDTO();
        programCollectionDTO1.setId(1L);
        ProgramCollectionDTO programCollectionDTO2 = new ProgramCollectionDTO();
        assertThat(programCollectionDTO1).isNotEqualTo(programCollectionDTO2);
        programCollectionDTO2.setId(programCollectionDTO1.getId());
        assertThat(programCollectionDTO1).isEqualTo(programCollectionDTO2);
        programCollectionDTO2.setId(2L);
        assertThat(programCollectionDTO1).isNotEqualTo(programCollectionDTO2);
        programCollectionDTO1.setId(null);
        assertThat(programCollectionDTO1).isNotEqualTo(programCollectionDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(programCollectionMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(programCollectionMapper.fromId(null)).isNull();
    }
}
