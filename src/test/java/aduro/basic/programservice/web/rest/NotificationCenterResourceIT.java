package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.NotificationCenter;
import aduro.basic.programservice.repository.NotificationCenterRepository;
import aduro.basic.programservice.service.NotificationCenterService;
import aduro.basic.programservice.service.dto.NotificationCenterDTO;
import aduro.basic.programservice.service.mapper.NotificationCenterMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.NotificationCenterCriteria;
import aduro.basic.programservice.service.NotificationCenterQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import aduro.basic.programservice.domain.enumeration.NotificationType;
/**
 * Integration tests for the {@Link NotificationCenterResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class NotificationCenterResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_PARTICIPANT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARTICIPANT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_EXECUTE_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_EXECUTE_STATUS = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_ATTEMPT_COUNT = 1;
    private static final Integer UPDATED_ATTEMPT_COUNT = 2;

    private static final Instant DEFAULT_LAST_ATTEMPT_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_ATTEMPT_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ERROR_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_ERROR_MESSAGE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final NotificationType DEFAULT_NOTIFICATION_TYPE = NotificationType.WELLMETRIC_SCREENING_RESULT;
    private static final NotificationType UPDATED_NOTIFICATION_TYPE = NotificationType.WELLMETRIC_SCREENING_RESULT;

    private static final String DEFAULT_EVENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_EVENT_ID = "BBBBBBBBBB";

    private static final Boolean DEFAULT_IS_READY = false;
    private static final Boolean UPDATED_IS_READY = true;

    private static final String DEFAULT_RECEIVER_NAME = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVER_NAME = "BBBBBBBBBB";

    @Autowired
    private NotificationCenterRepository notificationCenterRepository;

    @Autowired
    private NotificationCenterMapper notificationCenterMapper;

    @Autowired
    private NotificationCenterService notificationCenterService;

    @Autowired
    private NotificationCenterQueryService notificationCenterQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNotificationCenterMockMvc;

    private NotificationCenter notificationCenter;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NotificationCenterResource notificationCenterResource = new NotificationCenterResource(notificationCenterService, notificationCenterQueryService);
        this.restNotificationCenterMockMvc = MockMvcBuilders.standaloneSetup(notificationCenterResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NotificationCenter createEntity(EntityManager em) {
        NotificationCenter notificationCenter = new NotificationCenter()
            .title(DEFAULT_TITLE)
            .participantId(DEFAULT_PARTICIPANT_ID)
            .executeStatus(DEFAULT_EXECUTE_STATUS)
            .createdAt(DEFAULT_CREATED_AT)
            .attemptCount(DEFAULT_ATTEMPT_COUNT)
            .lastAttemptTime(DEFAULT_LAST_ATTEMPT_TIME)
            .errorMessage(DEFAULT_ERROR_MESSAGE)
            .content(DEFAULT_CONTENT)
            .notificationType(DEFAULT_NOTIFICATION_TYPE)
            .eventId(DEFAULT_EVENT_ID)
            .isReady(DEFAULT_IS_READY)
            .receiverName(DEFAULT_RECEIVER_NAME);
        return notificationCenter;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NotificationCenter createUpdatedEntity(EntityManager em) {
        NotificationCenter notificationCenter = new NotificationCenter()
            .title(UPDATED_TITLE)
            .participantId(UPDATED_PARTICIPANT_ID)
            .executeStatus(UPDATED_EXECUTE_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .attemptCount(UPDATED_ATTEMPT_COUNT)
            .lastAttemptTime(UPDATED_LAST_ATTEMPT_TIME)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .content(UPDATED_CONTENT)
            .notificationType(UPDATED_NOTIFICATION_TYPE)
            .eventId(UPDATED_EVENT_ID)
            .isReady(UPDATED_IS_READY)
            .receiverName(UPDATED_RECEIVER_NAME);
        return notificationCenter;
    }

    @BeforeEach
    public void initTest() {
        notificationCenter = createEntity(em);
    }

    @Test
    @Transactional
    public void createNotificationCenter() throws Exception {
        int databaseSizeBeforeCreate = notificationCenterRepository.findAll().size();

        // Create the NotificationCenter
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);
        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isCreated());

        // Validate the NotificationCenter in the database
        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeCreate + 1);
        NotificationCenter testNotificationCenter = notificationCenterList.get(notificationCenterList.size() - 1);
        assertThat(testNotificationCenter.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testNotificationCenter.getParticipantId()).isEqualTo(DEFAULT_PARTICIPANT_ID);
        assertThat(testNotificationCenter.getExecuteStatus()).isEqualTo(DEFAULT_EXECUTE_STATUS);
        assertThat(testNotificationCenter.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testNotificationCenter.getAttemptCount()).isEqualTo(DEFAULT_ATTEMPT_COUNT);
        assertThat(testNotificationCenter.getLastAttemptTime()).isEqualTo(DEFAULT_LAST_ATTEMPT_TIME);
        assertThat(testNotificationCenter.getErrorMessage()).isEqualTo(DEFAULT_ERROR_MESSAGE);
        assertThat(testNotificationCenter.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testNotificationCenter.getNotificationType()).isEqualTo(DEFAULT_NOTIFICATION_TYPE);
        assertThat(testNotificationCenter.getEventId()).isEqualTo(DEFAULT_EVENT_ID);
        assertThat(testNotificationCenter.isIsReady()).isEqualTo(DEFAULT_IS_READY);
        assertThat(testNotificationCenter.getReceiverName()).isEqualTo(DEFAULT_RECEIVER_NAME);
    }

    @Test
    @Transactional
    public void createNotificationCenterWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notificationCenterRepository.findAll().size();

        // Create the NotificationCenter with an existing ID
        notificationCenter.setId(1L);
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NotificationCenter in the database
        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setTitle(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkParticipantIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setParticipantId(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkExecuteStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setExecuteStatus(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreatedAtIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setCreatedAt(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLastAttemptTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setLastAttemptTime(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContentIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setContent(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNotificationTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setNotificationType(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEventIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setEventId(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkReceiverNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificationCenterRepository.findAll().size();
        // set the field null
        notificationCenter.setReceiverName(null);

        // Create the NotificationCenter, which fails.
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        restNotificationCenterMockMvc.perform(post("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNotificationCenters() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList
        restNotificationCenterMockMvc.perform(get("/api/notification-centers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notificationCenter.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID.toString())))
            .andExpect(jsonPath("$.[*].executeStatus").value(hasItem(DEFAULT_EXECUTE_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].attemptCount").value(hasItem(DEFAULT_ATTEMPT_COUNT)))
            .andExpect(jsonPath("$.[*].lastAttemptTime").value(hasItem(DEFAULT_LAST_ATTEMPT_TIME.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID.toString())))
            .andExpect(jsonPath("$.[*].isReady").value(hasItem(DEFAULT_IS_READY.booleanValue())))
            .andExpect(jsonPath("$.[*].receiverName").value(hasItem(DEFAULT_RECEIVER_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getNotificationCenter() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get the notificationCenter
        restNotificationCenterMockMvc.perform(get("/api/notification-centers/{id}", notificationCenter.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(notificationCenter.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.participantId").value(DEFAULT_PARTICIPANT_ID.toString()))
            .andExpect(jsonPath("$.executeStatus").value(DEFAULT_EXECUTE_STATUS.toString()))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.attemptCount").value(DEFAULT_ATTEMPT_COUNT))
            .andExpect(jsonPath("$.lastAttemptTime").value(DEFAULT_LAST_ATTEMPT_TIME.toString()))
            .andExpect(jsonPath("$.errorMessage").value(DEFAULT_ERROR_MESSAGE.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.notificationType").value(DEFAULT_NOTIFICATION_TYPE.toString()))
            .andExpect(jsonPath("$.eventId").value(DEFAULT_EVENT_ID.toString()))
            .andExpect(jsonPath("$.isReady").value(DEFAULT_IS_READY.booleanValue()))
            .andExpect(jsonPath("$.receiverName").value(DEFAULT_RECEIVER_NAME.toString()));
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where title equals to DEFAULT_TITLE
        defaultNotificationCenterShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the notificationCenterList where title equals to UPDATED_TITLE
        defaultNotificationCenterShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultNotificationCenterShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the notificationCenterList where title equals to UPDATED_TITLE
        defaultNotificationCenterShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where title is not null
        defaultNotificationCenterShouldBeFound("title.specified=true");

        // Get all the notificationCenterList where title is null
        defaultNotificationCenterShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByParticipantIdIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where participantId equals to DEFAULT_PARTICIPANT_ID
        defaultNotificationCenterShouldBeFound("participantId.equals=" + DEFAULT_PARTICIPANT_ID);

        // Get all the notificationCenterList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultNotificationCenterShouldNotBeFound("participantId.equals=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByParticipantIdIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where participantId in DEFAULT_PARTICIPANT_ID or UPDATED_PARTICIPANT_ID
        defaultNotificationCenterShouldBeFound("participantId.in=" + DEFAULT_PARTICIPANT_ID + "," + UPDATED_PARTICIPANT_ID);

        // Get all the notificationCenterList where participantId equals to UPDATED_PARTICIPANT_ID
        defaultNotificationCenterShouldNotBeFound("participantId.in=" + UPDATED_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByParticipantIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where participantId is not null
        defaultNotificationCenterShouldBeFound("participantId.specified=true");

        // Get all the notificationCenterList where participantId is null
        defaultNotificationCenterShouldNotBeFound("participantId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByExecuteStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where executeStatus equals to DEFAULT_EXECUTE_STATUS
        defaultNotificationCenterShouldBeFound("executeStatus.equals=" + DEFAULT_EXECUTE_STATUS);

        // Get all the notificationCenterList where executeStatus equals to UPDATED_EXECUTE_STATUS
        defaultNotificationCenterShouldNotBeFound("executeStatus.equals=" + UPDATED_EXECUTE_STATUS);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByExecuteStatusIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where executeStatus in DEFAULT_EXECUTE_STATUS or UPDATED_EXECUTE_STATUS
        defaultNotificationCenterShouldBeFound("executeStatus.in=" + DEFAULT_EXECUTE_STATUS + "," + UPDATED_EXECUTE_STATUS);

        // Get all the notificationCenterList where executeStatus equals to UPDATED_EXECUTE_STATUS
        defaultNotificationCenterShouldNotBeFound("executeStatus.in=" + UPDATED_EXECUTE_STATUS);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByExecuteStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where executeStatus is not null
        defaultNotificationCenterShouldBeFound("executeStatus.specified=true");

        // Get all the notificationCenterList where executeStatus is null
        defaultNotificationCenterShouldNotBeFound("executeStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where createdAt equals to DEFAULT_CREATED_AT
        defaultNotificationCenterShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the notificationCenterList where createdAt equals to UPDATED_CREATED_AT
        defaultNotificationCenterShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultNotificationCenterShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the notificationCenterList where createdAt equals to UPDATED_CREATED_AT
        defaultNotificationCenterShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where createdAt is not null
        defaultNotificationCenterShouldBeFound("createdAt.specified=true");

        // Get all the notificationCenterList where createdAt is null
        defaultNotificationCenterShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByAttemptCountIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where attemptCount equals to DEFAULT_ATTEMPT_COUNT
        defaultNotificationCenterShouldBeFound("attemptCount.equals=" + DEFAULT_ATTEMPT_COUNT);

        // Get all the notificationCenterList where attemptCount equals to UPDATED_ATTEMPT_COUNT
        defaultNotificationCenterShouldNotBeFound("attemptCount.equals=" + UPDATED_ATTEMPT_COUNT);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByAttemptCountIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where attemptCount in DEFAULT_ATTEMPT_COUNT or UPDATED_ATTEMPT_COUNT
        defaultNotificationCenterShouldBeFound("attemptCount.in=" + DEFAULT_ATTEMPT_COUNT + "," + UPDATED_ATTEMPT_COUNT);

        // Get all the notificationCenterList where attemptCount equals to UPDATED_ATTEMPT_COUNT
        defaultNotificationCenterShouldNotBeFound("attemptCount.in=" + UPDATED_ATTEMPT_COUNT);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByAttemptCountIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where attemptCount is not null
        defaultNotificationCenterShouldBeFound("attemptCount.specified=true");

        // Get all the notificationCenterList where attemptCount is null
        defaultNotificationCenterShouldNotBeFound("attemptCount.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByAttemptCountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where attemptCount greater than or equals to DEFAULT_ATTEMPT_COUNT
        defaultNotificationCenterShouldBeFound("attemptCount.greaterOrEqualThan=" + DEFAULT_ATTEMPT_COUNT);

        // Get all the notificationCenterList where attemptCount greater than or equals to UPDATED_ATTEMPT_COUNT
        defaultNotificationCenterShouldNotBeFound("attemptCount.greaterOrEqualThan=" + UPDATED_ATTEMPT_COUNT);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByAttemptCountIsLessThanSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where attemptCount less than or equals to DEFAULT_ATTEMPT_COUNT
        defaultNotificationCenterShouldNotBeFound("attemptCount.lessThan=" + DEFAULT_ATTEMPT_COUNT);

        // Get all the notificationCenterList where attemptCount less than or equals to UPDATED_ATTEMPT_COUNT
        defaultNotificationCenterShouldBeFound("attemptCount.lessThan=" + UPDATED_ATTEMPT_COUNT);
    }


    @Test
    @Transactional
    public void getAllNotificationCentersByLastAttemptTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where lastAttemptTime equals to DEFAULT_LAST_ATTEMPT_TIME
        defaultNotificationCenterShouldBeFound("lastAttemptTime.equals=" + DEFAULT_LAST_ATTEMPT_TIME);

        // Get all the notificationCenterList where lastAttemptTime equals to UPDATED_LAST_ATTEMPT_TIME
        defaultNotificationCenterShouldNotBeFound("lastAttemptTime.equals=" + UPDATED_LAST_ATTEMPT_TIME);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByLastAttemptTimeIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where lastAttemptTime in DEFAULT_LAST_ATTEMPT_TIME or UPDATED_LAST_ATTEMPT_TIME
        defaultNotificationCenterShouldBeFound("lastAttemptTime.in=" + DEFAULT_LAST_ATTEMPT_TIME + "," + UPDATED_LAST_ATTEMPT_TIME);

        // Get all the notificationCenterList where lastAttemptTime equals to UPDATED_LAST_ATTEMPT_TIME
        defaultNotificationCenterShouldNotBeFound("lastAttemptTime.in=" + UPDATED_LAST_ATTEMPT_TIME);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByLastAttemptTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where lastAttemptTime is not null
        defaultNotificationCenterShouldBeFound("lastAttemptTime.specified=true");

        // Get all the notificationCenterList where lastAttemptTime is null
        defaultNotificationCenterShouldNotBeFound("lastAttemptTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByErrorMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where errorMessage equals to DEFAULT_ERROR_MESSAGE
        defaultNotificationCenterShouldBeFound("errorMessage.equals=" + DEFAULT_ERROR_MESSAGE);

        // Get all the notificationCenterList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultNotificationCenterShouldNotBeFound("errorMessage.equals=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByErrorMessageIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where errorMessage in DEFAULT_ERROR_MESSAGE or UPDATED_ERROR_MESSAGE
        defaultNotificationCenterShouldBeFound("errorMessage.in=" + DEFAULT_ERROR_MESSAGE + "," + UPDATED_ERROR_MESSAGE);

        // Get all the notificationCenterList where errorMessage equals to UPDATED_ERROR_MESSAGE
        defaultNotificationCenterShouldNotBeFound("errorMessage.in=" + UPDATED_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByErrorMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where errorMessage is not null
        defaultNotificationCenterShouldBeFound("errorMessage.specified=true");

        // Get all the notificationCenterList where errorMessage is null
        defaultNotificationCenterShouldNotBeFound("errorMessage.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByContentIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where content equals to DEFAULT_CONTENT
        defaultNotificationCenterShouldBeFound("content.equals=" + DEFAULT_CONTENT);

        // Get all the notificationCenterList where content equals to UPDATED_CONTENT
        defaultNotificationCenterShouldNotBeFound("content.equals=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByContentIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where content in DEFAULT_CONTENT or UPDATED_CONTENT
        defaultNotificationCenterShouldBeFound("content.in=" + DEFAULT_CONTENT + "," + UPDATED_CONTENT);

        // Get all the notificationCenterList where content equals to UPDATED_CONTENT
        defaultNotificationCenterShouldNotBeFound("content.in=" + UPDATED_CONTENT);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByContentIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where content is not null
        defaultNotificationCenterShouldBeFound("content.specified=true");

        // Get all the notificationCenterList where content is null
        defaultNotificationCenterShouldNotBeFound("content.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByNotificationTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where notificationType equals to DEFAULT_NOTIFICATION_TYPE
        defaultNotificationCenterShouldBeFound("notificationType.equals=" + DEFAULT_NOTIFICATION_TYPE);

        // Get all the notificationCenterList where notificationType equals to UPDATED_NOTIFICATION_TYPE
        defaultNotificationCenterShouldNotBeFound("notificationType.equals=" + UPDATED_NOTIFICATION_TYPE);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByNotificationTypeIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where notificationType in DEFAULT_NOTIFICATION_TYPE or UPDATED_NOTIFICATION_TYPE
        defaultNotificationCenterShouldBeFound("notificationType.in=" + DEFAULT_NOTIFICATION_TYPE + "," + UPDATED_NOTIFICATION_TYPE);

        // Get all the notificationCenterList where notificationType equals to UPDATED_NOTIFICATION_TYPE
        defaultNotificationCenterShouldNotBeFound("notificationType.in=" + UPDATED_NOTIFICATION_TYPE);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByNotificationTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where notificationType is not null
        defaultNotificationCenterShouldBeFound("notificationType.specified=true");

        // Get all the notificationCenterList where notificationType is null
        defaultNotificationCenterShouldNotBeFound("notificationType.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByEventIdIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where eventId equals to DEFAULT_EVENT_ID
        defaultNotificationCenterShouldBeFound("eventId.equals=" + DEFAULT_EVENT_ID);

        // Get all the notificationCenterList where eventId equals to UPDATED_EVENT_ID
        defaultNotificationCenterShouldNotBeFound("eventId.equals=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByEventIdIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where eventId in DEFAULT_EVENT_ID or UPDATED_EVENT_ID
        defaultNotificationCenterShouldBeFound("eventId.in=" + DEFAULT_EVENT_ID + "," + UPDATED_EVENT_ID);

        // Get all the notificationCenterList where eventId equals to UPDATED_EVENT_ID
        defaultNotificationCenterShouldNotBeFound("eventId.in=" + UPDATED_EVENT_ID);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByEventIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where eventId is not null
        defaultNotificationCenterShouldBeFound("eventId.specified=true");

        // Get all the notificationCenterList where eventId is null
        defaultNotificationCenterShouldNotBeFound("eventId.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByIsReadyIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where isReady equals to DEFAULT_IS_READY
        defaultNotificationCenterShouldBeFound("isReady.equals=" + DEFAULT_IS_READY);

        // Get all the notificationCenterList where isReady equals to UPDATED_IS_READY
        defaultNotificationCenterShouldNotBeFound("isReady.equals=" + UPDATED_IS_READY);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByIsReadyIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where isReady in DEFAULT_IS_READY or UPDATED_IS_READY
        defaultNotificationCenterShouldBeFound("isReady.in=" + DEFAULT_IS_READY + "," + UPDATED_IS_READY);

        // Get all the notificationCenterList where isReady equals to UPDATED_IS_READY
        defaultNotificationCenterShouldNotBeFound("isReady.in=" + UPDATED_IS_READY);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByIsReadyIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where isReady is not null
        defaultNotificationCenterShouldBeFound("isReady.specified=true");

        // Get all the notificationCenterList where isReady is null
        defaultNotificationCenterShouldNotBeFound("isReady.specified=false");
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByReceiverNameIsEqualToSomething() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where receiverName equals to DEFAULT_RECEIVER_NAME
        defaultNotificationCenterShouldBeFound("receiverName.equals=" + DEFAULT_RECEIVER_NAME);

        // Get all the notificationCenterList where receiverName equals to UPDATED_RECEIVER_NAME
        defaultNotificationCenterShouldNotBeFound("receiverName.equals=" + UPDATED_RECEIVER_NAME);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByReceiverNameIsInShouldWork() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where receiverName in DEFAULT_RECEIVER_NAME or UPDATED_RECEIVER_NAME
        defaultNotificationCenterShouldBeFound("receiverName.in=" + DEFAULT_RECEIVER_NAME + "," + UPDATED_RECEIVER_NAME);

        // Get all the notificationCenterList where receiverName equals to UPDATED_RECEIVER_NAME
        defaultNotificationCenterShouldNotBeFound("receiverName.in=" + UPDATED_RECEIVER_NAME);
    }

    @Test
    @Transactional
    public void getAllNotificationCentersByReceiverNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        // Get all the notificationCenterList where receiverName is not null
        defaultNotificationCenterShouldBeFound("receiverName.specified=true");

        // Get all the notificationCenterList where receiverName is null
        defaultNotificationCenterShouldNotBeFound("receiverName.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNotificationCenterShouldBeFound(String filter) throws Exception {
        restNotificationCenterMockMvc.perform(get("/api/notification-centers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notificationCenter.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].participantId").value(hasItem(DEFAULT_PARTICIPANT_ID)))
            .andExpect(jsonPath("$.[*].executeStatus").value(hasItem(DEFAULT_EXECUTE_STATUS)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].attemptCount").value(hasItem(DEFAULT_ATTEMPT_COUNT)))
            .andExpect(jsonPath("$.[*].lastAttemptTime").value(hasItem(DEFAULT_LAST_ATTEMPT_TIME.toString())))
            .andExpect(jsonPath("$.[*].errorMessage").value(hasItem(DEFAULT_ERROR_MESSAGE)))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT)))
            .andExpect(jsonPath("$.[*].notificationType").value(hasItem(DEFAULT_NOTIFICATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].eventId").value(hasItem(DEFAULT_EVENT_ID)))
            .andExpect(jsonPath("$.[*].isReady").value(hasItem(DEFAULT_IS_READY.booleanValue())))
            .andExpect(jsonPath("$.[*].receiverName").value(hasItem(DEFAULT_RECEIVER_NAME)));

        // Check, that the count call also returns 1
        restNotificationCenterMockMvc.perform(get("/api/notification-centers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNotificationCenterShouldNotBeFound(String filter) throws Exception {
        restNotificationCenterMockMvc.perform(get("/api/notification-centers?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNotificationCenterMockMvc.perform(get("/api/notification-centers/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNotificationCenter() throws Exception {
        // Get the notificationCenter
        restNotificationCenterMockMvc.perform(get("/api/notification-centers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNotificationCenter() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        int databaseSizeBeforeUpdate = notificationCenterRepository.findAll().size();

        // Update the notificationCenter
        NotificationCenter updatedNotificationCenter = notificationCenterRepository.findById(notificationCenter.getId()).get();
        // Disconnect from session so that the updates on updatedNotificationCenter are not directly saved in db
        em.detach(updatedNotificationCenter);
        updatedNotificationCenter
            .title(UPDATED_TITLE)
            .participantId(UPDATED_PARTICIPANT_ID)
            .executeStatus(UPDATED_EXECUTE_STATUS)
            .createdAt(UPDATED_CREATED_AT)
            .attemptCount(UPDATED_ATTEMPT_COUNT)
            .lastAttemptTime(UPDATED_LAST_ATTEMPT_TIME)
            .errorMessage(UPDATED_ERROR_MESSAGE)
            .content(UPDATED_CONTENT)
            .notificationType(UPDATED_NOTIFICATION_TYPE)
            .eventId(UPDATED_EVENT_ID)
            .isReady(UPDATED_IS_READY)
            .receiverName(UPDATED_RECEIVER_NAME);
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(updatedNotificationCenter);

        restNotificationCenterMockMvc.perform(put("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isOk());

        // Validate the NotificationCenter in the database
        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeUpdate);
        NotificationCenter testNotificationCenter = notificationCenterList.get(notificationCenterList.size() - 1);
        assertThat(testNotificationCenter.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testNotificationCenter.getParticipantId()).isEqualTo(UPDATED_PARTICIPANT_ID);
        assertThat(testNotificationCenter.getExecuteStatus()).isEqualTo(UPDATED_EXECUTE_STATUS);
        assertThat(testNotificationCenter.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testNotificationCenter.getAttemptCount()).isEqualTo(UPDATED_ATTEMPT_COUNT);
        assertThat(testNotificationCenter.getLastAttemptTime()).isEqualTo(UPDATED_LAST_ATTEMPT_TIME);
        assertThat(testNotificationCenter.getErrorMessage()).isEqualTo(UPDATED_ERROR_MESSAGE);
        assertThat(testNotificationCenter.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testNotificationCenter.getNotificationType()).isEqualTo(UPDATED_NOTIFICATION_TYPE);
        assertThat(testNotificationCenter.getEventId()).isEqualTo(UPDATED_EVENT_ID);
        assertThat(testNotificationCenter.isIsReady()).isEqualTo(UPDATED_IS_READY);
        assertThat(testNotificationCenter.getReceiverName()).isEqualTo(UPDATED_RECEIVER_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingNotificationCenter() throws Exception {
        int databaseSizeBeforeUpdate = notificationCenterRepository.findAll().size();

        // Create the NotificationCenter
        NotificationCenterDTO notificationCenterDTO = notificationCenterMapper.toDto(notificationCenter);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNotificationCenterMockMvc.perform(put("/api/notification-centers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notificationCenterDTO)))
            .andExpect(status().isBadRequest());

        // Validate the NotificationCenter in the database
        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNotificationCenter() throws Exception {
        // Initialize the database
        notificationCenterRepository.saveAndFlush(notificationCenter);

        int databaseSizeBeforeDelete = notificationCenterRepository.findAll().size();

        // Delete the notificationCenter
        restNotificationCenterMockMvc.perform(delete("/api/notification-centers/{id}", notificationCenter.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<NotificationCenter> notificationCenterList = notificationCenterRepository.findAll();
        assertThat(notificationCenterList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NotificationCenter.class);
        NotificationCenter notificationCenter1 = new NotificationCenter();
        notificationCenter1.setId(1L);
        NotificationCenter notificationCenter2 = new NotificationCenter();
        notificationCenter2.setId(notificationCenter1.getId());
        assertThat(notificationCenter1).isEqualTo(notificationCenter2);
        notificationCenter2.setId(2L);
        assertThat(notificationCenter1).isNotEqualTo(notificationCenter2);
        notificationCenter1.setId(null);
        assertThat(notificationCenter1).isNotEqualTo(notificationCenter2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NotificationCenterDTO.class);
        NotificationCenterDTO notificationCenterDTO1 = new NotificationCenterDTO();
        notificationCenterDTO1.setId(1L);
        NotificationCenterDTO notificationCenterDTO2 = new NotificationCenterDTO();
        assertThat(notificationCenterDTO1).isNotEqualTo(notificationCenterDTO2);
        notificationCenterDTO2.setId(notificationCenterDTO1.getId());
        assertThat(notificationCenterDTO1).isEqualTo(notificationCenterDTO2);
        notificationCenterDTO2.setId(2L);
        assertThat(notificationCenterDTO1).isNotEqualTo(notificationCenterDTO2);
        notificationCenterDTO1.setId(null);
        assertThat(notificationCenterDTO1).isNotEqualTo(notificationCenterDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(notificationCenterMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(notificationCenterMapper.fromId(null)).isNull();
    }
}
