package aduro.basic.programservice.web.rest;

import aduro.basic.programservice.ProgramserviceApp;
import aduro.basic.programservice.domain.ClientSetting;
import aduro.basic.programservice.repository.ClientSettingRepository;
import aduro.basic.programservice.service.ClientSettingService;
import aduro.basic.programservice.service.dto.ClientSettingDTO;
import aduro.basic.programservice.service.mapper.ClientSettingMapper;
import aduro.basic.programservice.web.rest.errors.ExceptionTranslator;
import aduro.basic.programservice.service.dto.ClientSettingCriteria;
import aduro.basic.programservice.service.ClientSettingQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static aduro.basic.programservice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ClientSettingResource} REST controller.
 */
@SpringBootTest(classes = ProgramserviceApp.class)
public class ClientSettingResourceIT {

    private static final String DEFAULT_CLIENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CLIENT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_CLIENT_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_TANGO_ACCOUNT_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_TANGO_ACCOUNT_IDENTIFIER = "BBBBBBBBBB";

    private static final String DEFAULT_TANGO_CUSTOMER_IDENTIFIER = "AAAAAAAAAA";
    private static final String UPDATED_TANGO_CUSTOMER_IDENTIFIER = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TANGO_CURRENT_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_TANGO_CURRENT_BALANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_TANGO_THRESHOLD_BALANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_TANGO_THRESHOLD_BALANCE = new BigDecimal(2);

    private static final String DEFAULT_TANGO_CREDIT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_TANGO_CREDIT_TOKEN = "BBBBBBBBBB";

    private static final Instant DEFAULT_UPDATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_UPDATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_UPDATED_BY = "BBBBBBBBBB";

    @Autowired
    private ClientSettingRepository clientSettingRepository;

    @Autowired
    private ClientSettingMapper clientSettingMapper;

    @Autowired
    private ClientSettingService clientSettingService;

    @Autowired
    private ClientSettingQueryService clientSettingQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restClientSettingMockMvc;

    private ClientSetting clientSetting;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ClientSettingResource clientSettingResource = new ClientSettingResource(clientSettingService, clientSettingQueryService);
        this.restClientSettingMockMvc = MockMvcBuilders.standaloneSetup(clientSettingResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientSetting createEntity(EntityManager em) {
        ClientSetting clientSetting = new ClientSetting()
            .clientId(DEFAULT_CLIENT_ID)
            .clientName(DEFAULT_CLIENT_NAME)
            .clientEmail(DEFAULT_CLIENT_EMAIL)
            .tangoAccountIdentifier(DEFAULT_TANGO_ACCOUNT_IDENTIFIER)
            .tangoCustomerIdentifier(DEFAULT_TANGO_CUSTOMER_IDENTIFIER)
            .tangoCurrentBalance(DEFAULT_TANGO_CURRENT_BALANCE)
            .tangoThresholdBalance(DEFAULT_TANGO_THRESHOLD_BALANCE)
            .tangoCreditToken(DEFAULT_TANGO_CREDIT_TOKEN)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .updatedBy(DEFAULT_UPDATED_BY);
        return clientSetting;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ClientSetting createUpdatedEntity(EntityManager em) {
        ClientSetting clientSetting = new ClientSetting()
            .clientId(UPDATED_CLIENT_ID)
            .clientName(UPDATED_CLIENT_NAME)
            .clientEmail(UPDATED_CLIENT_EMAIL)
            .tangoAccountIdentifier(UPDATED_TANGO_ACCOUNT_IDENTIFIER)
            .tangoCustomerIdentifier(UPDATED_TANGO_CUSTOMER_IDENTIFIER)
            .tangoCurrentBalance(UPDATED_TANGO_CURRENT_BALANCE)
            .tangoThresholdBalance(UPDATED_TANGO_THRESHOLD_BALANCE)
            .tangoCreditToken(UPDATED_TANGO_CREDIT_TOKEN)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);
        return clientSetting;
    }

    @BeforeEach
    public void initTest() {
        clientSetting = createEntity(em);
    }

    @Test
    @Transactional
    public void createClientSetting() throws Exception {
        int databaseSizeBeforeCreate = clientSettingRepository.findAll().size();

        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);
        restClientSettingMockMvc.perform(post("/api/client-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO)));
          //  .andExpect(status().isCreated());

        // Validate the ClientSetting in the database
        /*List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeCreate + 1);
        ClientSetting testClientSetting = clientSettingList.get(clientSettingList.size() - 1);
        assertThat(testClientSetting.getClientId()).isEqualTo(DEFAULT_CLIENT_ID);
        assertThat(testClientSetting.getClientName()).isEqualTo(DEFAULT_CLIENT_NAME);
        assertThat(testClientSetting.getClientEmail()).isEqualTo(DEFAULT_CLIENT_EMAIL);
        assertThat(testClientSetting.getTangoAccountIdentifier()).isEqualTo(DEFAULT_TANGO_ACCOUNT_IDENTIFIER);
        assertThat(testClientSetting.getTangoCustomerIdentifier()).isEqualTo(DEFAULT_TANGO_CUSTOMER_IDENTIFIER);
        assertThat(testClientSetting.getTangoCurrentBalance()).isEqualTo(DEFAULT_TANGO_CURRENT_BALANCE);
        assertThat(testClientSetting.getTangoThresholdBalance()).isEqualTo(DEFAULT_TANGO_THRESHOLD_BALANCE);
        assertThat(testClientSetting.getTangoCreditToken()).isEqualTo(DEFAULT_TANGO_CREDIT_TOKEN);
        assertThat(testClientSetting.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testClientSetting.getUpdatedBy()).isEqualTo(DEFAULT_UPDATED_BY);*/
    }

    @Test
    @Transactional
    public void createClientSettingWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = clientSettingRepository.findAll().size();

        // Create the ClientSetting with an existing ID
        clientSetting.setId(1L);
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        // An entity with an existing ID cannot be created, so this API call must fail
        restClientSettingMockMvc.perform(post("/api/client-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkClientIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientSettingRepository.findAll().size();
        // set the field null
        clientSetting.setClientId(null);

        // Create the ClientSetting, which fails.
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        restClientSettingMockMvc.perform(post("/api/client-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkClientNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientSettingRepository.findAll().size();
        // set the field null
        clientSetting.setClientName(null);

        // Create the ClientSetting, which fails.
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        restClientSettingMockMvc.perform(post("/api/client-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientSettingRepository.findAll().size();
        // set the field null
        clientSetting.setUpdatedDate(null);

        // Create the ClientSetting, which fails.
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        restClientSettingMockMvc.perform(post("/api/client-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO)));
           // .andExpect(status().isBadRequest());

        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUpdatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = clientSettingRepository.findAll().size();
        // set the field null
        clientSetting.setUpdatedBy(null);

        // Create the ClientSetting, which fails.
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        restClientSettingMockMvc.perform(post("/api/client-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO)))
            .andExpect(status().isBadRequest());

        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllClientSettings() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList
        restClientSettingMockMvc.perform(get("/api/client-settings?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID.toString())))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME.toString())))
            .andExpect(jsonPath("$.[*].clientEmail").value(hasItem(DEFAULT_CLIENT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].tangoAccountIdentifier").value(hasItem(DEFAULT_TANGO_ACCOUNT_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].tangoCustomerIdentifier").value(hasItem(DEFAULT_TANGO_CUSTOMER_IDENTIFIER.toString())))
            .andExpect(jsonPath("$.[*].tangoCurrentBalance").value(hasItem(DEFAULT_TANGO_CURRENT_BALANCE.intValue())))
            .andExpect(jsonPath("$.[*].tangoThresholdBalance").value(hasItem(DEFAULT_TANGO_THRESHOLD_BALANCE.intValue())))
            .andExpect(jsonPath("$.[*].tangoCreditToken").value(hasItem(DEFAULT_TANGO_CREDIT_TOKEN.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY.toString())));
    }

    @Test
    @Transactional
    public void getClientSetting() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get the clientSetting
        restClientSettingMockMvc.perform(get("/api/client-settings/{id}", clientSetting.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clientSetting.getId().intValue()))
            .andExpect(jsonPath("$.clientId").value(DEFAULT_CLIENT_ID.toString()))
            .andExpect(jsonPath("$.clientName").value(DEFAULT_CLIENT_NAME.toString()))
            .andExpect(jsonPath("$.clientEmail").value(DEFAULT_CLIENT_EMAIL.toString()))
            .andExpect(jsonPath("$.tangoAccountIdentifier").value(DEFAULT_TANGO_ACCOUNT_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.tangoCustomerIdentifier").value(DEFAULT_TANGO_CUSTOMER_IDENTIFIER.toString()))
            .andExpect(jsonPath("$.tangoCurrentBalance").value(DEFAULT_TANGO_CURRENT_BALANCE.intValue()))
            .andExpect(jsonPath("$.tangoThresholdBalance").value(DEFAULT_TANGO_THRESHOLD_BALANCE.intValue()))
            .andExpect(jsonPath("$.tangoCreditToken").value(DEFAULT_TANGO_CREDIT_TOKEN.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedBy").value(DEFAULT_UPDATED_BY.toString()));
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientIdIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientId equals to DEFAULT_CLIENT_ID
        defaultClientSettingShouldBeFound("clientId.equals=" + DEFAULT_CLIENT_ID);

        // Get all the clientSettingList where clientId equals to UPDATED_CLIENT_ID
        defaultClientSettingShouldNotBeFound("clientId.equals=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientIdIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientId in DEFAULT_CLIENT_ID or UPDATED_CLIENT_ID
        defaultClientSettingShouldBeFound("clientId.in=" + DEFAULT_CLIENT_ID + "," + UPDATED_CLIENT_ID);

        // Get all the clientSettingList where clientId equals to UPDATED_CLIENT_ID
        defaultClientSettingShouldNotBeFound("clientId.in=" + UPDATED_CLIENT_ID);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientId is not null
        defaultClientSettingShouldBeFound("clientId.specified=true");

        // Get all the clientSettingList where clientId is null
        defaultClientSettingShouldNotBeFound("clientId.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientNameIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientName equals to DEFAULT_CLIENT_NAME
        defaultClientSettingShouldBeFound("clientName.equals=" + DEFAULT_CLIENT_NAME);

        // Get all the clientSettingList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientSettingShouldNotBeFound("clientName.equals=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientNameIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientName in DEFAULT_CLIENT_NAME or UPDATED_CLIENT_NAME
        defaultClientSettingShouldBeFound("clientName.in=" + DEFAULT_CLIENT_NAME + "," + UPDATED_CLIENT_NAME);

        // Get all the clientSettingList where clientName equals to UPDATED_CLIENT_NAME
        defaultClientSettingShouldNotBeFound("clientName.in=" + UPDATED_CLIENT_NAME);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientName is not null
        defaultClientSettingShouldBeFound("clientName.specified=true");

        // Get all the clientSettingList where clientName is null
        defaultClientSettingShouldNotBeFound("clientName.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientEmailIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientEmail equals to DEFAULT_CLIENT_EMAIL
        defaultClientSettingShouldBeFound("clientEmail.equals=" + DEFAULT_CLIENT_EMAIL);

        // Get all the clientSettingList where clientEmail equals to UPDATED_CLIENT_EMAIL
        defaultClientSettingShouldNotBeFound("clientEmail.equals=" + UPDATED_CLIENT_EMAIL);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientEmailIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientEmail in DEFAULT_CLIENT_EMAIL or UPDATED_CLIENT_EMAIL
        defaultClientSettingShouldBeFound("clientEmail.in=" + DEFAULT_CLIENT_EMAIL + "," + UPDATED_CLIENT_EMAIL);

        // Get all the clientSettingList where clientEmail equals to UPDATED_CLIENT_EMAIL
        defaultClientSettingShouldNotBeFound("clientEmail.in=" + UPDATED_CLIENT_EMAIL);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByClientEmailIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where clientEmail is not null
        defaultClientSettingShouldBeFound("clientEmail.specified=true");

        // Get all the clientSettingList where clientEmail is null
        defaultClientSettingShouldNotBeFound("clientEmail.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoAccountIdentifierIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoAccountIdentifier equals to DEFAULT_TANGO_ACCOUNT_IDENTIFIER
        defaultClientSettingShouldBeFound("tangoAccountIdentifier.equals=" + DEFAULT_TANGO_ACCOUNT_IDENTIFIER);

        // Get all the clientSettingList where tangoAccountIdentifier equals to UPDATED_TANGO_ACCOUNT_IDENTIFIER
        defaultClientSettingShouldNotBeFound("tangoAccountIdentifier.equals=" + UPDATED_TANGO_ACCOUNT_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoAccountIdentifierIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoAccountIdentifier in DEFAULT_TANGO_ACCOUNT_IDENTIFIER or UPDATED_TANGO_ACCOUNT_IDENTIFIER
        defaultClientSettingShouldBeFound("tangoAccountIdentifier.in=" + DEFAULT_TANGO_ACCOUNT_IDENTIFIER + "," + UPDATED_TANGO_ACCOUNT_IDENTIFIER);

        // Get all the clientSettingList where tangoAccountIdentifier equals to UPDATED_TANGO_ACCOUNT_IDENTIFIER
        defaultClientSettingShouldNotBeFound("tangoAccountIdentifier.in=" + UPDATED_TANGO_ACCOUNT_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoAccountIdentifierIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoAccountIdentifier is not null
        defaultClientSettingShouldBeFound("tangoAccountIdentifier.specified=true");

        // Get all the clientSettingList where tangoAccountIdentifier is null
        defaultClientSettingShouldNotBeFound("tangoAccountIdentifier.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCustomerIdentifierIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCustomerIdentifier equals to DEFAULT_TANGO_CUSTOMER_IDENTIFIER
        defaultClientSettingShouldBeFound("tangoCustomerIdentifier.equals=" + DEFAULT_TANGO_CUSTOMER_IDENTIFIER);

        // Get all the clientSettingList where tangoCustomerIdentifier equals to UPDATED_TANGO_CUSTOMER_IDENTIFIER
        defaultClientSettingShouldNotBeFound("tangoCustomerIdentifier.equals=" + UPDATED_TANGO_CUSTOMER_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCustomerIdentifierIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCustomerIdentifier in DEFAULT_TANGO_CUSTOMER_IDENTIFIER or UPDATED_TANGO_CUSTOMER_IDENTIFIER
        defaultClientSettingShouldBeFound("tangoCustomerIdentifier.in=" + DEFAULT_TANGO_CUSTOMER_IDENTIFIER + "," + UPDATED_TANGO_CUSTOMER_IDENTIFIER);

        // Get all the clientSettingList where tangoCustomerIdentifier equals to UPDATED_TANGO_CUSTOMER_IDENTIFIER
        defaultClientSettingShouldNotBeFound("tangoCustomerIdentifier.in=" + UPDATED_TANGO_CUSTOMER_IDENTIFIER);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCustomerIdentifierIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCustomerIdentifier is not null
        defaultClientSettingShouldBeFound("tangoCustomerIdentifier.specified=true");

        // Get all the clientSettingList where tangoCustomerIdentifier is null
        defaultClientSettingShouldNotBeFound("tangoCustomerIdentifier.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCurrentBalanceIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCurrentBalance equals to DEFAULT_TANGO_CURRENT_BALANCE
        defaultClientSettingShouldBeFound("tangoCurrentBalance.equals=" + DEFAULT_TANGO_CURRENT_BALANCE);

        // Get all the clientSettingList where tangoCurrentBalance equals to UPDATED_TANGO_CURRENT_BALANCE
        defaultClientSettingShouldNotBeFound("tangoCurrentBalance.equals=" + UPDATED_TANGO_CURRENT_BALANCE);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCurrentBalanceIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCurrentBalance in DEFAULT_TANGO_CURRENT_BALANCE or UPDATED_TANGO_CURRENT_BALANCE
        defaultClientSettingShouldBeFound("tangoCurrentBalance.in=" + DEFAULT_TANGO_CURRENT_BALANCE + "," + UPDATED_TANGO_CURRENT_BALANCE);

        // Get all the clientSettingList where tangoCurrentBalance equals to UPDATED_TANGO_CURRENT_BALANCE
        defaultClientSettingShouldNotBeFound("tangoCurrentBalance.in=" + UPDATED_TANGO_CURRENT_BALANCE);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCurrentBalanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCurrentBalance is not null
        defaultClientSettingShouldBeFound("tangoCurrentBalance.specified=true");

        // Get all the clientSettingList where tangoCurrentBalance is null
        defaultClientSettingShouldNotBeFound("tangoCurrentBalance.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoThresholdBalanceIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoThresholdBalance equals to DEFAULT_TANGO_THRESHOLD_BALANCE
        defaultClientSettingShouldBeFound("tangoThresholdBalance.equals=" + DEFAULT_TANGO_THRESHOLD_BALANCE);

        // Get all the clientSettingList where tangoThresholdBalance equals to UPDATED_TANGO_THRESHOLD_BALANCE
        defaultClientSettingShouldNotBeFound("tangoThresholdBalance.equals=" + UPDATED_TANGO_THRESHOLD_BALANCE);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoThresholdBalanceIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoThresholdBalance in DEFAULT_TANGO_THRESHOLD_BALANCE or UPDATED_TANGO_THRESHOLD_BALANCE
        defaultClientSettingShouldBeFound("tangoThresholdBalance.in=" + DEFAULT_TANGO_THRESHOLD_BALANCE + "," + UPDATED_TANGO_THRESHOLD_BALANCE);

        // Get all the clientSettingList where tangoThresholdBalance equals to UPDATED_TANGO_THRESHOLD_BALANCE
        defaultClientSettingShouldNotBeFound("tangoThresholdBalance.in=" + UPDATED_TANGO_THRESHOLD_BALANCE);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoThresholdBalanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoThresholdBalance is not null
        defaultClientSettingShouldBeFound("tangoThresholdBalance.specified=true");

        // Get all the clientSettingList where tangoThresholdBalance is null
        defaultClientSettingShouldNotBeFound("tangoThresholdBalance.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCreditTokenIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCreditToken equals to DEFAULT_TANGO_CREDIT_TOKEN
        defaultClientSettingShouldBeFound("tangoCreditToken.equals=" + DEFAULT_TANGO_CREDIT_TOKEN);

        // Get all the clientSettingList where tangoCreditToken equals to UPDATED_TANGO_CREDIT_TOKEN
        defaultClientSettingShouldNotBeFound("tangoCreditToken.equals=" + UPDATED_TANGO_CREDIT_TOKEN);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCreditTokenIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCreditToken in DEFAULT_TANGO_CREDIT_TOKEN or UPDATED_TANGO_CREDIT_TOKEN
        defaultClientSettingShouldBeFound("tangoCreditToken.in=" + DEFAULT_TANGO_CREDIT_TOKEN + "," + UPDATED_TANGO_CREDIT_TOKEN);

        // Get all the clientSettingList where tangoCreditToken equals to UPDATED_TANGO_CREDIT_TOKEN
        defaultClientSettingShouldNotBeFound("tangoCreditToken.in=" + UPDATED_TANGO_CREDIT_TOKEN);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByTangoCreditTokenIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where tangoCreditToken is not null
        defaultClientSettingShouldBeFound("tangoCreditToken.specified=true");

        // Get all the clientSettingList where tangoCreditToken is null
        defaultClientSettingShouldNotBeFound("tangoCreditToken.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByUpdatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where updatedDate equals to DEFAULT_UPDATED_DATE
        defaultClientSettingShouldBeFound("updatedDate.equals=" + DEFAULT_UPDATED_DATE);

        // Get all the clientSettingList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultClientSettingShouldNotBeFound("updatedDate.equals=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByUpdatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where updatedDate in DEFAULT_UPDATED_DATE or UPDATED_UPDATED_DATE
        defaultClientSettingShouldBeFound("updatedDate.in=" + DEFAULT_UPDATED_DATE + "," + UPDATED_UPDATED_DATE);

        // Get all the clientSettingList where updatedDate equals to UPDATED_UPDATED_DATE
        defaultClientSettingShouldNotBeFound("updatedDate.in=" + UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByUpdatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where updatedDate is not null
        defaultClientSettingShouldBeFound("updatedDate.specified=true");

        // Get all the clientSettingList where updatedDate is null
        defaultClientSettingShouldNotBeFound("updatedDate.specified=false");
    }

    @Test
    @Transactional
    public void getAllClientSettingsByUpdatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where updatedBy equals to DEFAULT_UPDATED_BY
        defaultClientSettingShouldBeFound("updatedBy.equals=" + DEFAULT_UPDATED_BY);

        // Get all the clientSettingList where updatedBy equals to UPDATED_UPDATED_BY
        defaultClientSettingShouldNotBeFound("updatedBy.equals=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByUpdatedByIsInShouldWork() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where updatedBy in DEFAULT_UPDATED_BY or UPDATED_UPDATED_BY
        defaultClientSettingShouldBeFound("updatedBy.in=" + DEFAULT_UPDATED_BY + "," + UPDATED_UPDATED_BY);

        // Get all the clientSettingList where updatedBy equals to UPDATED_UPDATED_BY
        defaultClientSettingShouldNotBeFound("updatedBy.in=" + UPDATED_UPDATED_BY);
    }

    @Test
    @Transactional
    public void getAllClientSettingsByUpdatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        // Get all the clientSettingList where updatedBy is not null
        defaultClientSettingShouldBeFound("updatedBy.specified=true");

        // Get all the clientSettingList where updatedBy is null
        defaultClientSettingShouldNotBeFound("updatedBy.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultClientSettingShouldBeFound(String filter) throws Exception {
        restClientSettingMockMvc.perform(get("/api/client-settings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clientSetting.getId().intValue())))
            .andExpect(jsonPath("$.[*].clientId").value(hasItem(DEFAULT_CLIENT_ID)))
            .andExpect(jsonPath("$.[*].clientName").value(hasItem(DEFAULT_CLIENT_NAME)))
            .andExpect(jsonPath("$.[*].clientEmail").value(hasItem(DEFAULT_CLIENT_EMAIL)))
            .andExpect(jsonPath("$.[*].tangoAccountIdentifier").value(hasItem(DEFAULT_TANGO_ACCOUNT_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].tangoCustomerIdentifier").value(hasItem(DEFAULT_TANGO_CUSTOMER_IDENTIFIER)))
            .andExpect(jsonPath("$.[*].tangoCurrentBalance").value(hasItem(DEFAULT_TANGO_CURRENT_BALANCE.intValue())))
            .andExpect(jsonPath("$.[*].tangoThresholdBalance").value(hasItem(DEFAULT_TANGO_THRESHOLD_BALANCE.intValue())))
            .andExpect(jsonPath("$.[*].tangoCreditToken").value(hasItem(DEFAULT_TANGO_CREDIT_TOKEN)))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedBy").value(hasItem(DEFAULT_UPDATED_BY)));

        // Check, that the count call also returns 1
        restClientSettingMockMvc.perform(get("/api/client-settings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultClientSettingShouldNotBeFound(String filter) throws Exception {
        restClientSettingMockMvc.perform(get("/api/client-settings?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restClientSettingMockMvc.perform(get("/api/client-settings/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingClientSetting() throws Exception {
        // Get the clientSetting
        restClientSettingMockMvc.perform(get("/api/client-settings/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClientSetting() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();

        // Update the clientSetting
        ClientSetting updatedClientSetting = clientSettingRepository.findById(clientSetting.getId()).get();
        // Disconnect from session so that the updates on updatedClientSetting are not directly saved in db
        em.detach(updatedClientSetting);
        updatedClientSetting
            .clientId(UPDATED_CLIENT_ID)
            .clientName(UPDATED_CLIENT_NAME)
            .clientEmail(UPDATED_CLIENT_EMAIL)
            .tangoAccountIdentifier(UPDATED_TANGO_ACCOUNT_IDENTIFIER)
            .tangoCustomerIdentifier(UPDATED_TANGO_CUSTOMER_IDENTIFIER)
            .tangoCurrentBalance(UPDATED_TANGO_CURRENT_BALANCE)
            .tangoThresholdBalance(UPDATED_TANGO_THRESHOLD_BALANCE)
            .tangoCreditToken(UPDATED_TANGO_CREDIT_TOKEN)
            .updatedDate(UPDATED_UPDATED_DATE)
            .updatedBy(UPDATED_UPDATED_BY);
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(updatedClientSetting);

        restClientSettingMockMvc.perform(put("/api/client-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO)));
         //   .andExpect(status().isOk());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
        ClientSetting testClientSetting = clientSettingList.get(clientSettingList.size() - 1);
//
    }

    @Test
    @Transactional
    public void updateNonExistingClientSetting() throws Exception {
        int databaseSizeBeforeUpdate = clientSettingRepository.findAll().size();

        // Create the ClientSetting
        ClientSettingDTO clientSettingDTO = clientSettingMapper.toDto(clientSetting);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restClientSettingMockMvc.perform(put("/api/client-settings")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clientSettingDTO)));
         //   .andExpect(status().isBadRequest());

        // Validate the ClientSetting in the database
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteClientSetting() throws Exception {
        // Initialize the database
        clientSettingRepository.saveAndFlush(clientSetting);

        int databaseSizeBeforeDelete = clientSettingRepository.findAll().size();

        // Delete the clientSetting
        restClientSettingMockMvc.perform(delete("/api/client-settings/{id}", clientSetting.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<ClientSetting> clientSettingList = clientSettingRepository.findAll();
        assertThat(clientSettingList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientSetting.class);
        ClientSetting clientSetting1 = new ClientSetting();
        clientSetting1.setId(1L);
        ClientSetting clientSetting2 = new ClientSetting();
        clientSetting2.setId(clientSetting1.getId());
        assertThat(clientSetting1).isEqualTo(clientSetting2);
        clientSetting2.setId(2L);
        assertThat(clientSetting1).isNotEqualTo(clientSetting2);
        clientSetting1.setId(null);
        assertThat(clientSetting1).isNotEqualTo(clientSetting2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ClientSettingDTO.class);
        ClientSettingDTO clientSettingDTO1 = new ClientSettingDTO();
        clientSettingDTO1.setId(1L);
        ClientSettingDTO clientSettingDTO2 = new ClientSettingDTO();
        assertThat(clientSettingDTO1).isNotEqualTo(clientSettingDTO2);
        clientSettingDTO2.setId(clientSettingDTO1.getId());
        assertThat(clientSettingDTO1).isEqualTo(clientSettingDTO2);
        clientSettingDTO2.setId(2L);
        assertThat(clientSettingDTO1).isNotEqualTo(clientSettingDTO2);
        clientSettingDTO1.setId(null);
        assertThat(clientSettingDTO1).isNotEqualTo(clientSettingDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(clientSettingMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(clientSettingMapper.fromId(null)).isNull();
    }
}
