package aduro.basic.programservice.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import aduro.basic.programservice.web.rest.TestUtil;

public class ProgramUserLevelProgressDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserLevelProgressDTO.class);
        ProgramUserLevelProgressDTO programUserLevelProgressDTO1 = new ProgramUserLevelProgressDTO();
        programUserLevelProgressDTO1.setId(1L);
        ProgramUserLevelProgressDTO programUserLevelProgressDTO2 = new ProgramUserLevelProgressDTO();
        assertThat(programUserLevelProgressDTO1).isNotEqualTo(programUserLevelProgressDTO2);
        programUserLevelProgressDTO2.setId(programUserLevelProgressDTO1.getId());
        assertThat(programUserLevelProgressDTO1).isEqualTo(programUserLevelProgressDTO2);
        programUserLevelProgressDTO2.setId(2L);
        assertThat(programUserLevelProgressDTO1).isNotEqualTo(programUserLevelProgressDTO2);
        programUserLevelProgressDTO1.setId(null);
        assertThat(programUserLevelProgressDTO1).isNotEqualTo(programUserLevelProgressDTO2);
    }
}
