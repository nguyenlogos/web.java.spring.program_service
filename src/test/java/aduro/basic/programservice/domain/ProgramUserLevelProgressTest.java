package aduro.basic.programservice.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import aduro.basic.programservice.web.rest.TestUtil;

public class ProgramUserLevelProgressTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProgramUserLevelProgress.class);
        ProgramUserLevelProgress programUserLevelProgress1 = new ProgramUserLevelProgress();
        programUserLevelProgress1.setId(1L);
        ProgramUserLevelProgress programUserLevelProgress2 = new ProgramUserLevelProgress();
        programUserLevelProgress2.setId(programUserLevelProgress1.getId());
        assertThat(programUserLevelProgress1).isEqualTo(programUserLevelProgress2);
        programUserLevelProgress2.setId(2L);
        assertThat(programUserLevelProgress1).isNotEqualTo(programUserLevelProgress2);
        programUserLevelProgress1.setId(null);
        assertThat(programUserLevelProgress1).isNotEqualTo(programUserLevelProgress2);
    }
}
