FROM openjdk:8-jdk

EXPOSE 8182

# Install vim
RUN apt-get update && apt-get install -y vim

# ==========================================
# Install nginx
RUN apt-get update && apt-get install -y --allow-unauthenticated nginx
#============================================


# Create user
RUN useradd --create-home program && \
    mkdir -p /home/program/ && \
    mkdir -p /home/program/logs

# Copy file to container
COPY "target/program.jar" "/home/program/program.jar"
COPY "etc/bin/entrypoint.sh" "/home/program/"
COPY "etc/nginx/conf/program.nginx" "/etc/nginx/sites-enabled/program.nginx"

# Edit permissions
RUN chown program:program /home/program/* && \
    chmod 700 /home/program/entrypoint.sh

# Change working dir
WORKDIR /home/program

# Entry point
ENTRYPOINT ./entrypoint.sh
