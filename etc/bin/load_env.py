#!/bin/env python
import boto3
import sys
import os
import collections

region = sys.argv[1]
envFile = sys.argv[2]
dynamodb = boto3.resource('dynamodb', region)
appName = os.environ['APPLICATION_NAME']
appEnv = os.environ['APPLICATION_ENV']
table = dynamodb.Table('configuration')
response = table.get_item(Key={'app': appName, 'env': appEnv})
item = response['Item']
item_sorted = collections.OrderedDict(sorted(item.items()))
with open(envFile, 'w') as f:
    for key, value in item_sorted.iteritems():
        if value != '""':
            f.write('{}={}\n'.format(key, value))
        else:
            f.write('{}=\n'.format(key))
    f.close()
