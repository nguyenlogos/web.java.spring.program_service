#!/bin/bash

set -x

# Config nginx
service nginx stop
unlink /etc/nginx/sites-enabled/default
nginx -t
service nginx restart

# Running java app
java -server \
     -Dserver.port=8080 \
     -jar /home/program/program.jar
